<?php

namespace App;

class ApiConfig {
   // const API_BASE_URL = 'http://192.168.1.154:3033/v1/';
   // const API_BASE_URL = 'http://15.206.148.217:3033/v1/'; // Live URl
   // const API_BASE_URL = 'http://127.0.0.1:3033/v1/'; // Local URl
   // const API_BASE_URL = 'http://192.168.1.154:3033/v1/'; // Local URl Dwani PC
   const API_BASE_URL = 'http://127.0.0.1:8000/api/v1/'; // Laravel API Local URl
   // const API_BASE_URL = 'http://localhost:3033/v1/'; // Local URl

   const API_PORT = '8000';

   const ACCESS_TOKEN = "asdrrqwq232lKJ23JLJ2ljsflkk32assdDSF";
   // const ACCESS_TOKEN = "mSN76fFyb36WODY7MpgNDj9otIFqwNx3";
   const REGISTRATION_TOKEN = "123";

   const PAGINATION_LIMIT = 10;

   // Login Apis
   const SESSION_TOKEN_URL = "sessions";
   const ADMIN_LOGIN_URL = "admin/auth/%s";

   // Dashboard Module Apis

   const ADMIN_DASHBOARD_DETAILS_URL = "admin/dashboard";
   const ADMIN_MONTHLY_CHART_URL = "admin/dashboard/monthchart";
   const ADMIN_DAILY_CHART_URL = "admin/dashboard/daychart";
   const ADMIN_BUYER_LATEST_ORDER_URL = "admin/dashboard/latestOrder";
   const ADMIN_SUPPLIER_LATEST_ORDER_URL = "admin/dashboard/latestVendorOrder";
   const ADMIN_LOGISTICS_LATEST_ORDER_URL = "admin/dashboard/latestLogisticsOrder";
   const ADMIN_UNVERIFIED_PRODUCT_URL = "admin/dashboard/unverifiedProduct";

   const ADMIN_PRODUCT_DETAIL_URL = "admin/product/%s";


   // User Module Apis
   const ADMIN_USER_LISTING_URL = "admin/users/adminlisting";
   const ADMIN_USER_ADD_URL = "admin/users";
   const ADMIN_USER_EDIT_URL = "admin/users/%s";
   const ADMIN_REQUEST_OTP_URL = "admin/users/requestOTP";
   const ADMIN_REQUEST_OTP_FOR_EDIT_PROFILE_URL = "admin/users/requestOTPForUpdateProfile";
   const ADMIN_USER_DELETE_URL = "admin/users/%s";

   const ADMIN_LOGISTICS_LISTING_URL = "admin/users/logisticslisting";
   const ADMIN_SUPPLIERS_LISTING_URL = "admin/users/vendorlisting";
   const ADMIN_SUPPLIERS_PLANTS_LISTING_URL = "admin/users/vendorAddresslisting?vendor_id=%s";
   const ADMIN_SUPPLIERS_VERIFY_URL = "admin/users/editVendor/%s";
   const ADMIN_SUPPLIERS_PLANT_VERIFY_URL = "admin/users/updateVendorAddress/%s";
   const ADMIN_SUPPLIERS_PLANT_BLOCK_URL = "admin/users/updateVendorAddress/%s";
   const ADMIN_SUPPLIERS_BLOCK_URL = "admin/users/editVendor/%s";
   const ADMIN_SUPPLIERS_REJECTED_URL = "admin/users/editVendor/%s";
   const ADMIN_SUPPLIERS_PLANT_REJECTED_URL = "admin/users/updateVendorAddress/%s";
   const ADMIN_LOGISTICS_VERIFY_URL = "admin/users/editLogistics/%s";
   const ADMIN_BUYERS_LISTING_URL = "admin/users/buyerlisting";


   // Vehicle Module Apis
   const ADMIN_TM_CATEGORY_LISTING_URL = "admin/TM/category";
   const ADMIN_TM_CATEGORY_ADD_URL = "admin/TM/category";
   const ADMIN_TM_CATEGORY_EDIT_URL = "admin/TM/category/%s";

   const ADMIN_TM_SUB_CATEGORY_LISTING_URL = "admin/TM/subCategory";
   const ADMIN_TM_SUB_CATEGORY_ADD_URL = "admin/TM/subCategory/%s";

   const ADMIN_TM_SUB_CATEGORY_EDIT_URL = "admin/TM/subCategory/%s";


   const ADMIN_PUMP_CATEGORY_LISTING_URL = "admin/concrete_pump/category";
   const ADMIN_PUMP_CATEGORY_ADD_URL = "admin/concrete_pump/category";
   const ADMIN_PUMP_CATEGORY_EDIT_URL = "admin/concrete_pump/category/%s";


   const ADMIN_VEHICLE_LISTING_URL = "admin/TM";

   const ADMIN_VEHICLE_PM_KM_ADD_URL = "admin/vehicle_rate";
   const ADMIN_VEHICLE_PM_KM_EDIT_URL = "admin/vehicle_rate/%s";
   const ADMIN_VEHICLE_PM_KM_LISTING_URL = "admin/vehicle_rate";

   const GET_PM_KM_RATE_BY_STATE_ID = "logistics/vehicle/calculateRate/%s";

   // Region Module Apis
   const ADMIN_REGION_COUNTRY_LISTING_URL = "admin/region/country";
   const ADMIN_REGION_COUNTRY_ADD_URL = "admin/region/addCountry";
   const ADMIN_REGION_COUNTRY_EDIT_URL = "admin/region/editCountry/%s";
   const ADMIN_REGION_COUNTRY_DELETE_URL = "admin/region/removeCountry/%s";

   const ADMIN_REGION_STATE_LISTING_URL = "admin/region/state";
   const ADMIN_REGION_STATE_ADD_URL = "admin/region/addState";
   const ADMIN_REGION_STATE_EDIT_URL = "admin/region/editState/%s";
   const ADMIN_REGION_STATE_DELETE_URL = "admin/region/removeState/%s";

   const ADMIN_REGION_CITY_LISTING_URL = "admin/region/city";
   const ADMIN_REGION_CITY_ADD_URL = "admin/region/addCity";
   const ADMIN_REGION_CITY_EDIT_URL = "admin/region/editCity/%s";
   const ADMIN_REGION_CITY_DELETE_URL = "admin/region/removeCity/%s";

   //Porduct Moduel Apis

   const ADMIN_PRODUCT_CATEGORY_LISTING_URL = "admin/aggregate-sand-category";
   const ADMIN_PRODUCT_CATEGORY_ADD_URL = "admin/aggregate-sand-category";
   const ADMIN_PRODUCT_CATEGORY_EDIT_URL = "admin/aggregate-sand-category/%s";

   const ADMIN_PRODUCT_SUB_CATEGORY_LISTING_URL = "admin/aggregate-sand-category/subcategory/%s";
   const ADMIN_PRODUCT_SUB_CATEGORY_ADD_URL = "admin/aggregate-sand-category/subcategory/%s";
   const ADMIN_PRODUCT_SUB_CATEGORY_EDIT_URL = "admin/aggregate-sand-category/subcategory/%s";


   const ADMIN_PRODUCTS_LISTING_URL = "admin/design_mix";
   const ADMIN_PRODUCTS_EDIT_URL = "admin/product/%s";
   const ADMIN_PRODUCTS_DETAIL_URL = "admin/design_mix/%s";

   const ADMIN_AGGREGATE_SOURCE_LISTING_URL = "admin/aggregate_source";
   const ADMIN_AGGREGATE_SOURCE_ADD_URL = "admin/aggregate_source";
   const ADMIN_AGGREGATE_SOURCE_EDIT_URL = "admin/aggregate_source/%s";
   const ADMIN_AGGREGATE_SOURCE_DELETE_URL = "admin/aggregate_source/%s";


   const ADMIN_SAND_SOURCE_LISTING_URL = "admin/sand_source";
   const ADMIN_SAND_SOURCE_ADD_URL = "admin/sand_source";
   const ADMIN_SAND_SOURCE_EDIT_URL = "admin/sand_source/%s";
   const ADMIN_SAND_SOURCE_DELETE_URL = "admin/sand_source/%s";
   
   const ADMIN_SAND_ZONE_LISTING_URL = "admin/sand_zone";
   const ADMIN_SAND_ZONE_ADD_URL = "admin/sand_zone";
   const ADMIN_SAND_ZONE_EDIT_URL = "admin/sand_zone/%s";
   const ADMIN_SAND_ZONE_DELETE_URL = "admin/sand_zone/%s";

   const ADMIN_FLYASH_SOURCE_LISTING_URL = "admin/fly_ash_source";
   const ADMIN_FLYASH_SOURCE_ADD_URL = "admin/fly_ash_source";
   const ADMIN_FLYASH_SOURCE_EDIT_URL = "admin/fly_ash_source/%s";
   const ADMIN_FLYASH_SOURCE_DELETE_URL = "admin/fly_ash_source/%s";


   const ADMIN_GST_SLAB_LISTING_URL = "admin/gst_slab";
   const ADMIN_GST_SLAB_ADD_URL = "admin/gst_slab";
   const ADMIN_GST_SLAB_EDIT_URL = "admin/gst_slab/%s";


   const ADMIN_MARGIN_RATE_LISTING_URL = "admin/margin_rate";
   const ADMIN_MARGIN_RATE_ADD_URL = "admin/margin_rate";
   const ADMIN_MARGIN_RATE_EDIT_URL = "admin/margin_rate/%s";

   //Email Tempalte Apis

   const ADMIN_EMAIL_TEMPLATE_LISTING_URL = "admin/email-template";
   const ADMIN_EMAIL_TEMPLATE_ADD_URL = "admin/email-template/%s";
   const ADMIN_EMAIL_TEMPLATE_GET_URL = "admin/email-template/%s";
   const ADMIN_EMAIL_TEMPLATE_EDIT_URL = "admin/email-template/%s";
   const ADMIN_EMAIL_TEMPLATE_ADD_ATTACHMENT_URL = "admin/email-template/attachments/%s";
   const ADMIN_EMAIL_TEMPLATE_DELETE_ATTACHMENT_URL = "admin/email-template/attachments/%s";


   //Product Reviews Apis
   const ADMIN_PRODUCT_REVIEWS_LISTING_URL = "admin/review";
   const ADMIN_FEEDBACK_LISTING_URL = "admin/feedback";
   const ADMIN_PRODUCT_REVIEWS_EDIT_URL = "admin/review/%s";

   //Access Log Apis
   const ADMIN_EMAIL_ACCESS_LOG_LISTING_URL = "admin/access-logs/email";
   const ADMIN_EMAIL_RESEND_EMAIL_URL = "admin/send-mail";

   const ADMIN_ACTION_LOG_LISTING_URL = "admin/access-logs";

   //Ledger Apis
   const ADMIN_ORDERS_LISTING_URL = "admin/order";
   // const ADMIN_ORDERS_STATUS_UPDATE_URL = "admin/order/%s";
   const ADMIN_ORDERS_STATUS_UPDATE_URL = "admin/order/status/%s";
   const ADMIN_VENDOR_ORDERS_STATUS_UPDATE_URL = "admin/order/%s/updateVendorOrderStatus";
   const ADMIN_ORDERS_DETAILS_URL = "admin/order/%s";
   const ADMIN_ORDERS_ITEM_STATUS_URL = "admin/order/item/%s";
   const ADMIN_ORDERS_INVOICE_URL = "admin/invoice";
   const ADMIN_ORDERS_ITEM_TRACK_URL = "admin/order_track";

   const ADMIN_SUPPLIERS_ORDERS_LISTING_URL = "admin/order/vendorOrder";
   const ADMIN_SUPPLIERS_ORDER_DETAIL_URL = "admin/order/vendorOrder/%s";
   const ADMIN_SUPPLIERS_ORDER_BILL_STATUS_UPDATE_URL = "admin/order/%s/updateVendorOrderBillStatus";
   const ADMIN_SUPPLIERS_ORDER_CREDIT_BILL_STATUS_UPDATE_URL = "admin/order/%s/updateVendorOrderCreditNoteStatus";
   const ADMIN_SUPPLIERS_ORDER_DEBIT_BILL_STATUS_UPDATE_URL = "admin/order/%s/updateVendorOrderDebitNoteStatus";
         const ADMIN_LOGISTICS_ORDERS_LISTING_URL = "admin/order/logisticsOrder";
         const ADMIN_LOGISTICS_ORDERS_DETAIL_URL = "admin/order/logisticsOrder/%s";
         const ADMIN_LOGISTICS_ORDER_BILL_STATUS_UPDATE_URL = "admin/order/%s/updateLogisticsOrderBillStatus";
         const ADMIN_LOGISTICS_ORDER_CREDIT_BILL_STATUS_UPDATE_URL = "admin/order/%s/updateLogisticsOrderCreditNoteStatus";
         const ADMIN_LOGISTICS_ORDER_DEBIT_BILL_STATUS_UPDATE_URL = "admin/order/%s//updateLogisticsOrderDebitNoteStatus";
         const ADMIN_LOGISTICS_BUYER_ORDER_TRACK_URL = "admin/order_track/buyerOrder/%s";
   const ADMIN_BUYER_ORDER_CP_TRACK_URL = "admin/order_track/buyerOrder/CP_track/%s";
   const ADMIN_SUPPLIER_ORDER_CP_TRACK_URL = "admin/order_track/vendorOrder/CP_track/%s";
   const ADMIN_LOGISTICS_BUYER_ORDER_ITEM_TRACK_URL = "admin/order_track/buyerOrder/%s/%s";
   const ADMIN_LOGISTICS_SUPPLIER_ORDER_ITEM_TRACK_URL = "admin/order_track/vendorOrder/%s/%s";
   const ADMIN_LOGISTICS_LOGISTICS_ORDER_ITEM_TRACK_URL = "admin/order_track/logisticsOrder/%s/%s";

   const ADMIN_TRANSACTION_LISTING_URL = "admin/transaction";

   // Support Ticket Apis
   const ADMIN_SUPPORT_TICKET_LISTING_URL = "admin/support_ticket";
   const ADMIN_SUPPORT_TICKET_ADD_URL = "admin/support_ticket";
   const ADMIN_SUPPORT_TICKET_DETAIL_URL = "admin/support_ticket/%s";
   const ADMIN_SUPPORT_TICKET_REPLY_URL = "admin/support_ticket/reply";
   const ADMIN_SUPPORT_TICKET_STATUS_UPDATE_URL = "admin/support_ticket/%s";
   const ADMIN_SUPPORT_TICKET_MESSAGE_URL = "admin/support_ticket/reply/%s";

   //Proposal APIs
   const ADMIN_PROPOSAL_LISTING_URL = "admin/rfp";

   //Profile APis
   const ADMIN_PROFILE_DETAIL_URL = "admin/users/profile";
   const ADMIN_PROFILE_UPDATE_URL = "admin/users";
   const ADMIN_CHANGE_PASSWORD_URL = "admin/users/changePassword";
   const ADMIN_FORGOT_PASSWORD_URL = "admin/users/forgotPassword";

   const ADMIN_FORGOT_PASS_OTP_SEND_VIA_MOBILE_URL = "admin/auth/users/%s";
   const ADMIN_FORGOT_PASS_OTP_SEND_VIA_EMAIL_URL = "admin/auth/users/%s";
   const ADMIN_FORGOT_PASS_OTP_VERIFY_URL = "admin/auth/users/%s/codes/%s";


   //Reports
   const ADMIN_ADMIX_BRAND_LISTING_REPORT_URL = "admin/report/admix_brand";
   const ADMIN_ADMIX_TYPES_LISTING_REPORT_URL = "admin/report/admix_types";
   const ADMIN_AGGR_SAND_LISTING_REPORT_URL = "admin/report/agg_sand_cat";
   const ADMIN_AGGR_SAND_SUB_CAT_LISTING_REPORT_URL = "admin/report/agg_sand_subcategory";
   const ADMIN_AGGR_SOURCE_LISTING_REPORT_URL = "admin/report/agg_source";
   const ADMIN_BUYER_LISTING_REPORT_URL = "admin/report/buyerlisting";
   const ADMIN_BUYER_ORDERS_REPORT_URL = "admin/report/buyerorder";
   const ADMIN_BUYER_ORDERS_DETAIL_REPORT_URL = "admin/report/buyerorder/%s";
   const ADMIN_CEMENT_BRAND_REPORT_URL = "admin/report/cement_brand";
   const ADMIN_CEMENT_GRADE_REPORT_URL = "admin/report/cement_grade";
   const ADMIN_CONCRETE_GRADE_REPORT_URL = "admin/report/concrete_grade";
   const ADMIN_CONCRETE_PUMP_CATEGORY_REPORT_URL = "admin/report/concrete_pump_category";
   const ADMIN_COUPON_REPORT_URL = "admin/report/coupon";
   const ADMIN_SUPPORT_TICKET_REPORT_URL = "admin/report/support_ticket";
   const ADMIN_FLYASH_SOURCE_REPORT_URL = "admin/report/flyAsh_source";
   const ADMIN_GST_SLAB_REPORT_URL = "admin/report/gst_slab";
   const ADMIN_MARGIN_RATE_REPORT_URL = "admin/report/margin_rate";
   const ADMIN_SAND_SOURCE_REPORT_URL = "admin/report/sand_source";
   const ADMIN_TM_CAT_REPORT_URL = "admin/report/TM_category";
   const ADMIN_TM_SUB_CAT_REPORT_URL = "admin/report/TM_subCategory";


   const ADMIN_SUPPLIER_LISTING_REPORT_URL = "admin/report/vendorlisting";
   const ADMIN_LOGISTICS_LISTING_REPORT_URL = "admin/report/logisticslisting";
   
   const ADMIN_SUPPLIER_ORDERS_REPORT_URL = "admin/report/vendorOrder";
   const ADMIN_SUPPLIER_ORDERS_DETAIL_URL = "admin/report/vendorOrder/%s";
   const ADMIN_LOGISTICS_ORDERS_REPORT_URL = "admin/report/logisticsorder";
   const ADMIN_LOGISTICS_ORDERS_DETAILS_URL = "admin/report/logisticsorder/%s";
   const ADMIN_DESIGN_MIX_LIST_REPORT_URL = "admin/report/designMixList";
   const ADMIN_DESIGN_MIX_DETAIL_REPORT_URL = "admin/report/design_mix/%s";
   // const ADMIN_VEHICLE_LIST_REPORT_URL = "admin/report/vehicles";
   const ADMIN_VEHICLE_LIST_REPORT_URL = "admin/report/TM";


   //Conmkhamanix...............

   //Grade Cat Brand......................................

   const ADMIN_GET_CONCRETE_GRADE_URL = "admin/concrete_grade";
   const ADMIN_ADD_CONCRETE_GRADE_URL = "admin/concrete_grade";
   const ADMIN_EDIT_CONCRETE_GRADE_URL = "admin/concrete_grade/%s";

   const ADMIN_GET_CEMENT_BRAND_URL = "admin/cement_brand";
   const ADMIN_ADD_CEMENT_BRAND_URL = "admin/cement_brand";
   const ADMIN_EDIT_CEMENT_BRAND_URL = "admin/cement_brand/%s";

   const ADMIN_GET_CEMENT_GRADE_URL = "admin/cement_grade";
   const ADMIN_ADD_CEMENT_GRADE_URL = "admin/cement_grade";
   const ADMIN_EDIT_CEMENT_GRADE_URL = "admin/cement_grade/%s";

   const ADMIN_GET_ADMIXTURE_BRAND_URL = "admin/admixture_brand";
   const ADMIN_GET_ADMIXTURE_CAT_TYPES_URL = "admin/admixture_type";
   const ADMIN_ADD_ADMIXTURE_BRAND_URL = "admin/admixture_brand";
   const ADMIN_EDIT_ADMIXTURE_BRAND_URL = "admin/admixture_brand/%s";

   const ADMIN_GET_ADMIXTURE_TYPES_URL = "admin/admixture_category";
   const ADMIN_ADD_ADMIXTURE_TYPES_URL = "admin/admixture_category";
   const ADMIN_EDIT_ADMIXTURE_TYPES_URL = "admin/admixture_category/%s";

   const ADMIN_GET_PAYMENT_METHOD_URL = "admin/pay_method";
   const ADMIN_ADD_PAYMENT_METHOD_URL = "admin/pay_method";
   const ADMIN_EDIT_PAYMENT_METHOD_URL = "admin/pay_method/%s";

   //Grade Cat Brand......................................

   // Coupons APis......

   const ADMIN_GET_COUPONS_URL = "admin/coupon";
   const ADMIN_ADD_COUPONS_URL = "admin/coupon";
   const ADMIN_EDIT_COUPONS_URL = "admin/coupon/%s";
   const ADMIN_DELETE_COUPONS_URL = "admin/coupon/%s";

   //Conmix...............

   //Admin Panel APi OVER.................................................
   //Supplier Panel APi Start.................................................

   //Login APis
   const SUPPLIER_LOGIN_URL = "vendor/auth/%s";
   const SUPPLIER_SIGNUP_OTP_SEND_VIA_MOBILE_URL = "vendor/auth/mobiles/%s";
   const SUPPLIER_SIGNUP_OTP_SEND_VIA_EMAIL_URL = "vendor/auth/email/%s";
   const SUPPLIER_SIGNUP_OTP_VERIFY_URL = "vendor/auth/mobiles/%s/code/%s";
   const SUPPLIER_SIGNUP_REGISTRATION_URL = "vendor/users?signup_type=%s";
   const SUPPLIER_SIGNUP_EMAIL_VERIFY_URL = "vendor/auth/email/%s";

   const SUPPLIER_FORGOT_PASS_OTP_SEND_VIA_MOBILE_URL = "vendor/auth/users/%s";
   const SUPPLIER_FORGOT_PASS_OTP_SEND_VIA_EMAIL_URL = "vendor/auth/users/%s";
   const SUPPLIER_FORGOT_PASS_OTP_VERIFY_URL = "vendor/auth/users/%s/codes/%s";

   //Product Moduel APi Start.................................................

   const SUPPLIER_PRODUCT_LISTING_URL = "vendor/product";
   const SUPPLIER_PRODUCT_ADD_URL = "vendor/product";
   const SUPPLIER_PRODUCT_EDIT_URL = "vendor/product/%s";
   const SUPPLIER_PRODUCT_QUANTITY_EDIT_URL = "vendor/product/quantity/%s";

   //Product Moduel APi OVER.................................................

   //Contact Detials Moduel APi Start.................................................
   const SUPPLIER_CONTACT_DETAILS_LISTING_URL = "vendor/contact-person";
   const SUPPLIER_CONTACT_DETAILS_ADD_URL = "vendor/contact-person";
   const SUPPLIER_CONTACT_DETAILS_EDIT_URL = "vendor/contact-person/%s";
   //Contact Detials Moduel APi OVER.................................................


   //Bnak Detials Moduel APi Start.................................................

   const SUPPLIER_BANK_DETAILS_LISTING_URL = "vendor/bank";
   const SUPPLIER_BANK_DETAILS_ADD_URL = "vendor/bank";
   const SUPPLIER_BANK_DETAILS_EDIT_URL = "vendor/bank/%s";

   //Bnak Detials Moduel APi OVER.................................................

   //Address Moduel APi Start.................................................

   const SUPPLIER_ADDRESS_LISTING_URL = "vendor/address";
   const SUPPLIER_ADDRESS_ADD_URL = "vendor/address";
   const SUPPLIER_ADDRESS_EDIT_URL = "vendor/address/%s";

   const SUPPLIER_BILLING_ADDRESS_LISTING_URL = "vendor/billing_address";
   const SUPPLIER_BILLING_ADDRESS_ADD_URL = "vendor/billing_address";
   const SUPPLIER_BILLING_ADDRESS_EDIT_URL = "vendor/billing_address/%s";

   const SUPPLIER_GET_REGION_SERV_URL = "vendor/source/region";
   const SUPPLIER_GET_SOURCE_BY_REGION_SERV_URL = "vendor/source";

   //Address Moduel APi OVER.................................................

   // Support Ticket Apis
   const SUPPLIER_SUPPORT_TICKET_LISTING_URL = "vendor/support_ticket";
   const SUPPLIER_SUPPORT_TICKET_ADD_URL = "vendor/support_ticket";
   const SUPPLIER_SUPPORT_TICKET_DETAIL_URL = "vendor/support_ticket/%s";
   const SUPPLIER_SUPPORT_TICKET_REPLY_URL = "vendor/support_ticket/reply";
   const SUPPLIER_SUPPORT_TICKET_STATUS_UPDATE_URL = "vendor/support_ticket/%s";
   const SUPPLIER_SUPPORT_TICKET_MESSAGE_URL = "vendor/support_ticket/reply/%s";

   //Profile APis
   const SUPPLIER_PROFILE_DETAIL_URL = "vendor/users/profile";
   const SUPPLIER_PROFILE_UPDATE_URL = "vendor/users/update";
   const SUPPLIER_CHANGE_PASSWORD_URL = "vendor/users/changePassword";
   const SUPPLIER_NOTIFICATION_STATUS_UPDATE_URL = "vendor/notification/update_status/%s";
   const SUPPLIER_FORGOT_PASSWORD_URL = "vendor/users/forgotPassword";
   const SUPPLIER_REQUEST_OTP_URL = "vendor/users/requestOTP";
   const SUPPLIER_REQUEST_OTP_TO_MASTER_URL = "vendor/users/requestOTPForMaster";
   const SUPPLIER_REQUEST_OTP_TO_PLANT_URL = "vendor/address/requestOTPForAddress";
   const SUPPLIER_REQUEST_OTP_FOR_EDIT_PROFILE_URL = "vendor/users/requestOTPForUpdateProfile";

   //Orders APis
   const SUPPLIER_ORDER_LISTING_URL = "vendor/order";
   const SUPPLIER_SURPRISE_ORDER_LISTING_URL = "vendor/surprise_order";
   const SUPPLIER_ORDER_DETAILS_URL = "vendor/order/%s";
   const SUPPLIER_SURPRISE_ORDER_DETAILS_URL = "vendor/surprise_order/%s";
   const SUPPLIER_ORDER_TRUCK_ASSIGN = "vendor/order_track/%s/%s";
   const SUPPLIER_ORDER_CP_ASSIGN = "vendor/order_track/CP/%s/%s";
   const SUPPLIER_ORDER_BILL_UPLOAD_URL = "vendor/order/%s/uploadBill";
   const BUYER_ORDER_BILL_UPLOAD_URL = "buyer/order_track/%s/%s/%s/uploadDoc";
   const SUPPLIER_ORDER_REASSIGNING_URL = "vendor/order/reassigning";
   const SUPPLIER_ORDER_TRACKING_URL = "vendor/order_track/%s/%s";
   const SUPPLIER_GET_ASSIGNED_ORDER_BY_DATE_RANGE_URL = "vendor/assigned_order_by_date_range";
   const SUPPLIER_ORDER_CP_TRACKING_URL = "vendor/order_track/CP/track/%s";
   const SUPPLIER_ORDER_GET_DELIVERED_CP_URL = "vendor/order_track/get_CP_delivered/%s/%s";
   const SUPPLIER_ORDER_ACCEPTS = "vendor/order/%s";
   const SUPPLIER_SURPRISE_ORDER_ACCEPTS = "vendor/surprise_order/%s";
   const SUPPLIER_ORDER_7_DAY_REPORT = "vendor/order_track/%s/%s/%s/uploadDoc";
   const SUPPLIER_ORDER_28_DAY_REPORT = "vendor/order_track/%s/%s/%s/uploadDoc";

   //Proposal APIS
   const SUPPLIER_PROPOSAL_LISTING_URL = "vendor/rfp";
   const SUPPLIER_PROPOSAL_ADD_URL = "vendor/rfp";


   //Dashboard APIs
   const SUPPLIER_DASHBOARD_DETAILS_URL = "vendor/dashboard";
   const SUPPLIER_MONTHLY_CHART_URL = "vendor/dashboard/monthchart?monthly_chart=yes";
   const SUPPLIER_DAILY_CHART_URL = "vendor/dashboard/daychart?daily_chart=yes";

   const SUPPLIER_PRODUCT_BASE_MONTHLY_CHART_URL = "vendor/dashboard/designMixBasedmonthchart?monthly_chart=yes";
   const SUPPLIER_PRODUCT_BASE_DAILY_CHART_URL = "vendor/dashboard/designMixBasedDaychart?daily_chart=yes";
   const SUPPLIER_LATEST_ORDERS_URL = "vendor/dashboard/latestVendorOrder";
   const SUPPLIER_QUEUE_ORDERS_URL = "vendor/dashboard/inQueueVendorOrder";
   const SUPPLIER_LATEST_PRODUCTS_URL = "vendor/dashboard/latestDesignMix";
//    const SUPPLIER_PRODUCT_STOCK_URL = "vendor/dashboard/productStock";
   const SUPPLIER_PRODUCT_STOCK_URL = "vendor/dashboard/checkProductStock";
   const SUPPLIER_DAY_CAPACITY_URL = "vendor/dashboard/dayCapacity?delivery_date=%s";

   //Report APIs
   const SUPPLIER_PLANT_ADDRESS_LISTING_REPORT_URL = "vendor/report/address";
   const SUPPLIER_BILLING_ADDRESS_LISTING_REPORT_URL = "vendor/report/billing_address";
   const SUPPLIER_TM_DRIVER_LISTING_REPORT_URL = "vendor/report/driver";
   const SUPPLIER_OPERATOR_LISTING_REPORT_URL = "vendor/report/operator";
   const SUPPLIER_TM_LISTING_REPORT_URL = "vendor/report/TM";
   const SUPPLIER_CP_LISTING_REPORT_URL = "vendor/report/CP";
   const SUPPLIER_ADMIX_STOCK_REPORT_URL = "vendor/report/admix_stock";
   const SUPPLIER_AGGREGATE_STOCK_REPORT_URL = "vendor/report/agg_stock";
   const SUPPLIER_CEMENT_STOCK_REPORT_URL = "vendor/report/cement_stock";
   const SUPPLIER_FLYASH_STOCK_REPORT_URL = "vendor/report/flyAsh_stock";
   const SUPPLIER_SAND_STOCK_REPORT_URL = "vendor/report/sand_stock";
   const SUPPLIER_BANK_REPORT_URL = "vendor/report/bank";
   const SUPPLIER_DESIGN_MIX_REPORT_URL = "vendor/report/design_mix_variant";
   const SUPPLIER_ORDER_LISTING_REPORT_URL = "vendor/report/order";
   const SUPPLIER_PRODUCT_LISTING_REPORT_URL = "vendor/report/productList";


   //Driver API
   const SUPPLIER_DRIVER_LISTING_URL = "vendor/driver";
   const SUPPLIER_DRIVER_DETAILS_ADD_URL = "vendor/driver";
   const SUPPLIER_DRIVER_DETAILS_EDIT_URL = "vendor/driver/%s";

   //Operator API
   const SUPPLIER_OPERATOR_LISTING_URL = "vendor/operator";
   const SUPPLIER_OPERATOR_DETAILS_ADD_URL = "vendor/operator";
   const SUPPLIER_OPERATOR_DETAILS_EDIT_URL = "vendor/operator/%s";
   
   //Pump Gang API
   const SUPPLIER_PUMP_GANG_LISTING_URL = "vendor/pump_gang";
   const SUPPLIER_PUMP_GANG_DETAILS_ADD_URL = "vendor/pump_gang";
   const SUPPLIER_PUMP_GANG_DETAILS_EDIT_URL = "vendor/pump_gang/%s";


   //TM APis
   const SUPPLIER_TM_LISTING_URL = "vendor/TM";
   const SUPPLIER_CATEGORY_LISTING_URL = "vendor/TM/categories";
   const SUPPLIER_SUB_CATEGORY_LISTING_URL = "vendor/TM/subCategory";
   const SUPPLIER_TM_ADD_URL = "vendor/TM/%s/%s";
   const SUPPLIER_TM_EDIT_URL = "vendor/TM/%s";
   const SUPPLIER_TM_DETAILS_BY_ID_URL = "vendor/TM/%s";

   //Concrete pump API
   const SUPPLIER_CONCRETE_PUMP_LISTING_URL = "vendor/concrete_pump";
   const SUPPLIER_CONCRETE_PUMP_DETAILS_ADD_URL = "vendor/concrete_pump/%s";
   const SUPPLIER_CONCRETE_PUMP_DETAILS_EDIT_URL = "vendor/concrete_pump/%s";


   //Design Mix API
   const SUPPLIER_SUBVENDOR_LISTING_URL = "vendor/users/subvendorlisting";
   const SUPPLIER_GET_FILL_DETAILS_URL = "vendor/get_fill_details";
   const SUPPLIER_DESIGN_MIX_LISTING_URL = "vendor/design_mix";
   const SUPPLIER_GET_DESIGN_MIX_GRADE_WISE_URL = "vendor/design_mix/%s";
   const SUPPLIER_GET_DESIGN_MIX_BY_ID_URL = "vendor/design_mix/variant/%s";
   const SUPPLIER_GET_DESIGN_MIX_COMBINATION_BY_ID_URL = "vendor/design_mix/variant/combination/%s";
   const SUPPLIER_GET_SINGLE_DESIGN_MIX_COMBINATION_BY_ID_URL = "vendor/design_mix/variant/single_combination/%s";
   const SUPPLIER_DESIGN_MIX_DETAILS_ADD_URL = "vendor/design_mix";
   const SUPPLIER_DESIGN_MIX_VARIENT_ADD_URL = "vendor/design_mix/variant/%s";
   const SUPPLIER_DESIGN_MIX_DETAILS_EDIT_URL = "vendor/design_mix/%s";
   const SUPPLIER_DESIGN_MIX_DETAILS_EDIT_VARIENT_URL = "vendor/design_mix/variant/%s";


   //Media API
   const SUPPLIER_MEDIA_LISTING_URL = "vendor/media";
   const SUPPLIER_PRIMARY_MEDIA_ADD_URL = "vendor/media/primary";
   const SUPPLIER_SECONDARY_MEDIA_ADD_URL = "vendor/media/secondary";
   const SUPPLIER_MEDIA_DELETE_URL = "vendor/media";

   //Cement Rate API
   const SUPPLIER_CEMENT_RATE_LISTING_URL = "vendor/cement_rate";
   const SUPPLIER_CEMENT_RATE_ADD_URL = "vendor/cement_rate";
   const SUPPLIER_CEMENT_RATE_EDIT_URL = "vendor/cement_rate/%s";
   const SUPPLIER_CEMENT_RATE_DELETE_URL = "vendor/cement_rate/%s";

   //Sand Rate API
   const SUPPLIER_SAND_RATE_LISTING_URL = "vendor/sand_source_rate";
   const SUPPLIER_SAND_RATE_ADD_URL = "vendor/sand_source_rate";
   const SUPPLIER_SAND_RATE_EDIT_URL = "vendor/sand_source_rate/%s";
   const SUPPLIER_SAND_RATE_DELETE_URL = "vendor/sand_source_rate/%s";

   //Aggregate Rate API
   const SUPPLIER_AGGREGATE_RATE_LISTING_URL = "vendor/aggregate_source_rate";
   const SUPPLIER_AGGREGATE_RATE_ADD_URL = "vendor/aggregate_source_rate";
   const SUPPLIER_AGGREGATE_RATE_EDIT_URL = "vendor/aggregate_source_rate/%s";
   const SUPPLIER_AGGREGATE_RATE_DELETE_URL = "vendor/aggregate_source_rate/%s";

   //Flyash Rate API
   const SUPPLIER_FLYASH_RATE_LISTING_URL = "vendor/fly_ash_source_rate";
   const SUPPLIER_FLYASH_RATE_ADD_URL = "vendor/fly_ash_source_rate";
   const SUPPLIER_FLYASH_RATE_EDIT_URL = "vendor/fly_ash_source_rate/%s";
   const SUPPLIER_FLYASH_RATE_DELETE_URL = "vendor/fly_ash_source_rate/%s";

   //AdMixture Rate API
   const SUPPLIER_ADMIXTURE_RATE_LISTING_URL = "vendor/admixture_rate";
   const SUPPLIER_ADMIXTURE_RATE_ADD_URL = "vendor/admixture_rate";
   const SUPPLIER_ADMIXTURE_RATE_EDIT_URL = "vendor/admixture_rate/%s";
   const SUPPLIER_ADMIXTURE_RATE_DELETE_URL = "vendor/admixture_rate/%s";

   //Water Rate API
   const SUPPLIER_WATER_RATE_LISTING_URL = "vendor/water_rate";
   const SUPPLIER_WATER_RATE_ADD_URL = "vendor/water_rate";
   const SUPPLIER_WATER_RATE_EDIT_URL = "vendor/water_rate/%s";
   const SUPPLIER_WATER_RATE_DELETE_URL = "vendor/water_rate/%s";



   //Profit Margin API
   const SUPPLIER_PROFIT_MARGIN_LISTING_URL = "vendor/profit_margin";
   const SUPPLIER_PROFIT_MARGIN_ADD_URL = "vendor/profit_margin";
   const SUPPLIER_PROFIT_MARGIN_EDIT_URL = "vendor/profit_margin/%s";
   const SUPPLIER_PROFIT_MARGIN_DELETE_URL = "vendor/profit_margin/%s";
   
   //Overhead Margin API
   const SUPPLIER_OVERHEAD_MARGIN_LISTING_URL = "vendor/overhead_margin";
   const SUPPLIER_OVERHEAD_MARGIN_ADD_URL = "vendor/overhead_margin";
   const SUPPLIER_OVERHEAD_MARGIN_EDIT_URL = "vendor/overhead_margin/%s";
   const SUPPLIER_OVERHEAD_MARGIN_DELETE_URL = "vendor/overhead_margin/%s";
   
   //Cement Stock API
   const SUPPLIER_CEMENT_STOCK_LISTING_URL = "vendor/cement_quantity";
   const SUPPLIER_CEMENT_STOCK_ADD_URL = "vendor/cement_quantity";
   const SUPPLIER_CEMENT_STOCK_EDIT_URL = "vendor/cement_quantity/%s";
   const SUPPLIER_CEMENT_STOCK_DELETE_URL = "vendor/cement_quantity/%s";

   //Sand Stock API
   const SUPPLIER_SAND_STOCK_LISTING_URL = "vendor/sand_source_quantity";
   const SUPPLIER_SAND_STOCK_ADD_URL = "vendor/sand_source_quantity";
   const SUPPLIER_SAND_STOCK_EDIT_URL = "vendor/sand_source_quantity/%s";
   const SUPPLIER_SAND_STOCK_DELETE_URL = "vendor/sand_source_quantity/%s";

   //Aggregate Stock API
   const SUPPLIER_AGGREGATE_STOCK_LISTING_URL = "vendor/aggregate_source_quantity";
   const SUPPLIER_AGGREGATE_STOCK_ADD_URL = "vendor/aggregate_source_quantity";
   const SUPPLIER_AGGREGATE_STOCK_EDIT_URL = "vendor/aggregate_source_quantity/%s";
   const SUPPLIER_AGGREGATE_STOCK_DELETE_URL = "vendor/aggregate_source_quantity/%s";

   //Flyash Stock API
   const SUPPLIER_FLYASH_STOCK_LISTING_URL = "vendor/fly_ash_source_quantity";
   const SUPPLIER_FLYASH_STOCK_ADD_URL = "vendor/fly_ash_source_quantity";
   const SUPPLIER_FLYASH_STOCK_EDIT_URL = "vendor/fly_ash_source_quantity/%s";
   const SUPPLIER_FLYASH_STOCK_DELETE_URL = "vendor/fly_ash_source_quantity/%s";

   //AdMixture Stock API
   const SUPPLIER_ADMIXTURE_STOCK_LISTING_URL = "vendor/admixture_quantity";
   const SUPPLIER_ADMIXTURE_STOCK_ADD_URL = "vendor/admixture_quantity";
   const SUPPLIER_ADMIXTURE_STOCK_EDIT_URL = "vendor/admixture_quantity/%s";
   const SUPPLIER_ADMIXTURE_STOCK_DELETE_URL = "vendor/admixture_quantity/%s";

   //Settings API
   const SUPPLIER_SETTING_ADD_URL = "vendor/setting";
   const SUPPLIER_SETTING_GET_URL = "vendor/setting";
   const SUPPLIER_SETTING_UPDATE_URL = "vendor/setting";
   const SUPPLIER_ADMIX_SETTING_GET_URL = "vendor/ad_mix_setting";
   const SUPPLIER_ADMIX_SETTING_ADD_URL = "vendor/ad_mix_setting";
   const SUPPLIER_ADMIX_SETTING_UPDATE_URL = "vendor/ad_mix_setting/%s";

   //TM Un Availability
   const SUPPLIER_TM_UNAVAILABILITY_GET_URL = "vendor/TM_unavailbilty/%s";
   const SUPPLIER_TM_UNAVAILABILITY_ADD_URL = "vendor/TM_unavailbilty/%s";
   const SUPPLIER_TM_UNAVAILABILITY_EDIT_URL = "vendor/TM_unavailbilty/%s";
   const SUPPLIER_TM_UNAVAILABILITY_DELETE_URL = "vendor/TM_unavailbilty/%s";

   //CP Un Availability
   const SUPPLIER_CP_UNAVAILABILITY_GET_URL = "vendor/CP_unavailbilty/%s";
   const SUPPLIER_CP_UNAVAILABILITY_ADD_URL = "vendor/CP_unavailbilty/%s";
   const SUPPLIER_CP_UNAVAILABILITY_EDIT_URL = "vendor/CP_unavailbilty/%s";
   const SUPPLIER_CP_UNAVAILABILITY_DELETE_URL = "vendor/CP_unavailbilty/%s";


   //Common API
   const SUPPLIER_CONCRETE_PUMP_CATEGORY_LISTING_URL = "concrete_pump/categories";

   const COMMON_CEMENT_BRAND_LISTING_URL = "cement_brand";
   const COMMON_SAND_SOURCE_LISTING_URL = "source/sand";
   const COMMON_SAND_ZONE_LISTING_URL = "source/zone";
   const COMMON_AGGREGATE_LISTING_URL = "source/aggregate";
   const COMMON_AGGREGATE_SAND_CATEGORY_LISTING_URL = "aggregate-sand-category";
   const COMMON_AGGREGATE_SAND_SUB_CATEGORY_LISTING_URL = "aggregate-sand-category/subcategory/%s";
   const COMMON_GET_AGGREGATE_SAND_SUB_CATEGORY_FOR_CUSTOM_MIX_URL = "aggregate-sand-category/check/%s";
   const COMMON_FLY_ASH_SOURCE_LISTING_URL = "source/fly_ash";
   const COMMON_ADMIXTURE_BRNAD_LISTING_URL = "admixture_brand";
   const COMMON_CONCRETE_GRADE_LISTING_URL = "concrete_grade";
   const COMMON_ADMIXTURE_TYPES_LISTING_URL = "admixture_category";
   const COMMON_CEMENT_GRADE_LISTING_URL = "cement_grade";

   const COMMON_TM_CATEGORY_LISTING_URL = "TM/categories";
   // const COMMON_TM_SUB_CATEGORY_LISTING_URL = "TM/subCategory/%s";
   const COMMON_TM_SUB_CATEGORY_LISTING_URL = "TM/subCategory";


   //Supplier Panel APi OVER.................................................

   //Logistics Panel APi Start.................................................

   //Login APis
   const LOGISTICS_LOGIN_URL = "logistics/auth/%s";
   const LOGISTICS_SIGNUP_OTP_SEND_VIA_MOBILE_URL = "logistics/auth/mobiles/%s";
   const LOGISTICS_SIGNUP_OTP_SEND_VIA_EMAIL_URL = "logistics/auth/email/%s";
   const LOGISTICS_SIGNUP_OTP_VERIFY_URL = "logistics/auth/mobiles/%s/code/%s";
   const LOGISTICS_SIGNUP_REGISTRATION_URL = "logistics/users?signup_type=%s";
   const LOGISTICS_SIGNUP_REGISTRATION_ADDRESS_URL = "logistics/users/addAddress";

   const LOGISTICS_FORGOT_PASS_OTP_SEND_VIA_MOBILE_URL = "logistics/auth/users/%s";
   const LOGISTICS_FORGOT_PASS_OTP_SEND_VIA_EMAIL_URL = "logistics/auth/users/%s";
   const LOGISTICS_FORGOT_PASS_OTP_VERIFY_URL = "logistics/auth/users/%s/codes/%s";

   //Quote APis
   const LOGISTICS_QUOTE_LISTING_URL = "logistics/quote";
   const LOGISTICS_QUOTE_UPDATE_URL = "logistics/quote/%s";

   //Vehicles APis
   const LOGISTICS_VEHICLES_LISTING_URL = "logistics/vehicle";
   const LOGISTICS_CATEGORY_LISTING_URL = "logistics/vehicle/categories";
   const LOGISTICS_SUB_CATEGORY_LISTING_URL = "logistics/vehicle/subCategory";
   const LOGISTICS_VEHICLE_ADD_URL = "logistics/vehicle/%s/%s";
   const LOGISTICS_VEHICLE_EDIT_URL = "logistics/vehicle/%s";
   const LOGISTICS_VEHICLE_DETAILS_BY_ID_URL = "logistics/vehicle/%s";

   //Bank APis
   const LOGISTICS_BANK_LISTING_URL = "logistics/bank";
   const LOGISTICS_BANK_DETAILS_ADD_URL = "logistics/bank";
   const LOGISTICS_BANK_DETAILS_EDIT_URL = "logistics/bank/%s";

   //Driver API
   const LOGISTICS_DRIVER_LISTING_URL = "logistics/driver";
   const LOGISTICS_DRIVER_DETAILS_ADD_URL = "logistics/driver";
   const LOGISTICS_DRIVER_DETAILS_EDIT_URL = "logistics/driver/%s";


   const LOGISTICS_SUPPORT_TICKET_LISTING_URL = "";
   const LOGISTICS_SUPPORT_TICKET_ADD_URL = "";
   const LOGISTICS_SUPPORT_TICKET_DETAIL_URL = "";
   const LOGISTICS_SUPPORT_TICKET_REPLY_URL = "";

   //Profile API
   const LOGISTICS_PROFILE_DETAIL_URL = "logistics/users/profile";
   const LOGISTICS_PROFILE_UPDATE_URL = "logistics/users";

   //Dashboard
   const LOGISTICS_DASHBOARD_DETAILS_URL = "logistics/dashboard";
   const LOGISTICS_DASHBOARD_QUOTES_URL = "logistics/dashboard/quoteList";
   const LOGISTICS_DASHBOARD_LATEST_ORDERS_URL = "logistics/dashboard/latestProcessingLogiscticsOrder";
   const LOGISTICS_MONTHLY_CHART_URL = "logistics/dashboard/monthchart?monthly_chart=yes";
   const LOGISTICS_DAILY_CHART_URL = "logistics/dashboard/daychart?daily_chart=yes";

   const LOGISTICS_ORDER_LISTING = "logistics/order";
   const LOGISTICS_ORDER_DETAILS = "logistics/order/%s";
   const LOGISTICS_ORDER_TRUCK_ASSIGN = "logistics/order_track/%s/%s";
   const LOGISTICS_ORDER_BILL_UPLOAD_URL = "logistics/order/%s/uploadBill";
   const LOGISTICS_ORDER_TRACKING_URL = "logistics/order_track/%s/%s";

   //Reports
   const LOGISTICS_ORDER_LISTING_REPORT_URL = "logistics/report/order";


   //Logistics Panel APi OVER.................................................

   //Buyer Panel APi Start.................................................

   //Login APis
   const BUYER_LOGIN_URL = "buyer/auth/%s";
   const BUYER_SIGNUP_OTP_SEND_VIA_MOBILE_URL = "buyer/auth/mobiles/%s";
   const BUYER_SIGNUP_OTP_SEND_VIA_EMAIL_URL = "buyer/auth/email/%s";
   const BUYER_SIGNUP_OTP_VERIFY_URL = "buyer/auth/mobiles/%s/code/%s";
   const BUYER_SIGNUP_REGISTRATION_URL = "buyer/users?signup_type=%s";

   const BUYER_FORGOT_PASS_OTP_SEND_VIA_MOBILE_URL = "buyer/auth/users/%s";
   const BUYER_FORGOT_PASS_OTP_SEND_VIA_EMAIL_URL = "buyer/auth/users/%s";
   const BUYER_FORGOT_PASS_OTP_VERIFY_URL = "buyer/auth/users/%s/codes/%s";
   const BUYER_FORGOT_PASS_URL = "buyer/users/forgotPassword";

   //Products APis
   // const BUYER_PRODUCT_LISTING_URL = "buyer/product";
   const BUYER_SUPPLIER_CHECK_URL = "design_mix/check_supplier_availability?lat=%s&long=%s&state_id=%s";
   const BUYER_PRODUCT_LISTING_URL = "design_mix?grade_id=%s&lat=%s&long=%s&state_id=%s";
   const BUYER_PRODUCT_WITH_SOURCE_LISTING_URL = "product/category/%s?lat=%s&long=%s&state_id=%s&source_id=%s";
   const BUYER_PRODUCT_WITH_SUBCAT_LISTING_URL = "product/category/%s?lat=%s&long=%s&state_id=%s&sub_category_id=%s";
   const BUYER_PRODUCT_WITH_SUBCAT_SOURCE_LISTING_URL = "product/category/%s?lat=%s&long=%s&state_id=%s&sub_category_id=%s&source_id=%s";
   const BUYER_PRODUCT_DETAILS_URL = "design_mix/%s?lat=%s&long=%s&state_id=%s&delivery_date=%s";
   const BUYER_SINGLE_PRODUCT_COMBINATION_DETAILS_URL = "design_mix/single_combination/%s?lat=%s&long=%s&state_id=%s&delivery_date=%s";
   const BUYER_VENDOR_REVIEW_URL = "review/getVendorReview/%s";
   const BUYER_VENDOR_MATERIAL_DETAIL_URL = "design_mix/varient/vendor_rate_details/%s";
   const BUYER_PRODUCT_REVIEW_URL = "product-review/%s";
   const BUYER_PRODUCT_SOURCES_URL = "source";
   const ADD_TO_CART_URL = "buyer/cart";

   // const GET_CART_BY_ID_URL = "buyer/cart/%s";
   const GET_CART_URL = "buyer/cart?with_TM=%s&with_CP=%s&delivery_date=%s";
   // const GET_CART_BY_ID_URL = "buyer/cart/%s?with_TM=%s&with_CP=%s&delivery_date=%s";
   const GET_CART_BY_ID_URL = "buyer/cart/%s";

   const ADD_SITE_TO_CART_URL = "buyer/cart/address";
   const ADD_BILLING_ADD_TO_CART_URL = "buyer/cart/billing_address";
   // const PLACE_ORDER_URL = "buyer/order/%s?with_TM=%s&with_CP=%s&delivery_date=%s";
   const PLACE_ORDER_URL = "buyer/order/%s";

   const PLACE_ORDER_PROCESS = "cron/order/process?order_id=%s";
   const PLACE_QUOTE_PROCESS = "cron/quote/process";

   const GET_WISH_LIST_URL = "buyer/wishlist";
   const ADD_TO_WISH_LIST_URL = "buyer/wishlist";
   const DELETE_WISH_LIST_ITEM_URL = "buyer/wishlist/%s";

   const DELETE_CART_ITEM_URL = "buyer/cart/item";

   const BUYER_ORDERS_LIST_URL = "buyer/order";
   const BUYER_ORDERS_DETAILS_URL = "buyer/order/%s";

   const ORDER_CANCEL_URL = "buyer/order/%s/cancel";
   const ORDER_ITEM_CANCEL_URL = "buyer/order/%s/cancel/%s";
   const ORDER_COMPLAIN_URL = "buyer/complaint";
   const ORDER_FEEDBACK_URL = "buyer/feedback";
   const ORDER_ASSIGN_QTY_URL = "buyer/order/%s/%s";
   const ORDER_REASSIGN_STATUS_CHANGE_URL = "buyer/order/reassign_status_change";
   const ORDER_CHECK_TM_AVAILABILITY_URL = "buyer/order/CheckTMAvailbility";
   // const ORDER_PRODUCT_REVIEW_URL = "buyer/product-review";
   const ORDER_PRODUCT_REVIEW_URL = "buyer/review";
   const ORDER_PRODUCT_REVIEW_UPDATE_URL = "buyer/review/%s";
   // const ORDER_PRODUCT_TRACK_URL = "buyer/order_track?item_id=%s";
   const ORDER_PRODUCT_TRACK_URL = "buyer/order_track/%s";
   const ORDER_TRACK_DETAILS_URL = "buyer/order_track/%s/%s";
   const ORDER_TRACK_CP_DETAILS_URL = "buyer/order_track/CP/%s";

   //Profile APis
   const BUYER_PROFILE_DETAIL_URL = "buyer/users/profile";
   const BUYER_PROFILE_EDIT_URL = "buyer/users";
   const BUYER_PROFILE_SITE_LISTING_URL = "buyer/site";
   const BUYER_PROFILE_ADD_SITE_URL = "buyer/site";
   const BUYER_PROFILE_EDIT_SITE_URL = "buyer/site/%s";
   const BUYER_PROFILE_DELETE_SITE_URL = "buyer/site/%s";

   const BUYER_BILLING_ADDRESS_LISTING_URL = "buyer/billing_address";
   const BUYER_BILLING_ADDRESS_ADD_URL = "buyer/billing_address";
   const BUYER_BILLING_ADDRESS_EDIT_URL = "buyer/billing_address/%s";
   const BUYER_BILLING_ADDRESS_DELETE_URL = "buyer/billing_address/%s";
   
   const BUYER_CUSTOM_RMC_LISTING_URL = "buyer/custom_rmc";
   const BUYER_CUSTOM_RMC_ADD_URL = "buyer/custom_rmc";
   const BUYER_CUSTOM_RMC_EDIT_URL = "buyer/custom_rmc/%s";
   const BUYER_CUSTOM_RMC_DELETE_URL = "buyer/custom_rmc/%s";

   const BUYER_PROFILE_CHECK_SITE_EXIST_URL = "buyer/site/check";
   const BUYER_CHANGE_PASS_URL = "buyer/users/changePassword";
   const BUYER_REQUEST_OTP_URL = "buyer/users/requestOTP";
   const BUYER_REQUEST_OTP_FOR_EDIT_PROFILE_URL = "buyer/users/requestOTPForUpdateProfile";

   const BUYER_GET_COUPON_URL = "buyer/cart/coupon";
   const BUYER_GET_APPLY_URL = "buyer/cart/coupon";


   //Custom Mix APIs
   const BUYER_CUSTOM_MIX_DESIGN_URL = "custom_mix";
   const BUYER_CUSTOM_MIX_DESIGN_DETAIL_URL = "custom_mix/details/%s";

   //SUpport Ticket
   const BUYER_SUPPORT_TICKET_LISTING_URL = "buyer/support_ticket";
   const BUYER_SUPPORT_TICKET_ADD_URL = "buyer/support_ticket";
   const BUYER_SUPPORT_TICKET_DETAIL_URL = "buyer/support_ticket/%s";
   const BUYER_SUPPORT_TICKET_REPLY_URL = "buyer/support_ticket/reply";
   const BUYER_SUPPORT_TICKET_MESSAGE_URL = "buyer/support_ticket/reply/%s";

   //Buyer Panel APi OVER.................................................

   //Common APi Start.................................................

   const GET_PRODUCT_CATEGORY_URL = "product/category";
   const GET_PRODUCT_SUB_CATEGORY_URL = "product/sub_category/%s";
   const GET_COUNTRY_URL = "region/getCountries";
   const GET_STATE_URL = "region/getStates";
   const GET_CITY_URL = "region/getCities";
   const GET_STATE_BY_COUNRTY_URL = "region/getStatesByCountryId/%s";
   const GET_CITY_BY_STATE_URL = "region/getCitiesByStateId/%s";

   const GET_REGIONAL_DETAILS_URL = "region";

   const GET_NOTIFICATIONS_URL = "buyer/notification/all";
   const GET_NOTIFICATIONS_COUNT_URL = "buyer/notification/count";

   const GET_SUPPLIER_NOTIFICATIONS_URL = "vendor/notification/all";
   const GET_SUPPLIER_NOTIFICATIONS_COUNT_URL = "vendor/notification/count";

   const GET_ADMIN_NOTIFICATIONS_URL = "admin/notification/all";
   const GET_ADMIN_NOTIFICATIONS_COUNT_URL = "admin/notification/count";
   const ADMIN_NOTIFICATION_STATUS_UPDATE_URL = "admin/notification/update_status/%s";

   const GET_CUSTOM_MIX_OPTIONS_URL = "custom_mix/options";
   const GET_PAYMENT_METHOD_URL = "pay_method";

   //Common APi OVER.................................................

   const ADMIN_RIGHTS_TRUE = array(

      "dashboard_view" => true, 
      "users_view" => true,
      "admin_user_view" => true,
      "admin_user_action" => true, //admin_manager
      "admin_user_add" => true, //admin_manager
      "admin_user_edit" => true, //admin_manager
      "admin_user_delete" => true, //admin_manager
      "admin_user_active_inactive" => true, //admin_manager
      "buyers_view" => true,
      "suppliers_view" => true,
      "suppliers_actions" => true,
      "suppliers_verification" => true,
      "suppliers_rejection" => true,
      "suppliers_unblock" => true,
      "suppliers_block" => true,
      "region_view" => true,
      "country_view" => true,
      "state_view" => true,
      "state_add" => true,
      "state_edit" => true,
      "state_delete" => true,
      "city_view" => true,
      "city_add" => true,
      "city_edit" => true,
      "city_delete" => true,
      "grade_cat_brands_view" => true,
      "concrete_grade_view" => true,
      "concrete_grade_add" => true,
      "concrete_grade_edit" => true,
      "cement_brand_view" => true,
      "cement_brand_add" => true,
      "cement_brand_edit" => true,
      "cement_grade_view" => true,
      "cement_grade_add" => true,
      "cement_grade_edit" => true,
      "admixture_brand_view" => true,
      "admixture_brand_add" => true,
      "admixture_brand_edit" => true,
      "admixture_types_view" => true,
      "admixture_types_add" => true,
      "admixture_types_edit" => true,
      "aggregate_sand_view" => true,
      "aggregate_sand_add" => true,
      "aggregate_sand_edit" => true,
      "aggregate_sand_sub_view" => true,
      "aggregate_sand_sub_add" => true,
      "aggregate_sand_sub_edit" => true,
      "aggregate_source_view" => true,
      "aggregate_source_add" => true,
      "aggregate_source_edit" => true,
      "aggregate_source_delete" => true,
      "sand_source_view" => true,
      "sand_source_add" => true,
      "sand_source_edit" => true,
      "sand_source_delete" => true,
      "flyash_source_view" => true,
      "flyash_source_add" => true,
      "flyash_source_edit" => true,
      "flyash_source_delete" => true,
      "margin_slab_view" => true,
      "margin_slab_add" => true,
      "margin_slab_edit" => true,
      "gst_slab_view" => true,
      "gst_slab_add" => true,
      "gst_slab_edit" => true,
      "TM_CP_view" => true,
      "TM_category_view" => true,
      "TM_category_add" => true,
      "TM_category_edit" => true,
      "TM_sub_category_view" => true,
      "TM_sub_category_add" => true,
      "TM_sub_category_edit" => true,
      "CP_category_view" => true,
      "CP_category_add" => true,
      "CP_category_edit" => true,
      "ledger_view" => true,
      "buyer_order_view" => true,
      "supplier_order_view" => true,
      "supplier_order_action" => true,
      "customer_support_view" => true,
      "customer_support_add" => true,
      "customer_support_reply" => true,
      "customer_support_resolve" => true,
      "review_feedback_view" => true,
      "review_view" => true,
      "review_edit" => true,
      "feedback_view" => true,
      "report_view" => true,
      "inventory_report_view" => true,
      "sales_report_view" => true,
      "profile_view" => true,
      "profile_edit" => true, //admin_manager
      "profile_change_password" => true, //admin_manager


   );
   
   const ADMIN_RIGHTS_FALSE = array(

      "dashboard_view" => false, 
      "users_view" => false,
      "admin_user_view" => false,
      "admin_user_action" => false, //admin_manager
      "admin_user_add" => false, //admin_manager
      "admin_user_edit" => false, //admin_manager
      "admin_user_delete" => false, //admin_manager
      "admin_user_active_inactive" => false, //admin_manager
      "buyers_view" => false,
      "suppliers_view" => false,
      "suppliers_actions" => false,
      "suppliers_verification" => false,
      "suppliers_rejection" => false,
      "suppliers_unblock" => false,
      "suppliers_block" => false,
      "region_view" => false,
      "country_view" => false,
      "state_view" => false,
      "state_add" => false,
      "state_edit" => false,
      "state_delete" => false,
      "city_view" => false,
      "city_add" => false,
      "city_edit" => false,
      "city_delete" => false,
      "grade_cat_brands_view" => false,
      "concrete_grade_view" => false,
      "concrete_grade_add" => false,
      "concrete_grade_edit" => false,
      "cement_brand_view" => false,
      "cement_brand_add" => false,
      "cement_brand_edit" => false,
      "cement_grade_view" => false,
      "cement_grade_add" => false,
      "cement_grade_edit" => false,
      "admixture_brand_view" => false,
      "admixture_brand_add" => false,
      "admixture_brand_edit" => false,
      "admixture_types_view" => false,
      "admixture_types_add" => false,
      "admixture_types_edit" => false,
      "aggregate_sand_view" => false,
      "aggregate_sand_add" => false,
      "aggregate_sand_edit" => false,
      "aggregate_sand_sub_view" => false,
      "aggregate_sand_sub_add" => false,
      "aggregate_sand_sub_edit" => false,
      "aggregate_source_view" => false,
      "aggregate_source_add" => false,
      "aggregate_source_edit" => false,
      "aggregate_source_delete" => false,
      "sand_source_view" => false,
      "sand_source_add" => false,
      "sand_source_edit" => false,
      "sand_source_delete" => false,
      "flyash_source_view" => false,
      "flyash_source_add" => false,
      "flyash_source_edit" => false,
      "flyash_source_delete" => false,
      "margin_slab_view" => false,
      "margin_slab_add" => false,
      "margin_slab_edit" => false,
      "gst_slab_view" => false,
      "gst_slab_add" => false,
      "gst_slab_edit" => false,
      "TM_CP_view" => false,
      "TM_category_view" => false,
      "TM_category_add" => false,
      "TM_category_edit" => false,
      "TM_sub_category_view" => false,
      "TM_sub_category_add" => false,
      "TM_sub_category_edit" => false,
      "CP_category_view" => false,
      "CP_category_add" => false,
      "CP_category_edit" => false,
      "ledger_view" => false,
      "buyer_order_view" => false,
      "supplier_order_view" => false,
      "supplier_order_action" => false,
      "customer_support_view" => false,
      "customer_support_add" => false,
      "customer_support_reply" => false,
      "customer_support_resolve" => false,
      "review_feedback_view" => false,
      "review_view" => false,
      "review_edit" => false,
      "feedback_view" => false,
      "report_view" => false,
      "inventory_report_view" => false,
      "sales_report_view" => false,
      "profile_view" => false,
      "profile_edit" => false, //admin_manager
      "profile_change_password" => false, //admin_manager


   );
}
