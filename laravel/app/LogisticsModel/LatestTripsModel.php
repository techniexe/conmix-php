<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class LatestTripsModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showOrders(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        
        if(Request::get("delivery_status")){
            $data["delivery_status"] = Request::get("delivery_status");
        }

        // dd($data);
        $response = $this->api_model->showOrders($data);
        // dd($response);
        return $response;


    }
    
    public function showOrdersDetails($order_id){

        $data = array(
            "order_id" => $order_id
        );


        // dd($data);
        $response = $this->api_model->showOrdersDetails($data);
        // dd($response);
        return $response;


    }
    
    public function truckAssign(){

        $data = array(
            "vehicle_id" => Request::get("vehicle_id"),
            "pickup_quantity" => Request::get("delivered_quantity"),
            "assigned_at" => date("c", strtotime(Request::get("assigned_at")." 12:00 AM")),
            "order_id" => Request::get("order_id"),
            "order_item_id" => Request::get("order_item_id"),
        );


        // dd($data);
        $response = $this->api_model->truckAssign($data);
        // dd($response);
        return $response;


    }

    public function uploadOrderBill(){

        $data = array(
            "order_id" => "".Request::post('order_id')
        );

        $files = array();

        if(Request::file('bill_image')){

            $image = Request::file('bill_image');                

            if(Request::post('bill_type') == 'invoice'){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "bill_image"
                ));

            }else if(Request::post('bill_type') == 'credit_notes'){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "creditNote_image"
                ));

            }else if(Request::post('bill_type') == 'debit_notes'){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "debitNote_image"
                ));

            }

                
                       

        }

       // dd($data);
        $response = $this->api_model->uploadOrderBill($data,$files);
        // dd($response);
        return $response;

    }

    public function trackingDetails($item_id,$tracking_id){

        $data = array(
            "item_id" => $item_id,
            "tracking_id" => $tracking_id
        );      
        

        $response = $this->api_model->trackingDetails($data);
        // dd($response);
        return $response;

    }
}
