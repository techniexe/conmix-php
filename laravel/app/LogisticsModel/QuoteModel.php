<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class QuoteModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showQuotes(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showQuotes($data);
        // dd($response);
        return $response;


    }

    public function updateQuote(){

        $data = array(
            "quoted_amount" => Request::get("quoted_amount"),
            "quoteId" => Request::get("quoteId"),
        );

        // dd($data);
        $response = $this->api_model->updateQuote($data);

        if($response["status"] == 200){
            $data = $this->api_model->quoteProcess();
        }
        

        // dd($response);
        return $response;

    }
}
