<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProfileModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showProfileDetails(){

        $data = array();

        $response = $this->api_model->getLogisticsProfile();
        // dd($response);
        return $response;
    }

    public function editProfile(){

        $files = array();

        if(Request::post('is_my_profile') == 'yes'){

            $data = array(
                "full_name" => "".Request::post('full_name'),
                "email" => "".Request::post('email'),
                "mobile_number" => "".Request::post('mobile_number'),
                "company_type" => Request::post("company_type"),
                "company_certification_number" => Request::post("company_certification_number"),
            );
    
            if(Request::post('landline_number')){
                $data["landline_number"] =  Request::post('landline_number');
            } 
            
            if(Request::post('password')){
                $data["password"] =  Request::post('password');
            }
            
            if(Request::post('fax_number')){
                $data["fax_number"] =  Request::post('fax_number');
            }
            
            if(Request::post('website_URL')){
                $data["website_URL"] =  Request::post('website_URL');
            }
            
            if(Request::post('pan_number')){
                $data["pan_number"] =  Request::post('pan_number');
            }
            
            if(Request::post('gst_number')){
                $data["gst_number"] =  Request::post('gst_number');
            }

            $files = array();

            if(Request::file('pancard_image')){

                $image = Request::file('pancard_image');
                
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image" => $image,
                        "param_key" => "pancard_image"
                    ));

                    

            }
            
            if(Request::file('company_certification_image')){

                $image = Request::file('company_certification_image');
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image" => $image,
                        "param_key" => "company_certification_image"
                    ));

                           

            }
            
            if(Request::file('gst_certification_image')){

                $image = Request::file('gst_certification_image');
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image" => $image,
                        "param_key" => "gst_certification_image"
                    ));

                           

            }

        }     
        
        //  dd($data);
         $response = $this->api_model->editLogisticsProfile($data,$files);
         // dd($response);
         return $response;

    }
}
