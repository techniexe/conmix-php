<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;
use App\ApiConfig;
use App\BaseModel;

class LoginModel extends BaseModel
{
    //
    protected $api_model;

    protected $final_response;

    public function __construct(){
        $this->api_model = new ApisModel();

        $this->final_response = array(
            "status" => null,
            "data" => null,
            "error" => null
        );
    }

    public function login(){

        if(Request::post('login_btn')){
            $email_mobile = Request::post('email_mobile');
            $password = Request::post('password');

            $data = array(
                "email_mobile" => $email_mobile,
                "password" => $password
            );

            $auth_resposnse = $this->api_model->getSessionToken();

            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){
                //Success Response
                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                $login_response = $this->api_model->login($data);
                // dd($login_response);
                $this->final_response = $login_response;

                if($this->final_response["status"] == 200){
                    $custom_token = $this->final_response["data"]["customToken"];

                    session()->put("custom_token",$custom_token);

                    $admin_profile = $this->api_model->getLogisticsProfile();


                    if($admin_profile["status"] == 200){
                        session()->put("profile_details",$admin_profile["data"]);
                    }

                    // dd(session('profile_details', null));

                }

                // return $login_response;
                // $request->session()->put('session_token', ''.$session_token);

                // $globla = session('session_token', null);
                // $value = $request->session()->get('session_token', null);
                // echo "Global::".$globla.'<br>';
                // echo "value::".$value.'<br>';
                // echo $session_token;exit;
            }else{
                //Error Response
            }

        return $this->final_response;

        }
    }

    public function logout(){
        session()->forget('custom_token');
        session()->flush();
    }
}
