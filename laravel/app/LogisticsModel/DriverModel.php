<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class DriverModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showDriverDetails(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showDriverDetails($data);
        // dd($response);
        return $response;


    }

    public function addDriverDetails(){

        
        $data = array(
            "driver_name" => Request::post("driver_name"),
            "driver_mobile_number" => Request::post("driver_mobile_number")
            
        );        


        if(Request::post("driver_alt_mobile_number")){
            $data["driver_alt_mobile_number"] = Request::post("driver_alt_mobile_number");
        }
        
        if(Request::post("driver_whatsapp_number")){
            $data["driver_whatsapp_number"] = Request::post("driver_whatsapp_number");
        }

        $files = array();

        if(Request::file('driver_pic')){

            $image = Request::file('driver_pic');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "driver_pic"
            ));                

        }
        

        

        // dd($data);
        $response = $this->api_model->addDriverDetails($data,$files);

        return $response;


    }
    
    public function editDriverDetails(){

        
        $data = array(
            "driver_name" => Request::post("driver_name"),
            "driver_mobile_number" => Request::post("driver_mobile_number"),
            "_id" => Request::post("driver_id")
            
        );        


        if(Request::post("driver_alt_mobile_number")){
            $data["driver_alt_mobile_number"] = Request::post("driver_alt_mobile_number");
        }
        
        if(Request::post("driver_whatsapp_number")){
            $data["driver_whatsapp_number"] = Request::post("driver_whatsapp_number");
        }

        $files = array();

        if(Request::file('driver_pic')){

            $image = Request::file('driver_pic');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "driver_pic"
            ));                

        }
        

        

        // dd($data);
        $response = $this->api_model->editDriverDetails($data,$files);

        return $response;


    }

}
