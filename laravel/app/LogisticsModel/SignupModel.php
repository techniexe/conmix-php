<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class SignupModel extends Model
{
    //
    protected $api_model;

    protected $final_response;

    public function __construct(){
        $this->api_model = new ApisModel();

        $this->final_response = array(
            "status" => null,
            "data" => null,
            "error" => null
        );
    }

    public function signupOTPSend(){

        if(Request::post('register_btn')){

            $data = array(
                "email_mobile" => Request::post('email_mobile')
            );

            $auth_resposnse = $this->api_model->getSessionToken();
            // dd($auth_resposnse);
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){

                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                if($this->email_validation(Request::post('email_mobile'))){
                    $data = $this->api_model->signupOTPSendViaEmail($data);
                    // dump("email");

                    if($data["status"] == 200){
                        $data["data"]["message"] = "Enter the 6 digits OTP sent on your email ".Request::post('email_mobile');
                        $data["data"]["email_mobile"] = Request::post('email_mobile');
                        $data["data"]["signup_type"] = "email";
                    }

                }else{
                    $data = $this->api_model->signupOTPSendViaMobile($data);
                    // dump("mobile");

                    if($data["status"] == 200){
                        $data["data"]["message"] = "Enter the 6 digits OTP sent on your mobile no. ". str_replace(substr(Request::post('email_mobile'),5, 11),"XXXXXX",Request::post('email_mobile'));
                        $data["data"]["email_mobile"] = Request::post('email_mobile');
                        $data["data"]["signup_type"] = "mobile";
                    }
                }
                
                // dd($data);

                

            }
            return $data;
        }        

    }

    function email_validation($str) { 
        return (!preg_match( 
    "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $str)) 
            ? FALSE : TRUE; 
    } 


    public function signupOTPVerify(){

        if(Request::post('otp_btn')){
            $data = array(
                "email_mobile" => Request::post('email_mobile'),
                "code" => Request::post('otp_code')
            );

            $auth_resposnse = $this->api_model->getSessionToken();
            // dd($auth_resposnse);
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){

                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                $data = $this->api_model->signupOTPVerify($data);
                $data["data"]["signup_type"] = "mobile";
                $data["data"]["email_mobile"] = Request::post('email_mobile');
                if($data["status"] == 200){

                    $custom_token = $data["data"]["customToken"];
                    
                    session()->put("custom_token",$custom_token);

                }
            }

            return $data;
        }

    }

    public function signupRegistration(){

        if(Request::post('register_btn')){

            $data = array(
                "full_name" => Request::post("full_name"),
                "company_name" => Request::post("company_name"),
                "mobile_number" => Request::post("mobile_number"),
                "email" => Request::post("email"),
                "password" => Request::post("password"),
                "pan_number" => Request::post("pan_number"),
                "signup_type" => Request::post("signup_type"),
                "company_type" => Request::post("company_type"),
                "company_certification_number" => Request::post("company_certification_number"),
            );

            if(Request::post("landline_number")){
                $data["landline_number"] = Request::post("landline_number");
            }

            if(Request::post("gst_number")){
                $data["gst_number"] = Request::post("gst_number");
            }

            if(Request::post("fax_number")){
                $data["fax_number"] = Request::post("fax_number");
            }

            if(Request::post("account_type")){
                $data["account_type"] = Request::post("account_type");
            }

            $files = array();

            if(Request::file('pancard_image')){

                $image = Request::file('pancard_image');
                
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image_mime" => $image->getClientMimeType(),
                        "image" => $image,
                        "param_key" => "pancard_image"
                    ));

                    

            }
            
            if(Request::file('company_certification_image')){

                $image = Request::file('company_certification_image');
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image_mime" => $image->getClientMimeType(),
                        "image" => $image,
                        "param_key" => "company_certification_image"
                    ));

                           

            }
            
            if(Request::file('gst_certification_image')){

                $image = Request::file('gst_certification_image');
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image_mime" => $image->getClientMimeType(),
                        "image" => $image,
                        "param_key" => "gst_certification_image"
                    ));

                           

            }

        //  dd($files);
         $response = $this->api_model->supplierRegistration($data,$files);
        
         if($response["status"] == 200){

            $custom_token = $response["data"]["customToken"];
            
            session()->put("custom_token",$custom_token);

        }

         // dd($response);
         return $response;

        }

    }
    
    
    public function signupRegistration2Submit(){

        if(Request::post('register_btn')){

            $data = array(
                "state_id" => Request::post("state_id"),
                "city_id" => Request::post("city_id"),
                "line1" => Request::post("address_line_1"),
                "line2" => Request::post("address_line_2"),
                "pincode" => Request::post("pincode"),
                "pickup_location" => json_encode(array(
                    "coordinates" => array(
                        Request::post("us2_lon"),
                        Request::post("us2_lat")                   
                    )
                )),
            );

        //  dd($files);
         $response = $this->api_model->signupRegistration2Submit($data);
        // dd($response);
         if($response["status"] == 200){

            $custom_token = $response["data"]["customToken"];
            
            session()->put("custom_token",$custom_token);

            $admin_profile = $this->api_model->getLogisticsProfile();
                    

            if($admin_profile["status"] == 200){
                session()->put("profile_details",$admin_profile["data"]);    
            }


        }

         // dd($response);
         return $response;

        }

    }
}
