<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use App\ApiConfig;
use App\BaseModel;

class ApisModel extends BaseModel
{
    //
    public function getSessionToken(){

        $url_path = "".ApiConfig::SESSION_TOKEN_URL;
        $params = array(
            "accessToken"=> ApiConfig::ACCESS_TOKEN,
            "registrationToken"=> ApiConfig::REGISTRATION_TOKEN
        );
        $auth_resposnse = $this->requestPOST($url_path,$params);

        return $auth_resposnse;
    }

    public function login($data){
        // dd($data);
        $url_path = "".sprintf(ApiConfig::LOGISTICS_LOGIN_URL,$data["email_mobile"]);
        $params = array(
            "password"=> $data["password"]
        );
        $this->setTokenHeader($data["session_token"]);
        $login_resposnse = $this->requestPOST($url_path,$params);
        // dd($login_resposnse);
        return $login_resposnse;

    }

    public function signupOTPSendViaEmail($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_SIGNUP_OTP_SEND_VIA_EMAIL_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function signupOTPSendViaMobile($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_SIGNUP_OTP_SEND_VIA_MOBILE_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function signupOTPVerify($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_SIGNUP_OTP_VERIFY_URL,$data["email_mobile"],$data["code"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestPOST($url_path,$params);

        return $auth_resposnse;

    }   

    public function supplierRegistration($data,$files){

        $url_path = sprintf(ApiConfig::LOGISTICS_SIGNUP_REGISTRATION_URL,$data["signup_type"]);
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function signupRegistration2Submit($data){

        $url_path = ApiConfig::LOGISTICS_SIGNUP_REGISTRATION_ADDRESS_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }


    public function forgotPassOTPSendViaEmail($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_FORGOT_PASS_OTP_SEND_VIA_EMAIL_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function forgotPassOTPSendViaMobile($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_FORGOT_PASS_OTP_SEND_VIA_MOBILE_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function forgotPassOTPVerify($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_FORGOT_PASS_OTP_VERIFY_URL,$data["email_mobile"],$data["code"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestPOST($url_path,$params);

        return $auth_resposnse;

    }



    public function showQuotes($data){

        $url_path = ApiConfig::LOGISTICS_QUOTE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function updateQuote($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_QUOTE_UPDATE_URL,$data["quoteId"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestPUT($url_path,$params);
        
        return $response;

    }

    public function quoteProcess(){

        $url_path = ApiConfig::PLACE_QUOTE_PROCESS;
        $params = array();
    
        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;
    
    }
    
    public function getVehicles($data){

        $url_path = ApiConfig::LOGISTICS_VEHICLES_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function showVehicleCategory($data){

        $url_path = ApiConfig::LOGISTICS_CATEGORY_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function getVehicleSubCategoris($data){

        $url_path = ApiConfig::LOGISTICS_SUB_CATEGORY_LISTING_URL.''.$data["vehicle_cat_id"];
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }
    
    public function getVehicleDetail($vehicle_id){

        $url_path = sprintf(ApiConfig::LOGISTICS_VEHICLE_DETAILS_BY_ID_URL, $vehicle_id);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addVehicles($data,$files){

        $url_path = sprintf(ApiConfig::LOGISTICS_VEHICLE_ADD_URL,$data["vehicleCategoryId"],$data["vehicleSubCategoryId"]);
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function editVehicle($data,$files){

        $url_path = sprintf(ApiConfig::LOGISTICS_VEHICLE_EDIT_URL,$data["vehicleId"]);
        $params = $data;

        $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    //Bank Details Module Start.....................................................

    public function showBankDetails($data){

        $url_path = ApiConfig::LOGISTICS_BANK_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    } 

    public function addBankDetails($data,$files){

        $url_path = ApiConfig::LOGISTICS_BANK_DETAILS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function editBankDetails($data,$files,$bank_detail_id){

        $url_path = sprintf(ApiConfig::LOGISTICS_BANK_DETAILS_EDIT_URL,$bank_detail_id);
        $params = $data;

        $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    //Bank Details Module Over.....................................................

    //Driver Details Module Over.....................................................

    public function showDriverDetails($data){

        $url_path = ApiConfig::LOGISTICS_DRIVER_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addDriverDetails($data,$files){

        $url_path = ApiConfig::LOGISTICS_DRIVER_DETAILS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function editDriverDetails($data,$files){

        $url_path = sprintf(ApiConfig::LOGISTICS_DRIVER_DETAILS_EDIT_URL,$data["_id"]);
        $params = $data;
        
        $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    //Driver Details Module Over.....................................................

    // Support Ticket Module Start.........................................

    public function showSupportTickets($data){

        $url_path = ApiConfig::LOGISTICS_SUPPORT_TICKET_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addSupportTicket($data,$files){

        $url_path = ApiConfig::LOGISTICS_SUPPORT_TICKET_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function showTicketDetails($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_SUPPORT_TICKET_DETAIL_URL,$data["ticket_id"]);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function replySupportTicket($data,$files){

        $url_path = ApiConfig::LOGISTICS_SUPPORT_TICKET_REPLY_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    // Support Ticket Module Over.........................................

     // Profile Module Start.........................................

     public function getLogisticsProfile(){

        $url_path = ApiConfig::LOGISTICS_PROFILE_DETAIL_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }


    public function editLogisticsProfile($data,$files){

        $url_path = ApiConfig::LOGISTICS_PROFILE_UPDATE_URL;
        $params = $data;

        $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    // Profile Module Over.........................................

    // Dashboard Module Start.........................................

    public function getDashboardDetails(){

        $url_path = ApiConfig::LOGISTICS_DASHBOARD_DETAILS_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getQuoteList(){

        $url_path = ApiConfig::LOGISTICS_DASHBOARD_QUOTES_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getLatestOrders(){

        $url_path = ApiConfig::LOGISTICS_DASHBOARD_LATEST_ORDERS_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function getMonthlyChartDetails($data){

        $url_path = ApiConfig::LOGISTICS_MONTHLY_CHART_URL;

        $is_added = 0;
        
        
        if(isset($data["year"]) && $is_added==0){
            $url_path .= "&year=".$data["year"];
            
        }       
        
        
        if(isset($data["status"]) && $is_added==0){
            $url_path .= "&status=".$data["status"];
            
        }
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    
    public function getDayChartDetails($data){

        $url_path = ApiConfig::LOGISTICS_DAILY_CHART_URL;
        $is_added=0;
        if(isset($data["year"]) && $is_added==0){
            $url_path .= "&year=".$data["year"];
            
        }
        
        if(isset($data["month"]) && $is_added==0){
            $url_path .= "&month=".$data["month"];
            
        }
        
        if(isset($data["status"]) && $is_added==0){
            $url_path .= "&status=".$data["status"];
            
        }
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    // Dashboard Module End.........................................

    // Orders Module End.........................................


    public function showOrders($data){

        $url_path = ApiConfig::LOGISTICS_ORDER_LISTING;
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function showOrdersDetails($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_ORDER_DETAILS,$data["order_id"]);
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function truckAssign($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_ORDER_TRUCK_ASSIGN,$data["order_id"],$data["order_item_id"]);
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        
        return $response;

    }

    public function uploadOrderBill($data,$files){

        $url_path = sprintf(ApiConfig::LOGISTICS_ORDER_BILL_UPLOAD_URL,$data["order_id"]);
        $params = $data;
        // dd($url_path);
        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;


    }

    public function trackingDetails($data){

        $url_path = sprintf(ApiConfig::LOGISTICS_ORDER_TRACKING_URL,$data["item_id"],$data["tracking_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }

    // Orders Module End.........................................
    // Report Module Start.........................................

    public function order_listing_report($data){

        $url_path = ApiConfig::LOGISTICS_ORDER_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }
    

    // Report Module End.........................................
}
