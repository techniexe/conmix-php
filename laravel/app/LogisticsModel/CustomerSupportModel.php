<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class CustomerSupportModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showTickets(){

        $data = array();

        if(Request::get("severity")){
            $data["severity"] = Request::get("severity");
        }
        
        if(Request::get("subject")){
            $data["subject"] = Request::get("subject");
        }      
         
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showSupportTickets($data);
        // dd($response);
        return $response;

    }

    public function addTicket(){

        $profile_detials = session('profile_details', null);


        $data = array(
            "question_type" => "".Request::post('question_type'),
            "severity" => "".Request::post('severity'),
            "subject" => "".Request::post('subject'),
            "description" => "".Request::post('description'),
            "client_type" => "Supplier",
            "client_id" => $profile_detials["_id"]

        );
        // dd(session('profile_details', null));
        
        
        if(Request::post('order_id')){
            $data["order_id"] =  Request::post('order_id');
        }

        $files = array();

        if(Request::file('attachments')){

            $images = Request::file('attachments');
            foreach($images as $image){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image
                ));

            }            

        }

        //  dd($files);
         $response = $this->api_model->addSupportTicket($data,$files);
         // dd($response);
         return $response;

    }

    public function showTicketDetails($ticket_id){

        $data = array(
            "ticket_id" => $ticket_id
        );

        $response = $this->api_model->showTicketDetails($data);
         // dd($response);
         return $response;

    }

    public function replyTicket(){

        $data = array(
            "comment" => "".Request::post('comment'),
            "ticket_id" => "".Request::post('ticket_id')

        );

        if(Request::post('support_ticket_status')){
            $data["support_ticket_status"] =  Request::post('support_ticket_status');
        }

        $files = array();

        if(Request::file('attachments')){

            $images = Request::file('attachments');
            foreach($images as $image){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image
                ));

            }            

        }

        //  dd($files);
         $response = $this->api_model->replySupportTicket($data,$files);
         // dd($response);
         return $response;

    }
}
