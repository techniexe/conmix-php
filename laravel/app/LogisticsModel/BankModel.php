<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class BankModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showBankDetails(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }

        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showBankDetails($data);
        // dd($response);
        return $response;


    }

    public function addBankDetails(){


        $data = array(
            "account_holder_name" => Request::post("account_holder_name"),
            "account_number" => Request::post("account_number"),
            "ifsc" => Request::post("ifsc"),
            "account_type" => Request::post("account_type"),

        );

        $files = array();

        if(Request::file('cancelled_cheque_image')){

            $image = Request::file('cancelled_cheque_image');


            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "cancelled_cheque_image"
            ));

        }


        $bank = array();

        $bank["bank"] = json_encode($data);

        // dd($data);
        $response = $this->api_model->addBankDetails($bank,$files);

        return $response;


    }

    public function editBankDetails(){

        $data = array(
            "account_holder_name" => Request::post("account_holder_name"),
            "account_number" => Request::post("account_number"),
            "ifsc" => Request::post("ifsc"),
            "account_type" => Request::post("account_type"),
            "_id" => Request::post("bank_detail_id"),

        );

        $files = array();

        if(Request::file('cancelled_cheque_image')){

            $image = Request::file('cancelled_cheque_image');


            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "cancelled_cheque_image"
            ));

        }


        $bank = array();

        $bank["bank"] = json_encode($data);

        // dd($data);
        $response = $this->api_model->editBankDetails($bank,$files,Request::post("bank_detail_id"));

        return $response;


    }

    public function setDefaultBaknkDefault(){

        $data = array(
            "_id" => Request::post("bank_detail_id"),
            "is_default" => "true"

        );

        $files = array();

        $bank["bank"] = json_encode($data);

        // dd($data);
        $response = $this->api_model->editBankDetails($bank,$files,Request::post("bank_detail_id"));

        return $response;


    }
}
