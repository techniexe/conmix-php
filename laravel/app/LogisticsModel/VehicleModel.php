<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class VehicleModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showVehicles(){

        $data = array();

        if(Request::get("vehicle_category_id")){
            $data["vehicle_category_id"] = Request::get("vehicle_category_id");
        }
    
        if(Request::get("vehicle_sub_category_id")){
            $data["vehicle_sub_category_id"] = Request::get("vehicle_sub_category_id");
        }
        
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
    
        if(Request::get("is_gps_enabled")){
            $data["is_gps_enabled"] = Request::get("is_gps_enabled");
        }
        
        if(Request::get("is_insurance_active")){
            $data["is_insurance_active"] = Request::get("is_insurance_active");
        }
    
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $response = $this->api_model->getVehicles($data);

        return $response;

    }

    public function showVehicleCategory(){
        $data = array();

        $response = $this->api_model->showVehicleCategory($data);

        return $response;
    }

    public function showSubCategoies(){

        $data = array();

        if(Request::post("vehicle_cat_id")){
            $data["vehicle_cat_id"] = Request::post("vehicle_cat_id");
        }else{
            $data["vehicle_cat_id"] = "";
        }

        $response = $this->api_model->getVehicleSubCategoris($data);

        return $response;

    }
    
    public function getVehicleDetail($vehicle_id){

        $data = array();

        $response = $this->api_model->getVehicleDetail($vehicle_id);

        return $response;

    }

    public function addVehicle(){

        $rc_number = Request::post("vehicle_rc_number_1").'-'.Request::post("vehicle_rc_number_2").'-'.Request::post("vehicle_rc_number_3").'-'.Request::post("vehicle_rc_number_4");

        $is_gps_active = '';
        if(Request::post("is_gps_enabled") == 'on'){
            $is_gps_active = 'true';
        }else{
            $is_gps_active = 'false';
        }
        
        $is_insurance_active = '';
        if(Request::post("is_insurance_active") == 'on'){
            $is_insurance_active = 'true';
        }else{
            $is_insurance_active = 'false';
        }


        $data = array(
            "vehicleCategoryId" => Request::post("vehicleCategoryId"),
            "vehicleSubCategoryId" => Request::post("vehicleSubCategoryId"),
            "vehicle_rc_number" => $rc_number,
            "min_trip_price" => Request::post("min_trip_price"),
            "manufacturer_name" => Request::post("manufacturer_name"),
            "vehicle_model" => Request::post("vehicle_model"),
            "manufacture_year" => Request::post("manufacture_year"),
            "driver_name" => Request::post("driver_name"),
            "driver_mobile_number" => Request::post("driver_mobile_number"),
            "delivery_range" => Request::post("delivery_range"),
            "driver1_id" => Request::post("driver1_id"),
            "line1" => Request::post("line1"),
            "line2" => Request::post("line2"),
            "state_id" => Request::post("state_id"),
            "city_id" => Request::post("city_id"),
            "pincode" => Request::post("pincode"),
            "is_gps_enabled" => $is_gps_active,
            "is_insurance_active" => $is_insurance_active,
            "is_insurance_active" => $is_insurance_active,
            "pickup_location" => json_encode(array(
                "coordinates" => array(
                    Request::post("us2_lon"),
                    Request::post("us2_lat")                   
                )
            )),
        );
        // dd(Request::file('rc_book_image'));
        // if(Request::post("per_metric_ton_per_km_rate")){
        //     $data["per_metric_ton_per_km_rate"] = Request::post("per_metric_ton_per_km_rate");
        // }
        
        if(Request::post("driver2_id")){
            $data["driver2_id"] = Request::post("driver2_id");
        }
        $files = array();
        if(Request::file('rc_book_image')){

            $image = Request::file('rc_book_image');
            // dd($image);
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "rc_book_image"
                ));


        }
        
        if(Request::file('insurance_image')){

            $image = Request::file('insurance_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "insurance_image"
                ));

                       

        }
        
        if(Request::file('vehicle_image')){

            $image = Request::file('vehicle_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "vehicle_image"
                ));

                       

        }
        
         //  dd($files);
         $response = $this->api_model->addVehicles($data,$files);

         // dd($response);
         return $response;

    }
    
    
    public function editVehicle(){

        $rc_number = Request::post("vehicle_rc_number_1").'-'.Request::post("vehicle_rc_number_2").'-'.Request::post("vehicle_rc_number_3").'-'.Request::post("vehicle_rc_number_4");
        // $rc_number = Request::post("vehicle_rc_number");

        $is_gps_active = '';
        if(Request::post("is_gps_enabled") == 'on'){
            $is_gps_active = 'true';
        }else{
            $is_gps_active = 'false';
        }
        
        $is_insurance_active = '';
        if(Request::post("is_insurance_active") == 'on'){
            $is_insurance_active = 'true';
        }else{
            $is_insurance_active = 'false';
        }


        $data = array(
            "vehicle_rc_number" => $rc_number,
            "min_trip_price" => Request::post("min_trip_price"),
            "manufacturer_name" => Request::post("manufacturer_name"),
            "vehicle_model" => Request::post("vehicle_model"),
            "manufacture_year" => Request::post("manufacture_year"),
            "driver_name" => Request::post("driver_name"),
            "driver_mobile_number" => Request::post("driver_mobile_number"),
            "delivery_range" => Request::post("delivery_range"),
            "vehicleId" => Request::post("vehicleId"),
            "driver1_id" => Request::post("driver1_id"),
            "line1" => Request::post("line1"),
            "line2" => Request::post("line2"),
            "state_id" => Request::post("state_id"),
            "city_id" => Request::post("city_id"),
            "pincode" => Request::post("pincode"),
            "is_gps_enabled" => $is_gps_active,
            "is_insurance_active" => $is_insurance_active,
            "pickup_location" => json_encode(array(
                "coordinates" => array(
                    Request::post("us2_lon"),
                    Request::post("us2_lat")                   
                )
            )),
        );
        // dd(Request::file('rc_book_image'));
        // if(Request::post("per_metric_ton_per_km_rate")){
        //     $data["per_metric_ton_per_km_rate"] = Request::post("per_metric_ton_per_km_rate");
        // }

        if(Request::post("driver2_id")){
            $data["driver2_id"] = Request::post("driver2_id");
        }

        $files = array();
        if(Request::file('rc_book_image')){

            $image = Request::file('rc_book_image');
            // dd($image);
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "rc_book_image"
                ));


        }
        
        if(Request::file('insurance_image')){

            $image = Request::file('insurance_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "insurance_image"
                ));

                       

        }
        
        if(Request::file('vehicle_image')){

            $image = Request::file('vehicle_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "vehicle_image"
                ));

                       

        }

        //   dd($data);
         $response = $this->api_model->editVehicle($data,$files);

         // dd($response);
         return $response;

    }
}
