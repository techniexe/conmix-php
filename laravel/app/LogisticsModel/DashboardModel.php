<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class DashboardModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function getDashboardDetails(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getDashboardDetails($data);
        // dd($response);
        return $response;

    }
    
    public function getQuoteList(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getQuoteList($data);
        // dd($response);
        return $response;

    }
    
    public function getLatestOrders(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getLatestOrders($data);
        // dd($response);
        return $response;

    }

    public function getMonthlyChartDetails(){

        $data = array();

        if(Request::get('year')){
            $data['year'] = Request::get('year');
        }
        
        if(Request::get('status')){
            $data['status'] = Request::get('status');
        }
        
        // dd($data);
        $response = $this->api_model->getMonthlyChartDetails($data);
        // dd($response);
        return $response;

    }
    
    
    public function getDayChartDetails(){

        $data = array();

        if(Request::get('year')){
            $data['year'] = Request::get('year');
        }
        
        if(Request::get('month')){
            $data['month'] = Request::get('month');
        }
        
        if(Request::get('status')){
            $data['status'] = Request::get('status');
        }
        
        // dd($data);
        $response = $this->api_model->getDayChartDetails($data);
        // dd($response);
        return $response;

    }
}
