<?php

namespace App\LogisticsModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ReportModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();

        $month_array = array(

            "01" => 0,
            "02" => 1,
            "03" => 2,
            "04" => 3,
            "05" => 4,
            "06" => 5,
            "07" => 6,
            "08" => 7,
            "09" => 8,
            "10" => 9,
            "11" => 10,
            "12" => 11,
        
        );

        $all_month_array = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    }

    public function order_listing_report(){

        $data = array();       

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        
        if(Request::get("delivery_status")){
            $data["status"] = Request::get("delivery_status");
        }
        // dd($data);
        $data = $this->api_model->order_listing_report($data);  

        return $data;
    }
}
