<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    protected $common_model;
    public function boot()
    {
        // dump(Session::get('supplier_custom_token'));
        //
        View::composer('*', function ($view)
        {
            // dump(session('custom_token', null));
            // dump(Session::get('supplier_custom_token'));
            $this->common_model = new \App\CommonAPIModel();
            $this->product_model = new \App\BuyerModel\ProductModel();
            $this->profile_model = new \App\BuyerModel\ProfileModel();
            $this->address_model = new \App\SupplierModel\AddressModel();
            
            $path = Request::path();
            if (strpos($path, 'supplier/') !== false) {
                $plant_address = array();
                if (!Cache::has('plant_address')) {
                    if(Session::get('supplier_custom_token') != null){
                        $plant_address = $this->address_model->showAddressDetails(); 
                        Cache::put('plant_address', $plant_address, $seconds = 30);
                        // dump("Not Cache");
                    }
                }else{
                    $plant_address = Cache::get('plant_address',null);
                    // dump("From Cache");
                }
                $sub_vendors = array();
                if (!Cache::has('sub_vendors')) {
                    if(Session::get('supplier_custom_token') != null){
                        $sub_vendors = $this->address_model->getSubvendors(); 
                        Cache::put('sub_vendors', $sub_vendors, $seconds = 30);
                        // dump("Not Cache");
                    }
                }else{
                    $sub_vendors = Cache::get('sub_vendors',null);
                    // dump("From Cache");
                }
                $vendors_fill_details = array();
                if (!Cache::has('vendors_fill_details')) {
                    if(Session::get('supplier_custom_token') != null){
                        $vendors_fill_details = $this->address_model->getFillDetailsOfVendor();  
                        Cache::put('vendors_fill_details', $vendors_fill_details, $seconds = 30);
                        // dump("Not Cache");
                    }
                }else{
                    $vendors_fill_details = Cache::get('vendors_fill_details',null);
                    // dump("From Cache");
                }
                $supplier_noti_count_data = array();
                if (!Cache::has('supplier_noti_count_data')) {
                    if(Session::get('supplier_custom_token') != null){
                        $supplier_noti_count_data = $this->common_model->getSupplierNotificatioCount(); 
                        Cache::put('supplier_noti_count_data', $supplier_noti_count_data, $seconds = 10);
                        // dump("Not Cache");
                    }
                }else{
                    $supplier_noti_count_data = Cache::get('supplier_noti_count_data',null);
                    // dump("From Cache");
                }
                // $plant_address = $this->address_model->showAddressDetails(); 
                // $sub_vendors = $this->address_model->getSubvendors(); 
                // $vendors_fill_details = $this->address_model->getFillDetailsOfVendor(); 
                // $supplier_noti_count_data = $this->common_model->getSupplierNotificatioCount();
                // dd($vendors_fill_details);
                $view->with('supplier_noti_count_data', $supplier_noti_count_data);
                $view->with('plant_address', $plant_address);
                $view->with('sub_vendors', $sub_vendors);
                $view->with('vendors_fill_details', $vendors_fill_details);
            }

            // dd(Session::get('my_cart_id'));
            $product_cat = array();
            // $product_cat = $this->common_model->getProductCategory();
            // $cart_details = $this->common_model->getCartDetailsById(Session::get('my_cart_id'));
            // dd($cart_details);
            // $billing_address = $this->profile_model->showBillngAddress();
            
            

            // dd($sub_vendors);
            
            // $wish_list = $this->product_model->getWishList();
            $wish_list = array();

            // $site_data = $this->profile_model->showSites();
            // dd($site_data);
            if (!Cache::has('country_data')) {
                $country_data = $this->common_model->getCountries();
                Cache::put('country_data', $country_data, $seconds = 3600);
                    // dump("Not Cache");
            }else{
                $country_data = Cache::get('country_data',null);
                // dump("From Cache");
            }
            
            if (!Cache::has('state_data')) {
                $state_data = $this->common_model->getStates();
                Cache::put('state_data', $state_data, $seconds = 10);
                    // dump("Not Cache");
            }else{
                $state_data = Cache::get('state_data',null);
                // dump("From Cache");
            }
            
            if (!Cache::has('city_data')) {
                $city_data = $this->common_model->getCities();
                Cache::put('city_data', $city_data, $seconds = 10);
                    // dump("Not Cache");
            }else{
                $city_data = Cache::get('city_data',null);
                // dump("From Cache");
            }
            // $country_data = $this->common_model->getCountries();
            // $state_data = $this->common_model->getStates();
            // $city_data = $this->common_model->getCities();
            
            $site_data = array();
            $all_site_arr = array();
            $billing_address = array();

            $cement_brand_data = array();
            $sand_source_data = array();
            $sand_zone_data = array();
            $aggregate_source_data = array();
            $aggregate_cat_data = array();
            $cement_grade_data = array();

            $fly_ash_source_data = array();
            $admixture_brand_data = array();
            $all_concrete_data = array();
            $cart_details = array();

            if(session('buyer_profile_details', null) != null){
                // $noti_count_data = $this->common_model->gettNotificatioCount();

                if (!Cache::has('noti_count_data')) {
                    if(Session::get('buyer_custom_token') != null){
                        $noti_count_data = $this->common_model->gettNotificatioCount();
                        Cache::put('noti_count_data', $noti_count_data, $seconds = 10);
                        // dump("Not Cache");
                    }
                }else{
                    $noti_count_data = Cache::get('noti_count_data',null);
                    // dump("From Cache");
                }
                
                if (!Cache::has('site_data')) {
                    if(Session::get('buyer_custom_token') != null){
                        $site_data = $this->profile_model->showSites(); 
                        Cache::put('site_data', $site_data, $seconds = 30);
                        // dump("Not Cache");
                    }
                }else{
                    $site_data = Cache::get('site_data',null);
                    // dump("From Cache");
                }
                
                if (!Cache::has('billing_address')) {
                    if(Session::get('buyer_custom_token') != null){
                        $billing_address = $this->profile_model->showBillngAddress();
                        Cache::put('billing_address', $billing_address, $seconds = 30);
                        // dump("Not Cache");
                    }
                }else{
                    $billing_address = Cache::get('billing_address',null);
                    // dump("From Cache");
                }

                // $site_data = $this->profile_model->showSites();
                // $billing_address = $this->profile_model->showBillngAddress();
                
                if(isset($site_data["data"])){
                    foreach($site_data["data"] as $value){

                        $all_site_arr[$value["_id"]] = $value;

                    }
                }              

               

                $cart_details = $this->common_model->getCartDetailsById(Session::get('my_cart_id'));
                // dd($cart_details);
            }

            if (!Cache::has('custom_mix_options_data')) {
                $custom_mix_options_data = $this->common_model->customDesignOptions();
                Cache::put('custom_mix_options_data', $custom_mix_options_data, $seconds = 30);
                // dump("Not Cache");
                
            }else{
                $custom_mix_options_data = Cache::get('custom_mix_options_data',null);
                // dump("From Cache");
            }
            // $custom_mix_options_data = $this->common_model->customDesignOptions();
            // dd($custom_mix_options_data);
            
            // dd($custom_mix_options_data);

            if(isset($custom_mix_options_data["status"]) && $custom_mix_options_data["status"] == 200){

                if(isset($custom_mix_options_data["data"]) && count($custom_mix_options_data["data"])){
                    $cement_brand_data = $custom_mix_options_data['data']["cement_brand"];
                    $sand_source_data = $custom_mix_options_data['data']["sand_source"];
                    $sand_zone_data = $custom_mix_options_data['data']["sand_zone"];
                    $aggregate_source_data = $custom_mix_options_data['data']["agg_source"];
                    $aggregate_cat_data = $custom_mix_options_data['data']["agg1_sub_cat"];
                    $cement_grade_data = $custom_mix_options_data['data']["cement_grade"];

                    $fly_ash_source_data = $custom_mix_options_data['data']["fly_ash_source"];
                    $admixture_brand_data = $custom_mix_options_data['data']["admix_brand"];
                }

            }

            if(session('profile_details', null) != null){
                // $admin_noti_count_data = $this->common_model->getAdminNotificatioCount();

                if (!Cache::has('admin_noti_count_data')) {
                    if(Session::get('custom_token') != null){
                        $admin_noti_count_data = $this->common_model->getAdminNotificatioCount();
                        Cache::put('admin_noti_count_data', $admin_noti_count_data, $seconds = 10);
                        // dump("Not Cache");
                    }
                }else{
                    $admin_noti_count_data = Cache::get('admin_noti_count_data',null);
                    // dump("From Cache");
                }
            }
            
            if (!Cache::has('concrete_grade')) {
                $concrete_grade = $this->common_model->getConcreteGrade();
                Cache::put('concrete_grade', $concrete_grade, $seconds = 30);
                // dump("Not Cache");
                
            }else{
                $concrete_grade = Cache::get('concrete_grade',null);
                // dump("From Cache");
            }
            // $concrete_grade = $this->common_model->getConcreteGrade();
            // $payment_method = $this->common_model->getPaymentmethods();
            $payment_method = null;
            // dd($concrete_grade);
            //Design Mix
            // $concrete_grade_data = $this->common_model->getConcreteGrade();
            // dd($concrete_grade_data);
            // $cement_brand_data = $this->common_model->getCementBrand();
            // $sand_source_data = $this->common_model->getSandSource();
            // $aggregate_source_data = $this->common_model->getAggregateSource();
            // $aggregate_cat_data = $this->common_model->getAggregateSandCategory();
            // $cement_grade_data = $this->common_model->getCementGrade();

            // $fly_ash_source_data = $this->common_model->getFlyAshSource();
            // $admixture_brand_data = $this->common_model->getAdMixtureBrand();
            
            // $concrete_grade_data = $this->common_model->getConcreteGrade();
            $concrete_grade_data = $concrete_grade;
            // dd($concrete_grade_data);

            
            if(isset($concrete_grade_data["data"])){
                foreach($concrete_grade_data["data"] as $value){
                    $all_concrete_data[$value["_id"]] = $value;
                }
            }
            

            $aggregate_sub_cat_data = array(
                "data" => array()
            );
            // if($aggregate_cat_data["status"] == 200){
            //     if(isset($aggregate_cat_data["data"][0]["_id"])){
            //         $cat_id = $aggregate_cat_data["data"][0]["_id"];
    
            //         $aggregate_sub_cat_data = $this->common_model->getAggregateSandSubCategory($cat_id);
            //     }
                
            // }

            

            

            //Design Mix Over

            // dd($concrete_grade_data);
            // dd($state_data);
            // dd($wish_list);
            // dd($site_data);
            // dd($cement_brand_data);

            // $all_site_arr = array();
            // if(isset($site_data["data"])){
            //     foreach($site_data["data"] as $value){

            //         $all_site_arr[$value["_id"]] = $value;

            //     }
            // }
            
            // dd($all_site_arr);
           
            // $all_concrete_data = array();
            // if(isset($concrete_grade_data["data"])){
            //     foreach($concrete_grade_data["data"] as $value){
            //         $all_concrete_data[$value["_id"]] = $value;
            //     }
            // }

            // dd($city_data);

            // View::share('cart', $cart_details);
            $view->with('cart_details', $cart_details);
            $view->with('product_cat', $product_cat);
            $view->with('wish_list', $wish_list);
            $view->with('country_data', $country_data);
            $view->with('state_data', $state_data);
            $view->with('city_data', $city_data);
            $view->with('site_data', $site_data);
            $view->with('all_site_arr', $all_site_arr);
            $view->with('all_concrete_data', $all_concrete_data);
            $view->with('concrete_grade', $concrete_grade);

            $view->with("concrete_grade_data",$concrete_grade_data["data"]);
            $view->with("cement_brand_data",$cement_brand_data);
            $view->with("sand_source_data",$sand_source_data);
            $view->with("sand_zone_data",$sand_zone_data);
            $view->with("aggregate_source_data",$aggregate_source_data);
            $view->with("aggregate_sub_cat_data",$aggregate_cat_data);
            $view->with("fly_ash_source_data",$fly_ash_source_data);
            $view->with("admixture_brand_data",$admixture_brand_data);
            $view->with("cement_grade_data",$cement_grade_data);
            if(isset($noti_count_data) && $noti_count_data != null){
                $view->with('noti_count_data', $noti_count_data);
            }
            // $view->with('supplier_noti_count_data', $supplier_noti_count_data);
            if(isset( $admin_noti_count_data) && $admin_noti_count_data != null){
                $view->with('admin_noti_count_data', $admin_noti_count_data);
            }
            $view->with('payment_method', $payment_method);
            $view->with('billing_address', $billing_address);
           
            //its just a dummy data object.
            // $product_cat = $this->common_model->getProductCategory();
            // dd($product_cat);
            // $view->with('product_cat', $product_cat);
            
        });
    }
}
