<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use App\ApiConfig;
use App\BaseModel;

class ApisModel extends BaseModel
{
    //
    public function getSessionToken(){

        $url_path = "".ApiConfig::SESSION_TOKEN_URL;
        $params = array(
            "accessToken"=> ApiConfig::ACCESS_TOKEN,
            "registrationToken"=> ApiConfig::REGISTRATION_TOKEN
        );
        $auth_resposnse = $this->requestPOST($url_path,$params);

        return $auth_resposnse;
    }

    public function login($data){
        // dd($data);
        $url_path = "".sprintf(ApiConfig::SUPPLIER_LOGIN_URL,$data["email_mobile"]);
        $params = array(
            "password"=> $data["password"]
        );
        $this->setTokenHeader($data["session_token"]);
        // dump($url_path);
        $login_resposnse = $this->requestPOST($url_path,$params);
        // dd($login_resposnse);
        return $login_resposnse;

    }

    public function signupOTPSendViaEmail($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SIGNUP_OTP_SEND_VIA_EMAIL_URL,$data["email_mobile"]);
        $params = $data;
        // dd($url_path);
        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function signupOTPSendViaMobile($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SIGNUP_OTP_SEND_VIA_MOBILE_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function signupOTPVerify($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SIGNUP_OTP_VERIFY_URL,$data["email_mobile"],$data["code"]);
        $params = $data;
        
        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestPOST($url_path,$params);
        
        return $auth_resposnse;

    }   
    
    public function signupEmailVerify($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SIGNUP_EMAIL_VERIFY_URL,$data["email"]);
        $params = $data;
        
        $auth_resposnse = $this->requestGET($url_path,$params);
        
        return $auth_resposnse;

    }   

    public function supplierRegistration($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_SIGNUP_REGISTRATION_URL,$data["signup_type"]);
        $params = $data;
        // dd(json_encode($params));
        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;

    }


    public function forgotPassOTPSendViaEmail($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_FORGOT_PASS_OTP_SEND_VIA_EMAIL_URL,$data["email_mobile"]);
        $params = $data;

        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function forgotPassOTPSendViaMobile($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_FORGOT_PASS_OTP_SEND_VIA_MOBILE_URL,$data["email_mobile"]);
        $params = $data;

        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function forgotPassOTPVerify($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_FORGOT_PASS_OTP_VERIFY_URL,$data["email_mobile"],$data["code"]);
        $params = $data;

        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestPOST($url_path,$params);

        return $auth_resposnse;

    }   


    //Product Module Start.....................................................

    public function showProducts($data){

        $url_path = ApiConfig::SUPPLIER_PRODUCT_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addSupplierProduct($data){

        $url_path = ApiConfig::SUPPLIER_PRODUCT_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editSupplierProduct($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_PRODUCT_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function updateProductQuantity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_PRODUCT_QUANTITY_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    //Product Module Over.....................................................

    //Contact Details Module Start.....................................................

    public function showContactDetails($data){

        $url_path = ApiConfig::SUPPLIER_CONTACT_DETAILS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addContactDetails($data){

        $url_path = ApiConfig::SUPPLIER_CONTACT_DETAILS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;


    }
    
    public function editContactDetails($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CONTACT_DETAILS_EDIT_URL,$data["contact"]["_id"]);
        $params = $data;
        // dd($url_path);
        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;


    }

    //Contact Details Module Over.....................................................

    //Bank Details Module Start.....................................................

    public function showBankDetails($data){

        $url_path = ApiConfig::SUPPLIER_BANK_DETAILS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addBankDetails($data,$files){
        
        $url_path = ApiConfig::SUPPLIER_BANK_DETAILS_ADD_URL;
        // if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
            
        //     $params = $data['bank'];
        // }else{
            $params = $data;
        // }
        // dd($params);
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function editBankDetails($data,$files,$bank_detail_id){

        if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
            $url_path = sprintf(ApiConfig::SUPPLIER_BANK_DETAILS_EDIT_URL,$bank_detail_id);

            $params = $data;
            // dd($params);
            $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);

        }else{
            $url_path = sprintf(ApiConfig::SUPPLIER_BANK_DETAILS_EDIT_URL,$bank_detail_id);
            $params = $data;

            $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
            // dd($response);
        }
        return $response;

    }
    //Bank Details Module Over.....................................................

    //Address Module Over.....................................................

    public function showAddressDetails($data){

        $url_path = ApiConfig::SUPPLIER_ADDRESS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addAddress($data){

        $url_path = ApiConfig::SUPPLIER_ADDRESS_ADD_URL;
        $params = $data;
        // dd(json_encode($params));
        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editAddress($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ADDRESS_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function getRegionServ($data){

        $url_path = ApiConfig::SUPPLIER_GET_REGION_SERV_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function getSourceByRegion($data){

        $url_path = ApiConfig::SUPPLIER_GET_SOURCE_BY_REGION_SERV_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function showbillingAddress($data){

        $url_path = ApiConfig::SUPPLIER_BILLING_ADDRESS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addBillingAddress($data,$files){

        $url_path = ApiConfig::SUPPLIER_BILLING_ADDRESS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function editBillingAddress($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_BILLING_ADDRESS_EDIT_URL,$data["_id"]);
        $params = $data;

        // $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    //Address Module Over.....................................................

    // Support Ticket Module Start.........................................

    public function showSupportTickets($data){

        $url_path = ApiConfig::SUPPLIER_SUPPORT_TICKET_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addSupportTicket($data,$files){

        $url_path = ApiConfig::SUPPLIER_SUPPORT_TICKET_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function showTicketDetails($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SUPPORT_TICKET_DETAIL_URL,$data["ticket_id"]);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function replySupportTicket($data,$files){

        $url_path = ApiConfig::SUPPLIER_SUPPORT_TICKET_REPLY_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function updateTicketStatus($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SUPPORT_TICKET_STATUS_UPDATE_URL,$data["ticket_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function getTicketMessages($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SUPPORT_TICKET_MESSAGE_URL,$data["ticket_id"]);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }

    // Support Ticket Module Over.........................................

    // Order Module Start.........................................

    public function showOrders($data){

        $url_path = ApiConfig::SUPPLIER_ORDER_LISTING_URL;
        // dump($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function surpriseOrders($data){

        $url_path = ApiConfig::SUPPLIER_SURPRISE_ORDER_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function getOrderDetails($order_id){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_DETAILS_URL,$order_id);
        $params = array();
        // dd($url_path);
        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }
    
    public function surpriseOrderDetails($order_id){

        $url_path = sprintf(ApiConfig::SUPPLIER_SURPRISE_ORDER_DETAILS_URL,$order_id);
        $params = array();
        // dd($url_path);
        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }

    public function truckAssign($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_TRUCK_ASSIGN,$data["order_id"],$data["order_item_id"]);
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        
        return $response;

    }
    
    public function cpAssign($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_CP_ASSIGN,$data["order_id"],$data["order_item_id"]);
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        
        return $response;

    }
    
    public function orderAccept($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_ACCEPTS,$data["order_id"]);
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }
    
    public function surpriseOrderAccept($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SURPRISE_ORDER_ACCEPTS,$data["order_id"]);
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestPUT($url_path,$params);
        
        return $response;

    }
    
    public function report7DayCubeTest($data,$files){
        

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_7_DAY_REPORT,$data["report_7_track_id"],$data["report_7_tm_id"],$data["report_7_order_id"]);
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        
        return $response;

    }
    
    public function report28DayCubeTest($data,$files){
        

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_28_DAY_REPORT,$data["report_28_track_id"],$data["report_28_tm_id"],$data["report_28_order_id"]);
        
        // dd($url_path);
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        
        return $response;

    }
    
    public function uploadOrderBill($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_BILL_UPLOAD_URL,$data["order_id"]);
        $params = $data;
        // dd($url_path);
        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;


    }
    
    public function reassigningOrders($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_REASSIGNING_URL);
        $params = $data;
        // dd($url_path);
        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;


    }

    public function trackingDetails($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_TRACKING_URL,$data["item_id"],$data["tracking_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }
    
    public function CPtrackingDetails($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_CP_TRACKING_URL,$data["item_part_id"]);
        $params = array();
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }
    
    public function getDeliveredCP($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ORDER_GET_DELIVERED_CP_URL,$data["vendor_order_id"],$data["order_item_id"]);
        $params = array();
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }


    
    public function getAssignedOrderListByDate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_GET_ASSIGNED_ORDER_BY_DATE_RANGE_URL);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }

    // Order Module Over.........................................


    // Profile Module Start.........................................

    public function getSupplierProfile(){

        $url_path = ApiConfig::SUPPLIER_PROFILE_DETAIL_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }


    public function editSupplierProfile($data,$files){

        $url_path = ApiConfig::SUPPLIER_PROFILE_UPDATE_URL;
        $params = $data;

        // $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function changePassword($data){

        $url_path = ApiConfig::SUPPLIER_CHANGE_PASSWORD_URL;
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function updateNotiStatus($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_NOTIFICATION_STATUS_UPDATE_URL,$data['_id']);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function forgotPass($data){

        $url_path = ApiConfig::SUPPLIER_FORGOT_PASSWORD_URL;
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function requestOTP($data){
        $url_path = ApiConfig::SUPPLIER_REQUEST_OTP_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);

        return $response;

    }
    
    public function requestOTPToMaster(){
        $url_path = ApiConfig::SUPPLIER_REQUEST_OTP_TO_MASTER_URL;
        $params = array();
        
        $response = $this->requestGET($url_path,$params);

        return $response;
    }
    
    public function requestOTPToPlant($data){
        $url_path = ApiConfig::SUPPLIER_REQUEST_OTP_TO_PLANT_URL;
        $params = $data;
        
        $response = $this->requestPOST($url_path,$params);

        return $response;
    }
    
    public function requestOTPforEditProfile($data){
        $url_path = ApiConfig::SUPPLIER_REQUEST_OTP_FOR_EDIT_PROFILE_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);

        return $response;
    }

    // Profile Module Over.........................................
    // Proposal Module Start.........................................

    public function addProposal($data){

        $url_path = ApiConfig::SUPPLIER_PROPOSAL_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function showProposals(){

        $url_path = ApiConfig::SUPPLIER_PROPOSAL_LISTING_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    // Proposal Module Over.........................................

     // Dashboard Module Start.........................................

     public function getDashboardDetails(){

        $url_path = ApiConfig::SUPPLIER_DASHBOARD_DETAILS_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getMonthlyChartDetails($data){

        $url_path = ApiConfig::SUPPLIER_MONTHLY_CHART_URL;

        $is_added = 0;
        
        
        if(isset($data["year"]) && $is_added==0){
            $url_path .= "&year=".$data["year"];
            
        }       
        
        
        if(isset($data["status"]) && $is_added==0){
            $url_path .= "&status=".$data["status"];
            
        }
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    
    public function getDayChartDetails($data){

        $url_path = ApiConfig::SUPPLIER_DAILY_CHART_URL;
        $is_added=0;
        if(isset($data["year"]) && $is_added==0){
            $url_path .= "&year=".$data["year"];
            
        }
        
        if(isset($data["month"]) && $is_added==0){
            $url_path .= "&month=".$data["month"];
            
        }
        
        if(isset($data["status"]) && $is_added==0){
            $url_path .= "&status=".$data["status"];
            
        }
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getProductBaseMonthlyChartDetails($data){

        $url_path = ApiConfig::SUPPLIER_PRODUCT_BASE_MONTHLY_CHART_URL;

        $is_added = 0;
        if(isset($data["year"]) && $is_added==0){
            $url_path .= "&year=".$data["year"];
            
        }       
        
        if(isset($data["sub_category_id"]) && $is_added==0){
            $url_path .= "&sub_category_id=".$data["sub_category_id"];
            
        }
        
        if(isset($data["grade_id"]) && $is_added==0){
            $url_path .= "&grade_id=".$data["grade_id"];
            
        }
        
        if(isset($data["design_mix_id"]) && $is_added==0){
            $url_path .= "&design_mix_id=".$data["design_mix_id"];
            
        }
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getProductBaseDayChartDetails($data){

        $url_path = ApiConfig::SUPPLIER_PRODUCT_BASE_DAILY_CHART_URL;

        $is_added = 0;
        if(isset($data["year"]) && $is_added==0){
            $url_path .= "&year=".$data["year"];
            
        }       
        
        if(isset($data["month"]) && $is_added==0){
            $url_path .= "&month=".$data["month"];
            
        }

        if(isset($data["sub_category_id"]) && $is_added==0){
            $url_path .= "&sub_category_id=".$data["sub_category_id"];
            
        }

        if(isset($data["grade_id"]) && $is_added==0){
            $url_path .= "&grade_id=".$data["grade_id"];
            
        }

        if(isset($data["design_mix_id"]) && $is_added==0){
            $url_path .= "&design_mix_id=".$data["design_mix_id"];
            
        }
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getSuppliersLatesOrders(){

        $url_path = ApiConfig::SUPPLIER_LATEST_ORDERS_URL;
        
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getSuppliersQueueOrders(){

        $url_path = ApiConfig::SUPPLIER_QUEUE_ORDERS_URL;
        
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getLatestProducts(){

        $url_path = ApiConfig::SUPPLIER_LATEST_PRODUCTS_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getProductStock(){

        $url_path = ApiConfig::SUPPLIER_PRODUCT_STOCK_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getDayCapacity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_DAY_CAPACITY_URL,$data["delivery_date"]);
        $params = array();
        // dd($url_path);
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    // Dashboard Module Stop.........................................

    // Report Module Start.........................................

    public function plant_address_listing_report($data){

        $url_path = ApiConfig::SUPPLIER_PLANT_ADDRESS_LISTING_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function billing_address_listing_report($data){

        $url_path = ApiConfig::SUPPLIER_BILLING_ADDRESS_LISTING_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function TM_driver_listing_report($data){

        $url_path = ApiConfig::SUPPLIER_TM_DRIVER_LISTING_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function operator_listing_report($data){

        $url_path = ApiConfig::SUPPLIER_OPERATOR_LISTING_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function TM_listing_report($data){

        $url_path = ApiConfig::SUPPLIER_TM_LISTING_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function CP_listing_report($data){

        $url_path = ApiConfig::SUPPLIER_CP_LISTING_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function admix_stock_report($data){

        $url_path = ApiConfig::SUPPLIER_ADMIX_STOCK_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function agg_stock_report($data){

        $url_path = ApiConfig::SUPPLIER_AGGREGATE_STOCK_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function cement_stock_report($data){

        $url_path = ApiConfig::SUPPLIER_CEMENT_STOCK_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function flyAsh_stock_report($data){

        $url_path = ApiConfig::SUPPLIER_FLYASH_STOCK_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function sand_stock_report($data){

        $url_path = ApiConfig::SUPPLIER_SAND_STOCK_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function bank_report($data){

        $url_path = ApiConfig::SUPPLIER_BANK_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function design_mix_report($data){

        $url_path = ApiConfig::SUPPLIER_DESIGN_MIX_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function order_listing_report($data){

        $url_path = ApiConfig::SUPPLIER_ORDER_LISTING_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function product_listing_report($data){

        $url_path = ApiConfig::SUPPLIER_PRODUCT_LISTING_REPORT_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    // Report Module Stop.........................................



    //Driver Details Module Over.....................................................

    public function showDriverDetails($data){

        $url_path = ApiConfig::SUPPLIER_DRIVER_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addDriverDetails($data,$files){

        $url_path = ApiConfig::SUPPLIER_DRIVER_DETAILS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function editDriverDetails($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_DRIVER_DETAILS_EDIT_URL,$data["_id"]);
        $params = $data;
        
        // $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    //Driver Details Module Over.....................................................


    //operator Details Module Over.....................................................

    public function showOperatorDetails($data){

        $url_path = ApiConfig::SUPPLIER_OPERATOR_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addOperatorDetails($data,$files){

        $url_path = ApiConfig::SUPPLIER_OPERATOR_DETAILS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function editOperatorDetails($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_OPERATOR_DETAILS_EDIT_URL,$data["_id"]);
        $params = $data;
        
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    //operator Details Module Over.....................................................
    
    //Pump Gang Details Module Over.....................................................

    public function showPumpGangDetails($data){

        $url_path = ApiConfig::SUPPLIER_PUMP_GANG_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addPumpGangDetails($data,$files){

        $url_path = ApiConfig::SUPPLIER_PUMP_GANG_DETAILS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function editPumpGangDetails($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_PUMP_GANG_DETAILS_EDIT_URL,$data["_id"]);
        $params = $data;
        
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    //operator Details Module Over.....................................................

    //TM Module Over.....................................................


    public function getTM($data){

        $url_path = ApiConfig::SUPPLIER_TM_LISTING_URL;
        $params = $data;
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function showTMCategory($data){

        $url_path = ApiConfig::SUPPLIER_CATEGORY_LISTING_URL;
        $params = $data;
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function getTMSubCategoris($data){

        $url_path = ApiConfig::SUPPLIER_SUB_CATEGORY_LISTING_URL.''.$data["vehicle_cat_id"];
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }
    
    public function getTMDetail($vehicle_id){

        $url_path = sprintf(ApiConfig::SUPPLIER_TM_DETAILS_BY_ID_URL, $vehicle_id);
        $params = array();
        // dd($url_path);
        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;


    }

    public function addTM($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_TM_ADD_URL,$data["TMCategoryId"],$data["TMSubCategoryId"]);
        $params = $data;
        // dump($params);
        // dd($files);`q1
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function editTM($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_TM_EDIT_URL,$data["TMId"]);
        $params = $data;

        // $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }



    //TM Module Over.....................................................

    //Concrete pump Module Start.....................................................

    public function showConcretePump($data){

        $url_path = ApiConfig::SUPPLIER_CONCRETE_PUMP_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addConcretePump($data,$files){

        
        $url_path = sprintf(ApiConfig::SUPPLIER_CONCRETE_PUMP_DETAILS_ADD_URL,$data["ConcretePumpCategoryId"]);
        
        $params = $data;
        // dd($params);
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function editConcretePump($data,$files,$concrete_pump_id){
        
        if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
            $url_path = sprintf(ApiConfig::SUPPLIER_CONCRETE_PUMP_DETAILS_EDIT_URL,'edit/'.$concrete_pump_id);

            $params = $data;
            
            $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);

        }else{
            $url_path = sprintf(ApiConfig::SUPPLIER_CONCRETE_PUMP_DETAILS_EDIT_URL,$concrete_pump_id);

            $params = $data;

            $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        }
        
        // dd($response);
        return $response;

    }
    //Concrete pump Module Over.....................................................

    //Design Mix Module Over.....................................................

    public function getMix($data){

        $url_path = ApiConfig::SUPPLIER_DESIGN_MIX_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;


    }
    
    public function getSubvendors($data){

        $url_path = ApiConfig::SUPPLIER_SUBVENDOR_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getFillDetailsOfVendor($data){

        $url_path = ApiConfig::SUPPLIER_GET_FILL_DETAILS_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getDesignMixGradeWise($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_GET_DESIGN_MIX_GRADE_WISE_URL,$data["grade_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;


    }
    
    public function getDesignMixCombinationGradeWise($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_GET_DESIGN_MIX_COMBINATION_BY_ID_URL,$data["grade_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;


    }
    
    public function getSingleDesignMixCombinationByBrandIds($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_GET_SINGLE_DESIGN_MIX_COMBINATION_BY_ID_URL,$data["grade_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;


    }
    
    public function getDesignMixById($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_GET_DESIGN_MIX_BY_ID_URL,$data["mix_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addMix($data,$files){

        $url_path = ApiConfig::SUPPLIER_DESIGN_MIX_DETAILS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function addMixVarient($data,$files,$design_mix_id){

        $url_path = sprintf(ApiConfig::SUPPLIER_DESIGN_MIX_VARIENT_ADD_URL,$design_mix_id);
        $params = $data;
        // dd($url_path);
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function editMix($data,$files){

        $url_path = sprintf(ApiConfig::SUPPLIER_DESIGN_MIX_DETAILS_EDIT_VARIENT_URL,$data["mix_id"]);
        // dd($url_path);
        $params = $data;
// dd($params);
        $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }


    public function getMedia($data){

        $url_path = ApiConfig::SUPPLIER_MEDIA_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addPrimaryMedia($data,$files){

        $url_path = ApiConfig::SUPPLIER_PRIMARY_MEDIA_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function addSecondaryMedia($data,$files){

        $url_path = ApiConfig::SUPPLIER_SECONDARY_MEDIA_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function deleteMedia($data){

        $url_path = ApiConfig::SUPPLIER_MEDIA_DELETE_URL;
        $params = $data;
        // dd($params);

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }

    //Design Mix Module Over.....................................................


    //Rate Module Module Start.....................................................

    public function showCementRate($data){

        $url_path = ApiConfig::SUPPLIER_CEMENT_RATE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addCementRate($data){

        $url_path = ApiConfig::SUPPLIER_CEMENT_RATE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editCementRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CEMENT_RATE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteCementRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CEMENT_RATE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showSandRate($data){

        $url_path = ApiConfig::SUPPLIER_SAND_RATE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addSandRate($data){

        $url_path = ApiConfig::SUPPLIER_SAND_RATE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editSandRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SAND_RATE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteSandRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SAND_RATE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    
    public function showAggregateRate($data){

        $url_path = ApiConfig::SUPPLIER_AGGREGATE_RATE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addAggregateRate($data){

        $url_path = ApiConfig::SUPPLIER_AGGREGATE_RATE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editAggregateRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_AGGREGATE_RATE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteAggregateRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_AGGREGATE_RATE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showFlyashRate($data){

        $url_path = ApiConfig::SUPPLIER_FLYASH_RATE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addFlyashRate($data){

        $url_path = ApiConfig::SUPPLIER_FLYASH_RATE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editFlyashRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_FLYASH_RATE_EDIT_URL,$data["_id"]);
        $params = $data;
        
        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteFlyashRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_FLYASH_RATE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showAdmixtureRate($data){

        $url_path = ApiConfig::SUPPLIER_ADMIXTURE_RATE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addAdmixtureRate($data){

        $url_path = ApiConfig::SUPPLIER_ADMIXTURE_RATE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editAdmixtureRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ADMIXTURE_RATE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteAdmixtureRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ADMIXTURE_RATE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showWaterRate($data){

        $url_path = ApiConfig::SUPPLIER_WATER_RATE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addWaterRate($data){

        $url_path = ApiConfig::SUPPLIER_WATER_RATE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editWaterRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_WATER_RATE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteWaterRate($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_WATER_RATE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    //Rate Module Module Over.....................................................
    
    
    
    //Profit Overhead Margin Module Module Start.....................................................

    public function showProfitMargin($data){

        $url_path = ApiConfig::SUPPLIER_PROFIT_MARGIN_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addProfitMargin($data){

        $url_path = ApiConfig::SUPPLIER_PROFIT_MARGIN_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editProfitMargin($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_PROFIT_MARGIN_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteProfitMargin($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_PROFIT_MARGIN_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showOverheadMargin($data){

        $url_path = ApiConfig::SUPPLIER_OVERHEAD_MARGIN_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addOverheadMargin($data){

        $url_path = ApiConfig::SUPPLIER_OVERHEAD_MARGIN_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editOverheadMargin($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_OVERHEAD_MARGIN_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteOverheadMargin($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_OVERHEAD_MARGIN_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    //Stock Module Module Start.....................................................

    public function showCementStock($data){

        $url_path = ApiConfig::SUPPLIER_CEMENT_STOCK_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addCementStock($data){

        $url_path = ApiConfig::SUPPLIER_CEMENT_STOCK_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editCementStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CEMENT_STOCK_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteCementStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CEMENT_STOCK_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showSandStock($data){

        $url_path = ApiConfig::SUPPLIER_SAND_STOCK_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addSandStock($data){

        $url_path = ApiConfig::SUPPLIER_SAND_STOCK_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editSandStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SAND_STOCK_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteSandStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SAND_STOCK_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    
    public function showAggregateStock($data){

        $url_path = ApiConfig::SUPPLIER_AGGREGATE_STOCK_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addAggregateStock($data){

        $url_path = ApiConfig::SUPPLIER_AGGREGATE_STOCK_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editAggregateStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_AGGREGATE_STOCK_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteAggregateStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_AGGREGATE_STOCK_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showFlyashStock($data){

        $url_path = ApiConfig::SUPPLIER_FLYASH_STOCK_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addFlyashStock($data){

        $url_path = ApiConfig::SUPPLIER_FLYASH_STOCK_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editFlyashStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_FLYASH_STOCK_EDIT_URL,$data["_id"]);
        $params = $data;
        
        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteFlyashStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_FLYASH_STOCK_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showAdmixtureStock($data){

        $url_path = ApiConfig::SUPPLIER_ADMIXTURE_STOCK_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }
    
   


    public function addAdmixtureStock($data){

        $url_path = ApiConfig::SUPPLIER_ADMIXTURE_STOCK_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
   
    
    public function editAdmixtureStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ADMIXTURE_STOCK_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    
    
    public function deleteAdmixtureStock($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ADMIXTURE_STOCK_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    
    //Stock Module Module Over.....................................................
    //Setting Module Module Over.....................................................

    public function addSettings($data){

        $url_path = ApiConfig::SUPPLIER_SETTING_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    
    public function getSettings($data){

        $url_path = ApiConfig::SUPPLIER_SETTING_GET_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }
    
    public function getAdmixSettings($data){

        $url_path = ApiConfig::SUPPLIER_ADMIX_SETTING_GET_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }
    
    

    public function editSettings($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_SETTING_UPDATE_URL);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function addAdmixSettings($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_ADMIX_SETTING_ADD_URL);
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editAdmixSettings($data,$id){

        $url_path = sprintf(ApiConfig::SUPPLIER_ADMIX_SETTING_UPDATE_URL,$id);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    //Setting Module Module Over.....................................................
    //TM Un Availability Module Module Start.....................................................


    public function getTMunAvailabiity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_TM_UNAVAILABILITY_GET_URL,$data["_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addTMunAvailabiity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_TM_UNAVAILABILITY_ADD_URL,$data["TM_unavail_id"]);
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editTMunAvailabiity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_TM_UNAVAILABILITY_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteTMunAvailabiity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_TM_UNAVAILABILITY_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }


    public function deleteCPunAvailabiity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CP_UNAVAILABILITY_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }


    //TM Un Availability Module Module Over.....................................................
    
    //CP Un Availability Module Module Start.....................................................


    public function getCPunAvailabiity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CP_UNAVAILABILITY_GET_URL,$data["_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addCPunAvailabiity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CP_UNAVAILABILITY_ADD_URL,$data["CP_unavail_id"]);
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editCPunAvailabiity($data){

        $url_path = sprintf(ApiConfig::SUPPLIER_CP_UNAVAILABILITY_EDIT_URL,$data["CP_unavail_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }


    //TM Un Availability Module Module Over.....................................................

}
