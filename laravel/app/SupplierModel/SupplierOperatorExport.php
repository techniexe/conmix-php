<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SupplierOperatorExport extends Model implements FromArray,WithHeadings
{
    //
    protected $buyer_order;

    public function __construct(array $buyer_order)
    {
        $this->buyer_order = $buyer_order;
    }

    public function array(): array
    {
        return $this->buyer_order;
    }

    public function headings(): array
    {

        return [

            'Operator Full Name',
            'Mobile No.',
            'Alternate No.',
            'Whatsapp No.',
            'Plant Address For This CP Operator',
            'Created At',

        ];

    }
}
