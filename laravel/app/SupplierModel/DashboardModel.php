<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class DashboardModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function getDashboardDetails(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getDashboardDetails($data);
        // dd($response);
        return $response;

    }
    
    public function getMonthlyChartDetails(){

        $data = array();

        if(Request::get('year')){
            $data['year'] = Request::get('year');
        }
        
        if(Request::get('status')){
            $data['status'] = Request::get('status');
        }
        
        // dd($data);
        $response = $this->api_model->getMonthlyChartDetails($data);
        // dd($response);
        return $response;

    }
    
    
    public function getDayChartDetails(){

        $data = array();

        if(Request::get('year')){
            $data['year'] = Request::get('year');
        }
        
        if(Request::get('month')){
            $data['month'] = Request::get('month');
        }
        
        if(Request::get('status')){
            $data['status'] = Request::get('status');
        }
        
        // dd($data);
        $response = $this->api_model->getDayChartDetails($data);
        // dd($response);
        return $response;

    }
    
    public function getProductBaseMonthlyChartDetails(){

        $data = array();

        if(Request::get('year')){
            $data['year'] = Request::get('year');
        }
        
        if(Request::get('sub_category_id')){
            $data['sub_category_id'] = Request::get('sub_category_id');
        }
        
        if(Request::get('concrete_grade_id')){
            $data['grade_id'] = Request::get('concrete_grade_id');
        }
        
        if(Request::get('design_mix_type')){
            if(Request::get('design_mix_type') == 'custom'){
                $data['design_mix_id'] = 'false';
            }else if(Request::get('design_mix_type') == 'standard'){
                $data['design_mix_id'] = 'true';
            }
            
        }
        
        // dd($data);
        $response = $this->api_model->getProductBaseMonthlyChartDetails($data);
        // dd($response);
        return $response;

    }
    
    public function getProductBaseDayChartDetails(){

        $data = array();

        if(Request::get('year')){
            $data['year'] = Request::get('year');
        }
        
        if(Request::get('month')){
            $data['month'] = Request::get('month');
        }
        
        if(Request::get('sub_category_id')){
            $data['sub_category_id'] = Request::get('sub_category_id');
        }

        if(Request::get('concrete_grade_id')){
            $data['grade_id'] = Request::get('concrete_grade_id');
        }

        if(Request::get('design_mix_type')){
            if(Request::get('design_mix_type') == 'custom'){
                $data['design_mix_id'] = 'false';
            }else if(Request::get('design_mix_type') == 'standard'){
                $data['design_mix_id'] = 'true';
            }
            
        }
        
        // dd($data);
        $response = $this->api_model->getProductBaseDayChartDetails($data);
        // dd($response);
        return $response;

    }
    
    
    public function getSuppliersLatesOrders(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getSuppliersLatesOrders($data);
        // dd($response);
        return $response;

    }
    
    public function getSuppliersQueueOrders(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getSuppliersQueueOrders($data);
        // dd($response);
        return $response;

    }
    
    public function getLatestProducts(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getLatestProducts($data);
        // dd($response);
        return $response;

    }
    
    public function getProductStock(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getProductStock($data);
        // dd($response);
        return $response;

    }
    
    public function getDayCapacity(){

        // dd(Request::get("date"));
        $date = date("Y-m-d", strtotime(Request::get("date")));
        // dd($date);
        $data = array(
            "delivery_date"=>$date
        );
        
        // dd($data);
        $response = $this->api_model->getDayCapacity($data);
        // dd($response);
        return $response;

    }
}
