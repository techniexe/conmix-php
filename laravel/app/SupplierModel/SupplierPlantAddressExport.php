<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SupplierPlantAddressExport extends Model implements FromArray,WithHeadings
{
    //
    protected $buyer_order;

    public function __construct(array $buyer_order)
    {
        $this->buyer_order = $buyer_order;
    }

    public function array(): array
    {
        return $this->buyer_order;
    }

    public function headings(): array
    {

        return [

            'Plant Name (Pickup Address)',
            'Address Line 1',
            'Plant Manager Full name',
            'Contact No',
            'Email',
            'Plant capacity per hour (Cu.Mtr)',
            'No. of operation hours',
            'State Name',
            'City Name',
            'Billing Address For This Plant',
            'Created At',

        ];

    }
}
