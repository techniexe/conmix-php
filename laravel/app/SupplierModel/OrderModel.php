<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class OrderModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showOrders(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_status")){
            $data["order_status"] = Request::get("order_status");
        }
        
        if(Request::get("payment_status")){
            $data["payment_status"] = Request::get("payment_status");
        }

        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }      
        
        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("order_id")){
            $data["order_id"] = Request::get("order_id");
        }
        
        if(Request::get("buyer_id")){
            $data["buyer_order_id"] = Request::get("buyer_id");
        }
        
        if(Request::get("buyer_name")){
            $data["buyer_name"] = Request::get("buyer_name");
        }
        
        if(Request::get("year")){
            $data["year"] = Request::get("year");
        }
        
        if(Request::get("date")){

            $date = date('Y-m-d', strtotime(Request::post("date"))).'T00:00:00.000Z';

            $data["date"] = $date;
        }
        
        if(Request::get("month")){
            $month = date("M", strtotime(Request::get("month")));
            $year = date("Y", strtotime(Request::get("month")));          
            

            $data["month"] = $month;
            $data["year"] = $year;
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showOrders($data);
        // dd($response);
        return $response;

    }
    
    public function surpriseOrders(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_status")){
            $data["order_status"] = Request::get("order_status");
        }
        
        if(Request::get("payment_status")){
            $data["payment_status"] = Request::get("payment_status");
        }

        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }       

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }     
        

        if(Request::get("order_id")){
            $data["order_id"] = Request::get("order_id");
        }
        
        if(Request::get("buyer_id")){
            $data["buyer_id"] = Request::get("buyer_id");
        }
        
        if(Request::get("year")){
            $data["year"] = Request::get("year");
        }
        
        if(Request::get("date")){

            $date = date('Y-m-d', strtotime(Request::post("date"))).'T00:00:00.000Z';

            $data["date"] = $date;
        }
        
        if(Request::get("month")){
            $month = date("M", strtotime(Request::get("month")));
            $year = date("Y", strtotime(Request::get("month")));          
            

            $data["month"] = $month;
            $data["year"] = $year;
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->surpriseOrders($data);
        // dd($response);
        return $response;

    }

    public function orderDetails($order_id){

        $data = array();

       // dd($data);
        $response = $this->api_model->getOrderDetails($order_id);
        // dd($response);
        return $response;

    }
    
    public function surpriseOrderDetails($order_id){

        $data = array();

       // dd($data);
        $response = $this->api_model->surpriseOrderDetails($order_id);
        // dd($response);
        return $response;

    }

    public function truckAssign(){
        // dd(Request::all());
        $data = array(
            "TM_id" => Request::get("vehicle_id"),
            "pickup_quantity" => Request::get("delivered_quantity"),
            "assigned_at" => date("c", strtotime(Request::get("assigned_at")." 12:00 AM")),
            "order_id" => Request::get("order_id"),
            "order_item_id" => Request::get("order_item_id"),
            "order_item_part_id" => Request::get("order_item_part_id"),
            "start_time" => date("Y-m-d", strtotime(Request::get("assigned_at")))."T".Request::get("assigned_at_start_time").":00.000+00:00",
            "end_time" => date("Y-m-d", strtotime(Request::get("assigned_at")))."T".Request::get("assigned_at_end_time").":00.000+00:00",
        );


        // dd($data);
        $response = $this->api_model->truckAssign($data);
        // dd($response);
        return $response;


    }
    
    public function cpAssign(){
        // dd(Request::all());

        $is_already_delivered = 'false';

        if(Request::get("is_already_delivered") != null){

            if(Request::get("is_already_delivered") == 'on'){
                $is_already_delivered = 'true';
            }


        }

        $data = array(
            "CP_id" => Request::get("cp_id"),
            "assigned_at" => date("c", strtotime(Request::get("assigned_at")." 12:00 AM")),
            "order_id" => Request::get("order_id"),
            "order_item_id" => Request::get("order_item_id"),
            "order_item_part_id" => Request::get("order_item_part_id"),
            "start_time" => date("Y-m-d", strtotime(Request::get("assigned_at")))."T".Request::get("assigned_at_start_time").":00.000+00:00",
            "end_time" => date("Y-m-d", strtotime(Request::get("assigned_at")))."T".Request::get("assigned_at_end_time").":00.000+00:00",
            "is_already_delivered" => $is_already_delivered
        );


        // dd($data);
        $response = $this->api_model->cpAssign($data);
        // dd($response);
        return $response;


    }
    
    public function orderAccept(){

        $data = array(
            "order_id" => Request::get("order_id"),
            "order_status" => Request::get("order_status")
        );

        if(Request::get("authentication_code")){
            $data["authentication_code"] = Request::get("authentication_code");
        }


        // dd($data);
        $response = $this->api_model->orderAccept($data);
        // dd($response);
        return $response;

    }

    public function surpriseOrderAccept(){

        $data = array(
            "order_id" => Request::get("order_id"),
            "order_status" => Request::get("order_status")
        );

        if(Request::get("authentication_code")){
            $data["authentication_code"] = Request::get("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->surpriseOrderAccept($data);
        // dd($response);
        return $response;


    }
    
    public function report7DayCubeTest(){

        $data = array(
            "report_7_track_id" => Request::post("report_7_track_id"),
            "report_7_tm_id" => Request::post("report_7_tm_id"),
            "report_7_order_id" => Request::post("report_7_order_id")
        );

        $files = array();

        if(Request::file('pic_7_report')){

            $image = Request::file('pic_7_report');       

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image_mime" => $image->getClientMimeType(),
                "image" => $image,
                "param_key" => "qube_test_report_7days"
            ));

            
        }

        

        // dd($data);
        $response = $this->api_model->report7DayCubeTest($data,$files);
        // dd($response);
        return $response;


    }
    
    public function report28DayCubeTest(){

        $data = array(
            "report_28_track_id" => Request::post("report_28_track_id"),
            "report_28_tm_id" => Request::post("report_28_tm_id"),
            "report_28_order_id" => Request::post("report_28_order_id")
        );

        $files = array();

        if(Request::file('pic_28_report')){

            $image = Request::file('pic_28_report');       

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image_mime" => $image->getClientMimeType(),
                "image" => $image,
                "param_key" => "qube_test_report_28days"
            ));

            
        }

        

        // dd($data);
        $response = $this->api_model->report28DayCubeTest($data,$files);
        // dd($response);
        return $response;


    }

    public function uploadOrderBill(){

        $data = array(
            "order_id" => "".Request::post('order_id')
        );

        $files = array();

        if(Request::file('bill_image')){

            $image = Request::file('bill_image');                

            if(Request::post('bill_type') == 'invoice'){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "bill_image"
                ));

            }else if(Request::post('bill_type') == 'credit_notes'){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "creditNote_image"
                ));

            }else if(Request::post('bill_type') == 'debit_notes'){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "debitNote_image"
                ));

            }
                       

        }

       // dd($data);
        $response = $this->api_model->uploadOrderBill($data,$files);
        // dd($response);
        return $response;

    }
    
    public function reassigningOrders(){

        $data = array(
            "order_reassign_reason" => "".Request::post('order_reassign_reason'),
            "order_reassign_desc" => "".Request::post('order_reassign_desc'),
            "authentication_code" => "".Request::post('authentication_code'),
        );

        

        if(Request::post('vendor_order_id')){
            $vendor_order_id = Request::post('vendor_order_id');
            $vendor_id = Request::post('vendor_id');
            $buyer_id = Request::post('buyer_id');
            $order_item_part_id = Request::post('order_item_part_id');
            $buyer_order_id = Request::post('buyer_order_id');
            $buyer_order_item_id = Request::post('buyer_order_item_id');
            $previous_order_delivery_start_date = Request::post('previous_order_delivery_start_date');
            $previous_order_delivery_end_date = Request::post('previous_order_delivery_end_date');
            
            
            
            $new_order_delivery_start_date = Request::post('new_order_delivery_start_date');
            $new_order_delivery_end_date = Request::post('new_order_delivery_end_date');
            $cement_brand_id = Request::post('cement_brand_id');
            $cement_grade_id = Request::post('cement_grade_id');
            $sand_source_id = Request::post('sand_source_id');
            $sand_zone_id = Request::post('sand_zone_id');
            $aggr_source_id = Request::post('aggr_source_id');
            $aggr_cat1_id = Request::post('aggr_cat1_id');
            $aggr_cat2_id = Request::post('aggr_cat2_id');
            $admix_brand_id = Request::post('admix_brand_id');
            $admix_cat_id = Request::post('admix_cat_id');
            $flyash_source_id = Request::post('flyash_source_id');

            $final_order_list = array();

            for($i=0;$i<count($vendor_order_id);$i++){                
                
                if($vendor_order_id[$i] != null){

                    $previous_order_delivery_start_date_time = date('Y-m-d',strtotime($previous_order_delivery_start_date[$i])).'T'.date('H:i',strtotime($previous_order_delivery_start_date[$i])).':00+00:00';
                    $previous_order_delivery_end_date_time = date('Y-m-d',strtotime($previous_order_delivery_end_date[$i])).'T'.date('H:i',strtotime($previous_order_delivery_end_date[$i])).':00+00:00';
                    
                    $new_order_delivery_start_date_time = date('Y-m-d',strtotime($new_order_delivery_start_date[$i])).'T'.date('H:i',strtotime($new_order_delivery_start_date[$i])).':00+00:00';
                    $new_order_delivery_end_date_time = date('Y-m-d',strtotime($new_order_delivery_end_date[$i])).'T'.date('H:i',strtotime($new_order_delivery_end_date[$i])).':00+00:00';

                    if(Request::post('order_reassign_reason') == 'material_out_of_stock'){
                        $orders = array(

                            'vendor_order_id' => $vendor_order_id[$i],
                            'vendor_id' => $vendor_id[$i],
                            'buyer_id' => $buyer_id[$i],
                            'order_item_part_id' => $order_item_part_id[$i],
                            'buyer_order_id' => $buyer_order_id[$i],
                            'order_item_id' => $buyer_order_item_id[$i],
                            'previous_start_time' => $previous_order_delivery_start_date_time,
                            'previous_end_time' => $previous_order_delivery_end_date_time,
                            'new_start_time' => $new_order_delivery_start_date_time,
                            'new_end_time' => $new_order_delivery_end_date_time,
                            'reassign_reason' => Request::post('order_reassign_reason'),
                            'reassign_desc' => Request::post('order_reassign_desc'),
                            'cement_brand_id' => $cement_brand_id[$i],
                            'cement_grade_id' => $cement_grade_id[$i],
                            'sand_source_id' => $sand_source_id[$i],
                            'sand_zone_id' => $sand_zone_id[$i],
                            'aggr_source_id' => $aggr_source_id[$i],
                            'aggr_cat1_id' => $aggr_cat1_id[$i],
                            'aggr_cat2_id' => $aggr_cat2_id[$i],
                            'admix_brand_id' => $admix_brand_id[$i],
                            'admix_cat_id' => $admix_cat_id[$i],
                            'flyash_source_id' => $flyash_source_id[$i],

                        );
                    }else if(Request::post('order_reassign_reason') == 'breakdown'){

                        $orders = array(

                            'vendor_order_id' => $vendor_order_id[$i],
                            'vendor_id' => $vendor_id[$i],
                            'buyer_id' => $buyer_id[$i],
                            'order_item_part_id' => $order_item_part_id[$i],
                            'buyer_order_id' => $buyer_order_id[$i],
                            'order_item_id' => $buyer_order_item_id[$i],
                            'previous_start_time' => $previous_order_delivery_start_date[$i],
                            'previous_end_time' => $previous_order_delivery_end_date[$i],
                            'new_start_time' => $new_order_delivery_start_date[$i],
                            'new_end_time' => $new_order_delivery_end_date[$i],
                            'reassign_reason' => Request::post('order_reassign_reason'),
                            'reassign_desc' => Request::post('order_reassign_desc'),

                        );

                    }

                    array_push($final_order_list,$orders);

                }

            }

            $data['reassigned_orders'] = json_encode($final_order_list);
            // $data['reassigned_orders'] = ($final_order_list);
        }

        else if(Request::post('order_reassign_reason') == 'order_rejected'){

            $new_order_delivery_start_date = date('Y-m-d',strtotime(Request::post("new_order_delivery_start_date"))).'T'.date('H:i',strtotime(Request::post("new_order_delivery_start_date"))).':00+00:00';
            $new_order_delivery_end_date = date('Y-m-d',strtotime(Request::post("new_order_delivery_end_date"))).'T'.date('H:i',strtotime(Request::post("new_order_delivery_end_date"))).':00+00:00';

            $data = array(
                "order_reassign_reason" => "".Request::post('order_reassign_reason'),
                "order_reassign_desc" => "".Request::post('order_reassign_desc'),
                "authentication_code" => "".Request::post('authentication_code'),

                'order_item_track_id' => Request::post('order_item_track_id'),                         
                'new_start_time' => $new_order_delivery_start_date,                         
                'new_end_time' => $new_order_delivery_end_date,                         
                'reassign_reason' => Request::post('order_reassign_reason'),
                'reassign_desc' => Request::post('order_reassign_desc'),
                'reassigned_orders' => Request::post('order_reassign_desc'),

            );

        }

        

    //    dd($data);
        $response = $this->api_model->reassigningOrders($data);
        // dd($response);
        return $response;

    }

    public function trackingDetails($item_id,$tracking_id){

        $data = array(
            "item_id" => $item_id,
            "tracking_id" => $tracking_id
        );      
        

        $response = $this->api_model->trackingDetails($data);
        // dd($response);
        return $response;

    }
    
    public function CPtrackingDetails($item_id){

        $data = array(
            "item_part_id" => $item_id
        );      
        

        $response = $this->api_model->CPtrackingDetails($data);
        // dd($response);
        return $response;

    }
    
    public function getDeliveredCP($vendor_order_id,$order_item_id){

        $data = array(
            "vendor_order_id" => $vendor_order_id,
            "order_item_id" => $order_item_id,
        );      
        

        $response = $this->api_model->getDeliveredCP($data);
        // dd($response);
        return $response;

    }
    
    public function getAssignedOrderListByDate(){

        $data = array(
            "vendor_id" => Request::get('vendor_id'),
            "start_date" => Request::get('start_date'),
            "end_date" => Request::get('end_date'),
        );      
        

        $response = $this->api_model->getAssignedOrderListByDate($data);
        // dd($response);
        return $response;

    }
}
