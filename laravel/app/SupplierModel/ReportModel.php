<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ReportModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();

        $month_array = array(

            "01" => 0,
            "02" => 1,
            "03" => 2,
            "04" => 3,
            "05" => 4,
            "06" => 5,
            "07" => 6,
            "08" => 7,
            "09" => 8,
            "10" => 9,
            "11" => 10,
            "12" => 11,
        
        );

        $all_month_array = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    }

    public function plant_address_listing_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("city_id")){
            $data["city_id"] = Request::get("city_id");
        }
        
        if(Request::get("selected_month_year")){
            
            $selected_month_year = Request::get("selected_month_year");

            $selected_month_year_array = explode(" ",$selected_month_year);

            $month = $selected_month_year_array[0];
            $year = $selected_month_year_array[1];

            if($month){
                $data["month"] = $month;
            }
            
            if($year){
                $data["year"] = $year;
            }

        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->plant_address_listing_report($data);  

        return $data;
    }
    
    public function billing_address_listing_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("city_id")){
            $data["city_id"] = Request::get("city_id");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->billing_address_listing_report($data);  

        return $data;
    }
    
    public function TM_driver_listing_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["sub_vendor_id"] = Request::get("address_id");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->TM_driver_listing_report($data);  

        return $data;
    }
    
    public function operator_listing_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["sub_vendor_id"] = Request::get("address_id");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->operator_listing_report($data);  

        return $data;
    }
    
    
    public function TM_listing_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("TM_category_id")){
            $data["TM_category_id"] = Request::get("TM_category_id");
        }
    
        if(Request::get("TM_sub_category_id")){
            $data["TM_sub_category_id"] = Request::get("TM_sub_category_id");
        }
        
        if(Request::get("TM_rc_number")){
            $data["TM_rc_number"] = Request::get("TM_rc_number");
        }
        
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
    
        if(Request::get("is_gps_enabled")){
            $data["is_gps_enabled"] = Request::get("is_gps_enabled");
        }
        
        if(Request::get("is_insurance_active")){
            $data["is_insurance_active"] = Request::get("is_insurance_active");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }   

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->TM_listing_report($data);  

        return $data;
    }
    
    public function CP_listing_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("concrete_pump_category_id")){
            $data["concrete_pump_category_id"] = Request::get("concrete_pump_category_id");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->CP_listing_report($data);  

        return $data;
    }
    
    public function admix_stock_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("brand_id")){
            $data["brand_id"] = Request::get("brand_id");
        }
        
        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->admix_stock_report($data);  

        return $data;
    }
    
    public function agg_stock_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }
        
        if(Request::get("sub_cat_id")){
            $data["sub_cat_id"] = Request::get("sub_cat_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->agg_stock_report($data);  

        return $data;
    }
    
    public function cement_stock_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("brand_id")){
            $data["brand_id"] = Request::get("brand_id");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->cement_stock_report($data);  

        return $data;
    }
    
    public function flyAsh_stock_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->flyAsh_stock_report($data);  

        return $data;
    }
    
    public function sand_stock_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->sand_stock_report($data);  

        return $data;
    }
    
    public function bank_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->bank_report($data);  

        return $data;
    }
    
    public function design_mix_report(){

        $data = array();
        // dd(Request::all());
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }

        if(Request::get("cement_brand_id")){
            $data["cement_brand_id"] = Request::get("cement_brand_id");
        }
        
        if(Request::get("sand_source_id")){
            $data["sand_source_id"] = Request::get("sand_source_id");
        }
        
        if(Request::get("aggregate_source_id")){
            $data["aggregate_source_id"] = Request::get("aggregate_source_id");
        }
        
        if(Request::get("aggregate1_sub_category_id")){
            $data["aggregate1_sub_category_id"] = Request::get("aggregate1_sub_category_id");
        }
        
        if(Request::get("aggregate2_sub_category_id")){
            $data["aggregate2_sub_category_id"] = Request::get("aggregate2_sub_category_id");
        }
        
        if(Request::get("fly_ash_source_id")){
            $data["fly_ash_source_id"] = Request::get("fly_ash_source_id");
        }
        
        if(Request::get("ad_mixture_brand_id")){
            $data["ad_mixture_brand_id"] = Request::get("ad_mixture_brand_id");
        }
        
        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("cement_grade_id")){
            $data["cement_grade_id"] = Request::get("cement_grade_id");
        }

        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->design_mix_report($data);  

        return $data;
    }
    
    
    public function order_listing_report(){

        $data = array();

        // if(Request::get("logistic_id")){
        //     $data["logistic_id"] = Request::get("logistic_id");
        // }
        
        // if(Request::get("order_status")){
        //     $data["status"] = Request::get("order_status");
        // }
        
        // if(Request::get("selected_month_year")){
            
        //     $selected_month_year = Request::get("selected_month_year");

        //     $selected_month_year_array = explode(" ",$selected_month_year);

        //     $month = $selected_month_year_array[0];
        //     $year = $selected_month_year_array[1];

        //     if($month){
        //         $data["month"] = $month;
        //     }
            
        //     if($year){
        //         $data["year"] = $year;
        //     }

        // }

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_status")){
            $data["status"] = Request::get("order_status");
        }
        
        if(Request::get("payment_status")){
            $data["payment_status"] = Request::get("payment_status");
        }

        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }      
        
        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("order_id")){
            $data["order_id"] = Request::get("order_id");
        }
        
        if(Request::get("buyer_id")){
            $data["buyer_order_id"] = Request::get("buyer_id");
        }
        
        if(Request::get("buyer_name")){
            $data["buyer_name"] = Request::get("buyer_name");
        }
        
        if(Request::get("year")){
            $data["year"] = Request::get("year");
        }
        
        if(Request::get("date")){

            $date = date('Y-m-d', strtotime(Request::post("date"))).'T00:00:00.000Z';

            $data["date"] = $date;
        }
        
        if(Request::get("month")){
            $month = date("M", strtotime(Request::get("month")));
            $year = date("Y", strtotime(Request::get("month")));          
            

            $data["month"] = $month;
            $data["year"] = $year;
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->order_listing_report($data);  

        return $data;
    }
    
    public function product_listing_report(){

        $data = array();

        if(Request::get("categoryId")){
            $data["product_category_id"] = Request::get("categoryId");
        }
        
        if(Request::get("order_status")){
            $data["status"] = Request::get("order_status");
        }
        

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->product_listing_report($data);  

        return $data;
    }
}
