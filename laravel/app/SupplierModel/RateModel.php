<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class RateModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showCementRate(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("brand_id")){
            $data["brand_id"] = Request::get("brand_id");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("per_kg_rate_value") && Request::get("per_kg_rate")){
            $data["per_kg_rate_value"] = Request::get("per_kg_rate_value");
            $data["per_kg_rate"] = Request::get("per_kg_rate");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showCementRate($data);
        // dd($response);
        return $response;


    }

    public function addCementRate(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "brand_id" => Request::post("brand_id"),
            "grade_id" => Request::post("grade_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        

        // dd($data);
        $response = $this->api_model->addCementRate($data);

        return $response;


    }

    public function editCementRate(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "brand_id" => Request::post("brand_id"),
            "grade_id" => Request::post("grade_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->editCementRate($data);

        return $response;


    }
    
    public function deleteCementRate(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        

        // dd($data);
        $response = $this->api_model->deleteCementRate($data);

        return $response;


    }
    
    
    public function showSandRate(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }
        
        if(Request::get("per_kg_rate_value") && Request::get("per_kg_rate")){
            $data["per_kg_rate_value"] = Request::get("per_kg_rate_value");
            $data["per_kg_rate"] = Request::get("per_kg_rate");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showSandRate($data);
        // dd($response);
        return $response;


    }

    public function addSandRate(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "sand_zone_id" => Request::post("sand_zone_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addSandRate($data);

        return $response;


    }

    public function editSandRate(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "sand_zone_id" => Request::post("sand_zone_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->editSandRate($data);

        return $response;


    }
    
    public function deleteSandRate(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        

        // dd($data);
        $response = $this->api_model->deleteSandRate($data);

        return $response;


    }
    
    
    public function showAggregateRate(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }


        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }
        
        if(Request::get("sub_cat_id")){
            $data["sub_cat_id"] = Request::get("sub_cat_id");
        }
        
        if(Request::get("per_kg_rate_value") && Request::get("per_kg_rate")){
            $data["per_kg_rate_value"] = Request::get("per_kg_rate_value");
            $data["per_kg_rate"] = Request::get("per_kg_rate");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showAggregateRate($data);
        // dd($response);
        return $response;


    }

    public function addAggregateRate(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "sub_cat_id" => Request::post("sub_cat_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addAggregateRate($data);

        return $response;


    }

    public function editAggregateRate(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "sub_cat_id" => Request::post("sub_cat_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->editAggregateRate($data);

        return $response;


    }
    
    public function deleteAggregateRate(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        

        // dd($data);
        $response = $this->api_model->deleteAggregateRate($data);

        return $response;


    }
    
    public function showFlyashRate(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }
        
        if(Request::get("per_kg_rate_value") && Request::get("per_kg_rate")){
            $data["per_kg_rate_value"] = Request::get("per_kg_rate_value");
            $data["per_kg_rate"] = Request::get("per_kg_rate");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showFlyashRate($data);
        // dd($response);
        return $response;


    }

    public function addFlyashRate(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addFlyashRate($data);

        return $response;


    }

    public function editFlyashRate(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->editFlyashRate($data);

        return $response;


    }
    
    public function deleteFlyashRate(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        

        // dd($data);
        $response = $this->api_model->deleteFlyashRate($data);

        return $response;


    }
    
    public function showAdmixtureRate(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("brand_id")){
            $data["brand_id"] = Request::get("brand_id");
        }
        
        if(Request::get("category_id")){
            $data["category_id"] = Request::get("category_id");
        }
        
        if(Request::get("per_kg_rate_value") && Request::get("per_kg_rate")){
            $data["per_kg_rate_value"] = Request::get("per_kg_rate_value");
            $data["per_kg_rate"] = Request::get("per_kg_rate");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showAdmixtureRate($data);
        // dd($response);
        return $response;


    }

    public function addAdmixtureRate(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "brand_id" => Request::post("brand_id"),
            "category_id" => Request::post("ad_mixture_category_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addAdmixtureRate($data);

        return $response;


    }

    public function editAdmixtureRate(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "brand_id" => Request::post("brand_id"),
            "category_id" => Request::post("ad_mixture_category_id"),
            "per_kg_rate" => Request::post("per_kg_rate"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->editAdmixtureRate($data);

        return $response;


    }
    
    public function deleteAdmixtureRate(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        

        // dd($data);
        $response = $this->api_model->deleteAdmixtureRate($data);

        return $response;


    }
    
    public function showWaterRate(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("per_kg_rate_value") && Request::get("per_kg_rate")){
            $data["per_ltr_rate_value"] = Request::get("per_kg_rate_value");
            $data["per_ltr_rate"] = Request::get("per_kg_rate");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showWaterRate($data);
        // dd($response);
        return $response;


    }

    public function addWaterRate(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "water_type" => Request::post("water_type"),
            "per_ltr_rate" => Request::post("per_kg_rate"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addWaterRate($data);

        return $response;


    }

    public function editWaterRate(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "water_type" => Request::post("water_type"),
            "per_ltr_rate" => Request::post("per_kg_rate"),
            "_id" => Request::post("rate_id"),
            
            
        );   
        
        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->editWaterRate($data);

        return $response;


    }
    
    public function deleteWaterRate(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );              

        // dd($data);
        $response = $this->api_model->deleteWaterRate($data);

        return $response;


    }
}
