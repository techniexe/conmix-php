<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class VehicleModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showTM(){

        $data = array();

        if(Request::get("TM_category_id")){
            $data["TM_category_id"] = Request::get("TM_category_id");
        }
    
        if(Request::get("TM_sub_category_id")){
            $data["TM_sub_category_id"] = Request::get("TM_sub_category_id");
        }
        
        if(Request::get("TM_rc_number")){
            $data["TM_rc_number"] = Request::get("TM_rc_number");
        }
        
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
    
        if(Request::get("is_gps_enabled")){
            $data["is_gps_enabled"] = Request::get("is_gps_enabled");
        }
        
        if(Request::get("is_insurance_active")){
            $data["is_insurance_active"] = Request::get("is_insurance_active");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }   
    
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->getTM($data);

        return $response;

    }

    public function showTMCategory(){
        $data = array();

        $response = $this->api_model->showTMCategory($data);

        return $response;
    }

    public function getTMSubCategoris(){

        $data = array();

        if(Request::post("vehicle_cat_id")){
            $data["vehicle_cat_id"] = Request::post("vehicle_cat_id");
        }else{
            $data["vehicle_cat_id"] = "";
        }

        $response = $this->api_model->getTMSubCategoris($data);

        return $response;

    }
    
    public function getTMDetail($vehicle_id){

        $data = array();

        $response = $this->api_model->getTMDetail($vehicle_id);

        return $response;

    }

    public function addTM(){
        
        $rc_number = Request::post("vehicle_rc_number_1").'-'.Request::post("vehicle_rc_number_2").'-'.Request::post("vehicle_rc_number_3").'-'.Request::post("vehicle_rc_number_4");

        $is_gps_active = '';
        if(Request::post("is_gps_enabled") == 'on'){
            $is_gps_active = 'true';
        }else{
            $is_gps_active = 'false';
        }
        
        $is_insurance_active = '';
        if(Request::post("is_insurance_active") == 'on'){
            $is_insurance_active = 'true';
        }else{
            $is_insurance_active = 'false';
        }


        $data = array(
            "TMCategoryId" => Request::post("vehicleCategoryId"),
            "TMSubCategoryId" => Request::post("vehicleSubCategoryId"),
            "TM_rc_number" => $rc_number,
            "min_trip_price" => Request::post("min_trip_price"),
            "manufacturer_name" => Request::post("manufacturer_name"),
            "TM_model" => Request::post("vehicle_model"),
            "manufacture_year" => Request::post("manufacture_year"),
            "delivery_range" => Request::post("delivery_range"),
            "driver1_id" => Request::post("driver1_id"),
            "address_id" => Request::post("address_id"),
            "per_Cu_mtr_km" => Request::post("per_Cu_mtr_km"),
            
            "is_gps_enabled" => $is_gps_active,
            "is_insurance_active" => $is_insurance_active,
            "is_insurance_active" => $is_insurance_active
        );
        
        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd(Request::file('rc_book_image'));
        // if(Request::post("per_metric_ton_per_km_rate")){
        //     $data["per_metric_ton_per_km_rate"] = Request::post("per_metric_ton_per_km_rate");
        // }
        
        if(Request::post("driver2_id")){
            $data["driver2_id"] = Request::post("driver2_id");
        }
        $files = array();
        if(Request::file('rc_book_image')){

            $image = Request::file('rc_book_image');
            // dd($image);
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "rc_book_image"
                ));


        }
        
        if(Request::file('insurance_image')){

            $image = Request::file('insurance_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "insurance_image"
                ));

                       

        }
        
        if(Request::file('vehicle_image')){

            $image = Request::file('vehicle_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "TM_image"
                ));

                       

        }
        
         //  dd($files);
         $response = $this->api_model->addTM($data,$files);

         // dd($response);
         return $response;

    }
    
    
    public function editTM(){

        $rc_number = Request::post("vehicle_rc_number_1").'-'.Request::post("vehicle_rc_number_2").'-'.Request::post("vehicle_rc_number_3").'-'.Request::post("vehicle_rc_number_4");
        // $rc_number = Request::post("vehicle_rc_number");

        $is_gps_active = '';
        if(Request::post("is_gps_enabled") == 'on'){
            $is_gps_active = 'true';
        }else{
            $is_gps_active = 'false';
        }
        
        $is_insurance_active = '';
        if(Request::post("is_insurance_active") == 'on'){
            $is_insurance_active = 'true';
        }else{
            $is_insurance_active = 'false';
        }


        $data = array(
            "TM_rc_number" => $rc_number,
            "min_trip_price" => Request::post("min_trip_price"),
            "manufacturer_name" => Request::post("manufacturer_name"),
            "TM_model" => Request::post("vehicle_model"),
            "manufacture_year" => Request::post("manufacture_year"),
            "delivery_range" => Request::post("delivery_range"),
            "TMId" => Request::post("vehicleId"),
            "driver1_id" => Request::post("driver1_id"),
            "is_gps_enabled" => $is_gps_active,
            "is_insurance_active" => $is_insurance_active,
            "address_id" => Request::post("address_id"),
            "per_Cu_mtr_km" => Request::post("per_Cu_mtr_km"),
        );

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd(Request::file('rc_book_image'));
        // if(Request::post("per_metric_ton_per_km_rate")){
        //     $data["per_metric_ton_per_km_rate"] = Request::post("per_metric_ton_per_km_rate");
        // }

        if(Request::post("driver2_id")){
            $data["driver2_id"] = Request::post("driver2_id");
        }

        $files = array();
        if(Request::file('rc_book_image')){

            $image = Request::file('rc_book_image');
            // dd($image);
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "rc_book_image"
                ));


        }
        
        if(Request::file('insurance_image')){

            $image = Request::file('insurance_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "insurance_image"
                ));

                       

        }
        
        if(Request::file('vehicle_image')){

            $image = Request::file('vehicle_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "TM_image"
                ));

                       

        }

        //   dd($data);
         $response = $this->api_model->editTM($data,$files);

         // dd($response);
         return $response;

    }

    public function getTMunAvailabiity(){

        $data = array(
            "_id" => Request::get('TM_id')

        );

        $response = $this->api_model->getTMunAvailabiity($data);

        return $response;

    }

    public function addTMunAvailabiity(){             


        $start_time = date('Y-m-d', strtotime(Request::post("unavailable_at"))).'T'.Request::post("start_time").':00.000+00:00';
        $end_time = date('Y-m-d', strtotime(Request::post("unavailable_at"))).'T'.Request::post("end_time").':00.000+00:00';

        $data = array(
            "TM_unavail_id" => Request::post("TM_unavail_id"),
            // "unavailable_at" => Request::post("unavailable_at"),
            "start_time" => $start_time,
            "end_time" => $end_time,
        );

        if(Request::post("TM_sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("TM_sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        
        // dd($data);
         $response = $this->api_model->addTMunAvailabiity($data);

         // dd($response);
         return $response;

    }
    
    public function editTMunAvailabiity(){           


        $start_time = date('Y-m-d', strtotime(Request::post("unavailable_at"))).'T'.Request::post("start_time").':00.000+00:00';
        $end_time = date('Y-m-d', strtotime(Request::post("unavailable_at"))).'T'.Request::post("end_time").':00.000+00:00';

        $data = array(
            "_id" => Request::post("TM_unavail_id"),
            // "unavailable_at" => Request::post("unavailable_at"),
            "start_time" => $start_time,
            "end_time" => $end_time,
        );

        if(Request::post("TM_sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("TM_sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        
        // dd($data);
         $response = $this->api_model->editTMunAvailabiity($data);

         // dd($response);
         return $response;

    }
    
    public function deleteTMunAvailabiity(){           

        $data = array(
            "_id" => Request::get("_id")
        );       
        
        // dd($data);
         $response = $this->api_model->deleteTMunAvailabiity($data);

         // dd($response);
         return $response;

    }
    
    public function deleteCPunAvailabiity(){           

        $data = array(
            "_id" => Request::get("_id")
        );       
        
        // dd($data);
         $response = $this->api_model->deleteCPunAvailabiity($data);

         // dd($response);
         return $response;

    }
    
    public function getCPunAvailabiity(){

        $data = array(
            "_id" => Request::get('CP_id')

        );

        $response = $this->api_model->getCPunAvailabiity($data);

        // dd($response);

        return $response;

    }

    public function addCPunAvailabiity(){
        
        
        // dd(Request::all());

        $start_time = date('Y-m-d', strtotime(Request::post("unavailable_at"))).'T'.Request::post("start_time").':00.000+00:00';
        $end_time = date('Y-m-d', strtotime(Request::post("unavailable_at"))).'T'.Request::post("end_time").':00.000+00:00';

        $data = array(
            "CP_unavail_id" => Request::post("CP_unavail_id"),
            "unavailable_at" => date('Y-m-d', strtotime(Request::post("unavailable_at"))),
            "start_time" => $start_time,
            "end_time" => $end_time,
        );

        if(Request::post("CP_sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("CP_sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        
        
         $response = $this->api_model->addCPunAvailabiity($data);

         // dd($response);
         return $response;

    }
    
    public function editCPunAvailabiity(){
        
        
        // dd(Request::all());

        $start_time = date('Y-m-d', strtotime(Request::post("unavailable_at"))).'T'.Request::post("start_time").':00.000+00:00';
        $end_time = date('Y-m-d', strtotime(Request::post("unavailable_at"))).'T'.Request::post("end_time").':00.000+00:00';

        $data = array(
            "CP_unavail_id" => Request::post("CP_unavail_id"),
            "unavailable_at" => date('Y-m-d', strtotime(Request::post("unavailable_at"))),
            "start_time" => $start_time,
            "end_time" => $end_time,
        );

        if(Request::post("CP_sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("CP_sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        
        
         $response = $this->api_model->editCPunAvailabiity($data);

         // dd($response);
         return $response;

    }
}
