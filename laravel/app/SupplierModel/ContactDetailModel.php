<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ContactDetailModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showContactDetails(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["sub_vendor_id"] = Request::get("address_id");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        dd($data);
        $response = $this->api_model->showContactDetails($data);
        // dd($response);
        return $response;
    }

    public function addContactDetails(){

        
        $data = array(
            "person_name" => Request::post("person_name"),
            "title" => Request::post("title"),
            "email" => Request::post("email"),
            "mobile_number" => Request::post("mobile_number"),
            
        );

        if(Request::post("alt_mobile_number")){
            if(strlen(Request::post("alt_mobile_number")) > 5){
                $data["alt_mobile_number"] = Request::post("alt_mobile_number");
            }
        }
        
        if(Request::post("whatsapp_number")){
            if(strlen(Request::post("whatsapp_number")) > 5){
                $data["whatsapp_number"] = Request::post("whatsapp_number");
            }
            
        }
        
        if(Request::post("landline_number")){
            $data["landline_number"] = Request::post("landline_number");
        }

        $contact = array();

        $contact["contact"] = $data;

        // dd($data);
        $response = $this->api_model->addContactDetails($contact);

        return $response;


    }
    
    public function editContactDetails(){

        
        $data = array(
            "person_name" => Request::post("person_name"),
            "title" => Request::post("title"),
            "email" => Request::post("email"),
            "mobile_number" => Request::post("mobile_number"),
            "_id" => Request::post("contact_detail_id"),
            
        );

        if(Request::post("alt_mobile_number")){
            if(strlen(Request::post("alt_mobile_number")) > 5){
                $data["alt_mobile_number"] = Request::post("alt_mobile_number");
            }
        }
        
        if(Request::post("whatsapp_number")){
            if(strlen(Request::post("whatsapp_number")) > 5){
                $data["whatsapp_number"] = Request::post("whatsapp_number");
            }
        }
        
        if(Request::post("landline_number")){
            $data["landline_number"] = Request::post("landline_number");
        }

        $contact = array();

        $contact["contact"] = $data;

        // dd($data);
        $response = $this->api_model->editContactDetails($contact);

        return $response;


    }
}
