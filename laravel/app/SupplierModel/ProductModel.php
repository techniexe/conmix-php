<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProductModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showProducts(){

        $data = array();
        
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("categoryId")){
            $data["categories"] = array();
            array_push($data["categories"],Request::get("categoryId"));
        }
        
        if(Request::get("subcategoryId")){
            $data["sub_categories"] = array();
            array_push($data["sub_categories"],Request::get("subcategoryId"));
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        

        // dd($data);
        $response = $this->api_model->showProducts($data);
        // dd($response);
        return $response;


    }

    public function addProduct(){

        $data = array(
            "category_id" => Request::post("category_id"),
            "sub_category_id" => Request::post("sub_category_id"),
            "quantity" => Request::post("quantity"),
            "unit_price" => Request::post("unit_price"),
            "pickup_address_id" => Request::post("pickup_address_id"),
            "contact_person_id" => Request::post("contact_person_id"),
        );

        if(Request::post('minimum_order')){
            $data['minimum_order'] = Request::post("minimum_order");
        }
        
        if(Request::post('is_available')){
            $data['is_available'] = 'true';
        }else{
            $data['is_available'] = 'false';
        }

        if(Request::post('self_logistics')){
            $data['self_logistics'] = 'true';
        }else{
            $data['self_logistics'] = 'false';
        }

        if(Request::post('verified_by_admin')){
            if(Request::post('verified_by_admin')){
                $data["verified_by_admin"] = 'true';
            }else{
                $data["verified_by_admin"] = 'false';
            }
            
        }
        // dd($data);
        $response = $this->api_model->addSupplierProduct($data);


        

        return $response;

    }

    public function editProducts(){

        $data = array(
            "minimum_order" => Request::post("minimum_order"),
            "unit_price" => Request::post("unit_price"),
            "_id" => Request::post("add_product_id") 
        );

        if(Request::post('is_available')){
            $data['is_available'] = 'true';
        }else{
            $data['is_available'] = 'false';
        }

        if(Request::post('self_logistics')){
            $data['self_logistics'] = 'true';
        }else{
            $data['self_logistics'] = 'false';
        }

        if(Request::post('verified_by_admin')){
            if(Request::post('verified_by_admin')){
                $data["verified_by_admin"] = 'true';
            }else{
                $data["verified_by_admin"] = 'false';
            }
            
        }
        // dd($data);
        $response = $this->api_model->editSupplierProduct($data);

    

        return $response;


    }

    public function updateProductStock(){

        $quantity = 0;

        if(Request::post('plus_minus') == 1){
            $quantity = Request::post('quantity');
        }else{
            $quantity = -Request::post('quantity');
        }

        $request_data = array(
            "incVal" => $quantity,
            "_id" => Request::post("stock_product_id")
        );
        // dd($request_data);
        $response = $this->api_model->updateProductQuantity($request_data);

        return $response;
    }
}
