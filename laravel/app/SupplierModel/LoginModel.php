<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;
use App\ApiConfig;
use App\BaseModel;

class LoginModel extends BaseModel
{
    //
    protected $api_model;

    protected $final_response;

    public function __construct(){
        $this->api_model = new ApisModel();

        $this->final_response = array(
            "status" => null,
            "data" => null,
            "error" => null
        );
    }

    public function login(){
       
        if(Request::post('login_btn')){
            $email_mobile = Request::post('email_mobile');
            $password = Request::post('password');
            
            $data = array(
                "email_mobile" => $email_mobile,
                "password" => $password
            );
            
            $auth_resposnse = $this->api_model->getSessionToken();
            
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){
                //Success Response
                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                $login_response = $this->api_model->login($data);
                // dd($login_response);
                $this->final_response = $login_response;
                
                if($this->final_response["status"] == 200){
                    $custom_token = $this->final_response["data"]["customToken"];
                    
                    // session()->put("custom_token",$custom_token);
                    session()->put("supplier_custom_token",$custom_token);
                    // session()->put("custom_token",$custom_token);

                    $admin_profile = $this->api_model->getSupplierProfile();
                    // dd($admin_profile);
                    
                    if($admin_profile["status"] == 200){
                        session()->put("supplier_profile_details",$admin_profile["data"]);    
                    }

                    // dd(session('profile_details', null));
                    
                }

                // return $login_response;
                // $request->session()->put('session_token', ''.$session_token);

                // $globla = session('session_token', null);
                // $value = $request->session()->get('session_token', null);
                // echo "Global::".$globla.'<br>';
                // echo "value::".$value.'<br>';
                // echo $session_token;exit;
            }else{
                //Error Response
            }
            
        return $this->final_response;
            
        }
    }

    public function logout(){
        session()->forget('supplier_custom_token');
        session()->forget('supplier_profile_details');
        // session()->flush();
    }

    public function requestOTP(){

        $data = array();
        if(Request::get("event_type")){
            $event_type = Request::get("event_type");
            $data["event_type"] = $event_type;

            if($event_type == "update_password"){
                if(Request::get('password')){
                    $data["password"] =  Request::get('password');
                }
                
                if(Request::get('old_password')){
                    $data["old_password"] =  Request::get('old_password');
                }
            }else if($event_type == "edit_admix_stock"){

                $data["admix_address_id"] =  Request::get('address_id');
                $profile = session('supplier_profile_details', null);
                if(isset($profile["master_vendor_id"])){
                    $data["admix_master_vendor_id"] = $profile["master_vendor_id"];
                }

               $data["admix_brand_id"] =  Request::get('brand_id');
               $data["admix_category_id"] =  Request::get('ad_mixture_category_id');
               $data["AdmixtureQuantityId"] =  Request::get('rate_id');
              

            }else if($event_type == "edit_sand_stock"){

                $data["sand_address_id"] =  Request::get('address_id');
                $profile = session('supplier_profile_details', null);
                if(isset($profile["master_vendor_id"])){
                    $data["sand_master_vendor_id"] = $profile["master_vendor_id"];
                }

               $data["sand_source_id"] =  Request::get('source_id');
               $data["sand_zone_id"] =  Request::get('sand_zone_id');
               $data["SandSourceQuantityId"] =  Request::get('rate_id');
              

            }else if($event_type == "edit_agg_stock"){

                $data["agg_address_id"] =  Request::get('address_id');
                $profile = session('supplier_profile_details', null);
                if(isset($profile["master_vendor_id"])){
                    $data["agg_master_vendor_id"] = $profile["master_vendor_id"];
                }

               $data["agg_source_id"] =  Request::get('source_id');
               $data["agg_sub_cat_id"] =  Request::get('sub_cat_id');
               $data["AggregateSourceQuantityId"] =  Request::get('rate_id');
              

            }else if($event_type == "edit_cement_stock"){

                $data["cement_address_id"] =  Request::get('address_id');
                $profile = session('supplier_profile_details', null);
                if(isset($profile["master_vendor_id"])){
                    $data["cement_master_vendor_id"] = $profile["master_vendor_id"];
                }

               $data["cement_brand_id"] =  Request::get('brand_id');
               $data["cement_grade_id"] =  Request::get('grade_id');
               $data["CementQuantityId"] =  Request::get('rate_id');
              

            }else if($event_type == "edit_flyAsh_stock"){

                $data["flyAsh_address_id"] =  Request::get('address_id');
                $profile = session('supplier_profile_details', null);
                if(isset($profile["master_vendor_id"])){
                    $data["flyAsh_master_vendor_id"] = $profile["master_vendor_id"];
                }

               $data["flyAsh_source_id"] =  Request::get('source_id');
               $data["FlyAshSourceQuantityId"] =  Request::get('rate_id');
              

            }else if($event_type == "order_rejected"){

                $data["order_id"] =  Request::get('order_id');
              

            }
        }
        // dd($data);
        $response = $this->api_model->requestOTP($data);

        return $response;
    }
    
    public function requestOTPToMaster(){
        $response = $this->api_model->requestOTPToMaster();
        // dd($response);
        return $response;
    }
    
    public function requestOTPToPlant(){

        // dd(Request::all());

        $data = array();

        if(Request::get("plant_mobile")){
            $data["mobile_number"] = Request::get("plant_mobile");
        }
        
        if(Request::get("plant_email")){
            $data["email"] = Request::get("plant_email");
        }
        // dd($data);
        $response = $this->api_model->requestOTPToPlant($data);
        // dd($response);
        return $response;
    }
    
    public function requestOTPforEditProfile(){

        // dd(Request::all());

        $data = array(
            "requestOTP" => "yes"
        );

        if(Request::get("new_mobile_no")){
            $data["new_mobile_no"] = Request::get("new_mobile_no");
        }
        
        if(Request::get("new_email")){
            $data["new_email"] = Request::get("new_email");
        }
        
        if(Request::get("old_email")){
            $data["old_email"] = Request::get("old_email");
        }
        
        if(Request::get("old_mobile_no")){
            $data["old_mobile_no"] = Request::get("old_mobile_no");
        }

        $response = $this->api_model->requestOTPforEditProfile($data);
        // dd($response);
        return $response;
    }
}
