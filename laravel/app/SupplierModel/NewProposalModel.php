<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class NewProposalModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function addProposal(){

        $data = array(

            "contact_person_id" => Request::get("contact_person_id"),
            "category_name" => Request::get("category_name"),
            "description" => Request::get("description"),
            "quantity" => Request::get("quantity"),
            "pickup_address_id" => Request::get("pickup_address_id"),

        );

        if(Request::get("sub_category_name")){
            $data["sub_category_name"] = Request::get("sub_category_name");
        }

       // dd($data);
        $response = $this->api_model->addProposal($data);
        // dd($response);
        return $response;

    }

    public function showProposals(){

        $data = array();
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showProposals($data);
        // dd($response);
        return $response;

    }
}
