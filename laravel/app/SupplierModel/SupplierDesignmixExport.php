<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SupplierDesignmixExport extends Model implements FromArray,WithHeadings
{
    //
    protected $buyer_order;

    public function __construct(array $buyer_order)
    {
        $this->buyer_order = $buyer_order;
    }

    public function array(): array
    {
        return $this->buyer_order;
    }

    public function headings(): array
    {

        return [

            'Design Mix Grade',
            'Cement Brand',
            'Cement Grade',
            'Cement Qty (KG)',
            'Sand Source',
            'Sand Qty (KG) ',
            'Aggregate Source ',
            'Aggregate Category 1 ',
            'Aggregate Category 1 Qty (KG) ',
            'Aggregate Category 2 ',
            'Aggregate Category 2 Qty (KG) ',
            'Fly Ash Source',
            'Fly Ash Source Qty (KG)',
            'Water Qty (Ltr) (Water / Cement Ratio 0.35 - 0.55) ',
            'Selling Price (/Cu.Mtr) ',
            'Plant Address For This Design Mix',
            'Created',

        ];

    }
}
