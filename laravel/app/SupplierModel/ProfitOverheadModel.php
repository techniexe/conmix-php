<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProfitOverheadModel extends Model
{
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showProfitMargin(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("brand_id")){
            $data["brand_id"] = Request::get("brand_id");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        
        

        // dd($data);
        $response = $this->api_model->showProfitMargin($data);
        // dd($response);
        return $response;


    }

    public function addProfitMargin(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "margin_percentage" => Request::post("margin_percentage"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        

        // dd($data);
        $response = $this->api_model->addProfitMargin($data);

        return $response;


    }

    public function editProfitMargin(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "margin_percentage" => Request::post("margin_percentage"),
            "_id" => Request::post("profit_margin_id"),
            
            
        );     

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
       

        // dd($data);
        $response = $this->api_model->editProfitMargin($data);

        return $response;


    }
    
    public function deleteProfitMargin(){

        $data = array(
            "_id" => Request::get("profit_margin_id")
            
        );       

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }
        

        // dd($data);
        $response = $this->api_model->deleteProfitMargin($data);

        return $response;


    }
    
    public function showOverheadMargin(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("brand_id")){
            $data["brand_id"] = Request::get("brand_id");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        
        

        // dd($data);
        $response = $this->api_model->showOverheadMargin($data);
        // dd($response);
        return $response;


    }

    public function addOverheadMargin(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "margin_percentage" => Request::post("margin_percentage"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        

        // dd($data);
        $response = $this->api_model->addOverheadMargin($data);

        return $response;


    }

    public function editOverheadMargin(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "margin_percentage" => Request::post("margin_percentage"),
            "_id" => Request::post("overhead_margin_id"),
            
            
        );     

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
       

        // dd($data);
        $response = $this->api_model->editOverheadMargin($data);

        return $response;


    }
    
    public function deleteOverheadMargin(){

        $data = array(
            "_id" => Request::get("overhead_margin_id")
            
        );       

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }
        

        // dd($data);
        $response = $this->api_model->deleteOverheadMargin($data);

        return $response;


    }
}
