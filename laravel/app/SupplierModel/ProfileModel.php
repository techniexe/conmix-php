<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProfileModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showProfileDetails(){

        $data = array();

        $response = $this->api_model->getSupplierProfile();
        // dd($response);
        return $response;
    }

    public function editProfile(){

        $files = array();
        $data = array();
        if(Request::post('is_my_profile') == 'yes'){

            $data = array(
                "full_name" => "".Request::post('full_name'),
                "company_name" => "".Request::post('company_name'),
                
            );
    
            if(Request::post('company_type')){
                $data["company_type"] =  Request::post('company_type');
            } 
            
            if(Request::post('landline_number')){
                $data["landline_number"] =  Request::post('landline_number');
            } 
            
            if(Request::post('password')){
                $data["password"] =  Request::post('password');
            }

            if(Request::post('company_certification_number')){
                $data["company_certification_number"] =  Request::post('company_certification_number');
            }
            
            if(Request::post('pan_number')){
                $data["pan_number"] =  Request::post('pan_number');
            }
            
            if(Request::post('gst_number')){
                $data["gst_number"] =  Request::post('gst_number');
            }
            
            if(Request::post('plant_capacity_per_hour')){
                $data["plant_capacity_per_hour"] =  Request::post('plant_capacity_per_hour');
            }
            
            if(Request::post('no_of_plants')){
                $data["no_of_plants"] =  Request::post('no_of_plants');
            }
            
            if(Request::post('no_of_operation_hour')){
                $data["no_of_operation_hour"] =  Request::post('no_of_operation_hour');
            }
            // dd(Request::file('pancard_image'));
            if(Request::file('pancard_image')){

                $image = Request::file('pancard_image');              
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image" => $image,
                        "param_key" => "pancard_image"
                    ));

                    

            }
            
            if(Request::file('company_certification_image')){

                $image = Request::file('company_certification_image');                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image" => $image,
                        "param_key" => "company_certification_image"
                    ));
                           

            }

            if(Request::file('gst_certification_image')){

                $image = Request::file('gst_certification_image');
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image" => $image,
                        "param_key" => "gst_certification_image"
                    ));

                           

            }


        }else if(Request::post('is_my_profile') == 'no'){
            
            if(Request::post('cst')){
                $data["cst"] =  Request::post('cst');
            }
            
            if(Request::post('hse_number')){
                $data["hse_number"] =  Request::post('hse_number');
            }
            
            if(Request::post('annual_turnover')){
                $data["annual_turnover"] =  Request::post('annual_turnover');
            }
            
            if(Request::post('ehs_as_per_iso')){
                $data["ehs_as_per_iso"] =  Request::post('ehs_as_per_iso');
            }
            
            if(Request::post('trade_capacity')){
                $data["trade_capacity"] =  Request::post('trade_capacity');
            }
            
            if(Request::post('ohsas_as_per_iso')){
                $data["ohsas_as_per_iso"] =  Request::post('ohsas_as_per_iso');
            }
            
            if(Request::post('number_of_employee')){
                $data["number_of_employee"] =  Request::post('number_of_employee');
            }
            
            if(Request::post('working_with_other_ecommerce')){
                if(Request::post('working_with_other_ecommerce') == 'yes'){
                    $data["working_with_other_ecommerce"] =  'true';
                }else{
                    $data["working_with_other_ecommerce"] =  'false';
                }
                
            }
            
            

        }

        if(Request::post('is_my_profile')){
            $data["is_my_profile"] =  Request::post('is_my_profile');
        }
        
        // if(Request::post('mobile_code')){
        //     $data["new_mobile_code"] =  Request::post('mobile_code');
        // }
        
        // if(Request::post('email_code')){
        //     $data["new_email_code"] =  Request::post('email_code');
        // }
        
        // if(Request::post('authentication_code')){
        //     $data["authentication_code"] =  Request::post('authentication_code');
        // }       
        

        
        if(Request::get('otp_code_1')){
                
            $otp_code =  Request::get('otp_code_1');


            if(Request::get('mobile_or_email_type')){

                if(Request::get('mobile_or_email_type') == "old_mobile"){
                
                    $data["old_mobile_code"] =  $otp_code;
                    
                }
                
                if(Request::get('mobile_or_email_type') == "new_mobile"){
                
                    $data["new_mobile_code"] =  $otp_code;
                    
                }

                if(Request::get('mobile_or_email_type') == "old_email"){
                
                    $data["old_email_code"] =  $otp_code;
                    
                }

                if(Request::get('mobile_or_email_type') == "new_email"){
                
                    $data["new_email_code"] =  $otp_code;
                    
                }
               
                if(Request::get('email')){
                
                    $data["email"] =  Request::get('email');
                    
                }
                
                if(Request::get('mobile_number')){
                
                    $data["mobile_number"] =  Request::get('mobile_number');
                    
                }

            }

        }


        //  dd( $data);
        //  dd( $files);
         $response = $this->api_model->editSupplierProfile($data,$files);
         // dd($response);
         return $response;

    }

    public function changePassword(){

        $data = array(
            "authentication_code" => "".Request::post('authentication_code')

        );
        
        if(Request::post('password')){
            $data["password"] =  Request::post('password');
        }
        
        if(Request::post('old_password')){
            $data["old_password"] =  Request::post('old_password');
        }
        
        

        //  dd($data);
         $response = $this->api_model->changePassword($data);
         // dd($response);
         return $response;

    }
    
    public function updateNotiStatus($id){

        $data = array(
            "_id" => $id

        );       
        

        //  dd($data);
         $response = $this->api_model->updateNotiStatus($data);
         // dd($response);
         return $response;

    }
}
