<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class StockModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showCementStock(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("brand_id")){
            $data["brand_id"] = Request::get("brand_id");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        
        

        // dd($data);
        $response = $this->api_model->showCementStock($data);
        // dd($response);
        return $response;


    }

    public function addCementStock(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "brand_id" => Request::post("brand_id"),
            "grade_id" => Request::post("grade_id"),
            "quantity" => Request::post("quantity"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        

        // dd($data);
        $response = $this->api_model->addCementStock($data);

        return $response;


    }

    public function editCementStock(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "brand_id" => Request::post("brand_id"),
            "grade_id" => Request::post("grade_id"),
            "quantity" => Request::post("quantity"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }
        
        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
       

        // dd($data);
        $response = $this->api_model->editCementStock($data);

        return $response;


    }
    
    public function deleteCementStock(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }
        

        // dd($data);
        $response = $this->api_model->deleteCementStock($data);

        return $response;


    }
    
    
    public function showSandStock(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showSandStock($data);
        // dd($response);
        return $response;


    }

    public function addSandStock(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "sand_zone_id" => Request::post("sand_zone_id"),
            "quantity" => Request::post("quantity"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addSandStock($data);

        return $response;


    }

    public function editSandStock(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "sand_zone_id" => Request::post("sand_zone_id"),
            "quantity" => Request::post("quantity"),
            "_id" => Request::post("rate_id"),
            
            
        );    

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }
        
        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->editSandStock($data);

        return $response;


    }
    
    public function deleteSandStock(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->deleteSandStock($data);

        return $response;


    }
    
    
    public function showAggregateStock(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }
        
        if(Request::get("sub_cat_id")){
            $data["sub_cat_id"] = Request::get("sub_cat_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showAggregateStock($data);
        // dd($response);
        return $response;


    }

    public function addAggregateStock(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "sub_cat_id" => Request::post("sub_cat_id"),
            "quantity" => Request::post("quantity"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

    
        // dd($data);
        $response = $this->api_model->addAggregateStock($data);

        return $response;


    }

    public function editAggregateStock(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "sub_cat_id" => Request::post("sub_cat_id"),
            "quantity" => Request::post("quantity"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->editAggregateStock($data);

        return $response;


    }
    
    public function deleteAggregateStock(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->deleteAggregateStock($data);

        return $response;


    }
    
    public function showFlyashStock(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("source_id")){
            $data["source_id"] = Request::get("source_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showFlyashStock($data);
        // dd($response);
        return $response;


    }

    public function addFlyashStock(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "quantity" => Request::post("quantity"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addFlyashStock($data);

        return $response;


    }

    public function editFlyashStock(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "source_id" => Request::post("source_id"),
            "quantity" => Request::post("quantity"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->editFlyashStock($data);

        return $response;


    }
    
    public function deleteFlyashStock(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->deleteFlyashStock($data);

        return $response;


    }
    
    public function showAdmixtureStock(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("brand_id")){
            $data["brand_id"] = Request::get("brand_id");
        }
        
        if(Request::get("category_id")){
            $data["category_id"] = Request::get("category_id");
        }

        if(Request::get("quantity_value") && Request::get("quantity")){
            $data["quantity_value"] = Request::get("quantity_value");
            $data["quantity"] = Request::get("quantity");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showAdmixtureStock($data);
        // dd($response);
        return $response;


    }

    public function addAdmixtureStock(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "brand_id" => Request::post("brand_id"),
            "category_id" => Request::post("ad_mixture_category_id"),
            "quantity" => Request::post("quantity"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addAdmixtureStock($data);

        return $response;


    }

    public function editAdmixtureStock(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "brand_id" => Request::post("brand_id"),
            "category_id" => Request::post("ad_mixture_category_id"),
            "quantity" => Request::post("quantity"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        if(Request::post("is_active") != null){
            $data["is_active"] = Request::post("is_active");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->editAdmixtureStock($data);

        return $response;


    }
    
    public function deleteAdmixtureStock(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );       

        if(Request::post("authentication_code")){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->deleteAdmixtureStock($data);

        return $response;


    }
    
    public function showWaterStock(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showWaterStock($data);
        // dd($response);
        return $response;


    }

    public function addWaterStock(){

        
        $data = array(
            "address_id" => Request::post("address_id"),
            "per_ltr_rate" => Request::post("quantity"),
            
        );        

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->addWaterStock($data);

        return $response;


    }

    public function editWaterStock(){

        $data = array(
            "address_id" => Request::post("address_id"),
            "per_ltr_rate" => Request::post("quantity"),
            "_id" => Request::post("rate_id"),
            
            
        );     

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // dd($data);
        $response = $this->api_model->editWaterStock($data);

        return $response;


    }
    
    public function deleteWaterStock(){

        $data = array(
            "_id" => Request::get("rate_id")
            
        );              

        // dd($data);
        $response = $this->api_model->deleteWaterStock($data);

        return $response;


    }
}
