<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class DriverModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showDriverDetails(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["sub_vendor_id"] = Request::get("address_id");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showDriverDetails($data);
        // dd($response);
        return $response;


    }

    public function addDriverDetails(){

        $profile = session('supplier_profile_details', null);

        
        $data = array(
            "driver_name" => Request::post("driver_name"),
            "driver_mobile_number" => Request::post("driver_mobile_number")
            
        );        


        if(Request::post("driver_alt_mobile_number")){
            $data["driver_alt_mobile_number"] = Request::post("driver_alt_mobile_number");
        }
        
        if(Request::post("driver_whatsapp_number")){
            $data["driver_whatsapp_number"] = Request::post("driver_whatsapp_number");
        }
        
        if(Request::post("sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("sub_vendor_id");
        }

        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::file('driver_pic')){

            $image = Request::file('driver_pic');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "driver_pic"
            ));                

        }
        

        

        // dd($data);
        $response = $this->api_model->addDriverDetails($data,$files);

        return $response;


    }
    
    public function editDriverDetails(){

        
        
        $data = array(
            "driver_name" => Request::post("driver_name"),
            "driver_mobile_number" => Request::post("driver_mobile_number"),
            "_id" => Request::post("driver_id")
            
        );        


        if(Request::post("driver_alt_mobile_number")){
            $data["driver_alt_mobile_number"] = Request::post("driver_alt_mobile_number");
        }
        
        if(Request::post("driver_whatsapp_number")){
            $data["driver_whatsapp_number"] = Request::post("driver_whatsapp_number");
        }

        if(Request::post("sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::file('driver_pic')){

            $image = Request::file('driver_pic');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "driver_pic"
            ));                

        }
        

        
        // echo json_encode($data);
        // dd($data);
        $response = $this->api_model->editDriverDetails($data,$files);

        return $response;


    }
    
    
    
    public function showOperatorDetails(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("address_id")){
            $data["sub_vendor_id"] = Request::get("address_id");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showOperatorDetails($data);
        // dd($response);
        return $response;


    }

    public function addOperatorDetails(){

        
        $data = array(
            "operator_name" => Request::post("driver_name"),
            "operator_mobile_number" => Request::post("driver_mobile_number")
            
        );        


        if(Request::post("driver_alt_mobile_number")){
            $data["operator_alt_mobile_number"] = Request::post("driver_alt_mobile_number");
        }
        
        if(Request::post("driver_whatsapp_number")){
            $data["operator_whatsapp_number"] = Request::post("driver_whatsapp_number");
        }

        if(Request::post("sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::file('driver_pic')){

            $image = Request::file('driver_pic');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "operator_pic"
            ));                

        }
        

        

        // dd($data);
        $response = $this->api_model->addOperatorDetails($data,$files);

        return $response;


    }
    
    public function editOperatorDetails(){

        
        $data = array(
            "operator_name" => Request::post("driver_name"),
            "operator_mobile_number" => Request::post("driver_mobile_number"),
            "_id" => Request::post("driver_id")
            
        );        


        if(Request::post("driver_alt_mobile_number")){
            $data["operator_alt_mobile_number"] = Request::post("driver_alt_mobile_number");
        }
        
        if(Request::post("driver_whatsapp_number")){
            $data["operator_whatsapp_number"] = Request::post("driver_whatsapp_number");
        }

        if(Request::post("sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::file('driver_pic')){

            $image = Request::file('driver_pic');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "operator_pic"
            ));                

        }
        

        

        // dd($data);
        $response = $this->api_model->editOperatorDetails($data,$files);

        return $response;


    }
    
    public function showPumpGangDetails(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("address_id")){
            $data["sub_vendor_id"] = Request::get("address_id");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showPumpGangDetails($data);
        // dd($response);
        return $response;


    }

    public function addPumpGangDetails(){

        
        $data = array(
            "gang_name" => Request::post("driver_name"),
            "gang_mobile_number" => Request::post("driver_mobile_number")
            
        );        


        if(Request::post("driver_alt_mobile_number")){
            $data["gang_alt_number"] = Request::post("driver_alt_mobile_number");
        }
        
        if(Request::post("driver_whatsapp_number")){
            $data["gang_whatsapp_number"] = Request::post("driver_whatsapp_number");
        }

        if(Request::post("sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::file('driver_pic')){

            $image = Request::file('driver_pic');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "gang_pic"
            ));                

        }
        

        

        // dd($data);
        $response = $this->api_model->addPumpGangDetails($data,$files);

        return $response;


    }
    
    public function editPumpGangDetails(){

        
        $data = array(
            "gang_name" => Request::post("driver_name"),
            "gang_mobile_number" => Request::post("driver_mobile_number"),
            "_id" => Request::post("driver_id")
            
        );        


        if(Request::post("driver_alt_mobile_number")){
            $data["gang_alt_number"] = Request::post("driver_alt_mobile_number");
        }
        
        if(Request::post("driver_whatsapp_number")){
            $data["gang_whatsapp_number"] = Request::post("driver_whatsapp_number");
        }

        if(Request::post("sub_vendor_id")){
            $data["sub_vendor_id"] = Request::post("sub_vendor_id");
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::file('driver_pic')){

            $image = Request::file('driver_pic');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "gang_pic"
            ));                

        }
        

        

        // dd($data);
        $response = $this->api_model->editPumpGangDetails($data,$files);

        return $response;


    }

}
