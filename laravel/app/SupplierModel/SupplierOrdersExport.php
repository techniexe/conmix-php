<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SupplierOrdersExport extends Model implements FromArray,WithHeadings
{
    //
    protected $buyer_order;

    public function __construct(array $buyer_order)
    {
        $this->buyer_order = $buyer_order;
    }

    public function array(): array
    {
        return $this->buyer_order;
    }

    public function headings(): array
    {

        return [

            'Order Id',
            'Buyer Order Id',
            'Buyer Name',
            'Company Name',
            'Plant Name',
            'Qty (Cu. Mtr)',
            'Per Unit Price',
            'Total Amount',
            'Order Status',
            'Payment Status',
            'Pickup Location',
            'Delivery Location',
            'Order Date',

        ];

    }
}
