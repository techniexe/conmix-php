<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;
class AddressModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new \App\SupplierModel\ApisModel();

    }

    public function showAddressDetails(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("city_id")){
            $data["city_id"] = Request::get("city_id");
        }

        // if(Request::get("before")){
        //     $data["before"] = Request::get("before");
        // }
    
        // if(Request::get("after")){
        //     $data["after"] = Request::get("after");
        // }

        // dd($data);
        $response = $this->api_model->showAddressDetails($data);
        // dd($response);
        return $response;
    }

    public function addAddress(){
        
        $data = array(
            "business_name" => Request::post("business_name"),
            "line1" => Request::post("line1"),
            "state_id" => Request::post("state_id"),
            "city_id" => Request::post("city_id"),
            "pincode" => Request::post("pincode"),
            "address_type" => Request::post("address_type"),
            "full_name" => Request::post("full_name"),
            "mobile_number" => Request::post("mobile"),
            "email" => Request::post("email"),
            "password" => Request::post("password"),
            "billing_address_id" => Request::post("billing_address_id"),
            "location" => array(
                "coordinates" => array(
                    Request::post("us2_lon"),
                    Request::post("us2_lat")                   
                )
            )
            
        );

        if(Request::post("line2")){
            $data["line2"] = Request::post("line2");
        }


        if(Request::post("plant_capacity_per_hour")){
            $data["plant_capacity_per_hour"] = Request::post("plant_capacity_per_hour");

            $data["no_of_plants"] = 1;
        }
        
        // if(Request::post("no_of_plants")){
        //     $data["no_of_plants"] = Request::post("no_of_plants");
        // }
        
        if(Request::post("no_of_operation_hour")){
            $data["no_of_operation_hour"] = Request::post("no_of_operation_hour");
        }
        
        if(Request::post("mobile_code")){
            $data["mobile_code"] = Request::post("mobile_code");
        }
        
        if(Request::post("email_code")){
            $data["email_code"] = Request::post("email_code");
        }

        // echo json_encode($data);
        // dd($data);
        $response = $this->api_model->addAddress($data);
        // dd($response);
        return $response;


    }
    
    public function editAddress(){
       
        $data = array(
            "business_name" => Request::post("business_name"),
            "line1" => Request::post("line1"),
            "state_id" => Request::post("state_id"),
            "city_id" => Request::post("city_id"),
            "pincode" => Request::post("pincode"),
            "address_type" => Request::post("address_type"),
            "_id" => Request::post("address_id"),
            "full_name" => Request::post("full_name"),
            "mobile_number" => Request::post("mobile"),
            "email" => Request::post("email"),
            "billing_address_id" => Request::post("billing_address_id"),
            "location" => array(
                "coordinates" => array(
                    Request::post("us2_lon"),
                    Request::post("us2_lat")                   
                )
            )
            
        );

        if(Request::post("line2")){
            $data["line2"] = Request::post("line2");
        }
        
        if(Request::post("password")){
            $data["password"] = Request::post("password");
        }

        if(Request::post("plant_capacity_per_hour")){
            $data["plant_capacity_per_hour"] = Request::post("plant_capacity_per_hour");

            $data["no_of_plants"] = 1;
        }
        
        // if(Request::post("no_of_plants")){
            
        // }
        
        if(Request::post("no_of_operation_hour")){
            $data["no_of_operation_hour"] = Request::post("no_of_operation_hour");
        }

        if(Request::post("mobile_code")){
            if(Request::post("mobile_code") != 'undefined'){
                $data["mobile_code"] = Request::post("mobile_code");
            }
        }
        
        if(Request::post("email_code")){
            if(Request::post("email_code") != 'undefined'){
                $data["email_code"] = Request::post("email_code");
            }
        }
        
        // dd($data);
        $response = $this->api_model->editAddress($data);

        return $response;


    }

    public function getRegionServ(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->getRegionServ($data);
        // dd($response);
        return $response;

    }

    public function getSourceByRegion(){

        $data = array();

        if(Request::get("source_name")){
            $data["source_name"] = Request::get("source_name");
        }

        if(Request::get("region_id")){
            $data["region_id"] = Request::get("region_id");
        }    

        // dd($data);
        $response = $this->api_model->getSourceByRegion($data);
        // dd($response);
        return $response;

    }


    public function showbillingAddress(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("city_id")){
            $data["city_id"] = Request::get("city_id");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showbillingAddress($data);
        // dd($response);
        return $response;
    }

    public function addBillingAddress(){
        
        $data = array(
            "company_name" => Request::post("company_name"),
            "line1" => Request::post("line1"),
            "state_id" => Request::post("state_id"),
            "city_id" => Request::post("city_id"),
            "pincode" => Request::post("pincode"),
            "gst_number" => Request::post("gst_number")
            
        );

        if(Request::post("line2")){
            $data["line2"] = Request::post("line2");
        }

        $files = array();

        if(Request::file('gst_image')){

            $image = Request::file('gst_image');                  

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "gst_image"
            ));                

        }
        
        if(Request::file('logo_image')){

            $image = Request::file('logo_image');                  

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "logo_image"
            ));                

        }

        // echo json_encode($data);
        // dd($data);
        $response = $this->api_model->addBillingAddress($data,$files);
        // dd($response);
        return $response;


    }
    
    public function editBillingAddress(){
        
        $data = array(
            "company_name" => Request::post("company_name"),
            "line1" => Request::post("line1"),
            "state_id" => Request::post("state_id"),
            "city_id" => Request::post("city_id"),
            "pincode" => Request::post("pincode"),
            "gst_number" => Request::post("gst_number"),
            "_id" => Request::post("address_id"),
            
        );

        if(Request::post("line2")){
            $data["line2"] = Request::post("line2");
        }

        $files = array();

        if(Request::file('gst_image')){

            $image = Request::file('gst_image');                  

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "gst_image"
            ));                

        }

        if(Request::file('logo_image')){

            $image = Request::file('logo_image');                  

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "logo_image"
            ));                

        }

        // echo json_encode($data);
        // dd($data);
        $response = $this->api_model->editBillingAddress($data,$files);
        // dd($response);
        return $response;


    }


    public function getSubvendors(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        // if(Request::get("before")){
        //     $data["before"] = Request::get("before");
        // }
    
        // if(Request::get("after")){
        //     $data["after"] = Request::get("after");
        // }

        // dd($data);
        $response = $this->api_model->getSubvendors($data);
        // dd($response);
        return $response;
    }
    
    public function getFillDetailsOfVendor(){

        $data = array();

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // if(Request::get("before")){
        //     $data["before"] = Request::get("before");
        // }
    
        // if(Request::get("after")){
        //     $data["after"] = Request::get("after");
        // }

        // dd($data);
        $response = $this->api_model->getFillDetailsOfVendor($data);
        // dd($response);
        return $response;
    }
}
