<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class SupportTicketModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showTickets(){

        $data = array();

        if(Request::get("severity")){
            $data["severity"] = Request::get("severity");
        }
        
        if(Request::get("subject")){
            $data["subject"] = Request::get("subject");
        }    
        
        if(Request::get("ticket_id")){
            $data["ticket_id"] = Request::get("ticket_id");
        }
         
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showSupportTickets($data);
        // dd($response);
        return $response;

    }

    public function addTicket(){

        $profile_detials = session('supplier_profile_details', null);


        $data = array(
            "question_type" => "".Request::post('question_type'),
            "severity" => "".Request::post('severity'),
            "subject" => "".Request::post('subject'),
            "description" => "".Request::post('description'),
            "client_type" => "Vendor",
            "client_id" => $profile_detials["_id"]

        );
        // dd(session('supplier_profile_details', null));
        
        
        if(Request::post('order_id')){
            $data["order_id"] =  Request::post('order_id');
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::file('attachments')){

            $images = Request::file('attachments');
            foreach($images as $image){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image
                ));

            }            

        }

        //  dd($files);
         $response = $this->api_model->addSupportTicket($data,$files);
         // dd($response);
         return $response;

    }

    public function showTicketDetails($ticket_id){

        $data = array(
            "ticket_id" => $ticket_id
        );

        $response = $this->api_model->showTicketDetails($data);
        //  dd($response);
         return $response;

    }

    public function replyTicket(){

        $data = array(
            "comment" => "".Request::post('comment'),
            "ticket_id" => "".Request::post('ticket_id')

        );

        if(Request::post('support_ticket_status')){
            $data["support_ticket_status"] =  Request::post('support_ticket_status');
        }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::file('attachments')){

            $images = Request::file('attachments');
            foreach($images as $image){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image
                ));

            }            

        }

        //  dd($files);
         $response = $this->api_model->replySupportTicket($data,$files);
         // dd($response);
         return $response;

    }

    public function updateTicketStatus(){

        $data = array(
            "ticket_id" => Request::post('support_ticket_id')
        );

        if(Request::post('support_ticket_status')){
            $data["support_ticket_status"] =  Request::post('support_ticket_status');
        }

        $response = $this->api_model->updateTicketStatus($data);
         // dd($response);
         return $response;

    }

    public function getTicketMessages($ticket_id){

        $data = array(
            "ticket_id" => $ticket_id
        );

        $response = $this->api_model->getTicketMessages($data);
         // dd($response);
         return $response;

    }

}
