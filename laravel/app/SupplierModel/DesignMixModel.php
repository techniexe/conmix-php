<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class DesignMixModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showDesignMix(){

        $data = array();

        if(Request::get("vehicle_category_id")){
            $data["vehicle_category_id"] = Request::get("vehicle_category_id");
        }
    
        if(Request::get("vehicle_sub_category_id")){
            $data["vehicle_sub_category_id"] = Request::get("vehicle_sub_category_id");
        }
        
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
    
        if(Request::get("is_gps_enabled")){
            $data["is_gps_enabled"] = Request::get("is_gps_enabled");
        }
        
        if(Request::get("is_insurance_active")){
            $data["is_insurance_active"] = Request::get("is_insurance_active");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }     
    
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $response = $this->api_model->getMix($data);

        return $response;

    }


    public function addMix(){
        
        

        $is_available = '';
        if(Request::post("is_availble") == 'on'){
            $is_available = 'true';
        }else{
            $is_available = 'false';
        }
        
        $is_custom = '';
        if(Request::post("is_custom") == 'on'){
            $is_custom = 'true';
        }else{
            $is_custom = 'false';
        }

        $ad_mixture_quantity = 0;

        if(Request::post("cement_quantity")){
            $ad_mixture_quantity = (Request::post("cement_quantity") * 2.20) / 100;
        }


        $data = array(
            "grade_id" => Request::post("grade_id"),
            "product_name" => Request::post("product_name_1").''.Request::post("design_mix_number"),
            // "cement_brand_id" => Request::post("cement_brand_id"),
            "cement_quantity" => Request::post("cement_quantity"),
            // "sand_source_id" => Request::post("sand_source_id"),
            // "sand_zone_id" => Request::post("sand_zone_id"),
            "sand_quantity" => Request::post("sand_quantity"),
            // "aggregate_source_id" => Request::post("aggregate_source_id"),
            // "aggregate1_sub_category_id" => Request::post("aggregate1_sub_category_id"),
            "aggregate1_quantity" => Request::post("aggregate1_quantity"),
            // "aggregate2_sub_category_id" => Request::post("aggregate2_sub_category_id"),
            "aggregate2_quantity" => Request::post("aggregate2_quantity"),
            // "ad_mixture_brand_id" => Request::post("ad_mixture_brand_id"),
            "admix_quantity_per" => Request::post("admix_quantity_per"),
            "ad_mixture_quantity" => Request::post("ad_mixture_quantity"),
            "water_quantity" => Request::post("water_quantity"),
            // "selling_price" => Request::post("selling_price"),
            "description" => Request::post("description"),
            // "cement_brand_name" => Request::post("cement_brand_name"),
            // "sand_source_name" => Request::post("sand_source_name"),
            // "aggregate_source_name" => Request::post("aggregate_source_name"),
            // "fly_ash_source_name" => Request::post("fly_ash_source_name"),
            // "aggregate1_sub_category_name" => Request::post("aggregate1_sub_category_name"),
            // "aggregate2_sub_category_name" => Request::post("aggregate2_sub_category_name"),
            // "admix_brand_name" => Request::post("admix_brand_name"),
            // "ad_mixture_category_id" => Request::post("ad_mixture_category_id"),
            // "cement_grade_id" => Request::post("cement_grade_id"),
            "address_id" => Request::post("address_id"),
            
            "is_available" => $is_available,
            // "is_custom" => $is_custom,
        );
        
        // if(Request::post("fly_ash_source_id")){
        if(Request::post("fly_ash_quantity")){
            // $data["fly_ash_source_id"] = Request::post("fly_ash_source_id");
            $data["fly_ash_quantity"] = Request::post("fly_ash_quantity");
        }

        // dd(Request::file('rc_book_image'));
        // if(Request::post("per_metric_ton_per_km_rate")){
        //     $data["per_metric_ton_per_km_rate"] = Request::post("per_metric_ton_per_km_rate");
        // }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        
        $files = array();

        if(Request::file('vehicle_image')){

            $image = Request::file('vehicle_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "TM_image"
                ));

                       

        }

        // $final_data = array(
        //     "grade_id" => Request::post("grade_id"),
        //     "variant" => array($data)
        // );
        
        $final_data = array(
            "grade_id" => Request::post("grade_id"),
            "address_id" => Request::post("address_id")
        );

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $final_data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        // echo json_encode($data);
        // echo json_encode($final_data);
        //   dd($final_data);
         //  dd($files);
         $response = $this->api_model->addMix($final_data,$files);
        //  dd($response);
        // dd($response);
         if($response["status"] == 200){
            
            $design_mix_id = $response["data"]["_id"];

            $final_data = array(
                "variant" => array($data)
            );

            $response = $this->api_model->addMixVarient($final_data,$files,$design_mix_id);


         }

        //  dd($response);
         return $response;

    }
    
    public function getDesignMixGradeWise(){      
    


        $data = array(
            "grade_id" => Request::get("grade_id")
        );
        
        
         $response = $this->api_model->getDesignMixGradeWise($data);

         // dd($response);
         return $response;

    }
    
    public function getDesignMixCombinationGradeWise(){  

        $data = array(
            "grade_id" => Request::get("grade_id")
        );
        
        
         $response = $this->api_model->getDesignMixCombinationGradeWise($data);

         // dd($response);
         return $response;

    }
    
    public function getSingleDesignMixCombinationByBrandIds(){  
        // dd(Request::all());
        $data = array(
            "grade_id" => Request::get("design_mix_id"),
            "cement_brand_id" => Request::get("cement_brand_id"),
            "cement_grade_id" => Request::get("cement_grade_id"),
            "sand_source_id" => Request::get("sand_source_id"),
            "sand_zone_id" => Request::get("sand_zone_id"),
            "aggr_source_id" => Request::get("aggr_source_id"),
            "aggr_cat_1_id" => Request::get("aggr_cat_1_id"),
            "aggr_cat_2_id" => Request::get("aggr_cat_2_id"),
            "admix_brand_id" => Request::get("admix_brand_id"),
            "admix_cat_id" => Request::get("admix_cat_id"),
        );

        if(Request::get("fly_ash_id") != null){

            $data['fly_ash_id'] = Request::get("fly_ash_id");

        }
        
        if(Request::get("water_type") != null){

            $data['water_type'] = Request::get("water_type");

        }
        
        
         $response = $this->api_model->getSingleDesignMixCombinationByBrandIds($data);

         // dd($response);
         return $response;

    }
    
    public function getDesignMixById($id){      
    


        $data = array(
            "mix_id" => $id
        );
        
        
         $response = $this->api_model->getDesignMixById($data);

         // dd($response);
         return $response;

    }


    public function editMix(){
        
        

        $is_available = '';
        if(Request::post("is_availble") == 'on'){
            $is_available = 'true';
        }else{
            $is_available = 'false';
        }
        
        $is_custom = '';
        if(Request::post("is_custom") == 'on'){
            $is_custom = 'true';
        }else{
            $is_custom = 'false';
        }

        $ad_mixture_quantity = 0;

        if(Request::post("cement_quantity")){
            $ad_mixture_quantity = (Request::post("cement_quantity") * 2.20) / 100;
        }

        $data = array(
            "product_name" => Request::post("product_name_1").''.Request::post("design_mix_number"),
            // "cement_brand_id" => Request::post("cement_brand_id"),
            "cement_quantity" => Request::post("cement_quantity"),
            // "sand_source_id" => Request::post("sand_source_id"),
            // "sand_zone_id" => Request::post("sand_zone_id"),
            "sand_quantity" => Request::post("sand_quantity"),
            // "aggregate_source_id" => Request::post("aggregate_source_id"),
            // "aggregate1_sub_category_id" => Request::post("aggregate1_sub_category_id"),
            "aggregate1_quantity" => Request::post("aggregate1_quantity"),
            // "aggregate2_sub_category_id" => Request::post("aggregate2_sub_category_id"),
            "aggregate2_quantity" => Request::post("aggregate2_quantity"),
            // "fly_ash_source_id" => Request::post("fly_ash_source_id"),
            "fly_ash_quantity" => Request::post("fly_ash_quantity"),
            // "ad_mixture_brand_id" => Request::post("ad_mixture_brand_id"),
            "admix_quantity_per" => Request::post("admix_quantity_per"),
            "ad_mixture_quantity" => Request::post("ad_mixture_quantity"),
            "water_quantity" => Request::post("water_quantity"),
            // "selling_price" => Request::post("selling_price"),
            "description" => Request::post("description"),
            // "cement_brand_name" => Request::post("cement_brand_name"),
            // "sand_source_name" => Request::post("sand_source_name"),
            // "aggregate_source_name" => Request::post("aggregate_source_name"),
            // "fly_ash_source_name" => Request::post("fly_ash_source_name"),
            // "aggregate1_sub_category_name" => Request::post("aggregate1_sub_category_name"),
            // "aggregate2_sub_category_name" => Request::post("aggregate2_sub_category_name"),
            // "admix_brand_name" => Request::post("admix_brand_name"),
            // "ad_mixture_category_id" => Request::post("ad_mixture_category_id"),
            // "cement_grade_id" => Request::post("cement_grade_id"),
            "address_id" => Request::post("address_id"),
            "mix_id" => Request::post("mix_id"),
            
            "is_available" => $is_available,
            // "is_custom" => $is_custom,
        );
        
        // dd(Request::file('rc_book_image'));
        // if(Request::post("per_metric_ton_per_km_rate")){
        //     $data["per_metric_ton_per_km_rate"] = Request::post("per_metric_ton_per_km_rate");
        // }

        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }
        
        $files = array();

        if(Request::file('vehicle_image')){

            $image = Request::file('vehicle_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image,
                    "param_key" => "TM_image"
                ));

                       

        }

        // $final_data = array(
        //     "grade_id" => Request::post("grade_id"),
        //     "variant" => array($data)
        // );


        
        //   dd($data);
         //  dd($files);
         $response = $this->api_model->editMix($data,$files);

         // dd($response);
         return $response;

    }


    public function showMedia(){

        $data = array();
    
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $response = $this->api_model->getMedia($data);

        return $response;

    }


    public function addPrimaryMedia(){


        $data = array();
        
        // dd(Request::file('rc_book_image'));
        // if(Request::post("per_metric_ton_per_km_rate")){
        //     $data["per_metric_ton_per_km_rate"] = Request::post("per_metric_ton_per_km_rate");
        // }
        
        $files = array();

        if(Request::file('primary_image')){

            $image = Request::file('primary_image');
            

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "media_url"
                ));

                       

        }

        // $final_data = array(
        //     "grade_id" => Request::post("grade_id"),
        //     "variant" => array($data)
        // );
        
        // $final_data = array(
        //     "grade_id" => Request::post("grade_id")
        // );


        
        //   dd($final_data);
         //  dd($files);
         $response = $this->api_model->addPrimaryMedia($data,$files);

        // dd($response);
         

         // dd($response);
         return $response;

    }
    
    public function addSecondoryMedia(){


        $data = array();
        
        // dd(Request::file('rc_book_image'));
        // if(Request::post("per_metric_ton_per_km_rate")){
        //     $data["per_metric_ton_per_km_rate"] = Request::post("per_metric_ton_per_km_rate");
        // }
        
        $all_files = Request::file('secondory_image');

        $files = array();
            $count = 0;
        foreach($all_files as $value){

            $image = $value;
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image_mime" => $image->getClientMimeType(),
                "image" => $image,
                "param_key" => "media_url[".$count."]"
            ));

            $count++;

        }


        
        //   dd($final_data);
        //   dd($files);
         $response = $this->api_model->addSecondaryMedia($data,$files);

        // dd($response);
         

         // dd($response);
         return $response;

    }
    
    public function deleteMedia(){


        $data = array(
            "mediaId" => Request::get('mediaId')
        );
        
        //   dd($final_data);
        //   dd($data);
         $response = $this->api_model->deleteMedia($data);

        // dd($response);
         

         // dd($response);
         return $response;

    }

    
}
