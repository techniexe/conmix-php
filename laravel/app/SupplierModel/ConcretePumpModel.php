<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ConcretePumpModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showConcretePump(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }
        
        if(Request::get("concrete_pump_category_id")){
            $data["concrete_pump_category_id"] = Request::get("concrete_pump_category_id");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showConcretePump($data);
        // dd($response);
        return $response;


    }

    public function addConcretePump(){

        
        $data = array(
            "ConcretePumpCategoryId" => Request::post("ConcretePumpCategoryId"),
            "company_name" => Request::post("company_name"),
            "concrete_pump_model" => Request::post("concrete_pump_model"),
            "manufacture_year" => Request::post("manufacture_year"),
            "serial_number" => Request::post("serial_number"),
            "pipe_connection" => Request::post("pipe_connection"),
            "concrete_pump_capacity" => Request::post("concrete_pump_capacity"),
            "is_Operator" => Request::post("is_Operator"),
            "gang_id" => Request::post("gang_id"),
            "is_helper" => Request::post("is_helper"),
            "transportation_charge" => Request::post("transportation_charge"),
            "concrete_pump_price" => Request::post("concrete_pump_price"),
            "address_id" => Request::post("address_id"),
            
        );    
        
        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::post('is_helper') == 'on'){
            $data["is_helper"] = 'true';  
        }else{
            $data["is_helper"] = 'false';  
        }
        
        if(Request::post('is_Operator') == 'on'){
            $data["is_Operator"] = 'true'; 

            if(Request::post('operator_id')){
                $data["operator_id"] = Request::post('operator_id');  
            }
        }else{
            $data["is_Operator"] = 'false';  
        }
        
        
        if(Request::file('concrete_pump_image')){

            $image = Request::file('concrete_pump_image');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "concrete_pump_image"
            ));                

        }

        // dd($data);
        $response = $this->api_model->addConcretePump($data,$files);

        return $response;


    }

    public function editConcretePump(){

        $data = array(
            "ConcretePumpCategoryId" => Request::post("ConcretePumpCategoryId"),
            "company_name" => Request::post("company_name"),
            "concrete_pump_model" => Request::post("concrete_pump_model"),
            "manufacture_year" => Request::post("manufacture_year"),
            "serial_number" => Request::post("serial_number"),
            "pipe_connection" => Request::post("pipe_connection"),
            "concrete_pump_capacity" => Request::post("concrete_pump_capacity"),
            "is_Operator" => Request::post("is_Operator"),
            "is_helper" => Request::post("is_helper"),
            "transportation_charge" => Request::post("transportation_charge"),
            "concrete_pump_price" => Request::post("concrete_pump_price"),
            "address_id" => Request::post("address_id"),
            "gang_id" => Request::post("gang_id"),
            
        );    
        
        $profile = session('supplier_profile_details', null);
        if(isset($profile["master_vendor_id"])){
            $data["master_vendor_id"] = $profile["master_vendor_id"];
        }

        $files = array();

        if(Request::post('is_helper') == 'on'){
            $data["is_helper"] = 'true';  
        }else{
            $data["is_helper"] = 'false';  
        }
        
        if(Request::post('is_Operator') == 'on'){
            $data["is_Operator"] = 'true'; 

            if(Request::post('operator_id')){
                $data["operator_id"] = Request::post('operator_id');  
            }
        }else{
            $data["is_Operator"] = 'false';  
        }
        
        if(Request::file('concrete_pump_image')){

            $image = Request::file('concrete_pump_image');      
            

            array_push($files, array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "concrete_pump_image"
            ));                

        }
        


        // dd($data);
        $response = $this->api_model->editConcretePump($data,$files,Request::post("concrete_pump_id"));

        return $response;


    }
}
