<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SupplierCPListExport extends Model implements FromArray,WithHeadings
{
    //
    protected $buyer_order;

    public function __construct(array $buyer_order)
    {
        $this->buyer_order = $buyer_order;
    }

    public function array(): array
    {
        return $this->buyer_order;
    }

    public function headings(): array
    {

        return [

            'Plant Address',
            'Concrete Pump Category',
            'Concrete Pump Make',
            'Concrete Pump Model',
            'Manufacture Year',
            'CP Serial No',
            'Pipe Connection (Meter)',
            'CP Capacity (Cu.Mtr) / Hour',
            'Operator',
            'With Helper',
            'Transportation Charge (Per KM)',
            'Concrete Pump Price (Cu.Mtr)',
            'Created',

        ];

    }
}
