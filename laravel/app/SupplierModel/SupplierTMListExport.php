<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SupplierTMListExport extends Model implements FromArray,WithHeadings
{
    //
    protected $buyer_order;

    public function __construct(array $buyer_order)
    {
        $this->buyer_order = $buyer_order;
    }

    public function array(): array
    {
        return $this->buyer_order;
    }

    public function headings(): array
    {

        return [

            'TM Category',
            'TM Sub Category',
            'TM Pick Up Address',
            'TM Registration No',
            'Min Trip Price',
            'Per Cu.Mtr/KM Price',
            'TM Make',
            'TM Type Modal',
            'TM Manufacture Year',
            'Delivery Range KM',
            'Is Insurance Active',
            'Is GPS Active',
            'Created',

        ];

    }
}
