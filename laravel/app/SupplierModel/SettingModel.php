<?php

namespace App\SupplierModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class SettingModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();

        

    }

    public function addSettings(){
        
        $data = array(
            "with_TM" => true,
            "TM_price" => 0,
            "with_CP" => true,
            "CP_price" => 0,
            "is_customize_design_mix" => true,
            
        );        

         // dd($data);
        $response = $this->api_model->addSettings($data);

        return $response;


    }
    
    public function getSettings(){       
            
        $data = array();
         // dd($data);
        $response = $this->api_model->getSettings($data);

        return $response;


    }
    
    public function getAdmixSettings(){       
            
        $data = array();
         // dd($data);
        $response = $this->api_model->getAdmixSettings($data);

        return $response;


    }


    public function editSettings(){

        $with_TM = '';
        if(Request::post("with_TM") == 'on'){
            $with_TM = 'true';
        }else{
            $with_TM = 'false';
        }
        
        $with_CP = '';
        if(Request::post("with_CP") == 'on'){
            $with_CP = 'true';
        }else{
            $with_CP = 'false';
        }
        
        $is_customize_design_mix = '';
        if(Request::post("is_customize_design_mix") == 'on'){
            $is_customize_design_mix = 'true';
        }else{
            $is_customize_design_mix = 'false';
        }


        $data = array(
            "with_TM" => $with_TM,
            "TM_price" => Request::post("TM_price"),
            "with_CP" => $with_CP,
            "CP_price" => Request::post("CP_price"),
            "is_customize_design_mix" => $is_customize_design_mix,
          
        );
        

        //   dd($data);
         $response = $this->api_model->editSettings($data);

         // dd($response);
         return $response;

    }
    
    public function editAdmixSettings(){

        $less_than_5_array = array();
        $km_5_10_array = array();
        $km_11_15_array = array();
        $km_16_20_array = array();
        $km_21_25_array = array();
        $km_26_30_array = array();

        $setting_data = array();
        $data = array();
        if((Request::post("less_than_5_ad_mixture_brand_id_1")) && (Request::post("less_than_5_ad_mixture_category_id_1")) 
                && (Request::post("less_than_5_ad_mixture_brand_id_2")) && (Request::post("less_than_5_ad_mixture_category_id_2")) 
                && (Request::post("less_than_5_rate"))){

                    $less_than_5_array = array(
                        "min_km" => Request::post("less_than_5_min_km"),
                        "max_km" => Request::post("less_than_5_max_km"),
                        "ad_mixture1_brand_id" => Request::post("less_than_5_ad_mixture_brand_id_1"),
                        "ad_mixture1_category_id" => Request::post("less_than_5_ad_mixture_category_id_1"),
                        "ad_mixture2_brand_id" => Request::post("less_than_5_ad_mixture_brand_id_2"),
                        "ad_mixture2_category_id" => Request::post("less_than_5_ad_mixture_category_id_2"),
                        "price" => Request::post("less_than_5_rate")
                    );

                    array_push($setting_data,$less_than_5_array);
            
        }
        
        if((Request::post("ad_5_10_ad_mixture_brand_id_1")) && (Request::post("ad_5_10_ad_mixture_category_id_1")) 
                && (Request::post("ad_5_10_ad_mixture_brand_id_2")) && (Request::post("ad_5_10_ad_mixture_category_id_2")) 
                && (Request::post("ad_5_10_rate"))){

                    $km_5_10_array = array(
                        "min_km" => Request::post("5_10_min_km"),
                        "max_km" => Request::post("5_10_max_km"),
                        "ad_mixture1_brand_id" => Request::post("ad_5_10_ad_mixture_brand_id_1"),
                        "ad_mixture1_category_id" => Request::post("ad_5_10_ad_mixture_category_id_1"),
                        "ad_mixture2_brand_id" => Request::post("ad_5_10_ad_mixture_brand_id_2"),
                        "ad_mixture2_category_id" => Request::post("ad_5_10_ad_mixture_category_id_2"),
                        "price" => Request::post("ad_5_10_rate")
                    );

                    array_push($setting_data,$km_5_10_array);
            
        }
        
        if((Request::post("ad_11_15_ad_mixture_brand_id_1")) && (Request::post("ad_11_15_ad_mixture_category_id_1")) 
                && (Request::post("ad_11_15_ad_mixture_brand_id_2")) && (Request::post("ad_11_15_ad_mixture_category_id_2")) 
                && (Request::post("ad_11_15_rate"))){
            
                    $km_11_15_array = array(
                        "min_km" => Request::post("11_15_min_km"),
                        "max_km" => Request::post("11_15_max_km"),
                        "ad_mixture1_brand_id" => Request::post("ad_11_15_ad_mixture_brand_id_1"),
                        "ad_mixture1_category_id" => Request::post("ad_11_15_ad_mixture_category_id_1"),
                        "ad_mixture2_brand_id" => Request::post("ad_11_15_ad_mixture_brand_id_2"),
                        "ad_mixture2_category_id" => Request::post("ad_11_15_ad_mixture_category_id_2"),
                        "price" => Request::post("ad_11_15_rate")
                    ); 

                    array_push($setting_data,$km_11_15_array);
        }
        
        if((Request::post("ad_16_20_ad_mixture_brand_id_1")) && (Request::post("ad_16_20_ad_mixture_category_id_1")) 
                && (Request::post("ad_16_20_ad_mixture_brand_id_2")) && (Request::post("ad_16_20_ad_mixture_category_id_2")) 
                && (Request::post("ad_16_20_rate"))){
            
                    $km_16_20_array = array(
                        "min_km" => Request::post("16_20_min_km"),
                        "max_km" => Request::post("16_20_max_km"),
                        "ad_mixture1_brand_id" => Request::post("ad_16_20_ad_mixture_brand_id_1"),
                        "ad_mixture1_category_id" => Request::post("ad_16_20_ad_mixture_category_id_1"),
                        "ad_mixture2_brand_id" => Request::post("ad_16_20_ad_mixture_brand_id_2"),
                        "ad_mixture2_category_id" => Request::post("ad_16_20_ad_mixture_category_id_2"),
                        "price" => Request::post("ad_16_20_rate")
                    ); 

                    array_push($setting_data,$km_16_20_array);

        }
        
        if((Request::post("ad_21_25_ad_mixture_brand_id_1")) && (Request::post("ad_21_25_ad_mixture_category_id_1")) 
                && (Request::post("ad_21_25_ad_mixture_brand_id_2")) && (Request::post("ad_21_25_ad_mixture_category_id_2")) 
                && (Request::post("ad_21_25_rate"))){
            
                    $km_21_25_array = array(
                        "min_km" => Request::post("21_25_min_km"),
                        "max_km" => Request::post("21_25_max_km"),
                        "ad_mixture1_brand_id" => Request::post("ad_21_25_ad_mixture_brand_id_1"),
                        "ad_mixture1_category_id" => Request::post("ad_21_25_ad_mixture_category_id_1"),
                        "ad_mixture2_brand_id" => Request::post("ad_21_25_ad_mixture_brand_id_2"),
                        "ad_mixture2_category_id" => Request::post("ad_21_25_ad_mixture_category_id_2"),
                        "price" => Request::post("ad_21_25_rate")
                    ); 

                    array_push($setting_data,$km_21_25_array);
        }
        
        if((Request::post("ad_26_30_ad_mixture_brand_id_1")) && (Request::post("ad_26_30_ad_mixture_category_id_1")) 
                && (Request::post("ad_26_30_ad_mixture_brand_id_2")) && (Request::post("ad_26_30_ad_mixture_category_id_2")) 
                && (Request::post("ad_26_30_rate"))){

                    $km_26_30_array = array(
                        "min_km" => Request::post("26_30_min_km"),
                        "max_km" => Request::post("26_30_max_km"),
                        "ad_mixture1_brand_id" => Request::post("ad_26_30_ad_mixture_brand_id_1"),
                        "ad_mixture1_category_id" => Request::post("ad_26_30_ad_mixture_category_id_1"),
                        "ad_mixture2_brand_id" => Request::post("ad_26_30_ad_mixture_brand_id_2"),
                        "ad_mixture2_category_id" => Request::post("ad_26_30_ad_mixture_category_id_2"),
                        "price" => Request::post("ad_26_30_rate")
                    ); 

                    array_push($setting_data,$km_26_30_array);
            
        }
        
        if(Request::post("ad_mix_setting_id")){
            // $setting_data["_id"] = Request::post("ad_mix_setting_id");
        }

        $data["settings"] = $setting_data;
        

        //   dd($data);
        if(Request::post("ad_mix_setting_id")){
            $response = $this->api_model->editAdmixSettings($data,Request::post("ad_mix_setting_id"));
        }else{
            $response = $this->api_model->addAdmixSettings($data);
        }

        //  dd($response);
         return $response;

    }
    
    public function orderAceeptOrNot(){

        $new_order_taken = '';
        if(Request::get("new_order_taken") == 'true'){
            $new_order_taken = 'true';
        }else{
            $new_order_taken = 'false';
        }


        $data = array(
            "new_order_taken" => $new_order_taken
          
        );

        if(Request::get("authentication_code")){
            $data["authentication_code"] = Request::get("authentication_code");
        }
        

        //   dd($data);
         $response = $this->api_model->editSettings($data);

         if($response["status"] == 200){

            $profile = session('supplier_profile_details', null);

            $profile["new_order_taken"] = $new_order_taken;
            // dd($new_order_taken);
            session()->put("supplier_profile_details",$profile);

            // dd(session('supplier_profile_details', null));
         }

         // dd($response);
         return $response;

    }

}
