<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use Request;
use Illuminate\Support\Facades\Session;

class ProductModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function checkSuppliersAvailable($product_id,$subcat_id,$request_data, $source_name, $short_by_rating, $short_by_stock){
        // dd(Request::all());
        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if($request_data["lat"]){
            $data["lat"] = (float)$request_data["lat"];
        }
        
        if($request_data["long"]){
            $data["long"] = (float)$request_data["long"];
        }
        
        if($request_data["state_id"]){
            $data["state_id"] = $request_data["state_id"];
        }

        // if($request_data["delivery_date"]){
        //     $data["delivery_date"] = $request_data["delivery_date"];
        // }
        
        if(isset($request_data["selected_delivery_date"])){
            $data["delivery_date"] = $request_data["selected_delivery_date"];
        }
        
        if(isset($request_data["end_date"])){
            $data["end_date"] = $request_data["end_date"];
        }
        
        if(isset($request_data["quantity"])){
            $data["quantity"] = $request_data["quantity"];
        }

        if(Session::get("is_site_selected") == "1"){
            $data["site_id"] = Session::get("selected_site_address");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        $data["sub_category_id"] = "";
        if(isset($subcat_id) && ($subcat_id != 'all_ajax')){
            $data["sub_category_id"] = $subcat_id;
        }
        
        if(!empty($source_name) && ($source_name != 'no_source')){
            $data["source_id"] = $source_name;
        }
        
        if(Request::get("short_by_rating_low_high") && (Request::get("short_by_rating_low_high") != 'no_short_rating_low_high')){
            $data["order_by"] = "rating";
        }
        
        if(Request::get("short_by_rating_high_low") && (Request::get("short_by_rating_high_low") != 'no_short_rating_high_low')){
            $data["order_by"] = "rating:desc";
        }
        
        if(Request::get("short_by_stock_low_high") && (Request::get("short_by_stock_low_high") != 'no_short_stock_low_high')){
            $data["order_by"] = "quantity";
        }
        
        if(Request::get("short_by_stock_high_low") && (Request::get("short_by_stock_high_low") != 'no_short_stock_high_low')){
            $data["order_by"] = "quantity:desc";
        }
        
        if(!empty($product_id)){
            $data["product_id"] = $product_id;
        }
        

        // dd($data);
        $response = $this->api_model->checkSuppliersAvailable($data);
        // dd($response);
        return $response;


    }
    
    public function showProducts($product_id,$subcat_id,$request_data, $source_name, $short_by_rating, $short_by_stock){
        // dd(Request::all());
        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if($request_data["lat"]){
            $data["lat"] = (float)$request_data["lat"];
        }
        
        if($request_data["long"]){
            $data["long"] = (float)$request_data["long"];
        }
        
        if($request_data["state_id"]){
            $data["state_id"] = $request_data["state_id"];
        }

        // if($request_data["delivery_date"]){
        //     $data["delivery_date"] = $request_data["delivery_date"];
        // }
        
        if(isset($request_data["selected_delivery_date"])){
            $data["delivery_date"] = $request_data["selected_delivery_date"];
        }
        
        if(isset($request_data["end_date"])){
            $data["end_date"] = $request_data["end_date"];
        }
        
        if(isset($request_data["quantity"])){
            $data["quantity"] = $request_data["quantity"];
        }

        if(Session::get("is_site_selected") == "1"){
            $data["site_id"] = Session::get("selected_site_address");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        $data["sub_category_id"] = "";
        if(isset($subcat_id) && ($subcat_id != 'all_ajax')){
            $data["sub_category_id"] = $subcat_id;
        }
        
        if(!empty($source_name) && ($source_name != 'no_source')){
            $data["source_id"] = $source_name;
        }
        
        if(Request::get("short_by_rating_low_high") && (Request::get("short_by_rating_low_high") != 'no_short_rating_low_high')){
            $data["order_by"] = "rating";
        }
        
        if(Request::get("short_by_rating_high_low") && (Request::get("short_by_rating_high_low") != 'no_short_rating_high_low')){
            $data["order_by"] = "rating:desc";
        }
        
        if(Request::get("short_by_stock_low_high") && (Request::get("short_by_stock_low_high") != 'no_short_stock_low_high')){
            $data["order_by"] = "quantity";
        }
        
        if(Request::get("short_by_stock_high_low") && (Request::get("short_by_stock_high_low") != 'no_short_stock_high_low')){
            $data["order_by"] = "quantity:desc";
        }
        
        if(!empty($product_id)){
            $data["product_id"] = $product_id;
        }
        

        // dd($data);
        $response = $this->api_model->showProducts($data);
        // dd($response);
        return $response;


    }

    public function getProductSources(){

        $data = array();

        $response = $this->api_model->getProductSources($data);

        return $response;
    }
    

    public function showproductDetails($product_id,$request_data){

        $data = array();

        if($request_data["lat"]){
            $data["lat"] = (float)$request_data["lat"];
        }
        
        if($request_data["long"]){
            $data["long"] = (float)$request_data["long"];
        }
        
        if($request_data["state_id"]){
            $data["state_id"] = $request_data["state_id"];
        }
        
        if($request_data["delivery_date"]){
            $data["delivery_date"] = $request_data["delivery_date"];
        }
        
        if(isset($request_data["with_TM"])){
            $data["with_TM"] = $request_data["with_TM"];
        }
        
        if(isset($request_data["with_CP"])){
            $data["with_CP"] = $request_data["with_CP"];
        }

        if($request_data["end_date"]){
            $data["end_date"] = $request_data["end_date"];
        }
        
        if($request_data["quantity"]){
            $data["quantity"] = $request_data["quantity"];
        }

        if(Session::get("is_site_selected") == "1"){
            $data["site_id"] = Session::get("selected_site_address");
        }

        
        $data["product_id"] = $product_id;
        // dd($data);
        $response = $this->api_model->showProductDetails($data);
        // dd($response);
        return $response;

    }
    
    public function showVendorReview($vendor_id){

        $data = array();        

        $data["vendor_id"] = $vendor_id;

        $response = $this->api_model->showVendorReview($data);
        // dd($response);
        return $response;

    }
    
    public function getAllActiveVendorRatesWithDetails($vendor_id){

        $data = array();        

        $data["vendor_id"] = $vendor_id;

        $response = $this->api_model->getAllActiveVendorRatesWithDetails($data);
        // dd($response);
        return $response;

    }

    public function getSingleDesignMixCombinationByBrandIds($request_data){  
        // dd(Request::all());
        $data = array(
            "grade_id" => Request::get("design_mix_id"),
            "cement_brand_id" => Request::get("cement_brand_id"),
            "cement_grade_id" => Request::get("cement_grade_id"),
            "sand_source_id" => Request::get("sand_source_id"),
            "sand_zone_id" => Request::get("sand_zone_id"),
            "aggr_source_id" => Request::get("aggr_source_id"),
            "aggr_cat_1_id" => Request::get("aggr_cat_1_id"),
            "aggr_cat_2_id" => Request::get("aggr_cat_2_id"),
            "admix_brand_id" => Request::get("admix_brand_id"),
            "admix_cat_id" => Request::get("admix_cat_id"),
        );

        if(Request::get("fly_ash_id") != null){

            $data['fly_ash_id'] = Request::get("fly_ash_id");

        }

        if(Request::get("water_type") != null){

            $data['water_type'] = Request::get("water_type");

        }

        if($request_data["lat"]){
            $data["lat"] = (float)$request_data["lat"];
        }
        
        if($request_data["long"]){
            $data["long"] = (float)$request_data["long"];
        }
        
        if($request_data["state_id"]){
            $data["state_id"] = $request_data["state_id"];
        }
        
        if($request_data["delivery_date"]){
            $data["delivery_date"] = $request_data["delivery_date"];
        }
        
        if(isset($request_data["with_TM"])){
            $data["with_TM"] = $request_data["with_TM"];
        }
        
        if(isset($request_data["with_CP"])){
            $data["with_CP"] = $request_data["with_CP"];
        }

        if($request_data["end_date"]){
            $data["end_date"] = $request_data["end_date"];
        }
        
        if($request_data["quantity"]){
            $data["quantity"] = $request_data["quantity"];
        }

        if(Session::get("is_site_selected") == "1"){
            $data["site_id"] = Session::get("selected_site_address");
        }

        
        $data["product_id"] = Request::get("design_mix_id");
        
        // dd($data);
         $response = $this->api_model->getSingleDesignMixCombinationByBrandIds($data);

         // dd($response);
         return $response;

    }
    
    public function getProductReview($product_id){

        $data = array();

        $data["product_id"] = $product_id;

        $response = $this->api_model->getProductReview($data);
        // dd($response);
        return $response;

    }

    public function addTocart(){
        // dd(Session::get('my_cart_id'));
        // dd(Request::all());
        $lat = "";
        $long = "";
        $site_id = "";
        $state_id = "";
        if(Session::get("selected_lat") != null){
            $lat = Session::get("selected_lat");
        }

        if(Session::get("selected_long") != null){
            $long = Session::get("selected_long");
        }
        
        if(Session::get("is_site_selected") == "1"){
            $site_id = Session::get("selected_site_address");
        }

        if(Session::get("selected_state_id") != null){
            $state_id = Session::get("selected_state_id");
        }

        $data = array(
            
            "quantity" => Request::post("quantity")
            
        );

        if(Request::post("is_custom_mix") == 1){

            $post_custom_mix = Request::post("product_details");            

            $custom_mix = json_decode($post_custom_mix);
            // dd($custom_mix);
            if(Session::get("selected_delivery_date") != null){
                $custom_mix->delivery_date = Session::get("selected_delivery_date").'T00:00:00.000+00:00';
                // $custom_mix->delivery_date =  date("Y-m-d\TH:i:s.000\Z", strtotime(Session::get("selected_delivery_date")." 00:00:00"));;
            }

            if(Request::post("time_cart_delivery_date")){
                $custom_mix->delivery_date = Request::post("time_cart_delivery_date");
            }

            $data["custom_mix"] = $custom_mix;

            if(Request::post("item_id")){ // For only Edit and Delete 

                $data["custom_mix_id"] = Request::post("item_id");
    
            }

            // dd($data["custom_mix"]);

        }else if(Request::post("is_custom_mix") == 0){

            $data["design_mix_id"] = Request::post("design_mix_id");
            $data["design_mix_details"] = Request::post("design_mix_details");

            if(Request::post("item_id")){ // For only Edit and Delete 

                $data["item_id"] = Request::post("item_id");
    
            }

        }

        

        $session_custom_mix_design = Session::get("custom_mix_design");

        $data["with_TM"] = Request::post("with_TM") == "yes" ? 'true' : 'false';
        $data["with_CP"] = Request::post("with_CP") == "yes" ? 'true' : 'false';
        // dd($data);
        if((Request::post("cart_place")) && (Request::post("cart_place") == 'slide_cart')){
            if(isset($session_custom_mix_design)){

                // $with_TM = $session_custom_mix_design["with_TM"];
                // $with_CP = $session_custom_mix_design["with_CP"];

                

                // $data["with_TM"] = $session_custom_mix_design["with_TM"] == "yes" ? 'true' : 'false';
                // $data["with_CP"] = $session_custom_mix_design["with_CP"] == "yes" ? 'true' : 'false';

                if(Request::post("with_TM")){
                    $data["with_TM"] = Request::post("with_TM") == "yes" ? 'true' : 'false';

                    $session_custom_mix_design["with_TM"] = $data["with_TM"];

                    Session::put("custom_mix_design",$session_custom_mix_design);
                }else{
                    $data["with_TM"] = $session_custom_mix_design["with_TM"] == "yes" ? 'true' : 'false';
                }
                
                if(Request::post("with_CP")){
                    $data["with_CP"] = Request::post("with_CP") == "yes" ? 'true' : 'false';

                    $session_custom_mix_design["with_CP"] = $data["with_CP"];

                    Session::put("custom_mix_design",$session_custom_mix_design);
                }else{
                    $data["with_CP"] = $session_custom_mix_design["with_CP"] == "yes" ? 'true' : 'false';
                }
                

                

            }
        }

        if(isset($data["with_TM"]) && ($data["with_TM"] == 'false') ){

            $buyer_TM_details = Session::get("buyer_TM_details",null);

            if($buyer_TM_details != null){

                $data["TM_no"] = $buyer_TM_details["TM_no"];
                $data["driver_name"] = $buyer_TM_details["driver_name"];
                $data["driver_mobile"] = $buyer_TM_details["driver_mobile"];
                $data["TM_operator_name"] = $buyer_TM_details["TM_operator_name"];
                $data["TM_operator_mobile_no"] = $buyer_TM_details["TM_operator_mobile_no"];

            }

            if(Request::post("TM_no")){

                $data["TM_no"] = Request::post("TM_no");
                $data["driver_name"] = Request::post("driver_name");
                $data["driver_mobile"] = Request::post("driver_mobile");
                $data["TM_operator_name"] = Request::post("TM_operator_name");
                $data["TM_operator_mobile_no"] = Request::post("TM_operator_mobile_no");


            }

        }

        if(Session::get("selected_delivery_date") != null){
            $data["delivery_date"] = Session::get("selected_delivery_date");
        }
        
        if(Session::get("selected_end_delivery_date") != null){
            $data["end_date"] = Session::get("selected_end_delivery_date");
        }

        if(Request::post("time_cart_delivery_date")){
            $data["delivery_date"] = date('Y-m-d',strtotime(Request::post("time_cart_delivery_date")));
        }

        if(Request::post("time_cart_end_delivery_date") != null){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("time_cart_end_delivery_date")));
        }
        
        if(Request::post("time_cart_design_mix_detail_id") != null){
            $data["cart_design_mix_detail_id"] = Request::post("time_cart_design_mix_detail_id");
        }

        if(!empty($lat)){
            $data["lat"] = $lat;
        }
        
        if(!empty($long)){
            $data["long"] = $long;
        }
        
        if(!empty($site_id)){
            $data["site_id"] = $site_id;
        }
        
        if(!empty($state_id)){
            $data["state_id"] = $state_id;
        }
        // echo json_encode($data);
        // dd($data);
        $response = $this->api_model->addTocart($data);
        // dd($response);

        if($response["status"] == 200){

            $status = $response["data"]["status"];

            if($status == 'error'){

            }else{

                $cart_id = $response["data"]["_id"];
                // dump($cart_id);
                // session()->put("my_cart_id",$cart_id);
                Session::put('my_cart_id', $cart_id);
                // dd(Session::get('my_cart_id'));
                // session()->save();
                // dump(session('my_cart_id', null));

            }
            
        }

        return $response;

    }

    public function getCart(){
        $lat = "";
        $long = "";

        if(Session::get("selected_lat") != null){
            $lat = Session::get("selected_lat");
        }

        if(Session::get("selected_long") != null){
            $long = Session::get("selected_long");
        }


        $data = array();

        if(!empty($lat)){
            $data["lat"] = $lat;
        }
        
        if(!empty($long)){
            $data["long"] = $long;
        }

        $response = $this->api_model->getCart($data);

        return $response;

    }

    public function addToWishList(){

        $data = array(
            "product_id" => Request::post("product_id"),
            "quantity" => Request::post("quantity")
        );

        $response = $this->api_model->addToWishList($data);
        // dd($response);

        // if($response["status"] == 200){

        //     $cart_id = $response["data"]["_id"];
        //     // dump($cart_id);
        //     // session()->put("my_cart_id",$cart_id);
        //     Session::put('my_cart_id', $cart_id);
        //     // dd(Session::get('my_cart_id'));
        //     // session()->save();
        //     // dump(session('my_cart_id', null));
        // }

        return $response;


    }

    public function getWishList(){

        $data = array();
        // dd(Session::get("selected_state_id"));
        $lat = "";
        $long = "";
        $state_id = "";
        if(Session::get("selected_lat") != null){
            $lat = Session::get("selected_lat");
        }

        if(Session::get("selected_long") != null){
            $long = Session::get("selected_long");
        }
        
        if(Session::get("selected_state_id") != null){
            $state_id = Session::get("selected_state_id");
        }

        if(!empty($lat)){
            $data["lat"] = $lat;
        }
        
        if(!empty($long)){
            $data["long"] = $long;
        }
        if(!empty($state_id)){
            $data["state_id"] = $state_id;
        }

        $response = $this->api_model->getWishList($data);

        return $response;

    }

    public function deleteWishListItem(){

        $data = array(
            "wishlistedId" => Request::post("wish_list_id")
        );

        $response = $this->api_model->deleteWishListItem($data);
        // dd($response);

        return $response;


    }
}
