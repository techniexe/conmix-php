<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use App\ApiConfig;
use App\BaseModel;

class ApisModel extends BaseModel
{
    //
    public function getSessionToken(){

        $url_path = "".ApiConfig::SESSION_TOKEN_URL;
        $params = array(
            "accessToken"=> ApiConfig::ACCESS_TOKEN,
            "registrationToken"=> ApiConfig::REGISTRATION_TOKEN
        );
        $auth_resposnse = $this->requestPOST($url_path,$params);

        return $auth_resposnse;
    }

    public function login($data){
        // dd($data);
        $url_path = "".sprintf(ApiConfig::BUYER_LOGIN_URL,$data["email_mobile"]);
        $params = array(
            "password"=> $data["password"]
        );
        $this->setTokenHeader($data["session_token"]);
        $login_resposnse = $this->requestPOST($url_path,$params);
        // dd($login_resposnse);
        return $login_resposnse;

    }

    public function signupOTPSendViaEmail($data){

        $url_path = sprintf(ApiConfig::BUYER_SIGNUP_OTP_SEND_VIA_EMAIL_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function signupOTPSendViaMobile($data){

        $url_path = sprintf(ApiConfig::BUYER_SIGNUP_OTP_SEND_VIA_MOBILE_URL,$data["email_mobile"]);
        $params = $data;
        // dd($url_path);
        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function signupOTPVerify($data){

        $url_path = sprintf(ApiConfig::BUYER_SIGNUP_OTP_VERIFY_URL,$data["email_mobile"],$data["code"]);
        $params = $data;
        // dd($params);
        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestPOST($url_path,$params);
        // dd($auth_resposnse);
        return $auth_resposnse;

    }   

    public function buyerRegistration($data,$files){

        $url_path = sprintf(ApiConfig::BUYER_SIGNUP_REGISTRATION_URL,$data["signup_type"]);
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }


    public function forgotPassOTPSendViaEmail($data){

        $url_path = sprintf(ApiConfig::BUYER_FORGOT_PASS_OTP_SEND_VIA_EMAIL_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function forgotPassOTPSendViaMobile($data){

        $url_path = sprintf(ApiConfig::BUYER_FORGOT_PASS_OTP_SEND_VIA_MOBILE_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);
        // dd($auth_resposnse);
        return $auth_resposnse;

    }

    public function forgotPassOTPVerify($data){

        $url_path = sprintf(ApiConfig::BUYER_FORGOT_PASS_OTP_VERIFY_URL,$data["email_mobile"],$data["code"]);
        $params = $data;
        // dd($url_path);
        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestPOST($url_path,$params);

        return $auth_resposnse;

    }
    
    public function forgotPass($data){

        $url_path = ApiConfig::BUYER_FORGOT_PASS_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestPATCH($url_path,$params);

        return $auth_resposnse;

    }


//Product Module.......................................

    public function checkSuppliersAvailable($data){

        // if(!empty($data["sub_category_id"])){
        //     if(!empty($data["source_id"])){
        //         $url_path = sprintf(ApiConfig::BUYER_PRODUCT_WITH_SUBCAT_SOURCE_LISTING_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"],$data["sub_category_id"],$data["source_id"]);
        //     }else{
        //         $url_path = sprintf(ApiConfig::BUYER_PRODUCT_WITH_SUBCAT_LISTING_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"],$data["sub_category_id"]);
        //     }
            
        // }else{
        //     if(!empty($data["source_id"])){
        //         $url_path = sprintf(ApiConfig::BUYER_PRODUCT_WITH_SOURCE_LISTING_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"],$data["source_id"]);
        //     }else if(isset($data["product_id"]) && !empty($data["product_id"])){
        //         $url_path = sprintf(ApiConfig::BUYER_PRODUCT_LISTING_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"]);
        //     }else{
                $url_path = sprintf(ApiConfig::BUYER_SUPPLIER_CHECK_URL,$data["lat"],$data["long"],$data["state_id"]);
        //     }
            
        // }

        // if(isset($data["site_id"])){
        //     $url_path .= "&site_id=".$data["site_id"];
        // }
        
        // if(isset($data["delivery_date"])){
        //     $url_path .= "&delivery_date=".$data["delivery_date"];
        // }
        
        // if(isset($data["end_date"])){
        //     $url_path .= "&end_date=".$data["end_date"];
        // }
        
        // if(isset($data["quantity"])){
        //     $url_path .= "&quantity=".$data["quantity"];
        // }
        // // dd($url_path);
        // if(isset($data["order_by"])){

        //     $url_path = $url_path . "&order_by=".$data["order_by"];


        // }

        // dd($url_path);
        
        $params = array();
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);
        // dd($auth_resposnse);
        return $auth_resposnse;

    }
    
    public function showProducts($data){

        if(!empty($data["sub_category_id"])){
            if(!empty($data["source_id"])){
                $url_path = sprintf(ApiConfig::BUYER_PRODUCT_WITH_SUBCAT_SOURCE_LISTING_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"],$data["sub_category_id"],$data["source_id"]);
            }else{
                $url_path = sprintf(ApiConfig::BUYER_PRODUCT_WITH_SUBCAT_LISTING_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"],$data["sub_category_id"]);
            }
            
        }else{
            if(!empty($data["source_id"])){
                $url_path = sprintf(ApiConfig::BUYER_PRODUCT_WITH_SOURCE_LISTING_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"],$data["source_id"]);
            }else if(isset($data["product_id"]) && !empty($data["product_id"])){
                $url_path = sprintf(ApiConfig::BUYER_PRODUCT_LISTING_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"]);
            }else{
                $url_path = sprintf(ApiConfig::BUYER_SUPPLIER_CHECK_URL,$data["lat"],$data["long"],$data["state_id"]);
            }
            
        }

        if(isset($data["site_id"])){
            $url_path .= "&site_id=".$data["site_id"];
        }
        
        if(isset($data["delivery_date"])){
            $url_path .= "&delivery_date=".$data["delivery_date"];
        }
        
        if(isset($data["end_date"])){
            $url_path .= "&end_date=".$data["end_date"];
        }
        
        if(isset($data["quantity"])){
            $url_path .= "&quantity=".$data["quantity"];
        }
        // dd($url_path);
        if(isset($data["order_by"])){

            $url_path = $url_path . "&order_by=".$data["order_by"];


        }

        // dd($url_path);
        
        $params = array();
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);
        // dd($auth_resposnse);
        return $auth_resposnse;

    }
    
    public function showProductDetails($data){

        $url_path = sprintf(ApiConfig::BUYER_PRODUCT_DETAILS_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"],$data["delivery_date"]);

        if(isset($data["site_id"])){
            $url_path .= "&site_id=".$data["site_id"];
        }
        
        if(isset($data["with_TM"])){
            $url_path .= "&with_TM=".$data["with_TM"];
        }
        
        if(isset($data["with_CP"])){
            $url_path .= "&with_CP=".$data["with_CP"];
        }

        $params = array();
        // dump($url_path);
        // dd(session('buyer_custom_token', null));
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);
        // dd($auth_resposnse);
        return $auth_resposnse;

    }
    
    public function showVendorReview($data){

        $url_path = sprintf(ApiConfig::BUYER_VENDOR_REVIEW_URL,$data["vendor_id"]);
        $params = array();
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function getAllActiveVendorRatesWithDetails($data){

        $url_path = sprintf(ApiConfig::BUYER_VENDOR_MATERIAL_DETAIL_URL,$data["vendor_id"]);
        $params = array();
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function getSingleDesignMixCombinationByBrandIds($data){

        $url_path = sprintf(ApiConfig::BUYER_SINGLE_PRODUCT_COMBINATION_DETAILS_URL,$data["product_id"],$data["lat"],$data["long"],$data["state_id"],$data["delivery_date"]);

        if(isset($data["site_id"])){
            $url_path .= "&site_id=".$data["site_id"];
        }
        
        if(isset($data["with_TM"])){
            $url_path .= "&with_TM=".$data["with_TM"];
        }
        
        if(isset($data["with_CP"])){
            $url_path .= "&with_CP=".$data["with_CP"];
        }

        $params = $data;
        // dump($url_path);
        // dd(session('buyer_custom_token', null));
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);
        // dd($auth_resposnse);
        return $auth_resposnse;


    }
    
    public function getProductReview($data){

        $url_path = sprintf(ApiConfig::BUYER_PRODUCT_REVIEW_URL,$data["product_id"]);
        $params = array();
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function getProductSources($data){

        $url_path = ApiConfig::BUYER_PRODUCT_SOURCES_URL;
        $params = array();
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);
        // dd($auth_resposnse);
        return $auth_resposnse;

    }

//Product Module Over....................................................


//Profile Module Start....................................................

    public function getBuyerProfile(){

        $url_path = ApiConfig::BUYER_PROFILE_DETAIL_URL;
        $params = array();
        // dd($url_path);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function editBuyerProfile($data,$files){

        $url_path = ApiConfig::BUYER_PROFILE_EDIT_URL;
        $params = $data;

        $response = $this->requestPATCH_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function changePass($data){

        $url_path = ApiConfig::BUYER_CHANGE_PASS_URL;
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function requestOTP($data){
        $url_path = ApiConfig::BUYER_REQUEST_OTP_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function requestOTPForEditProfile($data){
        $url_path = ApiConfig::BUYER_REQUEST_OTP_FOR_EDIT_PROFILE_URL;
        $params = $data;
        // dd($params);
        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function addSites($data){

        $url_path = ApiConfig::BUYER_PROFILE_ADD_SITE_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function checkSiteExist($data){

        $url_path = ApiConfig::BUYER_PROFILE_CHECK_SITE_EXIST_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    

    public function showSites(){

        $url_path = ApiConfig::BUYER_PROFILE_SITE_LISTING_URL;
        $params = array();
        // dd($url_path);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function editSites($data){

        $url_path = sprintf(ApiConfig::BUYER_PROFILE_EDIT_SITE_URL,$data["site"]["site_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteSites($data){

        $url_path = sprintf(ApiConfig::BUYER_PROFILE_DELETE_SITE_URL,$data["site_id"]);
        $params = $data;
        // dd($url_path);
        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteBilling($data){

        $url_path = sprintf(ApiConfig::BUYER_BILLING_ADDRESS_DELETE_URL,$data["billing_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }

    public function showBillngAddress(){

        $url_path = ApiConfig::BUYER_BILLING_ADDRESS_LISTING_URL;
        $params = array();
        // dd($url_path);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function addBillngAddress($data){

        $url_path = ApiConfig::BUYER_BILLING_ADDRESS_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editBillngAddress($data){

        $url_path = sprintf(ApiConfig::BUYER_BILLING_ADDRESS_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showCustomRMC(){

        $url_path = ApiConfig::BUYER_CUSTOM_RMC_LISTING_URL;
        $params = array();
        // dd($url_path);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function addCustomRMC($data){

        $url_path = ApiConfig::BUYER_CUSTOM_RMC_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editCustomRMC($data){

        $url_path = sprintf(ApiConfig::BUYER_CUSTOM_RMC_EDIT_URL,$data["custom_mix_edit_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function deleteCustomRMC($data){

        $url_path = sprintf(ApiConfig::BUYER_CUSTOM_RMC_DELETE_URL,$data["custom_rmc_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }
//Profile Module Over....................................................

// Support Ticket Module Start.........................................

public function showSupportTickets($data){

    $url_path = ApiConfig::BUYER_SUPPORT_TICKET_LISTING_URL;
    $params = $data;

    $response = $this->requestGET($url_path,$params);
    
    return $response;

}

public function addSupportTicket($data,$files){

    $url_path = ApiConfig::BUYER_SUPPORT_TICKET_ADD_URL;
    $params = $data;

    $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
    // dd($response);
    return $response;

}

public function showTicketDetails($data){

    $url_path = sprintf(ApiConfig::BUYER_SUPPORT_TICKET_DETAIL_URL,$data["ticket_id"]);
    $params = $data;

    $response = $this->requestGET($url_path,$params);
    
    return $response;

}

public function replySupportTicket($data,$files){

    $url_path = ApiConfig::BUYER_SUPPORT_TICKET_REPLY_URL;
    $params = $data;

    $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
    // dd($response);
    return $response;

}

public function getTicketMessages($data){

    $url_path = sprintf(ApiConfig::BUYER_SUPPORT_TICKET_MESSAGE_URL,$data["ticket_id"]);
    $params = array();

    $response = $this->requestGET($url_path,$params);
    // dd($response);
    return $response;

}

// Support Ticket Module Over.........................................
// Product Module Start.........................................

public function addTocart($data){

    $url_path = ApiConfig::ADD_TO_CART_URL;
    $params = $data;
    // dd(json_encode($params));
    $response = $this->requestPOST($url_path,$params);
    // dd($response);
    return $response;

}

public function addToWishList($data){

    $url_path = ApiConfig::ADD_TO_WISH_LIST_URL;
    $params = $data;

    $response = $this->requestPOST($url_path,$params);
    // dd($response);
    return $response;

}

public function deleteWishListItem($data){

    $url_path = sprintf(ApiConfig::DELETE_WISH_LIST_ITEM_URL,$data["wishlistedId"]);
    $params = $data;
    // dd($url_path);
    $response = $this->requestDELETE($url_path,$params);
    // dd($response);
    return $response;

}

public function deleteCartItem($data){

    $url_path = ApiConfig::DELETE_CART_ITEM_URL;
    $params = $data;
    // dd($url_path);
    $response = $this->requestDELETE($url_path,$params);
    // dd($response);
    return $response;

}

public function getWishList($data){

    $url_path = ApiConfig::GET_WISH_LIST_URL;
    $params = $data;

    $response = $this->requestGET($url_path,$params);
    // dd($response);
    return $response;

}

public function getCart($data){

    $url_path = ApiConfig::GET_CART_URL;
    $params = $data;

    $response = $this->requestGET($url_path,$params);
    // dd($response);
    return $response;

}

public function addSiteIdToCart($data){

    $url_path = ApiConfig::ADD_SITE_TO_CART_URL;
    $params = $data;

    $response = $this->requestPOST($url_path,$params);
    // dd($response);
    return $response;

}

public function addBiilingIdToCart($data){

    $url_path = ApiConfig::ADD_BILLING_ADD_TO_CART_URL;
    $params = $data;

    $response = $this->requestPOST($url_path,$params);
    // dd($response);
    return $response;

}

public function placeOrder($data){

    $url_path = sprintf(ApiConfig::PLACE_ORDER_URL,$data["cart_id"]);
    $params = $data;
    // dd($params);
    $response = $this->requestPOST($url_path,$params);
    // dd($response);
    return $response;

}

public function orderProcess($order_id){

    $url_path = sprintf(ApiConfig::PLACE_ORDER_PROCESS,$order_id);
    $params = array();

    $response = $this->requestGET($url_path,$params);
    // dd($response);
    return $response;

}



// Product Module Over.........................................

// Order Module Start.........................................

public function showOrders($data){

    $url_path = ApiConfig::BUYER_ORDERS_LIST_URL;
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $auth_resposnse = $this->requestGET($url_path,$params);

    return $auth_resposnse;

}

public function showOrderDetails($data){

    $url_path = sprintf(ApiConfig::BUYER_ORDERS_DETAILS_URL,$data["order_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $auth_resposnse = $this->requestGET($url_path,$params);
    // dd($auth_resposnse);
    return $auth_resposnse;

}

public function cancelOrder($data){

    $url_path = sprintf(ApiConfig::ORDER_CANCEL_URL,$data["order_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function cancelOrderItem($data){

    $url_path = sprintf(ApiConfig::ORDER_ITEM_CANCEL_URL,$data["item_id"],$data["order_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function orderComplain($data){

    $url_path = ApiConfig::ORDER_COMPLAIN_URL;
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function orderFeedback($data){

    $url_path = ApiConfig::ORDER_FEEDBACK_URL;
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function assignQtyOrder($data){

    $url_path = sprintf(ApiConfig::ORDER_ASSIGN_QTY_URL,$data["order_id"],$data["order_item_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function reassignStatusChange($data){

    $url_path = sprintf(ApiConfig::ORDER_REASSIGN_STATUS_CHANGE_URL);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function acceptReject7And28DaysReport($data){
    
    $url_path = sprintf(ApiConfig::BUYER_ORDER_BILL_UPLOAD_URL,$data["track_id"],$data["TM_Id"],$data["vendor_order_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST_WithMultipleFile($url_path,$params);
    // $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function reject7And28DaysReport($data,$files){
    
    $url_path = sprintf(ApiConfig::BUYER_ORDER_BILL_UPLOAD_URL,$data["track_id"],$data["TM_Id"],$data["vendor_order_id"]);
    $params = $data;
    // dd($url_path);
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
    // $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function checkTMAvailability($data){

    $url_path = ApiConfig::ORDER_CHECK_TM_AVAILABILITY_URL;
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function addProductReview($data){

    $url_path = ApiConfig::ORDER_PRODUCT_REVIEW_URL;
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function updateProductReview($data){

    $url_path = sprintf(ApiConfig::ORDER_PRODUCT_REVIEW_UPDATE_URL,$data["review_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPATCH($url_path,$params);

    return $resposnse;

}

public function trackOrderItem($data){

    $url_path = sprintf(ApiConfig::ORDER_PRODUCT_TRACK_URL,$data["item_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestGET($url_path,$params);
    // dd($resposnse);
    return $resposnse;

}

public function trackingDetails($data){

    $url_path = sprintf(ApiConfig::ORDER_TRACK_DETAILS_URL,$data["item_id"],$data["tracking_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestGET($url_path,$params);
    // dd($resposnse);
    return $resposnse;

}

public function CPtrackingDetails($data){

    $url_path = sprintf(ApiConfig::ORDER_TRACK_CP_DETAILS_URL,$data["order_item_part_id"]);
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestGET($url_path,$params);

    return $resposnse;

}

// Order Module Over.........................................

// Custom Design Module Start.........................................

public function showCustomDesign($data){

    $url_path = ApiConfig::BUYER_CUSTOM_MIX_DESIGN_URL;
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

public function showCustomDetail($vendor_id,$data){

    $url_path = sprintf(ApiConfig::BUYER_CUSTOM_MIX_DESIGN_DETAIL_URL,$vendor_id);
    $params = $data;
    // dd($params);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestPOST($url_path,$params);

    return $resposnse;

}

// Custom Design Module Over.........................................

public function getCoupons($data){

    $url_path = ApiConfig::BUYER_GET_COUPON_URL;
    $params = $data;
    // dd($url_path);
    // $this->setTokenHeader($data["session_token"]);

    $resposnse = $this->requestGET($url_path,$params);
    // dd($resposnse);
    return $resposnse;

}

public function apply_coupon($data){

    $url_path = ApiConfig::BUYER_GET_APPLY_URL;
    $params = $data;

    $response = $this->requestPOST($url_path,$params);
    // dd($response);
    return $response;
}

}
