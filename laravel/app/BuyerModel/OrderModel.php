<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class OrderModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showOrders(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("site_id")){
            $data["site_id"] = Request::get("site_id");
        }
        
        if(Request::get("order_status")){
            $data["order_status"] = Request::get("order_status");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';;
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("total_amount_range") && Request::get("total_amount")){
            $data["total_amount_range"] = Request::get("total_amount_range");
            $data["total_amount"] = Request::get("total_amount");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // $data["product_id"] = $product_id;

        // dd($data);
        $response = $this->api_model->showOrders($data);
        // dd($response);
        return $response;


    }
    
    public function showOrderDetails($order_id){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $data["order_id"] = $order_id;

        // dd($data);
        $response = $this->api_model->showOrderDetails($data);
        // dd($response);
        return $response;


    }

    public function cancelOrder(){

        $data = array(
            "order_id" => Request::post("order_id")
        );

        $response = $this->api_model->cancelOrder($data);
        // dd($response);
        return $response;
    }
    
    public function cancelOrderItem(){

        $data = array(
            "order_id" => Request::get("order_id"),
            "item_id" => Request::get("item_id")
        );

        if((Request::get("gateway_transaction_id") != null) && !empty(Request::get("gateway_transaction_id"))){

            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");

        }

        $response = $this->api_model->cancelOrderItem($data);
        // dd($response);
        return $response;
    }
    
    public function orderComplain(){

        $data = array();

        if(Request::post("order_id")){
            $data["order_id"] = Request::post("order_id");
        }
        
        if(Request::post("complaint_text")){
            $data["complaint_text"] = Request::post("complaint_text");
        }
        
        if(Request::post("complaint_type")){
            $data["complaint_type"] = Request::post("complaint_type");
        }
        
        if(Request::post("category_id")){
            $data["category_id"] = Request::post("category_id");
        }
        
        if(Request::post("sub_category_id")){
            $data["sub_category_id"] = Request::post("sub_category_id");
        }

        $final_data = array(
            "complaint" => $data
        );

        $response = $this->api_model->orderComplain($final_data);
        // dd($response);
        return $response;
    }
    
    public function orderFeedback(){

        $data = array();

        if(Request::post("order_id")){
            $data["order_id"] = Request::post("order_id");
        }
        
        if(Request::post("feedback_description")){
            $data["feedback"] = Request::post("feedback_description");
        }
        
        if(Request::post("feedback_type")){
            $data["feedback_type"] = Request::post("feedback_type");
        }
        
        // if(Request::post("category_id")){
        //     $data["category_id"] = Request::post("category_id");
        // }
        
        // if(Request::post("sub_category_id")){
        //     $data["sub_category_id"] = Request::post("sub_category_id");
        // }

        // $final_data = array(
        //     "complaint" => $data
        // );

        $response = $this->api_model->orderFeedback($data);
        // dd($response);
        return $response;
    }
    
    public function assignQtyOrder(){

        // dd(Request::all());

        $data = array();

        if(Request::post("order_id")){
            $data["order_id"] = Request::post("order_id");
        }
        
        if(Request::post("order_item_id")){
            $data["order_item_id"] = Request::post("order_item_id");
        }

        $delivery_date = date('Y-m-d',strtotime(Request::post("delivery_date")));
        $delivery_start_date_time = date('Y-m-d',strtotime(Request::post("order_start_date_time"))).'T'.date('H:i',strtotime(Request::post("order_start_date_time"))).':00+00:00';
        $delivery_end_date_time = date('Y-m-d',strtotime(Request::post("order_end_date_time"))).'T'.date('H:i',strtotime(Request::post("order_end_date_time"))).':00+00:00';
        if(Request::post("delivery_date")){
            $data["delivery_date"] = date('Y-m-d',strtotime(Request::post("delivery_date")));
        }
        
        if(Request::post("assigned_quantity")){
            $data["assigned_quantity"] = Request::post("assigned_quantity");
        }
        
        if($delivery_start_date_time){
            $data["start_time"] = $delivery_start_date_time;
        }
        
        if($delivery_end_date_time){
            $data["end_time"] = $delivery_end_date_time;
        }

        // dd($data);
        $response = $this->api_model->assignQtyOrder($data);
        // dd($response);
        return $response;
    }
    
    public function reassignStatusChange(){

        // dd(Request::all());

        $data = array();

        if(Request::post("reassign_status")){
            $data["accept_reject"] = Request::get("reassign_status");
        }
        
        if(Request::post("order_item_part_id")){
            $data["order_item_part_id"] = Request::get("order_item_part_id");
        }

        // $delivery_date = date('Y-m-d',strtotime(Request::post("delivery_date")));
        // $delivery_start_date_time = date('Y-m-d',strtotime(Request::post("order_start_date_time"))).'T'.date('H:i',strtotime(Request::post("order_start_date_time"))).':00+00:00';
        // $delivery_end_date_time = date('Y-m-d',strtotime(Request::post("order_end_date_time"))).'T'.date('H:i',strtotime(Request::post("order_end_date_time"))).':00+00:00';
        // if(Request::post("delivery_date")){
        //     $data["delivery_date"] = date('Y-m-d',strtotime(Request::post("delivery_date")));
        // }
        
        // if(Request::post("assigned_quantity")){
        //     $data["assigned_quantity"] = Request::post("assigned_quantity");
        // }
        
        // if($delivery_start_date_time){
        //     $data["start_time"] = $delivery_start_date_time;
        // }
        
        // if($delivery_end_date_time){
        //     $data["end_time"] = $delivery_end_date_time;
        // }

        // dd($data);
        $response = $this->api_model->reassignStatusChange($data);
        // dd($response);
        return $response;
    }
    
    public function accept7And28DaysReport(){

        // dd(Request::all());

        $data = array(
            'vendor_order_id' => Request::get('vendor_order_id'),
            'track_id' => Request::get('track_id'),
            'TM_Id' => Request::get('TM_Id'),
        );

        if(Request::get("qube_test_report_7days_accept_reject")){
            $data["qube_test_report_7days_accept_reject"] = (int)Request::get("qube_test_report_7days_accept_reject");
        }    
        
        if(Request::get("qube_test_report_28days_accept_reject")){
            $data["qube_test_report_28days_accept_reject"] = Request::get("qube_test_report_28days_accept_reject");
        }        

        // $delivery_date = date('Y-m-d',strtotime(Request::post("delivery_date")));
        // $delivery_start_date_time = date('Y-m-d',strtotime(Request::post("order_start_date_time"))).'T'.date('H:i',strtotime(Request::post("order_start_date_time"))).':00+00:00';
        // $delivery_end_date_time = date('Y-m-d',strtotime(Request::post("order_end_date_time"))).'T'.date('H:i',strtotime(Request::post("order_end_date_time"))).':00+00:00';
        // if(Request::post("delivery_date")){
        //     $data["delivery_date"] = date('Y-m-d',strtotime(Request::post("delivery_date")));
        // }
        
        // if(Request::post("assigned_quantity")){
        //     $data["assigned_quantity"] = Request::post("assigned_quantity");
        // }
        
        // if($delivery_start_date_time){
        //     $data["start_time"] = $delivery_start_date_time;
        // }
        
        // if($delivery_end_date_time){
        //     $data["end_time"] = $delivery_end_date_time;
        // }

        // dd($data);
        $response = $this->api_model->acceptReject7And28DaysReport($data);
        // dd($response);
        return $response;
    }
    
    public function reject7And28DaysReport(){

        // dd(Request::all());

        $data = array(
            'vendor_order_id' => Request::post('vendor_order_id'),
            'track_id' => Request::post('track_id'),
            'TM_Id' => Request::post('TM_Id'),
        );

        $files = array();

        if(Request::get("qube_test_report_7days_accept_reject")){
            $data["qube_test_report_7days_accept_reject"] = (int)Request::post("qube_test_report_7days_accept_reject");
            $data["qube_test_report_7days_reject_reason"] = Request::post("qube_test_report_7days_reject_reason");

            if(Request::file('buyer_qube_test_report_7days')){

                $image = Request::file('buyer_qube_test_report_7days');       
    
                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "buyer_qube_test_report_7days"
                ));
    
                
            }
        }       
        
        if(Request::get("qube_test_report_28days_accept_reject")){
            $data["qube_test_report_28days_accept_reject"] = (int)Request::post("qube_test_report_28days_accept_reject");
            $data["qube_test_report_28days_reject_reason"] = Request::post("qube_test_report_28days_reject_reason");

            if(Request::file('buyer_qube_test_report_28days')){

                $image = Request::file('buyer_qube_test_report_28days');       
    
                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image_mime" => $image->getClientMimeType(),
                    "image" => $image,
                    "param_key" => "buyer_qube_test_report_28days"
                ));
    
                
            }
        }        

        // $delivery_date = date('Y-m-d',strtotime(Request::post("delivery_date")));
        // $delivery_start_date_time = date('Y-m-d',strtotime(Request::post("order_start_date_time"))).'T'.date('H:i',strtotime(Request::post("order_start_date_time"))).':00+00:00';
        // $delivery_end_date_time = date('Y-m-d',strtotime(Request::post("order_end_date_time"))).'T'.date('H:i',strtotime(Request::post("order_end_date_time"))).':00+00:00';
        // if(Request::post("delivery_date")){
        //     $data["delivery_date"] = date('Y-m-d',strtotime(Request::post("delivery_date")));
        // }
        
        // if(Request::post("assigned_quantity")){
        //     $data["assigned_quantity"] = Request::post("assigned_quantity");
        // }
        
        // if($delivery_start_date_time){
        //     $data["start_time"] = $delivery_start_date_time;
        // }
        
        // if($delivery_end_date_time){
        //     $data["end_time"] = $delivery_end_date_time;
        // }

        // dd($data);
        $response = $this->api_model->reject7And28DaysReport($data,$files);
        // dd($response);
        return $response;
    }
    
    public function checkTMAvailability(){

        // dd(Request::all());

        $data = array();

        if(Request::post("vendor_id")){
            $data["vendor_id"] = Request::post("vendor_id");
        }

        $delivery_date = date('Y-m-d',strtotime(Request::post("delivery_date")));
        $delivery_start_date_time = date('Y-m-d',strtotime(Request::post("start_time"))).'T'.date('H:i',strtotime(Request::post("start_time"))).':00+00:00';
        $delivery_end_date_time = date('Y-m-d',strtotime(Request::post("end_time"))).'T'.date('H:i',strtotime(Request::post("end_time"))).':00+00:00';
        
        if(Request::post("delivery_date")){
            $data["delivery_date"] = date('Y-m-d',strtotime(Request::post("delivery_date")));
        }
        
        if(Request::post("start_time")){
            $data["start_time"] = $delivery_start_date_time;
        }
        
        if(Request::post("end_time")){
            $data["end_time"] = $delivery_end_date_time;
        }

        // dd($data);
        $response = $this->api_model->checkTMAvailability($data);
        // dd($response);
        return $response;
    }

    public function addProductReview(){

        $data = array();

        if(Request::post("vendor_id")){
            $data["vendor_id"] = Request::post("vendor_id");
        }
        
        if(Request::post("review_text")){
            $data["review_text"] = Request::post("review_text");
        }
        
        if(Request::post("rating")){
            $data["rating"] = Request::post("rating");
        }
        
        if(Request::post("status")){
            $data["status"] = Request::post("status");
        }
        
        if(Request::post("review_id")){
            $data["review_id"] = Request::post("review_id");
        }
        
        if(Request::post("order_item_id")){
            $data["order_item_id"] = Request::post("order_item_id");
        }
        


        // if(Request::post("review_form_type") == 'add'){
            $response = $this->api_model->addProductReview($data);
        // }else{
        //     $response = $this->api_model->updateProductReview($data);
        // }
        
        
        // dd($response);
        return $response;

    }

    public function trackOrderItem($item_id){

        $data = array(
            "item_id" => $item_id
        );

       
        

        $response = $this->api_model->trackOrderItem($data);
        // dd($response);
        return $response;

    }
    
    public function trackingDetails($item_id,$tracking_id){

        $data = array(
            "item_id" => $item_id,
            "tracking_id" => $tracking_id
        );      
        

        $response = $this->api_model->trackingDetails($data);
        // dd($response);
        return $response;

    }
    
    public function CPtrackingDetails($item_part_id){

        $data = array(
            "order_item_part_id" => $item_part_id,
        );      
        

        $response = $this->api_model->CPtrackingDetails($data);
        // dd($response);
        return $response;

    }

}
