<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ForgotPassModel extends Model
{
    //
    protected $api_model;

    protected $final_response;

    public function __construct(){
        $this->api_model = new ApisModel();

        $this->final_response = array(
            "status" => null,
            "data" => null,
            "error" => null
        );
    }

    public function forgotPassOTPSend(){

        if(Request::post('otp_send_btn')){
            
            $data = array(
                "email_mobile" => Request::post('email_mobile')
            );

            $auth_resposnse = $this->api_model->getSessionToken();
            // dd($auth_resposnse);
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){

                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                if($this->email_validation(Request::post('email_mobile'))){
                    $data = $this->api_model->forgotPassOTPSendViaEmail($data);
                    // dump("email");

                    if($data["status"] == 200){
                        $data["data"]["message"] = "Enter the 6 digits OTP sent on your email ".Request::post('email_mobile');
                        $data["data"]["email_mobile"] = Request::post('email_mobile');
                        $data["data"]["signup_type"] = "email";
                    }

                }else{
                    $data = $this->api_model->forgotPassOTPSendViaMobile($data);
                    // dump("mobile");

                    if($data["status"] == 200){
                        $data["data"]["message"] = "Enter the 6 digits OTP sent on your mobile no. ". Request::post('email_mobile');
                        $data["data"]["email_mobile"] = Request::post('email_mobile');
                        $data["data"]["signup_type"] = "mobile";
                    }
                }
                
                // dd($data);

                

            }
            return $data;
        }        

    }

    function email_validation($str) { 
        return (!preg_match( 
    "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $str)) 
            ? FALSE : TRUE; 
    } 


    public function forgotPassOTPVerify(){

        if(Request::post('otp_btn')){

            $otp_code =  Request::post('otp_code_1') . '' .Request::post('otp_code_2') . '' . '' .Request::post('otp_code_3') . '' .'' .Request::post('otp_code_4') . '' .'' .Request::post('otp_code_5') . '' .'' .Request::post('otp_code_6');

            $data = array(
                "email_mobile" => Request::post('email_mobile'),
                "code" => $otp_code
            );

            $auth_resposnse = $this->api_model->getSessionToken();
            // dd($auth_resposnse);
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){

                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                $data = $this->api_model->forgotPassOTPVerify($data);
                $data["data"]["signup_type"] = "mobile";
                $data["data"]["email_mobile"] = Request::post('email_mobile');
                if($data["status"] == 200){

                    $custom_token = $data["data"]["customToken"];
                    
                    session()->put("forgot_buyer_custom_token",$custom_token);

                    // $admin_profile = $this->api_model->getBuyerProfile();
                    
                    // // dd($admin_profile);
                    // if($admin_profile["status"] == 200){
                    //     session()->put("buyer_profile_details",$admin_profile["data"]);    
                    // }

                }
            }

            return $data;
        }

    }

    public function forgotPass(){

        $files = array();
            
        if(Request::post('new_pass')){
            $data["password"] =  Request::post('new_pass');
        }    

        if(session('forgot_buyer_custom_token', null)){
            session()->put("buyer_custom_token",session('forgot_buyer_custom_token', null));
        }

        $files = array();
        //  dd($data);
         $response = $this->api_model->forgotPass($data,$files);
         // dd($response);
         return $response;

    }
}
