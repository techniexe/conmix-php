<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;

class CustomMixModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    function showCustomDesign($data){


        // dd($data);
        $response = $this->api_model->showCustomDesign($data);
        // dd($response);
        return $response;


    }

    public function showCustomDetail($vendor_id,$request_data){

        $data = $request_data;

        $response = $this->api_model->showCustomDetail($vendor_id,$data);
        // dd($response);
        return $response;

    }

    public function addCustomRMC($request_data){

      

        $data = $request_data;

        // if(Request::post("company_name")){
        //     $data['company_name'] = Request::post("company_name");
        // }
        
        // if(Request::post("full_name")){
        //     $data['full_name'] = Request::post("full_name");
        // }
        // $final_data = array();

        // $final_data["site"] = $data;

        //  dd($final_data);
        $response = $this->api_model->addCustomRMC($data);
        // dd($response);
        
        return $response;


    }

    public function editCustomRMC($request_data){

        $data = $request_data;

        // if(Request::post("company_name")){
        // $data['company_name'] = Request::post("company_name");
        // }
        
        // if(Request::post("full_name")){
        //     $data['full_name'] = Request::post("full_name");
        // }

        //  dd($data);
        $response = $this->api_model->editCustomRMC($data);
        // dd($response);
        
        return $response;


    }
}
