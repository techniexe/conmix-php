<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProductDetailModel extends Model
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function showProducts(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showProducts($data);
        // dd($response);
        return $response;


    }
}
