<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use Request;
use Illuminate\Support\Facades\Session;

class CartModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();

    }

    public function addSiteIdToCart(){

        $data = array(
            "site_id" => Request::get("site_id")
        );

        $response = $this->api_model->addSiteIdToCart($data);
        // dd($response);
        

        return $response;

    }
    
    public function addBiilingIdToCart(){

        $data = array(
            "billing_address_id" => Request::get("billing_address_id")
        );

        $response = $this->api_model->addBiilingIdToCart($data);
        // dd($response);
        

        return $response;

    }

    public function placeOrder(){

        $data = array(
            "cart_id" => Session::get('my_cart_id')
        );

        $session_custom_mix_design = Session::get("custom_mix_design");

        if(isset($session_custom_mix_design)){

            $with_TM = $session_custom_mix_design["with_TM"];
            $with_CP = $session_custom_mix_design["with_CP"];
            

            $data["with_TM"] = $session_custom_mix_design["with_TM"] == "yes" ? 'true' : 'false';
            $data["with_CP"] = $session_custom_mix_design["with_CP"] == "yes" ? 'true' : 'false';           

            

        }

        if(Session::get("selected_delivery_date") != null){
            $data["delivery_date"] = Session::get("selected_delivery_date");
        }

        if(Request::get("transaction_id")){
            $data["gateway_transaction_id"] = Request::get("transaction_id");
        }

        $response = $this->api_model->placeOrder($data);
        // dd($response);
        
        if($response["status"] == 200){

            $order_id = '';

            if(isset($response["data"]["_id"])){

                $order_id = $response["data"]["_id"];

                $response_data = $this->api_model->orderProcess($order_id);
                
                // $response = $this->api_model->quoteProcess($order_id);

            }
            
            if(isset($response["data"]["display_id"])){

                $order_id = $response["data"]["display_id"];

                $response_data = $this->api_model->orderProcess($order_id);

                // $response = $this->api_model->quoteProcess($order_id);
                // dd($response_data);
            }

            // $cart_id = $response["data"]["_id"];
            // dump($cart_id);
            // session()->put("my_cart_id",$cart_id);
            // Session::put('my_cart_id', $cart_id);
            Session::forget('my_cart_id');
            // dd(Session::get('my_cart_id'));
            // session()->save();
            // dump(session('my_cart_id', null));
        }

        return $response;

    }

    public function deleteCartItem(){

        $data = array(
            "item_id" => Request::post("product_id"),
            "cart_id" => Session::get('my_cart_id'),
        );

        if(Request::post("cart_design_mix_detail_id")){
            $data['cart_design_mix_detail_id'] = Request::post("cart_design_mix_detail_id");
        }

        $response = $this->api_model->deleteCartItem($data);
        // dd($response);


        return $response;

    }

    public function getCoupons(){

        $data = array();

        $profile = session('buyer_profile_details', null);
        // dd($profile);
        // if(isset($profile)){
        //     $data["buyer_id"] = $profile["_id"];
        // }

        $response = $this->api_model->getCoupons($data);
        // dd($response);
        return $response;
    }

    public function apply_coupon(){
        // dd(Request::all());
        $coupon_code = "";
        if(Request::get("coupon_code")){
            
            $coupon_code = Request::get("coupon_code");

        }
        $data =  array(
            "coupon_code" => $coupon_code
        );
        // dd($data);
        $response = $this->api_model->apply_coupon($data);
        //dd($response);
        return $response;

    }
}
