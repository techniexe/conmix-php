<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class LoginModel extends Model
{
    //
    protected $api_model;

    protected $final_response;

    public function __construct(){
        $this->api_model = new ApisModel();

        $this->final_response = array(
            "status" => null,
            "data" => null,
            "error" => null
        );
    }

    public function login(){
        
        if(Request::post('login_btn')){
            $email_mobile = Request::post('email_mobile');
            $password = Request::post('password');
            
            $data = array(
                "email_mobile" => $email_mobile,
                "password" => $password
            );
            
            $auth_resposnse = $this->api_model->getSessionToken();
            
            
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){
                //Success Response
                $session_token = $auth_resposnse["data"]["token"];
                session()->forget('buyer_custom_token');
                $data["session_token"] = $session_token;

                $login_response = $this->api_model->login($data);
                // dd($login_response);
                $this->final_response = $login_response;
                
                if($this->final_response["status"] == 200){
                    $custom_token = $this->final_response["data"]["customToken"];
                    
                    session()->put("buyer_custom_token",$custom_token);

                    $admin_profile = $this->api_model->getBuyerProfile();
                    
                    // dd($admin_profile);
                    if($admin_profile["status"] == 200){
                        session()->put("buyer_profile_details",$admin_profile["data"]);    
                    }

                    // dd(session('buyer_profile_details', null));
                    
                }

                // return $login_response;
                // $request->session()->put('session_token', ''.$session_token);

                // $globla = session('session_token', null);
                // $value = $request->session()->get('session_token', null);
                // echo "Global::".$globla.'<br>';
                // echo "value::".$value.'<br>';
                // echo $session_token;exit;
            }else{
                //Error Response
            }
            
        return $this->final_response;
            
        }
    }

    public function logout(){
        session()->forget('buyer_custom_token');
        session()->forget('buyer_profile_details');
        // session()->flush();
    }

    public function login_mobile(){
        
        // if(Request::post('login_btn')){
            $email_mobile = Request::get('email_mobile');
            $password = Request::get('password');
            
            $data = array(
                "email_mobile" => '+'.$email_mobile,
                "password" => $password
            );
            // dd($data);
            $auth_resposnse = $this->api_model->getSessionToken();
            // dd($auth_resposnse);
            
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){
                //Success Response
                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                $login_response = $this->api_model->login($data);
                // dd($login_response);
                $this->final_response = $login_response;
                
                if($this->final_response["status"] == 200){
                    $custom_token = $this->final_response["data"]["customToken"];
                    
                    session()->put("buyer_custom_token",$custom_token);

                    $admin_profile = $this->api_model->getBuyerProfile();
                    
                    // dd($admin_profile);
                    if($admin_profile["status"] == 200){
                        session()->put("buyer_profile_details",$admin_profile["data"]);    
                    }

                    // dd(session('buyer_profile_details', null));
                    
                }

                // return $login_response;
                // $request->session()->put('session_token', ''.$session_token);

                // $globla = session('session_token', null);
                // $value = $request->session()->get('session_token', null);
                // echo "Global::".$globla.'<br>';
                // echo "value::".$value.'<br>';
                // echo $session_token;exit;
            }else{
                //Error Response
            }
            
        // return $this->final_response;

        return redirect()->route('buyer_my_support_ticket');
            
        // }
    }
}
