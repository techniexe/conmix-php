<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BuyerOrdersExport extends Model implements FromArray,WithHeadings
{
    //
    protected $buyer_order;

    public function __construct(array $buyer_order)
    {
        $this->buyer_order = $buyer_order;
    }

    public function array(): array
    {
        return $this->buyer_order;
    }

    public function headings(): array
    {

        return [

            'Order Id',
            'Order Date',
            'Sub Total (₹)',
            'GST (₹)',
            'Total Amount (₹)',
            'Order Status',

        ];

    }
}
