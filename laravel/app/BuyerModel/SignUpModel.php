<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class SignUpModel extends Model
{
    //
    protected $api_model;

    protected $final_response;

    public function __construct(){
        $this->api_model = new ApisModel();

        $this->final_response = array(
            "status" => null,
            "data" => null,
            "error" => null
        );
    }

    public function signupOTPSend(){

        if(Request::post('register_btn')){

            $data = array(
                "email_mobile" => Request::post('email_mobile')
            );

            $auth_resposnse = $this->api_model->getSessionToken();
            // dd($auth_resposnse);
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){

                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                if($this->email_validation(Request::post('email_mobile'))){
                    $data = $this->api_model->signupOTPSendViaEmail($data);
                    // dump("email");

                    if($data["status"] == 200){
                        $data["data"]["message"] = "Enter the 6 digits OTP sent on your email ".Request::post('email_mobile');
                        $data["data"]["email_mobile"] = Request::post('email_mobile');
                        $data["data"]["signup_type"] = "email";
                    }

                }else{
                    $data = $this->api_model->signupOTPSendViaMobile($data);
                    // dump("mobile");

                    if($data["status"] == 200){
                        // $data["data"]["message"] = "Enter the 6 digits OTP sent on your mobile no. ". str_replace(substr(Request::post('email_mobile'),5, 11),"XXXXXX",Request::post('email_mobile'));
                        $data["data"]["message"] = "Enter the 6 digits OTP sent on your mobile no. ". Request::post('email_mobile');
                        $data["data"]["email_mobile"] = Request::post('email_mobile');
                        $data["data"]["signup_type"] = "mobile";
                    }
                }

                // dd($data);

            }
            return $data;
        }

    }

    function email_validation($str) {
        return (!preg_match(
    "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $str))
            ? FALSE : TRUE;
    }

    public function signupOTPVerify(){

        if(Request::post('otp_btn')){

            $otp_code =  Request::post('otp_code_1') . '' .Request::post('otp_code_2') . '' . '' .Request::post('otp_code_3') . '' .'' .Request::post('otp_code_4') . '' .'' .Request::post('otp_code_5') . '' .'' .Request::post('otp_code_6');

            $data = array(
                "email_mobile" => Request::post('email_mobile'),
                "code" => $otp_code
            );

            $auth_resposnse = $this->api_model->getSessionToken();
            // dd($auth_resposnse);
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){

                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                $data = $this->api_model->signupOTPVerify($data);
                // dd($data);
                $data["data"]["signup_type"] = Request::post('signup_type');
                $data["data"]["email_mobile"] = Request::post('email_mobile');
                if($data["status"] == 200){

                    $custom_token = $data["data"]["customToken"];

                    session()->put("buyer_custom_token",$custom_token);

                }
            }

            return $data;
        }

    }

    public function signupRegistration(){

        if(Request::post('register_btn')){

            $data = array(
                "full_name" => Request::post("full_name"),
                "mobile_number" => Request::post("mobile_number"),
                "email" => Request::post("email"),
                "password" => Request::post("password"),
                "signup_type" => Request::post("signup_type"),
                "company_type" => Request::post("company_type"),
                "company_name" => Request::post("company_name"),
                "pay_method_id" => Request::post("payment_method"),
            );

            if(Request::post("landline_number")){
                $data["landline_number"] = Request::post("landline_number");
            }

            if(Request::post("gst_number")){
                $data["gst_number"] = Request::post("gst_number");
            }

            if(Request::post("pan_number")){
                $data["pan_number"] = Request::post("pan_number");
            }

            if(Request::post("account_type")){
                $data["account_type"] = Request::post("account_type");
            }



            $files = array();

            if(Request::file('identity_image')){

                $image = Request::file('identity_image');



                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image" => $image,
                        "param_key" => "identity_image"
                    ));



            }



        //  dd($data);
         $response = $this->api_model->buyerRegistration($data,$files);
        // dd($response);
         if($response["status"] == 200){

            $custom_token = $response["data"]["customToken"];

            session()->put("buyer_custom_token",$custom_token);

            $admin_profile = $this->api_model->getBuyerProfile();

            // dd($admin_profile);
            if($admin_profile["status"] == 200){
                session()->put("buyer_profile_details",$admin_profile["data"]);
            }

        }

         // dd($response);
         return $response;

        }

    }
}
