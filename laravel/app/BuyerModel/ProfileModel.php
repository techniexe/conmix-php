<?php

namespace App\BuyerModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProfileModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApisModel();
    }

    public function showProfileDetails(){

        $data = array();

        $response = $this->api_model->getBuyerProfile();
        // dd($response);
        return $response;
    }

    public function editProfile(){
        
        // if(Request::post('update_profile')){
            $data = array();
            if(Request::get('otp_code_1') &&
                Request::get('otp_code_2') &&
                Request::get('otp_code_3') &&
                Request::get('otp_code_4') &&
                Request::get('otp_code_5') &&
                Request::get('otp_code_6')){
                    
                $otp_code =  Request::get('otp_code_1') . '' .Request::get('otp_code_2') . '' . '' .Request::get('otp_code_3') . '' .'' .Request::get('otp_code_4') . '' .'' .Request::get('otp_code_5') . '' .'' .Request::get('otp_code_6');


                if(Request::get('mobile_or_email_type')){

                    if(Request::get('mobile_or_email_type') == "old_mobile"){
                    
                        $data["old_mobile_code"] =  $otp_code;
                       
                    }
                    
                    if(Request::get('mobile_or_email_type') == "new_mobile"){
                    
                        $data["new_mobile_code"] =  $otp_code;
                       
                    }
    
                    if(Request::get('mobile_or_email_type') == "old_email"){
                    
                        $data["old_email_code"] =  $otp_code;
                       
                    }
    
                    if(Request::get('mobile_or_email_type') == "new_email"){
                    
                        $data["new_email_code"] =  $otp_code;
                       
                    }
    
                }

            }

            

            if(Request::post("company_name")){
                $data["company_name"] = Request::post("company_name");
            }else{
                $data["company_name"] = "";
            }
            
            if(Request::post("company_type")){
                $data["company_type"] = Request::post("company_type");
            }
            
            if(Request::post("full_name")){
                $data["full_name"] = Request::post("full_name");
            }
            
            if(Request::post("mobile_number")){
                $data["mobile_number"] = Request::post("mobile_number");
            }
            
            if(Request::post("email")){
                $data["email"] = Request::post("email");
            }
            
            if(Request::post("password")){
                $data["password"] = Request::post("password");
            }

            if(Request::post("gst_number")){
                $data["gst_number"] = Request::post("gst_number");
            }

            if(Request::post("pan_number")){
                $data["pan_number"] = Request::post("pan_number");
            }

            if(Request::post("account_type")){
                $data["account_type"] = Request::post("account_type");
            }
            
            if(Request::post("payment_method")){
                $data["pay_method_id"] = Request::post("payment_method");
            }

            
            
            

            $files = array();

            if(Request::file('identity_image')){

                $image = Request::file('identity_image');
                
                

                    array_push($files, array(
                        "image_name" => $image->getClientOriginalName(),
                        "image_ext" => $image->getClientOriginalExtension(),
                        "image" => $image,
                        "param_key" => "identity_image"
                    ));

                    

            }
            
            

        //  dd($data);
         $response = $this->api_model->editBuyerProfile($data,$files);
        // dd($response);
        //  if($response["status"] == 200){

        //     $custom_token = $response["data"]["customToken"];
            
        //     session()->put("custom_token",$custom_token);

        // }

         // dd($response);
         return $response;

        // }

    }
    
    public function changePass(){

        if(Request::post('update_profile')){

            $data = array(
                
            );
            
            if(Request::post("old_password")){
                $data["old_password"] = Request::post("old_password");
            }
            
            if(Request::post("password")){
                $data["password"] = Request::post("password");
            }
            
            if(Request::post("authentication_code")){
                $data["authentication_code"] = Request::post("authentication_code");
            }

            
            

        //  dd($data);
         $response = $this->api_model->changePass($data);
        // dd($response);
        //  if($response["status"] == 200){

        //     $custom_token = $response["data"]["customToken"];
            
        //     session()->put("custom_token",$custom_token);

        // }

         // dd($response);
         return $response;

        }

    }

    public function requestOTP(){

        $data = array();

        if(Request::post("old_password")){
            $data["old_password"] = Request::post("old_password");
        }
        
        if(Request::post("password")){
            $data["password"] = Request::post("password");
        }

        $response = $this->api_model->requestOTP($data);

        return $response;
    }

    public function requestOTPForEditProfile(){

        $data = array(
            "requestOTP" => "yes"
        );

        if(Request::get("new_mobile_no")){
            $data["new_mobile_no"] = Request::get("new_mobile_no");
        }
        
        if(Request::get("new_email")){
            $data["new_email"] = Request::get("new_email");
        }
        
        if(Request::get("old_email")){
            $data["old_email"] = Request::get("old_email");
        }
        
        if(Request::get("old_mobile_no")){
            $data["old_mobile_no"] = Request::get("old_mobile_no");
        }
        // dd($data);
        $response = $this->api_model->requestOTPForEditProfile($data);

        return $response;
    }

    public function showSites(){

        $data = array();

        $response = $this->api_model->showSites();
        // dd($response);
        return $response;
    }

    public function addSites(){

        $cordinates = array(
            Request::post("us2_lon"),
            Request::post("us2_lat"),
        );

        $data = array(
              "company_name" => Request::post("company_name"),  
              "site_name" => Request::post("site_name"),  
              "person_name" => Request::post("person_name"),  
              "mobile_number" => Request::post("mobile_number"),  
              "email" => Request::post("email"),  
              "address_line1" => Request::post("address_line1"),  
              "address_line2" => Request::post("address_line2"),  
              "country_id" => Request::post("country_id"),  
              "state_id" => Request::post("state_id"),  
              "city_id" => Request::post("city_id"), 
              "pincode" => Request::post("pincode"), 
              "billing_address_id" => Request::post("billing_address_id"), 
              "location" => array(
                  "coordinates" => $cordinates
              ) 
        );

        $final_data = array();

        $final_data["site"] = $data;

        //  dd($final_data);
        $response = $this->api_model->addSites($final_data);
        // dd($response);
        
        return $response;


    }
    
    public function checkSiteExist(){


        $data = array(
              "site_name" => Request::get("site_name")
              
        );


        //  dd($final_data);
        $response = $this->api_model->checkSiteExist($data);
        // dd($response);
        
        return $response;


    }
    
    public function editSites(){

        $cordinates = array(
            Request::post("us2_lon"),
            Request::post("us2_lat"),
        );

        $data = array(
              "company_name" => Request::post("company_name"),  
              "site_name" => Request::post("site_name"),  
              "person_name" => Request::post("person_name"),  
              "mobile_number" => Request::post("mobile_number"),  
              "email" => Request::post("email"),  
              "address_line1" => Request::post("address_line1"),  
              "address_line2" => Request::post("address_line2"),
              "pincode" => Request::post("pincode"), 
              "site_id" => Request::post("site_id"), 
              "billing_address_id" => Request::post("billing_address_id"), 
              "location" => array(
                  "coordinates" => $cordinates
              ) 
        );

        if(Request::post("country_id")){
            $data["country_id"] = Request::post("country_id");
        }
        
        if(Request::post("state_id")){
            $data["state_id"] = Request::post("state_id");
        }
        
        if(Request::post("city_id")){
            $data["city_id"] = Request::post("city_id");
        }

        $final_data = array();

        $final_data["site"] = $data;

        //  dd($final_data);
        $response = $this->api_model->editSites($final_data);
        // dd($response);
        
        return $response;


    }
    
    public function deleteSites(){

        

        $data = array(
              "site_id" => Request::get("site_id"),  
              
        );

   

        //  dd($final_data);
        $response = $this->api_model->deleteSites($data);
        // dd($response);
        
        return $response;


    }
    
    public function deleteBilling(){

        

        $data = array(
              "billing_id" => Request::get("billing_id"),  
              
        );

   

        //  dd($final_data);
        $response = $this->api_model->deleteBilling($data);
        // dd($response);
        
        return $response;


    }
    
    public function changePassword(){

        

        $data = array(
              "password" => Request::post("password"),  
              
        );

        $final_data = array();

        $final_data["site"] = $data;

        //  dd($final_data);
        $response = $this->api_model->editSites($final_data);
        // dd($response);
        
        return $response;


    }


    public function showBillngAddress(){

        $data = array();

        $response = $this->api_model->showBillngAddress();
        // dd($response);
        return $response;
    }

    public function addBillngAddress(){

      

        $data = array( 
              "line1" => Request::post("address_line1"),  
              "line2" => Request::post("address_line2"),  
              "state_id" => Request::post("state_id"),  
              "city_id" => Request::post("city_id"), 
              "pincode" => Request::post("pincode"), 
              "gst_number" => Request::post("gst_number"), 
              
        );

        if(Request::post("company_name")){
            $data['company_name'] = Request::post("company_name");
        }
        
        if(Request::post("full_name")){
            $data['full_name'] = Request::post("full_name");
        }
        // $final_data = array();

        // $final_data["site"] = $data;

        //  dd($final_data);
        $response = $this->api_model->addBillngAddress($data);
        // dd($response);
        
        return $response;


    }

    public function editBillngAddress(){

        $data = array(
            "line1" => Request::post("address_line1"),  
            "line2" => Request::post("address_line2"),  
            "state_id" => Request::post("state_id"),  
            "city_id" => Request::post("city_id"), 
            "pincode" => Request::post("pincode"), 
            "gst_number" => Request::post("gst_number"), 
            "_id" => Request::post("billing_address_id"), 
            
      );

      if(Request::post("company_name")){
        $data['company_name'] = Request::post("company_name");
        }
        
        if(Request::post("full_name")){
            $data['full_name'] = Request::post("full_name");
        }

        //  dd($data);
        $response = $this->api_model->editBillngAddress($data);
        // dd($response);
        
        return $response;


    }
    
    public function showCustomRMC(){

        $data = array();

        $response = $this->api_model->showCustomRMC();
        // dd($response);
        return $response;
    }
    
    public function deleteCustomRMC(){

        $data = array(
            'custom_rmc_id' => Request::get('custom_rmc_id')
        );

        $response = $this->api_model->deleteCustomRMC($data);
        // dd($response);
        return $response;
    }

    

}
