<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Excel;
use App\AdminModel\BuyerOrderListExport;
use App\AdminModel\SupplierOrderListExport;
use App\AdminModel\BuyerOrdersExport;
use App\AdminModel\SupplierOrdersExport;
use App\AdminModel\SupplierAdmixBrandExport;
use App\AdminModel\SupplierAdmixTypesExport;
use App\AdminModel\SupplierAggrSandExport;
use App\AdminModel\AdminAggrSandCatExport;
use App\AdminModel\AdminAggrSourceCatExport;
use App\AdminModel\CementBrandExport;
use App\AdminModel\CementGradeExport;
use App\AdminModel\ConcreteGradeExport;
use App\AdminModel\ConcretePumpCatExport;
use App\AdminModel\CouponExport;
use App\AdminModel\SupportTicketExport;
use App\AdminModel\FlyAshExport;
use App\AdminModel\GSTExport;
use App\AdminModel\MarginRateExport;
use App\AdminModel\SandSourceExport;
use App\AdminModel\DesignMixExport;
use App\AdminModel\TMExport;
use App\AdminModel\TMCatExport;
use App\AdminModel\TMSubCatExport;
use App\AdminModel\SupplierListExport;

class ReportContrller extends Controller
{
    //

    protected $report_model;
    protected $product_model;
    protected $admin_model;
    public function __construct(){
        
        $this->report_model = new \App\AdminModel\ReportModel();
        $this->product_model = new \App\AdminModel\ProductModel();
        $this->admin_model = new \App\AdminModel\VehicleModel();
    }

    public function index(){

        $data = array();
        // $data = $this->report_model->showOrders();  

        // dd($data);

        return view('admin.report.report')->with("data",$data);

    }
    
    public function report_detail(){

        $data = array();
        // $data = $this->report_model->showOrders();  

        // dd($data);

        return view('admin.report.report_detail')->with("data",$data);

    }

    public function admix_brand_report(){

        $data = array();
        $data = $this->report_model->admix_brand_report();  

        // dd($data);

        return view('admin.report.admix_brand_report')->with("data",$data);

    }

    public function AdmixBrandExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->admix_brand_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["name"]) ? ($value["name"]).' ' : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new SupplierAdmixBrandExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_admix_brand_report'.$file_type);
        }
    }

    public function agg_sand_cat_report(){

        $data = array();
        $data = $this->report_model->agg_sand_cat_report();  

        // dd($data);

        return view('admin.report.aggr_sand_report')->with("data",$data);

    }

    public function AggrSandExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->agg_sand_cat_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["category_name"]) ? ($value["category_name"]).' ' : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new SupplierAggrSandExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_aggr_sand_report'.$file_type);
        }
    }



    public function admix_types_report(){

        $data = array();
        $data = $this->report_model->admix_types_report();  

        // dd($data);

        return view('admin.report.admix_types_report')->with("data",$data);

    }

    public function AdmixTypesExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->admix_types_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["admixture_brand_name"]) ? ($value["admixture_brand_name"]).' ' : ''),
                    (isset($value["category_name"]) ? ($value["category_name"]).' ' : ''),
                    (isset($value["admixture_type"]) ? ($value["admixture_type"]).' ' : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new SupplierAdmixTypesExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_admix_types_report'.$file_type);
        }
    }

    public function agg_sand_subcategory_report(){

        $data = array();

        $cat_data = $this->product_model->getCategories();
        $margin_rate_data = $this->product_model->showMarginRate();

        $default_category_id = "";
        if(isset($cat_data["data"])){

            foreach($cat_data["data"] as $value){
                if($value["category_name"] == "Aggregate"){
                    $default_category_id  = $value["_id"];
                }
            }

        }

        if(Request::get('categoryId')){
            $data = $this->report_model->agg_sand_subcategory_report(Request::get('categoryId'));
            // dd($data);
        }else{
            $data = $this->report_model->agg_sand_subcategory_report($default_category_id);
        }

        $data["product_cat_data"] = $cat_data["data"];
        $data["margin_rate_data"] = $margin_rate_data["data"];
        // dd($data);

        return view('admin.report.aggr_sand_sub_cat_report')->with("data",$data);

    }

    public function AggrSandSubCatExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $cat_data = $this->product_model->getCategories();
        $default_category_id = "";
        if(isset($cat_data["data"])){

            foreach($cat_data["data"] as $value){
                if($value["category_name"] == "Aggregate"){
                    $default_category_id  = $value["_id"];
                }
            }

        }

        if(Request::get('categoryId')){
            $response = $this->report_model->agg_sand_subcategory_report(Request::get('categoryId'));
            // dd($data);
        }else{
            $response = $this->report_model->agg_sand_subcategory_report($default_category_id);
        }
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["aggregate_sand_category"]["category_name"]) ? ($value["aggregate_sand_category"]["category_name"]).' ' : ''),
                    (isset($value["sub_category_name"]) ? ($value["sub_category_name"]).' ' : ''),
                    (isset($value["margin_rate"]["title"]) ? ($value["margin_rate"]["title"]).' ' : ''),
                    (isset($value["gst_slab"]["title"]) ? ($value["gst_slab"]["title"]).' ' : ''),
                    (isset($value["min_quantity"]["MT"]) ? ($value["min_quantity"]["MT"]).' ' : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new AdminAggrSandCatExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_aggr_sand_sub_report'.$file_type);
        }
    }

    public function agg_source_report(){

        $data = array();
        $data = $this->report_model->agg_source_report();  

        // dd($data);

        return view('admin.report.aggr_source_report')->with("data",$data);

    }

    public function AggrSourceExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        
        $response = $this->report_model->agg_source_report();
      
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["region_name"]) ? ($value["region_name"]).' ' : ''),
                    (isset($value["aggregate_source_name"]) ? ($value["aggregate_source_name"]).' ' : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new AdminAggrSourceCatExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_aggr_source_report'.$file_type);
        }
    }
    
   
    

    
    public function buyer_orders_report(){

        $data = array();
        $data = $this->report_model->buyer_orders_report();  

        // dd($data);

        return view('admin.report.buyer_orders')->with("data",$data);

    }

    public function buyerOrderExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->buyer_orders_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    $value["display_id"],
                    (isset($value["buyer"]["full_name"]) ? $value["buyer"]["full_name"] : ''),
                    (isset($value["buyer"]["company_name"]) ? $value["buyer"]["company_name"] : ''),
                    (isset($value["mobile_number"]) ? $value["mobile_number"] : ''),
                    (isset($value["total_amount"]) ? number_format($value["total_amount"],2) : ''),
                    (isset($value["site_name"]) ? $value["site_name"] : ''),
                    (isset($value["gateway_transaction_id"]) ? $value["gateway_transaction_id"] : ''),
                    (isset($value["site_name"]) ? ($value["site_name"]).' ' : '').
                        (isset($value["address_line1"]) ? ($value["address_line1"]).' ' : ''). 
                        (isset($value["address_line2"]) ? ($value["address_line2"]).' ' : '').
                        (isset($value["state_name"]) ? ($value["state_name"]).' ' : ''). 
                        (isset($value["city_name"]) ? ($value["city_name"]).' ' : ''). 
                        (isset($value["pincode"]) ? ($value["pincode"]).' ' : ''),
                    (isset($value["order_status"]) ? $value["order_status"] : ''),
                    (isset($value["payment_status"]) ? $value["payment_status"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new BuyerOrdersExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,
             'buyer_orders'.$file_type);
        }
    }
    
    public function buyer_orders_detail_report($id){

        $data = array();
        $data = $this->report_model->buyer_orders_detail_report($id);  

        // dd($data);

        return view('admin.report.buyer_order_details')->with("data",$data);

    }

    public function buyer_listing_report(){

        $data = array();
        $data = $this->report_model->buyer_listing_report();  

        // dd($data);

        return view('admin.report.buyer_listing_report')->with("data",$data);

    }

    public function buyerListingExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $buyer_list = $this->report_model->buyer_listing_report();
        // dd($buyer_list);
        $data = array();
        if(isset($buyer_list['data']) && sizeof($buyer_list['data'])>0){
            $data = $buyer_list['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    $value["_id"],
                    $value["full_name"],
                    $value["order_count"],date('d M Y', strtotime($value["created_at"]))
                ];
                $i++;
            }

            $export = new BuyerOrderListExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export, 'buyer_list'.$file_type);
        }
    }

    public function cement_brand_report(){

        $data = array();
        $data = $this->report_model->cement_brand_report();  

        // dd($data);

        return view('admin.report.cement_brand_report')->with("data",$data);

    }

    public function CementbrandExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->cement_brand_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["name"]) ? $value["name"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new CementBrandExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_cement_brand'.$file_type);
        }
    }

    public function cement_grade_report(){

        $data = array();
        $data = $this->report_model->cement_grade_report();  

        // dd($data);

        return view('admin.report.cement_grade_report')->with("data",$data);

    }

    public function CementGradeExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->cement_grade_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["name"]) ? $value["name"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new CementGradeExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_cement_grade'.$file_type);
        }
    }

    public function concrete_grade_report(){

        $data = array();
        $data = $this->report_model->concrete_grade_report();  

        // dd($data);

        return view('admin.report.concrete_grade_report')->with("data",$data);

    }

    public function ConcreteGradeExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->concrete_grade_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["name"]) ? $value["name"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new ConcreteGradeExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_concrete_grade'.$file_type);
        }
    }

    public function concrete_pump_category_report(){

        $data = array();
        $data = $this->report_model->concrete_pump_category_report();  

        // dd($data);

        return view('admin.report.concrete_pump_category_report')->with("data",$data);

    }

    public function ConcretePumpCatExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->concrete_pump_category_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["category_name"]) ? $value["category_name"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new ConcretePumpCatExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_concrete_pump_cat'.$file_type);
        }
    }

    public function coupon_report(){

        $data = array();
        $data = $this->report_model->coupon_report();  

        // dd($data);

        return view('admin.report.coupon_report')->with("data",$data);

    }

    public function CouponExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->coupon_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["code"]) ? $value["code"] : ''),
                    (isset($value["discount_type"]) ? $value["discount_type"] : ''),
                    (isset($value["discount_value"]) ? $value["discount_value"] : ''),
                    
                    (isset($value["start_date"]) ? (date('d M Y h:i:s a', strtotime($value["start_date"]))) : ''),
                    (isset($value["end_date"]) ? (date('d M Y h:i:s a', strtotime($value["end_date"]))) : ''),
                    (isset($value["min_order"]) ? $value["min_order"] : ''),
                    (isset($value["max_discount"]) ? $value["max_discount"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '',
                    (isset($value["is_active"]) ? ($value["is_active"] == 'true' ? 'Active' : 'InActive') : '')
                    
                ];
                $i++;
            }

            $export = new CouponExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_coupon'.$file_type);
        }
    }

    public function support_ticket_report(){

        $data = array();
        $data = $this->report_model->support_ticket_report();  

        // dd($data);

        return view('admin.report.support_ticket_report')->with("data",$data);

    }

    public function SupportTicketExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->support_ticket_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["ticket_id"]) ? $value["ticket_id"] : ''),
                    (isset($value["subject"]) ? $value["subject"] : ''),
                    (isset($value["severity"]) ? $value["severity"] : ''),
                    (isset($value["client_type"]) ? $value["client_type"] : ''),
                    ((isset($value["vendor"]["company_name"]) ? $value["vendor"]["company_name"] . ' - ' : '').
                        (isset($value["buyer"]["company_name"]) ? $value["buyer"]["company_name"] . ' - ' : '').
                        (isset($value["vendor"]["full_name"]) ? $value["vendor"]["full_name"] : '').
                        (isset($value["buyer"]["full_name"]) ? $value["buyer"]["full_name"] : '')
                        ),
                    
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '',
                    (isset($value["support_ticket_status"]) ? ($value["support_ticket_status"]) : '')
                    
                ];
                $i++;
            }

            $export = new SupportTicketExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_support_ticket'.$file_type);
        }
    }
    
    public function flyAsh_source_report(){

        $data = array();
        $data = $this->report_model->flyAsh_source_report();  

        // dd($data);

        return view('admin.report.flyAsh_source_report')->with("data",$data);

    }

    public function FlyAshExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->flyAsh_source_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["region_name"]) ? $value["region_name"] : ''),
                    (isset($value["fly_ash_source_name"]) ? $value["fly_ash_source_name"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new FlyAshExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_flyash_source'.$file_type);
        }
    }

    public function gst_slab_report(){

        $data = array();
        $data = $this->report_model->gst_slab_report();  

        // dd($data);

        return view('admin.report.gst_slab_report')->with("data",$data);

    }

    public function GstExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->gst_slab_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["title"]) ? $value["title"] : ''),
                    (isset($value["igst_rate"]) ? $value["igst_rate"] : ''),
                    (isset($value["sgst_rate"]) ? $value["sgst_rate"] : ''),
                    (isset($value["cgst_rate"]) ? $value["cgst_rate"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new GSTExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_gst_export'.$file_type);
        }
    }

    public function margin_rate_report(){

        $data = array();
        $data = $this->report_model->margin_rate_report();  

        // dd($data);

        return view('admin.report.margin_rate_report')->with("data",$data);

    }

    public function MarginRateExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->margin_rate_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $upto = "Upto Price :";
                $rate = "Margin Rate:";
                if(isset($value["slab"])){
                    foreach($value["slab"] as $slab_value){
                        $upto .= $slab_value["upto"].'::';
                        $rate .= $slab_value["rate"].'::';
                    }
                }

                    
                $final_array[$i] = [
                    (isset($value["title"]) ? $value["title"] : ''),
                    ($upto.'-'.$rate),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new MarginRateExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_margin_rate_export'.$file_type);
        }
    }
    
    public function sand_source_report(){

        $data = array();
        $data = $this->report_model->sand_source_report();  

        // dd($data);

        return view('admin.report.sand_source_report')->with("data",$data);

    }

    public function SandSourceExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->sand_source_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                

                    
                $final_array[$i] = [
                    (isset($value["region_name"]) ? $value["region_name"] : ''),
                    (isset($value["sand_source_name"]) ? $value["sand_source_name"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new SandSourceExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_sand_source_export'.$file_type);
        }
    }
    
    public function supplier_listing_report(){

        $data = array();
        $data = $this->report_model->supplier_listing_report();  

        // dd($data);

        return view('admin.report.supplier_listing_report')->with("data",$data);

    }
    
    
    
    public function supplier_orders_report(){

        // dd(Request::all());

        $data = array();
        $data = $this->report_model->supplier_orders_report();  

        // dd($data);

        return view('admin.report.supplier_orders')->with("data",$data);

    }

    public function supplier_orders_detail_report($id, $user_id){

        $data = array();
        $data = $this->report_model->supplier_orders_detail_report($id,$user_id);  

        // dd($data);

        return view('admin.report.supplier_order_detail')->with("data",$data);

    }
    
    
    
    public function logistics_orders_report(){

        // dd(Request::all());

        $data = array();
        $data = $this->report_model->logistics_orders_report();  

        // dd($data);

        return view('admin.report.logistics_orders')->with("data",$data);

    }

    public function logistics_orders_detail_report($id, $user_id){

        $data = array();
        $data = $this->report_model->logistics_orders_detail_report($id,$user_id);  

        // dd($data);

        return view('admin.report.logistic_order_detail')->with("data",$data);

    }
    
    public function designmix_listing_report(){

        $data = array();

        $data = $this->report_model->designmix_listing_report();         

        // dd($data);

        return view('admin.report.designmix_list_report')->with("data",$data);
        // return view('admin.report.designmix_lists')->with("data",$data);

    }

    public function DesignMixListExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->designmix_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                

                    
                $final_array[$i] = [
                    (isset($value["vendor"]["company_name"]) ? $value["vendor"]["company_name"] : ''),
                    (isset($value["concrete_grade"]["name"]) ? $value["concrete_grade"]["name"] : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new DesignMixExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_designmix_export'.$file_type);
        }
    }
    
    public function designmix_detail_report(){

        $data = array();


        $data = $this->report_model->designmix_detail_report();  
       

        // dd($data);
        if($data["status"] == 200){

            
            $suggestion_array = array();
            $final_data = array();
            
            // dd($cart_details);
            // $final_data["cart_details"] = $cart_details;

                $returnHTML = view('supplier.design_mix.design_mix_grade_wise_ajax')->with('data', $data["data"])->render();
                $returnHTMLslide = "";        

                
        

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $data;
        }


        return $data;

    }
    
    public function vehicle_listing_report(){

        $data = array();

        $category_data = $this->admin_model->showTMCategory();
        $sub_category_data = $this->admin_model->showTMSubCategoies();

        $data = $this->report_model->vehicle_listing_report();  
        $data["vehicle_categoies"] = $category_data;
        $data["subcategories"] = $sub_category_data;

        // dd($data);

        // return view('admin.report.vehicle_list')->with("data",$data);
        return view('admin.report.TM_list_report')->with("data",$data);

    }

    public function VehicleListExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->vehicle_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                

                    
                $final_array[$i] = [
                    (isset($value["vendor"]["company_name"]) ? $value["vendor"]["company_name"] : ''),                                        
                    (isset($value["business_name"]) ? $value["business_name"] : ''),                                        
                    (isset($value["TM_category"]["category_name"]) ? $value["TM_category"]["category_name"] : ''),                                        
                    (isset($value["TM_sub_category"]["sub_category_name"]) ? $value["TM_sub_category"]["sub_category_name"] : ''),                                        
                    (isset($value["TM_rc_number"]) ? $value["TM_rc_number"] : ''),                                      
                    (isset($value["min_trip_price"]) ? $value["min_trip_price"] : ''),                                      
                    (isset($value["per_Cu_mtr_km"]) ? $value["per_Cu_mtr_km"] : ''),                                      
                    (isset($value["TM_model"]) ? $value["TM_model"] : ''),                                      
                    (isset($value["manufacture_year"]) ? $value["manufacture_year"] : ''),                                      
                    (isset($value["is_insurance_active"]) ? ($value["is_insurance_active"] == 'true' ? 'Active' : 'InActive') : ''),     
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new TMExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_TM_export'.$file_type);
        }
    }

    public function TM_category_report(){

        $data = array();


        $data = $this->report_model->TM_category_report();  

        // dd($data);

        // return view('admin.report.vehicle_list')->with("data",$data);
        return view('admin.report.TM_cat_report')->with("data",$data);

    }

    public function TMCatExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->TM_category_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                

                    
                $final_array[$i] = [
                    (isset($value["category_name"]) ? $value["category_name"] : ''), 
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new TMCatExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_TM_cat_export'.$file_type);
        }
    }

    public function TM_sub_category_report(){

        $data = array();

        $category_data = $this->admin_model->showTMCategory();
        $data = $this->report_model->TM_sub_category_report();  
        $data["vehicle_categoies"] = $category_data;

        // dd($data);

        // return view('admin.report.vehicle_list')->with("data",$data);
        return view('admin.report.TM_sub_cat_report')->with("data",$data);

    }


    public function TMsubCatExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->TM_sub_category_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                

                    
                $final_array[$i] = [
                    (isset($value["TM_category"]["category_name"]) ? $value["TM_category"]["category_name"] : ''), 
                    (isset($value["sub_category_name"]) ? $value["sub_category_name"] : ''), 
                    (isset($value["load_capacity"]) ? $value["load_capacity"] : ''), 
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new TMSubCatExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'admin_TM_sub_cat_export'.$file_type);
        }
    }
    
    
    public function supplierListingExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->supplier_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["company_type"]) ? $value["company_type"] : '' ),
                    (isset($value["company_name"]) ? $value["company_name"] : '' ),
                    (isset($value["full_name"]) ? $value["full_name"] : '' ),
                    (isset($value["mobile_number"]) ? $value["mobile_number"] : '' ),
                    (isset($value["email"]) ? $value["email"] : '' ),
                    (isset($value["company_certification_number"]) ? $value["company_certification_number"] : '' ),
                     isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                ];
                $i++;
            }

            $export = new SupplierListExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export, 'supplier_list'.$file_type);
        }
    }
    
    
    
    public function supplierOrderExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->supplier_orders_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["buyer_order_display_id"]) ? $value["buyer_order_display_id"] : '' ),
                    (isset($value["buyer"]["full_name"]) ? $value["buyer"]["full_name"] : '' ),
                    
                    (isset($value["_id"]) ? $value["_id"] : '' ),
                    (isset($value["vendor"]["full_name"]) ? $value["vendor"]["full_name"] : '' ),
                    (isset($value["vendor"]["company_name"]) ? $value["vendor"]["company_name"] : '' ),
                    (isset($value["vendor"]["mobile_number"]) ? $value["vendor"]["mobile_number"] : '' ),
                    (isset($value["quantity"]) ? $value["quantity"] : '' ),
                    
                    (isset($value["total_amount"]) ? number_format($value["total_amount"],2) : 0 ),
                    (isset($value["business_name"]) ? $value["business_name"] : '' ),
                    (isset($value["order_status"]) ? $value["order_status"] : '' ),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                ];
                $i++;
            }

            $export = new SupplierOrdersExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_orders'.$file_type);
        }
    }
}
