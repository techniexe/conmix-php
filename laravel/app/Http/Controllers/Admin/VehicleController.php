<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    //
    protected $admin_model;
    public function __construct(){
        $this->admin_model = new \App\AdminModel\VehicleModel();
    }

    public function index(){
        $data = array();
        $data = $this->admin_model->showTMCategory();
        // dd($data);
        return view("admin.vehicle.TM_category")->with("data",$data);
    }

    public function addTMCategory(Request $request){
        
        $data = $this->admin_model->addTMCategory();

        return $data;
    }

    public function editTMCategory(Request $request){
        // dd($request->request);
        $data = $this->admin_model->editTMCategory();

        return $data;
    }

    public function showTMSubCategoies(){

        $category_data = $this->admin_model->showTMCategory();
        $data = $this->admin_model->showTMSubCategoies();
        $data["vehicle_categoies"] = $category_data;
        // dd($data);
        return view("admin.vehicle.TM_sub_category")->with("data",$data);

    }

    public function addTMSubCategory(Request $request){
        // dd($request->request);
        $data = $this->admin_model->addTMSubCategory();

        return $data;
    }

    public function editTMSubCategory(Request $request){
        // dd($request->request);
        $data = $this->admin_model->editTMSubCategory();

        return $data;
    }


    public function showPumpCategory(){
        $data = array();
        $data = $this->admin_model->showPumpCategory();
        // dd($data);
        return view("admin.vehicle.pump_category")->with("data",$data);
    }

    public function addPumpCategory(Request $request){
        
        $data = $this->admin_model->addPumpCategory();

        return $data;
    }

    public function editPumpCategory(Request $request){
        // dd($request->request);
        $data = $this->admin_model->editPumpCategory();

        return $data;
    }




    public function showVehicles(){

        $category_data = $this->admin_model->showTMCategory();
        $sub_category_data = $this->admin_model->showTMSubCategoies();
        
        $data = $this->admin_model->showVehicles();
        $data["vehicle_categoies"] = $category_data;
        $data["subcategories"] = $sub_category_data;
        // dd($data);
        return view("admin.vehicle.TM_detail")->with("data",$data);
    }

    public function showVehiclesPmKM(){
        $data = array();
        // $category_data = $this->admin_model->showVehicleCategory();
        // $sub_category_data = $this->admin_model->showSubCategoies();
        
        $data = $this->admin_model->showVehiclesPMRM();
        // $data["vehicle_categoies"] = $category_data;
        // $data["subcategories"] = $sub_category_data;
        // dd($data);
        return view("admin.vehicle.vehicle_pm_km_rate")->with("data",$data);
    }

    public function addVehiclePMKM(Request $request){
        // dd($request->request);
        $data = $this->admin_model->addVehiclePMKM();

        return $data;
    }
    
    public function editVehiclePMKM(Request $request){
        // dd($request->request);
        $data = $this->admin_model->editVehiclePMKM();

        return $data;
    }
    
    public function getPMKMRateByStateId($state_id){
        // dd($request->request);
        $data = $this->admin_model->getPMKMRateByStateId($state_id);
        // dd($data);
        return $data;
    }
}
