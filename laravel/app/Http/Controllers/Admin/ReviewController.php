<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class ReviewController extends Controller
{
    //

    protected $review_model;
    protected $product_model;
    protected $auth_model;
    public function __construct(){
        $this->review_model = new \App\AdminModel\ReviewModel();
        $this->product_model = new \App\AdminModel\ProductModel();
        $this->auth_model = new \App\AdminModel\AuthenticationModel();
    }

    public function index(){

        $cat_data = $this->product_model->getCategories();
        $product_data = $this->product_model->showProducts();
        $user_data = $this->auth_model->showBuyers();
        $data = $this->review_model->showReviews();

        if(Request::get('categoryId')){
            $sub_cat_data = $this->product_model->getSubCategories();
            $data["sub_category"] = $sub_cat_data["data"];
            // dd($data);
        }

        $data["category"] = $cat_data["data"];        
        $data["product_data"] = $product_data["data"];        
        $data["user_data"] = $user_data["data"];        

        // dd($data);

        return view('admin.review.list_review')->with("data",$data);
    }
    
    public function editReview(){

        // dd(Request::all());
        $data = $this->review_model->editReview();
        // dd($data);

        return $data;
    }

    public function getSubCategories(){
        // dd(Request::all());
        $data = array();
        if(Request::get('categoryId')){
            $data = $this->product_model->getSubCategories();
            
            // dd($data);
        }

        echo json_encode($data['data']);

    }
    
    public function getProducts(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->product_model->showProducts();
            
        // dd($data);        

        echo json_encode($data['data']);

    }
    
    public function getBuyersList(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->auth_model->showBuyers();
            
        // dd($data);        

        echo json_encode($data['data']);

    }

    public function getFeedback(){

        $cat_data = $this->product_model->getCategories();
        $product_data = $this->product_model->showProducts();
        $user_data = $this->auth_model->showBuyers();
        $data = $this->review_model->showFeedback();

        if(Request::get('categoryId')){
            $sub_cat_data = $this->product_model->getSubCategories();
            $data["sub_category"] = $sub_cat_data["data"];
            // dd($data);
        }

        $data["category"] = $cat_data["data"];        
        $data["product_data"] = $product_data["data"];        
        $data["user_data"] = $user_data["data"];        

        // dd($data);

        return view('admin.review.list_feedback')->with("data",$data);
    }
    
}
