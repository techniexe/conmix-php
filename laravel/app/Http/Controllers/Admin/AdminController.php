<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use DateTime;
use DateTimeZone;
use Auth;
use URL;


class AdminController extends Controller
{
    //
    const ADMIN_ROLES = array(

        'superAdmin' => 'Super Admin',
        'admin_manager' => 'Admin Manager',
        'admin_sales_marketing' => 'Admin Sales & Marketing',
        'admin_accounts' => 'Admin Accounts',
        'admin_customer_care_grievances' => 'Customer Care & Grievances Admin',
        'it_manager' => 'IT Manager',

    );

    const ADMIN_ROLES_BASE = array(

        'superAdmin' => "superAdmin",
        'admin_manager' => "admin_manager",
        'admin_sales_marketing' => "admin_sales_marketing",
        'admin_accounts' => "admin_accounts",
        'admin_customer_care_grievances' => "admin_customer_care_grievances",
        'it_manager' => "it_manager",

    );

    protected $authenticationModel;
    public function __construct(Request $request){
        
        $this->authenticationModel = new \App\AdminModel\AuthenticationModel();

        
    }

    public function index(){
        // echo "Dashboard";
        
        if(session("custom_token",null)){

            $profile = session('profile_details', null);

            if(isset($profile["rights"]["dashboard_view"]) && $profile["rights"]["dashboard_view"]){
                return redirect('admin/dashboard');
            }else{

                $admin_role = $profile["admin_type"];

                if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_manager']){

                }

                if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_sales_marketing']){

                }

                if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_accounts']){
                
                }

                if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_customer_care_grievances']){
                    return redirect('admin/orders');
                }

                if($admin_role == AdminController::ADMIN_ROLES_BASE['it_manager']){

                }

            }
        }else{
            return view("admin.login")->with('data',array());
        }
    }

    public function login(LoginRequest $request){
        
        

        if(session("custom_token",null)){

            $profile = session('profile_details', null);

            if(isset($profile["rights"]["dashboard_view"]) && $profile["rights"]["dashboard_view"]){
                return redirect('admin/dashboard');
            }else{

                $admin_role = $profile["admin_type"];

                if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_manager']){

                }

                if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_sales_marketing']){

                }

                if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_accounts']){
                
                }

                if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_customer_care_grievances']){
                    return redirect('admin/orders');
                }

                if($admin_role == AdminController::ADMIN_ROLES_BASE['it_manager']){

                }

            }
        }else{
            $validated = $request->validated();
            
            $data = $this->authenticationModel->login();
            
            // dd($data);
            if($data["status"] == 200){
                // return redirect('admin/dashboard');
                $profile = session('profile_details', null);
                // dd($profile);
                if(isset($profile["rights"]["dashboard_view"]) && $profile["rights"]["dashboard_view"]){
                    return redirect('admin/dashboard_after_login');
                }else{

                    $admin_role = $profile["admin_type"];

                    if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_manager']){

                    }

                    if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_sales_marketing']){

                    }

                    if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_accounts']){
                    
                    }

                    if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_customer_care_grievances']){
                        return redirect('admin/orders');
                    }

                    if($admin_role == AdminController::ADMIN_ROLES_BASE['it_manager']){

                    }

                }
            }else{
                return view("admin.login")->with('data',$data);
            }
        }
    }

    public function dashboard(){

        // $this->authenticationModel->getAuthToken();
        // exit;

        return view("admin.dashboard");
    }

    public function afterLogin(){
    
        // Auth::logout(); // logout user
        return redirect(\URL::previous());

    }

    public function adminUsersListing(){

        return view("admin.admin_user");

    }

    public function logout(){

        $this->authenticationModel->logout();

        // return redirect('admin');

        Auth::logout(); // logout user
        return redirect(\URL::previous());
    }

    public static function dateTimeFormat($utc){

        $dt = new DateTime($utc);
        $tz = new DateTimeZone('Asia/Kolkata'); // or whatever zone you're after

        $dt->setTimezone($tz);
        $datetime = $dt->format('d M Y, h:i:s a');

        return $datetime;

    }

    public static function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}
