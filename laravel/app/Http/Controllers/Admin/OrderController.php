<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class OrderController extends Controller
{
    //
    protected $order_model;
    public function __construct(){
        
        $this->order_model = new \App\AdminModel\OrdersModel();
    }

    public function index(){

        
        $data = $this->order_model->showOrders();  

        // dd($data);

        return view('admin.ledger.orders')->with("data",$data);

    }

    public function updateStatus(){

        // dd(Request::all());
        // dd($data);
        $response = $this->order_model->updateStatus();

        return $response;


    }

    public function orderDetails($order_id){
        // dd($order_id);
        $data = array();
        $data = $this->order_model->orderDetails($order_id);  

        // dd($data);

        return view('admin.ledger.order_details')->with("data",$data);

    }

    public function updateItemStatus(){

        // dd(Request::all());
        // dd($data);
        $response = $this->order_model->updateItemStatus();

        return $response;


    }
    
    public function viewInvoice($order_id){
        // dd($invoice_id);
        $data = array();
        $data = $this->order_model->showInvoice($order_id);  

        // dd($data);

        return view('admin.ledger.invoice')->with("data",$data);

    }
    
    public function itemTrack($item_id){
        
        $data = array();
        $data = $this->order_model->showItemTrack($item_id);  

        // dd($data);

        return view('admin.ledger.item_track')->with("data",$data);

    }
    
    public function transactions(){
        
        $data = array();
        $data = $this->order_model->showTransactions();  

        // dd($data);

        return view('admin.ledger.transaction')->with("data",$data);

    }

    public function buyersCancelOrders(){

        $data = array();
        // $data = $this->order_model->showSupplierOrderDetail();  

        // dd($data);

        return view('admin.ledger.buyers_cancel_orders')->with("data",$data);

    }
    
    public function buyersProcessingOrders(){

        $data = array();
        // $data = $this->order_model->showSupplierOrderDetail();  

        // dd($data);

        return view('admin.ledger.buyers_processing_orders')->with("data",$data);

    }


    public function trackOrderItem($item_id){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->trackOrderItem($item_id);
        // dd($data);
        
        if($data["status"] == 200){

            $returnHTML = view('admin.ledger.order_item_truck_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }      
        

        return $data;
        // return view("buyer.views.order_item_track")->with('data',$data);

    }


    public function trackingDetails($item_id,$tracking_id){

        // dd(Request::all());

        $data = $this->order_model->trackingDetails($item_id,$tracking_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('admin.ledger.order_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }       
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }


    public function CPtrackingDetails($item_part_id){

        // dd(Request::all());

        $data = $this->order_model->CPtrackingDetails($item_part_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('admin.ledger.order_cp_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }       
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }



    // Supplier Orderrs.........................................................................


    public function showSuppliersOrder(){

        $data = array();
        $data = $this->order_model->showSuppliersOrder();  

        // dd($data);

        return view('admin.ledger.supplier_order')->with("data",$data);


    }

    public function showSupplierOrderDetail($order_id){
        
        $data = array();
        $data = $this->order_model->showSupplierOrderDetail($order_id);  

        // dd($data);

        return view('admin.ledger.supplier_order_detail')->with("data",$data);

    }

    public function updateSupplierBillStatus(){
        // dd(Request::all());
        $data = array();
        $data = $this->order_model->updateSupplierBillStatus();  

        return $data;
    }

    public function supplierTrackingDetails($item_id,$tracking_id){

        // dd(Request::all());

        $data = $this->order_model->supplierTrackingDetails($item_id,$tracking_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('admin.ledger.supplier_order_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }

        
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }

    public function SupplierCPtrackingDetails($item_part_id){

        // dd(Request::all());

        $data = $this->order_model->SupplierCPtrackingDetails($item_part_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('admin.ledger.order_cp_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }       
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }

    // Supplier Orderrs Over.........................................................................


    // Logistics Orderrs Start.........................................................................

    public function showLogisticsOrder(){

        $data = array();
        $data = $this->order_model->showLogisticsOrder();

        // dd($data);

        return view('admin.ledger.logistics_order')->with("data",$data);


    }

    public function showLogisticsOrderDetail($order_id){

        $data = array();
        $data = $this->order_model->showLogisticsOrderDetail($order_id);  

        // dd($data);

        return view('admin.ledger.logistic_order_detail')->with("data",$data);

    }

    public function updateLogisticsBillStatus(){
        // dd(Request::all());
        $data = array();
        $data = $this->order_model->updateLogisticsBillStatus();  

        return $data;
    }

    public function logisticsTrackingDetails($item_id,$tracking_id){

        // dd(Request::all());

        $data = $this->order_model->logisticsTrackingDetails($item_id,$tracking_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('admin.ledger.logistics_order_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }

        
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }

    // Logistics Orderrs Over.........................................................................
}
