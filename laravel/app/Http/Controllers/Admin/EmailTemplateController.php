<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class EmailTemplateController extends Controller
{
    //
    protected $email_template_model;
    public function __construct(){
        $this->email_template_model = new \App\AdminModel\EmailTemplateModel();

    }

    public function index(){

        $data = $this->email_template_model->showTemplates();
        // dd($data);

        return view('admin.email_template.email_template')->with("data",$data);

    }

    public function addEmailTemplate(){
        // dd(Request::all());
        $data = $this->email_template_model->addEmailTemplate();

        return $data;
    }

    public function getEmailTemplate(){

        // dd(Request::all());
        $data = $this->email_template_model->getEmailTemplate();

        return $data;

    }

    public function showEmailTemplateInIframe(){

        // dd(Request::all());
        $data = $this->email_template_model->getEmailTemplate(false);
        // dd($data);
        $data = $data["data"];

        echo $data['html'];

    }

    public function editEmailTemplate(){

        // dd(Request::all());
        $data = $this->email_template_model->editEmailTemplate();

        return $data;

    }
    
    public function deleteEmailAttachment(){

        // dd(Request::all());
        $data = $this->email_template_model->deleteEmailAttachment();

        return $data;

    }
}
