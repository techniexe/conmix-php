<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    protected $dashboard_model;
    public function __construct(Request $request){
        
        $this->dashboard_model = new \App\AdminModel\DashboardModel();

        
    }

    public function index(){
        // echo "Dashboard";
        
        $data = $this->dashboard_model->getDashboardDetails();
        // dd($data);
        $monthly_chart_data = $this->dashboard_model->getMonthlyChartDetails();
        $day_chart_data = $this->dashboard_model->getDayChartDetails();
        $buyer_latest_order_data = $this->dashboard_model->getBuyerLatestOrders();
        $supplier_latest_order_data = $this->dashboard_model->getSuppliersLatestOrders();
        $logistics_latest_order_data = $this->dashboard_model->getLogisticsLatestOrders();
        $unverified_product_data = $this->dashboard_model->getUnverifiedProducts();
        
        $data["monthly_chart_data"] = $monthly_chart_data["data"];
        $data["day_chart_data"] = $day_chart_data["data"];
        $data["buyer_latest_order_data"] = $buyer_latest_order_data["data"];
        $data["supplier_latest_order_data"] = $supplier_latest_order_data["data"];
        $data["logistics_latest_order_data"] = $logistics_latest_order_data["data"];
        $data["unverified_product_data"] = $unverified_product_data["data"];
        // dd($data);
        return view("admin.dashboard")->with("data",$data);
    }

    public function getMonthlyChartDetails(){

        $data = $this->dashboard_model->getMonthlyChartDetails();
        // dd($data);
        return $data;
    }
    
    public function getDayChartDetails(){

        $data = $this->dashboard_model->getDayChartDetails();

        return $data;
    }
}
