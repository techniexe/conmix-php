<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class GradeCatBrandController extends Controller
{
    //
    protected $grade_model;
    public function __construct(){
        $this->grade_model = new \App\AdminModel\GradeCatBrandModel();
    }

    public function index(){

        $data = array();
        $data = $this->grade_model->getConcreteGrades();
        // dd($data);

        return view('admin.grade_cat_brand.concrete_grade')->with("data",$data);

    }

    public function addConcreteGrade(){
        // dd(Request::all());
        $data = $this->grade_model->addConcreteGrade();

        return $data;

    }

    public function editConcreteGrade(){
        // dd(Request::all());
        $data = $this->grade_model->editConcreteGrade();

        return $data;
    }


    public function getCementBrand(){

        $data = array();
        $data = $this->grade_model->getCementBrand();
        // dd($data);

        return view('admin.grade_cat_brand.cement_brand')->with("data",$data);

    }

    public function addCementBrand(){
        // dd(Request::all());
        $data = $this->grade_model->addCementBrand();

        return $data;

    }

    public function editCementBrand(){
        // dd(Request::all());
        $data = $this->grade_model->editCementBrand();

        return $data;
    }
    
    
    public function getAddMixtureBrand(){

        $data = array();
        $data = $this->grade_model->getAddMixtureBrand();
        // dd($data);


        return view('admin.grade_cat_brand.addmixture_brand')->with("data",$data);

    }

    public function addAddMixtureBrand(){
        // dd(Request::all());
        $data = $this->grade_model->addAddMixtureBrand();

        return $data;

    }

    public function editAddMixtureBrand(){
        // dd(Request::all());
        $data = $this->grade_model->editAddMixtureBrand();

        return $data;
    }
    
    public function getAddMixtureTypes(){

        $data = array();
        $data = $this->grade_model->getAddMixtureTypes();
        $admixture_brand_data = $this->grade_model->getAddMixtureBrand();
        $admix_types = $this->grade_model->getAddMixtureCatTypes();
        // dd($admix_types);
        $data['admixture_types'] = $admix_types['data'];

        $data["admixture_brand_data"]  = $admixture_brand_data["data"];

        // dd($data);

        return view('admin.grade_cat_brand.addmixture_types')->with("data",$data);

    }

    public function addAddMixtureTypes(){
        // dd(Request::all());
        $data = $this->grade_model->addAddMixtureTypes();

        return $data;

    }

    public function editAddMixtureTypes(){
        // dd(Request::all());
        $data = $this->grade_model->editAddMixtureTypes();

        return $data;
    }

    public function getCementGrade(){

        $data = array();
        $data = $this->grade_model->getCementGrade();
        // dd($data);

        return view('admin.grade_cat_brand.cement_grade')->with("data",$data);

    }

    public function addCementGrade(){
        // dd(Request::all());
        $data = $this->grade_model->addCementGrade();

        return $data;

    }

    public function editCementGrade(){
        // dd(Request::all());
        $data = $this->grade_model->editCementGrade();

        return $data;
    }
    
    public function getPaymentMethod(){

        $data = array();
        $data = $this->grade_model->getPaymentMethod();
        // dd($data);

        return view('admin.grade_cat_brand.payment_method')->with("data",$data);

    }

    public function addPaymentMethod(){
        // dd(Request::all());
        $data = $this->grade_model->addPaymentMethod();

        return $data;

    }

    public function editPaymentMethod(){
        // dd(Request::all());
        $data = $this->grade_model->editPaymentMethod();

        return $data;
    }
}
