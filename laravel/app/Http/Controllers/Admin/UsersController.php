<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\Input;

class UsersController extends Controller
{
    //
    protected $admin_model;
    public function __construct(){
        $this->admin_model = new \App\AdminModel\AuthenticationModel();
    }

    public function index(){
        
        $data = $this->admin_model->adminUserListing();
        
        return view("admin.admin_user")->with("data",$data);
    }

    public function addUser(Request $request){
       
        $data = $this->admin_model->addAdminUser($request);

        return $data;
    }

    public function requestOTP(){
        // dd(Request::all());
        $data = $this->admin_model->requestOTP();

        return $data;

    }
    
    public function requestOTPForEditProfile(){
        // dd(Request::all());
        $data = $this->admin_model->requestOTPForEditProfile();
        // dd($data);
        return $data;

    }

    public function activeInActiveUser(Request $request){
        // dd($request->request);

        $data = $this->admin_model->activeInActiveUser($request);

        return $data;
    }

    public function editUserDetails(Request $request){
        // dd($request->request);
        $data = $this->admin_model->editUserDetails($request);

        return $data;

    }

    public function deleteUser(Request $request){
        
        $data = $this->admin_model->deleteUser($request);

        return $data;


    }

    public function adminUserApplyFilter(){
        
        $data = $this->admin_model->adminUserListing();

        return view("admin.admin_user")->with("data",$data);
    }

// Logistics listing.................................

    public function showLogistics(){

        $data = $this->admin_model->showLogistics();
        // dd($data);
        return view("admin.logistics_user")->with("data",$data);

    }

// Logistics listing Over.................................

// Supliers listing .................................

    public function showSuppliers(){
        
        $data = $this->admin_model->showSuppliers();
        // dd($data);
        return view("admin.supliers_user")->with("data",$data);

    }

    public function supplierVerifyByAdmin(){

        // dd(Request::all());

        $data = $this->admin_model->supplierVerifyByAdmin();
        // dd($data);
        return $data;

    }
    
    public function supplierPlantVerifyByAdmin(){

        // dd(Request::all());

        $data = $this->admin_model->supplierPlantVerifyByAdmin();
        // dd($data);
        return $data;

    }
    
    public function supplierBlockedByAdmin(){

        // dd(Request::all());

        $data = $this->admin_model->supplierBlockedByAdmin();
        // dd($data);
        return $data;

    }
    
    public function supplierPlantBlockedByAdmin(){

        // dd(Request::all());

        $data = $this->admin_model->supplierPlantBlockedByAdmin();
        // dd($data);
        return $data;

    }
    
    public function supplierRejectedByAdmin(){

        // dd(Request::all());

        $data = $this->admin_model->supplierRejectedByAdmin();
        // dd($data);
        return $data;

    }
    
    public function supplierplantRejectedByAdmin(){

        // dd(Request::all());

        $data = $this->admin_model->supplierplantRejectedByAdmin();
        // dd($data);
        return $data;

    }
    
    public function logisticsVerifyByAdmin(){

        // dd(Request::all());

        $data = $this->admin_model->logisticsVerifyByAdmin();
        // dd($data);
        return $data;

    }

    public function showSuppliersPlants(){
        
        $data = $this->admin_model->showSuppliersPlants();
        // dd($data);

        $returnHTML = view('admin.suppliers_plants')->with('plant_details', $data)->render();
        $returnHTMLslide = "";
        
        return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));

    }

// Supliers listing Over.................................

// Buyers listing .................................

public function showBuyers(){
    $data = array(); 
    $data = $this->admin_model->showBuyers();
    // dd($data);
    return view("admin.buyers_user")->with("data",$data);

}

// Buyers listing Over.................................
    
}
