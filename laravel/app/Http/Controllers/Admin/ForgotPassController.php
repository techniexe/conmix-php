<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class ForgotPassController extends Controller
{
    //
    protected $forgot_pass_model;
    public function __construct(Request $request){
        
        $this->forgot_pass_model = new \App\AdminModel\ForgotPassModel();

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        // $data = $this->bank_model->showBankDetails();
        
        // dd($data);

        return view('admin.forgot_pass.forgot_pass_email_verifier')->with("data",$data);

    }

    public function forgotPassOTPSend(){
        // dd(Request::all());
        $method = Request::method();

        if($method == "POST"){

            $data = $this->forgot_pass_model->forgotPassOTPSend();
            // dd($data);
            if($data["status"] == 200){
                // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
                return view("admin.forgot_pass.forgot_pass_email_verifier_otp")->with('data',$data);
            }else{
                return view("admin.forgot_pass.forgot_pass_email_verifier")->with('data',$data);
            }
        }else{
            return redirect()->route('admin_forgot_pass_email_verifier');
        }

    }

    public function forgotPassOTPVerify(){

        $method = Request::method();

        if($method == "POST"){

            $data = $this->forgot_pass_model->forgotPassOTPVerify();
            // dd($data);
            if($data["status"] == 200){
                $data["message"] = "OTP verified successfully";
                // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
                return view("admin.forgot_pass.forgot_pass")->with('data',$data);

                // return redirect()->route('admin_dashboard');
            }else{
                return view("admin.forgot_pass.forgot_pass_email_verifier_otp")->with('data',$data);
            }
        }else{
            return redirect()->route('admin_forgot_pass_email_verifier');
        }

    }

    public function forgotPass(){

        $method = Request::method();

        if($method == "POST"){
            $data = $this->forgot_pass_model->forgotPass();
            // dd($data);
            if($data["status"] == 200){
                $data["message"] = "Password updated successfully. Please login";
                // dd($data);
                // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
                return redirect()->route('admin_login')->with('data',$data);
                // return view("admin.login")->with('data',$data);
            }else{
                return view("admin.forgot_pass.forgot_pass")->with('data',$data);
            }
        }else{
                return redirect()->route('admin_forgot_pass_email_verifier');
            }

    }

}
