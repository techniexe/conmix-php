<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class ProposalController extends Controller
{
    //

    protected $proposal_model;
    protected $product_model;
    protected $region_model;
    public function __construct(){
        
        $this->proposal_model = new \App\AdminModel\ProposalModel();
        $this->product_model = new \App\AdminModel\ProductModel();
        $this->region_model = new \App\AdminModel\RegionModel();
    }

    public function index(){

        $data = array();
        $cat_data = $this->product_model->getCategories();
        $city_data = $this->region_model->showCities();
        $data = $this->proposal_model->showProposals();   

        $data["product_cat_data"] = $cat_data["data"];
        $data["city_data"] = $city_data["data"];

        // dd($data);

        return view('admin.new_proposal.proposal')->with("data",$data);

    }

    public function sendProposalEmail(){

        // dd(Request::all());
        $data = $this->proposal_model->sendProposalEmail();

        // dd($data);

        return $data;


    }
}
