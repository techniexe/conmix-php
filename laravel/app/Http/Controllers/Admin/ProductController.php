<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class ProductController extends Controller
{
    //

    protected $product_model;
    public function __construct(){
        $this->product_model = new \App\AdminModel\ProductModel();
    }

    public function index(){

        $data = $this->product_model->showProductCategories();
        // dd($data);

        return view('admin.grade_cat_brand.product_cat')->with("data",$data);

    }

    public function addCategory(){
        // dd(Request::all());
        $data = $this->product_model->addCategory();

        return $data;

    }

    public function editCategory(){
        // dd(Request::all());
        $data = $this->product_model->editCategory();

        return $data;
    }

    public function getCategory(){

        $data = $this->product_model->showProductCategories();

        return json_encode($data["data"]);

    }

    public function showSubCategories(){
        
        $data = array();
        $data["data"] = array();
        $cat_data = $this->product_model->getCategories();
        $gst_slab_data = $this->product_model->showGstSalb();
        $margin_rate_data = $this->product_model->showMarginRate();
        // dd($cat_data);


        $default_category_id = "";
        if(isset($cat_data["data"])){

            foreach($cat_data["data"] as $value){
                if($value["category_name"] == "Aggregate"){
                    $default_category_id  = $value["_id"];
                }
            }

        }

        if(Request::get('categoryId')){
            $data = $this->product_model->showSubCategories(Request::get('categoryId'));
            // dd($data);
        }else{
            $data = $this->product_model->showSubCategories($default_category_id);
        }

        $data["product_cat_data"] = $cat_data["data"];
        $data["gst_slab_data"] = $gst_slab_data["data"];
        $data["margin_rate_data"] = $margin_rate_data["data"];
        // dd($data);

        return view('admin.grade_cat_brand.product_sub_cat')->with("data",$data);

    }
    public function getSubCategory(){

        $data = $this->product_model->showSubCategories();

        return json_encode($data["data"]);

    }


    public function addSubCategory(){

        // dd(Request::all());
        $data = $this->product_model->addSubCategory();

        return $data;

    }

    public function editSubCategory(){

        // dd(Request::all());
        $data = $this->product_model->editSubCategory();

        return $data;

    }

    public function showProducts(){

        $data = array();
        $cat_data = $this->product_model->getCategories();
        $sub_cat_data = $this->product_model->getSubCategories();
        
        $data = $this->product_model->showProducts();
        $data['category'] = $cat_data["data"];
        $data['sub_category'] = $sub_cat_data["data"];
        // dd($data);

        return view('admin.grade_cat_brand.designmix_lists')->with("data",$data);

    }

    public function designmix_detail(){

        $data = array();


        $data = $this->product_model->designmix_detail();  
       

        // dd($data);
        if($data["status"] == 200){

            
            $suggestion_array = array();
            $final_data = array();
            
            // dd($cart_details);
            // $final_data["cart_details"] = $cart_details;

                $returnHTML = view('admin.grade_cat_brand.design_mix_grade_wise_ajax')->with('data', $data["data"])->render();
                $returnHTMLslide = "";        

                
        

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $data;
        }


        return $data;

    }

    public function editProducts(){

        // dd(Request::all());
        $data = $this->product_model->editProducts();

        return $data;

    }

    public function verifyProducts(){

        // dd(Request::all());
        $data = $this->product_model->verifyProducts();

        return $data;

    }






    public function getGstSalb(){

        $data = $this->product_model->showGstSalb();

        return json_encode($data["data"]);

    }
    
    public function showGstSalb(){

        $data = $this->product_model->showGstSalb();
        // dd($data);
        return view('admin.grade_cat_brand.gst_info')->with("data",$data);

    }

    public function addGstSalb(){
        // dd(Request::all());
        $data = $this->product_model->addGstSalb();
        // dd($data);
        return $data;

    }

    public function editGstSalb(){
        // dd(Request::all());
        $data = $this->product_model->editGstSalb();
        // dd($data);
        return $data;

    }

    public function getMarginRate(){

        $data = $this->product_model->showMarginRate();

        return json_encode($data["data"]);

    }
    
    public function showMarginRate(){

        $data = $this->product_model->showMarginRate();
        // dd($data);
        return view('admin.grade_cat_brand.margin_slab')->with("data",$data);

    }
    
    public function addMarginSlab(){
        // dd(Request::all());
        $data = $this->product_model->addMarginSlab();
        // dd($data);
        return $data;

    }
    
    public function editMarginSlab(){
        // dd(Request::all());
        $data = $this->product_model->editMarginSlab();
        // dd($data);
        return $data;

    }


    public function showAggregateSource(){
        $data = array();
        $data = $this->product_model->showAggregateSource();
        // dd($data);
        return view('admin.grade_cat_brand.aggregate_source')->with("data",$data);

    }

    public function addAggregateSource(){
        // dd(Request::all());
        $data = $this->product_model->addAggregateSource();
        // dd($data);
        return $data;

    }
    
    public function editAggregateSource(){
        // dd(Request::all());
        $data = $this->product_model->editAggregateSource();
        // dd($data);
        return $data;

    }
    
    public function deleteAggregateSource(){
        // dd(Request::all());
        $data = $this->product_model->deleteAggregateSource();
        // dd($data);
        return $data;

    }
    
    
    public function showSandSource(){
        $data = array();
        $data = $this->product_model->showSandSource();
        // dd($data);
        return view('admin.grade_cat_brand.sand_source')->with("data",$data);

    }

    public function addSandSource(){
        // dd(Request::all());
        $data = $this->product_model->addSandSource();
        // dd($data);
        return $data;

    }
    
    public function editSandSource(){
        // dd(Request::all());
        $data = $this->product_model->editSandSource();
        // dd($data);
        return $data;

    }
    
    public function deleteSandSource(){
        // dd(Request::all());
        $data = $this->product_model->deleteSandSource();
        // dd($data);
        return $data;

    }
    
    
    public function showFlyashSource(){
        $data = array();
        $data = $this->product_model->showFlyashSource();
        // dd($data);
        return view('admin.grade_cat_brand.flyash')->with("data",$data);

    }

    public function addFlyashSource(){
        // dd(Request::all());
        $data = $this->product_model->addFlyashSource();
        // dd($data);
        return $data;

    }
    
    public function editFlyashSource(){
        // dd(Request::all());
        $data = $this->product_model->editFlyashSource();
        // dd($data);
        return $data;

    }
    
    public function deleteFlyashSource(){
        // dd(Request::all());
        $data = $this->product_model->deleteFlyashSource();
        // dd($data);
        return $data;

    }

    public function showSandZone(){
        $data = array();
        $data = $this->product_model->showSandZone();
        // dd($data);
        return view('admin.grade_cat_brand.sand_zone')->with("data",$data);

    }

    public function addSandZone(){
        // dd(Request::all());
        $data = $this->product_model->addSandZone();
        // dd($data);
        return $data;

    }
    
    public function editSandZone(){
        // dd(Request::all());
        $data = $this->product_model->editSandZone();
        // dd($data);
        return $data;

    }
    
    public function deleteSandZone(){
        // dd(Request::all());
        $data = $this->product_model->deleteSandZone();
        // dd($data);
        return $data;

    }
}
