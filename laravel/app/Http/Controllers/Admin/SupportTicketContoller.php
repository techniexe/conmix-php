<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class SupportTicketContoller extends Controller
{
    //

    protected $support_model;
    protected $auth_model;
    public function __construct(){
        
        $this->support_model = new \App\AdminModel\SupportTicketModel();
        $this->auth_model = new \App\AdminModel\AuthenticationModel();
    }

    public function index(){

        $data = array();
        $buyer_data = $this->auth_model->showBuyers();
        $supplier_data = $this->auth_model->showSuppliers();
        $logistics_data = $this->auth_model->showLogistics();
        
        $data = $this->support_model->showTickets();  

        $data["buyer_data"] = $buyer_data["data"]; 
        $data["supplier_data"] = $supplier_data["data"]; 
        $data["logistics_data"] = $logistics_data["data"]; 

        // dd($data);

        return view('admin.support_ticket.support_ticket')->with("data",$data);

    }
    
    public function showTicketDetails($ticket_id){

        $data = array();
        $data = $this->support_model->showTicketDetails($ticket_id);  
        $ticket_messages = $this->support_model->getTicketMessages($ticket_id);  
        $data["ticket_id"] = $ticket_id;
        $data["ticket_messages"] = $ticket_messages["data"];
        // dd($data);

        return view('admin.support_ticket.ticket_detail')->with("data",$data);

    }

    public function addTicket(){

        // dd(Request::all());
        // dd($data);
        $response = $this->support_model->addTicket();

        return $response;

    }
    
    public function replyTicket(){

        // dd(Request::all());
        // dd($data);
        $response = $this->support_model->replyTicket();

        return $response;

    }
    

    public function getSuppliersList(){

        $data = array();
        
        $data = $this->auth_model->showSuppliers();
            
        // dd($data);        

        echo json_encode($data['data']);

    }
    
    public function getBuyersList(){

        $data = array();
        
        $data = $this->auth_model->showBuyers();
            
        // dd($data);        

        echo json_encode($data['data']);

    }
    
    public function getLogisticsList(){

        $data = array();
        
        $data = $this->auth_model->showLogistics();
            
        // dd($data);        

        echo json_encode($data['data']);

    }

    public function updateTicketStatus(){

        $response = $this->support_model->updateTicketStatus();

        return $response;

    }
}
