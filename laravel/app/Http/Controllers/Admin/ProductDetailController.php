<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductDetailController extends Controller
{
    //
    protected $product_detail_model;
    public function __construct(){
        $this->product_detail_model = new \App\AdminModel\ProductDetailModel();
    }

    public function index($product_id){

        $data = $this->product_detail_model->getProductDetails($product_id);
        // dd($data);

        return view('admin.product.product_detail')->with("data",$data);

    }
}
