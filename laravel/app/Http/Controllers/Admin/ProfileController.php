<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class ProfileController extends Controller
{
    //
    protected $profile_model;
    protected $common_model;
    public function __construct(){
        
        $this->profile_model = new \App\AdminModel\ProfileModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){

        $data = $this->profile_model->showProfileDetails();

        // dd($data);


        return view('admin.profile.my_profile')->with("data",$data);

    }

    public function editProfile(){

    //    dd(Request::all());
         $response = $this->profile_model->editProfile();
        //  dd($response);
         return $response;

    }
    
    public function changePassword(){

    //    dd(Request::all());
         $response = $this->profile_model->changePassword();
        //  dd($response);
         return $response;

    }

    public function getAdminNotifications(){

        //    dd(Request::all());
            $response = $this->common_model->getAdminNotifications();

        //  dd($response);

        if($response["status"] == 200){



            $returnHTML = view('admin.common.noti_list_ajax')->with('data', $response)->render();
            $returnHTMLslide = "";
                
            
            

            return response()->json(array('status' => $response["status"], 'html'=>$returnHTML));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $response;
        }

        //  dd($response);
            return $response;
    
    }


    public function adminNotifications(){

        //    dd(Request::all());
        // $data = $this->common_model->getAdminNotifications();

        $data = array();

        $data = $this->common_model->getAdminNotifications();
        // dd($response);
        return view('admin.noti.notification')->with("data",$data);
    
    }

    public function updateNotiStatus($id){

        //    dd(Request::all());
        $response = $this->profile_model->updateNotiStatus($id);
        //  dd($response);
        return $response;

    }
}
