<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class CouponController extends Controller
{
    //
    protected $coupon_model;
    protected $admin_model;
    public function __construct(){
        $this->coupon_model = new \App\AdminModel\CouponModel();
        $this->admin_model = new \App\AdminModel\AuthenticationModel();
    }

    public function index(){

        $data = $this->coupon_model->showcoupons();

        $buyerlist = $this->admin_model->showBuyers();

        $data["buyerlist"] = $buyerlist["data"];
        // dd($data);

        return view('admin.coupon.coupons')->with("data",$data);

    }

    public function addCoupon(){

        // dd(Request::all());

        $data = $this->coupon_model->addCoupon();


        return $data;

    }

    public function editCoupon(){

        // dd(Request::all());

        $data = $this->coupon_model->editCoupon();


        return $data;

    }
    
    public function couponActiveInActive(){

        // dd(Request::all());

        $data = $this->coupon_model->couponActiveInActive();


        return $data;

    }
    
    public function couponDelete(){

        // dd(Request::all());

        $data = $this->coupon_model->couponDelete();


        return $data;

    }
}
