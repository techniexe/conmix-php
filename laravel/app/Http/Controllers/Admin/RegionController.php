<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class RegionController extends Controller
{
    //
    protected $region_model;
    public function __construct(){
        $this->region_model = new \App\AdminModel\RegionModel();
    }

    public function index(){

        $data = $this->region_model->showCountries();
        // dd($data);

        return view('admin.region.country')->with("data",$data);
    }

    public function addCountry(){
        // dd(Request::all());
        $data = $this->region_model->addCountry();

        return $data;
    }

    public function editCountry(){

        $data = $this->region_model->editCountry();

        return $data;
    }

    public function deleteCountry(){
        
        $data = $this->region_model->deleteCountry();

        return $data;
    }

    public function getCountries(){

        $data = $this->region_model->showCountries();
        // dd($data);

        echo json_encode($data["data"]);
    }

    public function showStates(){

        $countries = $this->region_model->showCountries();
        $data = $this->region_model->showStates('true');
        $data["countries"] = $countries["data"];
        // dd($data);

        return view('admin.region.state')->with("data",$data);

    }
    public function addState(){
        // dd(Request::all());
        $data = $this->region_model->addState();

        return $data;
    }

    public function editState(){

        $data = $this->region_model->editState();

        return $data;

    }

    public function deleteState(){

        $data = $this->region_model->deleteState();

        return $data;

    }

    public function getStates(){

        $data = $this->region_model->showStates();
        // dd($data);

        echo json_encode($data["data"]);
    }

    public function showCities(){

        $states = $this->region_model->showStates();
        $data = $this->region_model->showCities('true');
        $data["states"] = $states["data"];
        // dd($data);

        return view('admin.region.city')->with("data",$data);

    }

    public function addCity(){
        // dd(Request::all());
        $data = $this->region_model->addCity();
        // dd($data);
        return $data;
    }

    public function editCity(){

        $data = $this->region_model->editCity();

        return $data;

    }

    public function deleteCity(){

        $data = $this->region_model->deleteCity();

        return $data;

    }

    public function getCities(){

        $data = $this->region_model->showCities();

        echo json_encode($data["data"]);
    }

}
