<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;

class AccessLogController extends Controller
{
    //
    protected $access_model;
    public function __construct(){
        $this->access_model = new \App\AdminModel\AccessLogModel();
    }

    public function index(){
        
        $data = $this->access_model->showEmailAccessLog();

        // dd($data);

        return view('admin.logs_access.email_logs')->with("data",$data);
    }

    public function emailResend(){

        // dd(Request::all());
        $data = $this->access_model->emailResend();

        return $data;

    }

    public function showActionLogs(){

        $data = $this->access_model->showActionLogs();

        // dd($data);

        return view('admin.logs_access.action_logs')->with("data",$data);

    }
}
