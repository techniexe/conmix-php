<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Request;

class DriverController extends Controller
{
    //
    protected $driver_model;
    public function __construct(Request $request){
        
        $this->driver_model = new \App\LogisticsModel\DriverModel();

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->showDriverDetails();
        
        // dd($data);

        return view('logistics.driver.driver_details')->with("data",$data);

    }

    public function addDriverDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->addDriverDetails();
        
        // dd($data);

        return $data;

    }
    
    public function editDriverDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->editDriverDetails();
        
        // dd($data);

        return $data;

    }
}
