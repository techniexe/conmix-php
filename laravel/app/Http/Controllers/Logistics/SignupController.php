<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Request;

class SignupController extends Controller
{
    //
    protected $signup_model;
    protected $common_model;
    public function __construct(Request $request){
        
        $this->signup_model = new \App\LogisticsModel\SignupModel();
        
        $this->common_model = new \App\CommonAPIModel();

        
    }

    public function index(){
        // echo "Dashboard";
        
        if(session("custom_token",null)){
            return redirect('dashboard');
        }else{
            return view("logistics.Authentication.signup")->with('data',array());
        }
    }

    

    public function emailVerifier(){


        return view("logistics.Authentication.signup_email_verifier")->with('data',array());
    }
    
    public function emailVerifierOtp(){


        return view("logistics.Authentication.signup_email_verifier_otp")->with('data',array());
    }

    public function signupOTPSend(){

        $data = $this->signup_model->signupOTPSend();

        if($data["status"] == 200){
            // return redirect('logistics/signup_email_verifier_otp')->with('data',$data);
            return view("logistics.Authentication.signup_email_verifier_otp")->with('data',$data);
        }else{
            return view("logistics.Authentication.signup_email_verifier")->with('data',$data);
        }

    }

    public function signupOTPVerify(){

        $data = $this->signup_model->signupOTPVerify();
        // dd($data);
        if($data["status"] == 200){
            // return redirect('logistics/signup_email_verifier_otp')->with('data',$data);
            return view("logistics.Authentication.signup")->with('data',$data);
        }else{
            return view("logistics.Authentication.signup_email_verifier_otp")->with('data',$data);
        }

    }

    public function signupRegistration(){
        // dd(Request::all());
        $data = $this->signup_model->signupRegistration();
        
        // dd($data);
        if($data["status"] == 200){

            $state_data = $this->common_model->getStates();
            $city_data = $this->common_model->getCities();

            $data["state_data"] = $state_data["data"];
            $data["city_data"] = $city_data["data"];

            // return redirect('logistics/dashboard')->with('success',"You are registered successfully.");
            return view("logistics.Authentication.signup_2")->with('data',$data);
            // return view("logistics.Authentication.signup")->with('data',$data);
        }else{
            return view("logistics.Authentication.signup")->with('data',$data);
        }
    }
    
    public function signupRegistration2(){
        // dd(Request::all());
        $data = array();
        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();

        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];

        // $data = $this->signup_model->signupRegistration();
        // dd($data);
        // if($data["status"] == 200){
        //     return redirect('logistics/dashboard')->with('success',"You are registered successfully.");
        //     // return view("logistics.Authentication.signup")->with('data',$data);
        // }else{
            return view("logistics.Authentication.signup_2")->with('data',$data);
        // }
    }

    public function signupRegistration2Submit(){
        // dd(Request::all());
        $data = $this->signup_model->signupRegistration2Submit();
        // dd($data);
        if($data["status"] == 200){
            if((Request::post('is_my_profile') != null)){
                return redirect('logistics/profile/edit_view')->with('success',"Detail updated successfully");
            }else{
                return redirect('logistics/dashboard')->with('success',"You are registered successfully.");
            }
            
            // return view("logistics.Authentication.signup")->with('data',$data);
        }else{
            return view("logistics.Authentication.signup_2")->with('data',$data);
        }

    }
}
