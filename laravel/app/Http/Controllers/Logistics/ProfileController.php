<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    protected $profile_model;
    protected $common_model;
    public function __construct(){
        
        $this->profile_model = new \App\LogisticsModel\ProfileModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){

        $data = $this->profile_model->showProfileDetails();

        // dd($data);


        return view('logistics.profile.profile_view')->with("data",$data);

    }

    public function editProfileView(){

        $data = $this->profile_model->showProfileDetails();
        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();

        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];

        // dd($data);

        return view('logistics.profile.profile_edit')->with("data",$data);

    }

    public function editProfile(){

        //    dd(Request::all());
             $response = $this->profile_model->editProfile();
            //  dd($response);
             return $response;
    
    }
}
