<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Request;

class CustomerSupportController extends Controller
{
    //
    protected $support_model;
    public function __construct(){
        
        $this->support_model = new \App\LogisticsModel\SupportTicketModel();
    }

    public function index(){

        $data = array();
        // $buyer_data = $this->auth_model->showBuyers();
        // $supplier_data = $this->auth_model->showSuppliers();
        // $logistics_data = $this->auth_model->showLogistics();
        
        $data = $this->support_model->showTickets();  

        // $data["buyer_data"] = $buyer_data["data"]; 
        // $data["supplier_data"] = $supplier_data["data"]; 
        // $data["logistics_data"] = $logistics_data["data"]; 

        // dd($data);

        return view('logistics.support_ticket.support_ticket')->with("data",$data);

    }

    public function showTicketDetails($ticket_id){

        $data = array();
        $data = $this->support_model->showTicketDetails($ticket_id);  
        $data["ticket_id"] = $ticket_id;
        // dd($data);

        return view('logistics.support_ticket.ticket_detail')->with("data",$data);

    }

    public function addTicket(){

        // dd(Request::all());
        // dd($data);
        $response = $this->support_model->addTicket();
        // dd($response);
        return $response;

    }
    
    public function replyTicket(){

        // dd(Request::all());
        // dd($data);
        $response = $this->support_model->replyTicket();

        return $response;

    }
}
