<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentHistoryController extends Controller
{
    //
    protected $order_model;
    public function __construct(){
        
        $this->order_model = new \App\AdminModel\OrdersModel();
    }

    public function index(){

        $data = array();
        // $data = $this->order_model->showOrders();  

        // dd($data);

        return view('logistics.payment_history.payment_history')->with("data",$data);

    }
}
