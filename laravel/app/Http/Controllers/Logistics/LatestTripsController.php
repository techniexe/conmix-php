<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Request;

class LatestTripsController extends Controller
{
    //
    protected $trips_model;
    protected $vehicle_model;
    public function __construct(Request $request){
        
        $this->trips_model = new \App\LogisticsModel\LatestTripsModel();
        $this->vehicle_model = new \App\LogisticsModel\VehicleModel();

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        

        $data = $this->trips_model->showOrders();
        
        // dd($data);  

        return view('logistics.latest_trips.latest_trips')->with("data",$data);

    }

    public function tripDetail($order_id){

        $data = array();
        
        $vehicles = $this->vehicle_model->showVehicles();

        $data = $this->trips_model->showOrdersDetails($order_id);
        
        $data['vehicles'] = $vehicles["data"];
        

        // dd($data);

        return view('logistics.latest_trips.order_details')->with("data",$data);

    }

    public function truckAssign(){
        // dd(Request::all());
        $data = array();

        $data = $this->trips_model->truckAssign();

        return $data;

    }

    public function uploadOrderBill(){
        // dd(Request::all());
        $data = array();
        $data = $this->trips_model->uploadOrderBill();  

        return $data;
    }


    public function trackingDetails($item_id,$tracking_id){

        // dd(Request::all());

        $data = $this->trips_model->trackingDetails($item_id,$tracking_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('logistics.latest_trips.order_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }        
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }
}
