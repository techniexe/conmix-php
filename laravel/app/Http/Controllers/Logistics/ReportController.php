<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Request;

class ReportController extends Controller
{
    //
    protected $report_model;
    public function __construct(){
        
        $this->report_model = new \App\LogisticsModel\ReportModel();
    }

    public function index(){

        $data = array();
        // $data = $this->report_model->showOrders();  

        // dd($data);

        return view('logistics.report.report')->with("data",$data);

    }

    public function order_listing_report(){

        $data = array();
        $data = $this->report_model->order_listing_report();  

        // dd($data);

        return view('logistics.report.latest_trips')->with("data",$data);

    }
}
