<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Request;

class VehicleController extends Controller
{
    //
    protected $vehicle_model;
    protected $driver_model;
    protected $common_model;
    public function __construct(){
        $this->vehicle_model = new \App\LogisticsModel\VehicleModel();

        $this->driver_model = new \App\LogisticsModel\DriverModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){
        $data = array();
        $category_data = $this->vehicle_model->showVehicleCategory();
        $sub_category_data = $this->vehicle_model->showSubCategoies();

        $data = $this->vehicle_model->showVehicles();

        $data["vehicle_categoies"] = $category_data['data'];
        $data["subcategories"] = $sub_category_data['data'];
       
        // dd($data);
        return view("logistics.vehicles.vehicles")->with("data",$data);
    }

    public function addVehicleView(){

        $data = array();
        $category_data = $this->vehicle_model->showVehicleCategory();
        $sub_category_data = $this->vehicle_model->showSubCategoies();
        $driver_data = $this->driver_model->showDriverDetails();

        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();

        // $data = $this->vehicle_model->showVehicles();
        $data["vehicle_categoies"] = $category_data['data'];
        $data["subcategories"] = $sub_category_data['data'];
        $data["driver_data"] = $driver_data['data'];

        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];
        // dd($data);
        return view("logistics.vehicles.vehicle_add")->with("data",$data);

    }

    public function addVehicle(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->addVehicle();

        return $data;

    }

    public function editVehicleView($vehicle_id){

        $data = array();
        $category_data = $this->vehicle_model->showVehicleCategory();
        $sub_category_data = $this->vehicle_model->showSubCategoies();
        $driver_data = $this->driver_model->showDriverDetails();

        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();

        $data = $this->vehicle_model->getVehicleDetail($vehicle_id);

        // $data = $this->vehicle_model->showVehicles();
        $data["vehicle_categoies"] = $category_data['data'];
        $data["subcategories"] = $sub_category_data['data'];
        $data["driver_data"] = $driver_data['data'];

        

        // $data["data"] = json_decode(Request::post("vehicle_details"));

        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];
        // dd($data);
        return view("logistics.vehicles.vehicle_edit")->with("data",$data);

    }

    public function editVehicle(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->editVehicle();

        return $data;

    }

    public function getSubCatByCatId(){


        $data = $this->vehicle_model->showSubCategoies();

        return $data;
    }
}
