<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Request;

class QuoteController extends Controller
{
    //
    protected $quote_model;
    public function __construct(Request $request){
        
        $this->quote_model = new \App\LogisticsModel\QuoteModel();

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->quote_model->showQuotes();
        
        // dd($data);

        return view('logistics.quote.send_quote')->with("data",$data);

    }

    public function updateQuote(){

        // dd(Request::all());
        $data = array();
        
        $data = $this->quote_model->updateQuote();
        
        
        
        // dd($data);

        return $data;

    }
}
