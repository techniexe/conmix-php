<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    protected $dashboard_model;
    public function __construct(Request $request){
        
        $this->dashboard_model = new \App\LogisticsModel\DashboardModel();

        
    }

    public function index(){
        // echo "Dashboard";exit;
        
        $data = $this->dashboard_model->getDashboardDetails();
        $quote_list = $this->dashboard_model->getQuoteList();
        $latest_orders = $this->dashboard_model->getLatestOrders();
        // dd($latest_orders);
        $data["quotes"] = $quote_list["data"];
        $data["latest_orders"] = $latest_orders["data"];
        // if(session("custom_token",null)){
            // return redirect('dashboard');
            return view("logistics.Dashboard.dashboard")->with('data',$data);
        // }else{
        //     return view("supplier.Authentication.signup")->with('data',array());
        // }
    }

    public function getMonthlyChartDetail(){

        // dd(Request::all());
        $data = $this->dashboard_model->getMonthlyChartDetails();


        return $data;
    }
    
    public function getDailyChartDetail(){
        // dd(Request::all());
        $data = $this->dashboard_model->getDayChartDetails();


        return $data;
    }
}
