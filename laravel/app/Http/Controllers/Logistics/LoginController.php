<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    //
    protected $login_model;
    public function __construct(Request $request){
        
        $this->login_model = new \App\LogisticsModel\LoginModel();

        
    }

    public function index(){
        // echo "Dashboard";
        
        if(session("custom_token",null)){
            return redirect('logistics/dashboard');
        }else{
            return view("logistics.Authentication.login")->with('data',array());
        }
    }

    public function login(LoginRequest $request){
        
        // dd($request->all());

        $validated = $request->validated();
        
        $data = $this->login_model->login();

        // dd($data);
        if($data["status"] == 200){
            return redirect('logistics/dashboard');
        }else{
            return view("logistics.Authentication.login")->with('data',$data);
        }
    }

    public function logout(){

        $this->login_model->logout();

        return redirect('logistics');
    }
}
