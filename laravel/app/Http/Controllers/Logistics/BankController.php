<?php

namespace App\Http\Controllers\Logistics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BankController extends Controller
{
    //
    protected $bank_model;
    public function __construct(Request $request){
        
        $this->bank_model = new \App\LogisticsModel\BankModel();

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->bank_model->showBankDetails();
        
        // dd($data);

        return view('logistics.bank_details.bank_details')->with("data",$data);

    }

    public function addBankDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->bank_model->addBankDetails();
        
        // dd($data);

        return $data;

    }

    public function editBankDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->bank_model->editBankDetails();
        
        // dd($data);

        return $data;

    }
    
    public function setDefaultBaknkDefault(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->bank_model->setDefaultBaknkDefault();
        
        // dd($data);

        return $data;

    }
}
