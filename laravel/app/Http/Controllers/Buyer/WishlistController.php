<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    //
    protected $wishlist_model;
    protected $common_model;

    public function __construct(Request $request){
        
        $this->wishlist_model = new \App\BuyerModel\WishlistModel();
        $this->common_model = new \App\CommonAPIModel();

        

        
    }

    public function index(){

        $data = array();
        
        return view("buyer.views.wishlist")->with('data',$data);


    }
}
