<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    //
    protected $order_model;
    protected $common_model;

    public function __construct(Request $request){
        
        $this->order_model = new \App\BuyerModel\OrderModel();
        $this->common_model = new \App\CommonAPIModel();       

        
    }

    public function index(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.contact_us")->with('data',$data);

    }

    public function aboutUs(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.about_us")->with('data',$data);

    }
    
    public function shippingPolicy(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.shipping_policy")->with('data',$data);

    }
    
    public function privacyPolicy(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.privacy_policy")->with('data',$data);

    }
    
    public function termsOfUse(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.terms_of_use")->with('data',$data);

    }
    
    public function returnPolicy(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.return_policy")->with('data',$data);

    }
    
    public function faq(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.faq")->with('data',$data);

    }

    public function supplierPrivacyPolicy(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.supplier_privacy_policy")->with('data',$data);

    }
    
    public function supplierTermsOfUse(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.supplier_terms_of_use")->with('data',$data);

    }
    
    public function supplierReturnPolicy(){

        $data = array();


        // $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.supplier_return_policy")->with('data',$data);

    }
}
