<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;
use Auth;
use URL;

class LoginController extends Controller
{
    //
    protected $login_model;
    public function __construct(Request $request){
        
        $this->login_model = new \App\BuyerModel\LoginModel();

        
    }

    public function login(){
        
        // if(session("buyer_custom_token",null)){
        //     return redirect('home');
        // }else{
            // dd(Request::all());

            // $validated = $request->validated();
            
            $data = $this->login_model->login();

            // dd($data);
            
            return $data;
        // }
    }

    public function afterLogin(){
    
        // Auth::logout(); // logout user
        return redirect(\URL::previous());

    }

    public function logout(){

        $this->login_model->logout();

        // return redirect('home');

        Auth::logout(); // logout user
        return redirect(\URL::previous());
        // return redirect('home');
    }

    public function login_mobile(){
        
        // dd(Request::all());

        // $validated = $request->validated();
        
        $data = $this->login_model->login_mobile();

        // dd($data);
        
        return $data;
    }
}
