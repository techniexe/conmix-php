<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;

class ProfileController extends Controller
{
    //
    protected $profile_model;
    protected $common_model;
    public function __construct(){
        
        $this->profile_model = new \App\BuyerModel\ProfileModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){

        $data = $this->profile_model->showProfileDetails();
        
        $country_data = $this->common_model->getCountries();

        $data["country_data"] = $country_data["data"];

        // dd($data);

        return view('buyer.views.my_account.profile_view')->with("data",$data);

    }

    public function editProfile(){

    //    dd(Request::all());
            $response = $this->profile_model->editProfile();
        //  dd($response);

            if($response["status"] == 200){
                $admin_profile = $this->profile_model->showProfileDetails();
                session()->put("buyer_profile_details",$admin_profile["data"]);
            }

            return $response;
    
    }
    
    public function changePass(){

    //    dd(Request::all());
            $response = $this->profile_model->changePass();
        //  dd($response);

            

            return $response;
    
    }

    public function getSites(){

        // dd(Request::all());
        $data = $this->profile_model->showSites();

        // dd($data);
        return view('buyer.views.my_account.sites')->with("data",$data);
        
    }
    
    public function getPassword(){

        // dd(Request::all());
        // $data = $this->profile_model->showSites();

        // dd($data);
        $data = array();
        return view('buyer.views.my_account.password')->with("data",$data);
        
    }
    
    public function addSites(){

        // dd(Request::all());

        $data = $this->profile_model->addSites();

        return $data;   
    }

    public function checkSiteExist(){

        // dd(Request::all());
        $data = $this->profile_model->checkSiteExist();

        // dd($data);
        // return view('buyer.views.my_account.sites')->with("data",$data);

        return $data;
        
    }
    
    public function editSites(){

        // dd(Request::all());

        $data = $this->profile_model->editSites();

        return $data;
    }
    
    public function deleteSites(){

        // dd(Request::all());

        $data = $this->profile_model->deleteSites();

        return $data;
    }

    public function requestOTP(){
    
        $data = $this->profile_model->requestOTP();

        return $data;

    }

    public function requestOTPForEditProfile(){
        // dd(Request::all());
        $data = $this->profile_model->requestOTPForEditProfile();
        // dd($data);
        return $data;

    }
    
    


    public function getStateByCountry(){

        $country_id = Request::get('country_id');

        $country_data = $this->common_model->getStateByCountryID($country_id);

        // dd($country_data);

        return $country_data;

    }
    
    public function getCityByState(){

        $state_id = Request::get('state_id');

        $state_data = $this->common_model->getCityBystateID($state_id);

        // dd($state_data);

        return $state_data;

    }

    public function gettNotifications(){

        //    dd(Request::all());
             $response = $this->common_model->gettNotifications();
    
            //  dd($response);
    
             if($response["status"] == 200){
    
                return view('buyer.views.notifications')->with("data",$response);
    
    
                // $returnHTML = view('buyer.common.noti_list_ajax')->with('data', $response)->render();
                // $returnHTMLslide = "";
                  
              
                
    
                // return response()->json(array('status' => $response["status"], 'html'=>$returnHTML));
            }else{
                // return $data;
    
                // return response()->json(array('status' => $data["status"], 'html'=>""));
    
                
                return $response;
            }
    
            //  dd($response);
             return $response;
    
        }



        public function showBillngAddress(){

            // dd(Request::all());
            $state_data = $this->common_model->getStates();
            $city_data = $this->common_model->getCities();
            
            $data = $this->profile_model->showBillngAddress();
            $data["state_data"] = $state_data["data"];
            $data["city_data"] = $city_data["data"];
    
            // dd($da   ta);
            return view('buyer.views.my_account.billng_address')->with("data",$data);
            
        }

        public function addBillngAddress(){

            // dd(Request::all());
    
            $data = $this->profile_model->addBillngAddress();
    
            return $data;   
        }

        public function editBillngAddress(){

            // dd(Request::all());
    
            $data = $this->profile_model->editBillngAddress();
    
            return $data;
        }


        public function deleteBilling(){

            // dd(Request::all());
    
            $data = $this->profile_model->deleteBilling();
    
            return $data;
        }
        
        public function showCustomRMC(){

            // dd(Request::all());
            $state_data = $this->common_model->getStates();
            $city_data = $this->common_model->getCities();
            
            $data = $this->profile_model->showCustomRMC();
            $data["state_data"] = $state_data["data"];
            $data["city_data"] = $city_data["data"];
    
            // dd($data);
            return view('buyer.views.my_account.custom_rmc')->with("data",$data);
            
        }

        public function addCustomRMC(){

            // dd(Request::all());
    
            $data = $this->profile_model->addCustomRMC();
    
            return $data;   
        }

        public function editCustomRMC(){

            // dd(Request::all());
    
            $data = $this->profile_model->editCustomRMC();
    
            return $data;
        }


        public function deleteCustomRMC(){

            // dd(Request::all());
    
            $data = $this->profile_model->deleteCustomRMC();
    
            return $data;
        }

        public function deleteAccount(){
    
            // dd($data);
    
            return view('buyer.views.delete_account')->with("data",array());
    
        }
    
}
