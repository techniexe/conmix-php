<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use App\BuyerModel\BuyerOrdersExport;
use Request;
use Excel;

class OrderController extends Controller
{
    //
    protected $order_model;
    protected $common_model;

    public function __construct(Request $request){
        
        $this->order_model = new \App\BuyerModel\OrderModel();
        $this->common_model = new \App\CommonAPIModel();

        

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();


        $data = $this->order_model->showOrders();
        // dd($data);
        return view("buyer.views.order")->with('data',$data);

    }

    public function orderDetails($order_id){

        $data = array();

        $data = $this->order_model->showOrderDetails($order_id);
        // dd($data);
        return view("buyer.views.order_details")->with('data',$data);
    }

    public function cancelOrder(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->cancelOrder();

        return $data;
    }
    
    public function cancelOrderItem(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->cancelOrderItem();

        return $data;
    }

    public function addProductReview(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->addProductReview();

        return $data;

    }

    public function orderComplain(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->orderComplain();
        // dd($data);
        return $data;

    }
    
    public function orderFeedback(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->orderFeedback();
        // dd($data);
        return $data;

    }
    
    public function assignQtyOrder(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->assignQtyOrder();
        // dd($data);
        return $data;

    }
    
    public function reassignStatusChange(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->reassignStatusChange();
        // dd($data);
        return $data;

    }
    
    public function accept7And28DaysReport(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->accept7And28DaysReport();
        // dd($data);
        return $data;

    }
    
    public function reject7And28DaysReport(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->reject7And28DaysReport();
        // dd($data);
        return $data;

    }
    
    public function checkTMAvailability(){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->checkTMAvailability();

        return $data;

    }

    public function trackOrderItem($item_id){

        // dd(Request::all());

        $data = array();

        $data = $this->order_model->trackOrderItem($item_id);
        // dd($data);
        
        if($data["status"] == 200){

            $returnHTML = view('buyer.views.order_item_truck_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }

        
        

        return $data;
        // return view("buyer.views.order_item_track")->with('data',$data);

    }

    public function trackingDetails($item_id,$tracking_id){

        // dd(Request::all());

        $data = $this->order_model->trackingDetails($item_id,$tracking_id);
        // dd($data);

        return view("buyer.views.order_item_track")->with('data',$data);
    }
    
    public function CPtrackingDetails($item_id){

        // dd($item_id);

        $data = $this->order_model->CPtrackingDetails($item_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('buyer.views.order_cp_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }

        
        

        return $data;


        // return view("buyer.views.order_item_track")->with('data',$data);
    }

    public function buyerOrderExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->order_model->showOrders();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    $value["display_id"],
                    date('d M Y',strtotime($value["created_at"])),
                    $value["selling_price_With_Margin"],
                    $value["gst_price"],
                    (isset($value["total_amount"]) ? $value["total_amount"] : ''),
                    (isset($value["order_status"]) ? $value["order_status"] : ''),
                    
                    
                ];
                $i++;
            }

            $export = new BuyerOrdersExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'buyer_orders'.$file_type);
        }
    }
}
