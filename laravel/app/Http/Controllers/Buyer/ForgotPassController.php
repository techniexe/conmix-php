<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;

class ForgotPassController extends Controller
{
    //
    protected $forgot_pass_model;
    public function __construct(Request $request){
        
        $this->forgot_pass_model = new \App\BuyerModel\ForgotPassModel();

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        // $data = $this->bank_model->showBankDetails();
        
        // dd($data);

        return view('logistics.forgot_pass.forgot_pass_email_verifier')->with("data",$data);

    }

    public function forgotPassOTPSend(){
        // dd(Request::all());
        $data = $this->forgot_pass_model->forgotPassOTPSend();
        // dd($data);
        // if($data["status"] == 200){
        //     // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
        //     return view("logistics.forgot_pass.forgot_pass_email_verifier_otp")->with('data',$data);
        // }else{
        //     return view("logistics.forgot_pass.forgot_pass_email_verifier")->with('data',$data);
        // }

        return $data;

    }

    public function forgotPassOTPVerify(){

        $data = $this->forgot_pass_model->forgotPassOTPVerify();
        // dd($data);
        // if($data["status"] == 200){
        //     $data["message"] = "OTP verified successfully";
        //     // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
        //     // return view("logistics.forgot_pass.forgot_pass")->with('data',$data);
        //     return redirect()->route('supplier_dashboard');
        //     // redirect('supplier_dashboard');
        // }else{
        //     return view("logistics.forgot_pass.forgot_pass_email_verifier_otp")->with('data',$data);
        // }

        return $data;

    }

    public function forgotPass(){

        $data = $this->forgot_pass_model->forgotPass();
        // dd($data);
        // if($data["status"] == 200){
        //     $data["message"] = "Password updated successfully. Please login";
        //     // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
        //     return view("logistics.Authentication.login")->with('data',$data);
        // }else{
        //     return view("logistics.forgot_pass.forgot_pass")->with('data',$data);
        // }

        return $data;

    }
}
