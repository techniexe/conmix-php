<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    //
    protected $cart_model;
    protected $profile_model;
    protected $common_model;
    public function __construct(Request $request){
        
        $this->cart_model = new \App\BuyerModel\CartModel();
        $this->profile_model = new \App\BuyerModel\ProfileModel();
        $this->common_model = new \App\CommonAPIModel();

    }

    public function index(){
        
        $data = array();

        $site_details = $this->profile_model->showSites();
        $country_data = $this->common_model->getCountries();
        $coupon_data = $this->cart_model->getCoupons();

        $data["country_data"] = $country_data["data"];
        $data["site_details"] = $site_details["data"];
        $data["coupon_data"] = $coupon_data["data"];
        // dd($data);
        return view("buyer.views.cart")->with('data',$data);

    }

    public function addSiteIdToCart(){

        // dd(Request::all());

        $lat = Request::post("site_lat");
        $long = Request::post("site_long");
        $state_id = Request::post("state_id");
        $state_name = Request::post("state_name");
        $city_name = Request::post("city_name");
           

        if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null)){

            if((Session::get("selected_state_name") != $state_name) && (Session::get("selected_city_name") != $city_name)){

                // Session::put("selected_lat",$lat);
                // Session::put("selected_long",$long);
                // Session::put("selected_state_id",$state_id);
                // Session::put("selected_state_name",$state_name);
                // Session::put("selected_city_name",$city_name);

                $data["redirect_to_listing"] = "yes";
                $data["status"] = 301;
                return $data;

            }else{
                $data = $this->cart_model->addSiteIdToCart();

                return $data;
            }

        }else{
            $data = $this->cart_model->addSiteIdToCart();

            return $data;
        }       


        

    }

    public function changeSiteDetails(){

        // dd(Request::all());

        $lat = Request::post("site_lat");
        $long = Request::post("site_long");
        $state_id = Request::post("state_id");
        $state_name = Request::post("state_name");
        $city_name = Request::post("city_name");
        $site_id = Request::post("site_id");

        if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null)){

            if((Session::get("selected_state_name") != $state_name) && (Session::get("selected_city_name") != $city_name)){

                Session::put("selected_lat",$lat);
                Session::put("selected_long",$long);
                Session::put("selected_state_id",$state_id);
                Session::put("selected_state_name",$state_name);
                Session::put("selected_city_name",$city_name);

                Session::put("is_site_selected","1");
                Session::put("selected_site_address",$site_id);

                // $data["redirect_to_listing"] = "yes";
                // $data["status"] = 301;
                // return $data;

                $data = $this->cart_model->addSiteIdToCart();

                return $data;

            }

        }

    }

    public function addBiilingIdToCart(){

        // dd(Request::all());
            

       
        $data = $this->cart_model->addBiilingIdToCart();

        return $data;
            


        

    }

    public function placeOrder(){

        // dd(Request::all());

        $data = $this->cart_model->placeOrder();
        
        // dd($data);
        if($data["status"] == 200){
            Session::put("selected_display_delivery_date","");
        }

        return $data;

    }

    public function deleteCartItem(){

        // dd(Request::all());

        $data = $this->cart_model->deleteCartItem();

        if($data["status"] == 200){

            $final_data = array();
            $cart_details = $this->common_model->getCartDetailsById(Session::get('my_cart_id'));

            // $final_data["cart_details"] = $cart_details;

            if(Request::post("cart_place") == "slide_cart"){
                $returnHTML = view('buyer.views.slide_cart_view')->with('cart_details', $cart_details)->render();
                $returnHTMLslide = "";
            }else if(Request::post("cart_place") == "cart_checkout"){

                $site_details = $this->profile_model->showSites();
                $coupon_data = $this->cart_model->getCoupons();
                $final_data = array();
                $final_data["site_details"] =  $site_details["data"];
                $final_data["coupon_data"] =  $coupon_data["data"];
                // dd($site_details);
                $returnHTMLslide = view('buyer.views.slide_cart_view')->with('cart_details', $cart_details)->render();
                $returnHTML = view('buyer.views.cart_checkout_view')->with(['cart_details'=> $cart_details,"data"=>$final_data])->render();
            }
            

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }else{
            // return $data;

            return response()->json(array('status' => $data["status"], 'html'=>""));
        }


        

    }

    public function apply_coupon(){

        // dd(Request::all());

        $data = $this->cart_model->apply_coupon();
        // dd($data);
        if($data["status"] == 200){

            $final_data = array();
            $cart_details = $this->common_model->getCartDetailsById(Session::get('my_cart_id'));

            // $final_data["cart_details"] = $cart_details;


                $site_details = $this->profile_model->showSites();
                $coupon_data = $this->cart_model->getCoupons();
                $final_data = array();
                $final_data["site_details"] =  $site_details["data"];
                $final_data["coupon_data"] =  $coupon_data["data"];
                // dd($site_details);
                $returnHTMLslide = view('buyer.views.slide_cart_view')->with('cart_details', $cart_details)->render();
                $returnHTML = view('buyer.views.cart_checkout_view')->with(['cart_details'=> $cart_details,"data"=>$final_data])->render();
           
            

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }

        return $data;
    }
}
