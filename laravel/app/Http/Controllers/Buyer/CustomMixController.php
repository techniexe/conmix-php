<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;
use Session;
use Cookie;

class CustomMixController extends Controller
{
    //
    protected $custom_mix_model;
    protected $common_model;
    protected $product_cat;
    protected $profile_model;
    public function __construct(Request $request){
        
        $this->custom_mix_model = new \App\BuyerModel\CustomMixModel();
        $this->common_model = new \App\CommonAPIModel();
        $this->profile_model = new \App\BuyerModel\ProfileModel();

        $this->product_cat = $this->common_model->getProductCategory();

        
    }

    public function index(){
        // echo "Home";exit;
        // dd($product_id);
        // dd(session()->get('data'));
        // dd(session('my_cart_id', null));

        // if(!empty($subcat_id)){
        //     dd($subcat_id);
        // }

        // dd(Request::all());

        $method = Request::method();

        // dd($method);
        
        if($method == "POST"){
            $custom_mix_design = Request::all();
            // dd($custom_mix_design);
            

            if(isset($custom_mix_design["fly_ash_quantity"])){

            }else{
                unset($custom_mix_design["fly_ash_quantity"]);
            }
            
            if(isset($custom_mix_design["sand_quantity"])){

            }else{
                unset($custom_mix_design["sand_quantity"]);
            }
            
            if(isset($custom_mix_design["admix_brand_id"][0])){

            }else{
                unset($custom_mix_design["admix_brand_id"]);
            }
            
            if(isset($custom_mix_design["admix_code_id"][0])){

            }else{
                unset($custom_mix_design["admix_code_id"]);
            }
            
            if(isset($custom_mix_design["admix_brand_id_2"][0])){

            }else{
                unset($custom_mix_design["admix_brand_id_2"]);
            }
            
            if(isset($custom_mix_design["admix_code_id_2"][0])){

            }else{
                unset($custom_mix_design["admix_code_id_2"]);
            }
            
            if(isset($custom_mix_design["admix_quantity_per"])){

            }else{
                unset($custom_mix_design["admix_quantity_per"]);
            }
            
            if(isset($custom_mix_design["admix_quantity"])){

            }else{
                unset($custom_mix_design["admix_quantity"]);
            
            }
            
            if(isset($custom_mix_design["fly_ash_source_id"][0])){

            }else{
                unset($custom_mix_design["fly_ash_source_id"]);
            }
            
            if(isset($custom_mix_design["fly_ash_quantity"])){

            }else{
                unset($custom_mix_design["fly_ash_quantity"]);
            }
            
            Session::put("custom_mix_design",$custom_mix_design);
            
        }        
        // dd(session()->get('custom_mix_design',null));
        $session_custom_mix_design = Session::get("custom_mix_design");
        // echo "tets...". Session::get("selected_long");
        // echo "tets...". Session::get("concrete_grade_id");
        // echo "tets...". Session::get("selected_test");
        // dd(Session::all());
        // dd($session_custom_mix_design);
        if(isset($session_custom_mix_design)){

            // dd(Session::get("selected_lat"));
            

            if(Session::get("selected_lat") != null){
                $session_custom_mix_design["lat"] = Session::get("selected_lat");
            }

            if(Session::get("selected_long") != null){
                $session_custom_mix_design["long"] = Session::get("selected_long");
            }   

            if(Session::get("selected_state_id") != null){
                $session_custom_mix_design["state_id"] = Session::get("selected_state_id");
            }
            
            if(Session::get("selected_delivery_date") != null){
                $session_custom_mix_design["delivery_date"] = Session::get("selected_delivery_date");
            }

            if(Session::get("selected_end_delivery_date") != null){
                $session_custom_mix_design["end_date"] = Session::get("selected_end_delivery_date");
            }

            if($session_custom_mix_design["is_custom_mix_open_from_save"] == 1){

                $session_custom_mix_design["lat"] = Session::get("custom_mix_selected_lat");
                $session_custom_mix_design["long"] = Session::get("custom_mix_selected_long");
                $session_custom_mix_design["state_id"] = Session::get("custom_mix_selected_state_id");

            }

            if(isset($session_custom_mix_design["admix_code_id"])){

                $session_custom_mix_design["admix_category_id"] = $session_custom_mix_design["admix_code_id"];
            }
            
            if(isset($session_custom_mix_design["admix_code_id_2"])){

                $session_custom_mix_design["admix_category_id_2"] = $session_custom_mix_design["admix_code_id_2"];
            }

            if($session_custom_mix_design["with_TM"]){
                $session_custom_mix_design["with_TM"] = $session_custom_mix_design["with_TM"] == "yes" ? 'true' : 'false';
            }

            if(isset($session_custom_mix_design["with_CP"])){

                $session_custom_mix_design["with_CP"] = $session_custom_mix_design["with_CP"] == "yes" ? 'true' : 'false';

            }

            // $cookie = Cookie::make('name', 'value', 60);
            // Cookie::queue('name', 'value', 60);

            

            // dd($session_custom_mix_design);

        }
        
        
        
        // dd(Request::post("is_custom_mix_save"));
        // echo json_encode($session_custom_mix_design);
        // dd($session_custom_mix_design);
        
        // $getCart = $this->custom_mix_model->getCart();
        if($method == "GET"){

            $data = $this->custom_mix_model->showCustomDesign($session_custom_mix_design);
                
            // dd($data);

            if(($method == "POST") && (Request::post("is_ajax"))){
                return $data;
            }else{
                // $final_data = array();
                return view("buyer.views.custom_mix_listing")->with('data',$data);
            }

        }else{
            if(Request::post("is_custom_mix_save") == 0){
                // dd($session_custom_mix_design);
                $data = $this->custom_mix_model->showCustomDesign($session_custom_mix_design);
                
                // dd($data);

                if(($method == "POST") && (Request::post("is_ajax"))){
                    return $data;
                }else{
                    // $final_data = array();
                    return view("buyer.views.custom_mix_listing")->with('data',$data);
                }
                
                // $source_data = $this->custom_mix_model->getProductSources();
                // dd($source_data);

                // $data["source_data"] = $source_data["data"];

                // $sub_category = $this->common_model->getProductSubCategory($product_id);
                // $state_data = $this->common_model->getStates();
                // $city_data = $this->common_model->getCities();

                // $data["state_data"] = $state_data["data"];
                // $data["city_data"] = $city_data["data"];
                // $data["product_cat"] = $this->product_cat["data"];
                // $data["sub_categories"] = $sub_category["data"];
                // $data["category_id"] = $product_id;
                // $data["subcategory_id"] = $subcat_id;        
            }else if((Request::post("is_custom_mix_save") == 1) && (Request::post("is_ajax"))){
                if(Request::post("is_custom_mix_add_edit") == 1){
                    //Add
                    // dd($session_custom_mix_design);
                    $data = $this->custom_mix_model->addCustomRMC($session_custom_mix_design);

                }else if(Request::post("is_custom_mix_add_edit") == 2){
                    //Edit
                    // dd($session_custom_mix_design);
                    $data = $this->custom_mix_model->editCustomRMC($session_custom_mix_design);
    
                }

                return $data;

            }
        }
        
        
        
    }


    function showCustomDetail($vendor_id, $adress_id){

        // echo "Home";exit;
        // dd(Request::all());
        // $data = $this->product_detail_model->showProducts();
        // dd(session('my_cart_id', null));
        // dd($product_id);
        $data = array();
        // $data = Request::post("product_detail");
        
        // $data = json_decode($data);
        $session_custom_mix_design = Session::get("custom_mix_design");
        if(Request::get("with_TM")){

            $session_custom_mix_design["with_TM"] = Request::get("with_TM");

        }
        
        if(Request::get("with_CP")){

            $session_custom_mix_design["with_CP"] = Request::get("with_CP");

        }

        Session::put("custom_mix_design",$session_custom_mix_design);

        $session_custom_mix_design = Session::get("custom_mix_design");
        
// dd($session_custom_mix_design);
        if(isset($session_custom_mix_design)){

            // dd(Session::get("selected_lat"));
            

            if(Session::get("selected_lat") != null){
                $session_custom_mix_design["lat"] = Session::get("selected_lat");
            }

            if(Session::get("selected_long") != null){
                $session_custom_mix_design["long"] = Session::get("selected_long");
            }   

            if(Session::get("selected_state_id") != null){
                $session_custom_mix_design["state_id"] = Session::get("selected_state_id");
            }

            if(Request::post("delivery_date")){

                Session::put("selected_delivery_date",Request::post("delivery_date"));                

            }
            
            if(Session::get("selected_delivery_date") != null){
                $session_custom_mix_design["delivery_date"] = Session::get("selected_delivery_date");
            }            
            
            
            if(isset($adress_id)){
                $session_custom_mix_design["address_id"] = $adress_id;
            }

            if(isset($session_custom_mix_design["admix_code_id"])){
                $session_custom_mix_design["admix_category_id"] = $session_custom_mix_design["admix_code_id"];
            }

            if($session_custom_mix_design["with_TM"]){
                if($session_custom_mix_design["with_TM"] == "yes"){
                    $session_custom_mix_design["with_TM"] = 'true';
                    
                }else{

                    if($session_custom_mix_design["with_TM"] == "true"){
                        $session_custom_mix_design["with_TM"] = 'true';
                        
                    }else{

                        $session_custom_mix_design["with_TM"] = 'false';
                    }
                }
            }

            if($session_custom_mix_design["with_CP"]){

                if($session_custom_mix_design["with_CP"]  == "yes"){
                    $session_custom_mix_design["with_CP"] = 'true';
                    
                }else{
                    if($session_custom_mix_design["with_CP"] == "true"){
                        $session_custom_mix_design["with_CP"] = 'true';
                    }else{
                        $session_custom_mix_design["with_CP"] = 'false';
                    }
                    
                }

            }
            // dd($session_custom_mix_design);
            // $cookie = Cookie::make('name', 'value', 60);
            // Cookie::queue('name', 'value', 60);

            
            if(isset($session_custom_mix_design["with_TM"]) && ($session_custom_mix_design["with_TM"] == 'false')){
                $buyer_TM_details = array();

                if(Request::post("TM_no")){

                    $buyer_TM_details["TM_no"] = Request::post("TM_no");           

                }
                
                if(Request::post("driver_name")){

                    $buyer_TM_details["driver_name"] = Request::post("driver_name");           

                }
                
                if(Request::post("driver_mobile")){

                    $buyer_TM_details["driver_mobile"] = Request::post("driver_mobile");           

                }
                
                if(Request::post("TM_operator_name")){

                    $buyer_TM_details["TM_operator_name"] = Request::post("TM_operator_name");           

                }
                
                if(Request::post("TM_operator_mobile_no")){

                    $buyer_TM_details["TM_operator_mobile_no"] = Request::post("TM_operator_mobile_no");           

                }
                
                if(count($buyer_TM_details) > 0){

                    Session::put("buyer_TM_details",$buyer_TM_details);    

                }
            }

            // dd($session_custom_mix_design);

        }
        // dd($session_custom_mix_design);
        $data = $this->custom_mix_model->showCustomDetail($vendor_id,$session_custom_mix_design);

        // dd($data);

        // $product_review = $this->product_model->getProductReview($product_id);

        // dd($product_review);
        $final_data = array();
        // $final_data["product_cat"] = $this->product_cat["data"];
        // $final_data["product_review"] = $product_review["data"];
        $final_data["data"] = $data["data"]["response"];
        $final_data["suggestion"] = $data["data"]["suggestion"];
        // array_push($final_data,$data);
        // $final_data["data"] = $data;
        
        // dd($final_data);
        
        // if(isset($data["error"])){
        //     return view("buyer.views.home")->with('data',$data);
        // }else{
            if(Request::post("delivery_date")){
                return $data;
            }else{
                return view("buyer.views.custom_mix_details")->with('data',$final_data);
            }
        // }
        

    }

    function getAggregateSandSubCatForCustomMix($sub_cat_id){

        $data = $this->common_model->getAggregateSandSubCategoryForCustomMix();


        return $data;

    }
}
