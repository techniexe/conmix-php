<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    //
    protected $common_model;
    public function __construct()
    {
        $this->common_model = new \App\CommonAPIModel();
        //its just a dummy data object.
        $product_cat = $this->common_model->getProductCategory();
        // dd($product_cat);
        // Sharing is caring
        View::share('product_cat', $product_cat);
    }
}
