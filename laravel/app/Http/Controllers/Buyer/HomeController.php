<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Request;

class HomeController extends Controller
{
    //
    protected $home_model;
    protected $common_model;
    protected $product_cat;
    protected $cart_details;
    public function __construct(Request $request){

        $this->home_model = new \App\BuyerModel\HomeModel();
        $this->common_model = new \App\CommonAPIModel();

    }

    public function index(Request $request){

        $conmix_cookie = Request::cookie('conmix_cookie');
        $from_landing = Request::get('from_landing');
        // dd(Request::all());
        if($conmix_cookie != null || $from_landing != null){
            // dd($value);
            
            $data = array();
            $state_data = $this->common_model->getStates();
            $city_data = $this->common_model->getCities();
            $concrete_grade = $this->common_model->getConcreteGrade();

            // $dd = '{"data":{"customToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3R5cGUiOiJidXllciIsImZ1bGxfbmFtZSI6InRlc3QiLCJ1aWQiOiI2MTNiMDM1ZWY3Y2YyYjViYTJlMDRkMzUiLCJpYXQiOjE2MzEyNTc0NTksImV4cCI6MTYzMzg0OTQ1OSwiaXNzIjoidGVjaG5pZXhlIn0.r-v3AgRsMbdT4dZPNwQKnGZLdbf-Xp_LaOs1mZ7WH-A"}}';
            // dump($dd);
            // $dd = json_decode($dd);
            // dd($dd->data->customToken);


            $data["state_data"] = $state_data["data"];
            $data["city_data"] = $city_data["data"];
            $data["concrete_grade"] = $concrete_grade["data"];
            // $data["product_cat"] = $this->product_cat["data"];
            // dd($data);
            // if(session("custom_token",null)){
                // return redirect('dashboard');
                // return view("buyer.views.home")->with('data',$data)->withCookie(cookie('my_redirect', "test", '15'));

                $response = new \Illuminate\Http\Response(view('buyer.views.home')->with('data',$data));
                $response->withCookie(cookie('conmix_cookie', "Visited", time()+31556926));
                return $response;

                // return view("buyer.views.comming_soon")->with('data',$data);
            // }else{
            //     return view("supplier.Authentication.signup")->with('data',array());
            // }
        }else{

            return view("buyer.views.landing.landing")->with('data',array());

        }
    }

    
    public function landingHome(){

        // return redirect(route('buyer_home'))->with('from_landing','yes')->withCookie(cookie('conmix_cookie', "Visited", time()+31556926));

        // return redirect()->route('buyer_home',['key'=> "test"]);
        // return redirect('home?from_landing=yes');

    }
    
    public function getStates(){

        $data = array();

        $data = $this->common_model->getStates();

        // dd($data);

        return $data["data"];

    }

    public function getCities(){

        $data = array();

        $data = $this->common_model->getCities();

        // dd($data);

        return $data["data"];

    }

    public function getCountries(){

        $data = array();

        $data = $this->common_model->getCountries();

        // dd($data);

        return $data["data"];

    }

    public function notification(){

        return view("buyer.views.notifications")->with('data',array());

    }
}
