<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\Session;
use Cookie;

class ProductController extends Controller
{
    //
    protected $product_model;
    protected $common_model;
    protected $product_cat;
    protected $profile_model;
    protected $cart_model;
    public function __construct(Request $request){
        
        $this->product_model = new \App\BuyerModel\ProductModel();
        $this->common_model = new \App\CommonAPIModel();
        $this->profile_model = new \App\BuyerModel\ProfileModel();
        $this->cart_model = new \App\BuyerModel\CartModel();

        $this->product_cat = $this->common_model->getProductCategory();

        
    }

    public function index($product_id,$subcat_id = '',$isAjax = '', $source_name = '', $short_by_rating = '', $short_by_stock = ''){
        // echo "Home";exit;
        // dd($product_id);
        // dd(session()->get('data'));
        // dd(session('my_cart_id', null));

        // if(!empty($subcat_id)){
        //     dd($subcat_id);
        // }


        $request_data = session()->get('data',null);
        
        if(isset($request_data)){

        }else{
            $request_data = array();

            if(Session::get("selected_lat") != null){
                $request_data["lat"] = Session::get("selected_lat");
            }

            if(Session::get("selected_long") != null){
                $request_data["long"] = Session::get("selected_long");
            }

            if(Session::get("selected_state_id") != null){
                $request_data["state_id"] = Session::get("selected_state_id");
            }
            
            if(Session::get("selected_delivery_date") != null){
                $request_data["selected_delivery_date"] = date('Y-m-d', strtotime(Session::get("selected_delivery_date")));
                
            }

            if(Session::get("selected_end_delivery_date") != null){
                $request_data["end_date"] = Session::get("selected_end_delivery_date");
            }
            
            if(Session::get("selected_delivery_qty") != null){
                $request_data["quantity"] = Session::get("selected_delivery_qty");
            }

            // $cookie = Cookie::make('name', 'value', 60);
            // Cookie::queue('name', 'value', 60);

            
        }
        
        if(isset($request_data["lat"]) && isset($request_data["long"]) ){

            // dd($request_data);
            // $getCart = $this->product_model->getCart();
            $data = $this->product_model->showProducts($product_id,$subcat_id,$request_data, $source_name, $short_by_rating, $short_by_stock);
            
            // dd($data);
            
            // $source_data = $this->product_model->getProductSources();
            // dd($source_data);

            // $data["source_data"] = $source_data["data"];

            // $sub_category = $this->common_model->getProductSubCategory($product_id);
            // $state_data = $this->common_model->getStates();
            // $city_data = $this->common_model->getCities();

            // $data["state_data"] = $state_data["data"];
            // $data["city_data"] = $city_data["data"];
            // $data["product_cat"] = $this->product_cat["data"];
            // $data["sub_categories"] = $sub_category["data"];
            // $data["category_id"] = $product_id;
            // $data["subcategory_id"] = $subcat_id;        
            $data["grade_id"] = $product_id;        


            // dd($data);
            
            // if(isset($data["error"])){
            //     return view("buyer.views.home")->with('data',$data);
            // }else{
                if(!empty($isAjax)){
                    $returnHTML = view('buyer.views.product_list_ajax')->with('data', $data)->render();
                    $returnHTMLslide = "";

                    return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
                }else{
                    return view("buyer.views.product_listing")->with('data',$data);
                }   
                
            // }
        }else{

            return redirect()->route('buyer_home');

        }
        
        
    }

    public function productListing(){

        // dd(Request::all());
        // dump(Request::post("selected_end_delivery_date"));
        // dd(date('Y-m-d',strtotime(Request::post("selected_end_delivery_date"))));
        $data = array();
        Session::put("selected_testing","testing");
        if(Request::post("is_site_selected") && (Request::post("is_site_selected") == 1)){
            Session::put("selected_lat",Request::post("lat"));
            Session::put("selected_long",Request::post("long"));
            Session::put("selected_state_id",Request::post("state_id"));
            Session::put("selected_state_name",Request::post("state_name"));
            Session::put("selected_city_name",Request::post("city_name"));
            Session::put("selected_delivery_date", date('Y-m-d',strtotime(Request::post("selected_delivery_date"))));
            Session::put("selected_display_delivery_date",Request::post("selected_delivery_date"));
            
            Session::put("selected_end_delivery_date", date('Y-m-d',strtotime(Request::post("selected_end_delivery_date"))));
            Session::put("selected_end_display_delivery_date",Request::post("selected_end_delivery_date"));
            
            Session::put("selected_delivery_qty",Request::post("delivery_qty"));
        }
        Session::put("is_site_selected",Request::post("is_site_selected"));
        Session::put("selected_site_address",Request::post("selected_site_address"));

        if(Request::post("selected_delivery_date")){
            Session::put("selected_delivery_date",date('Y-m-d',strtotime(Request::post("selected_delivery_date"))));
            Session::put("selected_display_delivery_date",Request::post("selected_delivery_date"));

            Session::put("selected_end_delivery_date", date('Y-m-d',strtotime(Request::post("selected_end_delivery_date"))));
            Session::put("selected_end_display_delivery_date",Request::post("selected_end_delivery_date"));

            if(Request::post("delivery_qty")){
                Session::put("selected_delivery_qty",Request::post("delivery_qty"));
            }

            if(Request::post("state_id")){
                Session::put("selected_state_id",Request::post("state_id"));
            }
            
            if(Request::post("city_id")){
                Session::put("selected_city_id",Request::post("city_id"));
            }
            
            if(Request::post("lat")){
                Session::put("selected_lat",Request::post("lat"));
            }
            
            if(Request::post("long")){
                Session::put("selected_long",Request::post("long"));
            }
        }

        if(Request::post("type") == 'product_listing'){

            $id = Request::post("product_cat_id");

            $data["product_cat_id"] = Request::post("product_cat_id");
            // $data["state_id"] = Request::post("state_id");
            $data["state_id"] = Session::get("selected_state_id");
            // $data["city_id"] = Request::post("city_id");
            $data["city_id"] = Session::get("selected_city_id");
            // $data["lat"] = Request::post("lat");
            $data["lat"] = Session::get("selected_lat");
            // $data["long"] = Request::post("long");         
            $data["long"] = Session::get("selected_long");  

            $data["selected_delivery_date"] = date('Y-m-d',strtotime(Session::get("selected_delivery_date")));         
            $data["selected_display_delivery_date"] = Session::get("selected_delivery_date");         
            
            $data["end_date"] = Session::get("selected_end_delivery_date");         
            $data["quantity"] = Session::get("selected_delivery_qty");         
        
            Session::put("selected_type_of_rmc",Request::post("type_of_rmc"));
            Session::put("selected_product_cat_id",Request::post("product_cat_id"));
            if(Request::post("rmc_grade_id")){
                Session::put("selected_rmc_grade_id",Request::post("rmc_grade_id"));
            }

            // Cookie::queue('name', 'value', 60);

                // $value = Cookie::get('name');
                //                             dd("testing...".$value);
            $vendor_data = $this->checkSuppliersAvailable();
            $is_show_nearest_vendor_popup = 'yes';

            // if($vendor_data["status"] == 200){
                // if(count($vendor_data["data"]) > 0){
                    $message = "";

                    $data["message"] = $message; 
                    if(isset($data["error"]["message"])){
                        $data["error"]["message"] = $message;
                    } 
                    // $nearest_product = array();
                    // foreach($data["data"] as $value){

                    //     array_push($nearest_product,$value['concrete_grade']);

                    // }

                    return redirect()->route('buyer_product_listing',[$id])->with( ['data' => $data] );

                // }else{
                //     $message = "No RMC supplier found in selected region.";
                //     $data["message"] = $message; 
                //     return redirect()->route('buyer_home')->with( ['data_change_loc' => $data,'is_show_nearest_vendor_popup'=>$is_show_nearest_vendor_popup] );
                // } 
            // }else{
            //     $message = "No RMC supplier found in selected region.";
            //     $data["message"] = $message; 
            //     return redirect()->route('buyer_home')->with( ['data_change_loc' => $data,'is_show_nearest_vendor_popup'=>$is_show_nearest_vendor_popup] );
            // }
    
            

        }else if(Request::post("type") == 'get_wish_list'){


            return redirect()->route('buyer_wishlist')->with( ['data' => $data] );

        }else if(Request::post("type") == 'change_location'){

            Session::put("selected_lat",Request::post("lat"));
            Session::put("selected_long",Request::post("long"));
            Session::put("selected_state_id",Request::post("state_id"));
            Session::put("selected_state_name",Request::post("state_name"));
            Session::put("selected_city_name",Request::post("city_name"));
            Session::put("selected_delivery_date",date('Y-m-d',strtotime(Request::post("selected_delivery_date"))));
            Session::put("selected_display_delivery_date",Request::post("selected_delivery_date"));

            Session::put("selected_end_delivery_date", date('Y-m-d',strtotime(Request::post("selected_end_delivery_date"))));
            Session::put("selected_end_display_delivery_date",Request::post("selected_end_delivery_date"));

            Session::put("selected_delivery_qty",Request::post("delivery_qty"));
            Session::put("selected_type_of_rmc",Request::post("type_of_rmc"));
            Session::put("selected_product_cat_id",Request::post("product_cat_id"));
            if(Request::post("rmc_grade_id")){
                Session::put("selected_rmc_grade_id",Request::post("rmc_grade_id"));
            }

            $is_show_nearest_vendor_popup = 'yes';
            if(Request::post("product_cat_id")){
                if(Request::post("product_cat_id") == "custom_mix_dialog_on"){

                    $data["custom_mix_dialog_on"] = Request::post("product_cat_id");
                    if(Request::post("custom_mix_save_id")){
                        $data["custom_mix_save_id"] = Request::post("custom_mix_save_id");
                    }
                    $is_show_nearest_vendor_popup = 'no';
                }
            }
            
            
            // dd($data);

            $vendor_data = $this->checkSuppliersAvailable();
            
            // dd($vendor_data);
            // if($vendor_data["status"] == 200){
                // if(count($vendor_data["data"]) > 0){
                    $message = "";
                    $data["message"] = $message; 
                    $nearest_product = array();
                    $product = array();
                    if(isset($vendor_data["data"])){
                        foreach($vendor_data["data"] as $value){

                            if(!isset($product[$value['concrete_grade']['_id']])){
                                $product[$value['concrete_grade']['_id']] = $value;
                                array_push($nearest_product,$value['concrete_grade']);
                            }

                        }                    
                    }
                    
                    return redirect()->route('buyer_home')->with( ['data_change_loc' => $data,'nearest_product'=>$nearest_product,'is_show_nearest_vendor_popup'=>$is_show_nearest_vendor_popup] );
                // }else{
                //     $message = "No RMC supplier found in selected region.";
                //     $data["message"] = $message; 
                //     return redirect()->route('buyer_home')->with( ['data_change_loc' => $data,'is_show_nearest_vendor_popup'=>$is_show_nearest_vendor_popup] );
                // } 
            // }else{
            //     $message = "No RMC supplier found in selected region.";
            //     $data["message"] = $message; 
            //     return redirect()->route('buyer_home')->with( ['data_change_loc' => $data,'is_show_nearest_vendor_popup'=>$is_show_nearest_vendor_popup] );
            // }
    
            // $data["message"] = $message; 
            // return redirect()->route('buyer_home')->with( ['data_change_loc' => $data] );

        }

    }

    public function product_detail($product_id){
        // echo "Home";exit;
        // dd(Request::all());
        // $data = $this->product_detail_model->showProducts();
        // dd(session('my_cart_id', null));
        // dd($product_id);
        $data = array();
        // $data = Request::post("product_detail");
        
        // $data = json_decode($data);

        $request_data = array();

        if(Session::get("selected_lat") != null){
            $request_data["lat"] = Session::get("selected_lat");
        }

        if(Session::get("selected_long") != null){
            $request_data["long"] = Session::get("selected_long");
        }
        
        if(Session::get("selected_state_id") != null){
            $request_data["state_id"] = Session::get("selected_state_id");
        }

        if(Session::get("selected_end_delivery_date") != null){
            $request_data["end_date"] = Session::get("selected_end_delivery_date");
        }
        
        if(Session::get("selected_delivery_qty") != null){
            $request_data["quantity"] = Session::get("selected_delivery_qty");
        }

        if(Request::get("delivery_date")){

            Session::put("selected_delivery_date",date('Y-m-d', strtotime(Request::get("delivery_date"))));                
            Session::put("selected_display_delivery_date",Request::get("delivery_date"));                

        }
        
        if(Session::get("selected_delivery_date") != null){
            $request_data["delivery_date"] = date('Y-m-d', strtotime(Session::get("selected_delivery_date")));
            $request_data["selected_display_delivery_date"] = Session::get("selected_delivery_date");
        }
        $request_data["with_TM"]  = Session::get("with_TM") != null ? Session::get("with_TM") : 'true';
        if(Request::get("with_TM")){

            $request_data["with_TM"] = Request::get("with_TM") == "yes" ? 'true' : 'false';
            Session::put("with_TM",Request::get("with_TM") == "yes" ? 'true' : 'false');  

        }
        // $request_data["with_CP"]  = Session::get("with_CP") != null ? Session::get("with_CP") : 'false';
        $request_data["with_CP"]  = 'false';
        if(Request::get("with_CP")){

            $request_data["with_CP"] = Request::get("with_CP") == "yes" ? 'true' : 'false';
            // Session::put("with_CP",Request::get("with_CP") == "yes" ? 'true' : 'false');  

        }
        // dd($request_data);
        if(isset($request_data["with_TM"]) && ($request_data["with_TM"] == 'false')){
            $buyer_TM_details = array();

            if(Request::post("TM_no")){

                $buyer_TM_details["TM_no"] = Request::post("TM_no");           

            }
            
            if(Request::post("driver_name")){

                $buyer_TM_details["driver_name"] = Request::post("driver_name");           

            }
            
            if(Request::post("driver_mobile")){

                $buyer_TM_details["driver_mobile"] = Request::post("driver_mobile");           

            }
            
            if(Request::post("TM_operator_name")){

                $buyer_TM_details["TM_operator_name"] = Request::post("TM_operator_name");           

            }
            
            if(Request::post("TM_operator_mobile_no")){

                $buyer_TM_details["TM_operator_mobile_no"] = Request::post("TM_operator_mobile_no");           

            }
            
            if(count($buyer_TM_details) > 0){

                Session::put("buyer_TM_details",$buyer_TM_details);    

            }
        }

        // dd($session_custom_mix_design);

    

        // dd($request_data);
        $data = $this->product_model->showproductDetails($product_id,$request_data);

        // dd($data);

        $vendor_review = "";
        $vendor_material_details = array();
        if($data["status"] == 200){
            if(isset($data["data"]["sub_vendor_id"])){
                $vendor_review = $this->product_model->showVendorReview($data["data"]["sub_vendor_id"]);
                $vendor_material_details = $this->product_model->getAllActiveVendorRatesWithDetails($data["data"]["sub_vendor_id"]);
                // dd($vendor_material_details);
                // dd($vendor_review);
            }
            

            
        }

        
        

        // $product_review = $this->product_model->getProductReview($product_id);

        // dd($product_review);
        $final_data = array();
        // $final_data["product_cat"] = $this->product_cat["data"];
        // $final_data["product_review"] = $product_review["data"];
        $final_data = $data;
        $final_data["data"] = isset($data["data"]) ? $data["data"] : array();
        $final_data["reviews"] = isset($vendor_review["data"]) ? $vendor_review["data"] : array();
        $final_data["brand_details"] = $vendor_material_details;
        // array_push($final_data,$data);
        // $final_data["data"] = $data;
        $final_data["grade_id"] = $product_id;  
        // dd($final_data);
        
        if(Request::post("delivery_date")){
            return $data;
        }else{
            return view("buyer.views.product_detail")->with('data',$final_data);
        }
        
        
    }

    public function getSingleDesignMixCombinationByBrandIds(){
        // dd(Request::all());
        $data = array();

        $request_data = array();

        if(Session::get("selected_lat") != null){
            $request_data["lat"] = Session::get("selected_lat");
        }

        if(Session::get("selected_long") != null){
            $request_data["long"] = Session::get("selected_long");
        }
        
        if(Session::get("selected_state_id") != null){
            $request_data["state_id"] = Session::get("selected_state_id");
        }

        if(Session::get("selected_end_delivery_date") != null){
            $request_data["end_date"] = Session::get("selected_end_delivery_date");
        }
        
        if(Session::get("selected_delivery_qty") != null){
            $request_data["quantity"] = Session::get("selected_delivery_qty");
        }

        if(Request::get("delivery_date")){

            Session::put("selected_delivery_date",date('Y-m-d', strtotime(Request::get("delivery_date"))));                
            Session::put("selected_display_delivery_date",Request::get("delivery_date"));                

        }
        
        if(Session::get("selected_delivery_date") != null){
            $request_data["delivery_date"] = date('Y-m-d', strtotime(Session::get("selected_delivery_date")));
            $request_data["selected_display_delivery_date"] = Session::get("selected_delivery_date");
        }
        $request_data["with_TM"]  = Session::get("with_TM") != null ? Session::get("with_TM") : 'true';
        if(Request::get("with_TM")){

            $request_data["with_TM"] = Request::get("with_TM") == "yes" ? 'true' : 'false';
            Session::put("with_TM",Request::get("with_TM") == "yes" ? 'true' : 'false');  

        }
        // $request_data["with_CP"]  = Session::get("with_CP") != null ? Session::get("with_CP") : 'false';
        $request_data["with_CP"]  = 'false';
        if(Request::get("with_CP")){

            $request_data["with_CP"] = Request::get("with_CP") == "yes" ? 'true' : 'false';
            // Session::put("with_CP",Request::get("with_CP") == "yes" ? 'true' : 'false');  

        }

        $data = $this->product_model->getSingleDesignMixCombinationByBrandIds($request_data);

        // dd($data);
        $vendor_review = "";
        $vendor_material_details = array();
        if($data["status"] == 200){
            if(isset($data["data"]["sub_vendor_id"])){
                $vendor_review = $this->product_model->showVendorReview($data["data"]["sub_vendor_id"]);
                $vendor_material_details = $this->product_model->getAllActiveVendorRatesWithDetails($data["data"]["sub_vendor_id"]);
                // dd($vendor_material_details);
                // dd($vendor_review);
            }
            

            
        }

        
        

        // $product_review = $this->product_model->getProductReview($product_id);

        // dd($product_review);
        $final_data = array();
        // $final_data["product_cat"] = $this->product_cat["data"];
        // $final_data["product_review"] = $product_review["data"];
        $final_data = $data;
        $final_data["data"] = isset($data["data"]) ? $data["data"] : array();
        $final_data["reviews"] = isset($vendor_review["data"]) ? $vendor_review["data"] : array();
        $final_data["brand_details"] = $vendor_material_details;
        // array_push($final_data,$data);
        // $final_data["data"] = $data;
        $final_data["grade_id"] = Request::get("design_mix_id");  
        // dd($final_data);

        if($data["status"] == 200){

            
            $suggestion_array = array();
            // $final_data = array();
            
            // if(Request::get('rmc_div_id')){
            //     $data["data"]["rmc_div_id"] = Request::get('rmc_div_id');
            // }

            // dd($cart_details);
            // $final_data["cart_details"] = $cart_details;

                $returnHTML = view('buyer.views.product_single_combination_detail')->with('data', $final_data)->render();
                $returnHTMLslide = "";
           

                
        

        return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
    }else{
        // return $data;

        // return response()->json(array('status' => $data["status"], 'html'=>""));

        return $data;
    }


    return $data;

    }

    public function addTocart(){

        // dd(Request::all());
        // dd(Session::get("buyer_TM_details",null));
        $data = $this->product_model->addTocart();
        // dd($data);
        if($data["status"] == 200){

            $status = $data["data"]["status"];

            if($status == "error"){
                dd("suggestions");
                $returnHTML = "";
                $returnHTMLslide = "";
                $suggestion_array = array();
                if(isset($data["data"]["suggestion"])){
                    $suggestion = $data["data"]["suggestion"];
                    $low = $suggestion["low"];
                    $high = $suggestion["high"];

                    for($i = 0; $i < count($low); $i++){

                        $low_value = $low[$i];
                        $high_value = $high[$i];
                        
                        $low_load_quntity = $low_value["load_quantity"];
                        $low_vehicle_count = $low_value["count"]; 
                        
                        $high_load_quntity = $high_value["load_quantity"];
                        $high_vehicle_count = $high_value["count"];

                        $msg = "You have to select quantity between ".$high_load_quntity." MT (Delivered in ".$high_vehicle_count." vehicle) to ".$low_load_quntity." MT (Delivered in ".$low_vehicle_count." vehicle)";

                        array_push($suggestion_array,$msg);

                    }

                }

            }else{
                $suggestion_array = array();
                $final_data = array();
                $cart_details = $this->common_model->getCartDetailsById(Session::get('my_cart_id'));
                // dd($cart_details);
                // $final_data["cart_details"] = $cart_details;

                if(Request::post("cart_place") == "slide_cart"){
                    $returnHTML = view('buyer.views.slide_cart_view')->with('cart_details', $cart_details)->render();
                    $returnHTMLslide = "";
                }else if(Request::post("cart_place") == "cart_checkout"){

                    $site_details = $this->profile_model->showSites();
                    $coupon_data = $this->cart_model->getCoupons();
                    $final_data = array();
                    $final_data["site_details"] =  $site_details["data"];
                    $final_data["coupon_data"] =  $coupon_data["data"];
                    // dd($site_details);
                    $returnHTMLslide = view('buyer.views.slide_cart_view')->with('cart_details', $cart_details)->render();
                    $returnHTML = view('buyer.views.cart_checkout_view')->with(['cart_details'=> $cart_details,"data"=>$final_data])->render();
                }

            }            
            

            return response()->json(array('status' => $data["status"],"status_type" => $status, 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide, 'suggest_array' => $suggestion_array));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $data;
        }

        // return $data;
    }

    public function addToWishList(){

        // dd(Request::all());

        $data = $this->product_model->addToWishList();


        return $data;

    }

    public function getWishList(){

        $data = $this->product_model->getWishList();


        return $data;

    }

    public function deleteWishListItem(){

        // dd(Request::all());

        $data = $this->product_model->deleteWishListItem();


        return $data;

    }


    public function getRegionalDetails(){

        $data = $this->common_model->getRegionalDetails();

        // dd($data);


        if($data["status"] == 200){

            if(Request::post("is_search") == "yes"){

                Session::put("selected_lat",Request::post("lat"));
                Session::put("selected_long",Request::post("long"));

                Session::put("selected_state_id",$data["data"]["state_id"]);
                Session::put("selected_state_name",$data["data"]["state_name"]);

                Session::put("selected_city_id",$data["data"]["_id"]);
                Session::put("selected_city_name",$data["data"]["city_name"]);            
                
                Session::put("is_site_selected",0);
                Session::put("selected_site_address","");

            }

            

        }

        return $data;

    }


    public function checkSuppliersAvailable(){

        // dd(Request::all());

        $data = array();
        

        // Session::put("selected_lat",Request::post("lat"));
        // Session::put("selected_long",Request::post("long"));
        // Session::put("selected_state_id",Request::post("state_id"));
        // Session::put("selected_state_name",Request::post("state_name"));
        // Session::put("selected_city_name",Request::post("city_name"));
        

        
        $request_data = array();

        if(Request::get("lat")){
            $request_data["lat"] = Request::get("lat");
        }

        if(Request::get("long")){
            $request_data["long"] = Request::get("long");
        }

        if(Request::get("state_id")){
            $request_data["state_id"] = Request::get("state_id");
        }

        // $cookie = Cookie::make('name', 'value', 60);
        // Cookie::queue('name', 'value', 60);
            
        
        
        // dd($request_data);
        // $getCart = $this->product_model->getCart();
        $data = $this->product_model->checkSuppliersAvailable("","",$request_data, "", "", "");
        // dd($data);
        $message = "";
        if($data["status"] == 200){

            if(count($data["data"]) > 0){
                $message = "";

                

            }else{
                $message = "No RMC supplier found in selected region.";
            }

            
            

        }

        $data["message"] = $message; 

        return $data;

    }

    
}
