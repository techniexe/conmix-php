<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;

class SignUpController extends Controller
{
    //
    protected $signup_model;
    public function __construct(Request $request){
        
        $this->signup_model = new \App\BuyerModel\SignUpModel();

        
    }

    public function signupOTPSend(){
        // dd(Request::all());
        $data = $this->signup_model->signupOTPSend();

        return $data;

    }

    public function signupOTPVerify(){
        // dd(Request::all());
        $data = $this->signup_model->signupOTPVerify();
        // dd($data);
        
        return $data;

    }

    public function signupRegistration(){
        // dd(Request::all());
        $data = $this->signup_model->signupRegistration();
        // dd($data);
        
        return $data;
    }
}
