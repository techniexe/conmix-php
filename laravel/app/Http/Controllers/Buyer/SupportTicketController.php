<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;

class SupportTicketController extends Controller
{
    //
    protected $support_model;
    public function __construct(){
        
        $this->support_model = new \App\BuyerModel\SupportTicketModel();
    }

    public function index(){

        $data = array();
        
        $data = $this->support_model->showTickets();

        // dd($data);

        return view('buyer.views.support_ticket.support_ticket')->with("data",$data);

    }
    
    public function mobileSupportTicket($custom_token){
        // dd($custom_token);

        session()->put("buyer_custom_token",$custom_token);

        $data = array();
        
        $data = $this->support_model->showTickets();

        // dd($data);

        return view('buyer.views.support_ticket.support_ticket_mobile')->with("data",$data);

    }

    public function showTicketDetails($ticket_id){

        $data = array();
        $data = $this->support_model->showTicketDetails($ticket_id);  
        $ticket_messages = $this->support_model->getTicketMessages($ticket_id); 
        $data["ticket_id"] = $ticket_id;
        $data["ticket_messages"] = $ticket_messages["data"];
        // dd($data);

        return view('buyer.views.support_ticket.ticket_detail')->with("data",$data);

    }

    public function addTicket(){

        // dd(Request::all());
        // dd($data);
        $response = $this->support_model->addTicket();
        // dd($response);
        return $response;

    }

    public function replyTicket(){

        // dd(Request::all());
        // dd($data);
        $response = $this->support_model->replyTicket();

        return $response;

    }
}
