<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;
use Razorpay\Api\Api;
use Session;
use Redirect;

class RazorpayController extends Controller
{
    //

    public function payWithRazorpay()
    {        
        return view('payWithRazorpay');
    }

    public function payment()
    {
        $data = [
            "status" => 200,
            "data" => array(),
            "error" => array()  
        ];
        //Input items of form
        $input = Request::all();
        // dd($input);
        //get API Configuration 
        $api = new Api(config('razor_key.razor_key'), config('razor_key.razor_secret'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($input['razorpay_payment_id']);

        if(count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 

            } catch (\Exception $e) {
                // return  $e->getMessage();
                $error_msg = $e->getMessage();
                // \Session::put('error',$e->getMessage());
                $error = array(
                    "msg" => $error_msg
                );
                // return redirect()->back();

                $data["status"] = "300";
                $data["error"] = $error;

                return $data;
            }

            // Do something here for store payment details in database...
        }
        
        // \Session::put('success', 'Payment successful, your order will be despatched in the next 48 hours.');
        // dd(Session::get('success'));
        // return redirect()->back();
        return $data;
    }
}
