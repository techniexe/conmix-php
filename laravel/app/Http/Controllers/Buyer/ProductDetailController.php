<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Request;

class ProductDetailController extends Controller
{
    //
    protected $product_detail_model;
    protected $common_model;
    protected $product_cat;
    public function __construct(Request $request){
        
        $this->product_detail_model = new \App\BuyerModel\ProductDetailModel();
        $this->common_model = new \App\CommonAPIModel();

        $this->product_cat = $this->common_model->getProductCategory();
        
    }

    public function index(){
        // echo "Home";exit;
        // dd(Request::all());
        // $data = $this->product_detail_model->showProducts();
        $data = Request::post("product_detail");

        $data = json_decode($data);
        

        $final_data = array();
        $final_data["product_cat"] = $this->product_cat["data"];
        array_push($final_data,$data);
        // $final_data["data"] = $data;

        // dd($final_data);
        
        // if(isset($data["error"])){
        //     return view("buyer.views.home")->with('data',$data);
        // }else{
            return view("buyer.views.product_detail")->with('data',$final_data);
        // }
        
        
    }
}
