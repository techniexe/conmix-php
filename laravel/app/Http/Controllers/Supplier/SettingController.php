<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class SettingController extends Controller
{
    //
    //
    protected $setting_model;
    protected $common_model;
    public function __construct(){
        
        $this->setting_model = new \App\SupplierModel\SettingModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){

        $data = array();
        $data = $this->setting_model->getSettings();  
        $admixture_brand_data = $this->common_model->getAdMixtureBrand();
        $admixture_settings = $this->setting_model->getAdmixSettings();
        // dd($admixture_settings);
        $data["admixture_brand_data"] = $admixture_brand_data["data"];
        $data["admixture_settings"] = $admixture_settings["data"];

        // dd($data);

        return view('supplier.settings.settings')->with("data",$data);

    }

    public function editSettings(){
        // dd(Request::all());
        $data = array();

        $data = $this->setting_model->editSettings();

        return $data;

    }
    
    public function editAdmixSettings(){
        // dd(Request::all());
        $data = array();

        $data = $this->setting_model->editAdmixSettings();

        return $data;

    }
    
    public function orderAceeptOrNot(){
        // dd(Request::all());
        $data = array();

        $data = $this->setting_model->orderAceeptOrNot();
        // dd($data);
        return $data;

    }

}
