<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class ProductController extends Controller
{
    //
    protected $product_model;
    protected $admin_product_model;
    protected $common_model;
    protected $contact_model;
    protected $address_model;
    public function __construct(Request $request){
        
        $this->product_model = new \App\SupplierModel\ProductModel();
        $this->admin_product_model = new \App\AdminModel\ProductModel();
        $this->common_model = new \App\CommonAPIModel();
        $this->contact_model = new \App\SupplierModel\ContactDetailModel();
        $this->address_model = new \App\SupplierModel\AddressModel();

        
    }

    public function showProducts(){

        $data = array();
        $cat_data = $this->common_model->getProductCategory();
        $contact_data = $this->contact_model->showContactDetails();
        $address_data = $this->address_model->showAddressDetails();
        // $sub_cat_data = $this->admin_product_model->getSubCategories();
        
        $data = $this->product_model->showProducts();
        $data['category'] = $cat_data["data"];
        $data['contact_data'] = $contact_data["data"];
        $data['address_data'] = $address_data["data"];
        // $data['sub_category'] = $sub_cat_data["data"];
        // dd($data);
        // $data["data"] = array();
        return view('supplier.product.product_list')->with("data",$data);

    }

    public function addProduct(){
        // dd(Request::all());
        $data = array();

        $data = $this->product_model->addProduct();

        return $data;

    }

    public function editProducts(){

        // dd(Request::all());
        $data = $this->product_model->editProducts();

        return $data;

    }

    public function getSubCategory($cat_id){
        
        $sub_cat_data = $this->common_model->getProductSubCategory($cat_id);
        // dd($sub_cat_data);
        return $sub_cat_data;
    }

    public function updateProductStock(){
        // dd(Request::all());
        $sub_cat_data = $this->product_model->updateProductStock();
        // dd($sub_cat_data);
        return $sub_cat_data;

    }
}
