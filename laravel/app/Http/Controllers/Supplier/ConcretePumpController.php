<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class ConcretePumpController extends Controller
{
    //
    protected $concrete_model;
    protected $driver_model;
    protected $common_model;
    protected $address_model;
    public function __construct(){
        $this->concrete_model = new \App\SupplierModel\ConcretePumpModel();
        $this->address_model = new \App\SupplierModel\AddressModel();

        $this->driver_model = new \App\SupplierModel\DriverModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->concrete_model->showConcretePump();
        $category_data = $this->common_model->getConcretePumpCategory();
        $operator_data = $this->driver_model->showOperatorDetails();
        $gang_data = $this->driver_model->showPumpGangDetails();
        $address_data = $this->address_model->showAddressDetails();
        
        $data["category_data"] = $category_data["data"];
        $data["operator_data"] = $operator_data["data"];
        $data["gang_data"] = $gang_data["data"];
        $data["address_data"] = $address_data['data'];
        // dd($data);

        return view('supplier.concrete_pump.concrete_pump')->with("data",$data);

    }

    public function addConcretePump(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->concrete_model->addConcretePump();
        
        // dd($data);

        return $data;

    }

    public function editConcretePump(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->concrete_model->editConcretePump();
        
        // dd($data);

        return $data;

    }
}
