<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class ForgotPassController extends Controller
{
    //
    protected $forgot_pass_model;
    public function __construct(Request $request){
        
        $this->forgot_pass_model = new \App\SupplierModel\ForgotPassModel();

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        // $data = $this->bank_model->showBankDetails();
        
        // dd($data);

        return view('supplier.forgot_pass.forgot_pass_email_verifier')->with("data",$data);

    }

    public function forgotPassOTPSend(){
        // dd(Request::all());
        $data = $this->forgot_pass_model->forgotPassOTPSend();

        if($data["status"] == 200){
            // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
            return view("supplier.forgot_pass.forgot_pass_email_verifier_otp")->with('data',$data);
        }else{
            return view("supplier.forgot_pass.forgot_pass_email_verifier")->with('data',$data);
        }

    }

    public function forgotPassOTPVerify(){

        $data = $this->forgot_pass_model->forgotPassOTPVerify();
        // dd($data);
        if($data["status"] == 200){
            $data["message"] = "OTP verified successfully";
            // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
            return view("supplier.forgot_pass.forgot_pass")->with('data',$data);
            // return redirect()->route('supplier_dashboard');
            // redirect('supplier_dashboard');
        }else{

            // if(Request::post('email_mobile') == "email"){

            //     $data["data"]["message"] = "Enter the 6 digits OTP sent on your email ".Request::post('email_mobile');
            //     $data["data"]["email_mobile"] = Request::post('email_mobile');
            //     $data["data"]["signup_type"] = "email";

            // }else if(Request::post('email_mobile') == "mobile"){
                
            //     $data["data"]["message"] = "Enter the 6 digits OTP sent on your mobile no. ". Request::post('email_mobile');
            //     $data["data"]["email_mobile"] = Request::post('email_mobile');
            //     $data["data"]["signup_type"] = "mobile";

            // }
            // dd($data);
            return view("supplier.forgot_pass.forgot_pass_email_verifier_otp")->with('data',$data);
        }

    }

    public function forgotPass(){

        $data = $this->forgot_pass_model->forgotPass();
        // dd($data);
        if($data["status"] == 200){
            $data["message"] = "Password updated successfully. Please login";
            return redirect('supplier/login')->with('message',$data["message"]);
            // return view("supplier.Authentication.login")->with('data',$data);
        }else{
            return view("supplier.forgot_pass.forgot_pass")->with('data',$data);
        }

    }
}
