<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class BankDetailsController extends Controller
{
    //
    protected $bank_model;
    public function __construct(Request $request){
        
        $this->bank_model = new \App\SupplierModel\BankDetailsModel();

        
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->bank_model->showBankDetails();
        
        // dd($data);

        return view('supplier.bank_details.bank_details')->with("data",$data);

    }

    public function addBankDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->bank_model->addBankDetails();
        
        // dd($data);

        return $data;

    }

    public function editBankDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->bank_model->editBankDetails();
        
        // dd($data);

        return $data;

    }
    
    public function setDefaultBaknkDefault(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->bank_model->setDefaultBaknkDefault();
        
        // dd($data);

        return $data;

    }
    
}
