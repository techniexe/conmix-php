<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class ProfileController extends Controller
{
    //
    protected $profile_model;
    protected $common_model;
    public function __construct(){
        
        $this->profile_model = new \App\SupplierModel\ProfileModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){

        $data = $this->profile_model->showProfileDetails();

        
        // dd($data);

        return view('supplier.profile.profile_view')->with("data",$data);

    }
    
    public function editProfileView(){

        $data = $this->profile_model->showProfileDetails();

        // dd($data);

        return view('supplier.profile.profile_edit')->with("data",$data);

    }

    public function editProfile(){

    //    dd(Request::all());
         $response = $this->profile_model->editProfile();
        //  dd($response);
         return $response;

    }

    public function changePassword(){

        //    dd(Request::all());
             $response = $this->profile_model->changePassword();
            //  dd($response);
             return $response;
    
        }

        public function getSupplierNotifications(){

            //    dd(Request::all());
            
                 $response = $this->common_model->getSupplierNotifications();
        
                //  dd($response);
        
                 if($response["status"] == 200){
        
        
        
                    $returnHTML = view('supplier.common.noti_list_ajax')->with('data', $response)->render();
                    $returnHTMLslide = "";
                      
                  
                    
        
                    return response()->json(array('status' => $response["status"], 'html'=>$returnHTML));
                }else{
                    // return $data;
        
                    // return response()->json(array('status' => $data["status"], 'html'=>""));
        
                    return $response;
                }
        
                //  dd($response);
                 return $response;
        
            }
            
        public function supplierNotifications(){

            //    dd(Request::all());
                //  $data = $this->common_model->getSupplierNotifications();
                $data = array();
                $data = $this->common_model->getSupplierNotifications();
                // dd($data);
                 return view('supplier.noti.notification')->with("data",$data);

        }

        public function updateNotiStatus($id){

            //    dd(Request::all());
            $response = $this->profile_model->updateNotiStatus($id);
            //  dd($response);
            return $response;

        }
}
