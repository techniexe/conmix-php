<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class ProfitOverheadController extends Controller
{
    //
    protected $profit_overhead;
    protected $address_model;
    protected $common_model;
    public function __construct(Request $request){
        
        $this->profit_overhead = new \App\SupplierModel\ProfitOverheadModel();
        $this->address_model = new \App\SupplierModel\AddressModel();
        $this->common_model = new \App\CommonAPIModel();

        
    }

    public function showProfitMargin(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->profit_overhead->showProfitMargin();
        $address_data = $this->address_model->showAddressDetails();
        
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.profit_overhead_margin.profit_margin_detail')->with("data",$data);

    }

    public function addProfitMargin(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->profit_overhead->addProfitMargin();
        
        // dd($data);

        return $data;

    }

    public function editProfitMargin(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->profit_overhead->editProfitMargin();
        
        // dd($data);

        return $data;

    }
    
    public function deleteProfitMargin(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->profit_overhead->deleteProfitMargin();
        
        // dd($data);

        return $data;

    }
    
    public function showOverheadMargin(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->profit_overhead->showOverheadMargin();
        $address_data = $this->address_model->showAddressDetails();
        
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.profit_overhead_margin.overhead_margin_detail')->with("data",$data);

    }

    public function addOverheadMargin(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->profit_overhead->addOverheadMargin();
        
        // dd($data);

        return $data;

    }

    public function editOverheadMargin(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->profit_overhead->editOverheadMargin();
        
        // dd($data);

        return $data;

    }
    
    public function deleteOverheadMargin(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->profit_overhead->deleteOverheadMargin();
        
        // dd($data);

        return $data;

    }
}
