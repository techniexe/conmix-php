<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class ContactDetailController extends Controller
{
    //
    protected $contact_model;
    public function __construct(Request $request){
        
        $this->contact_model = new \App\SupplierModel\ContactDetailModel();

        
    }

    public function index(){

        $data = array();
        
        $data = $this->contact_model->showContactDetails();
        
        // dd($data);

        return view('supplier.contact_details.contact_details')->with("data",$data);

    }
    
    public function addContactDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->contact_model->addContactDetails();
        
        // dd($data);

        return $data;

    }
    
    public function editContactDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->contact_model->editContactDetails();
        
        // dd($data);

        return $data;

    }
}
