<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class DesignMixController extends Controller
{
    //
    protected $design_mix;
    protected $driver_model;
    protected $common_model;
    protected $address_model;
    public function __construct(){
        $this->design_mix = new \App\SupplierModel\DesignMixModel();
        $this->address_model = new \App\SupplierModel\AddressModel();

        $this->driver_model = new \App\SupplierModel\DriverModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->design_mix->showDesignMix();
        // $category_data = $this->common_model->getConcretePumpCategory();
        // $operator_data = $this->driver_model->showOperatorDetails();
        
        // $data["category_data"] = $category_data["data"];
        // $data["operator_data"] = $operator_data["data"];
        // dd($data);

        return view('supplier.design_mix.design_mix_detail')->with("data",$data);

    }

    public function addMixView(){

        $data = array();
        $concrete_grade_data = $this->common_model->getConcreteGrade();
        $cement_brand_data = $this->common_model->getCementBrand();
        $sand_source_data = $this->common_model->getSandSource();
        $sand_zone_data = $this->common_model->getSandZone();
        $aggregate_source_data = $this->common_model->getAggregateSource();
        $aggregate_cat_data = $this->common_model->getAggregateSandCategory();
        $cement_grade_data = $this->common_model->getCementGrade();
        $address_data = $this->address_model->showAddressDetails();
        // dd($aggregate_cat_data);
        $aggregate_sub_cat_data = array(
            "data" => array()
        );
        if($aggregate_cat_data["status"] == 200){
            if(isset($aggregate_cat_data["data"][0]["_id"])){
                $cat_id = $aggregate_cat_data["data"][0]["_id"];

                $aggregate_sub_cat_data = $this->common_model->getAggregateSandSubCategory($cat_id);
            }
            
        }

        
        $fly_ash_source_data = $this->common_model->getFlyAshSource();
        $admixture_brand_data = $this->common_model->getAdMixtureBrand();

        $data["concrete_grade_data"] = $concrete_grade_data['data'];
        $data["cement_brand_data"] = $cement_brand_data['data'];
        $data["sand_source_data"] = $sand_source_data['data'];
        $data["sand_zone_data"] = $sand_zone_data['data'];
        $data["aggregate_source_data"] = $aggregate_source_data['data'];
        $data["aggregate_sub_cat_data"] = $aggregate_sub_cat_data["data"];
        $data["fly_ash_source_data"] = $fly_ash_source_data["data"];
        $data["admixture_brand_data"] = $admixture_brand_data["data"];
        $data["cement_grade_data"] = $cement_grade_data["data"];
        $data["address_data"] = $address_data["data"];

        // $data["state_data"] = $state_data["data"];
        // $data["city_data"] = $city_data["data"];
        // dd($data);
        return view("supplier.design_mix.design_mix_add")->with("data",$data);

    }

    public function addMix(){
        // dd(Request::all());
        $data = array();

        $data = $this->design_mix->addMix();

        return $data;

    }
    
    public function getDesignMixGradeWise(){
        // dd(Request::all());
        $data = array();

        $data = $this->design_mix->getDesignMixGradeWise();

        // dd($data);
        if($data["status"] == 200){

            
                $suggestion_array = array();
                $final_data = array();
                
                // dd($cart_details);
                // $final_data["cart_details"] = $cart_details;

                    $returnHTML = view('supplier.design_mix.design_mix_grade_wise_ajax')->with('data', $data["data"])->render();
                    $returnHTMLslide = "";
               

                    
            

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $data;
        }


        return $data;

    }
    
    public function getDesignMixCombinationGradeWise(){
        // dd(Request::all());
        $data = array();

        $data = $this->design_mix->getDesignMixCombinationGradeWise();

        // dd($data);
        if($data["status"] == 200){

            
                $suggestion_array = array();
                $final_data = array();
                
                // dd($cart_details);
                // $final_data["cart_details"] = $cart_details;

                    $returnHTML = view('supplier.design_mix.design_mix_combination_grade_wise_ajax')->with('data', $data["data"])->render();
                    $returnHTMLslide = "";
               

                    
            

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $data;
        }


        return $data;

    }
    
    public function getSingleDesignMixCombinationByBrandIds(){
        // dd(Request::all());
        $data = array();

        $data = $this->design_mix->getSingleDesignMixCombinationByBrandIds();

        // dd($data);
        if($data["status"] == 200){

            
                $suggestion_array = array();
                $final_data = array();
                
                if(Request::get('rmc_div_id')){
                    $data["data"]["rmc_div_id"] = Request::get('rmc_div_id');
                }

                // dd($cart_details);
                // $final_data["cart_details"] = $cart_details;

                    $returnHTML = view('supplier.design_mix.design_mix_single_combination_grade_wise_ajax')->with('data', $data["data"])->render();
                    $returnHTMLslide = "";
               

                    
            

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $data;
        }


        return $data;

    }


    public function editMixView($mix_id){

        $data = array();
        $data = $this->design_mix->getDesignMixById($mix_id);
        // dd($data);
        $concrete_grade_data = $this->common_model->getConcreteGrade();
        $cement_brand_data = $this->common_model->getCementBrand();
        $sand_source_data = $this->common_model->getSandSource();
        $sand_zone_data = $this->common_model->getSandZone();
        $aggregate_source_data = $this->common_model->getAggregateSource();
        $aggregate_cat_data = $this->common_model->getAggregateSandCategory();
        $cement_grade_data = $this->common_model->getCementGrade();
        $admixture_types_data = $this->common_model->getAdmixtureTypes();
        $address_data = $this->address_model->showAddressDetails();
        // dd($aggregate_cat_data);
        $aggregate_sub_cat_data = array(
            "data" => array()
        );
        if($aggregate_cat_data["status"] == 200){
            if(isset($aggregate_cat_data["data"][0]["_id"])){
                $cat_id = $aggregate_cat_data["data"][0]["_id"];

                $aggregate_sub_cat_data = $this->common_model->getAggregateSandSubCategory($cat_id);
            }
            
        }

        
        $fly_ash_source_data = $this->common_model->getFlyAshSource();
        $admixture_brand_data = $this->common_model->getAdMixtureBrand();

        $data["concrete_grade_data"] = $concrete_grade_data['data'];
        $data["cement_brand_data"] = $cement_brand_data['data'];
        $data["sand_source_data"] = $sand_source_data['data'];
        $data["sand_zone_data"] = $sand_zone_data['data'];
        $data["aggregate_source_data"] = $aggregate_source_data['data'];
        $data["aggregate_sub_cat_data"] = $aggregate_sub_cat_data["data"];
        $data["fly_ash_source_data"] = $fly_ash_source_data["data"];
        $data["admixture_brand_data"] = $admixture_brand_data["data"];
        $data["cement_grade_data"] = $cement_grade_data["data"];
        $data["admixture_types_data"] = $admixture_types_data["data"];
        $data["address_data"] = $address_data["data"];

        // $data["state_data"] = $state_data["data"];
        // $data["city_data"] = $city_data["data"];
        // dd($data);
        return view("supplier.design_mix.design_mix_edit")->with("data",$data);

    }

    public function editMix(){
        // dd(Request::all());
        $data = array();

        $data = $this->design_mix->editMix();

        return $data;

    }
    
    public function getAdmixtureTypes(){
        // dd(Request::all());
        $data = array();

        $data = $this->common_model->getAdmixtureTypes();

        return $data;

    }
    
    public function getAggregateSubCat2($sub_cat_id){
        // dd(Request::all());
        $data = array();

        $data = $this->common_model->getAggregateSandSubCategoryForCustomMix($sub_cat_id);

        return $data;

    }


    public function showMedia(){

        $data = array();
        
        $data = $this->design_mix->showMedia();
        // $category_data = $this->common_model->getConcretePumpCategory();
        // $operator_data = $this->driver_model->showOperatorDetails();
        
        // $data["category_data"] = $category_data["data"];
        // $data["operator_data"] = $operator_data["data"];
        // dd($data);

        return view('supplier.design_mix_media.design_mix_media')->with("data",$data);

    }

    public function addPrimaryMedia(){

        // dd(Request::all());
        // dd($data);
        $response = $this->design_mix->addPrimaryMedia();
        // dd($response);
        return $response;

    }
    
    public function addSecondoryMedia(){

        // dd(Request::all());
        // dd($data);
        $response = $this->design_mix->addSecondoryMedia();
        // dd($response);
        return $response;

    }
    
    public function deleteMedia(){

        // dd(Request::all());
        // dd($data);
        $response = $this->design_mix->deleteMedia();
        // dd($response);
        return $response;

    }
}
