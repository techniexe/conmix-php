<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class DashboardController extends Controller
{
    //
    protected $dashboard_model;
    protected $common_model;
    protected $login_model;
    protected $setting_model;
    public function __construct(Request $request){
        
        $this->dashboard_model = new \App\SupplierModel\DashboardModel();
        $this->common_model = new \App\CommonAPIModel();
        $this->login_model = new \App\SupplierModel\LoginModel();
        $this->setting_model = new \App\SupplierModel\SettingModel();

        
    }

    public function index(){    
        // echo "Dashboard";
        
        $profile = session('supplier_profile_details', null);
        // dd($profile);
        if($profile == null){

            $this->login_model->logout();

            return redirect('supplier');

        }
        
        $data = $this->dashboard_model->getDashboardDetails();
        
        $day_chart_data = $this->dashboard_model->getDayChartDetails();
        
        $product_monthly_chart_data = $this->dashboard_model->getProductBaseMonthlyChartDetails();
        $product_day_chart_data = $this->dashboard_model->getProductBaseDayChartDetails();
        $monthly_chart_data = $this->dashboard_model->getMonthlyChartDetails();
        $latest_orders = $this->dashboard_model->getSuppliersLatesOrders();
        $queue_orders = $this->dashboard_model->getSuppliersQueueOrders();
        $latest_products = $this->dashboard_model->getLatestProducts();
        $product_stock = $this->dashboard_model->getProductStock();
        $setting_data = $this->setting_model->getSettings(); 
        // dd($product_stock);
        // dd($setting_data);
        $cat_data = $this->common_model->getProductCategory();

        $concrete_grade_data = $this->common_model->getConcreteGrade();


        $data["monthly_chart_data"] = $monthly_chart_data["data"];
        $data["day_chart_data"] = $day_chart_data["data"];
        $data["product_monthly_chart_data"] = $product_monthly_chart_data["data"];
        $data["product_day_chart_data"] = $product_day_chart_data["data"];
        $data["latest_orders"] = $latest_orders["data"];
        $data["queue_orders"] = $queue_orders["data"];
        $data["latest_products"] = $latest_products["data"];
        $data["product_stock"] = $product_stock["data"];
        $data['category'] = $cat_data["data"];
        $data["concrete_grade_data"] = $concrete_grade_data['data'];
        if(isset($setting_data['data']["new_order_taken"])){
            $data["new_order_taken"] = $setting_data['data']["new_order_taken"];
        }else{
            $data["new_order_taken"] = "false";
        }

        // dd($data);
        
        return view("supplier.Dashboard.dashboard")->with('data',$data);
        
    }

    public function getDayCapacity(){

        // dd(Request::all());

        $data = $this->dashboard_model->getDayCapacity();

        return $data;
    }

    public function afterLogin(){
    
        // Auth::logout(); // logout user
        return redirect(\URL::previous());

    }
    public function getMonthlyChartDetail(){

        // dd(Request::all());
        $data = $this->dashboard_model->getMonthlyChartDetails();


        return $data;
    }
    
    public function getDailyChartDetail(){
        // dd(Request::all());
        $data = $this->dashboard_model->getDayChartDetails();


        return $data;
    }
    
    public function getProductBaseMonthlyChartDetails(){
        // dd(Request::all());
        $data = $this->dashboard_model->getProductBaseMonthlyChartDetails();


        return $data;
    }
    
    public function getProductBaseDayChartDetails(){
        // dd(Request::all());
        $data = $this->dashboard_model->getProductBaseDayChartDetails();


        return $data;
    }
}
