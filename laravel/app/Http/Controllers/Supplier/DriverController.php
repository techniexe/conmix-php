<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class DriverController extends Controller
{
    //
    protected $driver_model;
    protected $address_model;
    public function __construct(Request $request){
        
        $this->driver_model = new \App\SupplierModel\DriverModel();
        $this->address_model = new \App\SupplierModel\AddressModel();

        
    }

    public function index(){
        
        // dd(Request::all());
        $data = array();

        
        
        $data = $this->driver_model->showDriverDetails();
        // dd($data);
        $address_data = $this->address_model->showAddressDetails();
        $data["address_data"] = $address_data["data"];
        

        return view('supplier.driver.driver_details')->with("data",$data);

    }

    public function addDriverDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->addDriverDetails();
        
        // dd($data);

        return $data;

    }
    
    public function editDriverDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->editDriverDetails();
        
        // dd($data);

        return $data;

    }
    
    
    public function showOperatorDetails(){
        
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->showOperatorDetails();
        $address_data = $this->address_model->showAddressDetails();
        $data["address_data"] = $address_data["data"];
        // dd($data);

        return view('supplier.operator.operator_details')->with("data",$data);

    }

    public function addOperatorDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->addOperatorDetails();
        
        // dd($data);

        return $data;

    }
    
    public function editOperatorDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->editOperatorDetails();
        
        // dd($data);

        return $data;

    }
    
    public function showPumpGangDetails(){
        
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->showPumpGangDetails();
        $address_data = $this->address_model->showAddressDetails();
        $data["address_data"] = $address_data["data"];
        // dd($data);

        return view('supplier.pump_gang.pump_gang_details')->with("data",$data);

    }

    public function addPumpGangDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->addPumpGangDetails();
        
        // dd($data);

        return $data;

    }
    
    public function editPumpGangDetails(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->driver_model->editPumpGangDetails();
        
        // dd($data);

        return $data;

    }
}
