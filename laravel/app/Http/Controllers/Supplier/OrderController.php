<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class OrderController extends Controller
{
    //
    protected $order_model;
    protected $vehicle_model;
    protected $cp_model;
    public function __construct(){
        
        $this->order_model = new \App\SupplierModel\OrderModel();
        $this->vehicle_model = new \App\SupplierModel\VehicleModel();
        $this->cp_model = new \App\SupplierModel\ConcretePumpModel();
    }

    public function index(){

        // dd(Request::all());
        $data = $this->order_model->showOrders();  

        // dd($data);

        return view('supplier.orders.orders')->with("data",$data);

    }

    public function orderDetails($order_id){
        // dd($order_id);
        $data = array();

        $vehicles = $this->vehicle_model->showTM();
        $cp_lists = $this->cp_model->showConcretePump();

        $data = $this->order_model->orderDetails($order_id);  

        $data['vehicles'] = $vehicles["data"];
        $data['cp_lists'] = $cp_lists["data"];

        // dd($data);

        return view('supplier.orders.order_details')->with("data",$data);

    }

    public function truckAssign(){
        // dd(Request::all());
        $data = array();

        $data = $this->order_model->truckAssign();
        // dd($data);
        return $data;

    }
    
    public function cpAssign(){
        // dd(Request::all());
        $data = array();

        $data = $this->order_model->cpAssign();
        // dd($data);
        return $data;

    }

    public function uploadOrderBill(){
        // dd(Request::all());
        $data = array();
        $data = $this->order_model->uploadOrderBill();  

        return $data;
    }
    
    public function reassigningOrders(){
        //  dd(Request::all());
        $data = array();
        $data = $this->order_model->reassigningOrders();  

        return $data;
    }

    public function trackingDetails($item_id,$tracking_id){

        // dd(Request::all());

        $data = $this->order_model->trackingDetails($item_id,$tracking_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('supplier.orders.order_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }

        
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }
    
    
    
    public function CPtrackingDetails($item_id){
        // dd($item_id);
        // dd(Request::all());

        $data = $this->order_model->CPtrackingDetails($item_id);
        // dd($data);

        if($data["status"] == 200){

            $returnHTML = view('supplier.orders.order_cp_track_popup')->with(["data"=>$data])->render();


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }

        
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }
    
    public function getDeliveredCP($vendor_order_id,$order_item_id){
        // dd($item_id);
        // dd(Request::all());

        $data = $this->order_model->getDeliveredCP($vendor_order_id,$order_item_id);
        // dd($data);

        // if($data["status"] == 200){

        //     $returnHTML = view('supplier.orders.order_cp_track_popup')->with(["data"=>$data])->render();


        //     return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        // }else{

        // }

        
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }

    public function orderAccept(){
        // dd(Request::all());
        $data = array();
        $data = $this->order_model->orderAccept();  
        // dd($data);
        return $data;
    }

    public function surpriseOrders(){

        
        $data = $this->order_model->surpriseOrders();  

        // dd($data);

        return view('supplier.orders.surprise_orders')->with("data",$data);

    }

    public function surpriseOrderDetails($order_id){
        // dd($order_id);
        $data = array();

        $vehicles = $this->vehicle_model->showTM();

        $data = $this->order_model->surpriseOrderDetails($order_id);  

        $data['vehicles'] = $vehicles["data"];

        // dd($data);

        return view('supplier.orders.surprise_order_details')->with("data",$data);

    }

    public function surpriseOrderAccept(){
        // dd(Request::all());
        $data = array();
        $data = $this->order_model->surpriseOrderAccept();  
        // dd($data);
        return $data;
    }
    
    public function report7DayCubeTest(){
        // dd(Request::all());
        $data = array();
        $data = $this->order_model->report7DayCubeTest();  

        return $data;
    }
    
    public function report28DayCubeTest(){
        // dd(Request::all());
        $data = array();
        $data = $this->order_model->report28DayCubeTest();  

        return $data;
    }

    public function getAssignedOrderListByDate(){

        // dd(Request::all());

        $reassign_reason = Request::get('reassign_reason');

        $data = $this->order_model->getAssignedOrderListByDate();
        // dd($data);

        if($data["status"] == 200){

            if($reassign_reason == 'material_out_of_stock'){
                $returnHTML = view('supplier.orders.reassign_orders_for_material')->with(["data"=>$data])->render();
            }else if($reassign_reason == 'breakdown'){
                $returnHTML = view('supplier.orders.reassign_orders_for_breakdown')->with(["data"=>$data])->render();
            }else if($reassign_reason == 'order_rejected'){
                $returnHTML = view('supplier.orders.reassign_orders_for_rejected')->with(["data"=>$data])->render();
            }


            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML));
        }else{

        }

        
        

        return $data;

        // return view("supplier.orders.order_track_popup")->with('data',$data);
    }
}
