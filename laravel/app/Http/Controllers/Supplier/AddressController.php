<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class AddressController extends Controller
{
    //.
    protected $address_model;
    protected $common_model;
    public function __construct(Request $request){
        
        $this->address_model = new \App\SupplierModel\AddressModel();
        $this->common_model = new \App\CommonAPIModel();

        
    }

    public function index(){

        $data = array();
        
        $data = $this->address_model->showAddressDetails();        
        
        // dd($data);

        return view('supplier.address.address')->with("data",$data);

    }
    
    public function addAddressView(){

        $data = array();
        $billing_address = $this->address_model->showbillingAddress();
        // dd($billing_address);
        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();
        $region_serv_data = $this->address_model->getRegionServ();
        // $data = $this->address_model->showAddressDetails();
        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];
        $data["region_serv_data"] = $region_serv_data["data"];
        $data["billing_address"] = $billing_address["data"];
        // dd($data);

        return view('supplier.address.address_add')->with("data",$data);

    }

    public function addAddress(){
        // dd(Request::all());
        $data = array();

        $data = $this->address_model->addAddress();
        // dd($data);
        // if($data["status"] == 200){
        //     return redirect('supplier/address');
        // }else{
        //     return view('supplier.address.address_add')->with("data",$data);
        // }

        return $data;

    }
    
    

    public function editAddressView(){

        // dd(Request::all());
        $data = array();
        $billing_address = $this->address_model->showbillingAddress();
        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();
        $region_serv_data = $this->address_model->getRegionServ();
        // $data = $this->address_model->showAddressDetails();
        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];
        $data["data"] = json_decode(Request::post("address_details"));
        $data["region_serv_data"] = $region_serv_data["data"];
        $data["billing_address"] = $billing_address["data"];
        // dd($data);

        return view('supplier.address.address_edit')->with("data",$data);

    }

    public function editAddress(){
        // dd(Request::all());
        $data = array();

        $data = $this->address_model->editAddress();
        // dd($data);
        // if($data["status"] == 200){
        //     return redirect('supplier/address');
        // }else{
        //     return view('supplier.address.address_add')->with("data",$data);
        // }

        return $data;

    }

    public function getStates(){

        $data = array();
        
        $data = $this->common_model->getStates();
        
        // dd($data);

        return $data["data"];

    }
    
    public function getCities(){

        $data = array();
        
        $data = $this->common_model->getCities();
        
        // dd($data);

        return $data["data"];

    }
    
    public function getSourceByRegion(){

        $data = array();
        
        $data = $this->address_model->getSourceByRegion();
        
        // dd($data);

        return $data["data"];

    }
    
// Billing Address

    public function showbillingAddress(){

        $data = array();
        
        $data = $this->address_model->showbillingAddress();
        
        // dd($data);

        return view('supplier.billing_address.billing_address')->with("data",$data);

    }

    public function addBillingAddressView(){

        $data = array();
        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();
        $region_serv_data = $this->address_model->getRegionServ();
        // $data = $this->address_model->showAddressDetails();
        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];
        $data["region_serv_data"] = $region_serv_data["data"];
        // dd($data);

        return view('supplier.billing_address.billing_address_add')->with("data",$data);

    }

    public function addBillingAddress(){
        // dd(Request::all());
        $data = array();

        $data = $this->address_model->addBillingAddress();
        // dd($data);
        // if($data["status"] == 200){
        //     return redirect('supplier/address');
        // }else{
        //     return view('supplier.address.address_add')->with("data",$data);
        // }

        return $data;

    }

    public function editBillingAddressView(){

        // dd(Request::all());
        $data = array();
        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();
        $region_serv_data = $this->address_model->getRegionServ();
        // $data = $this->address_model->showAddressDetails();
        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];
        $data["data"] = json_decode(Request::post("address_details"));
        $data["region_serv_data"] = $region_serv_data["data"];
        // dd($data);

        return view('supplier.billing_address.billing_address_edit')->with("data",$data);

    }

    public function editBillingAddress(){
        // dd(Request::all());
        $data = array();

        $data = $this->address_model->editBillingAddress();
        // dd($data);
        // if($data["status"] == 200){
        //     return redirect('supplier/address');
        // }else{
        //     return view('supplier.address.address_add')->with("data",$data);
        // }

        return $data;

    }


}
