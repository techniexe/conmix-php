<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class StockController extends Controller
{
    //
    protected $stock_model;
    protected $address_model;
    protected $common_model;
    public function __construct(Request $request){
        
        $this->stock_model = new \App\SupplierModel\StockModel();
        $this->address_model = new \App\SupplierModel\AddressModel();
        $this->common_model = new \App\CommonAPIModel();

        
    }

    public function showCementStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->showCementStock();
        $address_data = $this->address_model->showAddressDetails();
        $cement_brand_data = $this->common_model->getCementBrand();
        $cement_grade_data = $this->common_model->getCementGrade();
        
        $data["cement_brand_data"] = $cement_brand_data["data"];
        $data["cement_grade_data"] = $cement_grade_data["data"];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.stock_details.cement_stock_detail')->with("data",$data);

    }

    public function addCementStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->addCementStock();
        
        // dd($data);

        return $data;

    }

    public function editCementStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->editCementStock();
        
        // dd($data);

        return $data;

    }
    
    public function deleteCementStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->deleteCementStock();
        
        // dd($data);

        return $data;

    }
    
    
    public function showSandStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->showSandStock();
        $address_data = $this->address_model->showAddressDetails();
        $sand_source_data = $this->common_model->getSandSource();
        $sand_zone_data = $this->common_model->getSandZone();
        
        
        $data["sand_source_data"] = $sand_source_data["data"];
        $data["sand_zone_data"] = $sand_zone_data["data"];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.stock_details.sand_stock_detail')->with("data",$data);

    }

    public function addSandStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->addSandStock();
        
        // dd($data);

        return $data;

    }

    public function editSandStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->editSandStock();
        
        // dd($data);

        return $data;

    }
    
    public function deleteSandStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->deleteSandStock();
        
        // dd($data);

        return $data;

    }
    
    public function showAggregateStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->showAggregateStock();
        $address_data = $this->address_model->showAddressDetails();
        $aggregate_source_data = $this->common_model->getAggregateSource();
        $aggregate_cat_data = $this->common_model->getAggregateSandCategory();

        $aggregate_sub_cat_data = array();

        if($aggregate_cat_data["status"] == 200){
            if(isset($aggregate_cat_data["data"][0]["_id"])){
                $cat_id = $aggregate_cat_data["data"][0]["_id"];

                $aggregate_sub_cat_data = $this->common_model->getAggregateSandSubCategory($cat_id);
            }
            
        }
        
        $data["aggregate_source_data"] = $aggregate_source_data['data'];
        $data["aggregate_sub_cat_data"] = isset($aggregate_sub_cat_data["data"]) ? $aggregate_sub_cat_data["data"] : array();
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.stock_details.aggregate_stock_detail')->with("data",$data);

    }

    public function addAggregateStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->addAggregateStock();
        
        // dd($data);

        return $data;

    }

    public function editAggregateStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->editAggregateStock();
        
        // dd($data);

        return $data;

    }
    
    public function deleteAggregateStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->deleteAggregateStock();
        
        // dd($data);

        return $data;

    }
    
    
    public function showFlyashStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->showFlyashStock();
        $address_data = $this->address_model->showAddressDetails();
        $fly_ash_source_data = $this->common_model->getFlyAshSource();

        
        $data["fly_ash_source_data"] = $fly_ash_source_data['data'];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.stock_details.flyash_stock_detail')->with("data",$data);

    }

    public function addFlyashStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->addFlyashStock();
        
        // dd($data);

        return $data;

    }

    public function editFlyashStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->editFlyashStock();
        
        // dd($data);

        return $data;

    }
    
    public function deleteFlyashStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->deleteFlyashStock();
        
        // dd($data);

        return $data;

    }
    
    public function showAdmixtureStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->showAdmixtureStock();
        $address_data = $this->address_model->showAddressDetails();
        $admixture_brand_data = $this->common_model->getAdMixtureBrand();
        $admixture_types_data = $this->common_model->getAdmixtureTypes();
        
        $data["admixture_brand_data"] = $admixture_brand_data['data'];
        $data["admixture_types_data"] = $admixture_types_data['data'];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.stock_details.admixture_stock_detail')->with("data",$data);
        // return view('supplier.stock_details.admixture_stock_add')->with("data",$data);

    }

    public function addAdmixtureStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->addAdmixtureStock();
        
        // dd($data);

        return $data;

    }

    public function editAdmixtureStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->editAdmixtureStock();
        
        // dd($data);

        return $data;

    }
    
    public function deleteAdmixtureStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->deleteAdmixtureStock();
        
        // dd($data);

        return $data;

    }
    
    public function showWaterStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->showWaterStock();
        $address_data = $this->address_model->showAddressDetails();
        

        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.stock_details.water_stock_detail')->with("data",$data);

    }

    public function addWaterStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->addWaterStock();
        
        // dd($data);

        return $data;

    }

    public function editWaterStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->editWaterStock();
        
        // dd($data);

        return $data;

    }
    
    public function deleteWaterStock(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->stock_model->deleteWaterStock();
        
        // dd($data);

        return $data;

    }
}
