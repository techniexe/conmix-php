<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;
use App\Http\Requests\LoginRequest;
use Auth;
use URL;
use Session;

class LoginController extends Controller
{
    //
    protected $login_model;
    public function __construct(Request $request){
        
        $this->login_model = new \App\SupplierModel\LoginModel();

        
    }

    public function index(){
        // echo "Dashboard";
        // dd(Session::get('message'));
        if(session("supplier_custom_token",null)){
            return redirect('supplier/dashboard');
        }else{
            // return view("supplier.Authentication.login")->with('data',array("message"=>Session::get('message')));
            $response = new \Illuminate\Http\Response(view("supplier.Authentication.login")->with('data',array("message"=>Session::get('message'))));
            $response->withCookie(cookie('conmix_cookie', "Visited", time()+31556926));
            return $response;
        }
    }

    public function login(LoginRequest $request){
        
        // dd($request->all());
        if(session("supplier_custom_token",null)){
            return redirect('supplier/dashboard');
        }else{
            $validated = $request->validated();
            
            $data = $this->login_model->login();

            // dd($data);
            if($data["status"] == 200){
                // return redirect('supplier/dashboard');
                return redirect('supplier/dashboard_after_login');
            }else{
                return view("supplier.Authentication.login")->with('data',$data);
            }
        }
    }

    public function logout(){

        $this->login_model->logout();

        // return redirect('supplier');
        
        Auth::logout(); // logout user
        return redirect(\URL::previous());
    }

    public function requestOTP(){
        // dd(Request::all());
        $data = $this->login_model->requestOTP();

        return $data;

    }
    
    public function requestOTPToMaster(){
        
        $data = $this->login_model->requestOTPToMaster();

        return $data;

    }
    
    public function requestOTPToPlant(){
        
        
        $data = $this->login_model->requestOTPToPlant();

        return $data;

    }
    
    public function requestOTPforEditProfile(){
        
        
        $data = $this->login_model->requestOTPforEditProfile();

        return $data;

    }
}
