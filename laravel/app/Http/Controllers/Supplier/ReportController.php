<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminController;
use Request;
use Excel;
use \App\SupplierModel\SupplierOrdersExport;
use \App\SupplierModel\SupplierPlantAddressExport;
use \App\SupplierModel\SupplierBillingAddressExport;
use \App\SupplierModel\SupplierTMDriverExport;
use \App\SupplierModel\SupplierOperatorExport;
use \App\SupplierModel\SupplierTMListExport;
use \App\SupplierModel\SupplierCPListExport;
use \App\SupplierModel\SupplierAdmixStockExport;
use \App\SupplierModel\SupplierAggStockExport;
use \App\SupplierModel\SupplierCementStockExport;
use \App\SupplierModel\SupplierFlyAshStockExport;
use \App\SupplierModel\SupplierSandStockExport;
use \App\SupplierModel\SupplierBankDetailExport;
use \App\SupplierModel\SupplierDesignmixExport;

class ReportController extends Controller
{
    //
    protected $report_model;
    protected $common_model;
    public function __construct(){
        
        $this->report_model = new \App\SupplierModel\ReportModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){

        $data = array();
        // $data = $this->report_model->showOrders();  

        // dd($data);

        return view('supplier.report.report')->with("data",$data);

    }

    public function plant_address_listing_report(){

        $data = array();
        $data = $this->report_model->plant_address_listing_report();  

        // dd($data);

        return view('supplier.report.plant_address_list')->with("data",$data);

    }

    public function plantAddressOrderExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->plant_address_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["business_name"]) ? ($value["business_name"]).' ' : ''),
                    (isset($value["line1"]) ? ($value["line1"]).' ' : '').  (isset($value["line2"]) ? ($value["line2"]) : ''),
                    isset($value["vendorDetails"]["full_name"]) ? ($value["vendorDetails"]["full_name"]) : '',
                    isset($value["vendorDetails"]["mobile_number"]) ? ($value["vendorDetails"]["mobile_number"]) : '',
                    isset($value["vendorDetails"]["email"]) ? ($value["vendorDetails"]["email"]) : '',
                    isset($value["vendorDetails"]["plant_capacity_per_hour"]) ? ($value["vendorDetails"]["plant_capacity_per_hour"]) : '',
                    isset($value["vendorDetails"]["no_of_operation_hour"]) ? ($value["vendorDetails"]["no_of_operation_hour"]) : '',
                    isset($value["stateDetails"]["state_name"]) ? ($value["stateDetails"]["state_name"]) : '',
                    isset($value["cityDetails"]["city_name"]) ? ($value["cityDetails"]["city_name"]) : '',
                    (isset($value["billingAddressDetails"]["company_name"]) ? ($value["billingAddressDetails"]["company_name"]).' ' : '').
                        (isset($value["billingAddressDetails"]["line1"]) ? ($value["billingAddressDetails"]["line1"]).' ' : ''). 
                        (isset($value["billingAddressDetails"]["line2"]) ? ($value["billingAddressDetails"]["line2"]).' ' : '').
                        (isset($value["billingAddressStateDetails"]["state_name"]) ? ($value["billingAddressStateDetails"]["state_name"]).' ' : ''). 
                        (isset($value["billingAddressCityDetails"]["city_name"]) ? ($value["billingAddressCityDetails"]["city_name"]).' ' : ''). 
                        (isset($value["billingAddressDetails"]["pincode"]) ? ($value["billingAddressDetails"]["pincode"]).' ' : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new SupplierPlantAddressExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_plant_address_report'.$file_type);
        }
    }
    
    
    

    
    public function billing_address_listing_report(){

        $data = array();
        $data = $this->report_model->billing_address_listing_report();  

        // dd($data);

        return view('supplier.report.billing_address_list')->with("data",$data);

    }

    public function billingAddressOrderExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->billing_address_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    isset($value["company_name"]) ? $value["company_name"] : '',
                                        
                    (isset($value["line1"]) ? ($value["line1"]) : '')." ". ( isset($value["line2"]) ? ($value["line2"]) : ''),
                    isset($value["gst_number"]) ? ($value["gst_number"]) : '',
                    isset($value["stateDetails"]["state_name"]) ? ($value["stateDetails"]["state_name"]) : '',
                    isset($value["cityDetails"]["city_name"]) ? ($value["cityDetails"]["city_name"]) : '',
                    isset($value["pincode"]) ? ($value["pincode"]) : '',
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '',
                    
                ];
                $i++;
            }

            $export = new SupplierBillingAddressExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_billing_address_report'.$file_type);
        }
    }
    
    public function TM_driver_listing_report(){

        $data = array();
        $data = $this->report_model->TM_driver_listing_report();  

        // dd($data);

        return view('supplier.report.TM_driver_list')->with("data",$data);

    }

    public function TMDriverExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->TM_driver_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["driver_name"]) ? $value["driver_name"] : ''),
                                        
                    (isset($value["driver_mobile_number"]) ? ($value["driver_mobile_number"]) : ''),
                    (isset($value["driver_alt_mobile_number"]) ? ($value["driver_alt_mobile_number"]) : ''),
                    (isset($value["driver_whatsapp_number"]) ? ($value["driver_whatsapp_number"]) : ''),
                    (isset($value["addressDetails"]["business_name"]) ? ($value["addressDetails"]["business_name"]) : '').",". 
                        (isset($value["addressDetails"]["line1"]) ? ($value["addressDetails"]["line1"]) : '').",". 
                        (isset($value["addressDetails"]["line2"]) ? ($value["addressDetails"]["line2"]) : '').",". 
                        (isset($value["addressDetails"]["state_name"]) ? ($value["addressDetails"]["state_name"]) : '').",". 
                        (isset($value["addressDetails"]["city_name"]) ? ($value["addressDetails"]["city_name"]) : '').",". 
                        (isset($value["addressDetails"]["pincode"]) ? ($value["addressDetails"]["pincode"]) : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new SupplierTMDriverExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_TM_driver_report'.$file_type);
        }
    }

    public function operator_listing_report(){

        $data = array();
        $data = $this->report_model->operator_listing_report();  

        // dd($data);

        return view('supplier.report.operator_list')->with("data",$data);

    }

    public function operatorExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->operator_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["operator_name"]) ? $value["operator_name"] : ''),
                                        
                    (isset($value["operator_mobile_number"]) ? $value["operator_mobile_number"] : '-'),
                    (isset($value["operator_alt_mobile_number"]) ? $value["operator_alt_mobile_number"] : '-'),
                    (isset($value["operator_whatsapp_number"]) ? $value["operator_whatsapp_number"] : '-'),
                    (isset($value["addressDetails"]["business_name"]) ? ($value["addressDetails"]["business_name"]) : '').",". 
                        (isset($value["addressDetails"]["line1"]) ? ($value["addressDetails"]["line1"]) : '').",". 
                        (isset($value["addressDetails"]["line2"]) ? ($value["addressDetails"]["line2"]) : '').",". 
                        (isset($value["addressDetails"]["state_name"]) ? ($value["addressDetails"]["state_name"]) : '').",". 
                        (isset($value["addressDetails"]["city_name"]) ? ($value["addressDetails"]["city_name"]) : '').", ". 
                        (isset($value["addressDetails"]["pincode"]) ? ($value["addressDetails"]["pincode"]) : ''),
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new SupplierOperatorExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_operator_report'.$file_type);
        }
    }

    public function TM_listing_report(){

        $data = array();
        $category_data = $this->common_model->getTMCategory();
        $sub_category_data = $this->common_model->getTMSubCategoris();

        $data = $this->report_model->TM_listing_report();  

        $data["vehicle_categoies"] = $category_data;
        $data["subcategories"] = $sub_category_data;

        // dd($data);

        return view('supplier.report.TM_list')->with("data",$data);

    }

    public function TMListExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->TM_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    (isset($value["TM_category"]["category_name"]) ? $value["TM_category"]["category_name"] : ''),
                    (isset($value["TM_sub_category"]["sub_category_name"]) ? $value["TM_sub_category"]["sub_category_name"] : ''),
                    (isset($value["business_name"]) ? ($value["business_name"]) : '').",". 
                        (isset($value["line1"]) ? ($value["line1"]) : '').",". 
                        (isset($value["line2"]) ? ($value["line2"]) : '').",". 
                        (isset($value["state_name"]) ? ($value["state_name"]) : '').",". 
                        (isset($value["city_name"]) ? ($value["city_name"]) : '').", ". 
                        (isset($value["pincode"]) ? ($value["pincode"]) : ''),             
                    (isset($value["TM_rc_number"]) ? ($value["TM_rc_number"]) : '' ),
                    (isset($value["min_trip_price"]) ? ($value["min_trip_price"]) : '' ),
                    (isset($value["per_Cu_mtr_km"]) ? ($value["per_Cu_mtr_km"]) : '' ),
                    (isset($value["manufacturer_name"]) ? ($value["manufacturer_name"]) : '' ),
                    (isset($value["TM_model"]) ? ($value["TM_model"]) : '' ),
                    (isset($value["manufacture_year"]) ? ($value["manufacture_year"]) : '' ),
                    (isset($value["delivery_range"]) ? ($value["delivery_range"]) : '' ),
                    (isset($value["is_insurance_active"]) ? ($value["is_insurance_active"] == 'true' ? 'Yes' : 'No') : '' ),
                    (isset($value["is_gps_enabled"]) ? ($value["is_gps_enabled"] == 'true' ? 'Yes' : 'No') : '' ),
                    
                    isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : ''
                    
                ];
                $i++;
            }

            $export = new SupplierTMListExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_TM_report'.$file_type);
        }
    }


    public function CP_listing_report(){

        $data = array();

        $category_data = $this->common_model->getConcretePumpCategory();
        

        $data = $this->report_model->CP_listing_report();  

        $data["category_data"] = $category_data["data"];
        

        // dd($data);

        return view('supplier.report.CP_list')->with("data",$data);

    }

    public function CPListExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->CP_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    
                    (isset($value["business_name"]) ? ($value["business_name"]) : '').",". 
                        (isset($value["line1"]) ? ($value["line1"]) : '').",". 
                        (isset($value["line2"]) ? ($value["line2"]) : '').",". 
                        (isset($value["state_name"]) ? ($value["state_name"]) : '').",". 
                        (isset($value["city_name"]) ? ($value["city_name"]) : '').", ". 
                        (isset($value["pincode"]) ? ($value["pincode"]) : ''),             
                    (isset($value["concrete_pump_category"]["category_name"]) ? ($value["concrete_pump_category"]["category_name"]) : '' ),
                    (isset($value["company_name"]) ? ($value["company_name"]) : '' ),
                    (isset($value["concrete_pump_model"]) ? ($value["concrete_pump_model"]) : '' ),
                    (isset($value["manufacture_year"]) ? ($value["manufacture_year"]) : '' ),
                    (isset($value["serial_number"]) ? ($value["serial_number"]) : '' ),
                    (isset($value["pipe_connection"]) ? ($value["pipe_connection"]) : '' ),
                    (isset($value["concrete_pump_capacity"]) ? ($value["concrete_pump_capacity"]) : '' ),
                    (isset($value["operator_info"]["operator_name"]) ? ($value["operator_info"]["operator_name"]) : '' ),
                        
                    (isset($value["is_helper"]) ? ($value["is_helper"] == 'true' ? 'Yes' : 'No') : '' ),
                    (isset($value["transportation_charge"]) ? ($value["transportation_charge"]) : '' ),
                    (isset($value["concrete_pump_price"]) ? ($value["concrete_pump_price"]) : '' ),
                    (isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' ),
                    
                    
                    
                ];
                $i++;
            }

            $export = new SupplierCPListExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_CP_report'.$file_type);
        }
    }

    public function admix_stock_report(){

        $data = array();

        $data = $this->report_model->admix_stock_report(); 
        $admixture_brand_data = $this->common_model->getAdMixtureBrand();
        $admixture_types_data = $this->common_model->getAdmixtureTypes();

        $data["admixture_brand_data"] = $admixture_brand_data['data'];
        $data["admixture_types_data"] = $admixture_types_data['data'];

        // dd($data);

        return view('supplier.report.admix_stock')->with("data",$data);

    }

    public function admixStockExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->admix_stock_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    
                    (isset($value["address"]["business_name"]) ? ($value["address"]["business_name"]) : '').",". 
                        (isset($value["address"]["line1"]) ? ($value["address"]["line1"]) : '').",". 
                        (isset($value["address"]["line2"]) ? ($value["address"]["line2"]) : '').",". 
                        (isset($value["address"]["state_name"]) ? ($value["address"]["state_name"]) : '').",". 
                        (isset($value["address"]["city_name"]) ? ($value["address"]["city_name"]) : '').", ". 
                        (isset($value["address"]["pincode"]) ? ($value["address"]["pincode"]) : ''),             
                    (isset($value["admixture_brand"]["name"]) ? ($value["admixture_brand"]["name"]) : '' ),
                    (isset($value["admixture_category"]["category_name"]) ? ($value["admixture_category"]["category_name"].'-'. $value["admixture_category"]["admixture_type"]) : '' ),
                    (isset($value["quantity"]) ? ($value["quantity"]) : '' ),
                        
                    (isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' ),
                    
                    
                    
                ];
                $i++;
            }

            $export = new SupplierAdmixStockExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_admix_stock_report'.$file_type);
        }
    }

    public function agg_stock_report(){

        $data = array();

        $data = $this->report_model->agg_stock_report(); 
        $aggregate_source_data = $this->common_model->getAggregateSource();
        $aggregate_cat_data = $this->common_model->getAggregateSandCategory();

        if($aggregate_cat_data["status"] == 200){
            if(isset($aggregate_cat_data["data"][0]["_id"])){
                $cat_id = $aggregate_cat_data["data"][0]["_id"];

                $aggregate_sub_cat_data = $this->common_model->getAggregateSandSubCategory($cat_id);
            }
            
        }

        $data["aggregate_source_data"] = $aggregate_source_data['data'];
        $data["aggregate_sub_cat_data"] = $aggregate_sub_cat_data["data"];

        // dd($data);

        return view('supplier.report.agg_stock')->with("data",$data);

    }

    public function aggStockExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->agg_stock_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    
                    (isset($value["address"]["business_name"]) ? ($value["address"]["business_name"]) : '').",". 
                        (isset($value["address"]["line1"]) ? ($value["address"]["line1"]) : '').",". 
                        (isset($value["address"]["line2"]) ? ($value["address"]["line2"]) : '').",". 
                        (isset($value["address"]["state_name"]) ? ($value["address"]["state_name"]) : '').",". 
                        (isset($value["address"]["city_name"]) ? ($value["address"]["city_name"]) : '').", ". 
                        (isset($value["address"]["pincode"]) ? ($value["address"]["pincode"]) : ''),             
                    (isset($value["aggregate_source"]["aggregate_source_name"]) ? ($value["aggregate_source"]["aggregate_source_name"]) : '' ),
                    (isset($value["aggregate_sand_sub_category"]["sub_category_name"]) ? ($value["aggregate_sand_sub_category"]["sub_category_name"]) : '' ),
                    (isset($value["quantity"]) ? ($value["quantity"]) : '' ),
                        
                    (isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' ),
                    
                    
                    
                ];
                $i++;
            }

            $export = new SupplierAggStockExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_agg_stock_report'.$file_type);
        }
    }

    public function cement_stock_report(){

        $data = array();

        $data = $this->report_model->cement_stock_report(); 
        $cement_brand_data = $this->common_model->getCementBrand();
        $cement_grade_data = $this->common_model->getCementGrade();
        
        $data["cement_brand_data"] = $cement_brand_data["data"];
        $data["cement_grade_data"] = $cement_grade_data["data"];

        // dd($data);

        return view('supplier.report.cement_stock')->with("data",$data);

    }

    public function cementStockExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->cement_stock_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    
                    (isset($value["address"]["business_name"]) ? ($value["address"]["business_name"]) : '').",". 
                        (isset($value["address"]["line1"]) ? ($value["address"]["line1"]) : '').",". 
                        (isset($value["address"]["line2"]) ? ($value["address"]["line2"]) : '').",". 
                        (isset($value["address"]["state_name"]) ? ($value["address"]["state_name"]) : '').",". 
                        (isset($value["address"]["city_name"]) ? ($value["address"]["city_name"]) : '').", ". 
                        (isset($value["address"]["pincode"]) ? ($value["address"]["pincode"]) : ''),             
                    (isset($value["cement_brand"]["name"]) ? ($value["cement_brand"]["name"]) : '' ),
                    (isset($value["cement_grade"]["name"]) ? ($value["cement_grade"]["name"]) : '' ),
                    (isset($value["quantity"]) ? ($value["quantity"]) : '' ),
                        
                    (isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' ),
                    
                    
                    
                ];
                $i++;
            }

            $export = new SupplierCementStockExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_cement_stock_report'.$file_type);
        }
    }

    public function flyAsh_stock_report(){

        $data = array();

        $data = $this->report_model->flyAsh_stock_report(); 
        $fly_ash_source_data = $this->common_model->getFlyAshSource();

        $data["fly_ash_source_data"] = $fly_ash_source_data['data'];

        // dd($data);

        return view('supplier.report.flyash_stock')->with("data",$data);

    }

    public function flyAshStockExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->flyAsh_stock_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    
                    (isset($value["address"]["business_name"]) ? ($value["address"]["business_name"]) : '').",". 
                        (isset($value["address"]["line1"]) ? ($value["address"]["line1"]) : '').",". 
                        (isset($value["address"]["line2"]) ? ($value["address"]["line2"]) : '').",". 
                        (isset($value["address"]["state_name"]) ? ($value["address"]["state_name"]) : '').",". 
                        (isset($value["address"]["city_name"]) ? ($value["address"]["city_name"]) : '').", ". 
                        (isset($value["address"]["pincode"]) ? ($value["address"]["pincode"]) : ''),             
                    (isset($value["fly_ash_source"]["fly_ash_source_name"]) ? ($value["fly_ash_source"]["fly_ash_source_name"]) : '' ),
                    (isset($value["quantity"]) ? ($value["quantity"]) : '' ),
                        
                    (isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' ),
                    
                    
                    
                ];
                $i++;
            }

            $export = new SupplierFlyAshStockExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_flyash_stock_report'.$file_type);
        }
    }

    public function sand_stock_report(){

        $data = array();

        $data = $this->report_model->sand_stock_report(); 
        $sand_source_data = $this->common_model->getSandSource();
        
        
        $data["sand_source_data"] = $sand_source_data["data"];
        // dd($data);

        return view('supplier.report.sand_stock')->with("data",$data);

    }

    public function sandStockExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->sand_stock_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    
                    (isset($value["address"]["business_name"]) ? ($value["address"]["business_name"]) : '').",". 
                        (isset($value["address"]["line1"]) ? ($value["address"]["line1"]) : '').",". 
                        (isset($value["address"]["line2"]) ? ($value["address"]["line2"]) : '').",". 
                        (isset($value["address"]["state_name"]) ? ($value["address"]["state_name"]) : '').",". 
                        (isset($value["address"]["city_name"]) ? ($value["address"]["city_name"]) : '').", ". 
                        (isset($value["address"]["pincode"]) ? ($value["address"]["pincode"]) : ''),             
                    (isset($value["sand_source"]["sand_source_name"]) ? ($value["sand_source"]["sand_source_name"]) : '' ),
                    (isset($value["quantity"]) ? ($value["quantity"]) : '' ),
                        
                    (isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' ),
                    
                    
                    
                ];
                $i++;
            }

            $export = new SupplierSandStockExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_sand_stock_report'.$file_type);
        }
    }


    public function bank_report(){

        $data = array();

        $data = $this->report_model->bank_report(); 
        // dd($data);

        return view('supplier.report.bank_report')->with("data",$data);

    }

    public function BankdetailsExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->bank_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    
                    
                    (isset($value["bank_name"]) ? ($value["bank_name"]) : '' ),
                    (isset($value["account_holder_name"]) ? ($value["account_holder_name"]) : '' ),
                    (isset($value["account_number"]) ? ($value["account_number"]) : '' ),
                    (isset($value["ifsc"]) ? ($value["ifsc"]) : '' ),
                    (isset($value["account_type"]) ? ($value["account_type"]) : '' ),
                        
                    (isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' ),
                    
                    
                    
                ];
                $i++;
            }

            $export = new SupplierBankDetailExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_bank_detail_report'.$file_type);
        }
    }

    public function design_mix_report(){

        $data = array();

        $data = $this->report_model->design_mix_report();
        $cement_brand_data = $this->common_model->getCementBrand();
        $cement_grade_data = $this->common_model->getCementGrade();
        
        $data["cement_brand_data"] = $cement_brand_data["data"];
        $data["cement_grade_data"] = $cement_grade_data["data"]; 

        $fly_ash_source_data = $this->common_model->getFlyAshSource();

        $data["fly_ash_source_data"] = $fly_ash_source_data['data'];

        $sand_source_data = $this->common_model->getSandSource();
        
        $data["sand_source_data"] = $sand_source_data["data"];

        $admixture_brand_data = $this->common_model->getAdMixtureBrand();
        $data["admixture_brand_data"] = $admixture_brand_data['data'];

        $aggregate_source_data = $this->common_model->getAggregateSource();
        $aggregate_cat_data = $this->common_model->getAggregateSandCategory();

        if($aggregate_cat_data["status"] == 200){
            if(isset($aggregate_cat_data["data"][0]["_id"])){
                $cat_id = $aggregate_cat_data["data"][0]["_id"];

                $aggregate_sub_cat_data = $this->common_model->getAggregateSandSubCategory($cat_id);
            }
            
        }

        $data["aggregate_source_data"] = $aggregate_source_data['data'];
        $data["aggregate_sub_cat_data"] = $aggregate_sub_cat_data["data"];
        // dd($data);

        return view('supplier.report.design_mix_report')->with("data",$data);

    }

    public function DesignmixDetailExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->design_mix_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    
                    
                    (isset($value["concrete_grade_name"]) ? ($value["concrete_grade_name"]) : '' ),
                    (isset($value["cement_brand_name"]) ? ($value["cement_brand_name"]) : '' ),
                    (isset($value["cement_grade_name"]) ? ($value["cement_grade_name"]) : '' ),
                    (isset($value["cement_quantity"]) ? ($value["cement_quantity"]) : '' ),
                    (isset($value["sand_source_name"]) ? ($value["sand_source_name"]) : '' ),
                    (isset($value["sand_quantity"]) ? ($value["sand_quantity"]) : '' ),
                    (isset($value["aggregate_source_name"]) ? ($value["aggregate_source_name"]) : '' ),
                    (isset($value["aggregate1_sub_category_name"]) ? ($value["aggregate1_sub_category_name"]) : '' ),
                    (isset($value["aggregate1_quantity"]) ? ($value["aggregate1_quantity"]) : '' ),
                    (isset($value["aggregate2_sub_category_name"]) ? ($value["aggregate2_sub_category_name"]) : '' ),
                    (isset($value["aggregate2_quantity"]) ? ($value["aggregate2_quantity"]) : '' ),
                    (isset($value["fly_ash_source_name"]) ? ($value["fly_ash_source_name"]) : '' ),
                    (isset($value["fly_ash_quantity"]) ? ($value["fly_ash_quantity"]) : '' ),
                    (isset($value["water_quantity"]) ? ($value["water_quantity"]) : '' ),
                    (isset($value["selling_price"]) ? ($value["selling_price"]) : '' ),
                    (isset($value["business_name"]) ? ($value["business_name"]) : '').",". 
                        (isset($value["address_line1"]) ? ($value["address_line1"]) : '').",". 
                        (isset($value["address_line2"]) ? ($value["address_line2"]) : '').",". 
                        (isset($value["state_name"]) ? ($value["state_name"]) : '').",". 
                        (isset($value["city_name"]) ? ($value["city_name"]) : '').", ". 
                        (isset($value["pincode"]) ? ($value["pincode"]) : ''),
                    (isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' ),
                    
                    
                    
                ];
                $i++;
            }

            $export = new SupplierDesignmixExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_designmix_detail_report'.$file_type);
        }
    }

    public function order_listing_report(){

        $data = array();
        $data = $this->report_model->order_listing_report();  

        // dd($data);

        return view('supplier.report.orders_list')->with("data",$data);

    }
    
    public function product_listing_report(){

        $data = array();
        $cat_data = $this->common_model->getProductCategory();
        $data = $this->report_model->product_listing_report();  
        $data['category'] = $cat_data["data"];
        // dd($data);

        return view('supplier.report.product_list')->with("data",$data);

    }

    
    
    public function supplierOrderExport()
    {
        // dd(Request::all());
        // echo "test";exit;
        $response = $this->report_model->order_listing_report();
        // dd($response);
        $data = array();
        if(isset($response['data']) && sizeof($response['data'])>0){
            $data = $response['data'];
            $final_array = array();
            $i = 0;

            foreach($data as $k=>$value){
                // $final_array[$i] = [$v['name'],$v['contact_number'],$v['contact_person_name'],$v['city_name'],$v['state_name']];
                $final_array[$i] = [
                    $value["_id"],
                    $value["buyer_order_display_id"],
                    $value["buyer"]["full_name"],
                    $value["buyer"]["company_name"],
                    (isset($value["business_name"]) ? $value["business_name"] : ''),
                    (isset($value["quantity"]) ? $value["quantity"] : ''),
                    isset($value["unit_price"]) ? number_format($value["unit_price"],2) : 0,
                    isset($value["total_amount"]) ? number_format($value["total_amount"],2) : 0,
                    $value["order_status"],
                    (isset($value["payment_status"]) ? $value["payment_status"] : ''),
                    (isset($value["business_name"]) ? ($value["business_name"]) : '').",". 
                        (isset($value["line1"]) ? ($value["line1"]) : '').",". 
                        (isset($value["line2"]) ? ($value["line2"]) : '').",". 
                        (isset($value["state_name"]) ? ($value["state_name"]) : '').",". 
                        (isset($value["city_name"]) ? ($value["city_name"]) : '').", ". 
                        (isset($value["pincode"]) ? ($value["pincode"]) : ''),   
                    (isset($value["delivery_address"]["site_name"]) ? ($value["delivery_address"]["site_name"]) : '').",". 
                        (isset($value["delivery_address"]["address_line1"]) ? ($value["delivery_address"]["address_line1"]) : '').",". 
                        (isset($value["delivery_address"]["address_line2"]) ? ($value["delivery_address"]["address_line2"]) : '').",". 
                        (isset($value["delivery_address"]["state_name"]) ? ($value["delivery_address"]["state_name"]) : '').",". 
                        (isset($value["delivery_address"]["city_name"]) ? ($value["delivery_address"]["city_name"]) : '').", ". 
                        (isset($value["delivery_address"]["pincode"]) ? ($value["delivery_address"]["pincode"]) : ''), 
                    date('d M Y',strtotime($value["created_at"])),
                    
                    
                ];
                $i++;
            }

            $export = new SupplierOrdersExport([
                $final_array
            ]);

            $file_type = "";
            if(Request::get("file_type")){
                if(Request::get("file_type") == 'excel'){
                    $file_type = ".xlsx";
                }else if(Request::get("file_type") == 'pdf'){
                    $file_type = ".pdf";    
                }
            }
            
            return Excel::download($export,'supplier_orders'.$file_type);
        }
    }
}
