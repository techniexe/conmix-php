<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class RateContoller extends Controller
{
    //
    protected $rate_model;
    protected $address_model;
    protected $common_model;
    public function __construct(Request $request){
        
        $this->rate_model = new \App\SupplierModel\RateModel();
        $this->address_model = new \App\SupplierModel\AddressModel();
        $this->common_model = new \App\CommonAPIModel();

        
    }

    public function showCementRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->showCementRate();
        $address_data = $this->address_model->showAddressDetails();
        $cement_brand_data = $this->common_model->getCementBrand();
        $cement_grade_data = $this->common_model->getCementGrade();
        
        $data["cement_brand_data"] = $cement_brand_data["data"];
        $data["cement_grade_data"] = $cement_grade_data["data"];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.per_kr_rate.cement_rate_detail')->with("data",$data);

    }

    public function addCementRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->addCementRate();
        
        // dd($data);

        return $data;

    }

    public function editCementRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->editCementRate();
        
        // dd($data);

        return $data;

    }
    
    public function deleteCementRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->deleteCementRate();
        
        // dd($data);

        return $data;

    }
    
    
    public function showSandRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->showSandRate();
        $address_data = $this->address_model->showAddressDetails();
        $sand_source_data = $this->common_model->getSandSource();
        $sand_zone_data = $this->common_model->getSandZone();
        
        
        $data["sand_source_data"] = $sand_source_data["data"];
        $data["sand_zone_data"] = $sand_zone_data["data"];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.per_kr_rate.sand_rate_detail')->with("data",$data);

    }

    public function addSandRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->addSandRate();
        
        // dd($data);

        return $data;

    }

    public function editSandRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->editSandRate();
        
        // dd($data);

        return $data;

    }
    
    public function deleteSandRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->deleteSandRate();
        
        // dd($data);

        return $data;

    }
    
    public function showAggregateRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->showAggregateRate();
        $address_data = $this->address_model->showAddressDetails();
        $aggregate_source_data = $this->common_model->getAggregateSource();
        $aggregate_cat_data = $this->common_model->getAggregateSandCategory();

        if($aggregate_cat_data["status"] == 200){
            if(isset($aggregate_cat_data["data"][0]["_id"])){
                $cat_id = $aggregate_cat_data["data"][0]["_id"];

                $aggregate_sub_cat_data = $this->common_model->getAggregateSandSubCategory($cat_id);
            }
            
        }
        
        $data["aggregate_source_data"] = $aggregate_source_data['data'];
        $data["aggregate_sub_cat_data"] = isset($aggregate_sub_cat_data["data"]) ? $aggregate_sub_cat_data["data"] : array();
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.per_kr_rate.aggregate_rate_detail')->with("data",$data);

    }

    public function addAggregateRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->addAggregateRate();
        
        // dd($data);

        return $data;

    }

    public function editAggregateRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->editAggregateRate();
        
        // dd($data);

        return $data;

    }
    
    public function deleteAggregateRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->deleteAggregateRate();
        
        // dd($data);

        return $data;

    }
    
    
    public function showFlyashRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->showFlyashRate();
        $address_data = $this->address_model->showAddressDetails();
        $fly_ash_source_data = $this->common_model->getFlyAshSource();

        
        $data["fly_ash_source_data"] = $fly_ash_source_data['data'];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.per_kr_rate.flyash_rate_detail')->with("data",$data);

    }

    public function addFlyashRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->addFlyashRate();
        
        // dd($data);

        return $data;

    }

    public function editFlyashRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->editFlyashRate();
        
        // dd($data);

        return $data;

    }
    
    public function deleteFlyashRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->deleteFlyashRate();
        
        // dd($data);

        return $data;

    }
    
    public function showAdmixtureRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->showAdmixtureRate();
        $address_data = $this->address_model->showAddressDetails();
        $admixture_brand_data = $this->common_model->getAdMixtureBrand();
        $admixture_types_data = $this->common_model->getAdmixtureTypes();
        
        $data["admixture_brand_data"] = $admixture_brand_data['data'];
        $data["admixture_types_data"] = $admixture_types_data['data'];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.per_kr_rate.admixture_rate_detail')->with("data",$data);

    }

    public function addAdmixtureRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->addAdmixtureRate();
        
        // dd($data);

        return $data;

    }

    public function editAdmixtureRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->editAdmixtureRate();
        
        // dd($data);

        return $data;

    }
    
    public function deleteAdmixtureRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->deleteAdmixtureRate();
        
        // dd($data);

        return $data;

    }
    
    public function showWaterRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->showWaterRate();
        $address_data = $this->address_model->showAddressDetails();
        

        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.per_kr_rate.water_rate_detail')->with("data",$data);

    }

    public function addWaterRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->addWaterRate();
        
        // dd($data);

        return $data;

    }

    public function editWaterRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->editWaterRate();
        
        // dd($data);

        return $data;

    }
    
    public function deleteWaterRate(){
        // dd(Request::all());
        $data = array();
        
        $data = $this->rate_model->deleteWaterRate();
        
        // dd($data);

        return $data;

    }
}
