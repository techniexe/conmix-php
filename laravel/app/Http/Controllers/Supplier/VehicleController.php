<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class VehicleController extends Controller
{
    //
    protected $vehicle_model;
    protected $driver_model;
    protected $common_model;
    protected $address_model;
    public function __construct(){
        $this->vehicle_model = new \App\SupplierModel\VehicleModel();
        $this->address_model = new \App\SupplierModel\AddressModel();

        $this->driver_model = new \App\SupplierModel\DriverModel();
        $this->common_model = new \App\CommonAPIModel();
    }

    public function index(){
        $data = array();
        $category_data = $this->common_model->getTMCategory();
        $sub_category_data = $this->common_model->getTMSubCategoris();

        $data = $this->vehicle_model->showTM();

        $data["vehicle_categoies"] = $category_data;
        $data["subcategories"] = $sub_category_data;
       
        // dd($data);
        return view("supplier.TM.TM_detail")->with("data",$data);
    }

    public function addTMView(){

        $data = array();
        $category_data = $this->common_model->getTMCategory();
        $sub_category_data = $this->common_model->getTMSubCategoris();
        $driver_data = $this->driver_model->showDriverDetails();
        $address_data = $this->address_model->showAddressDetails();

        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();

        // $data = $this->vehicle_model->showVehicles();
        $data["vehicle_categoies"] = $category_data['data'];
        $data["subcategories"] = $sub_category_data['data'];
        $data["driver_data"] = $driver_data['data'];
        $data["address_data"] = $address_data['data'];

        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];
        // dd($data);
        return view("supplier.TM.TM_add")->with("data",$data);

    }

    public function addTM(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->addTM();

        return $data;

    }

    public function editTMView($vehicle_id){

        $data = array();
        $category_data = $this->common_model->getTMCategory();
        $sub_category_data = $this->common_model->getTMSubCategoris();
        $driver_data = $this->driver_model->showDriverDetails();
        $address_data = $this->address_model->showAddressDetails();

        $state_data = $this->common_model->getStates();
        $city_data = $this->common_model->getCities();

        $data = $this->vehicle_model->getTMDetail($vehicle_id);

        // $data = $this->vehicle_model->showVehicles();
        $data["vehicle_categoies"] = $category_data['data'];
        $data["subcategories"] = $sub_category_data['data'];
        $data["driver_data"] = $driver_data['data'];
        $data["address_data"] = $address_data['data'];
        

        // $data["data"] = json_decode(Request::post("vehicle_details"));

        $data["state_data"] = $state_data["data"];
        $data["city_data"] = $city_data["data"];
        // dd($data);
        return view("supplier.TM.TM_edit")->with("data",$data);

    }

    public function editTM(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->editTM();

        return $data;

    }

    public function getSubCatByCatId(){


        $data = $this->common_model->getTMSubCategoris();

        return $data;
    }
    
    public function getTMunAvailabiity(){


        $data = $this->vehicle_model->getTMunAvailabiity();

        // dd($data);
        if($data["status"] == 200){

            
            $suggestion_array = array();
            $final_data = array();
            
            // dd($cart_details);
            // $final_data["cart_details"] = $cart_details;

                $returnHTML = view('supplier.TM.TM_unavailable')->with('data', $data)->render();
                $returnHTMLslide = "";         

                
        

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $data;
        }

        return $data;
    }

    public function addTMunAvailabiity(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->addTMunAvailabiity();

        return $data;

    }
    
    public function editTMunAvailabiity(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->editTMunAvailabiity();

        return $data;

    }
    
    public function deleteTMunAvailabiity(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->deleteTMunAvailabiity();

        return $data;

    }
    
    public function getCPunAvailabiity(){


        $data = $this->vehicle_model->getCPunAvailabiity();

        // dd($data);
        if($data["status"] == 200){

            
            $suggestion_array = array();
            $final_data = array();
            
            // dd($cart_details);
            // $final_data["cart_details"] = $cart_details;

                $returnHTML = view('supplier.concrete_pump.CP_unavailable')->with('data', $data)->render();
                $returnHTMLslide = "";         

                
        

            return response()->json(array('status' => $data["status"], 'html'=>$returnHTML, 'slide_html'=>$returnHTMLslide));
        }else{
            // return $data;

            // return response()->json(array('status' => $data["status"], 'html'=>""));

            return $data;
        }

        return $data;
    }

    public function addCPunAvailabiity(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->addCPunAvailabiity();

        return $data;

    }
    
    public function editCPunAvailabiity(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->editCPunAvailabiity();

        return $data;

    }
    
    public function deleteCPunAvailabiity(){
        // dd(Request::all());
        $data = array();

        $data = $this->vehicle_model->deleteCPunAvailabiity();

        return $data;

    }
}
