<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SupportTicketController extends Controller
{
    //
    protected $support_model;
    protected $auth_model;
    public function __construct(){
        
        $this->support_model = new \App\SupplierModel\SupportTicketModel();
        $this->auth_model = new \App\AdminModel\AuthenticationModel();
    }

    public function index(){

        $data = array();
        // $buyer_data = $this->auth_model->showBuyers();
        // $supplier_data = $this->auth_model->showSuppliers();
        // $logistics_data = $this->auth_model->showLogistics();
        
        $data = $this->support_model->showTickets();  

        // $data["buyer_data"] = $buyer_data["data"]; 
        // $data["supplier_data"] = $supplier_data["data"]; 
        // $data["logistics_data"] = $logistics_data["data"]; 

        // dd($data);

        return view('supplier.support_ticket.support_ticket')->with("data",$data);

    }

    public function showTicketDetails($ticket_id){

        $data = array();
        $data = $this->support_model->showTicketDetails($ticket_id);  
        $ticket_messages = $this->support_model->getTicketMessages($ticket_id); 
        $data["ticket_id"] = $ticket_id;
        $data["ticket_messages"] = $ticket_messages["data"];
        // dd($data);

        return view('supplier.support_ticket.ticket_detail')->with("data",$data);

    }

    public function addTicket(){

        // dd(Request::all());
        // dd($data);
        $response = $this->support_model->addTicket();
        // dd($response);
        return $response;

    }
    
    public function replyTicket(){

        // dd(Request::all());
        // dd($data);
        $response = $this->support_model->replyTicket();

        return $response;

    }

    public function updateTicketStatus(){

        $response = $this->support_model->updateTicketStatus();

        return $response;

    }
}
