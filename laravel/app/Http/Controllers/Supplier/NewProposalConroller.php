<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class NewProposalConroller extends Controller
{
    //
    protected $proposal_model;
    protected $contact_model;
    protected $address_model;
    public function __construct(){
        
        $this->proposal_model = new \App\SupplierModel\NewProposalModel();
        $this->contact_model = new \App\SupplierModel\ContactDetailModel();
        $this->address_model = new \App\SupplierModel\AddressModel();
    }

    public function index(){

        $data = array();
        
        $data = $this->proposal_model->showProposals();
        $contact_data = $this->contact_model->showContactDetails();
        $address_data = $this->address_model->showAddressDetails();
        

        $data["contact_data"] = $contact_data["data"];
        $data["address_data"] = $address_data["data"];

        // dd($data);

        return view('supplier.proposal.proposal')->with("data",$data);

    }

    public function addProposal(){
        // dd(Request::all());
        $data = array();

        $data = $this->proposal_model->addProposal();

        return $data;
    }
}
