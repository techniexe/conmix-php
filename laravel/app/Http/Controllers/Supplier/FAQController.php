<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FAQController extends Controller
{
    //
    protected $order_model;
    public function __construct(){
        
        $this->order_model = new \App\AdminModel\OrdersModel();
    }

    public function index(){

        $data = array();
        // $data = $this->order_model->showOrders();  

        // dd($data);

        return view('supplier.faq.faq')->with("data",$data);

    }
    
    public function howItWorks(){

        $data = array();
        // $data = $this->order_model->showOrders();  

        // dd($data);

        return view('supplier.how_it_works.how_it_works')->with("data",$data);

    }
}
