<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use Request;

class SignupController extends Controller
{
    //
    protected $signup_model;
    public function __construct(Request $request){
        
        $this->signup_model = new \App\SupplierModel\SignupModel();

        
    }

    public function index(){
        // echo "Dashboard";
        
        if(session("custom_token",null)){
            return redirect('dashboard');
        }else{
            return view("supplier.Authentication.signup")->with('data',array());
        }
    }

    

    public function emailVerifier(){

        session()->forget('custom_token');
        // session()->flush();

        return view("supplier.Authentication.signup_email_verifier")->with('data',array());
    }
    
    public function emailVerifierOtp(){


        return view("supplier.Authentication.signup_email_verifier_otp")->with('data',array());
    }

    public function signupOTPSend(){

        $data = $this->signup_model->signupOTPSend();
        // dd($data);
        if($data["status"] == 200){
            // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
            return view("supplier.Authentication.signup_email_verifier_otp")->with('data',$data);
        }else{
            return view("supplier.Authentication.signup_email_verifier")->with('data',$data);
        }

    }

    public function signupOTPVerify(){

        $data = $this->signup_model->signupOTPVerify();
        // dd($data);
        if($data["status"] == 200){
            // return redirect('supplier/signup_email_verifier_otp')->with('data',$data);
            return view("supplier.Authentication.signup")->with('data',$data);
        }else{
            return view("supplier.Authentication.signup_email_verifier_otp")->with('data',$data);
        }

    }

    public function signupEmailVerify(){
        // dd(Request::all());
        $data = $this->signup_model->signupEmailVerify();
        // dd($data);
        // if($data["status"] == 200){
        //     return redirect('supplier/dashboard')->with('success',"You are registered successfully.");
        //     // return view("supplier.Authentication.signup")->with('data',$data);
        // }else{
        //     $data["details"] = Request::all();
        //     // dd($data);
        //     return view("supplier.Authentication.signup")->with('data',$data);
        // }

        return $data;
    }

    public function signupRegistration(){
        // dd(Request::all());
        $data = $this->signup_model->signupRegistration();
        // dd($data);
        // if($data["status"] == 200){
        //     return redirect('supplier/dashboard')->with('success',"You are registered successfully.");
        //     // return view("supplier.Authentication.signup")->with('data',$data);
        // }else{
        //     $data["details"] = Request::all();
        //     // dd($data);
        //     return view("supplier.Authentication.signup")->with('data',$data);
        // }

        return $data;
    }

}
