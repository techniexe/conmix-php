<?php

namespace App\Http\Middleware;

use Request;
use Closure;

class SupplierAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has("supplier_custom_token")){
            return redirect("supplier");
        }

        return $next($request);
    }
}
