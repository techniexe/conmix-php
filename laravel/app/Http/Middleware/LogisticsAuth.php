<?php

namespace App\Http\Middleware;

use Closure;

class LogisticsAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has("custom_token")){
            return redirect("supplier");
        }
        
        return $next($request);
    }
}
