<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ReviewModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }


    public function showReviews(){

        $data = array();

        if(Request::get("categoryId")){
            $data["categoryId"] = Request::get("categoryId");
        }
        
        if(Request::get("subcategoryId")){
            $data["sub_category_id"] = Request::get("subcategoryId");
        }
        
        if(Request::get("product_id")){
            $data["product_id"] = Request::get("product_id");
        }

        if(Request::get("search_term")){
            $data["search"] = Request::get("search_term");
        }
        
        if(Request::get("status")){
            $data["status"] = Request::get("status");
        }
        
        if(Request::get("review_user_id")){
            $data["user_id"] = Request::get("review_user_id");
        }
        
        if(Request::get("buyer_name")){
            $data["buyer_name"] = Request::get("buyer_name");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showProductReviews($data);
        // dd($response);
        return $response;

    }
    
    public function showFeedback(){

        $data = array();

        if(Request::get("categoryId")){
            $data["categoryId"] = Request::get("categoryId");
        }
        
        if(Request::get("subcategoryId")){
            $data["sub_category_id"] = Request::get("subcategoryId");
        }
        
        if(Request::get("product_id")){
            $data["product_id"] = Request::get("product_id");
        }

        if(Request::get("search_term")){
            $data["search"] = Request::get("search_term");
        }
        
        if(Request::get("feedback_type")){
            $data["feedback_type"] = Request::get("feedback_type");
        }
        
        if(Request::get("review_user_id")){
            $data["user_id"] = Request::get("review_user_id");
        }
        
        if(Request::get("buyer_name")){
            $data["buyer_name"] = Request::get("buyer_name");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showFeedback($data);
        // dd($response);
        return $response;

    }

    public function editReview(){

        $data = array(
            "status" => Request::post("status"),
            "review_text" => Request::post("review_text"),
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("peoduct_review_id")
        );
        // dd($data);
        $response = $this->api_model->editProductReview($data);

        return $response;

    }

}
