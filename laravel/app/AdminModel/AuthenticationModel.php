<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Request;
use App\ApiConfig;
use App\Http\Controllers\Admin\AdminController;

class AuthenticationModel extends BaseModel
{
    //
    protected $api_model;

    protected $final_response;

    public function __construct(){
        $this->api_model = new ApiModel();

        $this->final_response = array(
            "status" => null,
            "data" => null,
            "error" => null
        );
    }
    
    public function login(){
       
        if(Request::post('login_btn')){
            $email_mobile = Request::post('email_mobile');
            $password = Request::post('password');
            
            $data = array(
                "email_mobile" => $email_mobile,
                "password" => $password
            );

            $auth_resposnse = $this->api_model->getSessionToken();
            
            $this->final_response = $auth_resposnse;
            if($auth_resposnse["status"] == 200){
                //Success Response
                $session_token = $auth_resposnse["data"]["token"];

                $data["session_token"] = $session_token;

                $login_response = $this->api_model->login($data);
                
                $this->final_response = $login_response;
                
                if($this->final_response["status"] == 200){
                    $custom_token = $this->final_response["data"]["customToken"];
                    
                    session()->put("custom_token",$custom_token);

                    $admin_profile = $this->api_model->getAdminProfile();
                    

                    if($admin_profile["status"] == 200){
                        $admin_profile["data"]["rights"] = self::admin_rights($admin_profile["data"]["admin_type"]);
                        session()->put("profile_details",$admin_profile["data"]);    
                    }

                    // dd(session('profile_details', null));
                    
                }

                // return $login_response;
                // $request->session()->put('session_token', ''.$session_token);

                // $globla = session('session_token', null);
                // $value = $request->session()->get('session_token', null);
                // echo "Global::".$globla.'<br>';
                // echo "value::".$value.'<br>';
                // echo $session_token;exit;
            }else{
                //Error Response
            }
            
            return $this->final_response;
            
        }
    }

    public function logout(){
        session()->forget('custom_token');
        session()->forget('profile_details');
        // session()->flush();
    }

    public static function admin_rights($admin_role){

        $rights = ApiConfig::ADMIN_RIGHTS_TRUE;
        
        if($admin_role == AdminController::ADMIN_ROLES_BASE['superAdmin']){
            
        }
        
        if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_manager']){
            $rights['admin_user_action'] = false;
            $rights['admin_user_add'] = false;
            $rights['admin_user_edit'] = false;
            $rights['admin_user_delete'] = false;
            $rights['admin_user_active_inactive'] = false;
            $rights['profile_edit'] = false;
            $rights['profile_change_password'] = false;
        }
        
        if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_sales_marketing']){
            $rights = ApiConfig::ADMIN_RIGHTS_FALSE;

            $rights['ledger_view'] = true;
            $rights['buyer_order_view'] = true;
            $rights['supplier_order_view'] = true;
            $rights['dashboard_view'] = true;
            $rights['report_view'] = true;
            $rights['inventory_report_view'] = true;
            $rights['sales_report_view'] = true;
            $rights['review_feedback_view'] = true;
            $rights['review_view'] = true;
            $rights['review_edit'] = true;
            $rights['feedback_view'] = true;
        }
        
        if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_accounts']){
            $rights = ApiConfig::ADMIN_RIGHTS_FALSE;

            $rights['ledger_view'] = true;
            $rights['buyer_order_view'] = true;
            $rights['supplier_order_view'] = true;
            $rights['dashboard_view'] = true;
            $rights['report_view'] = true;
            $rights['inventory_report_view'] = true;
            $rights['sales_report_view'] = true;
        }
        
        if($admin_role == AdminController::ADMIN_ROLES_BASE['admin_customer_care_grievances']){
            $rights = ApiConfig::ADMIN_RIGHTS_FALSE;

            $rights['ledger_view'] = true;
            $rights['buyer_order_view'] = true;
            $rights['supplier_order_view'] = true;
            $rights['customer_support_view'] = true;
            $rights['inventory_report_view'] = true;
            $rights['customer_support_reply'] = true;
            $rights['customer_support_resolve'] = true;
        }
        
        if($admin_role == AdminController::ADMIN_ROLES_BASE['it_manager']){
            
            $rights['profile_edit'] = false;
            $rights['profile_change_password'] = false;

        }

        return $rights;

    }
// ........................Admin User Listing Operations............................

    public function adminUserListing(){
        
        $data = array();

        if(Request::get("name")){
            $data["search"] = Request::get("name");
        }

        if(Request::get("mobile_no")){
            $data["search"] = Request::get("mobile_no");
        }

        if(Request::get("email")){
            $data["search"] = Request::get("email");
        }

        if(Request::get("admin_type")){
            $data["admin_type"] = Request::get("admin_type");
        }
        
        if(Request::get("is_active")){

            $data["is_active"] = Request::get("is_active");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }

        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        
        $response = $this->api_model->getAdminUserList($data);

        return $response;
    }

    public function addAdminUser(Request $request){
        if(Request::post("email")){

            $data = array(
                "full_name" => "".Request::post("name"),
                "email" => "".Request::post("email"),
                "mobile_number" => "".Request::post("mobile"),
                "admin_type" => "".Request::post("admin_type"),
                "password" => "".Request::post("password")
            );

            $response = $this->api_model->addAdminUser($data);

            return $response;
        }

    }

    public function requestOTP(){
        // dd(Request::all());
        $data = array();
        if(Request::get("event_type")){
            $event_type = Request::get("event_type");
            $data["event_type"] = $event_type;

            if($event_type == "vendor_verification"){

                $data["vendor_id"] = Request::get("supplier_id");

            }else if($event_type == "vendor_rejection"){

                $data["vendor_id"] = Request::get("supplier_id");

            }else if($event_type == "plant_block"){
                
                $data["address_id"] = Request::get("address_id");

            }else if($event_type == "plant_unblock"){
                
                $data["address_id"] = Request::get("address_id");

            }else if($event_type == "plant_verification"){
                
                $data["address_id"] = Request::get("address_id");

            }else if($event_type == "plant_rejection"){
                
                $data["address_id"] = Request::get("address_id");

            }else if($event_type == "account_unblock"){
                
                $data["vendor_id"] = Request::get("supplier_id");

            }else if($event_type == "account_block"){
                
                $data["vendor_id"] = Request::get("supplier_id");

            }else if($event_type == "edit_TM_category"){
                
                $data["category_name"] = Request::get("category_name");
                $data["TMCategoryId"] = Request::get("cat_id");

            }else if($event_type == "edit_TM_subcategory"){
                
                $data["sub_category_name"] = Request::get("sub_category_name");
                $data["TMSubCategoryId"] = Request::get("TM_sub_cat_id");

            }else if($event_type == "edit_agg_category"){
                
                $data["agg_cat_name"] = Request::get("category_name");
                $data["categoryId"] = Request::get("product_cat_id");

            }else if($event_type == "edit_agg_sub_category"){
                
                $data["agg_sub_cat_name"] = Request::get("sub_category_name");
                $data["subCategoryId"] = Request::get("product_sub_cat_id");

            }else if($event_type == "edit_cg"){
                
                $data["cg_name"] = Request::get("grade_name");
                $data["cgId"] = Request::get("grade_id");

            }else if($event_type == "edit_cement_brand"){
                
                $data["cement_brand_name"] = Request::get("brand_name");
                $data["cementBrandId"] = Request::get("brand_id");

            }else if($event_type == "edit_admix_brand"){
                
                $data["admix_brand_name"] = Request::get("brand_name");
                $data["admixBrandId"] = Request::get("brand_id");

            }else if($event_type == "edit_pump_cat"){
                
                $data["pump_cat_name"] = Request::get("category_name");
                $data["concretePumpCategoryId"] = Request::get("cat_id");

            }else if($event_type == "edit_admix_cat"){
                
                $data["admix_cat_name"] = Request::get("category_name");
                $data["admixCatId"] = Request::get("admixture_type_id");
                $data["admix_brand_id"] = Request::get("brand_id");
                $data["admixture_type"] = Request::get("admixture_type");

            }else if($event_type == "edit_cement_grade"){
                
                $data["cement_grade_name"] = Request::get("cement_grade_name");
                $data["cementGradeId"] = Request::get("grade_id");

            }else if($event_type == "edit_agg_source"){
                
                $data["aggregate_source_name"] = Request::get("aggregate_source_name");
                $data["agg_source_id"] = Request::get("aggregate_source_id");

            }else if($event_type == "edit_flyAsh_source"){
                
                $data["fly_ash_source_name"] = Request::get("flyash_source_name");
                $data["flyAsh_source_id"] = Request::get("flyash_source_id");

            }else if($event_type == "edit_sand_source"){
                
                $data["sand_source_name"] = Request::get("sand_source_name");
                $data["sand_source_id"] = Request::get("sand_source_id");

            }
        }


        if(Request::get('password')){
            $data["password"] =  Request::post('password');
        }
        
        if(Request::get('old_password')){
            $data["old_password"] =  Request::post('old_password');
        }

        // dd($data);  
        $response = $this->api_model->requestOTP($data);

        return $response;
    }
    
    public function requestOTPForEditProfile(){

        $data = array(
            "requestOTP" => "yes"
        );

        if(Request::get("new_mobile_no")){
            $data["new_mobile_no"] = Request::get("new_mobile_no");
        }
        
        if(Request::get("new_email")){
            $data["new_email"] = Request::get("new_email");
        }
        
        if(Request::get("old_email")){
            $data["old_email"] = Request::get("old_email");
        }
        
        if(Request::get("old_mobile_no")){
            $data["old_mobile_no"] = Request::get("old_mobile_no");
        }
        // dd($data);
        $response = $this->api_model->requestOTPForEditProfile($data);

        return $response;
    }

    public function activeInActiveUser(Request $request){
        $request_data = Request::post("data");
        // dd($request_data);
        $is_active = 'true';
        // if($request_data["is_active"] == 'true'){
            
        //     $is_active = 'false';
        //     // dd($request_data);
        // }
        
        if($request_data["is_active"] == 0){
            
            $is_active = 'false';
            // dd($request_data);
        }
       
        $data = array(
            "authentication_code" => "".$request_data["authentication_code"],
            "is_active" => $is_active,
            "id" => $request_data["_id"]
        );        
        
        $response = $this->api_model->editAdminUserDetails($data);

        return $response;
    }

    public function editUserDetails(Request $request){

        $data = array(
            "full_name" => "".Request::post("name"),
            "authentication_code" => "".Request::post("authentication_code"),
            "id" => Request::post("_id")
        );

        if(Request::post("password")){
            $data["password"] = "".Request::post("password");
        }

        $response = $this->api_model->editAdminUserDetails($data);

        return $response;

    }

    public function deleteUser(Request $request){

        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "id" => Request::post("_id")
        );
        
        $response = $this->api_model->deleteAdminUser($data);

        return $response;
    }


// ..........................Admin User Listing Operations Over............................

//...........................Logistics Listing Operations...................................

    public function showLogistics(){

        $data = array();

        if(Request::get("name")){
            $data["search"] = Request::get("name");
        }

        if(Request::get("mobile_no")){
            $data["search"] = Request::get("mobile_no");
        }
        
        if(Request::get("email")){
            $data["search"] = Request::get("email");
        }

        if(Request::get("user_id")){
            $data["search"] = Request::get("user_id");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }

        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $response = $this->api_model->getAllLogistics($data);

        return $response;

    }

//...........................Logistics Listing Operations Over...................................

//...........................Suppliers Listing Operations ...................................

public function showSuppliers(){

    $data = array();

    if(Request::get("name")){
        $data["search"] = Request::get("name");
    }  
    
    if(Request::get("name")){
        $data["company_name"] = Request::get("name");
    }     

    if(Request::get("mobile_no")){
        $data["search"] = Request::get("mobile_no");
    }
    
    if(Request::get("mobile_no")){
        $data["mobile_no"] = Request::get("mobile_no");
    }
    
    if(Request::get("email")){
        $data["search"] = Request::get("email");
    }
    
    if(Request::get("email")){
        $data["email"] = Request::get("email");
    }

    if(Request::get("user_id")){
        $data["user_id"] = Request::get("user_id");
    }
    
    if(Request::get("pan_number")){
        $data["pan_number"] = Request::get("pan_number");
    }
    
    if(Request::get("company_type")){
        $data["company_type"] = Request::get("company_type");
    }
    
    if(Request::get("verified_by_admin")){
        $data["verified_by_admin"] = Request::get("verified_by_admin");
    }

    if(Request::get("before")){
        $data["before"] = Request::get("before");
    }

    if(Request::get("after")){
        $data["after"] = Request::get("after");
    }

    $response = $this->api_model->getAllSuppliers($data);

    return $response;

}

public function showSuppliersPlants(){

    $data = array(
        "supplier_id" => Request::get("supplier_id")
    );

    $response = $this->api_model->showSuppliersPlants($data);

    return $response;

}

public function supplierVerifyByAdmin(){

    $data = array(
        "supplier_id" => Request::post('supplier_id'),
        "authentication_code" => Request::post('authentication_code')
    );

    // if(Request::get("verified_by_admin")){
    //     $data["verified_by_admin"] = Request::get("verified_by_admin");
    // }

    if(Request::post('verified_by_admin')){
        if(Request::post('verified_by_admin')){
            $data["verified_by_admin"] = 'true';
        }else{
            $data["verified_by_admin"] = 'false';
        }
        
    }else{
        $data["verified_by_admin"] = 'false';
    }
    
    // dd($data);
    $response = $this->api_model->supplierVerifyByAdmin($data);

    return $response;

}

public function supplierPlantVerifyByAdmin(){

    $data = array(
        "address_id" => Request::post('address_id'),
        "vendor_id" => Request::post('supplier_id'),
        "authentication_code" => Request::post('authentication_code')
    );

    // if(Request::get("verified_by_admin")){
    //     $data["verified_by_admin"] = Request::get("verified_by_admin");
    // }

    if(Request::post('is_verified')){
        if(Request::post('is_verified')){
            $data["is_verified"] = 'true';
        }else{
            $data["is_verified"] = 'false';
        }
        
    }else{
        $data["is_verified"] = 'false';
    }
    

    $response = $this->api_model->supplierPlantVerifyByAdmin($data);

    return $response;

}

public function supplierBlockedByAdmin(){

    $data = array(
        "supplier_id" => Request::post('supplier_id'),
        "authentication_code" => Request::post('authentication_code')
    );

    // if(Request::get("verified_by_admin")){
    //     $data["verified_by_admin"] = Request::get("verified_by_admin");
    // }

    if(Request::post('blocked_by_admin')){
        if(Request::post('blocked_by_admin')){
            $data["is_blocked"] = 'true';
        }else{
            $data["is_blocked"] = 'false';
        }
        
    }else{
        $data["is_blocked"] = 'false';
    }
    

    $response = $this->api_model->supplierBlockedByAdmin($data);

    return $response;

}

public function supplierPlantBlockedByAdmin(){

    $data = array(
        "address_id" => Request::post('address_id'),
        "vendor_id" => Request::post('supplier_id'),
        "authentication_code" => Request::post('authentication_code')
    );

    // if(Request::get("verified_by_admin")){
    //     $data["verified_by_admin"] = Request::get("verified_by_admin");
    // }

    if(Request::post('blocked_by_admin')){
        if(Request::post('blocked_by_admin')){
            $data["is_blocked"] = 'true';
        }else{
            $data["is_blocked"] = 'false';
        }
        
    }else{
        $data["is_blocked"] = 'false';
    }
    

    $response = $this->api_model->supplierPlantBlockedByAdmin($data);

    return $response;

}

public function supplierRejectedByAdmin(){

    $data = array(
        "supplier_id" => Request::post('supplier_id'),
        "reject_reason" => Request::post('reject_reason'),
        "is_accepted" => 'false',
        "authentication_code" => Request::post('authentication_code')
    );

    // if(Request::get("verified_by_admin")){
    //     $data["verified_by_admin"] = Request::get("verified_by_admin");
    // }
    
        // dd($data);
    $response = $this->api_model->supplierRejectedByAdmin($data);

    return $response;

}

public function supplierplantRejectedByAdmin(){

    $data = array(
        "address_id" => Request::post('address_id'),
        "vendor_id" => Request::post('vendor_id'),
        "reject_reason" => Request::post('reject_reason'),
        "is_accepted" => 'false',
        "authentication_code" => Request::post('authentication_code')
    );

    // if(Request::get("verified_by_admin")){
    //     $data["verified_by_admin"] = Request::get("verified_by_admin");
    // }
    
        // dd($data);
    $response = $this->api_model->supplierplantRejectedByAdmin($data);

    return $response;

}

public function logisticsVerifyByAdmin(){

    $data = array(
        "logistics_id" => Request::post('logistics_id')
    );

    // if(Request::get("verified_by_admin")){
    //     $data["verified_by_admin"] = Request::get("verified_by_admin");
    // }

    if(Request::post('verified_by_admin')){
        if(Request::post('verified_by_admin')){
            $data["verified_by_admin"] = 'true';
        }else{
            $data["verified_by_admin"] = 'false';
        }
        
    }else{
        $data["verified_by_admin"] = 'false';
    }
    

    $response = $this->api_model->logisticsVerifyByAdmin($data);

    return $response;

}

//...........................Suppliers Listing Operations Over ...................................

//...........................Buyers Listing Operations ...................................

public function showBuyers(){

    $data = array();

    if(Request::get("name")){
        $data["search"] = Request::get("name");
    }

    if(Request::get("company_name")){
        $data["company_name"] = Request::get("company_name");
    }
    
    if(Request::get("account_type")){
        $data["account_type"] = Request::get("account_type");
    }

    if(Request::get("mobile_no")){
        $data["search"] = Request::get("mobile_no");
    }
    
    if(Request::get("email")){
        $data["search"] = Request::get("email");
    }

    if(Request::get("user_id")){
        $data["user_id"] = Request::get("user_id");
    }

    if(Request::get("before")){
        $data["before"] = Request::get("before");
    }

    if(Request::get("after")){
        $data["after"] = Request::get("after");
    }

    $response = $this->api_model->getAllBuyers($data);

    return $response;

}

//...........................Buyers Listing Operations Over ...................................

}
