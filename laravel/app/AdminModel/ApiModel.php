<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\ApiConfig;
use App\BaseModel;

class ApiModel extends BaseModel
{
    // 
    public function getSessionToken(){

        $url_path = "".ApiConfig::SESSION_TOKEN_URL;
        $params = array(
            "accessToken"=> ApiConfig::ACCESS_TOKEN,
            "registrationToken"=> ApiConfig::REGISTRATION_TOKEN
        );
        
        $auth_resposnse = $this->requestPOST($url_path,$params);
        // dd($auth_resposnse);
        return $auth_resposnse;
    }

    public function login($data){
        // dd($data);
        $url_path = "".sprintf(ApiConfig::ADMIN_LOGIN_URL,$data["email_mobile"]);
        $params = array(
            "password"=> $data["password"]
        );
        $this->setTokenHeader($data["session_token"]);
        $login_resposnse = $this->requestPOST($url_path,$params);
        // dd($login_resposnse);
        return $login_resposnse;

    }

    public function forgotPassOTPSendViaEmail($data){

        $url_path = sprintf(ApiConfig::ADMIN_FORGOT_PASS_OTP_SEND_VIA_EMAIL_URL,$data["email_mobile"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }
    
    public function forgotPassOTPSendViaMobile($data){

        $url_path = sprintf(ApiConfig::ADMIN_FORGOT_PASS_OTP_SEND_VIA_MOBILE_URL,$data["email_mobile"]);
        $params = $data;
        
        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestGET($url_path,$params);

        return $auth_resposnse;

    }

    public function forgotPassOTPVerify($data){

        $url_path = sprintf(ApiConfig::ADMIN_FORGOT_PASS_OTP_VERIFY_URL,$data["email_mobile"],$data["code"]);
        $params = $data;

        $this->setTokenHeader($data["session_token"]);

        $auth_resposnse = $this->requestPOST($url_path,$params);

        return $auth_resposnse;

    }   


    public function getAdminUserList($data){

        $url_path = ApiConfig::ADMIN_USER_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;
    }

    public function addAdminUser($data){

        $url_path = ApiConfig::ADMIN_USER_ADD_URL;
        $params = $data;
        
        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;
    }

    public function requestOTP($data){
        $url_path = ApiConfig::ADMIN_REQUEST_OTP_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function requestOTPForEditProfile($data){
        $url_path = ApiConfig::ADMIN_REQUEST_OTP_FOR_EDIT_PROFILE_URL;
        $params = $data;
        // dd($params);
        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editAdminUserDetails($data){

        $url_path = sprintf(ApiConfig::ADMIN_USER_EDIT_URL,$data["id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;
    }

    public function deleteAdminUser($data){

        $url_path = sprintf(ApiConfig::ADMIN_USER_DELETE_URL,$data["id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);

        return $response;
    }

    public function getAllLogistics($data){

        $url_path = ApiConfig::ADMIN_LOGISTICS_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function getAllSuppliers($data){

        $url_path = ApiConfig::ADMIN_SUPPLIERS_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function showSuppliersPlants($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_PLANTS_LISTING_URL,$data["supplier_id"]);
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function supplierVerifyByAdmin($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_VERIFY_URL,$data["supplier_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }
    
    public function supplierPlantVerifyByAdmin($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_PLANT_VERIFY_URL,$data["address_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }
    
    public function supplierBlockedByAdmin($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_BLOCK_URL,$data["supplier_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }
    
    public function supplierPlantBlockedByAdmin($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_PLANT_BLOCK_URL,$data["address_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }
    
    public function supplierRejectedByAdmin($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_REJECTED_URL,$data["supplier_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }
    
    public function supplierplantRejectedByAdmin($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_PLANT_REJECTED_URL,$data["address_id"]);
        $params = $data;
        // dd($url_path);
        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }
    
    public function logisticsVerifyByAdmin($data){

        $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_VERIFY_URL,$data["logistics_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }

    public function getAllBuyers($data){

        $url_path = ApiConfig::ADMIN_BUYERS_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

// Vehicle Module APIs

    public function showTMCategory($data){

        $url_path = ApiConfig::ADMIN_TM_CATEGORY_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addTMCategory($data){

        $url_path = ApiConfig::ADMIN_TM_CATEGORY_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        
        return $response;

    }

    public function editTMCategory($data){

        $url_path = sprintf(ApiConfig::ADMIN_TM_CATEGORY_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }

    public function getTMSubCategoris($data){

        $url_path = ApiConfig::ADMIN_TM_SUB_CATEGORY_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addTMSubCategory($data){

        $url_path = sprintf(ApiConfig::ADMIN_TM_SUB_CATEGORY_ADD_URL,$data["cat_id"]);
        $params = $data;
        // dd($url_path);
        $response = $this->requestPOST($url_path,$params);
        
        return $response;

    }

    public function editTMSubCategory($data){

        $url_path = sprintf(ApiConfig::ADMIN_TM_SUB_CATEGORY_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }


    public function showPumpCategory($data){

        $url_path = ApiConfig::ADMIN_PUMP_CATEGORY_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addPumpCategory($data){

        $url_path = ApiConfig::ADMIN_PUMP_CATEGORY_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        
        return $response;

    }

    public function editPumpCategory($data){

        $url_path = sprintf(ApiConfig::ADMIN_PUMP_CATEGORY_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }


    public function getVehicles($data){

        $url_path = ApiConfig::ADMIN_VEHICLE_LISTING_URL;
        $params = $data;
        // dd($url_path);
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getVehiclesPMRM($data){

        $url_path = ApiConfig::ADMIN_VEHICLE_PM_KM_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addVehiclePMKM($data){

        $url_path = ApiConfig::ADMIN_VEHICLE_PM_KM_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        
        return $response;

    }
    
    public function editVehiclePMKM($data){

        $url_path = sprintf(ApiConfig::ADMIN_VEHICLE_PM_KM_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }

    public function getPMKMRateByStateId($data){

        $url_path = sprintf(ApiConfig::GET_PM_KM_RATE_BY_STATE_ID,$data["state_id"]);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    // Vehicle Module Over.........................................

    // Region Module Start.........................................
    
    public function showRegionCountries($data){
        $url_path = ApiConfig::ADMIN_REGION_COUNTRY_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;
    }


    public function addRegionCountry($data){

        $url_path = ApiConfig::ADMIN_REGION_COUNTRY_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editRegionCountry($data){

        $url_path = sprintf(ApiConfig::ADMIN_REGION_COUNTRY_EDIT_URL,$data["_id"]);
        // dd($data);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function deleteRegionCountry($data){

        $url_path = sprintf(ApiConfig::ADMIN_REGION_COUNTRY_DELETE_URL,$data["_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }

    public function showRegionStates($data){

        $url_path = ApiConfig::ADMIN_REGION_STATE_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addRegionState($data){

        $url_path = ApiConfig::ADMIN_REGION_STATE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editRegionState($data){

        $url_path = sprintf(ApiConfig::ADMIN_REGION_STATE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function deleteRegionState($data){
        $url_path = sprintf(ApiConfig::ADMIN_REGION_STATE_DELETE_URL,$data["_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;
    }

    public function showRegionCities($data){

        $url_path = ApiConfig::ADMIN_REGION_CITY_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }

    public function addRegionCity($data){

        $url_path = ApiConfig::ADMIN_REGION_CITY_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editRegionCity($data){

        $url_path = sprintf(ApiConfig::ADMIN_REGION_CITY_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function deleteRegionCity($data){
        $url_path = sprintf(ApiConfig::ADMIN_REGION_CITY_DELETE_URL,$data["_id"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;
    }

    // Region Module Over.........................................

    // Product Module Start.........................................

    public function showProductCategories($data){

        $url_path = ApiConfig::ADMIN_PRODUCT_CATEGORY_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addProductCategory($data,$files){
        // dd(public_path('assets/admin/images/about-pic.jpg'));
        $url_path = ApiConfig::ADMIN_PRODUCT_CATEGORY_ADD_URL;
        $params = $data;

        // $response = $this->requestPOST_WithFile($url_path,$params,$files);
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function editProductCategory($data,$files){

        $url_path = sprintf(ApiConfig::ADMIN_PRODUCT_CATEGORY_EDIT_URL,$data["_id"]);
        $params = $data;

        if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
            $response = $this->requestPOST_WithFile($url_path,$params,$files);
        }else{
            $response = $this->requestPATCH_WithFile($url_path,$params,$files);
        }
        // dd($response);
        return $response;

    }

    public function showProductSubCategories($data){

        $url_path = sprintf(ApiConfig::ADMIN_PRODUCT_SUB_CATEGORY_LISTING_URL,$data["categoryId"]);
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }


    public function addProductSubCategory($data,$files){
        // dd(public_path('assets/admin/images/about-pic.jpg'));
        
        $url_path = sprintf(ApiConfig::ADMIN_PRODUCT_SUB_CATEGORY_ADD_URL,$data["categoryId"]);
        
        
        $params = $data;

        $response = $this->requestPOST_WithFile($url_path,$params,$files);
        
        return $response;

    }


    public function editProductSubCategory($data,$files){
        // dd($data["_id"]);
        if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
            $url_path = sprintf(ApiConfig::ADMIN_PRODUCT_SUB_CATEGORY_EDIT_URL,'edit/'.$data["_id"]);
        }else{
            $url_path = sprintf(ApiConfig::ADMIN_PRODUCT_SUB_CATEGORY_EDIT_URL,$data["_id"]);
        }
        $params = $data;
        // dd($url_path);
        if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
            $response = $this->requestPOST_WithFile($url_path,$params,$files);
        }else{
            $response = $this->requestPATCH_WithFile($url_path,$params,$files);
        }
        // dd($response);
        return $response;

    }


    public function showProducts($data){

        $url_path = ApiConfig::ADMIN_PRODUCTS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function editProduct($data){

        $url_path = sprintf(ApiConfig::ADMIN_PRODUCTS_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function designmix_detail($data){

        $url_path = sprintf(ApiConfig::ADMIN_PRODUCTS_DETAIL_URL,$data["grade_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }

    public function addAggregateSource($data){

        $url_path = ApiConfig::ADMIN_AGGREGATE_SOURCE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editAggregateSource($data){

        $url_path = sprintf(ApiConfig::ADMIN_AGGREGATE_SOURCE_EDIT_URL,$data["_id"]);
        $params = $data;
        
        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteAggregateSource($data){

        $url_path = sprintf(ApiConfig::ADMIN_AGGREGATE_SOURCE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }

    public function showAggregateSource($data){

        $url_path = ApiConfig::ADMIN_AGGREGATE_SOURCE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }



    public function addSandSource($data){

        $url_path = ApiConfig::ADMIN_SAND_SOURCE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editSandSource($data){

        $url_path = sprintf(ApiConfig::ADMIN_SAND_SOURCE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteSandSource($data){

        $url_path = sprintf(ApiConfig::ADMIN_SAND_SOURCE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }

    public function showSandSource($data){

        $url_path = ApiConfig::ADMIN_SAND_SOURCE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function addSandZone($data){

        $url_path = ApiConfig::ADMIN_SAND_ZONE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editSandZone($data){

        $url_path = sprintf(ApiConfig::ADMIN_SAND_ZONE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteSandZone($data){

        $url_path = sprintf(ApiConfig::ADMIN_SAND_ZONE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }

    public function showSandZone($data){

        $url_path = ApiConfig::ADMIN_SAND_ZONE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function addFlyashSource($data){

        $url_path = ApiConfig::ADMIN_FLYASH_SOURCE_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editFlyashSource($data){

        $url_path = sprintf(ApiConfig::ADMIN_FLYASH_SOURCE_EDIT_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function deleteFlyashSource($data){

        $url_path = sprintf(ApiConfig::ADMIN_FLYASH_SOURCE_DELETE_URL,$data["_id"]);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }

    public function showFlyashSource($data){

        $url_path = ApiConfig::ADMIN_FLYASH_SOURCE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }



    public function showGstSalb($data){

        $url_path = ApiConfig::ADMIN_GST_SLAB_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addGstSalb($data){

        $url_path = ApiConfig::ADMIN_GST_SLAB_ADD_URL;
        $params = $data;

        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editGstSalb($data){

        $url_path = sprintf(ApiConfig::ADMIN_GST_SLAB_EDIT_URL,$data['_id']);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    
    public function showMarginRate($data){

        $url_path = ApiConfig::ADMIN_MARGIN_RATE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addMarginSlab($data){

        $url_path = ApiConfig::ADMIN_MARGIN_RATE_ADD_URL;
        $params = $data;
        // dd(json_encode($params));
        $response = $this->requestPOST($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function editMarginSlab($data){

        $url_path = sprintf(ApiConfig::ADMIN_MARGIN_RATE_EDIT_URL,$data['_id']);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    // Product Module Over.........................................
    
    // Email Template Module Start.........................................

    public function showEmailTemplates($data){

        $url_path = ApiConfig::ADMIN_EMAIL_TEMPLATE_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }

    public function addEmailTemplates($data,$files){

        $url_path = sprintf(ApiConfig::ADMIN_EMAIL_TEMPLATE_ADD_URL,$data['templateType']);
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }
    
    public function editEmailTemplates($data){

        $url_path = sprintf(ApiConfig::ADMIN_EMAIL_TEMPLATE_EDIT_URL,$data['templateType']);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function addEmailAttachments($data,$files){

        $url_path = sprintf(ApiConfig::ADMIN_EMAIL_TEMPLATE_ADD_ATTACHMENT_URL,$data['templateType']);
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function getEmailTemplate($data){

        $url_path = sprintf(ApiConfig::ADMIN_EMAIL_TEMPLATE_GET_URL,$data['templateType']);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }

    public function deleteEmailAttachment($data){

        $url_path = sprintf(ApiConfig::ADMIN_EMAIL_TEMPLATE_DELETE_ATTACHMENT_URL,$data['templateType']);
        $params = $data;

        $response = $this->requestDELETE($url_path,$params);
        // dd($response);
        return $response;

    }


    // Email Template Module Over.........................................
    // Product Reviews Module Over.........................................

    public function showProductReviews($data){

        $url_path = ApiConfig::ADMIN_PRODUCT_REVIEWS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function showFeedback($data){

        $url_path = ApiConfig::ADMIN_FEEDBACK_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function editProductReview($data){

        $url_path = sprintf(ApiConfig::ADMIN_PRODUCT_REVIEWS_EDIT_URL,$data['_id']);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }


    // Product Reviews Module Over.........................................

    // Access Log Module Start.........................................

    public function showEmailAccessLog($data){

        $url_path = ApiConfig::ADMIN_EMAIL_ACCESS_LOG_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function resendEmail($data,$files){

        $url_path = ApiConfig::ADMIN_EMAIL_RESEND_EMAIL_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function showActionLogs($data){

        $url_path = ApiConfig::ADMIN_ACTION_LOG_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    // Access Log Module Over.........................................

    // Ledger Module Over.........................................

    public function showOrders($data){

        $url_path = ApiConfig::ADMIN_ORDERS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function showSuppliersOrder($data){

        $url_path = ApiConfig::ADMIN_SUPPLIERS_ORDERS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }
    
    
    
    
    public function showLogisticsOrder($data){

        $url_path = ApiConfig::ADMIN_LOGISTICS_ORDERS_LISTING_URL;
        // dd($url_path);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function updateOrderStatus($data){

        if(isset($data['vendor_order_status'])){
            $url_path = sprintf(ApiConfig::ADMIN_VENDOR_ORDERS_STATUS_UPDATE_URL,$data['_id']);
        }else{
            $url_path = sprintf(ApiConfig::ADMIN_ORDERS_STATUS_UPDATE_URL,$data['_id']);
        }
        
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }


    public function getOrderDetails($order_id){

        $url_path = sprintf(ApiConfig::ADMIN_ORDERS_DETAILS_URL,$order_id);
        $params = array();

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;


    }

    public function updateOrderItemStatus($data){

        $url_path = sprintf(ApiConfig::ADMIN_ORDERS_ITEM_STATUS_URL,$data['_id']);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function getInvoiceDetails($data){

        $url_path = ApiConfig::ADMIN_ORDERS_INVOICE_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getItemTrackeDetails($data){

        $url_path = ApiConfig::ADMIN_ORDERS_ITEM_TRACK_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getTransactionList($data){

        $url_path = ApiConfig::ADMIN_TRANSACTION_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    // Ledger Module Over.........................................

    // Support Ticket Module Start.........................................

    public function showSupportTickets($data){

        $url_path = ApiConfig::ADMIN_SUPPORT_TICKET_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function addSupportTicket($data,$files){

        $url_path = ApiConfig::ADMIN_SUPPORT_TICKET_ADD_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function showTicketDetails($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPORT_TICKET_DETAIL_URL,$data["ticket_id"]);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function replySupportTicket($data,$files){

        $url_path = ApiConfig::ADMIN_SUPPORT_TICKET_REPLY_URL;
        $params = $data;

        $response = $this->requestPOST_WithMultipleFile_with_mimetype($url_path,$params,$files);
        // dd($response);
        return $response;

    }

    public function updateTicketStatus($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPORT_TICKET_STATUS_UPDATE_URL,$data["ticket_id"]);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function getTicketMessages($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPORT_TICKET_MESSAGE_URL,$data["ticket_id"]);
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }

    // Support Ticket Module Over.........................................
    // Proposal Module Start.........................................

    public function showProposals($data){

        $url_path = ApiConfig::ADMIN_PROPOSAL_LISTING_URL;
        $params = $data;

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    // Proposal Module Over.........................................

    // Profile Module Start.........................................

    public function getAdminProfile(){

        $url_path = ApiConfig::ADMIN_PROFILE_DETAIL_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }

    public function editAdminProfile($data){

        $url_path = ApiConfig::ADMIN_PROFILE_UPDATE_URL;
        $params = $data;
        
        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function changePassword($data){

        $url_path = ApiConfig::ADMIN_CHANGE_PASSWORD_URL;
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    public function updateNotiStatus($data){

        $url_path = sprintf(ApiConfig::ADMIN_NOTIFICATION_STATUS_UPDATE_URL,$data['_id']);
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function forgotPass($data){

        $url_path = ApiConfig::ADMIN_FORGOT_PASSWORD_URL;
        $params = $data;

        $response = $this->requestPATCH($url_path,$params);
        // dd($response);
        return $response;

    }

    // Profile Module Over.........................................

    // Dashboard Module Start.........................................

    public function getDashboardDetails(){

        $url_path = ApiConfig::ADMIN_DASHBOARD_DETAILS_URL;
        $params = array();

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getMonthlyChartDetails($data){

        $url_path = ApiConfig::ADMIN_MONTHLY_CHART_URL;
        $params = $data;
        
        // if(isset($data["user_type"])){
        //     $url_path .= "&user_type=".$data["user_type"];
        // }
        
        // if(isset($data["year"])){
        //     $url_path .= "&year=".$data["year"];
        // }
        // dd($url_path);
        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }
    
    public function getDayChartDetails($data){

        $url_path = ApiConfig::ADMIN_DAILY_CHART_URL;
        $params = $data;
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getBuyerLatestOrders(){

        $url_path = ApiConfig::ADMIN_BUYER_LATEST_ORDER_URL;
        $params = array();
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getSuppliersLatestOrders(){

        $url_path = ApiConfig::ADMIN_SUPPLIER_LATEST_ORDER_URL;
        $params = array();
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getLogisticsLatestOrders(){

        $url_path = ApiConfig::ADMIN_LOGISTICS_LATEST_ORDER_URL;
        $params = array();
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getUnverifiedProducts(){

        $url_path = ApiConfig::ADMIN_UNVERIFIED_PRODUCT_URL;
        $params = array();
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }
    
    public function getProductDetails($product_id){

        $url_path = sprintf(ApiConfig::ADMIN_PRODUCT_DETAIL_URL,$product_id);
        $params = array();
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }


    public function showLogisticsOrderDetail($order_id){

        $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_ORDERS_DETAIL_URL,$order_id);
        $params = array();
        
        $response = $this->requestGET($url_path,$params);
        
        return $response;


    }
    
    public function showSupplierOrderDetail($order_id){
        // dd($order_id);
        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_ORDER_DETAIL_URL,$order_id);
        $params = array();
        
        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;


    }

    public function updateSupplierBillStatus($data){

        
        if($data["bill_type"] == 'invoice'){
            $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_ORDER_BILL_STATUS_UPDATE_URL,$data["order_id"]);
        }else if($data["bill_type"] == 'credit_notes'){
            $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_ORDER_CREDIT_BILL_STATUS_UPDATE_URL,$data["order_id"]);
        }else if($data["bill_type"] == 'debit_notes'){
            $url_path = sprintf(ApiConfig::ADMIN_SUPPLIERS_ORDER_DEBIT_BILL_STATUS_UPDATE_URL,$data["order_id"]);
        }
        $params = $data;
        
        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }
    
    public function updateLogisticsBillStatus($data){

        if($data["bill_type"] == 'invoice'){
            $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_ORDER_BILL_STATUS_UPDATE_URL,$data["order_id"]);
        }else if($data["bill_type"] == 'credit_notes'){
            $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_ORDER_CREDIT_BILL_STATUS_UPDATE_URL,$data["order_id"]);
        }else if($data["bill_type"] == 'debit_notes'){
            $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_ORDER_DEBIT_BILL_STATUS_UPDATE_URL,$data["order_id"]);
        }
        
        $params = $data;
        
        $response = $this->requestPATCH($url_path,$params);
        
        return $response;

    }

    public function trackOrderItem($data){

        $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_BUYER_ORDER_TRACK_URL,$data["item_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }

    public function trackingDetails($data){

        $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_BUYER_ORDER_ITEM_TRACK_URL,$data["item_id"],$data["tracking_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }
    
    public function CPtrackingDetails($data){

        $url_path = sprintf(ApiConfig::ADMIN_BUYER_ORDER_CP_TRACK_URL,$data["order_item_part_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }
    
    public function SupplierCPtrackingDetails($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIER_ORDER_CP_TRACK_URL,$data["order_item_part_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }
    
    public function supplierTrackingDetails($data){

        $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_SUPPLIER_ORDER_ITEM_TRACK_URL,$data["item_id"],$data["tracking_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }
    
    public function logisticsTrackingDetails($data){

        $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_LOGISTICS_ORDER_ITEM_TRACK_URL,$data["item_id"],$data["tracking_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;
    
    }

    // Dashboard Module Over.........................................

    // Report Module Start.........................................


    public function admix_brand_report($data){

        $url_path = ApiConfig::ADMIN_ADMIX_BRAND_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function admix_types_report($data){

        $url_path = ApiConfig::ADMIN_ADMIX_TYPES_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function agg_sand_cat_report($data){

        $url_path = ApiConfig::ADMIN_AGGR_SAND_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function agg_sand_subcategory_report($data){

        $url_path = ApiConfig::ADMIN_AGGR_SAND_SUB_CAT_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function agg_source_report($data){

        $url_path = ApiConfig::ADMIN_AGGR_SOURCE_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function buyer_listing_report($data){

        $url_path = ApiConfig::ADMIN_BUYER_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function cement_brand_report($data){

        $url_path = ApiConfig::ADMIN_CEMENT_BRAND_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function cement_grade_report($data){

        $url_path = ApiConfig::ADMIN_CEMENT_GRADE_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function concrete_grade_report($data){

        $url_path = ApiConfig::ADMIN_CONCRETE_GRADE_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function concrete_pump_category_report($data){

        $url_path = ApiConfig::ADMIN_CONCRETE_PUMP_CATEGORY_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    
    public function coupon_report($data){

        $url_path = ApiConfig::ADMIN_COUPON_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function support_ticket_report($data){

        $url_path = ApiConfig::ADMIN_SUPPORT_TICKET_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function flyAsh_source_report($data){

        $url_path = ApiConfig::ADMIN_FLYASH_SOURCE_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function gst_slab_report($data){

        $url_path = ApiConfig::ADMIN_GST_SLAB_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function margin_rate_report($data){

        $url_path = ApiConfig::ADMIN_MARGIN_RATE_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function sand_source_report($data){

        $url_path = ApiConfig::ADMIN_SAND_SOURCE_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function supplier_listing_report($data){

        $url_path = ApiConfig::ADMIN_SUPPLIER_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function TM_category_report($data){

        $url_path = ApiConfig::ADMIN_TM_CAT_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function TM_sub_category_report($data){

        $url_path = ApiConfig::ADMIN_TM_SUB_CAT_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function logistics_listing_report($data){

        $url_path = ApiConfig::ADMIN_LOGISTICS_LISTING_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function buyer_orders_report($data){

        $url_path = ApiConfig::ADMIN_BUYER_ORDERS_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function buyer_orders_detail_report($data){

        $url_path = sprintf(ApiConfig::ADMIN_BUYER_ORDERS_DETAIL_REPORT_URL,$data["_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function supplier_orders_detail_report($data){

        $url_path = sprintf(ApiConfig::ADMIN_SUPPLIER_ORDERS_DETAIL_URL,$data["_id"],$data["supplier_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function logistics_orders_detail_report($data){

        $url_path = sprintf(ApiConfig::ADMIN_LOGISTICS_ORDERS_DETAILS_URL,$data["_id"],$data["logistic_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function supplier_orders_report($data){

        $url_path = ApiConfig::ADMIN_SUPPLIER_ORDERS_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function logistics_orders_report($data){

        $url_path = ApiConfig::ADMIN_LOGISTICS_ORDERS_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function designmix_listing_report($data){

        $url_path = ApiConfig::ADMIN_DESIGN_MIX_LIST_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function designmix_detail_report($data){

        $url_path = sprintf(ApiConfig::ADMIN_DESIGN_MIX_DETAIL_REPORT_URL,$data["grade_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function vehicle_listing_report($data){

        $url_path = ApiConfig::ADMIN_VEHICLE_LIST_REPORT_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }

    // Report Module Over.........................................


    // .......................................................Conmix APIS.........................................

    

    public function getConcreteGrades($data){

        $url_path = ApiConfig::ADMIN_GET_CONCRETE_GRADE_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    
    
    public function addConcreteGrade($data){

        $url_path = ApiConfig::ADMIN_ADD_CONCRETE_GRADE_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestPOST($url_path,$params);
    
        return $resposnse;

    }
    
    public function editConcreteGrade($data){

        $url_path = sprintf(ApiConfig::ADMIN_EDIT_CONCRETE_GRADE_URL,$data["_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestPATCH($url_path,$params);
    
        return $resposnse;

    }
    
    


    public function getCementBrand($data){

        $url_path = ApiConfig::ADMIN_GET_CEMENT_BRAND_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    // dd($resposnse);
        return $resposnse;

    }

    public function addCementBrand($data,$files){

        $url_path = ApiConfig::ADMIN_ADD_CEMENT_BRAND_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
    
        return $response;

    }
    
    public function editCementBrand($data,$files){

        $url_path = sprintf(ApiConfig::ADMIN_EDIT_CEMENT_BRAND_URL,$data["_id"]);
        $params = $data;
        // dd($params);
        // $this->setTokenHeader($data["session_token"]);
        if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
            $response = $this->requestPOST_WithFile($url_path,$params,$files);
        }else{
            $response = $this->requestPATCH_WithFile($url_path,$params,$files);
        }
    
        return $response;

    }
    
    
    public function getAddMixtureBrand($data){

        $url_path = ApiConfig::ADMIN_GET_ADMIXTURE_BRAND_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    public function getAddMixtureCatTypes($data){

        $url_path = ApiConfig::ADMIN_GET_ADMIXTURE_CAT_TYPES_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }

    public function addAddMixtureBrand($data,$files){

        $url_path = ApiConfig::ADMIN_ADD_ADMIXTURE_BRAND_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
    
        return $response;

    }
    
    public function editAddMixtureBrand($data,$files){

        $url_path = sprintf(ApiConfig::ADMIN_EDIT_ADMIXTURE_BRAND_URL,$data["_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
            $response = $this->requestPOST_WithFile($url_path,$params,$files);
        }else{
            $response = $this->requestPATCH_WithFile($url_path,$params,$files);
        }
    
        return $response;

    }
    
    public function getAddMixtureTypes($data){

        $url_path = ApiConfig::ADMIN_GET_ADMIXTURE_TYPES_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    // dd($resposnse);
        return $resposnse;

    }

    public function addAddMixtureTypes($data,$files){

        $url_path = ApiConfig::ADMIN_ADD_ADMIXTURE_TYPES_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
    
        return $response;

    }
    
    public function editAddMixtureTypes($data,$files){

        $url_path = sprintf(ApiConfig::ADMIN_EDIT_ADMIXTURE_TYPES_URL,$data["_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestPATCH($url_path,$params,$files);
    
        return $response;

    }



    public function getCementGrade($data){

        $url_path = ApiConfig::ADMIN_GET_CEMENT_GRADE_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    

    public function addCementGrade($data,$files){

        $url_path = ApiConfig::ADMIN_ADD_CEMENT_GRADE_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
    
        return $response;

    }
    
    public function editCementGrade($data,$files){

        $url_path = sprintf(ApiConfig::ADMIN_EDIT_CEMENT_GRADE_URL,$data["_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        // if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
        //     $response = $this->requestPOST_WithFile($url_path,$params,$files);
        // }else{
            $response = $this->requestPATCH($url_path,$params,$files);
        // }
    
        return $response;

    }
    
    public function getPaymentMethod($data){

        $url_path = ApiConfig::ADMIN_GET_PAYMENT_METHOD_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    

    public function addPaymentMethod($data,$files){

        $url_path = ApiConfig::ADMIN_ADD_PAYMENT_METHOD_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestPOST_WithMultipleFile($url_path,$params,$files);
    
        return $response;

    }
    
    public function editPaymentMethod($data,$files){

        $url_path = sprintf(ApiConfig::ADMIN_EDIT_PAYMENT_METHOD_URL,$data["_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestPATCH_WithFile($url_path,$params,$files);
    
        return $response;

    }
    
    


    public function showcoupons($data){

        $url_path = ApiConfig::ADMIN_GET_COUPONS_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $resposnse = $this->requestGET($url_path,$params);
    
        return $resposnse;

    }
    
    

    public function addCoupon($data){

        $url_path = ApiConfig::ADMIN_ADD_COUPONS_URL;
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestPOST($url_path,$params);
    
        return $response;

    }

    public function editCoupon($data){

        $url_path = sprintf(ApiConfig::ADMIN_EDIT_COUPONS_URL,$data["_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
        // if(strpos(env('API_BASE_URL'),env('API_PORT')) != false){
        //     $response = $this->requestPATCH_WithFile($url_path,$params);
        // }else{
            $response = $this->requestPATCH($url_path,$params);
        // }
        
        // dd($response);
        return $response;

    }
    
    public function couponDelete($data){

        $url_path = sprintf(ApiConfig::ADMIN_DELETE_COUPONS_URL,$data["_id"]);
        $params = $data;
        // dd($url_path);
        // $this->setTokenHeader($data["session_token"]);
    
        $response = $this->requestDELETE($url_path,$params);
    
        return $response;

    }


    

    // .......................................................Conmix APIS.........................................

    
}
