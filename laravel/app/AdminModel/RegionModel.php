<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class RegionModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showCountries(){

        $data = array();

        if(Request::get("country_name")){
            $data["country_name"] = Request::get("country_name");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showRegionCountries($data);
        // dd($response);
        return $response;

    }

    public function addCountry(){

        $data = array(
            "country_id" => Request::post("country_id"),
            "country_name" => Request::post("country_name"),
            "country_code" => Request::post("country_code")
        );
        // dd($data);
        $response = $this->api_model->addRegionCountry($data);

        return $response;
    }

    public function editCountry(){

        $data = array(
            "country_name" => Request::post("country_name"),
            "country_code" => Request::post("country_code"),
            "_id" => Request::post("country_original_id")
        );
        // dd($data);
        $response = $this->api_model->editRegionCountry($data);

        return $response;
    }

    public function deleteCountry(){

        $data = array(
            "_id" => Request::post("_id")
        );
        // dd($data);
        $response = $this->api_model->deleteRegionCountry($data);

        return $response;

    }

    public function showStates($is_page = 'false'){

        $data = array();

        if(Request::get("country_code")){
            $data["country_code"] = Request::get("country_code");
        }
        
        if(Request::get("state_name")){
            $data["state_name"] = Request::get("state_name");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        if($is_page == 'true'){
            $data["is_page"] = $is_page;
        }

        // dd($data);
        $response = $this->api_model->showRegionStates($data);
        // dd($response);
        return $response;

    }

    public function addState(){

        $data = array(
            "state_name" => Request::post("state_name"),
            "country_id" => Request::post("country_id"),
            "country_name" => Request::post("country_name"),
            "country_code" => Request::post("country_code")
        );
        // dd($data);
        $response = $this->api_model->addRegionState($data);

        return $response;
    }

    public function editState(){

        $data = array(
            "state_name" => Request::post("state_name"),
            "country_id" => Request::post("country_id"),
            "country_name" => Request::post("country_name"),
            "country_code" => Request::post("country_code"),
            "_id" => Request::post("state_original_id") 
        );
        

        if(Request::post("authentication_code") ){
            $data["authentication_code"] = Request::post("authentication_code");
        }
        // dd($data);
        $response = $this->api_model->editRegionState($data);

        return $response;


    }
    public function deleteState(){

        $data = array(
            "_id" => Request::post("_id")
        );

        if(Request::post("authentication_code") ){
            $data["authentication_code"] = Request::post("authentication_code");
        }
        // dd($data);
        $response = $this->api_model->deleteRegionState($data);

        return $response;

    }

    public function showCities($is_page = 'false'){

        $data = array();

        if(Request::get("state_id")){
            $data["state_id"] = Request::get("state_id");
        }
        
        if(Request::get("country_code")){
            $data["country_code"] = Request::get("country_code");
        }
        
        if(Request::get("city_name")){
            $data["city_name"] = Request::get("city_name");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        if($is_page == 'true'){
            $data["is_page"] = $is_page;
        }

        // dd($data);
        $response = $this->api_model->showRegionCities($data);
        // dd($response);
        return $response;

    }

    public function addCity(){

        $data = array(
            "state_id" => Request::post("state_id"),
            "city_name" => Request::post("city_name"),
            "location" => array(
                "coordinates" => array(
                    Request::post("us2_lon"),
                    Request::post("us2_lat")                   
                )
            )
        );

        // dd(json_encode($data));
        $response = $this->api_model->addRegionCity($data);

        return $response;
    }

    public function editCity(){

        $data = array(
            "city_name" => Request::post("city_name"),
            "location" => array(
                "coordinates" => array(
                    Request::post("us2_lon"),
                    Request::post("us2_lat")
                )
                ),
            "_id" => Request::post("city_original_id")
        );

        if(Request::post("authentication_code") ){
            $data["authentication_code"] = Request::post("authentication_code");
        }
        // dd($data);
        $response = $this->api_model->editRegionCity($data);

        return $response;


    }

    public function deleteCity(){

        $data = array(
            "_id" => Request::post("_id")
        );

        if(Request::post("authentication_code") ){
            $data["authentication_code"] = Request::post("authentication_code");
        }

        // dd($data);
        $response = $this->api_model->deleteRegionCity($data);

        return $response;

    }
}
