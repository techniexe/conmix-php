<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class EmailTemplateModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showTemplates(){

        $data = array();

         // dd($data);
         $response = $this->api_model->showEmailTemplates($data);
         // dd($response);
         return $response;

    }

    public function addEmailTemplate(){

        $header_array = array();
        if(Request::post('header_name')){
            $count = 0;
            $headers_key = Request::post('header_name');
            $headers_value = Request::post('header_name');
            for($i = 0;$i<count(Request::post('header_name'));$i++){
                $header = array(
                    "header_name" => $headers_key[$i],
                    "header_value" => $headers_value[$i]
                );

                array_push($header_array,$header);
            }

        }

        $data = array(
            "html" => "".Request::post('html'),
            "from_email" => "".Request::post('from_email'),
            "templateType" => "".Request::post('templateType'),
            "headers" => json_encode($header_array)

        );

        if(Request::post('cc')){
            $data["cc"] = Request::post('cc');
        }
        
        if(Request::post('bcc')){
            $data["bcc"] = Request::post('bcc');
        }
        
        if(Request::post('subject')){
            $data["subject"] = Request::post('subject');
        }
        
        if(Request::post('from_name')){
            $data["from_name"] = Request::post('from_name');
        }

        $files = array();

        if(Request::file('attachments')){

            $images = Request::file('attachments');
            foreach($images as $image){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image
                ));

            }            

        }

        //  dd($files);
         $response = $this->api_model->addEmailTemplates($data,$files);
         // dd($response);
         return $response;

    }

    public function getEmailTemplate($is_post = true){

        $data = array();

        if($is_post){
            $data["templateType"] = Request::post('templateType');
        }else{
            $data["templateType"] = Request::get('templateType');
        }
        

        // dd($data);
        $response = $this->api_model->getEmailTemplate($data);
        // dd($response);
        return $response;

    }


    public function editEmailTemplate(){

        $header_array = array();
        if(Request::post('header_name')){
            $count = 0;
            $headers_key = Request::post('header_name');
            $headers_value = Request::post('header_value');
            for($i = 0;$i<count(Request::post('header_name'));$i++){
                $header = array(
                    "header_name" => $headers_key[$i],
                    "header_value" => $headers_value[$i]
                );

                array_push($header_array,$header);
            }

        }

        $data = array(
            "html" => "".Request::post('html'),
            "from_email" => "".Request::post('from_email'),
            "templateType" => "".Request::post('email_template_type'),
            "headers" => json_encode($header_array)

        );


        if(Request::post('cc')){
            $data["cc"] = Request::post('cc');
        }
        
        if(Request::post('bcc')){
            $data["bcc"] = Request::post('bcc');
        }
        
        if(Request::post('subject')){
            $data["subject"] = Request::post('subject');
        }
        
        if(Request::post('from_name')){
            $data["from_name"] = Request::post('from_name');
        }

        $files = array();

        if(Request::file('attachments')){

            $images = Request::file('attachments');
            foreach($images as $image){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image
                ));

            }            

        }

        //  dd($data);
         $response = $this->api_model->editEmailTemplates($data);

         if($response["status"] == 200){

            if($files){
                $data = array(
                    "templateType" => "".Request::post('email_template_type')
                 );
    
                $response = $this->api_model->addEmailAttachments($data,$files);


                
            }

             
         }
        //  dd($response);
         return $response;

    }

    public function deleteEmailAttachment(){

        $attachment_ids_array = array();

        if(Request::post("template_id")){

            array_push($attachment_ids_array,Request::post("template_id"));

        }

        $data = array(
            "templateType" => Request::post("template_type"),
            "attachment_ids" => $attachment_ids_array,
        );

        

        // dd($data);
        $response = $this->api_model->deleteEmailAttachment($data);
        // dd($response);
        return $response;

    }

}
