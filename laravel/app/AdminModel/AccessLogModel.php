<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class AccessLogModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showEmailAccessLog(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("from_email")){
            $data["from_email"] = Request::get("from_email");
        }
        
        if(Request::get("to_email")){
            $data["to_email"] = Request::get("to_email");
        }

        if(Request::get("subject")){
            $data["subject"] = Request::get("subject");
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showEmailAccessLog($data);
        // dd($response);
        return $response;

    }

    public function emailResend(){

        $header_array = array();
        if(Request::post('header_name')){
            $count = 0;
            $headers_key = Request::post('header_name');
            $headers_value = Request::post('header_name');
            for($i = 0;$i<count(Request::post('header_name'));$i++){
                $header = array(
                    "header_name" => $headers_key[$i],
                    "header_value" => $headers_value[$i]
                );

                array_push($header_array,$header);
            }

        }

        $data = array(
            "html" => "".Request::post('html'),
            "subject" => "".Request::post('subject'),
            "cc" => "".Request::post('cc'),
            "to_email" => "".Request::post('cc'),
            "from_email" => "".Request::post('from_email'),
            "from_name" => "".Request::post('from_name'),
            "headers" => json_encode($header_array)

        );

        if(Request::post('bcc')){
            $data["bcc"] = Request::post('bcc');
        }

        $files = array();

        if(Request::file('attachments')){

            $images = Request::file('attachments');
            foreach($images as $image){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image
                ));

            }

        }

        //  dd($files);
         $response = $this->api_model->resendEmail($data,$files);
         // dd($response);
         return $response;

    }

    public function showActionLogs(){
        // dd(Request::all());
        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("user_type")){
            $data["user_type"] = Request::get("user_type");
        }
        
        if(Request::get("access_type")){
            $data["access_type"] = Request::get("access_type");
        }
        
        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::get("start_date")));
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::get("end_date")));
        }

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showActionLogs($data);
        // dd($response);
        return $response;

    }
}
