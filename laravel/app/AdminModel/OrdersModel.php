<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class OrdersModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showOrders(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_status")){
            $data["order_status"] = Request::get("order_status");
        }
        
        if(Request::get("payment_status")){
            $data["payment_status"] = Request::get("payment_status");
        }

        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }     
        
        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }     
        

        if(Request::get("order_id")){
            $data["order_id"] = Request::get("order_id");
        }
        
        if(Request::get("buyer_id")){
            $data["buyer_order_id"] = Request::get("buyer_id");
        }
        
        if(Request::get("buyer_name")){
            $data["buyer_name"] = Request::get("buyer_name");
        }
        
        if(Request::get("year")){
            $data["year"] = Request::get("year");
        }
        
        if(Request::get("date")){

            $date = date('Y-m-d', strtotime(Request::post("date"))).'T00:00:00.000Z';

            $data["date"] = $date;
        }
        
        if(Request::get("month")){
            $month = date("M", strtotime(Request::get("month")));
            $year = date("Y", strtotime(Request::get("month")));          
            

            $data["month"] = $month;
            $data["year"] = $year;
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showOrders($data);
        // dd($response);
        return $response;

    }
    
    

    public function updateStatus(){

        $data = array(
            
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("order_id")
        );

        if(Request::post("order_status")){
            $data["order_status"] = Request::post("order_status");
        }
        
        if(Request::post("vendor_order_status")){
            $data["order_status"] = Request::post("vendor_order_status");
        }

        if(Request::post("admin_remarks")){
            $data["admin_remarks"] = Request::post("admin_remarks");
        }
        
        if(Request::post("vendor_order_status")){
            $data["vendor_order_status"] = Request::post("vendor_order_status");
        }
        
        if(Request::post("payment_status")){
            $data["payment_status"] = Request::post("payment_status");
        }

        // dd($data);
        $response = $this->api_model->updateOrderStatus($data);

        return $response;

    }
    
    public function updateItemStatus(){

        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("order_id")
        );

        if(Request::post("order_status")){
            $data["order_status"] = Request::post("order_status");
        }
        
        if(Request::post("payment_status")){
            $data["payment_status"] = Request::post("payment_status");
        }
        
        if(Request::post("admin_remarks")){
            $data["admin_remarks"] = Request::post("admin_remarks");
        }
        
        if(Request::post("order_item_status")){
            $data["order_item_status"] = Request::post("order_item_status");
            $data["_id"] = Request::post("item_id");
        }

        // dd($data);
        $response = $this->api_model->updateOrderItemStatus($data);

        return $response;

    }

    public function orderDetails($order_id){

        $data = array();

       // dd($data);
        $response = $this->api_model->getOrderDetails($order_id);
        // dd($response);
        return $response;

    }

    public function showInvoice($order_id){

        $data = array(
            "order_id" => $order_id
        );


        // dd($data);
         $response = $this->api_model->getInvoiceDetails($data);
         // dd($response);
         return $response;

    }
    
    public function showItemTrack($item_id){

        $data = array(
            "item_id" => $item_id
        );


        // dd($data);
         $response = $this->api_model->getItemTrackeDetails($data);
         // dd($response);
         return $response;

    }

    public function showTransactions(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_id")){
            $data["order_id"] = Request::get("order_id");
        }
        
        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }
        
        if(Request::get("transaction_status")){
            $data["transaction_status"] = Request::get("transaction_status");
        }
        
        if(Request::get("payment_gateway")){
            $data["payment_gateway"] = Request::get("payment_gateway");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
         $response = $this->api_model->getTransactionList($data);
         // dd($response);
         return $response;

    }

    public function showLogisticsOrderDetail($order_id){

        $data = array(
            "_id" => $order_id
        );

        
        // dd($data);
         $response = $this->api_model->showLogisticsOrderDetail($order_id);
         // dd($response);
         return $response;

    }
    
    //Suppliers Orders...............................................................

    public function showLogisticsOrder(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_status")){
            $data["order_status"] = Request::get("order_status");
        }
        
        if(Request::get("payment_status")){
            $data["payment_status"] = Request::get("payment_status");
        }

        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }       
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showLogisticsOrder($data);
        // dd($response);
        return $response;

    }
    
    public function showSuppliersOrder(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_status")){
            $data["order_status"] = Request::get("order_status");
        }
        
        if(Request::get("payment_status")){
            $data["payment_status"] = Request::get("payment_status");
        }

        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }       

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }     
        

        if(Request::get("order_id")){
            $data["order_id"] = Request::get("order_id");
        }
        
        if(Request::get("buyer_id")){
            $data["buyer_id"] = Request::get("buyer_id");
        }
        
        if(Request::get("year")){
            $data["year"] = Request::get("year");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("date")){

            $date = date('Y-m-d', strtotime(Request::post("date"))).'T00:00:00.000Z';

            $data["date"] = $date;
        }
        
        if(Request::get("month")){
            $month = date("M", strtotime(Request::get("month")));
            $year = date("Y", strtotime(Request::get("month")));          
            

            $data["month"] = $month;
            $data["year"] = $year;
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showSuppliersOrder($data);
        // dd($response);
        return $response;

    }

    public function showSupplierOrderDetail($order_id){

        $data = array(
            "_id" => $order_id
        );

        
        // dd($data);
         $response = $this->api_model->showSupplierOrderDetail($order_id);
         // dd($response);
         return $response;

    }

    public function updateSupplierBillStatus(){

        $data = array();

        if(Request::get("bill_type") == 'invoice'){

            $data = array(
                "order_id" => Request::get("order_id"),
                "bill_id" => Request::get("bill_id"),
                "bill_type" => Request::get("bill_type")
            );
    
            $bill_status = Request::get("bill_status");
    
            if($bill_status == 1){
    
                $data["bill_status"] = 'ACCEPTED';
    
            }else if($bill_status == 2){
    
                $data["bill_status"] = 'REJECTED';
    
                if(Request::get("bill_reject_reason")){
                    $data["bill_reject_reason"] = Request::get("bill_reject_reason");
                }
    
            }        

        }else if(Request::get("bill_type") == 'credit_notes'){


            $data = array(
                "order_id" => Request::get("order_id"),
                "credit_note_id" => Request::get("bill_id"),
                "bill_type" => Request::get("bill_type")
            );
    
            $bill_status = Request::get("bill_status");
    
            if($bill_status == 1){
    
                $data["credit_note_status"] = 'ACCEPTED';
    
            }else if($bill_status == 2){
    
                $data["credit_note_status"] = 'REJECTED';
    
                if(Request::get("bill_reject_reason")){
                    $data["credit_note_reject_reason"] = Request::get("bill_reject_reason");
                }
    
            }        

        }else if(Request::get("bill_type") == 'debit_notes'){

            $data = array(
                "order_id" => Request::get("order_id"),
                "debit_note_id" => Request::get("bill_id"),
                "bill_type" => Request::get("bill_type")
            );
    
            $bill_status = Request::get("bill_status");
    
            if($bill_status == 1){
    
                $data["debit_note_status"] = 'ACCEPTED';
    
            }else if($bill_status == 2){
    
                $data["debit_note_status"] = 'REJECTED';
    
                if(Request::get("bill_reject_reason")){
                    $data["debit_note_reject_reason"] = Request::get("bill_reject_reason");
                }
    
            }        

        }

        
        // dd($data);
         $response = $this->api_model->updateSupplierBillStatus($data);
         // dd($response);
         return $response;

    }
    
    
    public function updateLogisticsBillStatus(){

        $data = array();

        if(Request::get("bill_type") == 'invoice'){

            $data = array(
                "order_id" => Request::get("order_id"),
                "bill_id" => Request::get("bill_id"),
                "bill_type" => Request::get("bill_type")
            );
    
            $bill_status = Request::get("bill_status");
    
            if($bill_status == 1){
    
                $data["bill_status"] = 'ACCEPTED';
    
            }else if($bill_status == 2){
    
                $data["bill_status"] = 'REJECTED';
    
                if(Request::get("bill_reject_reason")){
                    $data["bill_reject_reason"] = Request::get("bill_reject_reason");
                }
    
            }        

        }else if(Request::get("bill_type") == 'credit_notes'){


            $data = array(
                "order_id" => Request::get("order_id"),
                "credit_note_id" => Request::get("bill_id"),
                "bill_type" => Request::get("bill_type")
            );
    
            $bill_status = Request::get("bill_status");
    
            if($bill_status == 1){
    
                $data["credit_note_status"] = 'ACCEPTED';
    
            }else if($bill_status == 2){
    
                $data["credit_note_status"] = 'REJECTED';
    
                if(Request::get("bill_reject_reason")){
                    $data["credit_note_reject_reason"] = Request::get("bill_reject_reason");
                }
    
            }        

        }else if(Request::get("bill_type") == 'debit_notes'){

            $data = array(
                "order_id" => Request::get("order_id"),
                "debit_note_id" => Request::get("bill_id"),
                "bill_type" => Request::get("bill_type")
            );
    
            $bill_status = Request::get("bill_status");
    
            if($bill_status == 1){
    
                $data["debit_note_status"] = 'ACCEPTED';
    
            }else if($bill_status == 2){
    
                $data["debit_note_status"] = 'REJECTED';
    
                if(Request::get("bill_reject_reason")){
                    $data["debit_note_reject_reason"] = Request::get("bill_reject_reason");
                }
    
            }        

        }
        

        
        // dd($data);
         $response = $this->api_model->updateLogisticsBillStatus($data);
         // dd($response);
         return $response;

    }

    public function trackOrderItem($item_id){

        $data = array(
            "item_id" => $item_id
        );

       
        

        $response = $this->api_model->trackOrderItem($data);
        // dd($response);
        return $response;

    }


    public function trackingDetails($item_id,$tracking_id){

        $data = array(
            "item_id" => $item_id,
            "tracking_id" => $tracking_id
        );      
        

        $response = $this->api_model->trackingDetails($data);
        // dd($response);
        return $response;

    }
    
    public function CPtrackingDetails($item_id){

        $data = array(
            "order_item_part_id" => $item_id
        );      
        

        $response = $this->api_model->CPtrackingDetails($data);
        // dd($response);
        return $response;

    }
    
    public function SupplierCPtrackingDetails($item_id){

        $data = array(
            "order_item_part_id" => $item_id
        );      
        

        $response = $this->api_model->SupplierCPtrackingDetails($data);
        // dd($response);
        return $response;

    }
    
    public function supplierTrackingDetails($item_id,$tracking_id){

        $data = array(
            "item_id" => $item_id,
            "tracking_id" => $tracking_id
        );      
        

        $response = $this->api_model->supplierTrackingDetails($data);
        // dd($response);
        return $response;

    }
    
    public function logisticsTrackingDetails($item_id,$tracking_id){

        $data = array(
            "item_id" => $item_id,
            "tracking_id" => $tracking_id
        );      
        

        $response = $this->api_model->logisticsTrackingDetails($data);
        // dd($response);
        return $response;

    }

    //Suppliers Orders...............................................................

}
