<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProposalModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showProposals(){

        $data = array();

        
        if(Request::get("category_id")){
            $data["category_id"] = Request::get("category_id");
        }
        
        if(Request::get("sub_category_id")){
            $data["sub_category_id"] = Request::get("sub_category_id");
        }

        if(Request::get("city_id")){
            $data["city_id"] = Request::get("city_id");
        }       
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $response = $this->api_model->showProposals($data);
        // dd($response);
        return $response;

    }

    public function sendProposalEmail(){

        $header_array = array();
        if(Request::post('header_name')){
            $count = 0;
            $headers_key = Request::post('header_name');
            $headers_value = Request::post('header_name');
            for($i = 0;$i<count(Request::post('header_name'));$i++){
                $header = array(
                    "header_name" => $headers_key[$i],
                    "header_value" => $headers_value[$i]
                );

                array_push($header_array,$header);
            }

        }

        $data = array(
            "html" => "".Request::post('html'),
            "subject" => "".Request::post('subject'),
            "to_email" => "".Request::post('to_email'),
            "headers" => json_encode($header_array)

        );

        if(Request::post('cc')){
            $data["cc"] = Request::post('cc');
        }
        
        if(Request::post('bcc')){
            $data["bcc"] = Request::post('bcc');
        }

        $files = array();

        if(Request::file('attachments')){

            $images = Request::file('attachments');
            foreach($images as $image){

                array_push($files, array(
                    "image_name" => $image->getClientOriginalName(),
                    "image_ext" => $image->getClientOriginalExtension(),
                    "image" => $image
                ));

            }

        }

        //  dd($files);
         $response = $this->api_model->resendEmail($data,$files);
         // dd($response);
         return $response;

    }
}
