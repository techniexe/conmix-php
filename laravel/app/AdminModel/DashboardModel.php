<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class DashboardModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }


    public function getDashboardDetails(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getDashboardDetails($data);
        // dd($response);
        return $response;

    }
    
    public function getMonthlyChartDetails(){

        $data = array();
        
        if(Request::get("user_type")){

            $data["user_type"] = Request::get("user_type");
            
        }else{
            $data["user_type"] = 'buyer';
        }

        if(Request::get("year")){

            $data["year"] = Request::get("year");
            
        }


        // dd($data);
        $response = $this->api_model->getMonthlyChartDetails($data);
        // dd($response);
        return $response;

    }
    
    public function getDayChartDetails(){

        $data = array();

        if(Request::get("user_type")){

            $data["user_type"] = Request::get("user_type");
            
        }else{
            $data["user_type"] = 'buyer';
        }

        if(Request::get("year")){

            $data["year"] = Request::get("year");
            
        }

        if(Request::get('month')){
            $data['month'] = Request::get('month');
        }
        
        // dd($data);
        $response = $this->api_model->getDayChartDetails($data);
        // dd($response);
        return $response;

    }
    
    public function getBuyerLatestOrders(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getBuyerLatestOrders($data);
        // dd($response);
        return $response;

    }
    
    public function getSuppliersLatestOrders(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getSuppliersLatestOrders($data);
        // dd($response);
        return $response;

    }
    
    public function getLogisticsLatestOrders(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getLogisticsLatestOrders($data);
        // dd($response);
        return $response;

    }
    
    public function getUnverifiedProducts(){

        $data = array();

        
        // dd($data);
        $response = $this->api_model->getUnverifiedProducts($data);
        // dd($response);
        return $response;

    }

}
