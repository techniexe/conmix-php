<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;

class ProductDetailModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function getProductDetails($product_id){

        $data = array();

        

        // dd($data);
        $response = $this->api_model->getProductDetails($product_id);
        // dd($response);
        return $response;

    }
}
