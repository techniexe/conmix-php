<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class GradeCatBrandModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }
    public function getConcreteGrades(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->getConcreteGrades($data);
        // dd($response);
        return $response;

    }


    

    public function addConcreteGrade(){

        $data = array(
            "name" => Request::post("grade_name")
            
        );

        

        // dd($files);
        $response = $this->api_model->addConcreteGrade($data);

        return $response;

    }

    public function editConcreteGrade(){

       
        
        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "name" => Request::post("grade_name"),
            "_id" => Request::post("grade_id"),        
                
        );

        
        // dd($data);
        $response = $this->api_model->editConcreteGrade($data);

        return $response;


    }
    
    

    public function getCementBrand(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->getCementBrand($data);
        // dd($response);
        return $response;

    }

    public function addCementBrand(){

        $image = Request::file('brand_img');
        $imageName = time().'.'.$image->getClientOriginalExtension();

        $data = array(
            "name" => Request::post("brand_name")
            
        );

        $files = array();        

        array_push($files, array(
            "image_name" => $image->getClientOriginalName(),
            "image_ext" => $image->getClientOriginalExtension(),
            "image" => $image,
            "param_key" => "image"
        ));

        

        // dd($files);
        $response = $this->api_model->addCementBrand($data,$files);

        return $response;

    }

    public function editCementBrand(){

        $files = array();
        if(Request::file('brand_img')){
            $image = Request::file('brand_img');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            // dd($image->getClientOriginalName());

            $files = array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "image"
            );
        }
       
        
        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "name" => Request::post("brand_name"),
            "_id" => Request::post("brand_id"),        
                
        );

        
        // dd($data);
        $response = $this->api_model->editCementBrand($data,$files);

        return $response;


    }
    
    
    public function getAddMixtureBrand(){

        $data = array();
        
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->getAddMixtureBrand($data);
        // dd($response);
        return $response;

    }
    
    public function getAddMixtureCatTypes(){

        $data = array();
        
        // if(Request::get("search")){
        //     $data["search"] = Request::get("search");
        // }

        // if(Request::get("before")){
        //     $data["before"] = Request::get("before");
        // }
    
        // if(Request::get("after")){
        //     $data["after"] = Request::get("after");
        // }

        // dd($data);
        $response = $this->api_model->getAddMixtureCatTypes($data);
        // dd($response);
        return $response;

    }

    public function addAddMixtureBrand(){

        $image = Request::file('brand_img');
        $imageName = time().'.'.$image->getClientOriginalExtension();

        $data = array(
            "name" => Request::post("brand_name")
            
        );

        $files = array();        

        array_push($files, array(
            "image_name" => $image->getClientOriginalName(),
            "image_ext" => $image->getClientOriginalExtension(),
            "image" => $image,
            "param_key" => "image"
        ));

        

        // dd($files);
        $response = $this->api_model->addAddMixtureBrand($data,$files);

        return $response;

    }

    public function editAddMixtureBrand(){

        $files = array();
        if(Request::file('brand_img')){
            $image = Request::file('brand_img');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            // dd($image->getClientOriginalName());

            $files = array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image,
                "param_key" => "image"
            );
        }
       
        
        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "name" => Request::post("brand_name"),
            "_id" => Request::post("brand_id"),        
                
        );

        
        // dd($data);
        $response = $this->api_model->editAddMixtureBrand($data,$files);

        return $response;


    }
    
    
    public function getAddMixtureTypes(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->getAddMixtureTypes($data);
        // dd($response);
        return $response;

    }

    public function addAddMixtureTypes(){

        $data = array(
            "category_name" => Request::post("category_name"),
            "brand_id" => Request::post("brand_id"),
            "admixture_type" => Request::post("admixture_type"),
            
        );

        $files = array();        

        

        // dd($files);
        $response = $this->api_model->addAddMixtureTypes($data,$files);

        return $response;

    }

    public function editAddMixtureTypes(){

        $files = array();
        
       
        
        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "category_name" => Request::post("category_name"),
            "brand_id" => Request::post("brand_id"),
            "admixture_type" => Request::post("admixture_type"),
            "_id" => Request::post("admixture_type_id"),        
                
        );

        
        // dd($data);
        $response = $this->api_model->editAddMixtureTypes($data,$files);

        return $response;


    }


    public function getCementGrade(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->getCementGrade($data);
        // dd($response);
        return $response;

    }

    public function addCementGrade(){

        $data = array(
            "name" => Request::post("cement_grade_name")
            
        );

        $files = array();        

      

        

        // dd($files);
        $response = $this->api_model->addCementGrade($data,$files);

        return $response;

    }

    public function editCementGrade(){

        $files = array();
       
       
        
        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "name" => Request::post("cement_grade_name"),
            "_id" => Request::post("grade_id"),        
                
        );

        
        // dd($data);
        $response = $this->api_model->editCementGrade($data,$files);

        return $response;


    }
    
    public function getPaymentMethod(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->getPaymentMethod($data);
        // dd($response);
        return $response;

    }

    public function addPaymentMethod(){

        $data = array(
            "name" => Request::post("payment_method_name")
            
        );

        $files = array();        

      

        

        // dd($files);
        $response = $this->api_model->addPaymentMethod($data,$files);

        return $response;

    }

    public function editPaymentMethod(){

        $files = array();
       
       
        
        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "name" => Request::post("payment_method_name"),
            "_id" => Request::post("pay_id"),        
                
        );

        
        // dd($data);
        $response = $this->api_model->editPaymentMethod($data,$files);

        return $response;


    }
}
