<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class CouponModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showcoupons(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("code")){
            $data["code"] = Request::get("code");
        }
        
        if(Request::get("discount_value")){
            $data["discount_value"] = Request::get("discount_value");
        }
        
        if(Request::get("start_date")){

            $date = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';

            $data["start_date"] = $date;
        }
        
        if(Request::get("end_date")){

            $date = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';

            $data["end_date"] = $date;
        }
        
        if(Request::get("min_order_range_value") && Request::get("min_order")){
            $data["min_order_range_value"] = Request::get("min_order_range_value");
            $data["min_order"] = Request::get("min_order");
        }
        
        if(Request::get("max_discount_range_value") && Request::get("max_discount")){
            $data["max_discount_range_value"] = Request::get("max_discount_range_value");
            $data["max_discount"] = Request::get("max_discount");
        }
        
        if(Request::get("is_active")){
            $data["is_active"] = Request::get("is_active");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showcoupons($data);
        // dd($response);
        return $response;

    }

    public function addCoupon(){

        $data = array(

            "code" => Request::post("code"),
            "discount_type" => Request::post("discount_type"),
            "discount_value" => Request::post("discount_value"),
            "min_order" => Request::post("min_order"),
            "max_discount" => Request::post("max_discount"),
            "max_usage" => Request::post("max_usage"),
            "unique_use" => Request::post("unique_use") == 'on' ? 'true' : 'false',
            "start_date" => Request::post("start_date"),
            "end_date" => Request::post("end_date"),
            "info" => Request::post("info"),
            "tnc" => Request::post("tnc"),

        );

        if(Request::post("buyer_id")){
            $data["buyer_ids"] = json_encode(Request::post("buyer_id"));
        }

        // dd($data);

        $response = $this->api_model->addCoupon($data);
        // dd($response);
        return $response;
    }
    
    public function editCoupon(){

        $data = array(

            "code" => Request::post("code"),
            "discount_type" => Request::post("discount_type"),
            "discount_value" => Request::post("discount_value"),
            "min_order" => Request::post("min_order"),
            "max_discount" => Request::post("max_discount"),
            "max_usage" => Request::post("max_usage"),
            "unique_use" => Request::post("unique_use") == 'on' ? 'true' : 'false',
            "start_date" => Request::post("start_date"),
            "end_date" => Request::post("end_date"),
            "info" => Request::post("info"),
            "tnc" => Request::post("tnc"),
            "_id" => Request::post("coupon_id"),
            "authentication_code" => Request::post("authentication_code"),

        );

        if(Request::post("buyer_id")){
            $data["buyer_ids"] = json_encode(Request::post("buyer_id"));
        }
        
        // dd($data);

        $response = $this->api_model->editCoupon($data);
        // dd($response);
        return $response;
    }

    public function couponActiveInActive(){
        $request_data = Request::post("data");
        
        $is_active = 'true';
        if($request_data["is_active"] == 'true'){
            
            $is_active = 'false';
            // dd($request_data);
        }else{
            $is_active = 'true';
        }
       
        $data = array(
            "authentication_code" => "".$request_data["authentication_code"],
            "is_active" => $is_active,
            "_id" => $request_data["_id"],
            "code" => $request_data["code"],
            "discount_type" => $request_data["discount_type"],
            "discount_value" => $request_data["discount_value"],
            "min_order" => $request_data["min_order"],
            "max_discount" => $request_data["max_discount"],
            "max_usage" => $request_data["max_usage"],
            "start_date" => $request_data["start_date"],
            "end_date" => $request_data["end_date"],
            "info" => $request_data["info"],
            "tnc" => $request_data["tnc"],
        );        
        // dd($data);
        $response = $this->api_model->editCoupon($data);

        return $response;
    }
    
    public function couponDelete(){

        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("_id")
        );
        
        $response = $this->api_model->couponDelete($data);

        return $response;
    }
}
