<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProductModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showProductCategories(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showProductCategories($data);
        // dd($response);
        return $response;

    }

    public function getCategories(){

        $data = array();

        // dd($data);
        $response = $this->api_model->showProductCategories($data);
        // dd($response);
        return $response;

    }

    public function addCategory(){

        $image = Request::file('product_img');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        // dd($image->getClientOriginalName());
        $data = array(
            "category_name" => Request::post("category_name")
            
        );

        $files = array();        

        array_push($files, array(
            "image_name" => $image->getClientOriginalName(),
            "image_ext" => $image->getClientOriginalExtension(),
            "image" => $image,
            "param_key" => "image"
        ));

        

        // dd($files);
        $response = $this->api_model->addProductCategory($data,$files);

        return $response;

    }
    
    public function editCategory(){

        $files = array();
        if(Request::file('product_img')){
            $image = Request::file('product_img');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            // dd($image->getClientOriginalName());

            $files = array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image
            );
        }
        
        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "category_name" => Request::post("category_name"),
            "_id" => Request::post("product_cat_id"),        
                
        );

        
        // dd($data);
        $response = $this->api_model->editProductCategory($data,$files);

        return $response;


    }

    public function showSubCategories($default_category_id){

        $data = array();
        
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("categoryId")){
            $data["categoryId"] = Request::get("categoryId");
        }else{
            $data["categoryId"] = $default_category_id;
        }

        if(Request::get("margin_rate_id")){
            $data["margin_rate_id"] = Request::get("margin_rate_id");
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        

        // dd($data);
        if(!empty($data["categoryId"])){
            $response = $this->api_model->showProductSubCategories($data);
        }else{
            $response["data"] = array();
        }
        // dd($response);
        return $response;

    }

    public function getSubCategories(){

        $data = array();

        if(Request::get("categoryId")){
            $data["categoryId"] = Request::get("categoryId");
        }

        if(Request::get("sub_cat_name")){
            $data["search"] = Request::get("sub_cat_name");
        }

        // dd($data);
        if(isset($data["categoryId"])){
            $response = $this->api_model->showProductSubCategories($data);
        }else{
            $response["data"] = array();
        }
        // dd($response);
        return $response;

    }

    public function addSubCategory(){

        $image = Request::file('product_sub_cat_image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        // dd($image->getClientOriginalName());
        $data = array(
            "sub_category_name" => Request::post("sub_category_name"),
            "categoryId" => Request::post("categoryId")
            
        );

        if(Request::post("gst_slab_id")){
            $data["gst_slab_id"] = Request::post('gst_slab_id');
        }
        
        if(Request::post("margin_rate_id")){
            $data["margin_rate_id"] = Request::post('margin_rate_id');
        }
        
        if(Request::post("selling_unit")){
            // $data["selling_unit[]"] = Request::post('selling_unit');
            $selling_unit_array = array();
            $count = 0;
            foreach(Request::post("selling_unit") as $key => $value){
                // array_push($selling_unit_array,$value);
                if(isset($value)){
                    $data["selling_unit[".$count."]"] = $value;
                    $count++;
                }
                
            }

            // $data["selling_unit"] = $selling_unit_array;

            if(Request::post("min_quantity")){

                $min_qty = array();

                foreach(Request::post("selling_unit") as $key => $value){
                    // array_push($min_qty, array($value => Request::post('min_quantity')));
                    if(isset($value)){
                        $min_qty[$value] = Request::post('min_quantity');
                    }
                    
                    // $data["min_quantity[".$value."]"] = Request::post('min_quantity');
                }

                $data["min_quantity"] = json_encode($min_qty);
            }
        }        
        

        $files = array(
            "image_name" => $image->getClientOriginalName(),
            "image_ext" => $image->getClientOriginalExtension(),
            "image" => $image
        );
        
        // dd($data);
        $response = $this->api_model->addProductSubCategory($data,$files);

        return $response;

    }

    public function editSubCategory(){

        $files = array();
        if(Request::file('product_sub_cat_image')){
            $image = Request::file('product_sub_cat_image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            // dd($image->getClientOriginalName());

            $files = array(
                "image_name" => $image->getClientOriginalName(),
                "image_ext" => $image->getClientOriginalExtension(),
                "image" => $image
            );
        }
        $data = array(
            "sub_category_name" => Request::post("sub_category_name"),
            "categoryId" => Request::post("categoryId"),
            "_id" => Request::post("product_sub_cat_id"),
            "authentication_code" => Request::post("authentication_code")
            
        );

        if(Request::post("gst_slab_id")){
            $data["gst_slab_id"] = Request::post('gst_slab_id');
        }
        
        if(Request::post("margin_rate_id")){
            $data["margin_rate_id"] = Request::post('margin_rate_id');
        }
        
        if(Request::post("selling_unit")){
            // $data["selling_unit[]"] = Request::post('selling_unit');
            $selling_unit_array = array();
            $count = 0;
            foreach(Request::post("selling_unit") as $key => $value){
                // array_push($selling_unit_array,$value);
                $data["selling_unit[".$count."]"] = $value;
                $count++;
            }

            // $data["selling_unit"] = $selling_unit_array;

            if(Request::post("min_quantity")){

                $min_qty = array();

                foreach(Request::post("selling_unit") as $key => $value){
                    // array_push($min_qty, array($value => Request::post('min_quantity')));
                    $min_qty[$value] = Request::post('min_quantity');
                    // $data["min_quantity[".$value."]"] = Request::post('min_quantity');
                }

                $data["min_quantity"] = json_encode($min_qty);
            }
        }            

        // dd($data);
        $response = $this->api_model->editProductSubCategory($data,$files);

        return $response;
    }

    public function showProducts(){

        $data = array();
        
        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("categoryId")){
            $data["categories"] = array();
            array_push($data["categories"],Request::get("categoryId"));
        }
        
        if(Request::get("subcategoryId")){
            $data["sub_categories"] = array();
            array_push($data["sub_categories"],Request::get("subcategoryId"));
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        

        // dd($data);
        $response = $this->api_model->showProducts($data);
        // dd($response);
        return $response;


    }

    public function editProducts(){

        $data = array(
            "minimum_order" => Request::post("minimum_order"),
            "unit_price" => Request::post("unit_price"),
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("add_product_id") 
        );

        if(Request::post('is_available')){
            $data['is_available'] = 'true';
        }else{
            $data['is_available'] = 'false';
        }

        if(Request::post('self_logistics')){
            $data['self_logistics'] = 'true';
        }else{
            $data['self_logistics'] = 'false';
        }

        if(Request::post('verified_by_admin')){
            if(Request::post('verified_by_admin')){
                $data["verified_by_admin"] = 'true';
            }else{
                $data["verified_by_admin"] = 'false';
            }
            
        }
        // dd($data);
        $response = $this->api_model->editProduct($data);

        return $response;


    }

    public function designmix_detail(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("categoryId")){
            // $data["categories"] = array();
            // array_push($data["categories"],Request::get("categoryId"));
            $data["product_category_id"] = Request::get("categoryId");
        }
        
        if(Request::get("subcategoryId")){
            // $data["sub_categories"] = array();
            // array_push($data["sub_categories"],Request::get("subcategoryId"));

            $data["product_sub_category_id"] = Request::get("subcategoryId");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);

        $data = $this->api_model->designmix_detail($data);  

        return $data;
    }
    
    public function verifyProducts(){

        $data = array(
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("product_id") 
        );

        if(Request::post('verified_by_admin')){
            
            $data["verified_by_admin"] = 'true';
            
        }else{
            $data["verified_by_admin"] = 'false';
        }
        // dd($data);
        $response = $this->api_model->editProduct($data);

        return $response;


    }



    public function showGstSalb(){

        $data = array();

        // dd($data);
        $response = $this->api_model->showGstSalb($data);
        // dd($response);
        return $response;

    }

    public function addGstSalb(){        
        
        $data = array(
            "title" => Request::post("title"),
            "igst_rate" => Request::post("igst_rate"),
            "sgst_rate" => Request::post("sgst_rate"),
            "cgst_rate" => Request::post("cgst_rate")
            
        );

        if(Request::post('hsn_code')){
            $data['hsn_code'] = Request::post('hsn_code');
        }

        // dd($data);
        $response = $this->api_model->addGstSalb($data);

        return $response;

    }
    
    public function editGstSalb(){        
        
        $data = array(
            "title" => Request::post("title"),
            "igst_rate" => Request::post("igst_rate"),
            "sgst_rate" => Request::post("sgst_rate"),
            "cgst_rate" => Request::post("cgst_rate"),
            "_id" => Request::post("gst_slab_id"),
            "authentication_code" => Request::post("authentication_code")
            
        );

        if(Request::post('hsn_code')){
            $data['hsn_code'] = Request::post('hsn_code');
        }

        // dd($data);
        $response = $this->api_model->editGstSalb($data);

        return $response;

    }


    public function showMarginRate(){

        $data = array();

        // dd($data);
        $response = $this->api_model->showMarginRate($data);
        // dd($response);
        return $response;

    }

    public function addMarginSlab(){

        $margin_slab = array();

        if(Request::post("margin_upto") && Request::post("margin_rate")){
            $margin_upto = Request::post("margin_upto");
            $margin_rate = Request::post("margin_rate");

            for($i=0;$i<sizeof($margin_upto);$i++){

                if(isset($margin_upto[$i]) && isset($margin_rate[$i]))
                {
                    $upto_rate_array = array(
                        "upto" => $margin_upto[$i],
                        "rate" => $margin_rate[$i]
                    );

                    array_push($margin_slab,$upto_rate_array);
                }

            }
        }
        
        $data = array(
            "title" => Request::post("slab_title"),
            "slab" => $margin_slab,
            // "authentication_code" => Request::post("authentication_code")
            
        );

        // dd($data);
        $response = $this->api_model->addMarginSlab($data);

        return $response;

    }
    
    public function editMarginSlab(){

        $margin_slab = array();

        if(Request::post("margin_upto") && Request::post("margin_rate")){
            $margin_upto = Request::post("margin_upto");
            $margin_rate = Request::post("margin_rate");

            for($i=0;$i<sizeof($margin_upto);$i++){

                if(isset($margin_upto[$i]) && isset($margin_rate[$i]))
                {
                    $upto_rate_array = array(
                        "upto" => $margin_upto[$i],
                        "rate" => $margin_rate[$i]
                    );
                }

                array_push($margin_slab,$upto_rate_array);

            }
        }
        
        $data = array(
            "title" => Request::post("slab_title"),
            "slab" => $margin_slab,
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("margin_slab_id")
            
        );

        // dd($data);
        $response = $this->api_model->editMarginSlab($data);

        return $response;

    }

    public function showAggregateSource(){

        $data = array();

        if(Request::get('region_id')){
            
            $data["region_id"] = Request::get('region_id');
            
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showAggregateSource($data);
        // dd($response);
        return $response;

    }

    public function addAggregateSource(){

        $data = array(
            "region_id" => Request::post("region_id"),
            "aggregate_source_name" => Request::post("aggregate_source_name"),
            
        );

        // dd($data);
        $response = $this->api_model->addAggregateSource($data);

        return $response;

    }
    
    public function editAggregateSource(){

        $data = array(
            "aggregate_source_name" => Request::post("aggregate_source_name"),            
            "_id" => Request::post("aggregate_source_id"),     
            "authentication_code" => Request::post("authentication_code")       
        );

        // dd($data);
        $response = $this->api_model->editAggregateSource($data);

        return $response;

    }
    
    public function deleteAggregateSource(){

        $data = array(           
            "_id" => Request::post("source_id"),   
            "authentication_code" => Request::post("authentication_code")         
        );

        // dd($data);
        $response = $this->api_model->deleteAggregateSource($data);

        return $response;

    }
    
    
    public function showSandSource(){

        $data = array();

        if(Request::get('region_id')){
            
            $data["region_id"] = Request::get('region_id');
            
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showSandSource($data);
        // dd($response);
        return $response;

    }

    public function addSandSource(){

        $data = array(
            "region_id" => Request::post("region_id"),
            "sand_source_name" => Request::post("sand_source_name"),
            
        );

        // dd($data);
        $response = $this->api_model->addSandSource($data);

        return $response;

    }
    
    public function editSandSource(){

        $data = array(
            "sand_source_name" => Request::post("sand_source_name"),            
            "_id" => Request::post("sand_source_id"),     
            "authentication_code" => Request::post("authentication_code")        
        );

        // dd($data);
        $response = $this->api_model->editSandSource($data);

        return $response;

    }
    
    public function deleteSandSource(){

        $data = array(           
            "_id" => Request::post("source_id"),  
            "authentication_code" => Request::post("authentication_code")           
        );

        // dd($data);
        $response = $this->api_model->deleteSandSource($data);

        return $response;

    }
    
    
    public function showFlyashSource(){

        $data = array();

        if(Request::get('region_id')){
            
            $data["region_id"] = Request::get('region_id');
            
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showFlyashSource($data);
        // dd($response);
        return $response;

    }

    public function addFlyashSource(){

        $data = array(
            "region_id" => Request::post("region_id"),
            "fly_ash_source_name" => Request::post("flyash_source_name"),
            
            
        );

        // dd($data);
        $response = $this->api_model->addFlyashSource($data);

        return $response;

    }
    
    public function editFlyashSource(){

        $data = array(
            "fly_ash_source_name" => Request::post("flyash_source_name"),            
            "_id" => Request::post("flyash_source_id"),            
            "authentication_code" => Request::post("authentication_code") 
        );

        // dd($data);
        $response = $this->api_model->editFlyashSource($data);

        return $response;

    }
    
    public function deleteFlyashSource(){

        $data = array(           
            "_id" => Request::post("source_id"),     
            "authentication_code" => Request::post("authentication_code")        
        );

        // dd($data);
        $response = $this->api_model->deleteFlyashSource($data);

        return $response;

    }


    public function showSandZone(){

        $data = array();

        if(Request::get('region_id')){
            
            $data["region_id"] = Request::get('region_id');
            
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        // dd($data);
        $response = $this->api_model->showSandZone($data);
        // dd($response);
        return $response;

    }

    public function addSandZone(){

        $data = array(
            "zone_name" => Request::post("zone_name"),
            "finess_module_range" => Request::post("finess_module_range"),
            
        );

        // dd($data);
        $response = $this->api_model->addSandZone($data);

        return $response;

    }
    
    public function editSandZone(){

        $data = array(
            "zone_name" => Request::post("zone_name"),            
            "finess_module_range" => Request::post("finess_module_range"),            
            "_id" => Request::post("sand_zone_id"),     
            "authentication_code" => Request::post("authentication_code")        
        );

        // dd($data);
        $response = $this->api_model->editSandZone($data);

        return $response;

    }
    
    public function deleteSandZone(){

        $data = array(           
            "_id" => Request::post("zone_id"),  
            "authentication_code" => Request::post("authentication_code")           
        );

        // dd($data);
        $response = $this->api_model->deleteSandZone($data);

        return $response;

    }

}
