<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TMExport extends Model implements FromArray,WithHeadings
{
    //
    protected $buyer_order;

    public function __construct(array $buyer_order)
    {
        $this->buyer_order = $buyer_order;
    }

    public function array(): array
    {
        return $this->buyer_order;
    }

    public function headings(): array
    {

        return [

            'Comapny Name',
            'Plant Name',
            'Category',
            'Sub Category',
            'Vehicle No',
            'Min Trip Price (Per Cu.Mtr/KM)',
            'Per Cu.Mtr/KM Rate',
            'Type Model ',
            'Manufacture Year',
            'Is Insurance Active',
            'Created At',

        ];

    }
}
