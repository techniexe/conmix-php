<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ProfileModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showProfileDetails(){

        $data = array();

        $response = $this->api_model->getAdminProfile();
        // dd($response);
        return $response;
    }

    public function editProfile(){

        $data = array();
            if(Request::get('otp_code_1')){
                    
                $otp_code =  Request::get('otp_code_1');


                if(Request::get('mobile_or_email_type')){

                    if(Request::get('mobile_or_email_type') == "old_mobile"){
                    
                        $data["old_mobile_code"] =  $otp_code;
                       
                    }
                    
                    if(Request::get('mobile_or_email_type') == "new_mobile"){
                    
                        $data["new_mobile_code"] =  $otp_code;
                       
                    }
    
                    if(Request::get('mobile_or_email_type') == "old_email"){
                    
                        $data["old_email_code"] =  $otp_code;
                       
                    }
    
                    if(Request::get('mobile_or_email_type') == "new_email"){
                    
                        $data["new_email_code"] =  $otp_code;
                       
                    }
    
                }

            }

        // $data = array(
        //     "authentication_code" => "".Request::post('authentication_code')
        // );

        if(Request::post('full_name')){
            $data["full_name"] =  Request::post('full_name');
        }
        
        if(Request::post('email')){
            $data["email"] =  Request::post('email');
        }
        
        if(Request::post('mobile_number')){
            $data["mobile_number"] =  Request::post('mobile_number');
        }
        
        if(Request::post('password')){
            $data["password"] =  Request::post('password');
        }       
        

        //  dd($data);
         $response = $this->api_model->editAdminProfile($data);
         // dd($response);
         return $response;

    }
    
    public function changePassword(){

        $data = array(
            "authentication_code" => "".Request::post('authentication_code')

        );

       
        
        if(Request::post('password')){
            $data["password"] =  Request::post('password');
        }
        
        if(Request::post('old_password')){
            $data["old_password"] =  Request::post('old_password');
        }
        
        

        //  dd($data);
         $response = $this->api_model->changePassword($data);
         // dd($response);
         return $response;

    }

    public function updateNotiStatus($id){

        $data = array(
            "_id" => $id

        );       
        

        //  dd($data);
         $response = $this->api_model->updateNotiStatus($data);
         // dd($response);
         return $response;

    }
}
