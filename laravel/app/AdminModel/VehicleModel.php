<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class VehicleModel extends Model
{
    //
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();
    }

    public function showTMCategory(){
        $data = array();

        $response = $this->api_model->showTMCategory($data);

        return $response;
    }

    public function addTMCategory(){

        $data = array(
            "category_name" => Request::post("category_name")
        );
        
        $response = $this->api_model->addTMCategory($data);

        return $response;
    }

    public function editTMCategory(){

        $data = array(
            "category_name" => Request::post("category_name"),
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("cat_id")
        );
        // dd($data);
        $response = $this->api_model->editTMCategory($data);
        // dd($response);
        return $response;
    }

    public function showTMSubCategoies(){

        $data = array();

        if(Request::get('TM_category_id')){
            
            $data["TM_category_id"] = Request::get('TM_category_id');
            
        }
        
        if(Request::get('load_capacity')){
            
            $data["load_capacity"] = Request::get('load_capacity');
            
        }
        // dd($data);
        $response = $this->api_model->getTMSubCategoris($data);
        // dd($response);
        return $response;

    }

    public function addTMSubCategory(){

        $data = array(
            "sub_category_name" => Request::post("sub_category_name"),
            "load_capacity" => Request::post("TM_capacity"),
            "weight_unit_code" => Request::post("weight_unit_code"),
            "weight_unit" => Request::post("weight_unit"),            
            "cat_id" => Request::post("TM_cat_id")
        );
        
        $response = $this->api_model->addTMSubCategory($data);

        return $response;
    }

    public function editTMSubCategory(){

        $data = array(
            "sub_category_name" => Request::post("sub_category_name"),
            "load_capacity" => Request::post("TM_capacity"),
            "weight_unit_code" => Request::post("weight_unit_code"),
            "weight_unit" => Request::post("weight_unit"),            
            "_id" => Request::post("TM_sub_cat_id"),
            "authentication_code" => Request::post("authentication_code")
        );
        // dd($data);
        $response = $this->api_model->editTMSubCategory($data);
        // dd($response);
        return $response;

    }


    public function showPumpCategory(){
        $data = array();

        $response = $this->api_model->showPumpCategory($data);

        return $response;
    }

    public function addPumpCategory(){

        $data = array(
            "category_name" => Request::post("category_name")
        );
        
        $response = $this->api_model->addPumpCategory($data);

        return $response;
    }

    public function editPumpCategory(){

        $data = array(
            "category_name" => Request::post("category_name"),
            "authentication_code" => Request::post("authentication_code"),
            "_id" => Request::post("cat_id")
        );
        // dd($data);
        $response = $this->api_model->editPumpCategory($data);
        // dd($response);
        return $response;
    }



    public function showVehicles(){

        $data = array();

        if(Request::get("TM_category_id")){
            $data["TM_category_id"] = Request::get("TM_category_id");
        }
    
        if(Request::get("TM_sub_category_id")){
            $data["TM_sub_category_id"] = Request::get("TM_sub_category_id");
        }
        
        if(Request::get("min_delivery_range_value") && Request::get("min_delivery_range")){
            $data["min_delivery_range_value"] = Request::get("min_delivery_range_value");
            $data["min_delivery_range"] = Request::get("min_delivery_range");
        }
        
        if(Request::get("min_trip_price_range_value") && Request::get("min_trip_price")){
            $data["min_trip_price_range_value"] = Request::get("min_trip_price_range_value");
            $data["min_trip_price"] = Request::get("min_trip_price");
        }
        
        if(Request::get("is_active")){
            $data["is_active"] = Request::get("is_active");
        }
        
        if(Request::get("is_insurance_active")){
            $data["is_insurance_active"] = Request::get("is_insurance_active");
        }
        
        if(Request::get("TM_rc_number")){
            $data["TM_rc_number"] = Request::get("TM_rc_number");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
    
        if(Request::get("is_gps_enabled")){
            $data["is_gps_enabled"] = Request::get("is_gps_enabled");
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $response = $this->api_model->getVehicles($data);

        return $response;

    }



    public function addVehiclePMKM(){

        $data = array(
            "state_id" => Request::post("state_id"),
            "region_type" => Request::post("region_type"),
            "per_metric_ton_per_km_rate" => Request::post("per_metric_ton_per_km_rate")
        );
        
        $response = $this->api_model->addVehiclePMKM($data);

        return $response;
    }
    
    public function editVehiclePMKM(){

        $data = array(
            "region_type" => Request::post("region_type"),
            "per_metric_ton_per_km_rate" => Request::post("per_metric_ton_per_km_rate"),
            "_id" => Request::post("vehical_pm_km_id")
        );
        
        $response = $this->api_model->editVehiclePMKM($data);

        return $response;
    }

    public function showVehiclesPMRM(){

        $data = array();
    
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $response = $this->api_model->getVehiclesPMRM($data);

        return $response;

    }
    
    public function getPMKMRateByStateId($state_id){

        $data = array(
            "state_id" => $state_id
        );

        $response = $this->api_model->getPMKMRateByStateId($data);

        return $response;

    }

}
