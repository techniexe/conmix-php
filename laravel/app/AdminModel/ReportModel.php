<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Request;

class ReportModel extends Model
{
    //

    
    
    
    protected $api_model;
    public function __construct(){
        $this->api_model = new ApiModel();

        $month_array = array(

            "01" => 0,
            "02" => 1,
            "03" => 2,
            "04" => 3,
            "05" => 4,
            "06" => 5,
            "07" => 6,
            "08" => 7,
            "09" => 8,
            "10" => 9,
            "11" => 10,
            "12" => 11,
        
        );

        $all_month_array = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    }

    public function admix_brand_report(){

        $data = array();

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $data = $this->api_model->admix_brand_report($data);  

        return $data;
    }
    
    public function admix_types_report(){

        $data = array();

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $data = $this->api_model->admix_types_report($data);  

        return $data;
    }
    
    public function agg_sand_cat_report(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $data = $this->api_model->agg_sand_cat_report($data);  

        return $data;
    }
    
    public function agg_sand_subcategory_report($default_category_id){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("categoryId")){
            $data["category_id"] = Request::get("categoryId");
        }else{
            $data["category_id"] = $default_category_id;
        }

        if(Request::get("margin_rate_id")){
            $data["margin_rate_id"] = Request::get("margin_rate_id");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->agg_sand_subcategory_report($data);  

        return $data;
    }
    
    public function agg_source_report(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("region_id")){
            $data["region_id"] = Request::get("region_id");
        }


        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $data = $this->api_model->agg_source_report($data);  

        return $data;
    }
    
    public function buyer_listing_report(){

        $data = array();

        if(Request::get("name")){
            $data["search"] = Request::get("name");
        }
    
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("account_type")){
            $data["account_type"] = Request::get("account_type");
        }
    
        if(Request::get("mobile_no")){
            $data["search"] = Request::get("mobile_no");
        }
        
        if(Request::get("email")){
            $data["search"] = Request::get("email");
        }
    
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->buyer_listing_report($data);  

        return $data;
    }
    
    public function cement_brand_report(){

        $data = array();

        if(Request::get("name")){
            $data["search"] = Request::get("name");
        }
    
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("account_type")){
            $data["account_type"] = Request::get("account_type");
        }
    
        if(Request::get("mobile_no")){
            $data["search"] = Request::get("mobile_no");
        }
        
        if(Request::get("email")){
            $data["search"] = Request::get("email");
        }
    
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->cement_brand_report($data);  

        return $data;
    }
    
    public function cement_grade_report(){

        $data = array();

        if(Request::get("name")){
            $data["search"] = Request::get("name");
        }
    
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("account_type")){
            $data["account_type"] = Request::get("account_type");
        }
    
        if(Request::get("mobile_no")){
            $data["search"] = Request::get("mobile_no");
        }
        
        if(Request::get("email")){
            $data["search"] = Request::get("email");
        }
    
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->cement_grade_report($data);  

        return $data;
    }
    
    public function concrete_grade_report(){

        $data = array();

        if(Request::get("name")){
            $data["search"] = Request::get("name");
        }
    
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("account_type")){
            $data["account_type"] = Request::get("account_type");
        }
    
        if(Request::get("mobile_no")){
            $data["search"] = Request::get("mobile_no");
        }
        
        if(Request::get("email")){
            $data["search"] = Request::get("email");
        }
    
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->concrete_grade_report($data);  

        return $data;
    }
    
    public function concrete_pump_category_report(){

        $data = array();

        if(Request::get("name")){
            $data["search"] = Request::get("name");
        }
    
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("account_type")){
            $data["account_type"] = Request::get("account_type");
        }
    
        if(Request::get("mobile_no")){
            $data["search"] = Request::get("mobile_no");
        }
        
        if(Request::get("email")){
            $data["search"] = Request::get("email");
        }
    
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->concrete_pump_category_report($data);  

        return $data;
    }
    
    public function coupon_report(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("code")){
            $data["code"] = Request::get("code");
        }
        
        if(Request::get("discount_value")){
            $data["discount_value"] = Request::get("discount_value");
        }
        
        if(Request::get("start_date")){

            $date = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';

            $data["start_date"] = $date;
        }
        
        if(Request::get("end_date")){

            $date = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';

            $data["end_date"] = $date;
        }
        
        if(Request::get("min_order_range_value") && Request::get("min_order")){
            $data["min_order_range_value"] = Request::get("min_order_range_value");
            $data["min_order"] = Request::get("min_order");
        }
        
        if(Request::get("max_discount_range_value") && Request::get("max_discount")){
            $data["max_discount_range_value"] = Request::get("max_discount_range_value");
            $data["max_discount"] = Request::get("max_discount");
        }
        
        if(Request::get("is_active")){
            $data["is_active"] = Request::get("is_active");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->coupon_report($data);  

        return $data;
    }
    
    public function support_ticket_report(){

        $data = array();

        if(Request::get("severity")){
            $data["severity"] = Request::get("severity");
        }
        
        if(Request::get("subject")){
            $data["subject"] = Request::get("subject");
        }
        
        if(Request::get("ticket_id")){
            $data["ticket_id"] = Request::get("ticket_id");
        }
        
        if(Request::get("client_type")){
            $data["client_type"] = Request::get("client_type");
        }
        
        if(Request::get("buyer_name")){
            $data["buyer_name"] = Request::get("buyer_name");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("support_ticket_status")){
            $data["support_ticket_status"] = Request::get("support_ticket_status");
        }

        if(Request::get("start_date")){

            $date = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';

            $data["start_date"] = $date;
        }
        
        if(Request::get("end_date")){

            $date = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';

            $data["end_date"] = $date;
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->support_ticket_report($data);  

        return $data;
    }
    
    public function flyAsh_source_report(){

        $data = array();

        if(Request::get('region_id')){
            
            $data["region_id"] = Request::get('region_id');
            
        }

        if(Request::get("start_date")){

            $date = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';

            $data["start_date"] = $date;
        }
        
        if(Request::get("end_date")){

            $date = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';

            $data["end_date"] = $date;
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->flyAsh_source_report($data);  

        return $data;
    }
    
    public function gst_slab_report(){

        $data = array();

        if(Request::get('region_id')){
            
            $data["region_id"] = Request::get('region_id');
            
        }

        if(Request::get("start_date")){

            $date = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';

            $data["start_date"] = $date;
        }
        
        if(Request::get("end_date")){

            $date = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';

            $data["end_date"] = $date;
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->gst_slab_report($data);  

        return $data;
    }
    
    public function margin_rate_report(){

        $data = array();

        if(Request::get('region_id')){
            
            $data["region_id"] = Request::get('region_id');
            
        }

        if(Request::get("start_date")){

            $date = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';

            $data["start_date"] = $date;
        }
        
        if(Request::get("end_date")){

            $date = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';

            $data["end_date"] = $date;
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->margin_rate_report($data);  

        return $data;
    }
    
    public function sand_source_report(){

        $data = array();

        if(Request::get('region_id')){
            
            $data["region_id"] = Request::get('region_id');
            
        }

        if(Request::get("start_date")){

            $date = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';

            $data["start_date"] = $date;
        }
        
        if(Request::get("end_date")){

            $date = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';

            $data["end_date"] = $date;
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
// dd($data);
        $data = $this->api_model->sand_source_report($data);  

        return $data;
    }
    
    public function supplier_listing_report(){

        $data = array();

        if(Request::get("name")){
            $data["search"] = Request::get("name");
        }  
        
    
        if(Request::get("mobile_no")){
            $data["search"] = Request::get("mobile_no");
        }
        
        if(Request::get("email")){
            $data["search"] = Request::get("email");
        }
    
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("pan_number")){
            $data["pan_number"] = Request::get("pan_number");
        }
        
        if(Request::get("company_type")){
            $data["company_type"] = Request::get("company_type");
        }
        
        if(Request::get("verified_by_admin")){
            $data["verified_by_admin"] = Request::get("verified_by_admin");
        }

        if(Request::get("start_date")){

            $date = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';

            $data["start_date"] = $date;
        }
        
        if(Request::get("end_date")){

            $date = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';

            $data["end_date"] = $date;
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);
        $data = $this->api_model->supplier_listing_report($data);  

        return $data;
    }
    
    public function logistics_listing_report(){

        $data = array();

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $data = $this->api_model->logistics_listing_report($data);  

        return $data;
    }
    
    public function buyer_orders_report(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_status")){
            $data["order_status"] = Request::get("order_status");
        }
        
        if(Request::get("payment_status")){
            $data["payment_status"] = Request::get("payment_status");
        }

        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }     
        
        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }     
        

        if(Request::get("order_id")){
            $data["order_id"] = Request::get("order_id");
        }
        
        if(Request::get("buyer_id")){
            $data["buyer_order_id"] = Request::get("buyer_id");
        }
        
        if(Request::get("buyer_name")){
            $data["buyer_name"] = Request::get("buyer_name");
        }
        
        if(Request::get("year")){
            $data["year"] = Request::get("year");
        }
        
        if(Request::get("date")){

            $date = date('Y-m-d', strtotime(Request::post("date"))).'T00:00:00.000Z';

            $data["date"] = $date;
        }
        
        if(Request::get("month")){
            $month = date("M", strtotime(Request::get("month")));
            $year = date("Y", strtotime(Request::get("month")));          
            

            $data["month"] = $month;
            $data["year"] = $year;
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        
        $data = $this->api_model->buyer_orders_report($data);  

        return $data;
    }
    
    public function buyer_orders_detail_report($id){

        $data = array(
            "_id" => $id
        );

        

        $data = $this->api_model->buyer_orders_detail_report($data);  

        return $data;
    }
    
    
    
    public function supplier_orders_report(){

        $data = array();

        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
        
        if(Request::get("order_status")){
            $data["order_status"] = Request::get("order_status");
        }
        
        if(Request::get("payment_status")){
            $data["payment_status"] = Request::get("payment_status");
        }

        if(Request::get("gateway_transaction_id")){
            $data["gateway_transaction_id"] = Request::get("gateway_transaction_id");
        }       

        if(Request::get("address_id")){
            $data["address_id"] = Request::get("address_id");
        }     
        

        if(Request::get("order_id")){
            $data["order_id"] = Request::get("order_id");
        }
        
        if(Request::get("buyer_id")){
            $data["buyer_id"] = Request::get("buyer_id");
        }
        
        if(Request::get("year")){
            $data["year"] = Request::get("year");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("date")){

            $date = date('Y-m-d', strtotime(Request::post("date"))).'T00:00:00.000Z';

            $data["date"] = $date;
        }
        
        if(Request::get("month")){
            $month = date("M", strtotime(Request::get("month")));
            $year = date("Y", strtotime(Request::get("month")));          
            

            $data["month"] = $month;
            $data["year"] = $year;
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $data = $this->api_model->supplier_orders_report($data);  

        return $data;
    }
    
    public function supplier_orders_detail_report($id,$user_id){

        $data = array(
            "_id" => $id,
            "supplier_id" => $user_id
        );

        

        $data = $this->api_model->supplier_orders_detail_report($data);  

        return $data;
    }
    
    

    public function logistics_orders_report(){

        $data = array();

        if(Request::get("logistic_id")){
            $data["logistic_id"] = Request::get("logistic_id");
        }
        
        if(Request::get("order_status")){
            $data["status"] = Request::get("order_status");
        }
        
        if(Request::get("selected_month_year")){
            
            $selected_month_year = Request::get("selected_month_year");

            $selected_month_year_array = explode(" ",$selected_month_year);

            $month = $selected_month_year_array[0];
            $year = $selected_month_year_array[1];

            if($month){
                $data["month"] = $month;
            }
            
            if($year){
                $data["year"] = $year;
            }

        }
        

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }

        $data = $this->api_model->logistics_orders_report($data);  

        return $data;
    }

    public function logistics_orders_detail_report($id,$user_id){

        $data = array(
            "_id" => $id,
            "logistic_id" => $user_id
        );

        

        $data = $this->api_model->logistics_orders_detail_report($data);  

        return $data;
    }
    
    public function designmix_listing_report(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }

        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("categoryId")){
            // $data["categories"] = array();
            // array_push($data["categories"],Request::get("categoryId"));
            $data["product_category_id"] = Request::get("categoryId");
        }
        
        if(Request::get("subcategoryId")){
            // $data["sub_categories"] = array();
            // array_push($data["sub_categories"],Request::get("subcategoryId"));

            $data["product_sub_category_id"] = Request::get("subcategoryId");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);

        $data = $this->api_model->designmix_listing_report($data);  

        return $data;
    }
    
    public function designmix_detail_report(){

        $data = array();

        if(Request::get("search")){
            $data["search"] = Request::get("search");
        }
        
        if(Request::get("grade_id")){
            $data["grade_id"] = Request::get("grade_id");
        }
        
        if(Request::get("categoryId")){
            // $data["categories"] = array();
            // array_push($data["categories"],Request::get("categoryId"));
            $data["product_category_id"] = Request::get("categoryId");
        }
        
        if(Request::get("subcategoryId")){
            // $data["sub_categories"] = array();
            // array_push($data["sub_categories"],Request::get("subcategoryId"));

            $data["product_sub_category_id"] = Request::get("subcategoryId");
        }

        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);

        $data = $this->api_model->designmix_detail_report($data);  

        return $data;
    }
    
    public function vehicle_listing_report(){

        $data = array();

        if(Request::get("TM_category_id")){
            $data["TM_category_id"] = Request::get("TM_category_id");
        }
    
        if(Request::get("TM_sub_category_id")){
            $data["TM_sub_category_id"] = Request::get("TM_sub_category_id");
        }
        
        if(Request::get("min_delivery_range_value") && Request::get("min_delivery_range")){
            $data["min_delivery_range_value"] = Request::get("min_delivery_range_value");
            $data["min_delivery_range"] = Request::get("min_delivery_range");
        }
        
        if(Request::get("min_trip_price_range_value") && Request::get("min_trip_price")){
            $data["min_trip_price_range_value"] = Request::get("min_trip_price_range_value");
            $data["min_trip_price"] = Request::get("min_trip_price");
        }
        
        if(Request::get("is_active")){
            $data["is_active"] = Request::get("is_active");
        }
        
        if(Request::get("is_insurance_active")){
            $data["is_insurance_active"] = Request::get("is_insurance_active");
        }
        
        if(Request::get("TM_rc_number")){
            $data["TM_rc_number"] = Request::get("TM_rc_number");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
    
        if(Request::get("is_gps_enabled")){
            $data["is_gps_enabled"] = Request::get("is_gps_enabled");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
    
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);

        $data = $this->api_model->vehicle_listing_report($data);  

        return $data;
    }
    
    public function TM_category_report(){

        $data = array();

        if(Request::get("TM_category_id")){
            $data["TM_category_id"] = Request::get("TM_category_id");
        }
    
        if(Request::get("TM_sub_category_id")){
            $data["TM_sub_category_id"] = Request::get("TM_sub_category_id");
        }
        
        if(Request::get("min_delivery_range_value") && Request::get("min_delivery_range")){
            $data["min_delivery_range_value"] = Request::get("min_delivery_range_value");
            $data["min_delivery_range"] = Request::get("min_delivery_range");
        }
        
        if(Request::get("min_trip_price_range_value") && Request::get("min_trip_price")){
            $data["min_trip_price_range_value"] = Request::get("min_trip_price_range_value");
            $data["min_trip_price"] = Request::get("min_trip_price");
        }
        
        if(Request::get("is_active")){
            $data["is_active"] = Request::get("is_active");
        }
        
        if(Request::get("is_insurance_active")){
            $data["is_insurance_active"] = Request::get("is_insurance_active");
        }
        
        if(Request::get("TM_rc_number")){
            $data["TM_rc_number"] = Request::get("TM_rc_number");
        }
        
        if(Request::get("company_name")){
            $data["company_name"] = Request::get("company_name");
        }
        
        if(Request::get("user_id")){
            $data["user_id"] = Request::get("user_id");
        }
    
        if(Request::get("is_gps_enabled")){
            $data["is_gps_enabled"] = Request::get("is_gps_enabled");
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
    
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);

        $data = $this->api_model->TM_category_report($data);  

        return $data;
    }
    
    public function TM_sub_category_report(){

        $data = array();

        if(Request::get('TM_category_id')){
            
            $data["TM_category_id"] = Request::get('TM_category_id');
            
        }
        
        if(Request::get('load_capacity')){
            
            $data["load_capacity"] = Request::get('load_capacity');
            
        }

        if(Request::get("start_date")){
            $data["start_date"] = date('Y-m-d', strtotime(Request::post("start_date"))).'T00:00:00.000Z';
        }
        
        if(Request::get("end_date")){
            $data["end_date"] = date('Y-m-d', strtotime(Request::post("end_date"))).'T00:00:00.000Z';
        }
    
        if(Request::get("before")){
            $data["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $data["after"] = Request::get("after");
        }
        // dd($data);

        $data = $this->api_model->TM_sub_category_report($data);  

        return $data;
    }
}
