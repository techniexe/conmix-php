<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Request;
use Illuminate\Support\Facades\Session;
class CommonAPIModel extends BaseModel
{
    //
    protected $api_model;

    public function __construct(){
        $this->api_model = new \App\SupplierModel\ApisModel();

    }

    public function getConcretePumpCategory(){

        $url_path = ApiConfig::SUPPLIER_CONCRETE_PUMP_CATEGORY_LISTING_URL;
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getProductCategory(){

        $url_path = ApiConfig::GET_PRODUCT_CATEGORY_URL;
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getProductSubCategory($cat_id){

        $url_path = sprintf(ApiConfig::GET_PRODUCT_SUB_CATEGORY_URL,$cat_id);
        // dd($url_path);
        $params = array();

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getCountries(){

        $url_path = ApiConfig::GET_COUNTRY_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getStates(){

        $url_path = ApiConfig::GET_STATE_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);
        
        return $response;

    }

    public function getCities(){

        $url_path = ApiConfig::GET_CITY_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getStateByCountryID($country_id){

        $url_path = sprintf(ApiConfig::GET_STATE_BY_COUNRTY_URL,$country_id);
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getCityBystateID($state_id){

        $url_path = sprintf(ApiConfig::GET_CITY_BY_STATE_URL,$state_id);
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getCartDetailsById($cart_id){

        $state_id = "";

        if(Session::get("selected_state_id") != null){
            $state_id = Session::get("selected_state_id");
        }

        $lat = "";
        $long = "";
        $with_TM = "";
        $with_CP = "";
        $delivery_date = "";

        if(Session::get("selected_lat") != null){
            $lat = Session::get("selected_lat");
        }

        if(Session::get("selected_long") != null){
            $long = Session::get("selected_long");
        }

        $session_custom_mix_design = Session::get("custom_mix_design");

        if(isset($session_custom_mix_design)){

            $with_TM = $session_custom_mix_design["with_TM"];
            $with_CP = isset($session_custom_mix_design["with_CP"]) ? $session_custom_mix_design["with_CP"] : 'no';

            $with_TM = $session_custom_mix_design["with_TM"] == "yes" ? 'true' : 'false';
            $with_CP = $with_CP == "yes" ? 'true' : 'false';

        }

        if(Session::get("selected_delivery_date") != null){
            $delivery_date = Session::get("selected_delivery_date");
        }



        // if(!empty($state_id)){
        //     $url_path = sprintf(ApiConfig::GET_CART_BY_ID_URL,$cart_id);
        //     $url_path = $url_path."?state_id=".$state_id;
        // }else{
            $url_path = sprintf(ApiConfig::GET_CART_BY_ID_URL,$cart_id);
        // }
        // dd($url_path);

        // if(Session::get('my_cart_id') != null){

        //     $url_path = sprintf(ApiConfig::GET_CART_BY_ID_URL,$cart_id,$with_TM,$with_CP,$delivery_date);

        // }else{
        //     $url_path = sprintf(ApiConfig::GET_CART_URL,$with_TM,$with_CP,$delivery_date);
        //     // dd($url_path);
        // }

        $params = array();

        // if(!empty($state_id)){
        //     $params["state_id"] = $state_id;
        // }

        // if(!empty($lat)){
        //     $params["lat"] = $lat;
        // }

        // if(!empty($long)){
        //     $params["long"] = $long;
        // }

        // if(!empty($with_TM)){
        //     $params["with_TM"] = $with_TM;
        // }

        // if(!empty($with_CP)){
        //     $params["with_CP"] = $with_CP;
        // }

        // if(!empty($delivery_date)){
        //     $params["delivery_date"] = $delivery_date;
        // }
        // dd($url_path);

        $response = $this->requestGET($url_path,$params);
        // dd($response);

        if($response["status"] == 200){

            if(isset($response["data"])){
                Session::put('my_cart_id',$response["data"]["_id"]);
            }

        }

        return $response;



    }

    public function getRegionalDetails(){

        $url_path = ApiConfig::GET_REGIONAL_DETAILS_URL;

        $cordinates = array(
            Request::get("long"),
            Request::get("lat"),
        );

        $params = array(

            "state_name" => Request::get("state_name"),
            "city_name" => Request::get("city_name"),
            "location" => array(
                "coordinates" => $cordinates
            )

        );

        $response = $this->requestPOST($url_path,$params);
        // dd($response);

        return $response;

    }

    public function getCementBrand(){

        $url_path = ApiConfig::COMMON_CEMENT_BRAND_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getSandSource(){

        $url_path = ApiConfig::COMMON_SAND_SOURCE_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }
    
    public function getSandZone(){

        $url_path = ApiConfig::COMMON_SAND_ZONE_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getAggregateSource(){

        $url_path = ApiConfig::COMMON_AGGREGATE_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getAggregateSandCategory(){

        $url_path = ApiConfig::COMMON_AGGREGATE_SAND_CATEGORY_LISTING_URL;
        // dd($url_path);
        $params = array();

        // if(Request::get("search")){
            $params["search"] = "Aggregate";
        // }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getAggregateSandSubCategory($cat_id){

        $url_path = sprintf(ApiConfig::COMMON_AGGREGATE_SAND_SUB_CATEGORY_LISTING_URL,$cat_id);
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getAggregateSandSubCategoryForCustomMix($sub_cat_id){

        $url_path = sprintf(ApiConfig::COMMON_GET_AGGREGATE_SAND_SUB_CATEGORY_FOR_CUSTOM_MIX_URL,$sub_cat_id);
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);
        // dd($response);
        return $response;

    }

    public function getFlyAshSource(){

        $url_path = ApiConfig::COMMON_FLY_ASH_SOURCE_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getAdMixtureBrand(){

        $url_path = ApiConfig::COMMON_ADMIXTURE_BRNAD_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getConcreteGrade(){

        $url_path = ApiConfig::COMMON_CONCRETE_GRADE_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getAdmixtureTypes(){

        $url_path = ApiConfig::COMMON_ADMIXTURE_TYPES_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        if(Request::get("admix_brand_id")){
            $params["brand_id"] = Request::get("admix_brand_id");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getCementGrade(){

        $url_path = ApiConfig::COMMON_CEMENT_GRADE_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getTMCategory(){

        $url_path = ApiConfig::COMMON_TM_CATEGORY_LISTING_URL;
        // dd($url_path);
        $params = array();

        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function getTMSubCategoris(){


        // dd($url_path);
        $params = array();


        if(Request::get("vehicle_cat_id")){
            // $data["vehicle_cat_id"] = Request::get("vehicle_cat_id");
            $params["vehicle_cat_id"] = Request::get("vehicle_cat_id");
        }
        // else{
        //     $data["vehicle_cat_id"] = "";
        // }
// dd($data);
        // $url_path = sprintf(ApiConfig::COMMON_TM_SUB_CATEGORY_LISTING_URL,$data["vehicle_cat_id"]);
        $url_path = ApiConfig::COMMON_TM_SUB_CATEGORY_LISTING_URL;
// dd($url_path);
        if(Request::get("search")){
            $params["search"] = Request::get("search");
        }

        $response = $this->requestGET($url_path,$params);

        return $response;

    }

    public function gettNotifications(){

        $url_path = ApiConfig::GET_NOTIFICATIONS_URL;


        $params = array();

        if(Request::get("before")){
            $params["before"] = Request::get("before");
        }

        if(Request::get("after")){
            $params["after"] = Request::get("after");
        }

        $response = $this->requestGET($url_path,$params);
        // dd($response);

        return $response;

    }

    public function gettNotificatioCount(){

        $url_path = ApiConfig::GET_NOTIFICATIONS_COUNT_URL;


        $params = array();

        $response = $this->requestGET($url_path,$params);

        // dd($response);
        return $response;

    }

    public function getSupplierNotifications(){

        $url_path = ApiConfig::GET_SUPPLIER_NOTIFICATIONS_URL;


        $params = array();

        if(Request::get("before")){
            $params["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $params["after"] = Request::get("after");
        }

        $response = $this->requestGET($url_path,$params);


        return $response;

    }

    public function getSupplierNotificatioCount(){

        $url_path = ApiConfig::GET_SUPPLIER_NOTIFICATIONS_COUNT_URL;


        $params = array();

        $response = $this->requestGET($url_path,$params);


        return $response;

    }

    public function getAdminNotifications(){

        $url_path = ApiConfig::GET_ADMIN_NOTIFICATIONS_URL;


        $params = array();

        if(Request::get("before")){
            $params["before"] = Request::get("before");
        }
    
        if(Request::get("after")){
            $params["after"] = Request::get("after");
        }
// dd($params);
        $response = $this->requestGET($url_path,$params);


        return $response;

    }

    public function getAdminNotificatioCount(){

        $url_path = ApiConfig::GET_ADMIN_NOTIFICATIONS_COUNT_URL;


        $params = array();

        $response = $this->requestGET($url_path,$params);


        return $response;

    }

    public function getPaymentmethods(){

        $url_path = ApiConfig::GET_PAYMENT_METHOD_URL;


        $params = array();

        $response = $this->requestGET($url_path,$params);


        return $response;

    }

    public function customDesignOptions(){

        $url_path = ApiConfig::GET_CUSTOM_MIX_OPTIONS_URL;


        $params = array();

        if(Session::get("selected_state_id")){
            $params["state_id"] = Session::get("selected_state_id");
        }

        $params["lat"] = Session::get("selected_lat");

        $params["long"] = Session::get("selected_long");

        if(Session::get("is_site_selected") == "1"){
            $params["site_id"] = Session::get("selected_site_address");
        }
        // dd($params);
        $response = $this->requestPOST($url_path,$params);
        // dd($response);

        return $response;

    }
}
