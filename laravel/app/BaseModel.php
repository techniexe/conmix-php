<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ixudra\Curl\Facades\Curl;
use \GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\RequestException;
use Request;

class BaseModel extends Model
{
    //
    private $client;
    private $path;
    private $headers = array();
    private $api_urls = ApiConfig::API_BASE_URL;
    public function __construct(){
        $this->headers["Content-Type"] = "application/json";

        $this->api_urls = env('API_BASE_URL');

        // $custom_token = Cache::get('custom_token');

        // if (is_array($custom_token)) {
        //     $custom_token = $custom_token['data']['customToken'];
        // }
        $this->path = Request::path();
            // dd($this->path);

        if (strpos($this->path, 'admin/') !== false) {
                if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }

        if (strpos($this->path, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($this->path, 'supplier/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }

        }

    }

    public function requestGET($url_path,$params=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        // dd(session('custom_token', null));
        if (strpos($url_path, 'admin/') !== false) {
            if (session('custom_token', null)) {
                // dd($url_path);
                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }
        // dump($url_path);
        // dump("...................................");
        if (strpos($url_path, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {
                // dump($url_path);
                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($url_path, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {
                // dd($url_path);
                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }
        }

        $url = $this->api_urls.''.$url_path;

        // if($url_path == "source"){
        //     dd($url);
        // }
        // dump($url);
        // dd($this->headers);
        // dd($url);
        // $url = "http://15.206.148.217:3033/v1/product/category/5b71765d06428d042413d708?lat=23.040180499907&long=72.514617486914";
        // if($url_path === 'cart/5ee1d1383317e33cbaef251d'){
            // dump($url);
            // dump($this->headers);
            // dd($params);
        //     // dd($response);
        // }
        $response = Curl::to($url)
            ->withHeaders( $this->headers )
            ->withData( $params )
            ->asJson(true)
            ->returnResponseObject();
        // dd($response);
        $response = $response->get();
        // dump($response);
        // if($url_path == "design_mix?grade_id=1&lat=23.022505&long=72.5713621&state_id=4&site_id=1&delivery_date=2022-12-14&end_date=2023-01-13&quantity=20"){
        //     // dump($url);
        //     dd($response);
        // }
        // if($url_path === 'design_mix/2?lat=23.022505&long=72.5713621&state_id=4&delivery_date=2023-01-06&site_id=1&with_TM=true&with_CP=false'){
        // if($url_path === 'vendor/operator'){
        // if($url_path === 'vendor/users/subvendorlisting'){
        // if($url_path === 'admin/notification/all'){
        // if($url_path === 'buyer/cart/91'){
        //     dump($url);
        //     // dump($this->headers);
        //     dd($response);
        // }
        // dump($response);

        // if(isset($params["email_mobile"])){
        //     echo "test..".$url;
        //     dump($this->headers);
        //     dd($response);
        //     // dd($params);
        //     // dd($this->headers);

        //     }

        $final_response = $this->handleResponse($response);


        return $final_response;

    }

    public function requestPOST($url,$params=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        unset($this->headers["Content-Type"]);
        if (strpos($url, 'admin/') !== false) {
            if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }
            
            if (session('supplier_forgot_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_forgot_custom_token', null);
                
            }
        }

        $url = $this->api_urls.''.$url;
        // dump($url);
        // dd($params);
        // dd($this->headers);
        $response = Curl::to($url)
            ->withHeaders( $this->headers )
            ->withData( $params )
            ->withTimeout(0)
            ->asJson( true )
            ->returnResponseObject()
            ->post();
            // dump($response);
            // if(isset($params["email_mobile"])){
            //     echo "test..".$url;
                // dd($response);
            //     // dd($params);
            //     // dd($this->headers);

            //     }

        $final_response = $this->handleResponse($response);
        // dd($final_response);
        // // exit;
        // return $final_response;

        return $final_response;

    }

    public function requestPATCH($url,$params=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        unset($this->headers["Content-Type"]);
        if (strpos($url, 'admin/') !== false) {
            if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }

            if (session('supplier_forgot_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_forgot_custom_token', null);
                
            }
        }

        $url = $this->api_urls.''.$url;
        // dump($url);
        // dd($params);
        // dd($this->headers);
        $response = Curl::to($url)
            ->withHeaders( $this->headers )
            ->withData( $params )
            ->asJson( true )
            ->returnResponseObject()
            ->patch();
        // dd($response);

        $final_response = $this->handleResponse($response);

        return $final_response;

    }

    public function requestPUT($url,$params=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');

        if (strpos($url, 'admin/') !== false) {
            if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }
        }

        $url = $this->api_urls.''.$url;
        // dd($url);
        // dd($this->headers);
        // dd($params);
        $response = Curl::to($url)
            ->withHeaders( $this->headers )
            ->withData( $params )
            ->asJson( true )
            ->returnResponseObject()
            ->put();
        // dd($response);

        $final_response = $this->handleResponse($response);

        return $final_response;

    }

    public function requestDELETE($url,$params=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        $custom_token = "";
        if (strpos($url, 'admin/') !== false) {
            if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);

            }
            $custom_token = session('custom_token', null);
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {
                // dump($url);
                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);

                $custom_token = session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);

                $custom_token = session('supplier_custom_token', null);
            }
        }

        $url = $this->api_urls.''.$url;
        // dump($url);
        // dd($params);
        // dd($this->headers);
        // $response = Curl::to($url)
        //     ->withHeaders( $this->headers )
        //     ->withData( $params )
        //     ->asJson( true )
        //     ->returnResponseObject()
        //     ->delete();

        // $response = Curl::to($url)
        //     ->withHeaders( $this->headers )
        //     ->withData( $params )
        //     ->asJson( true )
        //     ->returnResponseObject()
        //     ->delete();

        // dd($response);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
                array('Authorization: '.'Bearer ' . $custom_token,
                    'Content-Type: application/json'
            ));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $result = json_decode($result);
        // dd($result);


        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // dd($httpcode);

        if($httpcode >= 200 && $httpcode < 300){
            $httpcode = 200;
        }

        $messages = "";
        if(isset($result->error->message)){
            $messages = $result->error->message;
        }

        $final_response = array(
            "status" => $httpcode,
            "data" => null,
            "error" => array("message"=>$messages)
        );


        // $final_response = $this->handleResponse($response);

        return $final_response;
    }


    public function requestPOST_WithFile($url,$params=array(),$files=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        // $this->headers["Content-Type"] = "";
        unset($this->headers["Content-Type"]);
        if (strpos($url, 'admin/') !== false) {
            if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }
        }

        $url = $this->api_urls.''.$url;
        // dd(json_encode($params));
        // dd($files);
        // dd($this->headers);.

        // $response = Curl::to($url)
        //     ->withHeaders( $this->headers )
        //     ->withData( $params )
        //     ->withFile( 'image', $files['image'], 'image/'.$files['image_ext'], $files['image_name'] )
        //     ->containsFile(true)
        //     ->returnResponseObject()
        //     ->post();
        $response = Curl::to($url);
        $response = $response->withHeaders( $this->headers );
        $response = $response->withData( $params );
        if($files){
            $response = $response->withFile( 'image', $files['image'], 'image/'.$files['image_ext'], $files['image_name'] );
            $response = $response->containsFile(true);
        }

        $response = $response->returnResponseObject();
        $response = $response->post();

        // dd($response);
            // if(isset($params["full_name"])){
            //     echo "test..".$url;
            //     dd($response);
            //     // dd($params);
            //     // dd($this->headers);

            //     }

        $final_response = $this->handleResponse($response);
        // dd($final_response);
        // // exit;
        // return $final_response;

        return $final_response;

    }

    public function requestPATCH_WithFile($url,$params=array(),$files=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        $custom_token = "";
        // $this->headers["Content-Type"] = "";
        unset($this->headers["Content-Type"]);
        if (strpos($url, 'admin/') !== false) {
        if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);

            }
            $custom_token = session('custom_token', null);
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);

                $custom_token = session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);

                $custom_token = session('supplier_custom_token', null);
            }
        }

        $url = $this->api_urls.''.$url;
        // dd($this->headers);
        // dd($url);
        if($files){
            $response = Curl::to($url);

            $response = $response->withHeaders( $this->headers );
            $response = $response->withData( $params );
            // $response = $response->withFile( 'image', '', '', '' );
            // if($files){
                $response = $response->withFile( 'image', $files['image'], 'image/'.$files['image_ext'], $files['image_name'] );
                $response = $response->containsFile(true);
            // }
            // dd($response);
            $response = $response->returnResponseObject();
            $response = $response->patch();

            // dd($response);

            $final_response = $this->handleResponse($response);
        }else{
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                    array('Authorization: '.'Bearer ' . $custom_token
                ));
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            
            $result = json_decode($result);


            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            // dd($httpcode);
            $error = array("message"=>"Something went wrong. Please try again");
            $is_error = 0;
            if($httpcode >= 200 && $httpcode < 300){
                $httpcode = 200;
            }else{

                if(isset($result->error)){
                    // $final_response["error"] = $result["error"];
                    // $error = array("message"=>$result->error);
                    $error["message"] = $result->error->message;
                    $is_error = 1;
                }

                if($is_error == 0){
                    $result = json_decode($result);
                    if(isset($result->error->message)){
                        // $final_response["error"] = $result->error;
                        // $error = array("message"=>$result->error->message);
                        $error["message"] = $result->error->message;
                    }
                }

                // dd($error);

            }

            $final_response = array(
                "status" => $httpcode,
                "data" => null,
                "error" => $error
            );

        }

        // dd($final_response);
        // // exit;
        // return $final_response;

        return $final_response;

    }

    public function requestPOST_WithMultipleFile($url,$params=array(),$files=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        // $this->headers["Content-Type"] = "";
        unset($this->headers["Content-Type"]);
        // $this->headers["Content-Type"] = "multipart/form-data";
        if (strpos($url, 'admin/') !== false) {
            if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }
        }

        $url = $this->api_urls.''.$url;
        // dd($url);
        // dd($params);
        // dd(json_encode($params));
        // dd($files);
        // dd($this->headers);
        $response = Curl::to($url);

        $response = $response->withHeaders( $this->headers );
        $response = $response->withData( $params );
        // $response = $response->withFile( 'image', '', '', '' );
        if($files){
            $count = 0;
            foreach($files as $file){

                if(isset($file['param_key'])){
                    $response = $response->withFile( ''.$file['param_key'], $file['image'], 'image/'.$file['image_ext'], $file['image_name'] );
                }else{
                    $response = $response->withFile( 'attachments['.$count.']', $file['image'], 'image/'.$file['image_ext'], $file['image_name'] );
                }

                $response = $response->containsFile(true);
                $count++;
            }
        }
        // dd($response);
        // echo $url;
        // if(strpos($url,'variant') == false){
            
            $response = $response->returnResponseObject();
        // }
        $response = $response->post();

        // if(strpos($url,'variant') != false){
        //     dd($response);

        // }
        // dump($response);
        $final_response = $this->handleResponse($response);


        // dd($final_response);
            // if(isset($params["full_name"])){
            //     echo "test..".$url;
            //     dd($response);
            //     // dd($params);
            //     // dd($this->headers);

            //     }

        // $final_response = $this->handleResponse($response);
        // dd($final_response);
        // // exit;
        // return $final_response;

        return $final_response;

    }


    public function requestPOST_WithMultipleFile_with_mimetype($url,$params=array(),$files=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        // $this->headers["Content-Type"] = "";
        unset($this->headers["Content-Type"]);
        // $this->headers["Content-Type"] = "multipart/form-data";
        if (strpos($url, 'admin/') !== false) {
            if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }
        }

        $url = $this->api_urls.''.$url;
        // dump($url);
        // dd($params);
        // dd(json_encode($params));
        // dd($files);
        // dd($this->headers);
        $response = Curl::to($url);

        $response = $response->withHeaders( $this->headers );
        $response = $response->withData( $params );
        // $response = $response->withFile( 'image', '', '', '' );
        if($files){
            $count = 0;
            foreach($files as $file){

                if(isset($file['param_key'])){
                    $response = $response->withFile( ''.$file['param_key'], $file['image'], ''.$file['image_mime'], $file['image_name'] );
                }else{
                    $response = $response->withFile( 'attachments['.$count.']', $file['image'], ''.$file['image_mime'], $file['image_name'] );
                }

                $response = $response->containsFile(true);
                $count++;
            }
        }
        // dd($response);
        $response = $response->returnResponseObject();
        $response = $response->post();

        // dd($response);

        $final_response = $this->handleResponse($response);


        // dd($response);
            // if(isset($params["full_name"])){
            //     echo "test..".$url;
            //     dd($response);
            //     // dd($params);
            //     // dd($this->headers);

            //     }

        $final_response = $this->handleResponse($response);
        // dd($final_response);
        // // exit;
        // return $final_response;

        return $final_response;

    }


    public function requestPATCH_WithMultipleFile($url,$params=array(),$files=array(),$header = array()){
        $this->api_urls = env('API_BASE_URL');
        // $this->headers["Content-Type"] = "";
        unset($this->headers["Content-Type"]);
        if (strpos($url, 'admin/') !== false) {
            if (session('custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('custom_token', null);
            }
        }

        if (strpos($url, 'buyer/') !== false) {

            if (session('buyer_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('buyer_custom_token', null);
            }
        }

        if (strpos($url, 'vendor/') !== false) {
            if (session('supplier_custom_token', null)) {

                $this->headers["Authorization"] = 'Bearer ' . session('supplier_custom_token', null);
            }
        }

        $url = $this->api_urls.''.$url;
        // dd(json_encode($params));
        // dd($files);
        // dd($this->headers);
        // dd($url);
        $response = Curl::to($url);

        $response = $response->withHeaders( $this->headers );
        $response = $response->withData( $params );
        // $response = $response->withFile( 'image', '', '', '' );
        if($files){
            $count = 0;
            foreach($files as $file){
                if(isset($file['param_key'])){
                    $response = $response->withFile( ''.$file['param_key'], $file['image'], 'image/'.$file['image_ext'], $file['image_name'] );
                }else{
                    $response = $response->withFile( 'attachments['.$count.']', $file['image'], 'image/'.$file['image_ext'], $file['image_name'] );
                }
                $response = $response->containsFile(true);
                $count++;
            }

        }
        // dd($response);
        $response = $response->returnResponseObject();
        $response = $response->patch();

        // dd($response);

        $final_response = $this->handleResponse($response);


        // dd($response);
            // if(isset($params["full_name"])){
            //     echo "test..".$url;
            //     dd($response);
            //     // dd($params);
            //     // dd($this->headers);

            //     }

        $final_response = $this->handleResponse($response);
        // dd($final_response);
        // // exit;
        // return $final_response;

        return $final_response;

    }

    public function setTokenHeader($token)
    {
        $this->headers["Authorization"] = 'Bearer ' . $token;

    }

    public function handleResponse($response){
        // dd($response);
        $status_code = $response->status;
        $body = $response->content;
        // dump($body);
        if($status_code >= 200 && $status_code < 300){
            $status_code = 200;
        }

        $final_response = array(
            "status" => $status_code,
            "data" => null,
            "error" => null
        );
        switch($status_code){

            case 200:
                try{
                    if(isset($body["data"])){
                        // dd($body["data"]);
                        $final_response["data"] = $body["data"];

                    }else if(!empty($body)){
                        if(!is_array($body)){
                            $body = trim($body, '"');
                        }
                        // dd($body);
                        $res = json_decode($body);
                        // dump($res->data->_id);
                        // dd($res->data->customToken);

                        if(isset($res->_id)){
                            $final_response["data"] = array("_id"=>$res->_id);
                        }else if(isset($res->data->_id)){
                            $final_response["data"] = array("_id"=>$res->data->_id);
                        }else if(isset($res->data->customToken)){
                            $final_response["data"]["customToken"] = $res->data->customToken;
                            if(isset($res->data->user_id)){
                                $final_response["data"]["user_id"] = $res->data->user_id;
                            }
                        }
                        // dd($final_response);
                    }
                }catch(\Exception $e){

                }
                break;
            case 201:
                try{
                    if(isset($body["data"])){
                        $final_response["data"] = $body["data"];
                    }
                }catch(\Exception $e){

                }
                break;
            case 403:
                try{
                    $final_response["data"] = $body;
                }catch(\Exception $e){

                }
                break;
            case 422:
                try{
                    // dd($body);
                    if(isset($body["error"])){
                        $final_response["error"] = $body["error"];
                    }else{

                        if(isset($body["details"])){

                            if(isset($body["details"][0])){
                                $final_response["error"]["message"] = $body["details"][0]["description"];
                            }

                        }

                    }

                    if($final_response["error"] == null){
                        $body = json_decode($body);

                        if(isset($body->error)){
                            // $final_response["error"] = $body->error;
                            if(isset($body->error->details)){

                                if(isset($body->error->details[0])){
                                    $final_response["error"]["message"] = $body->error->details[0]->message;
                                }

                            }
                        }

                        if(isset($body->details)){

                        }
                    }

                    if($final_response["error"] == null){
                        // $body = json_decode($body);

                        if(isset($body->details)){
                            // $final_response["error"] = $body->error;
                            if(isset($body->details[0]->description)){

                                if(isset($body->details[0]->description)){
                                    $final_response["error"]["message"] = $body->details[0]->description;
                                }

                            }
                        }

                    }
                }catch(\Exception $e){

                }

                break;
            case 400:
                try{
                    // dd($body);
                    // {"error":{"message":"E11000 duplicate key error collection: material.email_auth_tokens index: user.user_type_1_verification_type_1_email_1 dup key: { user.user_type: \"supplier\", verification_type: \"profile\", email: \"yogi@gmail.com\" }"}}
                    // dd($body);
                    if(isset($body["error"])){
                        $final_response["error"] = $body["error"];
                    }

                    if($final_response["error"] == null){
                        $body = json_decode($body);
                        if(isset($body->error)){
                            $final_response["error"]["message"] = $body->error->message;
                        }
                    }
                    // dd($final_response);
                }catch(\Exception $e){

                }
                break;
            case 500:
                try{
                    if(isset($body["error"])){
                        $final_response["error"] = $body["error"];
                    }

                    if($final_response["error"] == null){
                        $body = json_decode($body);
                        if(isset($body->error)){
                            $final_response["error"] = $body->error;
                        }
                    }
                }catch(\Exception $e){

                }

                break;
            default:
                try{
                    if(isset($body["error"])){
                        $final_response["error"] = $body["error"];
                    }
                    if(isset($response->error)){
                        $final_response["error"]["message"] = $response->error;
                    }
                }catch(\Exception $e){

                }
                break;

        }
        // dd($final_response);
        return $final_response;

    }
}
