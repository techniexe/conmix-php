@section('title', 'Supplier Dashboard')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <div class="alert alert-danger" id="stock_strip" style="display:none;background-color: #ff9ba4f7;border-color: #ff9ba4f7;">
        <a href="#stock-block" style="color:#721c24">    
            Stock Qty is low. Please update your stock 
        </a>
    </div>
    @php
            $profile = session('supplier_profile_details', null);
            //dd($profile);
            //dd($profile);
    @endphp
    @php
    $fill_details = array();
        if(isset($vendors_fill_details['data'])){
          $fill_details = $vendors_fill_details['data'];
          
        }
        //dd($fill_details);
        $is_show_fill_detail_strip_for_master = 0;
        $is_show_fill_detail_strip_for_other_details = 0;
        $is_show_fill_detail_strip_for_setting_details = 0;
    @endphp
    @if(
        (isset($fill_details['billing_addr_count']) && $fill_details['billing_addr_count'] == 0)
        || (isset($fill_details['plant_addr_count']) && $fill_details['plant_addr_count'] == 0)
        || (isset($fill_details['bank_info_count']) && $fill_details['bank_info_count'] == 0)
    )
        @php $is_show_fill_detail_strip_for_master = 1; @endphp
    @endif
    
    @if(isset($profile["master_vendor_id"]))
    
        @if(
            (isset($fill_details['admix_setting_count']) && $fill_details['admix_setting_count'] == 0)
            || (isset($fill_details['TM_price']) && $fill_details['TM_price'] == 0)
            || (isset($fill_details['CP_price']) && $fill_details['CP_price'] == 0)
        )
        
            @php $is_show_fill_detail_strip_for_setting_details = 1; @endphp
        @endif

    @endif

    @if(
        (isset($fill_details['design_mix_count']) && $fill_details['design_mix_count'] == 0)
        || (isset($fill_details['TM_driver_count']) && $fill_details['TM_driver_count'] == 0)
        || (isset($fill_details['CP_operator_count']) && $fill_details['CP_operator_count'] == 0)
        || (isset($fill_details['TM_count']) && $fill_details['TM_count'] == 0)
        || (isset($fill_details['CP_count']) && $fill_details['CP_count'] == 0)
        || (isset($fill_details['cement_stock_count']) && $fill_details['cement_stock_count'] == 0)
        || (isset($fill_details['sand_stock_count']) && $fill_details['sand_stock_count'] == 0)
        || (isset($fill_details['aggregate_stock_count']) && $fill_details['aggregate_stock_count'] == 0)
        || (isset($fill_details['fly_ash_stock_count']) && $fill_details['fly_ash_stock_count'] == 0)
        || (isset($fill_details['admixture_stock_count']) && $fill_details['admixture_stock_count'] == 0)
        || (isset($fill_details['cement_rate_count']) && $fill_details['cement_rate_count'] == 0)
        || (isset($fill_details['sand_rate_count']) && $fill_details['sand_rate_count'] == 0)
        || (isset($fill_details['aggregate_rate_count']) && $fill_details['aggregate_rate_count'] == 0)
        || (isset($fill_details['flyash_rate_count']) && $fill_details['flyash_rate_count'] == 0)
        || (isset($fill_details['admixture_rate_count']) && $fill_details['admixture_rate_count'] == 0)
        || (isset($fill_details['water_rate_count']) && $fill_details['water_rate_count'] == 0)
    )
        @php $is_show_fill_detail_strip_for_other_details = 1; @endphp
    @endif

    @if($is_show_fill_detail_strip_for_master == 1 || $is_show_fill_detail_strip_for_other_details == 1 || $is_show_fill_detail_strip_for_setting_details == 1)
        <div class="alert alert-danger" style="background-color: #ff9ba4f7;border-color: #ff9ba4f7;">
            <a href="#" style="color:#721c24">    
                <!-- Your RMC plant will not be listed on Conmix RMC supplier list until highlited details are filled up.  -->
                Your RMC Plant is not listed yet. Kindly fill up all the details highlighted in red color to list your RMC Plant at Conmix
            </a>
        </div>
    @endif
    <div class="titel_wit_swich">
        <h1 class="main-title">Dashboard</h1>
        
        @if(isset($profile["master_vendor_id"]))
            <div class="avlbl-sswich d-flex">
                <label>Accept Order :</label>
                <div class="verified-switch-btn cstm-css-checkbox">
                    <label class="new-switch1 switch-green">
                        <input type="checkbox" name="is_order_accept" id="is_order_accept" class="switch-input" {{ isset($data['new_order_taken']) ? ($data['new_order_taken']=='true' ? 'checked' : '') : '' }}>
                        <span class="switch-label" data-on="On" data-off="off"></span>
                        <span class="switch-handle"></span>
                    </label>
                </div>
            </div>

            <script>

                var new_order_taken = '<?php echo $data['new_order_taken'] ?>';
                console.log("new_order_taken..."+new_order_taken);
                if(new_order_taken == 'true'){
                    $("#is_order_accept").prop('checked',true);
                }else{
                    $("#is_order_accept").prop('checked',false);
                }

            </script>
        @endif
    </div>
    <div class="clearfix"></div>
    <!--Order Trip-->
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
            <div class="inner">
            <h3><i class="fa fa-rupee"></i> 
                @if(isset($data["data"]["totalVendorOrderAmount"][0]["total"]))
                {{ isset($data["data"]["totalVendorOrderAmount"][0]["total"]) ? number_format($data["data"]["totalVendorOrderAmount"][0]["total"],2) : '   0' }}
                @elseif(isset($data["data"]["totalVendorOrderAmount"]))
                {{ isset($data["data"]["totalVendorOrderAmount"]) ? number_format($data["data"]["totalVendorOrderAmount"],2) : '   0' }}
                @endif
            </h3>
                <p>Total Sales</p>
            </div>
            <div class="icon bg-yellow">
                    <i class="fa fa-calculator"></i>
            </div>

            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
            <div class="inner">
            <h3><i class="fa fa-rupee"></i> 
                @if(isset($data["data"]["todayVendorOrderAmount"][0]["total"]))
                {{ isset($data["data"]["todayVendorOrderAmount"][0]["total"]) ? number_format($data["data"]["todayVendorOrderAmount"][0]["total"],2) : '   0' }}
                @elseif(isset($data["data"]["todayVendorOrderAmount"]))
                {{ isset($data["data"]["todayVendorOrderAmount"]) ? number_format($data["data"]["todayVendorOrderAmount"],2) : '   0' }}
                @endif
            </h3>
                <p>Today's Sales</p>
            </div>
            <div class="icon bg-dblue">

                    <i class="fa fa-calculator"></i>
            </div>

            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
            <div class="inner">
            <h3><i class="fa fa-rupee"></i> 
                @if(isset($data["data"]["thisWeekVendorOrderAmount"][0]["total"]))
                {{ isset($data["data"]["thisWeekVendorOrderAmount"][0]["total"]) ? number_format($data["data"]["thisWeekVendorOrderAmount"][0]["total"],2) : '   0' }}
                @elseif(isset($data["data"]["thisWeekVendorOrderAmount"]))
                {{ isset($data["data"]["thisWeekVendorOrderAmount"]) ? number_format($data["data"]["thisWeekVendorOrderAmount"],2) : '   0' }}
                @endif

                
            </h3>
                <p>This Week Sales</p>
            </div>
            <div class="icon bg-mpinch">
                    <i class="fa fa-calculator"></i>
            </div>

            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
            <div class="inner">
            <h3><i class="fa fa-rupee"></i> 
                @if(isset($data["data"]["thisMonthVendorOrderAmount"][0]["total"]))
                {{ isset($data["data"]["thisMonthVendorOrderAmount"][0]["total"]) ? number_format($data["data"]["thisMonthVendorOrderAmount"][0]["total"],2) : '   0' }}
                @elseif(isset($data["data"]["thisMonthVendorOrderAmount"]))
                {{ isset($data["data"]["thisMonthVendorOrderAmount"]) ? number_format($data["data"]["thisMonthVendorOrderAmount"],2) : '   0' }}
                @endif
            </h3>
                <p>This Month Sales</p>
            </div>
            <div class="icon bg-dgray">

                    <i class="fa fa-calculator"></i>
            </div>

            </div>
        </div>
    </div>
    <!--Order Trip End-->
     <!--Order Trip-->
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
            <div class="inner">
                <h3>{{ isset($data["data"]["totalVendorOrder"]) ? $data["data"]["totalVendorOrder"] : '   0' }}</h3>
                <p>Total Order</p>
            </div>
            <div class="icon bg-yellow">
              <i class="fa fa-truck"></i>
            </div>

            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
            <div class="inner">
                <h3>{{ isset($data["data"]["todayVendorOrder"]) ? $data["data"]["todayVendorOrder"] : '   0' }}</h3>
                <p>Today's Order</p>
            </div>
            <div class="icon bg-dblue">
               <i class="fa fa-truck"></i>
            </div>

            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
            <div class="inner">
                <h3>{{ isset($data["data"]["vendorOrderOfThisWeek"]) ? $data["data"]["vendorOrderOfThisWeek"] : '   0' }}</h3>
                <p>This Week Order</p>
            </div>
            <div class="icon bg-mpinch">
               <i class="fa fa-truck"></i>
            </div>

            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
            <div class="inner">
                <h3>{{ isset($data["data"]["vendorOrderOfThisMonth"]) ? $data["data"]["vendorOrderOfThisMonth"] : '   0' }}</h3>
                <p>This Month Order</p>
            </div>
            <div class="icon bg-dgray">
                <i class="fa fa-truck"></i>
            </div>

            </div>
        </div>
    </div>
    <!--Order Trip End-->
    <div class="clearfix"></div>
     <!--Latest Orders start-->
     <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="data-box">
                <div class="data-box-header with-border">
                    <h2 class="box-title">Orders In Queue</h2>
                </div>
                <div class="data-box-body">
                    <div class="comn-table latest-order-table table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Timer</th>
                                <th>Order Id</th>
                                <!-- <th>Category</th> -->
                                <!-- <th>Sub Category</th> -->
                                <th>Qty (Cu. Mtr)</th>
                                <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>Order Date & Time</th>
                                 <!-- <th>Bill Status</th> -->
                                <th>Order Status</th>
                                <!-- <th>Payment Status</th> -->
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($data["queue_orders"]) && count($data["queue_orders"]) > 0)
                            <?php //dd($data["queue_orders"]);
                                $second = 1000;
                                $minute = $second * 60;
                                $hour = $minute * 60;
                            ?>
                            <script defer>

                                const second = 1000,
                                minute = second * 60,
                                hour = minute * 60;

                            </script>
                                @foreach($data["queue_orders"] as $value)
                                <?php $is_time_out = 0; ?>
                                <tr>
                                    <td>
                                        @if(isset($value['max_accepted_at']) && ($value["order_status"] == 'RECEIVED'))
                                        <?php
                                            $current_time = time();
                                            $max_quote_time = strtotime($value['max_accepted_at']);
                                            // echo (date('d M Y, h:i:s',$current_time));
                                            // echo (date('d M Y, h:i:s',strtotime($value['max_accepted_at'])));

                                            // $date1 = new DateTime();
                                            // $date2 = new DateTime($value['max_accepted_at']);

                                            // $diff = $date2->diff($date1);

                                            // Call the format method on the DateInterval-object
                                            // echo $diff->format('%a Day and %h hours %i minutes');

                                            $final_diff = $max_quote_time - $current_time;
                                            // dd($max_quote_time.'...'.$current_time.'...'.$final_diff);
                                            $final_hour = round($final_diff / ($hour));
                                            $final_min = round(($final_diff % ($hour)) / ($minute));
                                            $final_sec = round(($final_diff % ($minute)) / $second);



                                            $final_hour = $final_hour > 0 ? $final_hour : 0;
                                            $final_min = $final_min > 0 ? $final_min : 0;
                                            $final_sec = $final_sec > 0 ? $final_sec : 0;

                                            // $final_hour = $diff->format('%h');
                                            // $final_min = $diff->format('%i');
                                            // $final_sec = $diff->format('%s');

                                            // echo "| ".$final_hour;
                                            // echo " | ".$final_min;
                                            // echo " | ".$final_sec;

                                        ?>

                                            <div class="custom_timer" id="timer_div_{{$value['_id']}}">

                                                @if($final_hour > 0 || $final_min > 0 || $final_sec > 0)
                                                    <input type="hidden" id="timer_hid_{{$value['_id']}}" value="{{ date('d M, Y H:i:s', strtotime($value['max_accepted_at'])) }}" />
                                                    <ul>
                                                        <li><span id="hours_{{$value['_id']}}"></span></li>
                                                        <li><span id="minutes__{{$value['_id']}}"></span></li>
                                                        <li><span id="seconds__{{$value['_id']}}"></span></li>
                                                    </ul>
                                                @else
                                                    <span class="text-danger">TimeOut</span>
                                                    <?php $is_time_out = 1; ?>
                                                @endif
                                            </div>
                                            <script defer>

                                                <?php //$time =   strtotime($value['max_accepted_at']) ?>

                                                x = setInterval(function() {
                                                    var time = $("#timer_hid_{{$value['_id']}}").val();
                                                    let countDown = new Date(time).getTime();
                                                    // console.log("countDown..."+countDown);
                                                    let current_date = new Date();
                                                    let now = current_date.getTime();
                                                    // console.log("time..."+time+"..."+current_date);
                                                    distance = countDown - now;

                                                    var diff_as_date = new Date(distance);
                                                    var getHours = diff_as_date.getHours();  // hours
                                                    var getMinutes = diff_as_date.getMinutes(); // minutes
                                                    var getSeconds = diff_as_date.getSeconds(); // seconds

                                                    // console.log("getHours..."+getHours+"...getMinutes..."+getMinutes+"...getSeconds..."+getSeconds);

                                                    // var final_hour = Math.round(distance  / (hour));
                                                    // var final_min = Math.round((distance % (hour)) / (minute));
                                                    // var final_sec = Math.round((distance % (minute)) / second);

                                                    // final_hour = final_hour > 0 ? final_hour : 0;
                                                    // final_min = final_min > 0 ? final_min : 0;
                                                    // final_sec = final_sec > 0 ? final_sec : 0;

                                                    final_hour = getHours;
                                                    final_min = getMinutes;
                                                    final_sec = getSeconds;


                                                    if(final_hour > 0 || final_min > 0 || final_sec > 0){
                                                        document.getElementById("hours_{{$value['_id']}}").innerText = final_hour,
                                                        document.getElementById("minutes__{{$value['_id']}}").innerText = final_min,
                                                        document.getElementById("seconds__{{$value['_id']}}").innerText = final_sec;
                                                    }else{
                                                        $("#timer_div_{{$value['_id']}}").html('<span class="text-danger">TimeOut</span>');
                                                        $("#action_btn_{{$value['_id']}}").html('<span class="text-danger">No Action</span>');
                                                    }

                                                }, second)
                                            </script>

                                        @else
                                                <span class="text-danger">-</span>
                                                <?php $is_time_out = 1; ?>

                                        @endif
                                    </td>
                                    <td>
                                        @if(($is_time_out == 1) && ($value["order_status"] == 'RECEIVED'))
                                                #{{ isset($value["_id"]) ? $value["_id"] : '' }}
                                        @else
                                            @if($value["order_status"] == 'REJECTED')
                                                #{{ isset($value["_id"]) ? $value["_id"] : '' }}
                                            @else
                                                <a href="{{ route('supplier_orders_details',$value['_id']) }}">#{{ isset($value["_id"]) ? $value["_id"] : '' }}</a>
                                            @endif
                                        @endif
                                    </td>
                                    <!-- <td>{{ isset($value["product_category"]["category_name"]) ? $value["product_category"]["category_name"] : '' }}</td> -->
                                    <!-- <td>{{ isset($value["product_sub_category"]["sub_category_name"]) ? $value["product_sub_category"]["sub_category_name"] : '' }}</td> -->
                                    <td>{{ isset($value["quantity"]) ? round($value["quantity"]) : '0' }}</td>
                                    <td>{{ isset($value["unit_price"]) ? number_format($value["unit_price"],2) : '0' }}</td>
                                    <td>{{ isset($value["total_amount"]) ? number_format($value["total_amount"],2) : '0' }}</td>
                                    <!-- <td>{{ isset($value["buyer"]["full_name"]) ? $value["buyer"]["full_name"] : '' }}</td> -->
                                    <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                    <!-- <td><span class="badge bg-red">Rejected</span></td> -->
                                    @if(isset($value["order_status"]))
                                        @if($value["order_status"] == 'DELIVERED' || $value["order_status"] == 'ACCEPTED')
                                            <td><div class="badge bg-green">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                        @else @if(($value["order_status"] == 'CANCELLED') || ($value["order_status"] == 'REJECTED') || ($value["order_status"] == 'LAPSED'))
                                            <td><div class="badge bg-red">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                        @else @if($value["order_status"] == 'RECEIVED')
                                            <td><div class="badge bg-blue">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                        @else @if($value["order_status"] == 'PICKUP')
                                            <td><div class="badge bg-purple">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>

                                        @else
                                            <td><div class="badge bg-yellow">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                        @endif
                                        @endif
                                        @endif
                                        @endif
                                    @else
                                        <td>-</td>
                                    @endif
                                    <td id="action_btn_{{$value['_id']}}">

                                            @if(($value["order_status"] == 'RECEIVED') || ($value["order_status"] == 'ORDER_RECEIVED'))
                                                @if($is_time_out == 0)
                                                    <button class="site-button green button-sm" type="button">
                                                        <span class="dropdown-label" onclick="confirmPopup(1,'{{ json_encode($value) }}','{{ csrf_token() }}','Are you sure you want to accept this order?')">Accept</span>

                                                    </button>
                                                    <button class="site-button red button-sm" type="button">
                                                        <span class="dropdown-label" onclick="confirmPopup(2,'{{ json_encode($value) }}','{{ csrf_token() }}','Are you sure you want to reject this order?')">Reject</span>

                                                    </button>
                                                @else
                                                    <span class="text-danger">No Action</span>
                                                @endif
                                            @else
                                                @if(isset($value["order_status"]))
                                                    @if($value["order_status"] == 'REJECTED')
                                                        <span class="text-danger">Rejected</span>
                                                    @else

                                                        <span class="text-success">Accepted</span>

                                                    @endif
                                                @endif
                                            @endif


                                        <div class="dropdown cstm-dropdown-select">

                                            <!-- <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('order_status','{{ json_encode($value) }}')">Change Delivery Status</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('payment_status','{{ json_encode($value) }}')">Change Payment Status</a></li>
                                                <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('cancel_status','{{ json_encode($value) }}')">Cancel</a></li>
                                            </ul> -->
                                        </div>
                                    </td>
                                    <!-- <td><div class="badge bg-red">{{ isset($value["payment_status"]) ? $value["payment_status"] : '' }}</div></td> -->
                                    <!-- <td>
                                        <div class="badge bg-yellow">
                                            {{ isset($value["delivery_status"]) ? $value["delivery_status"] : '' }}
                                        </div>
                                    </td> -->
                                </tr>

                                @endforeach
                            @else
                                   <td colspan="10">No Record Found</td>
                            @endif


                        </tbody>
                    </table>
                    </div>
                </div>
                <div class="data-box-footer clearfix">
                    <div class="pull-right">
                        @if(isset($data["queue_orders"]) && count($data["queue_orders"]) > 0)
                            <a href="{{ route('orders_view') }}" class="site-button dark">View All</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Latest Orders end-->
    <!--Latest Orders start-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="data-box">
                <div class="data-box-header with-border">
                    <h2 class="box-title">Latest Orders</h2>
                </div>
                <div class="data-box-body">
                    <div class="comn-table latest-order-table table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Order Id</th>
                                <!-- <th>Category</th> -->
                                <!-- <th>Sub Category</th> -->
                                <th>Qty (Cu. Mtr)</th>
                                <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>Order Date & Time</th>
                                 <!-- <th>Bill Status</th> -->
                                <th>Order Status</th>
                                <!-- <th>Payment Status</th> -->

                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($data["latest_orders"]) && count($data["latest_orders"]) > 0)
                                @foreach($data["latest_orders"] as $value)
                                <tr>

                                    <td><a href="{{ route('supplier_orders_details',$value['_id']) }}">#{{ isset($value["_id"]) ? $value["_id"] : '' }}</a></td>
                                    <!-- <td>{{ isset($value["product_category"]["category_name"]) ? $value["product_category"]["category_name"] : '' }}</td> -->
                                    <!-- <td>{{ isset($value["product_sub_category"]["sub_category_name"]) ? $value["product_sub_category"]["sub_category_name"] : '' }}</td> -->
                                    <td>{{ isset($value["quantity"]) ? round($value["quantity"]) : '0' }}</td>
                                    <td>{{ isset($value["unit_price"]) ? number_format($value["unit_price"],2) : '0' }}</td>
                                    <td>{{ isset($value["total_amount"]) ? number_format($value["total_amount"],2) : '0' }}</td>
                                    <!-- <td>{{ isset($value["buyer"]["full_name"]) ? $value["buyer"]["full_name"] : '' }}</td> -->
                                    <!-- <td>{{ isset($value["created_at"]) ? date('d M Y, h:i:s a', strtotime($value["created_at"])) : '' }}</td> -->

                                    <td>{{ isset($value["created_at"]) ?
                                            AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                    <!-- <td><span class="badge bg-red">Rejected</span></td> -->
                                        @if(isset($value["order_status"]))
                                            @if($value["order_status"] == 'DELIVERED' || $value["order_status"] == 'ACCEPTED')
                                                <td><div class="badge bg-green">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                            @else @if($value["order_status"] == 'CANCELLED' || $value["order_status"] == 'REJECTED'|| ($value["order_status"] == 'LAPSED'))
                                                <td><div class="badge bg-red">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                            @else @if($value["order_status"] == 'RECEIVED')
                                                <td><div class="badge bg-blue">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                            @else @if($value["order_status"] == 'PICKUP')
                                                <td><div class="badge bg-purple">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>

                                                @else
                                                    <td><div class="badge bg-yellow">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @endif
                                            @endif
                                            @endif
                                            @endif
                                    @else
                                        <td>-</td>
                                    @endif
                                    <!-- <td>
                                        <div class="badge bg-red">{{ isset($value["payment_status"]) ? $value["payment_status"] : '' }}</div>
                                    </td> -->
                                    <!-- <td>
                                        <div class="badge bg-yellow">
                                            {{ isset($value["delivery_status"]) ? $value["delivery_status"] : '' }}
                                        </div>
                                    </td> -->
                                </tr>

                                @endforeach
                            @else
                                   <td colspan="10">No Record Found</td>
                            @endif


                        </tbody>
                    </table>
                    </div>
                </div>
                <div class="data-box-footer clearfix">
                    <div class="pull-right">
                        @if(isset($data["latest_orders"]) && count($data["latest_orders"]) > 0)
                            <a href="{{ route('orders_view') }}" class="site-button dark">View All</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Latest Orders end-->
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <!--Graph block start-->
    @php
        $profile = session('supplier_profile_details', null);
        //dd($profile);
    @endphp
        <?php
        $current_year = date("Y");
        $total_years = 1;
        if(isset($profile["current_year"])){
            $regestered_year = $profile["current_year"];

            $total_years = $current_year- $regestered_year;
            if($total_years == 0){
                $total_years = 1;
            }
        }else{
            $regestered_year = $current_year;
        }




        ?>


    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">

            <div class="data-box">

                <div class="data-box-header with-border data-box-header-dropdown">
                    <h2 class="box-title">Monthly Report</h2>

                    <div class="cust_datepicker_outer">
                        <div class="cstm-select-box">
                            <select id="monthly_chart_data_status_filter" class="form-control">
                                <option disabled="disabled" selected="selected">Order Status</option>
                                <option value="RECEIVED" title="RECEIVED">RECEIVED</option>
                                <option value="ACCEPTED" title="ACCEPTED">ACCEPTED</option>
                                <option value="REJECTED" title="REJECTED">REJECTED</option>
                                <option value="TM_ASSIGNED" title="TM ASSIGNED">TM ASSIGNED</option>
                                <option value="PICKUP" title="PICKUP">PICKUP</option>
                                <option value="DELAYED" title="DELAYED">DELAYED</option>
                                <option value="DELIVERED" title="DELIVERED">DELIVERED</option>
                                <option value="CANCELLED" title="CANCELLED">CANCELLED</option>
                            </select>
                        </div>
                        <div class="cstm-select-box m-l10">
                            <select id="monthly_chart_data_year_filter" class="form-control">
                                <option disabled="disabled" selected="selected">Select Year</option>
                                <?php $my_year = $current_year; ?>
                                @for($i=0;$i<$total_years;$i++)
                                    <option value="{{ $my_year }}">{{ $my_year }}</option>

                                    <?php $my_year = $my_year - 1; ?>
                                @endfor
                            </select>
                        </div>
                    </div>

                </div>
                <div id="monthly_chart_loader" class="overlay" style="display:none">
                    <span class="spinner"></span>
                </div>
                <div class="data-box-body">
                    <!-- <div id="monthly-recap-report"></div> -->
                    <div id="monthly-report-chart"></div>

                    <div class="chart_checkbox d-flex" id="monthly_chart">
                        <div class="pretty_checkbox check_blue">
                            <input type="checkbox" id="monthly_chart_order" checked onclick="toggleMonthlyChartSeries(this)" value="Orders">
                            <label for="monthly_chart_order">Orders</label>
                        </div>
                        <div class="pretty_checkbox check_green">
                            <input type="checkbox" id="monthly_chart_quntity" checked onclick="toggleMonthlyChartSeries(this)" value="Quantity">
                            <label for="monthly_chart_quntity">Qty</label>
                        </div>
                        <div class="pretty_checkbox check_yellow">
                            <input type="checkbox" id="monthly_chart_salse" checked onclick="toggleMonthlyChartSeries(this)" value="Sales Amount">
                            <label for="monthly_chart_salse">Sales Amount</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
            <div class="data-box">
                <div class="data-box-header with-border data-box-header-dropdown">
                    <h2 class="box-title">Daily Report</h2>

                    <div class="cust_datepicker_outer">
                            <div class="cust_datepicker">
                                <input type="text" id="daily_chart_year_filter" class="form-control from" readonly="" placeholder="Select Year Month">
                            </div>
                        <div class="cstm-select-box m-l10">
                            <select id="daily_chart_status_filter" class="form-control">
                                <option disabled="disabled" selected="selected">Order Status</option>
                                <option value="RECEIVED" title="RECEIVED">RECEIVED</option>
                                <option value="ACCEPTED" title="ACCEPTED">ACCEPTED</option>
                                <option value="REJECTED" title="REJECTED">REJECTED</option>
                                <option value="TM_ASSIGNED" title="TM ASSIGNED">TM ASSIGNED</option>
                                <option value="PICKUP" title="PICKUP">PICKUP</option>
                                <option value="DELAYED" title="DELAYED">DELAYED</option>
                                <option value="DELIVERED" title="DELIVERED">DELIVERED</option>
                                <option value="CANCELLED" title="CANCELLED">CANCELLED</option>
                            </select>
                        </div>
                    </div>





                </div>
                <div id="daily_chart_loader" class="overlay" style="display:none">
                    <span class="spinner"></span>
                </div>
                <div class="data-box-body">
                    <div id="daily-report-chart"></div>

                    <div class="chart_checkbox d-flex" id="daily_chart_legend">
                        <div class="pretty_checkbox check_blue">
                            <input id="daily_chart_legend_order" type="checkbox" checked onclick="toggleDailyChartSeries(this)" value="Orders">
                            <label for="daily_chart_legend_order">Orders</label>
                        </div>

                        <div class="pretty_checkbox check_green">
                            <input id="daily_chart_legend_quantity" type="checkbox" checked onclick="toggleDailyChartSeries(this)" value="Quantity">
                            <label for="daily_chart_legend_quantity">Qty</label>
                        </div>

                        <div class="pretty_checkbox check_yellow">
                            <input id="daily_chart_legend_sales" type="checkbox" checked onclick="toggleDailyChartSeries(this)" value="Sales Amount">
                            <label for="daily_chart_legend_sales">Sales Amount</label>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>

    <!--Graph block End-->
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">

            <div class="data-box">
                <div class="data-box-header with-border data-box-header-dropdown">
                    <h2 class="box-title">Product Monthly Report</h2>

                    <div class="cust_datepicker_outer">

                        <div class="cstm-select-box">
                            <select id="product_monthly_chart_data_year_filter" class="form-control">
                                <option disabled="disabled" selected="selected">Select Year</option>
                                <?php $my_year = $current_year; ?>
                                @for($i=0;$i<$total_years;$i++)
                                    <option value="{{ $my_year }}">{{ $my_year }}</option>

                                    <?php $my_year = $my_year - 1; ?>
                                @endfor

                            </select>
                        </div>
                        <div class="cstm-select-box m-l10">
                            <select id="product_monthly_chart_data_type_filter" class="form-control">
                                <option>Select Type</option>
                                <option value="custom">Custom mix</option>
                                <option value="standard">Standard</option>
                            </select>
                        </div>
                        <div class="cstm-select-box m-l10 minw140">
                            <select name="concrete_grade_id" id="concrete_grade_id" class="form-control">
                                <option disabled="disabled" selected="selected">Concrete Grade</option>
                                @if(isset($data["concrete_grade_data"]))
                                    @foreach($data["concrete_grade_data"] as $value)
                                        <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="cstm-select-box m-l10" id="sub_category_id_div" style="display:none">
                            <select id="sub_category_id" class="form-control">


                            </select>
                        </div> -->
                    </div>



                </div>
                <div id="product_monthly_chart_loader" class="overlay" style="display:none">
                    <span class="spinner"></span>
                </div>
                <div class="data-box-body">
                    <!-- <div id="monthly-recap-report"></div> -->
                    <div id="product-monthly-report-chart"></div>

                    <div class="chart_checkbox d-flex" id="product_monthly_chart_legend">
                        <div class="pretty_checkbox check_blue">
                            <input id="pro_month_order" type="checkbox" checked onclick="toggleProductMonthlyChartSeries(this)" value="Orders">
                            <label for="pro_month_order">Orders</label>
                        </div>

                        <div class="pretty_checkbox check_green">
                            <input id="pro_month_quantity" type="checkbox" checked onclick="toggleProductMonthlyChartSeries(this)" value="Quantity">
                            <label for="pro_month_quantity">Qty</label>
                        </div>

                        <div class="pretty_checkbox check_yellow">
                            <input id="pro_month_sales" type="checkbox" checked onclick="toggleProductMonthlyChartSeries(this)" value="Sales Amount">
                            <label for="pro_month_sales">Sales Amount</label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
            <div class="data-box">
                <div class="data-box-header with-border data-box-header-dropdown">
                    <h2 class="box-title">Product Daily Report</h2>


                    <div class="cust_datepicker_outer">
                        <div class="cust_datepicker">
                            <input type="text" id="product_daily_chart_data_year_filter" class="form-control from" readonly="" placeholder="Select Year Month">
                        </div>
                        <div class="cstm-select-box m-l10">
                            <select id="product_daily_chart_data_type_filter" class="form-control">
                                <option>Select Type</option>
                                <option value="custom">Custom mix</option>
                                <option value="standard">Standard</option>
                            </select>
                        </div>
                        <div class="cstm-select-box m-l10 minw140">
                            <select name="concrete_grade_id" id="daily_concrete_grade_id" class="form-control">
                                <option disabled="disabled" selected="selected">Concrete Grade</option>
                                @if(isset($data["concrete_grade_data"]))
                                    @foreach($data["concrete_grade_data"] as $value)
                                        <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <!-- <div class="cstm-select-box m-l10" id="daily_sub_category_id_div" style="display:none">
                            <select id="daily_sub_category_id" class="form-control">


                            </select>
                        </div> -->
                    </div>
                </div>
                <div id="product_daily_chart_loader" class="overlay" style="display:none">
                    <span class="spinner"></span>
                </div>
                <div class="data-box-body">
                    <div id="product-daily-report-chart"></div>

                    <div class="chart_checkbox d-flex" id="product_daily_chart_legend">
                        <div class="pretty_checkbox check_blue">
                            <input id="pro_daily_order" type="checkbox" checked onclick="toggleProductDailyChartSeries(this)" value="Orders">
                            <label for="pro_daily_order">Orders</label>
                        </div>

                        <div class="pretty_checkbox check_green">
                            <input id="pro_daily_qty" type="checkbox" checked onclick="toggleProductDailyChartSeries(this)" value="Quantity">
                            <label for="pro_daily_qty">Qty</label>
                        </div>

                        <div class="pretty_checkbox check_yellow">
                            <input id="pro_daily_sales" type="checkbox" checked onclick="toggleProductDailyChartSeries(this)" value="Sales Amount">
                            <label for="pro_daily_sales">Sales Amount</label>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <!--Stock / Recently added product start -->
    <div class="empty_stock_bx" id="stock-block">
        <div class="row">
            <div class="col-md-6">
                <div class="data-box">
                    <div class="data-box-header with-border">
                        <h2 class="box-title">Cement Out Of Stock </h2>
                        Minimum Required Stock : {{ isset($data["product_stock"]["cement_total_quantity"]) ? $data["product_stock"]["cement_total_quantity"] : 0 }} Kg
                            
                    </div>
                    <div style="padding-bottom:15px;">
                        <h2 class="box-title"><span>(As on {{ date('d M Y') }})</span></h2>
                    </div>
                    <div>
                        <b>Note:</b><i>If your stock is below 99% of required stock Qty then your plant product will not be listed.</i>
                    </div>
                    <div class="data-box-body">
                        <div class="stock-block">
                            <ul class="box-li">
                                <?php //dd($data["product_stock"]); ?>
                                @if(isset($data["product_stock"]["cementData"]) && count($data["product_stock"]["cementData"]) > 0)
                                    @foreach($data["product_stock"]["cementData"] as $value)
                                        <li>
                                            <div class="stock-content-block">
                                                <!-- <div class="product-img">
                                                    <img src="" alt="">
                                                </div> -->
                                                <div class="stock-details-block">
                                                    <h3>{{ $value["cement_brand"]["name"] }} - {{ $value["cement_grade"]["name"] }}</h3>
                                                    <div class="stock-use-block">
                                                        <span class="text-red">{{ $value["quantity"] }} Kg</span> / {{ $data["product_stock"]["cement_total_quantity"] }} Kg
                                                    </div>
                                                    <div class="progress-status-block">
                                                        <div id="{{ $value['_id'] }}_cement"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <a href="javascript:;" onclick="editProductStock()" class="site-button gray button-sm change-stock-link" data-toggle="modal" data-target="#change-stock">Update Stock</a> -->
                                        </li>

                                        <script defer>
                                            $(document).ready(function(){
                                                var total_qty = '{{ $data["product_stock"]["cement_total_quantity"] }}';
                                                var remain_qty = '{{ isset($value['quantity']) ? $value['quantity'] : 0 }}';

                                                var percentage = (remain_qty * 100) / total_qty;
                                                percentage = 100 - percentage;
                                                var in_stock = "% In Stock";
                                                if(percentage > 0){
                                                    in_stock = "% Out Of Stock";
                                                }
                                                // var percentage = 90;
                                                /*progress bar script start*/
                                                if(percentage <= 25){
                                                    $('#{{ $value['_id'] }}_cement').LineProgressbar({
                                                        percentage: percentage,
                                                        fillBackgroundColor: '#00a65a',
                                                        unit: in_stock
                                                    });

                                                }else if((percentage > 25) && (percentage <= 50)){

                                                    $('#{{ $value['_id'] }}_cement').LineProgressbar({
                                                        percentage: percentage,
                                                        fillBackgroundColor: '#00c0ef',
                                                        unit: in_stock
                                                    });

                                                }else if((percentage > 50) && (percentage <= 75)){

                                                    $('#{{ $value['_id'] }}_cement').LineProgressbar({
                                                        percentage: percentage,
                                                        fillBackgroundColor: '#ff9600',
                                                        unit: in_stock
                                                    });

                                                }else if((percentage > 75) && (percentage <= 100)){

                                                    $('#{{ $value['_id'] }}_cement').LineProgressbar({
                                                        percentage: percentage,
                                                        fillBackgroundColor: '#dd4b39',
                                                        unit: in_stock
                                                    });

                                                }
                                                
                                                if(percentage < 0){

                                                    $('#{{ $value['_id'] }}_cement').LineProgressbar({
                                                        percentage: 100,
                                                        fillBackgroundColor: '#00a65a',
                                                        unit: in_stock
                                                    });

                                                }

                                                if(percentage > 0){

                                                    $("#stock_strip").show();

                                                }

                                            /*progress bar script end*/
                                        });
                                        </script>

                                    @endforeach
                                @else
                                    <li>Cement is in stock</li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="data-box">
                    <div class="data-box-header with-border">
                        <h2 class="box-title">Sand Out Of Stock </h2>
                        Minimum Required Stock : {{ isset($data["product_stock"]["sand_total_quantity"] ) ? $data["product_stock"]["sand_total_quantity"] : 0 }} Kg
                    </div>
                    <div style="padding-bottom:15px;">
                        <h2 class="box-title"><span>(As on {{ date('d M Y') }})</span></h2>
                    </div>
                    <div>
                        <b>Note:</b><i>If your stock is below 99% of required stock Qty then your plant product will not be listed.</i>
                    </div>
                    <div class="data-box-body">
                        <div class="stock-block">
                            <ul class="box-li">
                                <?php //dd($data["product_stock"]); ?>
                                @if(isset($data["product_stock"]["sandData"]) && count($data["product_stock"]["sandData"]) > 0)
                                    @foreach($data["product_stock"]["sandData"] as $value)
                                        <li>
                                            <div class="stock-content-block">
                                                <!-- <div class="product-img">
                                                    <img src="" alt="">
                                                </div> -->
                                                <div class="stock-details-block">
                                                    <h3>{{ $value["sand_source"]["sand_source_name"] }}</h3>
                                                    <div class="stock-use-block">
                                                        <span class="text-red">{{ $value["quantity"] }} Kg</span> / {{ $data["product_stock"]["sand_total_quantity"] }} Kg
                                                    </div>
                                                    <div class="progress-status-block">
                                                        <div id="{{ $value['_id'] }}_sand"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <a href="javascript:;" onclick="editProductStock()" class="site-button gray button-sm change-stock-link" data-toggle="modal" data-target="#change-stock">Update Stock</a> -->
                                        </li>

                                        <script defer>
                                            $(document).ready(function(){
                                            var total_qty = '{{ $data["product_stock"]["sand_total_quantity"] }}';
                                            var remain_qty = '{{ isset($value['quantity']) ? $value['quantity'] : 0 }}';

                                            var percentage = (remain_qty * 100) / total_qty;
                                            percentage = 100 - percentage;
                                            var in_stock = "% In Stock";
                                            if(percentage > 0){
                                                in_stock = "% Out Of Stock";
                                            }
                                            // var percentage = 90;
                                            /*progress bar script start*/
                                            if(percentage <= 25){
                                                $('#{{ $value['_id'] }}_sand').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#00a65a',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 25) && (percentage <= 50)){

                                                $('#{{ $value['_id'] }}_sand').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#00c0ef',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 50) && (percentage <= 75)){

                                                $('#{{ $value['_id'] }}_sand').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#ff9600',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 75) && (percentage <= 100)){

                                                $('#{{ $value['_id'] }}_sand').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#dd4b39',
                                                    unit: in_stock
                                                });

                                            }

                                            if(percentage < 0){

                                                $('#{{ $value['_id'] }}_sand').LineProgressbar({
                                                    percentage: 100,
                                                    fillBackgroundColor: '#00a65a',
                                                    unit: in_stock
                                                });

                                                }

                                            if(percentage > 0){

                                                $("#stock_strip").show();

                                            }

                                            /*progress bar script end*/
                                        });
                                        </script>

                                    @endforeach
                                @else
                                    <li>Sand is in stock</li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="data-box">
                    <div class="data-box-header with-border">
                        <h2 class="box-title">Aggregate Out Of Stock </h2>
                        <!-- Minimum Required Stock : {{ isset($data["product_stock"]["aggData"][0]["agg_total_quantity"]) ? $data["product_stock"]["aggData"][0]["agg_total_quantity"] . ' Kg' : 'Kg' }} -->
                    </div>
                    <div style="padding-bottom:15px;">
                        <h2 class="box-title"><span>(As on {{ date('d M Y') }})</span></h2>
                    </div>
                    <div>
                        <b>Note:</b><i>If your stock is below 99% of required stock Qty then your plant product will not be listed.</i>
                    </div>
                    <div class="data-box-body">
                        <div class="stock-block">
                            <ul class="box-li">
                                <?php //dd($data["product_stock"]); ?>
                                @if(isset($data["product_stock"]["aggData"]) && count($data["product_stock"]["aggData"]) > 0)
                                    @foreach($data["product_stock"]["aggData"] as $value)
                                        <li>
                                            <div class="stock-content-block">
                                                <!-- <div class="product-img">
                                                    <img src="" alt="">
                                                </div> -->
                                                <div class="stock-details-block">
                                                    <h3>{{ isset($value["aggregate_source"]["aggregate_source_name"]) ? $value["aggregate_source"]["aggregate_source_name"] : '' }} - {{ $value["aggregate_sand_sub_category"]["sub_category_name"] }}</h3>
                                                    <div class="stock-use-block">
                                                        <span class="text-red">{{ $value["quantity"] }} Kg</span> / {{ $value["agg_total_quantity"] }} Kg
                                                    </div>
                                                    <div class="progress-status-block">
                                                        <div id="{{ $value['_id'] }}_aggregate"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <a href="javascript:;" onclick="editProductStock()" class="site-button gray button-sm change-stock-link" data-toggle="modal" data-target="#change-stock">Update Stock</a> -->
                                        </li>
                                        <script defer>
                                            var total_qty = '{{ $value["agg_total_quantity"] }}';
                                            var remain_qty = '{{ isset($value['quantity']) ? $value['quantity'] : 0 }}';

                                            var percentage = (remain_qty * 100) / total_qty;
                                            percentage = 100 - percentage;
                                            var in_stock = "% In Stock";
                                            if(percentage > 0){
                                                in_stock = "% Out Of Stock";
                                            }
                                            // var percentage = 90;
                                            /*progress bar script start*/
                                            if(percentage <= 25){
                                                $('#{{ $value['_id'] }}_aggregate').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#00a65a',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 25) && (percentage <= 50)){

                                                $('#{{ $value['_id'] }}_aggregate').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#00c0ef',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 50) && (percentage <= 75)){

                                                $('#{{ $value['_id'] }}_aggregate').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#ff9600',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 75) && (percentage <= 100)){

                                                $('#{{ $value['_id'] }}_aggregate').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#dd4b39',
                                                    unit: in_stock
                                                });

                                            }

                                            if(percentage < 0){

                                                $('#{{ $value['_id'] }}_aggregate').LineProgressbar({
                                                    percentage: 100,
                                                    fillBackgroundColor: '#00a65a',
                                                    unit: in_stock
                                                });

                                                }


                                            if(percentage > 0){

                                                $("#stock_strip").show();

                                            }



                                            /*progress bar script end*/

                                        </script>
                                    @endforeach
                                @else
                                    <li>Aggregate is in stock</li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="data-box">
                    <div class="data-box-header with-border">
                        <h2 class="box-title">Admixture Out Of Stock </h2>
                        Minimum Required Stock : {{ isset($data["product_stock"]["admix_total_quantity"]) ? $data["product_stock"]["admix_total_quantity"] : 0 }} Kg
                    </div>
                    <div style="padding-bottom:15px;">
                        <h2 class="box-title"><span>(As on {{ date('d M Y') }})</span></h2>
                    </div>
                    <div>
                        <b>Note:</b><i>If your stock is below 99% of required stock Qty then your plant product will not be listed.</i>
                    </div>
                    <div class="data-box-body">
                        <div class="stock-block">
                            <ul class="box-li">
                                <?php //dd($data["product_stock"]); ?>
                                @if(isset($data["product_stock"]["admixData"]) && count($data["product_stock"]["admixData"]) > 0)
                                    @foreach($data["product_stock"]["admixData"] as $value)
                                        <li>
                                            <div class="stock-content-block">
                                                <!-- <div class="product-img">
                                                    <img src="" alt="">
                                                </div> -->
                                                <div class="stock-details-block">
                                                    <h3>{{ $value["admixture_brand"]["name"] }} - {{ $value["admixture_category"]["category_name"] }} {{ $value["admixture_category"]["admixture_type"] }}</h3>
                                                    <div class="stock-use-block">
                                                        <span class="text-red">{{ $value["quantity"] }} Kg</span> / {{ $data["product_stock"]["admix_total_quantity"] }} Kg
                                                    </div>
                                                    <div class="progress-status-block">
                                                        <div id="{{ $value['_id'] }}_admix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <a href="javascript:;" onclick="editProductStock()" class="site-button gray button-sm change-stock-link" data-toggle="modal" data-target="#change-stock">Update Stock</a> -->
                                        </li>
                                        <script defer>
                                            var total_qty = '{{ $data["product_stock"]["admix_total_quantity"] }}';
                                            var remain_qty = '{{ isset($value['quantity']) ? $value['quantity'] : 0 }}';

                                            var percentage = (remain_qty * 100) / total_qty;
                                            percentage = 100 - percentage;
                                            var in_stock = "% In Stock";
                                            if(percentage > 0){
                                                in_stock = "% Out Of Stock";
                                            }
                                            // var percentage = 90;
                                            /*progress bar script start*/
                                            if(percentage <= 25){
                                                $('#{{ $value['_id'] }}_admix').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#00a65a',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 25) && (percentage <= 50)){

                                                $('#{{ $value['_id'] }}_admix').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#00c0ef',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 50) && (percentage <= 75)){

                                                $('#{{ $value['_id'] }}_admix').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#ff9600',
                                                    unit: in_stock
                                                });

                                            }else if((percentage > 75) && (percentage <= 100)){

                                                $('#{{ $value['_id'] }}_admix').LineProgressbar({
                                                    percentage: percentage,
                                                    fillBackgroundColor: '#dd4b39',
                                                    unit: in_stock
                                                });

                                            }

                                            if(percentage < 0){

                                                $('#{{ $value['_id'] }}_admix').LineProgressbar({
                                                    percentage: 100,
                                                    fillBackgroundColor: '#00a65a',
                                                    unit: in_stock
                                                });

                                            }


                                            if(percentage > 0){

                                                $("#stock_strip").show();

                                            }


                                            /*progress bar script end*/

                                        </script>
                                    @endforeach
                                @else
                                    <li>Admixture is in stock</li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            @php
                $profile = session('supplier_profile_details', null);
                //dd($profile);
            @endphp
            @if(isset($profile["master_vendor_id"]))
            <div class="col-md-6">
                <div class="data-box">
                    <div class="data-box-header with-border">
                        <h2 class="box-title">Day Capacity</h2>

                        <div class="cust_datepicker_outer">
                            <div class="cust_datepicker">
                                <input type="text" id="day_capacity_filter" class="form-control day_from" readonly="" placeholder="Select Date">
                            </div>
                        </div>

                    </div>

                        <div class="data-box-body">
                            <div class="stock-block">
                                <ul class="box-li">
                                    <?php //dd($data["product_stock"]); ?>

                                        <li>
                                            <div class="stock-content-block">
                                                <!-- <div class="product-img">
                                                    <img src="" alt="">
                                                </div> -->
                                                <div class="stock-details-block">
                                                    <h3>Production</h3>
                                                    <div class="stock-use-block">
                                                        <span class="text-red" id="day_capacity_value">0 Cu.Mtr</span> / <span id="total_day_capacity">0 Cu.Mtr</span>
                                                    </div>
                                                    <div class="progress-status-block">
                                                        <div id="day_capacity_progress"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <a href="javascript:;" onclick="editProductStock()" class="site-button gray button-sm change-stock-link" data-toggle="modal" data-target="#change-stock">Update Stock</a> -->
                                        </li>
                                        @if(false)
                                            <script defer>
                                                var total_qty = '{{ $data["product_stock"]["admix_total_quantity"] }}';
                                                var remain_qty = '{{ isset($value['quantity']) ? $value['quantity'] : 0 }}';

                                                var percentage = (remain_qty * 100) / total_qty;
                                                // var percentage = 90;
                                                /*progress bar script start*/
                                                if(percentage <= 25){
                                                    $('#{{ $value['_id'] }}').LineProgressbar({
                                                        percentage: percentage,
                                                        fillBackgroundColor: '#00a65a'
                                                    });

                                                }else if((percentage > 25) && (percentage <= 50)){

                                                    $('#{{ $value['_id'] }}').LineProgressbar({
                                                        percentage: percentage,
                                                        fillBackgroundColor: '#00c0ef'
                                                    });

                                                }else if((percentage > 50) && (percentage <= 75)){

                                                    $('#{{ $value['_id'] }}').LineProgressbar({
                                                        percentage: percentage,
                                                        fillBackgroundColor: '#ff9600'
                                                    });

                                                }else if((percentage > 75) && (percentage <= 100)){

                                                    $('#{{ $value['_id'] }}').LineProgressbar({
                                                        percentage: percentage,
                                                        fillBackgroundColor: '#dd4b39'
                                                    });

                                                }

                                                /*progress bar script end*/

                                            </script>
                                        @endif

                                </ul>
                            </div>
                        </div>

                </div>
            </div>
            @endif
            <div class="col-md-5" style="display:none">
                <div class="data-box">
                    <div class="data-box-header with-border">
                        <h2 class="box-title">Recently Added Product</h2>
                    </div>
                    <div class="data-box-body">
                        <div class="rap-block">
                            <ul class="box-li">
                                @if(isset($data["latest_products"]) && count($data["latest_products"]) > 0)
                                    @foreach($data["latest_products"] as $value)
                                        <li>
                                            <div class="rap-content-block">
                                                <div class="product-img">
                                                    <img src="{{ isset($value['productCategory']['image_url']) ? $value['productCategory']['image_url'] : '' }}" alt="">
                                                </div>
                                                <div class="rap-details-block">
                                                    <h3>{{ isset($value["productCategory"]["category_name"]) ? $value["productCategory"]["category_name"] : '' }} - {{ isset($value["productSubCategory"]["sub_category_name"]) ? $value["productSubCategory"]["sub_category_name"] : '' }} {{ isset($value["address"]["source_name"]) ? '-'.$value["address"]["source_name"] : '' }}<span>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span></h3>
                                                    <div class="rap-status-block">
                                                        <span class="badge {{ isset($value["verified_by_admin"]) ? ($value['verified_by_admin'] ? 'bg-green' : 'bg-red') : '' }}">{{ isset($value["verified_by_admin"]) ? ($value["verified_by_admin"] ? 'Veryfied' : 'Unveryfied') : '' }}</span>
                                                    </div>
                                                    <div class="rap-available-block">
                                                    {{ isset($value["quantity"]) ? $value["quantity"] : '' }} MT <span>({{ isset($value["is_available"]) ? ($value["is_available"] ? 'Available' : 'Not Available') : '' }})</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @else

                                    <li>No Record Found</li>

                                @endif

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Stock / Recently added product end -->
</div>

<script defer>

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];

    var current_date = new Date();

    var dd = String(current_date.getDate()).padStart(2, '0');
    var mm = String(current_date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = current_date.getFullYear();

    // current_date = dd + '/' + mm + '/' + yyyy;
    current_date = yyyy + '-' + mm + '-' + dd;
    display_date = dd + ' ' + monthNames[mm-1] + ' ' + yyyy ;

    $('.day_from').datepicker().val(display_date).trigger('change');

    getDayCapacity(current_date);

    </script>


@endsection
