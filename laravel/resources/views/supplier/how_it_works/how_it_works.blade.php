@extends('supplier.layouts.supplier_layout')
@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">How it Works</h1>
    <div class="clearfix"></div>
    <!-- my profile start -->
    <div class="how-it-block wht-tble-bg mb-3">
        <div class="how_it_tital">
            <h1>How it Works</h1>
            <p>It is a long established fact that a reader will be distracted</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx arrow_cust_right">
                    <div class="how_left">1</div>
                    <div class="how_right">
                        <h2>Plant Address</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx arrow_cust_right">
                    <div class="how_left">2</div>
                    <div class="how_right">
                        <h2>Add Design Mix</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx arrow_cust_btm">
                    <div class="how_left">3</div>
                    <div class="how_right">
                        <h2>TM Driver</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx arrow_cust_btm">
                    <div class="how_left">6</div>
                    <div class="how_right">
                        <h2>Concrete Pump (CP)</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx arrow_cust_left">
                    <div class="how_left">5</div>
                    <div class="how_right">
                        <h2>Transit Mixer (TM)</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx arrow_cust_left">
                    <div class="how_left">4</div>
                    <div class="how_right">
                        <h2>Concrete Pump Operator</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx arrow_cust_right">
                    <div class="how_left">7</div>
                    <div class="how_right">
                        <h2>Stock Update</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx arrow_cust_right">
                    <div class="how_left">8</div>
                    <div class="how_right">
                        <h2>Design Mix Contain Rate</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how_bx">
                    <div class="how_left">9</div>
                    <div class="how_right">
                        <h2>Rate Setting</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page </p>
                        <a href="#" class="text-uppercase">more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="how-it-block wht-tble-bg">
        <div class="how_it_tital">
            <h1>How To Place Order</h1>
            <p>It is a long established fact that a reader will be distracted</p>
        </div>
        <div class="how_flow pb-5">
            <div class="how_flow_bx">
                <div class="how_flow_text">
                    <h2>Accept / Reject  / Timer Order List</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into.</p>
                </div>
                <div class="how_flow_img">
                    <img src="{{asset('assets/supplier/images/p1.png')}}" alt="image">
                </div>
            </div>
            <div class="how_flow_bx_seprator">
                <img src="{{asset('assets/supplier/images/02.png')}}" alt="sepretor">
            </div>
            <div class="how_flow_bx flex-row-reverse">
                <div class="how_flow_text">
                    <h2>Buyer Assign Qty & Date in Order Detail</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into.</p>
                </div>
                <div class="how_flow_img">
                    <img src="{{asset('assets/supplier/images/p2.png')}}" alt="image">
                </div>
            </div>
            <div class="how_flow_bx_seprator how_flow_bx_seprator_ltr">
                <img src="{{asset('assets/supplier/images/02.png')}}" alt="sepretor">
            </div>
            <div class="how_flow_bx">
                <div class="how_flow_text">
                    <h2>Vendor Assign CP & Assign TM  With Qty</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into.</p>
                </div>
                <div class="how_flow_img">
                    <img src="{{asset('assets/supplier/images/p3.png')}}" alt="image">
                </div>
            </div>
            <div class="how_flow_bx_seprator">
                <img src="{{asset('assets/supplier/images/02.png')}}" alt="sepretor">
            </div>
            <div class="how_flow_bx flex-row-reverse">
                <div class="how_flow_text">
                    <h2>Supplier Tracking App Order Satus</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into.</p>
                </div>
                <div class="how_flow_img how_flow_img_small ">
                    <img src="{{asset('assets/supplier/images/p4.png')}}" alt="image">
                </div>
            </div>
        </div>
    </div>
    <!-- my profile end -->
</div>


@endsection