@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Suggestion For New Product</h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="proposal-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                
                    <div class="sub-title-block">
                        <h2 class="sub-title pull-left">Suggestion Product List</h2>
                    <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                    <button class="site-button pull-right  m-r10" onclick="resetForm('add_proposal_form','add-proposal')" data-toggle="modal" data-target="#add-proposal">Suggestion For New Product</button>
                </div>
                <div class="proposal-block">
                    @if(isset($data["data"]) && (count($data["data"]) > 0))
                        @php                            
                            $first_created_date = '';
                            $last_created_date = '';
                            $count = 0;
                        @endphp
                        @foreach($data["data"] as $value)
                            
                            @if($count == 0)
                                @php 
                                    $first_created_date = $value["requested_at"];
                                    $count++;
                                @endphp
                            @endif
                            @php
                                $last_created_date = $value["requested_at"];
                            @endphp
                            <div class="proposal-contet-block m-b20">
                                <!-- <a href="javascript:;" class="site-button red adrs-dlt-icon top-right-link-icon" data-toggle="modal" data-target="#view-map-details" title="Delete"><i class="fa fa-trash"></i></a>
                                <a href="javascript:;" class="site-button green adrs-edt-icon pull-right top-right-link-icon m-r30" data-toggle="modal" data-target="" title="Edit"><i class="fa fa-edit"></i></a> -->

                                <!--  <a href="javascript:;" class="site-button outline gray pull-right top-right-link-icon" data-toggle="modal" data-target="#compose-new-message"><i class="fa fa-envelope"></i></a> -->
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <h3 class="proposal-title">M M Patel Hardware</h3>
                                        <ul>
                                                <li>
                                            <p><span class="vTitle">Contact Person : </span> <span class="vName">{{ $value["contactDetails"]["person_name"] }}</span></p>
                                        </li>
                                            <li>
                                                <p><span class="vTitle">Category Name : </span> <span class="vName">{{ $value["category_name"] }}</span></p>
                                            </li>
                                            <li>
                                                <p><span class="vTitle">Sub Category : </span> <span class="vName">{{ $value["sub_category_name"] }}</span></p>
                                            </li>
                                            <li>
                                                <p><span class="vTitle">Qty In Stock : </span> <span class="vName">{{ $value["quantity"] }} MT</span></p>
                                            </li>
                                            
                                            <li>
                                                <p><span class="vTitle">Requested On : </span> <span class="vName">{{ date('d M Y',strtotime($value["requested_at"])) }}</span></p>
                                            </li>
                                            <!-- <li>
                                                <p><span class="vTitle">Notified On: </span> <span class="vName">18 Jul 2019</span></p>
                                            </li> -->
                                            <li>
                                                <p><span class="vTitle">Status : </span> <span class="vName">
                                                <span class="badge {{ $value['status'] == 'Unverified' ? 'bg-red' : 'bg-green' }}">{{ $value["status"] }}</span></span></p>
                                            </li>
                                            <li>
                                                <p class="proposal-pickup-address">
                                                    <span class="vTitle">Pick up Address : </span> 
                                                    <span class="vName cstm-tooltip" data-direction="bottom">
                                                        <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View</a>
                                                        <span class="cstm-tooltip__item">
                                                            <span>{{ $value["address"]["line1"] }}, {{ $value["address"]["line2"] }} <br> {{ $value["address"]["city_name"] }} {{ $value["address"]["pincode"] }}, <br> {{ $value["address"]["state_name"] }} (INDIA)</span>
                                                        </span>
                                                    </span>
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="proposal-message-block">
                                            <h3>Description :</h3>
                                            <div class="msg_readmore">
                                                {{ $value["description"] }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p style="text-align: center;">No Record Found</p>
                    @endif
                    
                    
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                        @if(isset($data['data']) && (count($data['data']) >=  ApiConfig::PAGINATION_LIMIT))
                            
                            <div class="pagination justify-content-end m-0">
                                @if(isset($first_created_date))
                                <form action="{{ route('supplier_product_filters') }}" method="get">
                                    
                                    
                                    
                                    <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                    <button type="submit" class="site-button">Previous</button>                                    
                                </form>
                                @endif
                                @if(isset($last_created_date))
                                <form action="{{ route('supplier_product_filters') }}" method="get">
                                    
                                    
                                    
                                    <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                    <button type="submit" class="site-button">Next</button>
                                </form>
                                @endif
                            </div>
                            
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

@endsection