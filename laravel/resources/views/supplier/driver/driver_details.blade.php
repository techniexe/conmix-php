@section('title', 'Supplier TM Drivers')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="main-title">TM Driver Detail</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="contact-detail-main-block wht-tble-bg">
        <div class="sub-title-block">
            <h2 class="sub-title pull-left">TM Driver Detail</h2>
            <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
            <a href="{{ route('supplier_driver_details') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
            @if(isset($data['data']) && count($data['data']) > 0)
                <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
            @endif
        </div>
        <div class="row">

            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="contact-detail-block add-contact-detail-block">
                            <a href="javascript:;" onclick="resetForm('add_driver_form','add-bank-details')" data-toggle="modal" data-target="#add-bank-details"><i>+</i></a>
                        </div>
                    </li>

                    @if(isset($data["data"]))
                        @foreach($data["data"] as $value)
                            <li>
                                
                                <div class="contact-detail-block p-a15">
                                    @if(isset($value['driver_pic']))
                                    <p> 
                                        <span class="driver-img-block">
                                            <img src="{{ isset($value['driver_pic']) ? $value['driver_pic'] : '' }}" height="50px" width="50px" alt="" />
                                        </span>
                                    </p>
                                    @endif
                                    <p><i class="fa fa-calendar"></i> <span class="vTitle">Plant Address : </span> <span class="vName">{{ isset($value['addressDetails']) ? ($value['addressDetails']['business_name'].', '. $value['addressDetails']['line1'].', '. $value['addressDetails']['line2']) : '-' }}</span></p>
                                    <p><i class="fa fa-user"></i> <span class="vTitle">Driver Name : </span> <span class="vName">{{ isset($value['driver_name']) ? $value['driver_name'] : '-' }}</span></p>
                                    <p><i class="fa fa-phone-square"></i> <span class="vTitle">Mobile No. : </span> <span class="vName">{{ isset($value['driver_mobile_number']) ? $value['driver_mobile_number'] : '-' }}</span></p>
                                    <p><i class="fa fa-phone-square"></i>   <span class="vTitle">Alternate No. : </span> <span class="vName">{{ isset($value['driver_alt_mobile_number']) ? $value['driver_alt_mobile_number'] : '-' }}</span></p>
                                    <p><i class="fa fa-whatsapp"></i> <span class="vTitle">Whatsapp No. : </span> <span class="vName">{{ isset($value['driver_whatsapp_number']) ? $value['driver_whatsapp_number'] : '-' }}</span></p>
                                    <p><i class="fa fa-calendar"></i> <span class="vTitle">Created at : </span> <span class="vName">{{ isset($value['created_at']) ? AdminController::dateTimeFormat($value["created_at"]) : '-' }}</span></p>
                                    
                                    <div class="driver_opration_btn top-right-edit-link">
                                        <a href="javascript:;" onclick="showDriverDetailEditDialog('{{ json_encode($value) }}')" class="site-button green adrs-edt-icon"  title="Edit"><i class="fa fa-edit"></i></a>
                                       <!--  <a href="javascript:;" class="site-button red adrs-dlt-icon" data-toggle="modal" data-target="" title="Delete"><i class="fa fa-trash"></i></a> -->
                                    </div>
                                </div>
                                
                            </li>
                        @endforeach
                    @endif
                    
                    
                    
                    
                    
                </ul>
            </div>
        </div>
        <div><b>Note:</b> 
        <i>You should add at least 2 drivers to add TM

        </i></div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection