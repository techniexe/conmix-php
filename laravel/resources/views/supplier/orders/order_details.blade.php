@section('title', 'Supplier Order Details')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums"><a href="{{ route('orders_view') }}">Order List</a> / <span>Order Details</span></h1>
    <div class="clearfix"></div>
    <!--order Details start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Order Details</h2>
                </div>

                <div class="odrs-details-block p-a0">
                <?php //dd($data['data']["with_CP"]); 
                    // dd($data['data']);
                ?>
                    @if(isset($data['data']))
                        <?php $order_item_data = $data["data"]; ?>

                        <?php $vehicles = $data["vehicles"]; ?>
                        <?php $cp_lists = $data["cp_lists"]; ?>
                        <?php $data = $data["data"]; ?>
                        <?php $is_with_cp = $data["with_CP"]; ?>
                        <?php //$order_data = $data["data"]; ?>
                        <?php $buyer_data = isset($data["buyer"]) ? $data["buyer"] : array(); ?>
                        <?php //$product_data = $data["data"]; ?>

                        <?php $orderTrackDetails = $data["orderTrackData"]; ?>
                        <?php $orderItemPartData = $data["orderItemPartData"]; ?>
                    <div class="orders-content-block">
                        <div class="orders-content-head-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-id-date-block">
                                        <div class="ordlst-sd-text-block spa">
                                            Order Id : #<span>{{ $data["_id"] }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block spa">
                                            Buyer Order Id : #<span>{{ $data["buyer_order_display_id"] }}</span>
                                        </div>
                                        <!-- <div class="ordlst-id-block">
                                            Invoice Id: <a href="#">10090042</a>
                                        </div> -->
                                        <div class="ordlst-date-block">
                                            Order Date & Time : <span>{{ AdminController::dateTimeFormat($data["created_at"]) }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Buyer Name : <span>{{ isset($buyer_data["full_name"]) ? $buyer_data["full_name"] : '' }}</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-ship-dlvry-date-block">
                                        <div class="ordlst-sd-text-block">
                                            Payment Status : <span class="badge {{ $data['payment_status'] == 'PAID' ? 'bg-green' : 'bg-red' }} ">{{ $data["payment_status"] }}</span>
                                        </div>
                                        <!-- <div class="ordlst-sd-text-block">
                                            Bill Status: <span class="badge bg-red">Rejected</span>
                                        </div> -->
                                        <div class="ordlst-sd-text-block">
                                            Total Amount : <span class="rupee-text"><i class="fa fa-rupee"></i> {{ isset($data["total_amount_without_margin"]) ? number_format($data["total_amount_without_margin"],2) : 0 }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Pickup Location :
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                @if(isset($data["pickup_address"]))
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['pickup_address']['location']['coordinates'][1]) ? $data['pickup_address']['location']['coordinates'][1] : '' }}','{{ isset($data['pickup_address']['location']['coordinates'][0]) ? $data['pickup_address']['location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                {{ isset($data["pickup_address"]['line1']) ? $data["pickup_address"]['line1'] : '' }},
                                                    {{ isset($data["pickup_address"]['line2']) ? $data["pickup_address"]['line2'] : '' }},
                                                    {{ isset($data["pickup_address"]['state_name']) ? $data["pickup_address"]['state_name'] : '' }},
                                                    {{ isset($data["pickup_address"]['city_name']) ? $data["pickup_address"]['city_name'] : '' }} - {{ isset($data["pickup_address"]['pincode']) ? $data["pickup_address"]['pincode'] : '' }},
                                                @endif

                                                @if(isset($data['line1']))
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['latitude']) ? $data['latitude'] : '' }}','{{ isset($data['longitude']) ? $data['longitude'] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                    {{ isset($data['line1']) ? $data['line1'] : '' }},
                                                    {{ isset($data['line2']) ? $data['line2'] : '' }},
                                                    {{ isset($data['state_name']) ? $data['state_name'] : '' }},
                                                    {{ isset($data['city_name']) ? $data['city_name'] : '' }} - {{ isset($data['pincode']) ? $data['pincode'] : '' }},
                                                @endif
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Delivery Location :
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                @if(isset($data["delivery_address"]))
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['delivery_address']['location']['coordinates'][1]) ? $data['delivery_address']['location']['coordinates'][1] : '' }}','{{ isset($data['delivery_address']['location']['coordinates'][0]) ? $data['delivery_address']['location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                {{ isset($data["delivery_address"]['address_line1']) ? $data["delivery_address"]['address_line1'] : '' }},
                                                    {{ isset($data["delivery_address"]['address_line2']) ? $data["delivery_address"]['address_line2'] : '' }},
                                                    {{ isset($data["delivery_address"]['state_name']) ? $data["delivery_address"]['state_name'] : '' }},
                                                    {{ isset($data["delivery_address"]['city_name']) ? $data["delivery_address"]['city_name'] : '' }} - {{ isset($data["delivery_address"]['pincode']) ? $data["delivery_address"]['pincode'] : '' }},
                                                @endif

                                                @if(isset($data["order"]))
                                                    <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['order']['latitude']) ? $data['order']['latitude'] : '' }}','{{ isset($data['order']['longitude']) ? $data['order']['longitude'] : '' }}')">View Map</a>
                                                    <span class="cstm-tooltip__item">
                                                    {{ isset($data["order"]['address_line1']) ? $data["order"]['address_line1'] : '' }},
                                                    {{ isset($data["order"]['address_line2']) ? $data["order"]['address_line2'] : '' }},
                                                    {{ isset($data["order"]['state_name']) ? $data["order"]['state_name'] : '' }},
                                                    {{ isset($data["order"]['city_name']) ? $data["order"]['city_name'] : '' }} - {{ isset($data["order"]['pincode']) ? $data["order"]['pincode'] : '' }},
                                                @endif
                                                </span>
                                            </span>
                                        </div>



                                    </div>
                                    <!--     <div class="odr-select-optin-block">                                  <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="dropdown-label">Select Invoice <i class="fa fa-sort-down"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li><a class="dropdown-item" href="invoice.html">View invoice</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Download invoice</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#sendemil-sts-popup">Send invoice to email</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Download purchase order</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Send purchase order to email</a></li>
                                                </ul>
                                            </div>
                                        </div> -->

                                            <div class="dropdown cstm-dropdown-select bid-rate-dropdown upload_bill_drop" style="display: block;display: block;position: absolute; right: 10px; top: 0;">

                                                <!-- <a href="javascript:;" class="site-button green button-sm" >Accept Bill</a>
                                                <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm" data-toggle="dropdown" aria-expanded="false" data-tooltip="Reject Bill"><i class="fa fa-ban"></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <form action="" method="get">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <textarea class="form-control" placeholder="Write a Reason"></textarea>
                                                                    </div>
                                                                </div>
                                                                <button class="site-button blue">Submit</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div> -->
                                                @if(isset($data["bill_path"]))
                                                    <div id="gallery" class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                        <a href="{{ $data['bill_path'] }}" class="gal_link site-button blue button-sm" data-tooltip="View Bill"><i class="fa fa-eye"></i></a>

                                                    </div>
                                                @endif
                                                @if(!empty($data["bills"]) || !empty($data["creditNotes"]) || !empty($data["debitNotes"]))
                                                @endif
                                                @if(false)
                                                <div class="dropdown cstm-dropdown-select order_dtl_action dis-inline-block">
                                                    <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <i class="fa fa-file-text-o"></i> View Debit Notes
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">

                                                    @if(isset($data["bills"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["bills"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Invoice {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-yellow">{{ $bill_value["status"] }}</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Invoice {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Invoice {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <!-- <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                                        </p>
                                                                                    </li>
                                                                                </ul> -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ Credit Note Bill........................................................................  -->

                                                        @if(isset($data["creditNotes"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["creditNotes"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Credit Note {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-yellow">{{ $bill_value["status"] }}</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Credit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Credit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ Credit Note Bill Over........................................................................  -->


<!--................................ Debit Note Bill........................................................................  -->

                                                        @if(isset($data["debitNotes"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["debitNotes"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif
                                                                <?php 
                                                                    $data["bill_status"] = $bill_value["status"];
                                                                ?>
                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Debit Note {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-yellow">{{ $bill_value["status"] }}</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Debit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Debit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ Debit Note Bill Over........................................................................  -->

                                                        <!-- <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 </a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-green">Accept</span>
                                                                <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <li class="p-a15">
                                                                        <form action="" method="get">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <textarea class="form-control" placeholder="Write a Reason"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <button class="site-button blue">Submit</button>
                                                                        </form>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3</i></a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-green">Accepted</span>
                                                            </div>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3</i></a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-red">Rejected</span>
                                                                <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                        <li class="p-a15">
                                                                            <h3>Rejected By Admin :</h3>
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                            </p>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li> -->
                                                    </ul>
                                                </div>
                                                @endif
                                                <?php
                                                    $is_show_upload_bill = 1;
                                                ?>

                                                @if($data["order_status"] == 'DELIVERED')
                                                    <?php
                                                        $is_show_upload_bill = 1;
                                                    ?>
                                                    @if(isset($data["bill_status"]))

                                                        @if($data["bill_status"] == 'REJECTED')
                                                            <?php
                                                            $is_show_upload_bill = 1;
                                                            ?>
                                                        @else
                                                            <?php
                                                                $is_show_upload_bill = 0;
                                                            ?>
                                                        @endif

                                                    @endif
                                                @else
                                                    <?php
                                                        $is_show_upload_bill = 0;
                                                    ?>
                                                @endif

                                                @if($is_show_upload_bill == 1)
                                                @endif
                                                @if(false)
                                                    <a href="javascript:;" class="site-button blue dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Upload Debit Note</a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <form name="upload_order_bill_form" action="" method="post" enctype="multipart/form-data">
                                                            @csrf
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="cstm-select-box">
                                                                            <select class="form-control" name="bill_type">
                                                                                <option disabled="disabled" selected="selected">Select Bill Type</option>
                                                                                <!-- <option value="invoice">Invoice</option> -->
                                                                                <!-- <option value="credit_notes">Credit Note</option> -->
                                                                                <option value="debit_notes">Debit Note</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="file-browse" id="bill_image_div">
                                                                            <span class="button-browse">
                                                                                Browse <input type="file" name="bill_image">
                                                                            </span>
                                                                            <input type="text" class="form-control browse-input" placeholder="e. g. png, jpg" readonly="">
                                                                        </div>
                                                                        <input type="hidden" name="order_id" value="{{ $data['_id'] }}"/>
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="site-button blue">Upload</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                @endif
                                            @if(isset($data["bill_status"]))
                                                @if($data["bill_status"] == 'REJECTED')
                                                    <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                        <a href="javascript:;" class="site-button red button-sm button-sm-ctm" data-toggle="dropdown" aria-expanded="true" title="Bill Rejected" ><i class="fa fa-info-circle" ></i></a>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <li>
                                                                <h3>Rejected By Admin:</h3>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                </p>
                                                            </li>
                                                        </ul>
                                                    </div> -->
                                                @endif
                                            @endif
                                        </div>


                                </div>
                            </div>
                        </div>
                        <div class="orders-content-middle-block">
                            <div class="orders-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Concrete Grade</th>
                                            <th>RMC Design Mix</th>
                                            <th>Qty ( Cu.Mtr )</th>
                                            <th class="tble_heding_color_red">Balance Qty (Cu.Mtr)</th>
                                            <th class="tble_heding_color_green">Delivered Qty (Cu.Mtr)</th>
                                            <th>Unit Price ( <i class="fa fa-rupee"></i> / Cu.Mtr )</th>
                                            <th>TM & CP </th>
                                            <!-- <th>TM Price ( <i class="fa fa-rupee"></i> )</th>
                                            <th>CP Price ( <i class="fa fa-rupee"></i> )</th> -->
                                            <th>Base Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <th>GST Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <!-- <th>Pending Qty To Be Assign</th> -->
                                            <!-- <th>Assign TM </th>
                                            <th>Assign CP </th> -->
                                            <th>Order Status</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($order_item_data))

                                            <?php //dd($order_item_data); ?>
                                                <tr>
                                                    <td>
                                                        <div class="cate_img">

                                                            <?php
                                                                $primary_img = "";

                                                                if(isset($order_item_data["vendor_media"])){
                                                                    foreach($order_item_data["vendor_media"] as $media_value){
                                                                        if($media_value["type"] == "PRIMARY"){
                                                                            $primary_img = $media_value["media_url"];
                                                                        }

                                                                    }
                                                                }

                                                            ?>
                                                            @if($primary_img == "")
                                                                <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                                            @else
                                                                <img src="{{ $primary_img }}" alt="" />
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        {{ isset($order_item_data['concrete_grade']) ? $order_item_data['concrete_grade']['name'] : (isset($order_item_data['concrete_grade_name']) ? $order_item_data['concrete_grade_name'] : '') }}

                                                    </td>
                                                    <td>
                                                    <div class="dropdown tm_info_dropdown custom_rmc_dropdown">
                                                        <a href="#" class="dropdown-toggle text-nowrap" data-toggle="dropdown" aria-expanded="true">
                                                            <span class="dropdown-label">{{ isset($order_item_data['design_mix']["product_name"]) ? $order_item_data['design_mix']["product_name"] : (isset($order_item_data["orderItem"]["product_name"]) ? $order_item_data["orderItem"]["product_name"] : 'Custom Mix') }} <i class="fa fa-sort-down"></i></span>
                                                        </a>
                                                            <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <div class="custom_rmc_bx">
                                                                    <h4>Design Mix</h4>
                                                                    <div class="custom_rmc_info">
                                                                        <h5>{{ isset($order_item_data['concrete_grade']) ? $order_item_data['concrete_grade']['name'] : (isset($order_item_data['concrete_grade_name']) ? $order_item_data['concrete_grade_name'] : '') }} - {{ isset($order_item_data['design_mix']["product_name"]) ? $order_item_data['design_mix']["product_name"] : (isset($order_item_data["orderItem"]["product_name"]) ? $order_item_data["orderItem"]["product_name"] : 'Custom Mix') }}</h5>
                                                                        <div class="row">
                                                                            <div class="col-sm-6">
                                                                                <p>Cement ( Kg ) : <span>{{ isset($order_item_data['cement_quantity']) ? $order_item_data['cement_quantity'] : '' }}</span></p>
                                                                                <p>Coarse Sand ( Kg ) : <span>{{ isset($order_item_data['sand_quantity']) ? $order_item_data['sand_quantity'] : '' }}</span></p>
                                                                                <p>Fly Ash ( Kg ) : <span>{{ isset($order_item_data['fly_ash_quantity']) ? $order_item_data['fly_ash_quantity'] : '' }}</span></p>
                                                                                <p>Admixture ( Kg ) : <span>{{ isset($order_item_data['admix_quantity']) ? $order_item_data['admix_quantity'] : '' }}</span></p>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <p>{{ isset($order_item_data["aggregate1_sub_cat"]["sub_category_name"]) ? $order_item_data["aggregate1_sub_cat"]["sub_category_name"] : (isset($order_item_data["aggregate1_sub_cat_name"]) ? $order_item_data["aggregate1_sub_cat_name"] : '') }} ( Kg ) : 
                                                                                    <span>{{ isset($order_item_data['aggregate1_quantity']) ? $order_item_data['aggregate1_quantity'] : '' }}</span></p>
                                                                                <p>{{ isset($order_item_data["aggregate2_sub_cat"]["sub_category_name"]) ? $order_item_data["aggregate2_sub_cat"]["sub_category_name"] : (isset($order_item_data["aggregate2_sub_cat_name"]) ? $order_item_data["aggregate2_sub_cat_name"] : '') }} ( Kg ) : 
                                                                                    <span>{{ isset($order_item_data['aggregate2_quantity']) ? $order_item_data['aggregate2_quantity'] : '' }}</span></p>
                                                                                <p>Water ( Ltr ) : <span>{{ isset($order_item_data['water_quantity']) ? $order_item_data['water_quantity'] : '' }}</span></p>
                                                                                <p>Grade : <span>{{ isset($order_item_data['concrete_grade']) ? $order_item_data['concrete_grade']['name'] : (isset($order_item_data['concrete_grade_name']) ? $order_item_data['concrete_grade_name'] : '') }}</span></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="custom_rmc_info custom_rmc_dec">
                                                                        <b>Design Mix Description</b>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <p>Cement Brand : <span>{{ isset($order_item_data["cement_brand"]["name"]) ? $order_item_data["cement_brand"]["name"] : (isset($order_item_data["cement_brand_name"]) ? $order_item_data["cement_brand_name"] : '') }}</span></p>
                                                                                <p>Coarse Sand Source : <span>{{ isset($order_item_data["sand_source"]["sand_source_name"]) ? $order_item_data["sand_source"]["sand_source_name"] : (isset($order_item_data["sand_source_name"]) ? $order_item_data["sand_source_name"] : '') }}</span></p>
                                                                                <p>Coarse Sand Zone : <span>{{ (isset($order_item_data["sand_zone_name"]) ? $order_item_data["sand_zone_name"] : '') }}</span></p>
                                                                                <p>Aggregate Source : <span>{{ isset($order_item_data["aggregate_source"]["aggregate_source_name"]) ? $order_item_data["aggregate_source"]["aggregate_source_name"] : (isset($order_item_data["agg_source_name"]) ? $order_item_data["agg_source_name"] : '') }}</span></p>
                                                                                <p>Admixture Brand : <span>{{ isset($order_item_data["admix_brand"]["name"]) ? $order_item_data["admix_brand"]["name"] : (isset($order_item_data["ad_mixture1_brand_name"]) ? $order_item_data["ad_mixture1_brand_name"] : '') }} - {{ isset($order_item_data["admix_cat"]["category_name"]) ? $order_item_data["admix_cat"]["category_name"] : (isset($order_item_data["ad_mixture1_category_name"]) ? $order_item_data["ad_mixture1_category_name"] : '') }}</span></p>
                                                                                <p>Fly Ash Source Name : <span>{{ (isset($order_item_data["fly_ash_source_name"]) ? $order_item_data["fly_ash_source_name"] : '') }}</span></p>
                                                                                <p>Water :<span> {{ (isset($order_item_data["water_type"]) ? $order_item_data["water_type"] : '') }}</span></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td><span class="qty-text">{{ $order_item_data['quantity'] }}</span></td>
                                                    <td><span class="qty-text">{{ $order_item_data['orderItem']['remaining_quantity'] }}</span>
                                                                <input type="hidden" id="balance_qty" value="{{ $order_item_data['orderItem']['remaining_quantity'] }}" />
                                                    </td>
                                                    <td><span class="qty-text">{{ $order_item_data['orderItem']['pickup_quantity'] }}</span></td>
                                                    <td>{{ isset($order_item_data["unit_price"]) ? number_format($order_item_data["unit_price"],2) : 0 }}</td>
                                                    <td>{{ ($order_item_data["with_TM"] == 'true') ? (($order_item_data["with_CP"] == 'true') ? 'TM & CP' : 'TM Only' ): "Buyer's TM" }}
                                                        @if(!$order_item_data["with_TM"])
                                                            <div class="dropdown tm_info_dropdown">
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Buyer TM Info</a>
                                                                <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <div class="tm_info_bx">
                                                                        <h4>Buyer TM Detail</h4>
                                                                        <div class="tm_info_list"> <span>Delivery Date & Time :</span> 04 Jan 2021, 10:50 AM </div>
                                                                        <div class="tm_info_list"> <span>TM No :</span> GJ-01-AA-0000 </div>
                                                                        <div class="tm_info_list"> <span>Driver Name :</span> Jhon </div>
                                                                        <div class="tm_info_list"> <span>Mobile No. :</span> +91 9979016486 </div>
                                                                        <div class="tm_info_list"> <span>TM Operator Name :</span> Jhondoe </div>
                                                                        <div class="tm_info_list"> <span>TM Operator Mobile No. :</span> +91 9979016486 </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </td>

                                                    <!-- <td> {{ isset($order_item_data['TM_price']) ? number_format($order_item_data['TM_price'],2) : 0 }}</td>
                                                    <td> {{ isset($order_item_data['CP_price']) ? number_format($order_item_data['CP_price'],2) : 0 }}</td> -->
                                                    <td> {{ isset($order_item_data['selling_price']) ? number_format($order_item_data['selling_price'],2) : 0 }}</td>
                                                    <td> {{ isset($order_item_data['gst_price']) ? number_format($order_item_data['gst_price'],2) : 0 }}</td>
                                                    <td>{{ isset($order_item_data['total_amount_without_margin']) ? number_format($order_item_data['total_amount_without_margin'],2) : 0 }}</td>
                                                    <!-- <td><span class="qty-text">{{ $order_item_data['orderItem']['temp_quantity'] }}</span></td> -->
                                                    <!-- <td>
                                                        <input type="hidden" id="order_delivery_date" value="{{ date('d-m-Y',strtotime($order_item_data['orderItem']['delivery_date'])) }}" />
                                                        @if(isset($order_item_data['orderItem']['temp_quantity']) && $order_item_data['orderItem']['temp_quantity'] != 0)
                                                            <a href="javascript:;" onclick="openTMassignPopup('{{ date('d-m-Y',strtotime($order_item_data['orderItem']['delivery_date'])) }}')" class="site-button dark button-sm text-nowrap">+ Assign TM </a>
                                                        @else
                                                            <span class="text-success">TM Assigned</span>
                                                        @endif

                                                    </td>
                                                    <td>
                                                         <input type="hidden" id="order_delivery_date" value="{{ date('d-m-Y',strtotime($order_item_data['orderItem']['delivery_date'])) }}" />
                                                        @if(isset($order_item_data['orderItem']['temp_quantity']) && $order_item_data['orderItem']['temp_quantity'] != 0)
                                                            <a href="javascript:;" onclick="openCPassignPopup('{{ date('Y-m-d',strtotime($order_item_data['orderItem']['delivery_date'])) }}')" class="site-button dark button-sm text-nowrap">+ Assign CP </a>
                                                        @else
                                                            <span class="text-success">CP Assigned</span>
                                                        @endif

                                                    </td> -->
                                                    <td class="noowrp">
                                                        @if($order_item_data["order_status"] == 'DELIVERED' || $order_item_data["order_status"] == 'ACCEPTED')
                                                            <div class="badge bg-green">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>
                                                        @else @if($order_item_data["order_status"] == 'CANCELLED' || $order_item_data["order_status"] == 'REJECTED' || $order_item_data["order_status"] == 'LAPSED')
                                                            <div class="badge bg-red">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>
                                                        @else @if($order_item_data["order_status"] == 'RECEIVED')
                                                            <div class="badge bg-blue">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>
                                                        @else @if($order_item_data["order_status"] == 'PICKUP')
                                                            <div class="badge bg-purple">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>

                                                        @else
                                                            <div class="badge bg-yellow">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>
                                                        @endif
                                                        @endif
                                                        @endif
                                                        @endif
                                                        <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                            <a href="javascript:;" class="site-button bg-grey button-sm button-sm-ctm1" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i></a>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <li>
                                                                    <h3>Admin By :</h3>
                                                                    <p>
                                                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                    </p>
                                                                </li>
                                                            </ul>
                                                        </div> -->
                                                    </td>
                                                    <!-- <td>
                                                        <div class="dropdown cstm-dropdown-select">
                                                            <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <li><a class="dropdown-item" onclick="showOrderStatusDialog('item_status','{{ json_encode($order_item_data) }}')"  >Change Status</a></li>

                                                                <li><a class="dropdown-item" href="{{ url('admin/orders/track/'.$order_item_data['_id']) }}">Track</a></li>
                                                            </ul>
                                                        </div>
                                                    </td> -->
                                                </tr>

                                        @endif

                                    </tbody>
                                </table>

                                <div class="trip_driver_tbl">
                                    <h2 class="mb-3">Assigned Quantity & Date</h2>
                                    <div class="comn-table latest-order-table">
                                        <table class="table">
                                            <thead>
                                                <tr class="tr_heading">
                                                    <th colspan="4">By Buyer</th>
                                                    <th colspan="4">By You</th>
                                                </tr>
                                                <tr>
                                                    <th>Assigned Date</th>
                                                    <th>Assigned (Delivery) Start Time</th>
                                                    <th>Assigned (Delivery) End Time</th>
                                                    <th>Qty To Deliver</th>
                                                    <th>Pending Qty To Assign</th>

                                                    <th>Assign TM</th>
                                                    <th>Assign CP</th>
                                                    <th>Reassign</th>
                                                    <!-- <th>Action</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($orderItemPartData) && count($orderItemPartData) > 0)
                                            <?php //dd($orderItemPartData); ?>
                                                @foreach($orderItemPartData as $orderItemPartvalue)
                                                    <?php $item_track_data = isset($orderItemPartvalue['TMtrack_details']) ? $orderItemPartvalue['TMtrack_details'] : array(); ?>
                                                <tr>
                                                    <td>{{ date('d M Y', strtotime($orderItemPartvalue["start_time"])) }}</td>
                                                    <td>{{ date('d M Y, h:i a', strtotime($orderItemPartvalue["start_time"])) }}</td>
                                                    <td>{{ date('d M Y, h:i a', strtotime($orderItemPartvalue["end_time"])) }}</td>
                                                    <td>{{ $orderItemPartvalue["assigned_quantity"] }} Cu. Mtr</td>
                                                    <td>{{ $orderItemPartvalue["temp_quantity"] }} Cu. Mtr</td>
                                                    <td>
                                                        @if(isset($orderItemPartvalue['TMtrack_details']) && !empty($orderItemPartvalue['TMtrack_details']))
                                                        <?php $remaining_quantity = 0; ?>
                                                            @foreach($orderItemPartvalue['TMtrack_details'] as $TMtrack_details_value)

                                                                <?php
                                                                // $remaining_quantity = $TMtrack_details_value["remaining_quantity"];
                                                                $remaining_quantity = $orderItemPartvalue["temp_quantity"];

                                                                ?>

                                                            @endforeach

                                                            @if($is_with_cp == 'true')

                                                                @if(isset($orderItemPartvalue["CPtrack_details"][0]))

                                                                    @if($remaining_quantity > 0)
                                                                        <a href="javascript:;" onclick="openTMassignPopup('{{ date('d-m-Y',strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['end_time'])) }}','{{ $orderItemPartvalue['_id'] }}','{{ $orderItemPartvalue['assigned_quantity'] }}','{{ $orderItemPartvalue['temp_quantity'] }}','{{ $data['address_id'] }}')" class="site-button dark button-sm text-nowrap">+ Assign TM </a>
                                                                    @else
                                                                        <span class="text-success">TM Assigned</span>
                                                                    @endif

                                                                @else
                                                                    <span class="text-success">Please First Assign CP</span>
                                                                @endif
                                                            @else

                                                                @if($remaining_quantity > 0)
                                                                    <a href="javascript:;" onclick="openTMassignPopup('{{ date('d-m-Y',strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['end_time'])) }}','{{ $orderItemPartvalue['_id'] }}','{{ $orderItemPartvalue['assigned_quantity'] }}','{{ $orderItemPartvalue['temp_quantity'] }}','{{ $data['address_id'] }}')" class="site-button dark button-sm text-nowrap">+ Assign TM </a>
                                                                @else
                                                                    <span class="text-success">TM Assigned</span>
                                                                @endif

                                                            @endif

                                                        @else
                                                            @if($is_with_cp == 'true')
                                                                @if(isset($orderItemPartvalue["CPtrack_details"][0]))

                                                                    <a href="javascript:;" onclick="openTMassignPopup('{{ date('d-m-Y',strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['end_time'])) }}','{{ $orderItemPartvalue['_id'] }}','{{ $orderItemPartvalue['assigned_quantity'] }}','{{ $orderItemPartvalue['temp_quantity'] }}','{{ $data['address_id'] }}')" class="site-button dark button-sm text-nowrap">+ Assign TM </a>

                                                                @else
                                                                    <span class="text-success">Please First Assign CP</span>
                                                                @endif
                                                            @else

                                                                <a href="javascript:;" onclick="openTMassignPopup('{{ date('d-m-Y',strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['end_time'])) }}','{{ $orderItemPartvalue['_id'] }}','{{ $orderItemPartvalue['assigned_quantity'] }}','{{ $orderItemPartvalue['temp_quantity'] }}','{{ $data['address_id'] }}')" class="site-button dark button-sm text-nowrap">+ Assign TM </a>

                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <!-- <input type="hidden" id="order_delivery_date" value="{{ date('d-m-Y',strtotime($order_item_data['orderItem']['delivery_date'])) }}" /> -->
                                                        @if($is_with_cp == 'true')
                                                            @if(!isset($orderItemPartvalue['CPtrack_details'][0]))
                                                                <a href="javascript:;" onclick="openCPassignPopup('{{ date('Y-m-d',strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['start_time'])) }}','{{ date('h:i', strtotime($orderItemPartvalue['end_time'])) }}','{{ $orderItemPartvalue['_id'] }}','{{ $data['address_id'] }}')" class="site-button dark button-sm text-nowrap">+ Assign CP </a>
                                                            @else
                                                                <span class="text-success">CP Assigned</span>
                                                                <a href="javascript:void(0)" onclick="CPTrack('{{ $orderItemPartvalue['_id'] }}')">Track</a>
                                                            @endif
                                                        @else
                                                            Order placed without CP
                                                        @endif

                                                    </td>

                                                    <td>
                                                        @if(isset($orderItemPartvalue['order_reassign']) && count($orderItemPartvalue['order_reassign']) > 0)
                                                            
                                                            <div class="dropdown tm_info_dropdown">
                                                                <a href="#" class="color_orange" class="dropdown-toggle" role="button" id="dropdownMenuLink2" data-toggle="dropdown" aria-expanded="true">Reassign Request</a>
                                                                <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <?php
                                                                        $accept_reject = 0;
                                                                        $order_item_part_id = 0;
                                                                    ?>
                                                                    @foreach($orderItemPartvalue['order_reassign'] as $order_reassign_value)
                                                                    <?php
                                                                        $accept_reject = $order_reassign_value['accept_reject'];
                                                                        $order_item_part_id = $order_reassign_value['order_item_part_id'];

                                                                    ?>
                                                                        <div class="tm_info_bx">
                                                                            <h4 class="color_orange">Reassign Request</h4>
                                                                            <div class="tm_info_list"> <span>Reassign Reason :</span> {{ $order_reassign_value['reassign_reason'] }} </div>
                                                                            <div class="tm_info_list"> <span>Reassign Desc :</span> {{ $order_reassign_value['reassign_desc'] }} </div>
                                                                            <div class="tm_info_list"> <span>Previous Delivery Date :</span> {{ AdminController::dateTimeFormat($order_reassign_value["previous_start_time"]) }} - {{ AdminController::dateTimeFormat($order_reassign_value["previous_end_time"]) }} </div>
                                                                            <div class="tm_info_list"> <span>New Delivery Date :</span> {{ AdminController::dateTimeFormat($order_reassign_value["new_start_time"]) }} - {{ AdminController::dateTimeFormat($order_reassign_value["new_end_time"]) }} </div>
                                                                            <div class="tm_info_list"> <span>Status :</span> 
                                                                                                @if($order_reassign_value['accept_reject'] == 0) 
                                                                                                    <span class="badge bg-yellow">No Status</span>
                                                                                                @elseif($order_reassign_value['accept_reject'] == 1)
                                                                                                    <span class="badge bg-yellow">Accepted</span>
                                                                                                @elseif($order_reassign_value['accept_reject'] == 2)
                                                                                                    <span class="badge bg-yellow">Rejected</span>
                                                                                                @endif
                                                                            
                                                                            @if(isset($order_reassign_value['order_track']) && count($order_reassign_value['order_track']) > 0)
                                                                                <div class="tm_info_list">
                                                                                    <h4 class="color_orange">Track Details</h4> 
                                                                                    <div class="tm_info_list">
                                                                                        <span>TM No. :</span> {{ $order_reassign_value['order_track']['TM_rc_number'] }} 
                                                                                    </div>
                                                                                    <div class="tm_info_list">
                                                                                        <span>Qty. :</span> {{ $order_reassign_value['order_track']['pickup_quantity'] }} 
                                                                                    </div>
                                                                                    <!-- <div class="tm_info_list">
                                                                                        <span>Status :</span> {{ $order_reassign_value['order_track']['event_status'] }} 
                                                                                    </div> -->
                                                                                    
                                                                                    
                                                                                </div>
                                                                            @endif
                                                                            
                                                                            
                                                                        </div>
                                                                        <hr/>
                                                                    @endforeach

                                                                </div>
                                                            </div>

                                                        @else
                                                        <?php $is_show_reassign_option = 1; $is_any_not_delivered_item = 0; ?>
                                                            @if(isset($orderItemPartvalue['TMtrack_details']) && !empty($orderItemPartvalue['TMtrack_details']))
                                                                @foreach($item_track_data as $orderItemTrackvalue)
                                                                    @if($orderItemTrackvalue['event_status'] != 'DELIVERED')
                                                                        <?php $is_any_not_delivered_item = 1 ?>
                                                                    @endif                                                                
                                                                @endforeach
                                                                @if($is_any_not_delivered_item == 1)
                                                                    <?php $is_show_reassign_option = 1; ?>
                                                                @endif
                                                                @if($is_any_not_delivered_item == 0)
                                                                    <?php $is_show_reassign_option = 0; ?>
                                                                @endif
                                                            @endif

                                                            @if($is_show_reassign_option == 1)
                                                                <!-- <a href="javascript:void(0)" onclick="ReasignOrder()">Reassigning Request</a> -->
                                                                <div class="dropdown cstm-dropdown-select">
                                                                    <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                        <span class="dropdown-label">Reassigning Request <i class="fa fa-sort-down"></i></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                        
                                                                        <!-- <li><a class="dropdown-item" href="javascript:void(0);" onclick="ReasignOrder('order_rejected','{{ $orderItemPartvalue["vendor_id"] }}')">Due To Order Rejected</a></li> -->
                                                                        
                                                                        <li><a class="dropdown-item" href="javascript:void(0);" onclick="ReasignOrder('breakdown','{{ $orderItemPartvalue["vendor_id"] }}')">Due To Any Breakdown</a></li>
                                                                    
                                                                        <li><a class="dropdown-item" href="javascript:void(0);" onclick="ReasignOrder('material_out_of_stock','{{ $orderItemPartvalue["vendor_id"] }}')">Due To Material Out Of Stock</a></li>
                                                                    
                                                                    </ul>
                                                                </div>
                                                            @else
                                                                -
                                                            @endif

                                                        @endif
                                                    </td>

                                                </tr>
                                                @endforeach
                                            @else
                                            <tr>
                                                <td colspan="7">No Record Found</td>
                                            </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="trip_driver_tbl">
                                    <h2 class="mb-3">Track Order</h2>
                                    <div class="comn-table latest-order-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Category</th>
                                                    <th>TM No</th>
                                                    <th>Driver Detail</th>
                                                    <th>Assign Date</th>
                                                    <th>Delivery Start Time </th>
                                                    <th>Delivery End Time </th>
                                                    <th>Deliver Qty</th>
                                                    <th>Delivery Status</th>
                                                    <th>Reassign</th>
                                                    <th>Track</th>
                                                    <th>7th Day Cube Test Report</th>
                                                    <th>28th Day Cube Test Report</th>
                                                    <th>My Invoice</th>
                                                    <th>Buyer Invoice</th>
                                                    <th>Debit Note</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($orderTrackDetails) && count($orderTrackDetails) > 0)
                                            <?php //dd($orderTrackDetails); ?>
                                                @foreach($orderTrackDetails as $value)
                                                <tr>
                                                    <td>{{ isset($value["category_name"]) ? $value["category_name"] : $value["TM_category_name"] }}</td>
                                                    <td>{{ $value["TM_rc_number"] }}</td>
                                                    @if(isset($value["driver_name"]))
                                                        <td>{{ $value["driver_name"] }} - {{ $value["driver_mobile_number"] }}</td>
                                                    @endif
                                                    @if(isset($value["TM_driver1_name"]))
                                                        <td>{{ $value["TM_driver1_name"] }} - {{ $value["TM_driver1_mobile_number"] }}</td>
                                                    @endif
                                                    <td>{{ date('d M Y', strtotime($value["assigned_at"])) }}</td>
                                                    <td>{{ date('d M Y, h:i a', strtotime($value["start_time"])) }}</td>
                                                    <td>{{ date('d M Y, h:i a', strtotime($value["end_time"])) }}</td>
                                                    <td>{{ $value["pickup_quantity"] }} Cu.  Mtr</td>
                                                    <td>
                                                    @if($value["event_status"] == 'DELIVERED')
                                                        <div class="badge bg-green">{{ str_replace("_"," ",$value["event_status"]) }}</div>
                                                    @else @if($value["event_status"] == 'CANCELLED' || $value["event_status"] == 'REJECTED')
                                                        <div class="badge bg-red">{{ str_replace("_"," ",$value["event_status"]) }}</div>

                                                    @else @if($value["event_status"] == 'PICKUP')
                                                        <div class="badge bg-purple">{{ str_replace("_"," ",$value["event_status"]) }}</div>

                                                    @else
                                                        <div class="badge bg-yellow">{{ str_replace("_"," ",$value["event_status"]) }}</div>
                                                    @endif
                                                    @endif
                                                    @endif
                                                    </td>
                                                    <td>
                                                        @if(isset($value['order_reassign']) && count($value['order_reassign']) > 0)
                                                            
                                                            <div class="dropdown tm_info_dropdown">
                                                                <a href="#" class="color_orange" class="dropdown-toggle" role="button" id="dropdownMenuLink2" data-toggle="dropdown" aria-expanded="true">Reassign Request</a>
                                                                <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <?php
                                                                        $accept_reject = 0;
                                                                        $order_item_part_id = 0;
                                                                    ?>
                                                                    @foreach($value['order_reassign'] as $order_reassign_value)
                                                                    <?php
                                                                        $accept_reject = $order_reassign_value['accept_reject'];
                                                                        $order_item_part_id = $order_reassign_value['order_item_part_id'];

                                                                    ?>
                                                                        <div class="tm_info_bx">
                                                                            <h4 class="color_orange">Reassign Request</h4>
                                                                            <div class="tm_info_list"> <span>Reassign Reason :</span> {{ $order_reassign_value['reassign_reason'] }} </div>
                                                                            <div class="tm_info_list"> <span>Reassign Desc :</span> {{ $order_reassign_value['reassign_desc'] }} </div>
                                                                            <div class="tm_info_list"> <span>Previous Delivery Date :</span> {{ AdminController::dateTimeFormat($order_reassign_value["previous_start_time"]) }} - {{ AdminController::dateTimeFormat($order_reassign_value["previous_end_time"]) }} </div>
                                                                            <div class="tm_info_list"> <span>New Delivery Date :</span> {{ AdminController::dateTimeFormat($order_reassign_value["new_start_time"]) }} - {{ AdminController::dateTimeFormat($order_reassign_value["new_end_time"]) }} </div>
                                                                            <div class="tm_info_list"> <span>Status :</span> 
                                                                                                @if($order_reassign_value['accept_reject'] == 0) 
                                                                                                    <span class="badge bg-yellow">No Status</span>
                                                                                                @elseif($order_reassign_value['accept_reject'] == 1)
                                                                                                    <span class="badge bg-yellow">Accepted</span>
                                                                                                @elseif($order_reassign_value['accept_reject'] == 2)
                                                                                                    <span class="badge bg-yellow">Rejected</span>
                                                                                                @endif
                                                                            
                                                                            @if(isset($order_reassign_value['order_track']) && count($order_reassign_value['order_track']) > 0)
                                                                                <div class="tm_info_list">
                                                                                    <h4 class="color_orange">Track Details</h4> 
                                                                                    <div class="tm_info_list">
                                                                                        <span>TM No. :</span> {{ $order_reassign_value['order_track']['TM_rc_number'] }} 
                                                                                    </div>
                                                                                    <div class="tm_info_list">
                                                                                        <span>Qty. :</span> {{ $order_reassign_value['order_track']['pickup_quantity'] }} 
                                                                                    </div>
                                                                                    <!-- <div class="tm_info_list">
                                                                                        <span>Status :</span> {{ $order_reassign_value['order_track']['event_status'] }} 
                                                                                    </div> -->
                                                                                    
                                                                                    
                                                                                </div>
                                                                            @endif
                                                                            
                                                                            
                                                                        </div>
                                                                        <hr/>
                                                                    @endforeach

                                                                </div>
                                                            </div>

                                                        @else
                                                            @if($value["event_status"] == 'REJECTED')
                                                                <!-- <a href="javascript:void(0)" onclick="ReasignOrder()">Reassigning Request</a> -->
                                                                <div class="dropdown cstm-dropdown-select">
                                                                    <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                        <span class="dropdown-label">Reassigning Request <i class="fa fa-sort-down"></i></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                        
                                                                        <li><a class="dropdown-item" href="javascript:void(0);" onclick="ReasignOrder('order_rejected','{{ $value["sub_vendor_id"] }}','{{ $value["_id"] }}')">Due To Order Rejected</a></li>
                                                                        
                                                                        <!-- <li><a class="dropdown-item" href="javascript:void(0);" onclick="ReasignOrder('breakdown','{{ $value["sub_vendor_id"] }}','{{ $value["_id"] }}')">Due To Any Breakdown</a></li>
                                                                    
                                                                        <li><a class="dropdown-item" href="javascript:void(0);" onclick="ReasignOrder('material_out_of_stock','{{ $value["sub_vendor_id"] }}','{{ $value["_id"] }}')">Due To Material Out Of Stock</a></li> -->
                                                                    
                                                                    </ul>
                                                                </div>
                                                            @else
                                                                -
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="itemTrack('{{ $value['order_item_id'] }}','{{ $value['_id'] }}')">Track</a>
                                                    </td>
                                                    <td>
                                                        @if(isset($value["qube_test_report_7days"]))
                                                            
                                                            <div class="ordlst-date-block">
                                                                <a href="{{ isset($value["qube_test_report_7days"]) ? $value["qube_test_report_7days"] : '' }}"
                                                                    style="float: left;">
                                                                    <!-- <span class="chq-img-block">
                                                                        <img src="{{ isset($value["qube_test_report_7days"]) ? $value["qube_test_report_7days"] : '' }}" alt="" />
                                                                    </span> -->
                                                                    View
                                                                </a>
                                                                <span style="display: block;">
                                                                    <!-- <img src="{{ $value['qube_test_report_7days'] }}" width="50"/> -->
                                                                    <!-- <div class="dropdown order_dtl_drop"> -->
                                                                        <a class="" style="color:#136e9d;margin-left: 10px;" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-expanded="false">
                                                                            <!-- <i class="fas fa-ellipsis-v"></i> -->
                                                                            @if(isset($value['qube_test_report_7days_accept_reject']) && $value['qube_test_report_7days_accept_reject'] == 1)
                                                                                <i class="fa fa-check-circle" style="color:#2ECC71;font-size: 20px;" title="Accepted By Buyer"></i>
                                                                            @elseif(isset($value['qube_test_report_7days_accept_reject']) && $value['qube_test_report_7days_accept_reject'] == 2)
                                                                                <i class="fa fa-times-circle" style="color:#dc3545;font-size: 20px;" 
                                                                                title="Reason Of Rejection: {{ isset($value['qube_test_report_7days_reject_reason']) ? $value['qube_test_report_7days_reject_reason'] : 'Rejected By Buyer' }}"></i>
                                                                            @endif
                                                                        </a>
                                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink"
                                                                        style="position: inherit;">
                                                                            <li class="dropdown-item">
                                                                                <a href="{{ $value['qube_test_report_7days'] }}">Download </a>
                                                                                
                                                                                
                                                                            </li>
                                                                            <li class="dropdown-item">                                                                                

                                                                                @if(isset($value['buyer_qube_test_report_7days']) && $value['qube_test_report_7days_accept_reject'] == 2)
                                                                                    <a href="{{ $value['buyer_qube_test_report_7days'] }}">Download Buyer Report </a>
                                                                                @endif
                                                                            </li>
                                                                            @if(isset($value['qube_test_report_7days_accept_reject']) && $value['qube_test_report_7days_accept_reject'] == 2)
                                                                                <li class="dropdown-item" style="text-wrap: pretty;">                                                                                

                                                                                    <b>Reason Of Rejection</b>: 
                                                                                    {{ isset($value['qube_test_report_7days_reject_reason']) ? $value['qube_test_report_7days_reject_reason'] : 'Rejected By Buyer' }}
                                                                                </li>
                                                                            @endif
                                                                        </ul>
                                                                        
                                                                    <!-- </div> -->
                                                                </span>
                                                            </div>
                                                        @else
                                                            @if(isset($value["delivered_at"]))
                                                                    <?php
                                                                        // $delivered_at = date('Y-m-d',strtotime($value["delivered_at"]));
                                                                        $now = time(); // or your date as well
                                                                        $your_date = strtotime($value["delivered_at"]);
                                                                        // $your_date = strtotime("2021-08-15");
                                                                        $datediff = $now - $your_date;

                                                                        $days = round($datediff / (60 * 60 * 24));
                                                                        // echo round($datediff / (60 * 60 * 24));

                                                                        $remaining_days = 7 - $days;

                                                                        if($remaining_days > 0){

                                                                            ?>
                                                                            <a href="javascript:void(0)" >{{ $remaining_days }} days are left</a>
                                                                            <?php

                                                                        }else{
                                                                            ?>
                                                                            <a href="javascript:void(0)" onclick="show7CubeReportDialog('{{ $value['_id'] }}','{{ $value['TM_id'] }}','{{ $value['vendor_order_id'] }}')">Upload Report</a>
                                                                            <?php
                                                                        }
                                                                    ?>
                                                                @else
                                                                    <!-- <a href="javascript:void(0)" >Not delivered</a> -->
                                                                    -
                                                                @endif
                                                        @endif
                                                    </td>
                                                    <td>

                                                        @if(isset($value["qube_test_report_28days"]))
                                                            <div class="ordlst-date-block">
                                                                <a href="{{ isset($value["qube_test_report_28days"]) ? $value["qube_test_report_28days"] : '' }}"
                                                                style="float: left;">
                                                                    <!-- <span class="chq-img-block"><img src="{{ isset($value["qube_test_report_28days"]) ? $value["qube_test_report_28days"] : '' }}" alt="" /></span> -->
                                                                    View
                                                                </a>
                                                                <span style="display: block;">
                                                                    <!-- <img src="{{ $value['qube_test_report_28days'] }}" width="50"/> -->
                                                                    <!-- <div class="dropdown order_dtl_drop"> -->
                                                                        <a class="" style="color:#136e9d;margin-left: 10px;" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-expanded="false">
                                                                            <!-- <i class="fas fa-ellipsis-v"></i> -->
                                                                            @if(isset($value['qube_test_report_28days_accept_reject']) && $value['qube_test_report_28days_accept_reject'] == 1)
                                                                                <i class="fa fa-check-circle" style="color:#2ECC71;font-size: 20px;" title="Accepted By Buyer"></i>
                                                                            @elseif(isset($value['qube_test_report_28days_accept_reject']) && $value['qube_test_report_28days_accept_reject'] == 2)
                                                                                <i class="fa fa-times-circle" style="color:#dc3545;font-size: 20px;" 
                                                                                title="Reason Of Rejection: {{ isset($value['qube_test_report_28days_reject_reason']) ? $value['qube_test_report_28days_reject_reason'] : 'Rejected By Buyer' }}"></i>
                                                                            @endif
                                                                        </a>
                                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink"
                                                                        style="position: inherit;">
                                                                            <li class="dropdown-item">
                                                                                <a href="{{ $value['qube_test_report_7days'] }}">Download </a>
                                                                                
                                                                                
                                                                            </li>
                                                                            <li class="dropdown-item">                                                                                

                                                                                @if(isset($value['buyer_qube_test_report_28days']) && $value['qube_test_report_28days_accept_reject'] == 2)
                                                                                    <a href="{{ $value['buyer_qube_test_report_28days'] }}">Download Buyer Report </a>
                                                                                @endif
                                                                            </li>
                                                                            @if(isset($value['qube_test_report_28days_accept_reject']) && $value['qube_test_report_28days_accept_reject'] == 2)
                                                                                <li class="dropdown-item" style="text-wrap: pretty;">                                                                                

                                                                                    <b>Reason Of Rejection</b>: 
                                                                                    {{ isset($value['qube_test_report_28days_reject_reason']) ? $value['qube_test_report_28days_reject_reason'] : 'Rejected By Buyer' }}
                                                                                </li>
                                                                            @endif
                                                                        </ul>
                                                                        
                                                                    <!-- </div> -->
                                                                </span>
                                                            </div>
                                                        @else

                                                            @if(isset($value["delivered_at"]))
                                                                <?php
                                                                    // $delivered_at = date('Y-m-d',strtotime($value["delivered_at"]));
                                                                    $now = time(); // or your date as well
                                                                    // $your_date = strtotime("2021-08-15");
                                                                    $your_date = strtotime($value["delivered_at"]);
                                                                    $datediff = $now - $your_date;

                                                                    $days = round($datediff / (60 * 60 * 24));
                                                                    // echo round($datediff / (60 * 60 * 24));

                                                                    $remaining_days = 28 - $days;

                                                                    if($remaining_days > 0){

                                                                        ?>
                                                                        <a href="javascript:void(0)" >{{ $remaining_days }} days are left</a>
                                                                        <?php

                                                                    }else{
                                                                        ?>
                                                                        <a href="javascript:void(0)" onclick="show28CubeReportDialog('{{ $value['_id'] }}','{{ $value['TM_id'] }}','{{ $value['vendor_order_id'] }}')">Upload Report</a>
                                                                        <?php
                                                                    }
                                                                ?>
                                                            @else
                                                                <!-- <a href="javascript:void(0)" >Not delivered</a> -->
                                                                -

                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>

                                                        @if(isset($value["vendor_invoice_url"]) && !empty($value["vendor_invoice_url"]))

                                                            <a href="{{ $value['vendor_invoice_url'] }}" download>Download Invoice</a>

                                                        @else   

                                                            <!-- <a href="javascript:void(0)" >Not Generated</a>  -->
                                                            -

                                                        @endif

                                                    </td>
                                                    <td>

                                                        @if(isset($value["invoice_url"]) && !empty($value["invoice_url"]))

                                                            <a href="{{ $value['invoice_url'] }}" download>Download Invoice</a>

                                                        @else   

                                                            <!-- <a href="javascript:void(0)" >Not Generated</a>  -->
                                                            -
                                                        @endif

                                                    </td>
                                                    <td>

                                                        @if(isset($value["vendor_debit_note"]) && !empty($value["vendor_debit_note"]))

                                                            <a href="{{ $value['vendor_debit_note'] }}" download>Download Debit Note</a>

                                                        @else   

                                                            <!-- <a href="javascript:void(0)" >Not Generated</a>  -->
                                                            -
                                                        @endif

                                                    </td>

                                                </tr>
                                                @endforeach
                                            @else
                                            <tr>
                                                <td colspan="13">No Record Found</td>
                                            </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--order Details end -->
</div>

<script>
    $(document).ready(function(){
        $('.dropdown-submenu a.dropdown-submenu-link').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
</script>



@endsection