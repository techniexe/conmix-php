
<?php use App\Http\Controllers\Admin\AdminController;?>
<?php //dd($data); ?>
<table class="table">
    <thead>
        <tr class="tr_heading">
            <th colspan="3">Select Affected Order</th>
            <!-- <th colspan="5">Select Checkbox Of Out Of Stock Material</th> -->
        </tr>
        <tr>
            <th style="width: 14%;text-align: center;vertical-align: middle;">Order Id</th>
            <th style="width: 14%;text-align: center;vertical-align: middle;">New Delivery Date </th>
            <th style="width: 14%;text-align: center;vertical-align: middle;">Qty (Cu.Mtr) </th>
            <!-- <th style="width: 14%;text-align: center;vertical-align: middle;">Cement</th>
            <th style="width: 14%;text-align: center;vertical-align: middle;">Sand</th>
            <th style="width: 14%;text-align: center;vertical-align: middle;">Aggregate</th>
            <th style="width: 14%;text-align: center;vertical-align: middle;">Admixture </th>
            <th style="width: 14%;text-align: center;vertical-align: middle;">Flyash </th> -->
        </tr>
    </thead>
    <tbody id="reassigned_order_popup_tbody">
        @if(isset($data['data']) && count($data['data']) > 0)
            @foreach($data['data'] as $value)
                <tr>
                    <td style="width: 14%;text-align: center;vertical-align: middle;">
                        <label for="vendor_order_id_checkbox_{{ $value["orderItemPart"]['_id'] }}" class="checkbox">
                            <a href="{{ route('supplier_orders_details',$value['vendor_order_id']) }}">
                                #{{ $value['vendor_order_id'] }} 
                            </a>
                        </label>
                        <div class="check_blue m-0" style="/*display: flex;*/align-items: center;">
                            <input type="checkbox" value="{{ $value["vendor_order_id"] }}" 
                            data-item-part-id="{{ $value["orderItemPart"]['_id'] }}" 
                            class="form-control vendor_order_id_checkbox" 
                            id="vendor_order_id_checkbox_{{ $value["orderItemPart"]['_id'] }}" 
                            style="width: 20px;margin-right: 6px;display: initial;" >
                            

                        </div>    
                        <input type="hidden" name="vendor_order_id[]" id="vendor_order_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                        <input type="hidden" name="order_item_part_id[]" value="{{ $value["orderItemPart"]['_id'] }}"  />
                        <input type="hidden" name="buyer_order_id[]" value="{{ $value["orderItemPart"]['buyer_order_id'] }}"  />
                        <input type="hidden" name="buyer_order_item_id[]" value="{{ $value["orderItemPart"]['order_item_id'] }}"  />
                        
                        <input type="hidden" name="previous_order_delivery_start_date[]" value="{{ $value["orderItemPart"]['start_time'] }}" 
                        id="previous_order_delivery_start_date_{{ $value["orderItemPart"]['_id'] }}"  />
                        
                        <input type="hidden" name="previous_order_delivery_end_date[]" value="{{ $value["orderItemPart"]['end_time'] }}"  
                        id="previous_order_delivery_end_date_{{ $value["orderItemPart"]['_id'] }}" />
                        
                        <input type="hidden" name="vendor_id[]" value="{{ $value["vendor_id"] }}"  />
                        <input type="hidden" name="buyer_id[]" value="{{ $value["buyer_id"] }}"  />

                        <span class="error" id="vendor_order_error_{{ $value["orderItemPart"]['_id'] }}"></span>                                             
                    </td>
                    <td style="width: 14%;text-align: center;vertical-align: middle;">
                        <label class="checkbox">
                            Delivery Date: ({{ AdminController::dateTimeFormat($value["orderItemPart"]['start_time']) }} to {{ AdminController::dateTimeFormat($value["orderItemPart"]['end_time']) }})    
                        </label>
                        <div class="">
                            <div class="input-group">
                                <div class="range_picker m-r10" 
                                id="order_new_date_div_{{ $value["orderItemPart"]['_id'] }}"
                                style="display:none;width: 100%;margin-right: 0px;">
                                    <input type="text" autocomplete="off"
                                    id="order_new_date_{{ $value["orderItemPart"]['_id'] }}"
                                    data-item-part-id="{{ $value["orderItemPart"]['_id'] }}" 
                                     class="form-control"  placeholder="Select New Order Date">
                                    <i class="fa fa-calendar"></i>
                                </div>                                

                                <input type="hidden" name="new_order_delivery_start_date[]" id="new_order_delivery_start_date_{{ $value["orderItemPart"]['_id'] }}"  />
                                <input type="hidden" name="new_order_delivery_end_date[]" id="new_order_delivery_end_date_{{ $value["orderItemPart"]['_id'] }}"  />

                            </div>
                            
                            <div class="input-group" style="margin-top:10px;">
                                <select class="form-control delivery_start_time" 
                                id="delivery_start_time_{{ $value["orderItemPart"]['_id'] }}"
                                data-item-part-id="{{ $value["orderItemPart"]['_id'] }}" 
                                style="display:none;">
                                    <option disabled="disabled" selected="selected" value=''>Start Time</option>

                                    <option value="00:00">00:00</option>
                                    <option value="01:00">01:00</option>
                                    <option value="02:00">02:00</option>
                                    <option value="03:00">03:00</option>
                                    <option value="04:00">04:00</option>
                                    <option value="05:00">05:00</option>
                                    <option value="06:00">06:00</option>
                                    <option value="07:00">07:00</option>
                                    <option value="08:00">08:00</option>
                                    <option value="09:00">09:00</option>
                                    <option value="10:00">10:00</option>
                                    <option value="11:00">11:00</option>

                                    <option value="12:00">12:00</option>
                                    <option value="13:00">13:00</option>
                                    <option value="14:00">14:00</option>
                                    <option value="15:00">15:00</option>
                                    <option value="16:00">16:00</option>
                                    <option value="17:00">17:00</option>
                                    <option value="18:00">18:00</option>
                                    <option value="19:00">19:00</option>
                                    <option value="20:00">20:00</option>
                                    <option value="21:00">21:00</option>
                                    <option value="22:00">22:00</option>
                                    <option value="23:00">23:00</option>

                                </select>

                            </div>
                            <span class="error" id="new_delivery_date_error_{{ $value["orderItemPart"]['_id'] }}"></span>
                        </div>
                    </td>
                    <td style="width: 14%;text-align: center;vertical-align: middle;">
                        <label class="checkbox">{{ $value["orderItemPart"]['temp_quantity'] }} </label>
                        
                    </td>
                    @if(false)
                        <td style="width: 14%;text-align: center;vertical-align: middle;">
                            <label for="cement_checkbox_{{ $value["orderItemPart"]['_id'] }}" class="checkbox">{{ $value['cement_brand_name'] }} - {{ $value['cement_grade_name'] }} </label>
                            <div class="check_blue m-0" style="/*display: flex;*/align-items: center;">
                                <input type="checkbox" id="cement_checkbox_{{ $value["orderItemPart"]['_id'] }}" 
                                data-cement-brand-id="{{ $value["cement_brand_id"] }}" 
                                data-cement-grade-id="{{ $value["cement_grade_id"] }}" 
                                data-item-part-id="{{ $value["orderItemPart"]['_id'] }}" 
                                class="form-control cement_checkbox" 
                                style="width: 20px;margin-right: 6px;display: none;" >

                                <input type="hidden" name="cement_brand_id[]" id="cement_brand_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                                <input type="hidden" name="cement_grade_id[]" id="cement_grade_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                            </div> 
                        </td>
                        <td style="width: 14%;text-align: center;vertical-align: middle;">
                            <label for="sand_checkbox_{{ $value["orderItemPart"]['_id'] }}" class="checkbox">{{ $value['sand_source_name'] }} - {{ $value['sand_zone_name'] }} </label>
                            <div class="check_blue m-0" style="/*display: flex;*/align-items: center;">
                                <input type="checkbox" id="sand_checkbox_{{ $value["orderItemPart"]['_id'] }}" 
                                data-sand-source-id="{{ $value["sand_source_id"] }}" 
                                data-sand-zone-id="{{ $value["sand_zone_id"] }}" 
                                data-item-part-id="{{ $value["orderItemPart"]['_id'] }}" 
                                class="form-control sand_checkbox" 
                                style="width: 20px;margin-right: 6px;display: none;" >

                                <input type="hidden" name="sand_source_id[]" id="sand_source_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                                <input type="hidden" name="sand_zone_id[]" id="sand_zone_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                            </div> 
                        </td>
                        <td style="width: 14%;text-align: center;vertical-align: middle;">
                            <label for="aggr_checkbox_{{ $value["orderItemPart"]['_id'] }}" class="checkbox">{{ $value['agg_source_name'] }} - {{ $value['aggregate1_sub_cat_name'] }} - {{ $value['aggregate2_sub_cat_name'] }} </label>
                            <div class="check_blue m-0" style="/*display: flex;*/align-items: center;">
                                <input type="checkbox" id="aggr_checkbox_{{ $value["orderItemPart"]['_id'] }}" 
                                data-aggr-source-id="{{ $value["agg_source_id"] }}" 
                                data-aggr-cat1-id="{{ $value["aggregate1_sub_cat_id"] }}" 
                                data-aggr-cat2-id="{{ $value["aggregate2_sub_cat_id"] }}" 
                                data-item-part-id="{{ $value["orderItemPart"]['_id'] }}" 
                                class="form-control aggr_checkbox" 
                                style="width: 20px;margin-right: 6px;display: none;" >

                                <input type="hidden" name="aggr_source_id[]" id="aggr_source_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                                <input type="hidden" name="aggr_cat1_id[]" id="aggr_cat1_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                                <input type="hidden" name="aggr_cat2_id[]" id="aggr_cat2_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                            </div> 
                        </td>
                        <td style="width: 14%;text-align: center;vertical-align: middle;">
                            <label for="admix_checkbox_{{ $value["orderItemPart"]['_id'] }}" class="checkbox">{{ $value['ad_mixture1_brand_name'] }} - {{ $value['ad_mixture1_category_name'] }} </label>
                            <div class="check_blue m-0" style="/*display: flex;*/align-items: center;">
                                <input type="checkbox" id="admix_checkbox_{{ $value["orderItemPart"]['_id'] }}" 
                                data-admix-brand-id="{{ $value["ad_mixture1_brand_id"] }}" 
                                data-admix-cat-id="{{ $value["ad_mixture1_category_id"] }}"
                                data-item-part-id="{{ $value["orderItemPart"]['_id'] }}" 
                                class="form-control admix_checkbox" 
                                style="width: 20px;margin-right: 6px;display: none;" >

                                <input type="hidden" name="admix_brand_id[]" id="admix_brand_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                                <input type="hidden" name="admix_cat_id[]" id="admix_cat_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                            </div> 
                        </td>
                        <td style="width: 14%;text-align: center;vertical-align: middle;">
                            <label for="flyash_checkbox_{{ $value["orderItemPart"]['_id'] }}" class="checkbox">{{ $value['fly_ash_source_name'] }} </label>
                            <div class="check_blue m-0" style="/*display: flex;*/align-items: center;">
                                <input type="checkbox" id="flyash_checkbox_{{ $value["orderItemPart"]['_id'] }}" 
                                data-flyash-source-id="{{ $value["fly_ash_source_id"] }}" 
                                data-item-part-id="{{ $value["orderItemPart"]['_id'] }}" 
                                class="form-control flyash_checkbox" 
                                style="width: 20px;margin-right: 6px;display: none;" >

                                <input type="hidden" name="flyash_source_id[]" id="flyash_source_ids_{{ $value["orderItemPart"]['_id'] }}" value=""  />
                            </div> 
                        </td>
                    @endif
                </tr>

                <script>
                    $(function() {

                        var today = new Date();
                        var end = new Date();
                        // today.setDate(today.getDate() + 2);
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();

                        today = dd + '/' + mm + '/' + yyyy;
                        end = dd + '/' + mm + '/' + yyyy;
                        console.log(today);

                        var current_delivery_date = new Date('<?php echo $value["orderItemPart"]['start_time']; ?>');
                        current_delivery_date.setDate(current_delivery_date.getDate() + 1);
                        var dd = String(current_delivery_date.getDate()).padStart(2, '0');
                        var mm = String(current_delivery_date.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = current_delivery_date.getFullYear();

                        current_delivery_date = dd + '/' + mm + '/' + yyyy;

                        $('#order_new_date_{{ $value["orderItemPart"]['_id'] }}').datepicker({
                                autoclose: true,
                                startDate: current_delivery_date,
                                format: 'dd M yyyy'
                                }).on('changeDate', function(selected){
                                    // $("#new_delivery_date_error_{{ $value["orderItemPart"]['_id'] }}").html("");
                                    
                            
                                    var selected_time_value = $('#delivery_start_time_{{ $value["orderItemPart"]['_id'] }}').val();
                                    var selected_date_value = $('#order_new_date_{{ $value["orderItemPart"]['_id'] }}').val();
                                    
                                    console.log("selected_date_value......."+selected_date_value);
                                    
                                    if(selected_date_value != '' && selected_time_value != null){
                                        calcNewDeliveryEndData(selected_date_value,selected_time_value,'{{ $value["orderItemPart"]['_id'] }}');

                                        
                                    }
                                    reassignMaterialValdation();
                                    
                            });
                        });

                </script>
            @endforeach

            <script>
                    $(function() {

                        $('.vendor_order_id_checkbox').change(function(){

                            var order_item_part_id = $(this).attr('data-item-part-id');
                            console.log("data-item-part-id......."+order_item_part_id);
                            
                            if(this.checked) {

                                var vendor_order_id = $(this).val();
                                $('#vendor_order_ids_'+order_item_part_id).val(vendor_order_id);

                                $('#order_new_date_div_'+order_item_part_id).css('display','initial');
                                $('#delivery_start_time_'+order_item_part_id).css('display','initial');
                                $('#cement_checkbox_'+order_item_part_id).css('display','initial');
                                $('#sand_checkbox_'+order_item_part_id).css('display','initial');
                                $('#aggr_checkbox_'+order_item_part_id).css('display','initial');
                                $('#admix_checkbox_'+order_item_part_id).css('display','initial');
                                $('#flyash_checkbox_'+order_item_part_id).css('display','initial');                                
                                

                            }else{

                                $('#vendor_order_ids_'+order_item_part_id).val('');

                                $('#order_new_date_div_'+order_item_part_id).css('display','none');
                                $('#delivery_start_time_'+order_item_part_id).css('display','none');
                                $('#cement_checkbox_'+order_item_part_id).css('display','none');
                                $('#sand_checkbox_'+order_item_part_id).css('display','none');
                                $('#aggr_checkbox_'+order_item_part_id).css('display','none');
                                $('#admix_checkbox_'+order_item_part_id).css('display','none');
                                $('#flyash_checkbox_'+order_item_part_id).css('display','none');

                                $("#vendor_order_error_"+order_item_part_id).html("");
                                $("#new_delivery_date_error_"+order_item_part_id).html("");

                                if($('#cement_checkbox_'+order_item_part_id).prop('checked') == true){
                                    //do something
                                    $('#cement_checkbox_'+order_item_part_id).prop('checked',false).trigger("change");
                                }
                                
                                if($('#sand_checkbox_'+order_item_part_id).prop('checked') == true){
                                    //do something
                                    $('#sand_checkbox_'+order_item_part_id).prop('checked',false).trigger("change");
                                }
                                
                                if($('#aggr_checkbox_'+order_item_part_id).prop('checked') == true){
                                    //do something
                                    $('#aggr_checkbox_'+order_item_part_id).prop('checked',false).trigger("change");
                                }
                                
                                if($('#admix_checkbox_'+order_item_part_id).prop('checked') == true){
                                    //do something
                                    $('#admix_checkbox_'+order_item_part_id).prop('checked',false).trigger("change");
                                }
                                
                                if($('#flyash_checkbox_'+order_item_part_id).prop('checked') == true){
                                    //do something
                                    $('#flyash_checkbox_'+order_item_part_id).prop('checked',false).trigger("change");
                                }

                            }

                            reassignMaterialValdation();

                        });
                        
                        $('.cement_checkbox').change(function(){

                            var order_item_part_id = $(this).attr('data-item-part-id');
                            console.log("data-item-part-id......."+order_item_part_id);
                            
                            if(this.checked) {

                                var cement_brand_id = $(this).attr('data-cement-brand-id');
                                var cement_grade_id = $(this).attr('data-cement-grade-id');
                                
                                $('#cement_brand_ids_'+order_item_part_id).val(cement_brand_id);
                                $('#cement_grade_ids_'+order_item_part_id).val(cement_grade_id);                                                            
                                

                            }else{

                                $('#cement_brand_ids_'+order_item_part_id).val('');
                                $('#cement_grade_ids_'+order_item_part_id).val('');

                            }

                            reassignMaterialValdation();

                        });
                        
                        $('.sand_checkbox').change(function(){

                            var order_item_part_id = $(this).attr('data-item-part-id');
                            console.log("data-item-part-id......."+order_item_part_id);
                            
                            if(this.checked) {

                                var sand_source_id = $(this).attr('data-sand-source-id');
                                var sand_zone_id = $(this).attr('data-sand-zone-id');
                                
                                $('#sand_source_ids_'+order_item_part_id).val(sand_source_id);
                                $('#sand_zone_ids_'+order_item_part_id).val(sand_zone_id);                                                            
                                

                            }else{

                                $('#sand_source_ids_'+order_item_part_id).val('');
                                $('#sand_zone_ids_'+order_item_part_id).val('');

                            }

                            reassignMaterialValdation();

                        });
                        
                        $('.aggr_checkbox').change(function(){

                            var order_item_part_id = $(this).attr('data-item-part-id');
                            console.log("data-item-part-id......."+order_item_part_id);
                            
                            if(this.checked) {

                                var aggr_source_id = $(this).attr('data-aggr-source-id');
                                var aggr_cat1_id = $(this).attr('data-aggr-cat1-id');
                                var aggr_cat2_id = $(this).attr('data-aggr-cat2-id');
                                
                                $('#aggr_source_ids_'+order_item_part_id).val(aggr_source_id);
                                $('#aggr_cat1_ids_'+order_item_part_id).val(aggr_cat1_id);                                                            
                                $('#aggr_cat2_ids_'+order_item_part_id).val(aggr_cat2_id);                                                            
                                

                            }else{

                                $('#aggr_source_ids_'+order_item_part_id).val('');
                                $('#aggr_cat1_ids_'+order_item_part_id).val('');                                                            
                                $('#aggr_cat2_ids_'+order_item_part_id).val('');     

                            }

                            reassignMaterialValdation();

                        });
                        
                        $('.admix_checkbox').change(function(){

                            var order_item_part_id = $(this).attr('data-item-part-id');
                            console.log("data-item-part-id......."+order_item_part_id);
                            
                            if(this.checked) {

                                var admix_brand_id = $(this).attr('data-admix-brand-id');
                                var admix_cat_id = $(this).attr('data-admix-cat-id');
                                
                                $('#admix_brand_ids_'+order_item_part_id).val(admix_brand_id);
                                $('#admix_cat_ids_'+order_item_part_id).val(admix_cat_id);                                                              
                                

                            }else{

                                $('#admix_brand_ids_'+order_item_part_id).val('');
                                $('#admix_cat_ids_'+order_item_part_id).val('');  

                            }

                            reassignMaterialValdation();

                        });
                        
                        $('.flyash_checkbox').change(function(){

                            var order_item_part_id = $(this).attr('data-item-part-id');
                            console.log("data-item-part-id......."+order_item_part_id);
                            
                            if(this.checked) {

                                var flyash_source_id = $(this).attr('data-flyash-source-id');
                                
                                $('#flyash_source_ids_'+order_item_part_id).val(flyash_source_id);                                                        
                                

                            }else{

                                $('#flyash_source_ids_'+order_item_part_id).val('');

                            }

                            reassignMaterialValdation();

                        });
                        
                        $('.delivery_start_time').change(function(){

                            var order_item_part_id = $(this).attr('data-item-part-id');
                            
                            var selected_time_value = $('option:selected', this).val();
                            var selected_date_value = $('#order_new_date_'+order_item_part_id).val();
                            
                            console.log("selected_date_value......."+selected_date_value);
                            
                            if(selected_date_value != ''){
                                calcNewDeliveryEndData(selected_date_value,selected_time_value,order_item_part_id)

                                
                            }

                            reassignMaterialValdation();

                        });

                        
                        
                    });

                    function calcNewDeliveryEndData(selected_date,selected_start_time,order_item_part_id){

                        $previous_order_delivery_start_date = $("#previous_order_delivery_start_date_"+order_item_part_id).val();
                        $previous_order_delivery_end_date = $("#previous_order_delivery_end_date_"+order_item_part_id).val();

                        date1 = new Date($previous_order_delivery_start_date);
                        date2 = new Date($previous_order_delivery_end_date);

                        var diffHours = parseInt((date2 - date1) / (1000 * 60 * 60), 10); 

                        // console.log("$previous_order_delivery_start_date......."+$previous_order_delivery_start_date);
                        // console.log("$previous_order_delivery_end_date......."+$previous_order_delivery_end_date);
                        // console.log("diffHours......."+diffHours);

                        var delivery_date = new Date(selected_date);

                        var dd = String(delivery_date.getDate()).padStart(2, '0');
                        var mm = String(delivery_date.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = delivery_date.getFullYear();

                        delivery_date = yyyy+"-"+mm+"-"+dd;

                        var start_date_time = dd + '-' + mm + '-' + yyyy+" "+selected_start_time;

                        var end_date_time = new Date(delivery_date+"T"+selected_start_time+":00+05:30");

                        end_date_time.setHours(end_date_time.getHours() + parseInt(diffHours));

                        var dd = String(end_date_time.getDate()).padStart(2, '0');
                        var mm = String(end_date_time.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = end_date_time.getFullYear();
                        var hours = end_date_time.getHours();
                        var min = end_date_time.getMinutes();

                        end_date_time = dd + '-' + mm + '-' + yyyy+" "+hours+":00";

                        $("#new_order_delivery_start_date_"+order_item_part_id).val(start_date_time);
                        $("#new_order_delivery_end_date_"+order_item_part_id).val(end_date_time);
                    }

                    function reassignMaterialValdation(){

                        var order_reassign_reason = $('#order_reassign_reason_field').val();
                        var all_good = true;
                        if(order_reassign_reason == 'material_out_of_stock'){
                            $is_any_order_selected = 0;
                            $(".vendor_order_id_checkbox").each(function(){
                                if ($(this).prop('checked')==true){ 
                                    $is_any_order_selected = 1;
                                    //do something
                                    var order_item_part_id = $(this).attr('data-item-part-id');    
                                    console.log("checkbox checked");

                                    $is_any_material_selected = 0;
                                    $is_new_date_selected = 0;
                                    $is_new_time_selected = 0;

                                    if(($("#order_new_date_"+order_item_part_id).val() == '') || ($("#delivery_start_time_"+order_item_part_id).val() == null)){
                                        all_good = false;
                                        $("#new_delivery_date_error_"+order_item_part_id).html("Please select the new delivery date and time");
                                    }else{
                                        $("#new_delivery_date_error_"+order_item_part_id).html("");
                                    }
                                    
                                    // if($("#delivery_start_time_"+order_item_part_id).val() == ''){
                                    //     all_good = false;
                                    //     $("#new_delivery_date_error_"+order_item_part_id).html("Please select the new delivery date and time");
                                    // }else{
                                    //     $("#new_delivery_date_error_"+order_item_part_id).html("");
                                    // }

                                    if($('#cement_checkbox_'+order_item_part_id).prop('checked') == true){
                                        //do something
                                        $is_any_material_selected = 1;
                                    }
                                    
                                    if($('#sand_checkbox_'+order_item_part_id).prop('checked') == true){
                                        //do something
                                        $is_any_material_selected = 1;
                                    }
                                    
                                    if($('#aggr_checkbox_'+order_item_part_id).prop('checked') == true){
                                        //do something
                                        $is_any_material_selected = 1;
                                    }
                                    
                                    if($('#admix_checkbox_'+order_item_part_id).prop('checked') == true){
                                        //do something
                                        $is_any_material_selected = 1;
                                    }
                                    
                                    if($('#flyash_checkbox_'+order_item_part_id).prop('checked') == true){
                                        //do something
                                        $is_any_material_selected = 1;
                                    }

                                    if($is_any_material_selected == 0){
                                        all_good = false;
                                        $("#vendor_order_error_"+order_item_part_id).html("Please select the out of stock material");
                                    }else{
                                        $("#vendor_order_error_"+order_item_part_id).html("");
                                        
                                    }

                                }
                            });

                            if($is_any_order_selected == 0){
                                all_good = false;
                                $("#order_selection_error").html("Please select atleast one order");
                            }else{
                                $("#order_selection_error").html("");
                            }

                            return all_good;
                        }else if(order_reassign_reason == 'breakdown'){
                            
                            $(".vendor_order_id_checkbox").each(function(){
                                if ($(this).prop('checked')==true){ 
                                    $is_any_order_selected = 1;
                                    //do something
                                    var order_item_part_id = $(this).attr('data-item-part-id');    
                                    console.log("checkbox checked");

                                    $is_new_date_selected = 0;
                                    $is_new_time_selected = 0;

                                    if(($("#order_new_date_"+order_item_part_id).val() == '') || ($("#delivery_start_time_"+order_item_part_id).val() == null)){
                                        all_good = false;
                                        $("#new_delivery_date_error_"+order_item_part_id).html("Please select the new delivery date and time");
                                    }else{
                                        $("#new_delivery_date_error_"+order_item_part_id).html("");
                                    }                       
                                

                                }
                            });

                            if($is_any_order_selected == 0){
                                all_good = false;
                                $("#order_selection_error").html("Please select atleast one order");
                            }else{
                                $("#order_selection_error").html("");
                            }

                            return all_good;

                        }
}
                        
                </script>

        @else

        <tr>
            <td colspan="7">
                No Orders Available
            </td>
        </tr>

        @endif
    </tbody>
</table>

