<?php //dd($data["data"]); ?>
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="track-block">
    @if(isset($data["data"]))
        <?php $final_data = $data["data"];
        //dd($final_data)?>
    @if(isset($final_data["concrete_grade_name"]))
    <div class="truck-number-text p-b10">{{ $final_data["concrete_grade_name"] }} - {{ isset($final_data["designMixDetails"]["product_name"]) ? $final_data["designMixDetails"]["product_name"] : 'Custom Mix' }}</div>
    @endif
    
    @if(isset($final_data["order_items"]))
    <div class="truck-number-text p-b10">{{ $final_data["order_items"]["concrete_grade_name"] }} - {{ isset($final_data["order_items"]["product_name"]) ? $final_data["order_items"]["product_name"] : 'Custom Mix' }}</div>
    @endif
    <div class="row p-b20">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="ordlst-id-date-block mb-3">
                <div class="ordlst-id-block">
                    Order Id : <span>#{{ $final_data["vendor_order_id"] }}</span>
                </div>
                <div class="ordlst-date-block">
                    Order Date : <span>{{ AdminController::dateTimeFormat($final_data["created_at"]) }}</span>
                </div>
                <div class="ordlst-date-block">
                    Delivered Qty : <span>{{ $final_data["pickup_quantity"] }} Cu.Mtr </span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="ordlst-id-date-block mb-3">
                <div class="ordlst-date-block">
                    @if(isset($final_data["driver_name"]))
                        Driver Name : <span>{{ $final_data["driver_name"] }}</span>
                    @endif
                    @if(isset($final_data["TM_driver1_name"]))
                        Driver Name : <span>{{ $final_data["TM_driver1_name"] }}</span>
                    @endif
                </div>
                <div class="ordlst-date-block">
                    @if(isset($final_data["driver_mobile_number"]))
                        Mobile No. : <span>{{ $final_data["driver_mobile_number"] }}</span>
                    @endif
                    @if(isset($final_data["TM_driver1_mobile_number"]))
                        Mobile No. : <span>{{ $final_data["TM_driver1_mobile_number"] }}</span>
                    @endif
                </div>
                <div class="ordlst-date-block">
                    TM No : <span>{{ isset($final_data["TM_rc_number"]) ? $final_data["TM_rc_number"] : '' }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="track-process-step1">
        <ol class="progress progress--medium">
            <?php
                $ordered_status_class = 'is-complete';
                $processed_status_class = '';
                $truck_assigned_status_class = '';
                $pickup_status_class = '';
                $delivered_status_class = '';

                $cancel_delivered_status = 'Delivered';

                if(isset($final_data["event_status"])){

                    if($final_data["event_status"] == 'TM_ASSIGNED' || $final_data["event_status"] == 'TM_ASSIGNED_FOR_PICKUP'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-active';
                        $delivered_status_class = 'progress__last';

                    }else if($final_data["event_status"] == 'PICKUP'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-active';

                    }else if($final_data["event_status"] == 'DELAYED'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-active';

                    }else if($final_data["event_status"] == 'DELAY'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-active';

                    }else if($final_data["event_status"] == 'DELIVERED'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-complete';

                    }else if($final_data["event_status"] == 'REJECTED'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-inactive';

                        $cancel_delivered_status = 'Rejected';

                    }else{

                        $ordered_status_class = 'progress__last';
                        $processed_status_class = 'progress__last';
                        $truck_assigned_status_class = 'progress__last';
                        $pickup_status_class = 'progress__last';
                        $delivered_status_class = 'progress__last';

                    }


                }else{

                    if($final_data["order_status"] == 'PROCESSING'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-active';
                        $truck_assigned_status_class = 'progress__last';
                        $pickup_status_class = 'progress__last';
                        $delivered_status_class = 'progress__last';

                    }else if($final_data["order_status"] == 'PROCESSED'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-active';
                        $pickup_status_class = 'progress__last';
                        $delivered_status_class = 'progress__last';

                    }else{

                        $ordered_status_class = 'progress__last';
                        $processed_status_class = 'progress__last';
                        $truck_assigned_status_class = 'progress__last';
                        $pickup_status_class = 'progress__last';
                        $delivered_status_class = 'progress__last';

                    }


                }

            ?>

            <li class="{{ $ordered_status_class }}" data-step="1">Order Received</li>
            <!-- <li class="is-complete" data-step="2">Processed</li> -->
            <li class="{{ $truck_assigned_status_class }}" data-step="2">TM Assigned</li>
            <li class="{{ $pickup_status_class }}" data-step="3">Pickup</li>
            <li class="{{ $delivered_status_class }}" data-step="4">{{ $cancel_delivered_status }}</li>
        </ol>
    </div>
    <div class="track-details-block">
        <table class="table">
            <thead>
                <tr>
                    <th>Date/Time (As Par Status)</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($final_data["created_at"]))
                    <tr>
                        <td>
                            <!-- {{ date('d M Y h:i:s a', strtotime($final_data["created_at"])) }} -->
                            {{ AdminController::dateTimeFormat($final_data["created_at"]) }}
                        </td>
                        <td>
                            You have received order.
                        </td>
                    </tr>
                @endif
                @if(isset($final_data["dispatched_at"]))
                    <tr>
                        <td>
                            <!-- {{ date('d M Y h:i:s a', strtotime($final_data["dispatched_at"])) }} -->
                            {{ AdminController::dateTimeFormat($final_data["dispatched_at"]) }}

                        </td>
                        <td>
                            This item has been dispatched.
                        </td>
                    </tr>
                @endif
                @if(isset($final_data["assigned_at"]))
                    <tr>
                        <td>
                            <!-- {{ date('d M Y h:i:s a', strtotime($final_data["assigned_at"])) }} -->
                            {{ AdminController::dateTimeFormat($final_data["assigned_at"]) }}
                        </td>
                        <td>
                            Transit Mixer has been assigned.
                        </td>
                    </tr>
                @endif
                @if(isset($final_data["pickedup_at"]))
                    <tr>
                        <td>
                            <!-- {{ date('d M Y h:i:s a', strtotime($final_data["pickedup_at"])) }} -->
                            {{ AdminController::dateTimeFormat($final_data["pickedup_at"]) }}
                        </td>
                        <td>
                            This Item has been picked up.
                        </td>
                    </tr>
                @endif
                @if(isset($final_data["delayed_at"]))
                    <tr>
                        <td>
                        <!-- {{ date('d M Y h:i:s a', strtotime($final_data["delayed_at"])) }} -->
                        {{ AdminController::dateTimeFormat($final_data["delayed_at"]) }}
                        </td>
                        <td style="color: #f00;">
                            Your Order has been delay {{ $final_data["delayTime"] }} due to {{ $final_data["reasonForDelay"] }}.
                        </td>
                    </tr>
                @endif
                @if(isset($final_data["delivered_at"]))
                <tr>
                    <td>
                        <!-- {{ date('d M Y h:i:s a', strtotime($final_data["delivered_at"])) }} -->
                        {{ AdminController::dateTimeFormat($final_data["delivered_at"]) }}
                    </td>
                    <td>
                        Your Order has been delivered.
                    </td>
                </tr>
                @endif
                @if(isset($final_data["rejected_at"]))
                <tr>
                    <td>
                        <!-- {{ date('d M Y h:i:s a', strtotime($final_data["delivered_at"])) }} -->
                        {{ AdminController::dateTimeFormat($final_data["rejected_at"]) }}
                    </td>
                    <td>
                        Your Order has been rejected due to {{$final_data["reasonForReject"]}}.
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
    @endif
</div>