@section('title', 'Supplier Surprise Orders')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">

    <h1 class="main-title">Surprise Order</h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Surprise Order</h2>
                    <a href="{{ route('surprise_orders_view') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    @if(isset($data['data']) && count($data['data']) > 0)
                        <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    @endif
                </div>
                <div class="orders-block">

                    <div class="orders-content-middle-block">
                        <div class="comn-table1 p-a0">
                            <table class="table">
                            <thead>
                                <tr>
                                    <th>Timer</th>
                                    <th>Order Id</th>
                                    <!-- <th>Category</th> -->
                                    <th>Plant Name</th>
                                    <th>Qty ( Cu.Mtr )</th>
                                    <!-- <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                    <th>GST Amount ( <i class="fa fa-rupee"></i> )</th> -->
                                    <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                    <th>Order Date & Time</th>
                                    <!-- <th>Bill Status</th> -->
                                    <th>Order Status</th>
                                    <th>Payment Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php //dd($data['data']); ?>
                            @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data['data']) && count($data['data']) > 0)
                            <?php //dd($data["data"]);
                                $second = 1000;
                                $minute = $second * 60;
                                $hour = $minute * 60;
                            ?>
                            <script>

                                const second = 1000,
                                minute = second * 60,
                                hour = minute * 60;

                            </script>
                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php
                                            $first_created_date = $value["created_at"];
                                            $count++;
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                    <?php $is_time_out = 0; ?>
                                        <tr>
                                        <td>
                                                @if(isset($value['max_accepted_at']) && ($value["order_status"] == 'RECEIVED'))
                                                <?php
                                                    $current_time = time();
                                                    $max_quote_time = strtotime($value['max_accepted_at']);
                                                    // echo (date('d M Y, h:i:s',$current_time));
                                                    // echo (date('d M Y, h:i:s',strtotime($value['max_accepted_at'])));

                                                    // $date1 = new DateTime();
                                                    // $date2 = new DateTime($value['max_accepted_at']);

                                                    // $diff = $date2->diff($date1);

                                                    // Call the format method on the DateInterval-object
                                                    // echo $diff->format('%a Day and %h hours %i minutes');

                                                    $final_diff = $max_quote_time - $current_time;
                                                    // dd($max_quote_time.'...'.$current_time.'...'.$final_diff);
                                                    $final_hour = round($final_diff / ($hour));
                                                    $final_min = round(($final_diff % ($hour)) / ($minute));
                                                    $final_sec = round(($final_diff % ($minute)) / $second);



                                                    $final_hour = $final_hour > 0 ? $final_hour : 0;
                                                    $final_min = $final_min > 0 ? $final_min : 0;
                                                    $final_sec = $final_sec > 0 ? $final_sec : 0;

                                                    // $final_hour = $diff->format('%h');
                                                    // $final_min = $diff->format('%i');
                                                    // $final_sec = $diff->format('%s');

                                                    // echo "| ".$final_hour;
                                                    // echo " | ".$final_min;
                                                    // echo " | ".$final_sec;

                                                ?>

                                                    <div class="custom_timer" id="timer_div_{{$value['_id']}}">
                                                        
                                                        @if($final_hour > 0 || $final_min > 0 || $final_sec > 0)
                                                            <input type="hidden" id="timer_hid_{{$value['_id']}}" value="{{ date('d M, Y H:i:s', strtotime($value['max_accepted_at'])) }}" />
                                                            <ul>
                                                                <li><span id="hours_{{$value['_id']}}"></span></li>
                                                                <li><span id="minutes__{{$value['_id']}}"></span></li>
                                                                <li><span id="seconds__{{$value['_id']}}"></span></li>
                                                            </ul>
                                                        @else
                                                            <span class="text-danger">TimeOut</span>
                                                            <?php $is_time_out = 1; ?>
                                                        @endif
                                                    </div>
                                                    <script>

                                                        <?php //$time =   strtotime($value['max_accepted_at']) ?>

                                                        x = setInterval(function() {
                                                            var time = $("#timer_hid_{{$value['_id']}}").val();
                                                            let countDown = new Date(time).getTime();
                                                            // console.log("countDown..."+countDown);
                                                            let current_date = new Date();
                                                            let now = current_date.getTime();
                                                            console.log("time..."+time+"..."+current_date);
                                                            distance = countDown - now;

                                                            var diff_as_date = new Date(distance);
                                                            var getHours = diff_as_date.getHours();  // hours
                                                            var getMinutes = diff_as_date.getMinutes(); // minutes
                                                            var getSeconds = diff_as_date.getSeconds(); // seconds

                                                            console.log("getHours..."+getHours+"...getMinutes..."+getMinutes+"...getSeconds..."+getSeconds);

                                                            // var final_hour = Math.round(distance  / (hour));
                                                            // var final_min = Math.round((distance % (hour)) / (minute));
                                                            // var final_sec = Math.round((distance % (minute)) / second);

                                                            // final_hour = final_hour > 0 ? final_hour : 0;
                                                            // final_min = final_min > 0 ? final_min : 0;
                                                            // final_sec = final_sec > 0 ? final_sec : 0;

                                                            final_hour = getHours;
                                                            final_min = getMinutes;
                                                            final_sec = getSeconds;


                                                            if(final_hour > 0 || final_min > 0 || final_sec > 0){
                                                                document.getElementById("hours_{{$value['_id']}}").innerText = final_hour,
                                                                document.getElementById("minutes__{{$value['_id']}}").innerText = final_min,
                                                                document.getElementById("seconds__{{$value['_id']}}").innerText = final_sec;
                                                            }else{
                                                                $("#timer_div_{{$value['_id']}}").html('<span class="text-danger">TimeOut</span>');
                                                                $("#action_btn_{{$value['_id']}}").html('<span class="text-danger">No Action</span>');
                                                            }

                                                        }, second)
                                                    </script>
                                                @else
                                                    <span class="text-danger">TimeOut</span>
                                                    <?php $is_time_out = 1; ?>
                                                @endif
                                            </td>
                                            <td>
                                                @if(($is_time_out == 1) && ($value["order_status"] == 'RECEIVED' || $value["order_status"] == 'REJECTED'))
                                                    #{{ isset($value["_id"]) ? $value["_id"] : '' }}
                                                @else
                                                    <a href="{{ route('supplier_surprise_orders_details',$value['_id']) }}">#{{ isset($value["_id"]) ? $value["_id"] : '' }}</a>
                                                @endif
                                            </td>
                                            <!-- <td>{{ isset($value["product_category"]["category_name"]) ? $value["product_category"]["category_name"] : '' }}</td> -->
                                            <td>{{ isset($value["business_name"]) ? $value["business_name"] : '' }}</td>
                                            <td>{{ isset($value["quantity"]) ? round($value["quantity"]) : '0' }}</td>
                                            <!-- <td>{{ isset($value["unit_price"]) ? number_format($value["unit_price"],2) : '0' }}</td>
                                            <td>{{ isset($value["gst_price"]) ? number_format($value["gst_price"],2) : '0' }}</td> -->
                                            <td>{{ isset($value["total_amount_without_margin"]) ? number_format($value["total_amount_without_margin"],2) : '0' }}</td>
                                            <!-- <td>{{ isset($value["buyer"]["full_name"]) ? $value["buyer"]["full_name"] : '' }}</td> -->
                                            <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                            <!-- <td><span class="badge bg-red">Rejected</span></td> -->
                                            @if(isset($value["order_status"]))
                                                @if($value["order_status"] == 'DELIVERED' || $value["order_status"] == 'ACCEPTED')
                                                    <td><div class="badge bg-green">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'CANCELLED' || $value["order_status"] == 'REJECTED')
                                                    <td><div class="badge bg-red">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'RECEIVED')
                                                    <td><div class="badge bg-blue">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'PICKUP')
                                                    <td><div class="badge bg-purple">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>

                                                    @else
                                                        <td><div class="badge bg-yellow">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                    @endif
                                                @endif
                                                @endif
                                                @endif
                                            @else
                                                <td>-</td>
                                            @endif
                                            <td><div class="badge {{$value['payment_status'] == 'UNPAID' ? 'bg-red' : 'bg-green '}}">{{ isset($value["payment_status"]) ? $value["payment_status"] : '' }}</div></td>
                                            <!-- <td>
                                                <div class="badge bg-yellow">
                                                    {{ isset($value["delivery_status"]) ? $value["delivery_status"] : '' }}
                                                </div>
                                            </td> -->
                                            <td id="action_btn_{{$value['_id']}}">
                                                @if($value["order_status"] == 'RECEIVED')
                                                    @if($value["is_accepted_by_others"] == 1)
                                                        <?php 
                                                        
                                                            $is_time_out = 1;
                                                        ?>
                                                    @endif
                                                    @if($is_time_out == 0)
                                                        <button class="site-button green button-sm" type="button">
                                                            <span class="dropdown-label" onclick="confirmPopup(3,'{{ json_encode($value) }}','{{ csrf_token() }}','Are you sure you want to accept this order?')">Accept</span>

                                                        </button>
                                                        <button class="site-button red button-sm" type="button">
                                                            <span class="dropdown-label" onclick="confirmPopup(4,'{{ json_encode($value) }}','{{ csrf_token() }}','Are you sure you want to reject this order?')">Reject</span>

                                                        </button>
                                                    @else
                                                        @if($value["is_accepted_by_others"] == 1)
                                                            <span class="text-danger">Accepted By Others</span>
                                                        @else
                                                            <span class="text-danger">No Action</span>
                                                        @endif
                                                    @endif
                                                @else
                                                    @if($value["is_accepted_by_others"] == 1)
                                                        <span class="text-danger">Accepted By Others</span>
                                                    @else
                                                        -
                                                    @endif
                                                @endif
                                                <div class="dropdown cstm-dropdown-select">

                                                    <!-- <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('order_status','{{ json_encode($value) }}')">Change Delivery Status</a></li>
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('payment_status','{{ json_encode($value) }}')">Change Payment Status</a></li>
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('cancel_status','{{ json_encode($value) }}')">Cancel</a></li>
                                                    </ul> -->
                                                </div>
                                            </td>
                                        </tr>


                                            <!-- <td>
                                                <div class="dropdown cstm-dropdown-select">
                                                    <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('order_status','{{ json_encode($value) }}')">Change Delivery Status</a></li>
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('payment_status','{{ json_encode($value) }}')">Change Payment Status</a></li>
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('cancel_status','{{ json_encode($value) }}')">Cancel</a></li>
                                                    </ul>
                                                </div>
                                            </td> -->
                                        </tr>
                                @endforeach

                            @else
                                <tr>
                                     <td colspan="9" style="text-align: center;">No Record Found</td>
                                </tr>
                            @endif

                            </tbody>
                            </table>
                        </div>
                    </div>
                    <p class="small">
                        <i>Note: You will receive a Surprise Order when other RMC Supplier could not accept the Order. Better if you can make the most out of such Surprise Orders</i>
                    </p>
                </div>

                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="pagination-block m-t20">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                    <div class="pagination justify-content-end m-0">
                    @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{ route('surprise_orders_view') }}" method="get">

                            @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="payment_status"/>
                                @endif
                                @if(Request::get('address_id'))
                                    <input type="hidden" value="{{ Request::get('address_id') ? Request::get('address_id') : '' }}" name="address_id"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                 @if(Request::get('order_id'))
                                    <input type="hidden" value="{{ Request::get('order_id') ? Request::get('order_id') : '' }}" name="order_id"/>
                                @endif
                                @if(Request::get('buyer_id'))
                                    <input type="hidden" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : '' }}" name="buyer_id"/>
                                @endif
                                @if(Request::get('buyer_name'))
                                    <input type="hidden" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : '' }}" name="buyer_name"/>
                                @endif
                                @if(Request::get('order_year_filter'))
                                    <input type="hidden" value="{{ Request::get('order_year_filter') ? Request::get('order_year_filter') : '' }}" name="order_year_filter"/>
                                @endif
                                @if(Request::get('order_month_filter'))
                                    <input type="hidden" value="{{ Request::get('order_month_filter') ? Request::get('order_month_filter') : '' }}" name="order_month_filter"/>
                                @endif
                                @if(Request::get('order_date_filter'))
                                    <input type="hidden" value="{{ Request::get('order_date_filter') ? Request::get('order_date_filter') : '' }}" name="order_date_filter"/>
                                @endif

                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{ route('surprise_orders_view') }}" method="get">


                            @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="payment_status"/>
                                @endif
                                @if(Request::get('address_id'))
                                    <input type="hidden" value="{{ Request::get('address_id') ? Request::get('address_id') : '' }}" name="address_id"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                 @if(Request::get('order_id'))
                                    <input type="hidden" value="{{ Request::get('order_id') ? Request::get('order_id') : '' }}" name="order_id"/>
                                @endif
                                @if(Request::get('buyer_id'))
                                    <input type="hidden" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : '' }}" name="buyer_id"/>
                                @endif
                                @if(Request::get('buyer_name'))
                                    <input type="hidden" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : '' }}" name="buyer_name"/>
                                @endif
                                @if(Request::get('order_year_filter'))
                                    <input type="hidden" value="{{ Request::get('order_year_filter') ? Request::get('order_year_filter') : '' }}" name="order_year_filter"/>
                                @endif
                                @if(Request::get('order_month_filter'))
                                    <input type="hidden" value="{{ Request::get('order_month_filter') ? Request::get('order_month_filter') : '' }}" name="order_month_filter"/>
                                @endif
                                @if(Request::get('order_date_filter'))
                                    <input type="hidden" value="{{ Request::get('order_date_filter') ? Request::get('order_date_filter') : '' }}" name="order_date_filter"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button m-l10">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>

                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>

@endsection