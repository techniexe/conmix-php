@section('title', 'Supplier RMC Media')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Design Mix Media</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="bank-detail-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="bank-detail-block add-bank-detail-block">
                            <a href="javascript:;" onclick="resetForm('add_primary_media_detail_form','add-primary-media-details')" data-toggle="modal" data-target="#add-primary-media-details"><span>Add Primary Image</span><i>+</i></a>
                            
                        </div>
                    </li>
                    @if(isset($data["data"]))
                    @php
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                            @if($value["type"] == "PRIMARY")
                                <li>
                                    <div class="bank-details-select">
                                        
                                        
                                            <div class="make-default-btn site-button green">{{ $value["type"] }} Image</div>
                                        
                                        
                                        
                                        <div class="bank-detail-block p-a15">
                                            <div class="top-right-edit-link">
                                                <a href="javascript:;" onclick="deleteMedia('{{ $value["_id"] }}')" class="site-button green button-sm" data-toggle="modal" data-target="#add-product-list"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                            
                                            <p>
                                                <a href="{{ isset($value["media_url"]) ? $value["media_url"] : '' }}" class="gal_link">
                                                    <span class="chq-img-block"><img src="{{ isset($value["media_url"]) ? $value["media_url"] : '' }}" alt="" /></span>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    @endif
                    

                    
                </ul>
            </div>
            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="bank-detail-block add-bank-detail-block">
                        <a href="javascript:;" onclick="resetForm('add_secondory_media_detail_form','add-secondory-media-details')" data-toggle="modal" data-target="#add-secondory-media-details"><span>Add Secondory Image</span><i>+</i></a>
                            
                        </div>
                    </li>
                    @if(isset($data["data"]))
                    @php
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                            @if($value["type"] != "PRIMARY")
                                <li>
                                    <div class="bank-details-select">
                                        
                                        
                                            <!-- <div class="make-default-btn site-button green">{{ $value["type"] }} Image</div> -->
                                        
                                        
                                        
                                        <div class="bank-detail-block p-a15">
                                            <div class="top-right-edit-link">
                                                <a href="javascript:;" onclick="deleteMedia('{{ $value["_id"] }}')" class="site-button green button-sm" data-toggle="modal" data-target="#add-product-list"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                            
                                            <p>
                                                <a href="{{ isset($value["media_url"]) ? $value["media_url"] : '' }}" class="gal_link">
                                                    <span class="chq-img-block"><img src="{{ isset($value["media_url"]) ? $value["media_url"] : '' }}" alt="" /></span>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    @endif
                    

                    
                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection