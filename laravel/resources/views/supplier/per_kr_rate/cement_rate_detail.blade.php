@section('title', 'Supplier Cement Rate')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">

    <h1 class="breadcrums"><a href="#">Design Mix Contain Rate</a> / <span>Cement Per KG Rate (Excl. GST)</span></h1>

    <div class="clearfix"></div>
    
    <div class="review-complaints-block wht-tble-bg m-b30">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Cement Per KG Rate List 
                        (Edit to enter or update rate of your cement brand and turn the status to 'Active')
                    </h2>
                    <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                    <button class="site-button pull-right m-r10" onclick="resetForm('add_cement_rate_form','add-cement-rate-details')" data-toggle="modal" data-target="#add-cement-rate-details">Add Cement Rate</button>
                    <a href="{{ route('cement_rate') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    @if(isset($data['data']) && count($data['data']) > 0)
                        <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    @endif
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 5px;margin-bottom: 15px;background: #e8f1fb;border: 1px solid #dce9f8;">
                    <h2 class="sub-title">Select Cement Brand To Filter</h2>
                    @if(isset($data["cement_brand_data"]))
                        @foreach($data["cement_brand_data"] as $value)
                        <div class="col-md-3" style="float: left;margin-bottom: 15px;">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input cement_brand_checkbox" type="checkbox" id="" value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'checked' : '' }}>
                                <input type="hidden" value="" id="" />
                                <label class="form-check-label">
                                    {{ $value['name'] }}
                                </label>
                            </div>
                        </div>

                        @endforeach
                    @endif

                </div>

                <div class="support-ticket-list-block">
                    <div class="comn-table1 pull-left w-100 m-b20">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Cement Brand</th>
                                    <th>Cement Grade</th>
                                    <th>Plant Address</th>
                                    <th>Cement Per KG Rate (KG) (Excl. GST)</th>
                                    <th>Created On</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data['data']) && count($data['data']) > 0)
                                @php
                            //dd($data['data']);
                                    $total_record = count($data["data"]);
                                @endphp
                                @foreach($data["data"] as $value)  
                                    @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                <tr>
                                    
                                    <td>{{ isset($value["cement_brand"]["name"]) ? $value["cement_brand"]["name"] : '' }}</td>
                                    <td>{{ isset($value["cement_grade"]["name"]) ? $value["cement_grade"]["name"] : '' }}</td>
                                    
                                    <td>{{ isset($value["address"]["business_name"]) ? $value["address"]["business_name"] : '' }} - {{ isset($value["address"]["city_name"]) ? $value["address"]["city_name"] : '' }}, {{ isset($value["address"]["state_name"]) ? $value["address"]["state_name"] : '' }}</td>
                                    <td>{{ isset($value["per_kg_rate"]) ? $value["per_kg_rate"] : '' }}</td>
                                    <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                    <td>
                                        <div id="active_inactive_{{ $value['_id'] }}" class="badge {{ $value['is_active'] ? 'bg-green' : 'bg-red'}} ">
                                            {{ $value['is_active'] ? 'Active' : 'InActive'}}
                                        </div>
                                    </td>
                                    <td class="text-nowrap">
                                        <a href="javascript:;" onclick="showCementRateEditDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-edit"></i></a> 
                                        <!-- <a href="javascript:;" onclick="deleteCementRate('{{ $value['_id'] }}')" class="site-button red button-sm" data-tooltip="Delete"><i class="fa fa-trash-o"></i></a> -->
                                        @if($value["is_active"] == 0)
                                            <a href="javascript:;" onclick="showCementRateEditDialog('{{ json_encode($value) }}')" class="site-button button-sm red" data-tooltip="Active"><i class="fa fa-toggle-off"></i></a>
                                        @endif

                                        @if($value["is_active"] == 1)
                                            <a href="javascript:;" onclick="confirmPopup(13,'{{ json_encode($value) }}','','Are you sure you want inctive?')" class="site-button button-sm green" data-tooltip="InActive"><i class="fa fa-toggle-on"></i></a>
                                        @endif
                                    </td>
                                    
                                </tr>
                                @endforeach

                            @else
                                <tr>
                                      <td colspan="7" style="text-align: center;">No Record Found</td>
                                </tr>
                            @endif
                                
                            </tbody>
                        </table>
                    </div>

                    <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                    <div class="data-box-footer clearfix">
                        <div class="pagination-block">
                        <div><b>Note:</b> 
                        <i>The above entered rates per kg will be considered while deriving your standard & custom RMC supply list and accordingly it's price combinations will be reflected in the conmix RMC supply list.
                            
                        </i></div>
                            <!-- <ul class="pagination justify-content-end m-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            </ul>
                            <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                            <div class="pagination justify-content-end m-0">
                                @if($is_previous_avail == 1)
                                    @if(isset($first_created_date))
                                    <form action="{{route(Route::current()->getName())}}" method="get">
                                        
                                        @if(Request::get('address_id'))
                                            <input type="hidden" name="address_id" value="{{ Request::get('address_id') ? Request::get('address_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('brand_id'))
                                            <input type="hidden" name="brand_id" value="{{ Request::get('brand_id') ? Request::get('brand_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('grade_id'))
                                            <input type="hidden" name="grade_id" value="{{ Request::get('grade_id') ? Request::get('grade_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('per_kg_rate_value'))
                                            <input type="hidden" name="per_kg_rate_value" value="{{ Request::get('per_kg_rate_value') ? Request::get('per_kg_rate_value') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('per_kg_rate'))
                                            <input type="hidden" name="per_kg_rate" value="{{ Request::get('per_kg_rate') ? Request::get('per_kg_rate') : ''  }}" class="form-control" >
                                        @endif
                                        
                                        
                                        <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                        <button type="submit" class="site-button">Previous</button>                                    
                                    </form>
                                    @endif
                                    @endif

                                    @if($is_next_avail == 1)
                                    @if(isset($last_created_date))
                                    <form action="{{route(Route::current()->getName())}}" method="get">
                                        
                                        
                                        @if(Request::get('address_id'))
                                            <input type="hidden" name="address_id" value="{{ Request::get('address_id') ? Request::get('address_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('brand_id'))
                                            <input type="hidden" name="brand_id" value="{{ Request::get('brand_id') ? Request::get('brand_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('grade_id'))
                                            <input type="hidden" name="grade_id" value="{{ Request::get('grade_id') ? Request::get('grade_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('per_kg_rate_value'))
                                            <input type="hidden" name="per_kg_rate_value" value="{{ Request::get('per_kg_rate_value') ? Request::get('per_kg_rate_value') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('per_kg_rate'))
                                            <input type="hidden" name="per_kg_rate" value="{{ Request::get('per_kg_rate') ? Request::get('per_kg_rate') : ''  }}" class="form-control" >
                                        @endif
                                    
                                        
                                        <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                        <button type="submit" class="site-button">Next</button>
                                    </form>
                                    @endif
                                    @endif
                               
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
  <!--   <div class="customer-care-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="customer-care-content-block">
                    <div class="customer-care-left-block">
                        <img src="{{asset('assets/supplier/images/customer-service.png')}}">
                        
                        <h3>Call Us On: <a href="#">+91 98989 14789</a></h3>
                        <h4>Write Us On: <a href="#">info@example.com</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div>

@endsection