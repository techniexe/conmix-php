@section('title', 'Partner Account Sign Up Step 1')
@extends('supplier.layouts.supplier_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block register_addres_stap">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Partner Account - <span style="color:#687ae8;"> Conmix </span></h1>
                <div id="signupregister" style="display: block;">
                @if(isset($data["error"]))
                    <div class="error alert alert-danger">{{ str_replace("_"," ",$data["error"]["message"]) }}</div>
                @endif
                @if(isset($data["message"]))
                        <div class="alert alert-success">{{ $data["message"] }}</div>
                    @endif
                    <h2>Registration</h2>
                    <form action="" name="supplier_register_form" id="supplier_register_form" method="post" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <input type="hidden" name="signup_type" value="{{ $data['data']['signup_type'] }}" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Company Type <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="company_type" id="signup_company_type">
                                                <option disabled="disabled" selected="selected">Select Type</option>
                                                <option value="Partnership">Partnership</option>
                                                <option value="Proprietor">Proprietor</option>
                                                <option value="LLP">LLP</option>
                                                <option value="LTD">LTD.</option>
                                                <option value="PVT LTD">PVT LTD</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Person Name <span class="str">*</span></label>
                                        <input class="form-control text-capitalize" name="full_name" placeholder="e.g. Johndoe" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Company Full Name <span class="str">*</span></label>
                                        <input class="form-control text-capitalize" name="company_name" placeholder="e.g. Ambika Plant" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Mobile No. <span class="str">*</span></label>
                                        <input id="" class="form-control" name="mobile_number" type="text" placeholder="e.g. 9898989898" readonly value="{{ $data['data']['email_mobile'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Office Tel. (with STD) </label>
                                        <input type="text" class="form-control" name="landline_number" placeholder="e.g. 0791234567">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Email <span class="str">*</span></label>
                                        <input type="text" autocomplete="off" class="form-control" autocomplete="off" autofill="off" id="supplier_email" name="email" placeholder="e.g. johndoe@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Company Registration No <span class="str">*</span></label>
                                        <input class="form-control text-uppercase" name="company_certification_number" placeholder="e.g. CMP123456789" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group" id="company_certification_image_div">
                                        <label>Company Register Certification <span class="str">*</span></label>
                                        <div class="file-browse">
                                            <span class="button-browse" >
                                                Browse <input type="file" id="company_certification_image" name="company_certification_image" />
                                            </span>
                                            <input type="text" id="company_certification_image_text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>PAN No <span class="str">*</span></label>
                                        <input class="form-control text-uppercase" name="pan_number" placeholder="e.g. AAABB0000C" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group" id="pancard_image_div">
                                        <label>Upload Pancard <span class="str">*</span></label>
                                        <div class="file-browse" >
                                            <span class="button-browse">
                                                Browse <input type="file" id="pancard_image" name="pancard_image" />
                                            </span>
                                            <input type="text" id="pancard_image_text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>GST No <span class="str">*</span></label>
                                        <input class="form-control" name="gst_number" placeholder="e.g. 00AABBC0000A1BB" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group" id="gst_certification_image_div">
                                                <label>Upload GST Certificate<span class="str">*</span></label>
                                                <div class="file-browse">
                                                    <span class="button-browse">
                                                        Browse <input type="file" id="gst_certification_image" name="gst_certification_image"/>
                                                    </span>
                                                    <input type="text" id="gst_certification_image_text" class="form-control browse-input"  placeholder="e.g. pdf, jpg" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <div id="selectregion" class="input-group">
                                        <label>Region Serv <span class="str">*</span></label>
                                        <div class="input-group single-search-select2" >
                                            <select id="region_served_text" name="region_served[]" id="multiple" class="form-control-chosen form-control" data-placeholder="Select Region" multiple >
                                                <option></option>
                                                @if(isset($state_data["data"]))
                                                    @foreach($state_data["data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Password <span class="str">*</span></label>
                                        <input class="form-control" name="password" placeholder="e.g. Enter Password" type="password">
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Plant capacity per hour (Cu.Mtr) <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="plant_capacity_per_hour" id="plant_capacity_per_hour">
                                                <option disabled="disabled" selected="selected">Select Plant Capacity</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                                <option value="60">60</option>
                                                <option value="75">75</option>
                                                <option value="120">120</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>No. of plants <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="no_of_plants" id="no_of_plants">
                                                <option disabled="disabled" selected="selected">Select No. of plants</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>No. of operation hours <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="no_of_operation_hour" id="no_of_operation_hour">
                                                <option disabled="disabled" selected="selected">Select No. of operation hours</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label class="text-success" id="plant_capacity">Your Total plant capacity is 0 Cu. Mtr./day</label>
                                    </div>
                                </div>
                            </div>-->

                            <div class="col-md-12">
                                <div class="form-group" >
                                    <div class="pretty_checkbox check_blue m-0" id="terms_condition_div">
                                        <input type="checkbox" name="terms_condition" id="terms_condition">
                                        <label class="form-check-label" for="terms_condition"> I've read and accept the
                                            <a href="{{ route('supplier_terms_of_use') }}">Terms of Usage</a>, <a href="{{ route('supplier_privacy_policy') }}">Privacy Policy</a> and <a href="{{ route('supplier_return_policy') }}">Cancellations, Returns, and Refund Policy</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group m-t20">
                                    <div class="input-group text-left">
                                        <button type="submit" name="register_btn" value="register_btn" class="site-button">Register</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="overlay" style="display:none">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>
@endsection
<!-- MODAL Email Varification START-->
<div id="Email_very_otp" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Email Varification</h4>
            </div>
            <form name="email_verify_popup">
                @csrf
                <div class="modal-body p-tb20 p-lr15">
                    <div class="vehicle-block vehicle-category-block">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <h3>Enter Confirmation Code</h3>
                                    <div class="text-gray-dark">Enter the 6 digits OTP sent on your Email ID <span id="supplier_register_email_id"></span> </div>
                                    
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>OTP <span class="str">*</span></label>
                                        <input type="text" name="email_otp" id="email_otp" class="form-control" placeholder="One Time Password">
                                    </div>
                                </div>
                                <div class="text-gray-dark resend_otp_div text-left">
                                    <!-- <form action=""  name="" method="">
                                        @csrf -->
                                        <input class="form-control" name="" value="" required  type="hidden">
                                        <!-- <button type="submit" name="register_btn" value="register_btn" class="text-primary">Resend OTP</button> -->
                                        <div class="timer_otp mb-3">
                                            <span id="timer_forgot_pass">
                                                <span id="time_forgot_pass">02:00</span>
                                            </span>
                                        </div>
                                    <!-- </form> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer text-left">
                    <button type="submit" name="register_btn" value="register_btn" class="site-button m-r20">Register</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL Email Varification END-->

