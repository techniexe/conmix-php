@section('title', 'Partner Account Sign Up Step 2')
@extends('supplier.layouts.supplier_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Partner Account - <span style="color:#687ae8;"> Conmix </span></h1>
                <div id="signupemailverifyotp" style="display: block;">
                @if(isset($data["error"]))
                    <div class="error alert alert-danger">{{ str_replace("_"," ",$data["error"]["message"]) }}</div>
                @endif

                        <div class="form-group">
                            <div class="input-group">
                                <h3>Enter Confirmation Code</h3>
                                <span class="text-gray-dark">
                                @if(isset($data["data"]))
                                    <div class="">{{ isset($data["data"]["message"]) ? $data["data"]["message"] : '' }}</div>
                                @endif
                                <a href="{{ route('signup_email_verifier') }}" class="text-primary">Change Mobile No</a></span>
                            </div>
                        </div>
                        <form action="{{route('signup_otp_verify')}}" name="supplier_otp_verify_form" method="post" onsubmit="return validateForm()" autocomplete="off">
                            @csrf
                            <div class="form-group">
                                <div class="input-group">
                                    <label>OTP <span class="str">*</span></label>
                                    <input class="form-control" id="otp_code" name="otp_code" placeholder="One Time Password" type="text">
                                    <!-- <div class="invalid-feedback" id="otp_error"></div> -->
                                    <input class="form-control" name="email_mobile" value="{{ $data['data']['email_mobile'] }}" placeholder="One Time Password" type="hidden">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group text-left">
                                    <button type="submit" name="otp_btn" value="otp_btn" class="site-button" data-loading-text="<span class='fa fa-spinner fa-spin '></span> Processing">Continue</a>
                                </div>
                            </div>
                        </form>

                        <div class="text-gray-dark resend_otp_div">
                            <!-- <button type="submit" name="otp_send_btn" value="otp_send_btn" class="text-primary">Resend</button> -->

                            <form action="{{route('signup_otp_send')}}"  name="supplier_email_verify_form" method="post">
                                @csrf

                                <input class="form-control" name="email_mobile" value="{{ $data['data']['email_mobile'] }}" required  type="hidden">

                                
                                <div class="timer_otp mb-3">
                                    <span id="timer_forgot_pass">
                                        <span id="time_forgot_pass">02:00</span>
                                    </span>
                                </div>
                            </form>
                        </div>

                        <!-- <div class="form-group pull-right w-100">
                            <div class="input-group display-block">

                                <form action="{{route('signup_otp_send')}}"  name="supplier_email_verify_form" method="post">
                                    @csrf

                                    <input class="form-control" name="email_mobile" value="{{ $data['data']['email_mobile'] }}" required  type="hidden">

                                    <button type="submit" name="register_btn" value="register_btn" class="pull-right">Resend</button>
                                </form>
                            </div>
                        </div>                         -->

                </div>
            </div>
        </div>
    </div>
</div>

<script>
function validateForm() {

    $(".error").html();
    $(".error").removeClass("alert-danger");

    $("#otp_error").html("");

  var otp = document.forms["supplier_otp_verify_form"]["otp_code"].value.trim();

  var is_valid = validateOTP(otp);

    return is_valid;



}

// var counter = 60;
// var interval = setInterval(function() {
//     counter--;
//     // Display 'counter' wherever you want to display it.
//     if (counter <= 0) {
//             clearInterval(interval);

//         $('#timer_forgot_pass').html('<button type="submit" name="register_btn" value="register_btn" class="text-primary">Resend OTP</button>');
//         return;
//     }else{
//         $('#time_forgot_pass').text(counter);
//     console.log("Timer --> " + counter);
//     }
// }, 1000);

window.onload = function () {
    var fiveMinutes = 60 * 2,
        // display = document.querySelector('#time_forgot_pass');
        display = $('#time_forgot_pass');
    startTimer(fiveMinutes, display, "supplier_register");
};

$('button').on('click', function() {
    var $this = $(this);
  $this.button('loading');
//     setTimeout(function() {
//        $this.button('reset');
//    }, 8000);
});

</script>

@endsection