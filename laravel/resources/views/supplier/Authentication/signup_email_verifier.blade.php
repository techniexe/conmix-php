@section('title', 'Partner Account Sign Up Step 3')
@extends('supplier.layouts.supplier_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Partner Account - <span style="color:#687ae8;"> Conmix </span></h1>
                <div id="signupemailverify" style="display: block;">
                <h2>Registration</h2> 
                @if(isset($data["error"]))
                    <div class="error alert alert-danger">{{ str_replace("_"," ",$data["error"]["message"]) }}</div>
                @endif
                <!-- onsubmit="return validateForm()" -->
                    <form action="{{route('signup_otp_send')}}" name="supplier_email_verify_form" method="post" name="login_form" autocomplete="off" >
                        @csrf
                        <div class="form-group m-b40">
                            <div class="input-group">
                                <label>Mobile No. <span class="str">*</span></label>
                                <input class="form-control"  id="mobile_or_email" name="email_mobile" placeholder="e.g. 9898989898" type="text">
                                <div class="invalid-feedback" id="email_mobile_error"></div>
                            </div>
                            <span class="msg-text text-green display-block m-t15" style="display: none;">We have sent Verification to your email is johndoe@example.com</span>
                        </div>
                        <div class="form-group">
                            <div class="input-group text-left">
                                <div class="lsf-btn-block">
                                    <button type="submit" name="register_btn" value="register_btn" class="site-button m-b10" data-loading-text="<span class='fa fa-spinner fa-spin '></span> Processing">Register</button>
                                    <!-- <p class="m-b10 text-uppercase text-gray-dark fw-medium">Or</p>
                                    <button type="button" class="site-button google m-b0 m-r20"><i class="fa fa-google-plus"></i> Google</button>
                                    <button type="button" class="site-button linkedin m-b0"><i class="fa fa-linkedin"></i> Linkedin</button> -->
                                </div>
                            </div>
                        </div>
                        <span class="text-gray-dark display-block">Already have an account? <a href="{{ route('supplier') }}" class="text-primary">Log In</a></span>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function validateForm() {
    
    $(".error").html();
    $(".error").removeClass("alert-danger");

    $("#email_mobile_error").html("");
  
  var email_mobile = document.forms["supplier_email_verify_form"]["email_mobile"].value.trim();
  
  var is_valid = validateMobileEmail(email_mobile,'');

    return is_valid;

//   if(isNaN(email_mobile)){
//     //validate email address
//     var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//     if(!email_mobile.match(mailformat)){
//         $("#email_mobile_error").html("Please enter valid email address");
//         return false;
//     }
//   }else{
//     //validate Mobile number
//     email_mobile = email_mobile.trim().replace("+","");
//     var phoneno = /^\d{12}$/;
//     if (!email_mobile.match(phoneno)) {
//         // alert("Phone number should be 10 digits");
//         $("#email_mobile_error").html("Phone number should be 13 digits. including + sign");
//         return false;
//     }


//   }
  
} 

$('button').on('click', function() {
    var $this = $(this);
  $this.button('loading');
//     setTimeout(function() {
//        $this.button('reset');
//    }, 8000);
});

</script>

@endsection