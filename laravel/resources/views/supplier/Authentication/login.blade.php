@section('title', 'Partner Account Login')
@extends('supplier.layouts.supplier_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Partner Account - <span style="color:#687ae8;"> Conmix </span></h1>
                <div id="login" style="display: block;">
                <h2>Log In</h2>
                @if(isset($data["error"]))
                    <div class="error alert alert-danger">{{ str_replace("_"," ",$data["error"]["message"])  }}</div>
                @endif
                @if(isset($data["message"]))
                    <div class="alert alert-success">{{ $data["message"] }}</div>
                @endif
                <!-- onsubmit="return validateForm()" -->
                
                    <form action="{{route('supplier_login')}}" name="supplier_login_form" method="post" autocomplete="off" >
                        
                    @csrf
                    
                        <div class="form-group">
                            <div class="input-group">
                                <label>Mobile No. / Email Id <span class="str">*</span></label>
                                <input class="form-control" value="" id="mobile_or_email" name="email_mobile" placeholder="e.g. 9898989898 / johndoe@example.com" type="text" >
                                <!-- <p class="error" id="email_mobile_error"></p> -->
                                <!-- <div class="invalid-feedback" id="email_mobile_error"></div> -->
                                @error('email_mobile')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label>Password <span class="str">*</span></label>
                                <input class="form-control" id="pass" name="password" placeholder="e.g. Enter Password" type="password">
                                <div class="invalid-feedback" id="password_error"></div>
                                @error('password')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group pull-left w-100">
                            <div class="input-group display-block">
                                <a href="{{ route('forgot_pass_email_verifier') }}" class="pull-right">Forgot Password</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group text-left">
                                <div class="lsf-btn-block">
                                    <button type="submit" name="login_btn" value="login_btn" class="site-button m-b10">Log In</button>
                                    <!-- <p class="m-b10 text-uppercase text-gray-dark fw-medium m-b0">Or</p>
                                    <button type="button" class="site-button google m-b0 m-r20"><i class="fa fa-google-plus"></i> Google</button>
                                    <button type="button" class="site-button linkedin m-b0"><i class="fa fa-facebook"></i> Facebook</button> -->
                                </div>
                            </div>
                        </div>
                        <span class="text-gray-dark">Don't have an account?<a href="{{ route('signup_email_verifier') }}" class="text-primary"> Register
</a> </span>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function validateForm() {
    
    $(".error").html("");
    $(".error").removeClass("alert-danger");
    
    $("#email_mobile_error").html("");
    $("#password_error").html("");
    
  var email_mobile = document.forms["supplier_login_form"]["email_mobile"].value.trim();
  var pass = document.forms["supplier_login_form"]["password"].value.trim();
  
  var is_valid = validateLogin(email_mobile,pass);

  return is_valid;
  
} 

</script>

@endsection