@section('title', 'Add Supplier Admixture Stock')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Add Admixture Stock</h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="vehicle-block vehicle-category-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="data-box-header m-b20">
                    <h2 class="box-title">Add Admixture Stock</h2>
                </div>
                <div class="vehicle-list-block">
                    @if(isset($data["error"]))
                        <div class="error">{{ $data["error"]["message"] }}</div>
                    @endif
                    <div class="vehicle-block vehicle-category-block">
                        <form action="" method="post" name="add_admixture_form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Plant Address <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select name="address_id" id="address_id" class="form-control">
                                                <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                                @if(false)
                                                    @if(isset($data["address_data"]))
                                                        @foreach($data["address_data"] as $value)
                                                            <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                        @endforeach
                                                    @endif
                                                @endif

                                                @php

                                                    $profile = session('supplier_profile_details', null);

                                                @endphp
                                                @if(!isset($profile["master_vendor_id"]))
                                                    <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                    @if(isset($data["address_data"]))
                                                        @foreach($data["address_data"] as $value)
                                                            <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @if(isset($data["address_data"]))
                                                        @foreach($data["address_data"] as $value)
                                                            @if($profile["user_id"] == $value["sub_vendor_id"])
                                                                <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Admixture Brand <span class="str">*</span>(Choose minimum 3 brands)</label>
                                        <div class="">
                                            <!-- <select name="brand_id" id="ad_mixture_brand_id" class="form-control">
                                                <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                @if(isset($data["admixture_brand_data"]))
                                                    @foreach($data["admixture_brand_data"] as $value)
                                                        <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                    @endforeach
                                                @endif
                                            </select> -->
                                            @if(isset($data["admixture_brand_data"]))
                                                @foreach($data["admixture_brand_data"] as $value)

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input admix_brand_checkbox" type="checkbox" id="" value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">
                                                    <input type="hidden" value="" id="" />
                                                    <label class="form-check-label">
                                                        {{ $value['name'] }}
                                                    </label>
                                                </div>

                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Admixture Types <span class="str">*</span>(Choose minimum 3 types)</label>
                                        <div class="" id="admix_type_selection_box_div">
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                                <div class="input-group">
                                    <button type="submit" class="site-button m-r10">Save</button>
                                    <a href="{{ route('design_mix_detail') }}" class="site-button gray">Cancel</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

<div id="admixture_type_div_for_choose" style="display:none;">

    <label><b>Admixture Brand </b></label>
    @if(isset($data["admixture_types_data"]))
        @foreach($data["admixture_types_data"] as $value)
        <div class="col-md-4" style="float: left;margin-bottom: 15px;">
            <div class="form-check form-check-inline">
                <div class="">
                    <input class="form-check-input" type="checkbox" id="" >
                    <input type="hidden" value="" id="" />
                    <span class="form-check-label">
                        {{ $value['name'] }}
                    </span>
                    <div style="margin-top: 15px;">
                        <input type="text" name="product_name_1" value="" class="form-control" placeholder="Add Stock for {{ $value['name'] }} In KG">
                    </div>
                </div>
                
                    
                
            </div>
        </div>
        @endforeach
    @endif

</div>

<script>

$('#us2').locationpicker({
    enableAutocomplete: true,
        enableReverseGeocode: true,
    radius: 0,
    inputBinding: {
        latitudeInput: $('#us2_lat'),
        longitudeInput: $('#us2_lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2_address')
    },
    onchanged: function (currentLocation, radius, isMarkerDropped) {

            // var addressComponents = $(this).locationpicker('map').location.addressComponents;
        console.log(currentLocation);  //latlon
        // updateControls(addressComponents); //Data
        }
    });

    function updateControls(addressComponents) {
        console.log(addressComponents);
    }

</script>


@endsection