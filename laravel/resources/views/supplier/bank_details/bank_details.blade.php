@section('title', 'Bank Detail')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Bank Detail</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="bank-detail-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="bank-detail-block add-bank-detail-block">
                            <a href="javascript:;" onclick="resetForm('add_bank_detail_form','add-bank-details')" data-toggle="modal" data-target="#add-bank-details"><i>+</i></a>
                        </div>
                    </li>
                    @if(isset($data["data"]))
                    <?php //dd($data["data"]); ?>
                    @php
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                        <li>
                            <div class="bank-details-select {{ ($value['is_default'] == true) ? 'active' : '' }}">
                                
                                @if($value["is_default"])
                                    <div class="select-bank-detail cstm-css-checkbox">
                                        <input type="checkbox" id="selectBank" checked>
                                        <label for="selectBank"></label>
                                    </div> 
                                @else
                                    <div class="make-default-btn site-button green" onclick="showSetDefaultDialog('{{ json_encode($value) }}','{{ csrf_token() }}')">Make It default</div>
                                @endif
                                <div class="bank-detail-block p-a15">
                                    <div class="top-right-edit-link">
                                        <a href="javascript:;" onclick="showBankDetailEditDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-toggle="modal" data-target="#add-product-list"><i class="fa fa-pencil-square-o"></i></a>
                                    </div>
                                    <p><span class="vTitle">Account Holder Name : </span> <span class="vName">{{ isset($value["account_holder_name"]) ? $value["account_holder_name"] : '' }}</span></p>
                                    <p><span class="vTitle">Bank Name : </span> <span class="vName">{{ isset($value["bank_name"]) ? $value["bank_name"] : '' }}</span></p>
                                    <p><span class="vTitle">Account Number : </span> <span class="vName">{{ isset($value["account_number"]) ? $value["account_number"] : '' }}</span></p>
                                    <p><span class="vTitle">IFSC Code : </span> <span class="vName">{{ isset($value["ifsc"]) ? $value["ifsc"] : '' }}</span></p>
                                    <p><span class="vTitle">Branch Name : </span> <span class="vName">{{ isset($value["branch_name"]) ? $value["branch_name"] : '' }}</span></p>
                                    <p><span class="vTitle">Account Type : </span> <span class="vName">{{ isset($value["account_type"]) ? $value["account_type"] : '' }}</span></p>
                                    <p>
                                        <a href="{{ isset($value["cancelled_cheque_image_url"]) ? $value["cancelled_cheque_image_url"] : '' }}" class="gal_link">
                                            <span class="chq-img-block"><img src="{{ isset($value["cancelled_cheque_image_url"]) ? $value["cancelled_cheque_image_url"] : '' }}" alt="" /></span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    @endif
                    

                    
                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection