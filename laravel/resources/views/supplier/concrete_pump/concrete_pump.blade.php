@section('title', 'Supplier Concrete Pump')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="main-title">Concrete Pump (CP)</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="vehicle-block vehicle-category-block wht-tble-bg">
        <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="sub-title-block">
                <h2 class="sub-title pull-left">Concrete Pump (CP) List</h2>
                <a href="javascript:;" onclick="resetForm('add_concrete_pump_form','add-concrete-pump-details')" data-toggle="modal" data-target="#add-concrete-pump-details" class="site-button m-r10">Add CP</a>
                <a href="{{ route('supplier_concrete_pump_details') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                @if(isset($data['data']) && count($data['data']) > 0)
                    <button id="filterCollapse" class="site-button outline"><i class="fa fa-filter"></i></button>
                @endif
            </div>
            <div class="vehicle-list-block">
            @if(isset($data["data"]) && !empty($data["data"]))
                    @php
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)
                        @if($count == 0)
                            @php
                                $first_created_date = $value["created_at"];
                                $count++;
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                        <div class="vehicle-list-contet-block m-b20">
                            <div class="edit_unawail_btn">
                                <a href="javascript:;" onclick="viewCPunAvailability('{{$value['_id']}}','{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : $value['_id'] }}')" class="site-button red">UnAvailable On</a>
                                <a href="javascript:;" class="site-button green" onclick="showConcretePumpEditDialog('{{ json_encode($value) }}')" data-toggle="modal" data-target="#add-product-list" data-tooltip="Edit"><i class="fa fa-edit"></i></a>
                            </div>
                            <ul>
                                <li>
                                    <p><span class="vTitle">Concrete Pump Category : </span> <span class="vName">{{ isset($value["concrete_pump_category"]["category_name"]) ? $value["concrete_pump_category"]["category_name"] : '' }}</span></p>
                                    <p><span class="vTitle">Concrete Pump Make : </span> <span class="vName">{{ isset($value["company_name"]) ? $value["company_name"] : '' }}</span></p>
                                    <p><span class="vTitle">Concrete Pump Model : </span> <span class="vName">{{ isset($value["concrete_pump_model"]) ? $value["concrete_pump_model"] : '' }}</span></p>
                                    <p><span class="vTitle">Manufacture Year : </span> <span class="vName">{{ isset($value["manufacture_year"]) ? $value["manufacture_year"] : '' }}</span></p>
                                    <p><span class="vTitle">Pipe Connection (Meter) : </span> <span class="vName">{{ isset($value["pipe_connection"]) ? $value["pipe_connection"] : '' }}</span></p>
                                    
                                    <p>
                                        <span class="vTitle">Plant Address : </span> <span class="vName">
                                        {{ isset($value["business_name"]) ? $value["business_name"] : '' }},
                                        {{ isset($value["line1"]) ? $value["line1"] : '' }},
                                        {{ isset($value["line2"]) ? $value["line2"] : '' }},
                                        {{ isset($value["city_name"]) ? $value["city_name"] : '' }} - {{ isset($value["pincode"]) ? $value["pincode"] : '' }},
                                        {{ isset($value["state_name"]) ? $value["state_name"] : '' }}
                                        </span>
                                    </p>
                                    
                                </li>
                                <li>
                                    <p><span class="vTitle">Concrete Pump Capacity (Per Cu.Mtr / hour) : </span> <span class="vName">{{ isset($value["concrete_pump_capacity"]) ? $value["concrete_pump_capacity"] : '' }}</span></p>
                                    <!-- <p><span class="vTitle">With / `   Without Operator : </span> <span class="vName">{{ isset($value["is_Operator"]) ? 'With Operator' : 'Without Operator' }}</span></p> -->
                                    <p><span class="vTitle">With / Without Helper : </span> <span class="vName">{{ isset($value["is_helper"]) ? 'With Helper' : 'Without Helper' }}</span></p>
                                    <p><span class="vTitle">Transportation Charge (Per KM) : </span> <span class="vName">{{ isset($value["transportation_charge"]) ? $value["transportation_charge"] : '' }}</span></p>
                                    <p><span class="vTitle">Created On : </span> <span class="vName">{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span></p>
                                </li>
                                <li>
                                    <p><span class="vTitle">Concrete Pump Price (Per Day) : </span> <span class="vName">{{ isset($value["concrete_pump_price"]) ? $value["concrete_pump_price"] : '' }}</span></p>
                                    <p><span class="vTitle">Concrete Pump Serial No : </span> <span class="vName">{{ isset($value["serial_number"]) ? $value["serial_number"] : '' }}</span></p>
                                    <p><span class="vTitle">Operator Name : </span> <span class="vName">{{ isset($value["operator_info"]["operator_name"]) ? $value["operator_info"]["operator_name"] : '' }}</span></p>
                                    <p><span class="vTitle">Operator Mobile No. : </span> <span class="vName">{{ isset($value["operator_info"]["operator_mobile_number"]) ? $value["operator_info"]["operator_mobile_number"] : '' }}</span></p>
                                    <p><span class="vTitle">CP Gang Name : </span> <span class="vName">{{ isset($value["gang_info"]["gang_name"]) ? $value["gang_info"]["gang_name"] : '' }}</span></p>
                                    <p><span class="vTitle">CP Gang Mobile No. : </span> <span class="vName">{{ isset($value["gang_info"]["gang_mobile_number"]) ? $value["gang_info"]["gang_mobile_number"] : '' }}</span></p>
                                </li>
                                <li>
                                    <div id="gallery">
                                        <a href="{{ isset($value["concrete_pump_image_url"]) ? $value["concrete_pump_image_url"] : '' }}" class="gal_link vehicle-img-block">
                                            <span class="chq-img-block"><img src="{{ isset($value["concrete_pump_image_url"]) ? $value["concrete_pump_image_url"] : '' }}" alt="" /></span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                            <ul class="mt-3 vehicle_gallary d-none">
                                <li>
                                    <div id="gallery">
                                        <a href="{{ isset($value["concrete_pump_image_url"]) ? $value["concrete_pump_image_url"] : '' }}" class="gal_link">
                                            <span class="chq-img-block"><img src="{{ isset($value["concrete_pump_image_url"]) ? $value["concrete_pump_image_url"] : '' }}" alt="" /></span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    @endforeach

                @else

                    <p style="text-align: center;">No Record Found</p>


                @endif

                <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        <div class="pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if($first_created_date)
                            <form action="{{ route('logistic_vehicles') }}" method="get">
                                @if(Request::get('address_id'))
                                    <input type="hidden" name="address_id" value="{{ Request::get('address_id') ? Request::get('address_id') : ''  }}" class="form-control" placeholder="Name">
                                @endif
                                @if(Request::get('concrete_pump_category_id'))
                                    <input type="hidden" name="concrete_pump_category_id" value="{{ Request::get('concrete_pump_category_id') ? Request::get('concrete_pump_category_id') : ''  }}" class="form-control" placeholder="Mobile No">
                                @endif
                                
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if($last_created_date)
                            <form action="{{ route('logistic_vehicles') }}" method="get">
                                @if(Request::get('address_id'))
                                    <input type="hidden" name="address_id" value="{{ Request::get('address_id') ? Request::get('address_id') : ''  }}" class="form-control" placeholder="Name">
                                @endif
                                @if(Request::get('concrete_pump_category_id'))
                                    <input type="hidden" name="concrete_pump_category_id" value="{{ Request::get('concrete_pump_category_id') ? Request::get('concrete_pump_category_id') : ''  }}" class="form-control" placeholder="Mobile No">
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                    </div>

            </div>
        </div>
        
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection