
<!--Contact Detail start -->
<div class="bank-detail-main-block wht-tble-bg">
    <div class="row">
        <div class="col-md-12">
            <div class="tm_unawail_popup_list">
                
                @if(isset($data["data"]) && count($data["data"]) > 0)
                
                @foreach($data["data"] as $value)  
                    
                    <div class="unawail_date_bx">
                        
                        <!-- {{ isset($value["unavailable_at"]) ? date('d M Y', strtotime($value["unavailable_at"])) : '' }} -->
                        {{ isset($value["start_time"]) ? date('d M Y, h:i a', strtotime($value["start_time"])) : '' }} To {{ isset($value["end_time"]) ? date('d M Y, h:i a', strtotime($value["end_time"])) : '' }}
                        <div class="tm_time_action">
                            <a href="javascript:void(0)" onclick="showEditCPPopup('{{ $value['_id'] }}','{{date('d-M-Y', strtotime($value["start_time"]))}}','{{date('H:i', strtotime($value["start_time"]))}}','{{date('H:i', strtotime($value["end_time"]))}}')" data-tooltip="Edit"><i class="fa fa-edit"></i></a>
                            <a href="javascript:void(0)" onclick="confirmPopup(6,'{{ json_encode($value) }}','{{ csrf_token() }}','Are you sure you want to delete this time?')" data-tooltip="Delete"><i class="fa fa-trash"></i></a>
                        </div>  
                    </div>
                    @endforeach

                @else

                <div style="width:100%;text-align:center;">
                    No Data Found
                </div>

                @endif
                

                
            </div>
        </div>
    </div>
</div>
<!--Contact Detail end -->
