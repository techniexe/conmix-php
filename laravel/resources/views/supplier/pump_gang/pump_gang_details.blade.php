@section('title', 'Supplier Concrete Pump Gang')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="main-title">Concrete Pump Gang</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="contact-detail-main-block wht-tble-bg">
        <div class="sub-title-block">
            <h2 class="sub-title pull-left">Concrete Pump Gang</h2>
            <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
            <a href="{{ route('supplier_pumpgang_details') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
            @if(isset($data['data']) && count($data['data']) > 0)
                <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
            @endif
        </div>
        <div class="row">

            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="contact-detail-block add-contact-detail-block">
                            <a href="javascript:;" onclick="resetForm('add_pumpgang_form','add-pumpgang-details')" data-toggle="modal" data-target="#add-pumpgang-details"><i>+</i></a>
                        </div>
                    </li>

                    @if(isset($data["data"]))
                        @foreach($data["data"] as $value)
                            <li>
                                
                                <div class="contact-detail-block p-a15">
                                    @if(isset($value['gang_pic']))
                                    <p> <span class="driver-img-block"><img src="{{ isset($value['gang_pic']) ? $value['gang_pic'] : '' }}" height="50px" width="50px" alt="" /></span></p>
                                    @endif
                                    <p><i class="fa fa-calendar"></i> <span class="vTitle">Plant Address : </span> <span class="vName">{{ isset($value['addressDetails']) ? ($value['addressDetails']['business_name'].', '. $value['addressDetails']['line1'].', '. $value['addressDetails']['line2']) : '-' }}</span></p>
                                    <p><i class="fa fa-user"></i> <span class="vTitle">Person Name : </span> <span class="vName">{{ isset($value['gang_name']) ? $value['gang_name'] : '-' }}</span></p>
                                    <p><i class="fa fa-phone-square"></i> <span class="vTitle">Mobile No. : </span> <span class="vName">{{ isset($value['gang_mobile_number']) ? $value['gang_mobile_number'] : '-' }}</span></p>
                                    <p><i class="fa fa-phone-square"></i>   <span class="vTitle">Alternate No. : </span> <span class="vName">{{ isset($value['gang_alt_number']) ? $value['gang_alt_number'] : '-' }}</span></p>
                                    <p><i class="fa fa-whatsapp"></i> <span class="vTitle">Whatsapp No. : </span> <span class="vName">{{ isset($value['gang_whatsapp_number']) ? $value['gang_whatsapp_number'] : '-' }}</span></p>
                                    <p><i class="fa fa-calendar"></i> <span class="vTitle">Created at : </span> <span class="vName">{{ isset($value['created_at']) ? AdminController::dateTimeFormat($value["created_at"]) : '-' }}</span></p>
                                    <div class="driver_opration_btn top-right-edit-link">
                                        <a href="javascript:;" onclick="showPumpgangDetailEditDialog('{{ json_encode($value) }}')" class="site-button green adrs-edt-icon"  title="Edit"><i class="fa fa-edit"></i></a>
                                       <!--  <a href="javascript:;" class="site-button red adrs-dlt-icon" data-toggle="modal" data-target="" title="Delete"><i class="fa fa-trash"></i></a> -->
                                    </div>
                                </div>
                                
                            </li>
                        @endforeach
                    @endif
                    
                    
                    
                    
                    
                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection