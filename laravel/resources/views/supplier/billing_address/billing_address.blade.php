@section('title', 'Supplier Billing Address')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Billing Address</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="address-detail-main-block wht-tble-bg">
        <div class="sub-title-block">
            <h2 class="sub-title pull-left">Billing Address</h2>
            <a href="{{ route('suppllier_billing_address_view') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
            @if(isset($data['data']) && count($data['data']) > 0)
                <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="address-detail-block add-address-detail-block">
                            <a href="{{ route('suppllier_billing_address_add') }}"><i>+</i></a>
                        </div>
                    </li>
                    @if(isset($data["data"]))

                    @php
                        //dd($data["data"]);
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                        $lattitude = '';
                        $longitude = '';
                    @endphp
                    @foreach($data["data"] as $value)
                        @if($count == 0)
                            @php
                                $first_created_date = $value["created_at"];
                                $count++;
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp

                            <li>

                                <div class="address-detail-block p-a15">
                                    <div class="top-right-edit-link">
                                        <form action="{{ route('suppllier_billing_address_edit') }}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" value="{{ json_encode($value) }}" name="address_details" />
                                            <button class="site-button green button-sm"><i class="fa fa-pencil-square-o"></i></button>
                                        </form>
                                    </div>
                                    <p><span class="vTitle">Company Name : </span> <span class="vName">{{ isset($value["company_name"]) ? $value["company_name"] : '' }}</span></p>

                                    <p><span class="vTitle">Address : </span> <span class="vName">{{ isset($value["line1"]) ? $value["line1"] : '' }},{{ isset($value["line2"]) ? $value["line2"] : '' }}</span></p>
                                    <p><span class="vTitle">City : </span> <span class="vName">{{ isset($value["cityDetails"]["city_name"]) ? $value["cityDetails"]["city_name"] : '' }}</span></p>
                                    <p><span class="vTitle">State : </span> <span class="vName">{{ isset($value["stateDetails"]["state_name"]) ? $value["stateDetails"]["state_name"] : '' }}</span></p>

                                    <p><span class="vTitle">Pincode : </span> <span class="vName">{{ isset($value["pincode"]) ? $value["pincode"] : '' }}</span></p>
                                    <p><span class="vTitle">GST No. : </span> <span class="vName">{{ isset($value["gst_number"]) ? $value["gst_number"] : '' }}</span></p>
                                    <p><span class="vTitle">Plant Name. : </span> <span class="vName">
                                        <?php $plant_name = ""; ?>
                                        @if(isset($value["AddressDetails"]))
                                            @foreach($value["AddressDetails"] as $add_value)
                                                <?php $plant_name = $plant_name.''.$add_value["business_name"].', '; ?>
                                            @endforeach
                                            {{ rtrim($plant_name,', ') }}
                                        @endif

                                    </span></p>
                                    <p>
                                        @if(isset($value["gst_image_url"]))

                                            @if(strpos($value["gst_image_url"], '.jpg') || strpos($value["gst_image_url"], '.png') || strpos($value["gst_image_url"], '.jpeg'))
                                                <a href="{{ $value['gst_image_url'] }}" class="document_box gal_link">
                                                    <i class="fa fa-image"></i>
                                                    <span>GST Certificate</span>
                                                </a>

                                            @endif

                                            @if(strpos($value["gst_image_url"], '.pdf'))
                                                <a href="{{ $value['gst_image_url'] }}" class="document_box pdffile" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i>
                                                    <span>GST Certificate</span>
                                                </a>
                                            @endif

                                        @endif

                                    </p>
                                    <p>
                                        @if(isset($value["logo_image_url"]))

                                            @if(strpos($value["logo_image_url"], '.jpg') || strpos($value["logo_image_url"], '.png') || strpos($value["logo_image_url"], '.jpeg'))
                                                <a href="{{ $value['logo_image_url'] }}" class="document_box gal_link">
                                                    <i class="fa fa-image"></i>
                                                    <span>Logo image for invoice</span>
                                                </a>

                                            @endif                                           

                                        @endif

                                    </p>
                                </div>
                            </li>
                        @endforeach
                    @endif

                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection
