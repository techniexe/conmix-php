@section('title', 'Edit Supplier Billing Address')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Edit Billing Address</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="add-address-main-block wht-tble-bg">

        @if(isset($data["error"]))
        <div class="">{{ $data["error"]["message"] }}</div>
        @endif
        <form action="" name="add_billing_address_form" method="post" enctype="multipart/form-data">
            @csrf
            @if(isset($data["data"]))
                @php
                    $all_data = $data["data"];
                @endphp
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Registered Company Name<span class="str">*</span></label>
                                        <input type="text" value="{{ isset($all_data->company_name) ? $all_data->company_name : '' }}" name="company_name" class="form-control text-capitalize" placeholder="e.g. ambika hardware">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Address Line 1 <span class="str">*</span></label>
                                        <input type="text" value="{{ isset($all_data->line1) ? $all_data->line1 : '' }}" name="line1" class="form-control text-capitalize" placeholder="e.g. address line 1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Address Line 2 <span class="str">*</span></label>
                                        <input type="text" value="{{ isset($all_data->line2) ? $all_data->line2 : '' }}" name="line2" class="form-control text-capitalize" placeholder="e.g. address line 2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group single-search-select2">
                                        <label>State Name <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" id="state_name_city_address" name="state_id" class="form-control" data-placeholder="Select State Name">
                                                <option value="">Select State</option>
                                                @if(isset($data["state_data"]))
                                                    @foreach($data["state_data"] as $value)
                                                        <option value="{{ $value['_id'] }}" {{ isset($all_data->stateDetails->_id) ? ($all_data->stateDetails->_id == $value['_id'] ? "selected" : '') : '' }}>{{ $value["state_name"] }}</option>
                                                        
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group single-search-select2">
                                        <label>Select City Name <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" id="state_name_city_filter" name="city_id" class="form-control" data-placeholder="Select city name">
                                                <option value="">Select City</option>
                                                @if(isset($data["city_data"]))
                                                    @foreach($data["city_data"] as $value)
                                                        @if($all_data->cityDetails->_id == $value['_id'])
                                                            <option value="{{ $value['_id'] }}" {{ $all_data->cityDetails->_id == $value['_id'] ? "selected" : '' }}>{{ $value["city_name"] }}</option>
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Pincode <span class="str">*</span></label>
                                        <input type="text" name="pincode" value="{{ isset($all_data->pincode) ? $all_data->pincode : '' }}" class="form-control" placeholder="e.g. 123456">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>GST Number <span class="str">*</span></label>
                                        <input type="text" value="{{ isset($all_data->gst_number) ? $all_data->gst_number : '' }}" name="gst_number" id="gst_number" class="form-control text-uppercase" placeholder="e.g. 123456">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group" id="gst_image_div">
                                        <label>Upload GST Image/PDF <span class="str">*</span></label>
                                        <div class="file-browse">
                                            <span class="button-browse"> Browse <input type="file" name="gst_image"></span>
                                            <input type="text" id="gst_image_text"  class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="logo_image_div">
                                    <label>Upload company logo Image for Invoice <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse"> Browse <input type="file" name="logo_image"></span>
                                        <input type="text" id="logo_image_text"  class="form-control browse-input" placeholder="e.g. jpg" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <input type="hidden" name="add_billing_address_form_type" id="add_billing_address_form_type" value="edit"/>
                    <input type="hidden" name="address_id" id="address_id" value="{{ $all_data->_id }}"/>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group m-t20">
                            <div class="input-group">
                                <button type="submit" class="site-button m-r10">Save</button>
                                <a href="{{ route('suppllier_billing_address_view') }}" class="site-button gray">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </form>

    </div>
    <!--Contact Detail end -->
</div>

<script>

    @if(isset($all_data->region_id))
        $('#select_region_id').val('<?php echo $all_data->region_id; ?>');



        $(window).on('load', function() {
            // code here
            $('#select_region_id').trigger("change");
        });

    @endif

</script>

@endsection