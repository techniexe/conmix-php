@section('title', 'Add Supplier Billing Address')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Add Billing Address</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="add-address-main-block wht-tble-bg">
        @if(isset($data["error"]))
        <div class="">{{ $data["error"]["message"] }}</div>
        @endif
        <form action="" name="add_billing_address_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Registered Company Name <span class="str">*</span></label>
                                    <input type="text" name="company_name" class="form-control text-capitalize" placeholder="e.g. Ambika Plant">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Registered Address line 1 <span class="str">*</span></label>
                                    <input type="text" name="line1" id="address_line1" class="form-control text-capitalize" placeholder="e.g. Registered Address">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Registered Address Line 2 <span class="str">*</span></label>
                                    <input type="text" name="line2" id="address_line2" class="form-control text-capitalize" placeholder="e.g. Address line 2">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group single-search-select2" id="state_name_city_div">
                                    <label>State Name <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select id="state_name_city_address" name="state_id" class="form-control" data-placeholder="Select State Name">
                                            <option value="">Select State</option>
                                            @if(isset($data["state_data"]))
                                            @foreach($data["state_data"] as $value)
                                            <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <!-- <select id="add_site_state" readOnly name="state_id" class="form-control " data-placeholder="Select State Name">
                                                    <option disabled="disabled" selected="selected">State</option>

                                                </select> -->
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group single-search-select2" id="state_name_city_filter_div">
                                    <label>Select City Name <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select id="state_name_city_filter" name="city_id" class="form-control" data-placeholder="Select city name">
                                            <option value="">Select City</option>
                                            @if(isset($data["city_data"]))
                                            @foreach($data["city_data"] as $value)
                                            <option value="{{ $value['_id'] }}">{{ $value["city_name"] }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <!-- <select id="add_site_city" readOnly name="city_id" class="form-control" data-placeholder="Select City Name">
                                            <option disabled="disabled" selected="selected">City</option>

                                        </select> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Pincode <span class="str">*</span></label>
                                    <input type="text" name="pincode" id="pincode" class="form-control" placeholder="e.g. 123456">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>GST No <span class="str">*</span></label>
                                    <input type="text" name="gst_number" id="gst_number" class="form-control text-uppercase" placeholder="e.g. 00AABBC0000A1BB">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="gst_image_div">
                                    <label>Upload GST Image/PDF <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse"> Browse <input type="file" name="gst_image"></span>
                                        <input type="text" id="gst_image_text"  class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="logo_image_div">
                                    <label>Upload company logo Image for Invoice <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse"> Browse <input type="file" name="logo_image"></span>
                                        <input type="text" id="logo_image_text"  class="form-control browse-input" placeholder="e.g. jpg" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="" value="add" id="add_billing_address_form_type" />
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group m-t20">
                        <div class="input-group">
                            <button type="submit" class="site-button m-r10">Save</button>
                            <a href="{{ route('suppllier_billing_address_view') }}" class="site-button gray">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!--Contact Detail end -->


     <!--  <div class="form-group">
                                                <div class="input-group">

                                                     <div class="location_field">
                                                        <input type="text" class="form-control" placeholder="Enter city name">
                                                        <button type="button" class="site-button umcl-btn">Use my current location</button>
                                                    </div>
                                                   <input type="text" placeholder="Enter Your Location" required name="us2_address" id="us2_address" class="form-control" onkeydown="return event.key != 'Enter';"/ style=" margin-top: 15px;">
                                                </div>
                                            </div>
                                        <div class="map-content-block add-vehicle-map-block m-t20" style="height: 410px; margin-top: 0px;">

                                            <div id="us2" style="width: 100%; height: 400px;"></div>
                                        </div> -->

                                        <script>

                                            $('#us2').locationpicker({
                                                enableAutocomplete: true,
                                                enableReverseGeocode: true,
                                                radius: 0,
                                                inputBinding: {
                                                    latitudeInput: $('#us2_lat'),
                                                    longitudeInput: $('#us2_lon'),
                                                    radiusInput: $('#us2-radius'),
                                                    locationNameInput: $('#us2_address')
                                                },
                                                onchanged: function (currentLocation, radius, isMarkerDropped) {
                                                    // var addressComponents = $(this).locationpicker('map').location.addressComponents;
        console.log(currentLocation);  //latlon
        // updateControls(addressComponents); //Data
    }
});

                                            function updateControls(addressComponents) {
                                                console.log(addressComponents);
                                            }

                                        </script>

                                        @endsection

