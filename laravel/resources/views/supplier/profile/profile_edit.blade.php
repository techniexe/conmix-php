@section('title', 'Edit Supplier Profile')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Edit My Profile</h1>
    <div class="clearfix"></div>
    <!-- my profile start -->
    <div class="row">
        <div class="col-md-12">
            <form action="" name="edit_myprofile_details_form" method="post">
            @csrf
                @php
                    $profile_data = $data["data"];

                @endphp
                <div class="my-profile-block edit-my-profile-block">
                    <div class="my-profile-content-block">
                        <h3>Account Details</h3>
                        <div class="my-profile-details-block">
                            <div class="row">
                                     <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group ">
                                                <label>Company Type <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <select class="form-control" name="company_type">
                                                        <option disabled="disabled">Select Type</option>
                                                        <option value="Partnership" {{ $profile_data['company_type'] == 'Partnership' ? 'selected' : '' }}>Partnership</option>
                                                        <option value="Proprietor" {{ $profile_data['company_type'] == 'Proprietor' ? 'selected' : '' }}>Proprietor</option>
                                                        <option value="LLP" {{ $profile_data['company_type'] == 'LLP' ? 'selected' : '' }}>LLP.</option>
                                                        <option value="LTD" {{ $profile_data['company_type'] == 'LTD' ? 'selected' : '' }}>LTD.</option>
                                                        <option value="PVT LTD" {{ $profile_data['company_type'] == 'PVT LTD' ? 'selected' : '' }}>PVT.LTD.</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Company Name <span class="str">*</span></label>
                                            <input type="text" class="form-control" name="company_name" value="{{ isset($profile_data['company_name']) ? $profile_data['company_name'] : '' }}" placeholder="e.g. Ambika Hardware">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Person Name <span class="str">*</span></label>
                                            <input type="text" class="form-control" name="full_name" value="{{ isset($profile_data['full_name']) ? $profile_data['full_name'] : '' }}" placeholder="e.g. Johndoe">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div id="edit_profile_mobile_number_div">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <label>Mobile No. <span class="str">*</span></label>
                                                <a href="javascript:void(0)" onclick="OTPonUpdateMobile(1, 0, 0)">Update</a>
                                            </div>
                                            <input type="hidden" id="edit_address_mobile" value="{{ isset($profile_data['mobile_number']) ? $profile_data['mobile_number'] : '' }}" />
                                            <input type="tel" id="mobile_or_email" readOnly class="form-control" name="mobile_number" placeholder="e.g. 9898989898" value="{{ isset($profile_data['mobile_number']) ? $profile_data['mobile_number'] : '' }}" id="mobileno">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Landline No. (with STD)</label>
                                        <input type="text" class="form-control" name="landline_number" value="{{ isset($profile_data['landline_number']) ? $profile_data['landline_number'] : '' }}" placeholder="e.g. 0791234567">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <label>Email <span class="str">*</span></label>
                                            <a href="javascript:void(0)" onclick="OTPonUpdateEmail(1, 0, 0)">Update</a>
                                        </div>
                                            <input type="hidden" id="edit_address_email" value="{{ isset($profile_data['email']) ? $profile_data['email'] : '' }}" />
                                            <input type="email" id="edit_profile_email" readOnly class="form-control" name="email" value="{{ isset($profile_data['email']) ? $profile_data['email'] : '' }}" placeholder="e.g. johndoe@example.com">
                                    </div>
                                </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Company Registration No</label>
                                            <input type="text" class="form-control" name="company_certification_number" value="{{ isset($profile_data['company_certification_number']) ? $profile_data['company_certification_number'] : '' }}" placeholder="e.g. CSC123456789">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group" id="company_certification_image_div">
                                            <label>Company Registration Certificate</label>
                                            <div class="file-browse">
                                                <span class="button-browse">
                                                    Browse <input type="file" name="company_certification_image">
                                                </span>
                                                <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>PAN No <span class="str">*</span></label>
                                                    <input type="text" class="form-control" name="pan_number" value="{{ isset($profile_data['pan_number']) ? $profile_data['pan_number'] : '' }}" placeholder="e.g. AAABB0000C">
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group" id="pancard_image_div">
                                            <label>Upload Pancard </label>
                                            <div class="file-browse">
                                                <span class="button-browse">
                                                    Browse <input type="file" name="pancard_image">
                                                </span>
                                                <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>GST No <span class="str">*</span></label>
                                                    <input type="text" class="form-control" name="gst_number" value="{{ isset($profile_data['gst_number']) ? $profile_data['gst_number'] : '' }}" placeholder="e.g. 00AABBC0000A1BB">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Upload GST Certificate </label>
                                                    <div class="file-browse">
                                                        <span class="button-browse">
                                                            Browse <input type="file" name="gst_certification_image">
                                                        </span>
                                                        <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->




                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="is_my_profile" value="yes"/>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <button type="submit" class="site-button m-r10">Save</button>
                               <a href="{{ route('supplier_profile') }}" class="site-button gray">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="clearfix m-t20"></div>

            <form action="" name="edit_business_details_form" method="post">
            @csrf
                <div class="my-profile-block edit-my-profile-block">
                    <div class="my-profile-content-block">
                        <h3>Business Information</h3>
                        <div class="my-profile-details-block">
                            <div class="row">


                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Annual Turnover ( <i class="fa fa-rupee"></i> ) (Optional) </label>
                                            <input type="text" class="form-control" name="annual_turnover" value="{{ isset($profile_data['annual_turnover']) ? $profile_data['annual_turnover'] : '' }}" placeholder="e.g. 5,00,000">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>EHS Certificate No. (Optional) </label>
                                            <input type="text" class="form-control" name="ehs_as_per_iso" value="{{ isset($profile_data['ehs_as_per_iso']) ? $profile_data['ehs_as_per_iso'] : '' }}" placeholder="e.g. 123456789">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Trade Capacity (Optional) </label>
                                            <input type="tel" class="form-control" name="trade_capacity" value="{{ isset($profile_data['trade_capacity']) ? $profile_data['trade_capacity'] : '' }}" placeholder="e.g. 10000 MT">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>OHSAS Certificate No. (Optional) </label>
                                            <input type="text" class="form-control" name="ohsas_as_per_iso" value="{{ isset($profile_data['ohsas_as_per_iso']) ? $profile_data['ohsas_as_per_iso'] : '' }}" placeholder="e.g. 123456789">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>No of Employees (Optional) </label>
                                            <input type="text" class="form-control"  name="number_of_employee" value="{{ isset($profile_data['number_of_employee']) ? $profile_data['number_of_employee'] : '' }}" placeholder="e.g. 50">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Listed with other e-commerce Site (Optional) </label>
                                            <div class="cstm-select-box">
                                            <select class="form-control" name="working_with_other_ecommerce" >
                                                <option disabled="disabled" selected="selected">Select Account Type</option>
                                                @if(isset($profile_data['working_with_other_ecommerce']))
                                                    <option value="yes" {{ $profile_data['working_with_other_ecommerce'] ? 'selected' : '' }}>Yes</option>
                                                    <option value="no" {{ !$profile_data['working_with_other_ecommerce'] ? 'selected' : '' }}>No</option>
                                                @else
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                @endif
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <input type="hidden" name="is_my_profile" value="no"/>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <button type="submit" class="site-button m-r10">Save</button>
                               <a href="{{ route('supplier_profile') }}" class="site-button gray">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- my profile end -->
</div

@endsection

<!-- CP UnAvailable Module Over -->
<div id="Plant_otpModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="otpMessage">
        <label id="plant_OTP_label">Enter the 6 digits OTP sent on your given mobile no., Email id and current registered number <span id="plant_OTP_mob_no"></span><span class="str">*</span></label>
        <input type="text" id="plant_mobile_otp" class="form-control" placeholder="Enter OTP sent on mobile">
        <p id="plant_mobile_otp_error" style="color:red;"></p>

        <input type="text" id="plant_email_otp" class="form-control" placeholder="Enter OTP sent on Email">
        <p id="plant_email_otp_error" style="color:red;"></p>

        <input type="text" id="old_mobile_otp" class="form-control" placeholder="Enter OTP sent on your registered mobile number">
        <p id="old_mobile_otp_error" style="color:red;"></p>
        <div class="text-gray-dark resend_plant_otp_div text-left">
            <!-- <form action=""  name="" method="">
                @csrf -->
                <input class="form-control" name="" value="" required  type="hidden">
                <!-- <button type="submit" name="register_btn" value="register_btn" class="text-primary">Resend OTP</button> -->
                <!-- <div class="timer_otp mb-3">
                    <span id="timer_forgot_pass">
                        <span id="time_forgot_pass">02:00</span>
                    </span>
                </div> -->
            <!-- </form> -->
        </div>
      </div>
      <div class="modal-footer text-center">
          <button type="button" class="site-button green m-r15" id="plant_otpOk">Submit</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="plant_otpCancel">Cancel</button>
      </div>
    </div>
  </div>
</div>


<div id="update_mobile_profile" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <form name="update-mobile-otp-form" style="display: block;" autocomplete="off">
        @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="form-group">
                    <label id="update_mobile_otp_confirm_message">Enter the 6 digits OTP sent on your given mobile no. 2222222222222</label>
                    <input type="text" name="otp_code_1" id="otp_code_1_mobile" class="form-control" placeholder="Enter OTP sent on mobile">
                </div>
                
                <div class="text-gray-dark resend_otp_div text-left">
                    
                    <div class="timer_otp mb-3">
                        <span id="time_update_mobile">
                            <span id="times">01:59</span>
                        </span>
                    </div>
                </div>

                <input type="hidden" name="mobile_number" id="verify_update_mobile" />
                <input type="hidden" name="mobile_or_email_type" id="mobile_type" />
            </div>
            <div class="modal-footer text-center">
                <button type="submit" class="site-button green m-r15">Submit</button>
                <button type="button" data-dismiss="modal" class="site-button red">Cancel</button>
            </div>
            
        </form>
        
    </div>
  </div>
</div>

<div id="new_mobile_no_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="">New Mobile Number</h4>
        </div>
        <form action="" name="new_mobile_form" method="post">
        @csrf
        <div class="modal-body p-tb20 p-lr15">
            
            <div class="form-group">
                <div class="input-group">
                    <label>New Mobile No. <span class="str">*</span></label>
                    <input type="text" name="mobile" id="update_new_mobile_no" class="form-control mobile_validate" placeholder="e.g. 9898989898">
                </div>
            </div>

        </div>
       
        <div class="modal-footer text-left">
            <button type="submit" class="site-button m-r20">Save</button>
            <button class="site-button gray" data-dismiss="modal">Cancel</button>
        </div>
        </form>
    </div>
    </div>
</div>

<div id="update_email_profile" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    <form name="update-email-otp-form" style="display: block;" autocomplete="off">
    @csrf
        <div class="modal-body p-tb20 p-lr15" id="otpMessage">
                <div class="form-group">
                    <label id="update_email_otp_confirm_message"></label>
                    <input type="text" name="otp_code_1" id="otp_code_1_email" class="form-control" placeholder="Enter OTP sent on Email">
                </div>
                <div class="text-gray-dark resend_otp_div text-left">
                    <div class="timer_otp mb-3">
                        <span id="time_update_email">
                            <span id="times">01:59</span>
                        </span>
                    </div>
                </div>
        </div>
        <input type="hidden" name="email" id="verify_update_email" />
                        <input type="hidden" name="mobile_or_email_type" id="email_type" />
        <div class="modal-footer text-center">
            <button type="submit" class="site-button green m-r15" ">Submit</button>
            <button type="button" data-dismiss="modal" class="site-button red" ">Cancel</button>
        </div>
    </form>
    </div>
  </div>
</div>

<div id="new_email_no_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="">New Email</h4>
        </div>
        <form action="" name="new_email_form">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                
                <div class="form-group">
                    <div class="input-group">
                        <label>New Email <span class="str">*</span></label>
                        <input type="text" name="email" id="update_new_email" class="form-control mobile_validate" placeholder="e.g. johndoe@example.com">
                    </div>
                </div>

            </div>
        
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    </div>
</div>