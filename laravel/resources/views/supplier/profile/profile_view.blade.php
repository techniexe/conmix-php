@section('title', 'Supplier Profile')
@extends('supplier.layouts.supplier_layout')
@section('content')
@php
    $profile_data = $data["data"];
@endphp
<div class="middle-container-wrap">
    <h1 class="main-title">My Profile</h1>
    <div class="clearfix"></div>
    <!-- my profile start -->
    <div class="my-profile-block">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="my-profile-content-block">
                    <div class="sub-title-block">
                        <h2 class="sub-title pull-left">Account Details </h2>
                        @php
                            $profile = session('supplier_profile_details', null);
                            //dd($profile);
                        @endphp

                        @if(!isset($profile["master_vendor_id"]))
                            <a href="{{ route('supplier_profile_edit_view') }}" class="edit-profile-btn site-button outline"><i class="fa fa-edit"></i> Edit</a>
                        @endif
                    </div>
                    <div class="my-profile-details-block">
                          <p><strong>Company Type :</strong> {{ isset($profile_data['company_type']) ? $profile_data['company_type'] : '-' }}</p>
                          <p><strong>Company Name :</strong> {{ isset($profile_data['company_name']) ? $profile_data['company_name'] : '-' }}</p>
                        <p><strong>Person Name :</strong> {{ isset($profile_data['full_name']) ? $profile_data['full_name'] : '-' }}</p>

                           <p><strong>Mobile No. :</strong> {{ isset($profile_data['mobile_number']) ? $profile_data['mobile_number'] : '-' }}</p>
                              <p><strong>Landline No. :</strong> {{ isset($profile_data['landline_number']) ? $profile_data['landline_number'] : '-' }}</p>
                              <p><strong>Email :</strong> {{ isset($profile_data['email']) ? $profile_data['email'] : '-' }}</p>
                       
                        <!-- <p><strong>Fax Number:</strong> {{ isset($profile_data['email']) ? $profile_data['email'] : '' }}</p> -->
                        <!-- @if(isset($profile_data["region_name"]))
                            <?php 
                                $region = "";
                                foreach($profile_data["region_name"] as $value){
                                    $region = $region . $value . ', ';
                                }

                                $region = rtrim($region, ', ');
                            ?>
                            <p><strong>Region Serv :</strong> {{ !empty($region) ? $region : "-" }}</p>
                        @endif -->


                        <p>
                            <strong>Certification Image :</strong> 
                            @if(isset($profile_data["company_certification_image_url"]))

                                @if(strpos($profile_data["company_certification_image_url"], '.doc') || strpos($profile_data["company_certification_image_url"], '.docx'))
                                    <a href="{{ $profile_data['company_certification_image_url'] }}" class="document_box docfile" target="_blank">
                                        <i class="fa fa-file-word-o"></i>
                                        <span>Company Certificate</span>
                                    </a>
                                @endif

                                @if(strpos($profile_data["company_certification_image_url"], '.jpg') || strpos($profile_data["company_certification_image_url"], '.png') || strpos($profile_data["company_certification_image_url"], '.jpeg'))
                                    <a href="{{ $profile_data['company_certification_image_url'] }}" class="document_box gal_link" target="_blank">
                                        <i class="fa fa-image"></i>
                                        <span>Company Certificate</span>
                                    </a>

                                @endif

                                @if(strpos($profile_data["company_certification_image_url"], '.pdf'))
                                    <a href="{{ $profile_data['company_certification_image_url'] }}" class="document_box pdffile" target="_blank">
                                        <i class="fa fa-file-pdf-o"></i>
                                        <span>Company Certificate</span>
                                    </a>
                                @endif

                                <!-- <i class="fa fa-image" style="color: #388E3C;"></i>
                                <a href="{{ isset($profile_data['company_certification_image_url']) ? $profile_data['company_certification_image_url'] : '-' }}">Comapny registration Cerificate</a>
                                
                                <a href="#" class="document_box docfile">
                                    <i class="fa fa-file-word-o"></i>
                                    <span>Company Doc file</span>
                                </a> -->

                            @else
                                -
                            @endif
                        </p>

                        <p>
                            <strong>Pan Image :</strong> 
                                @if(isset($profile_data["pancard_image_url"]))
                                    @if(strpos($profile_data["pancard_image_url"], '.doc') || strpos($profile_data["pancard_image_url"], '.docx'))
                                        <a href="{{ $profile_data['pancard_image_url'] }}" class="document_box docfile" target="_blank">
                                            <i class="fa fa-file-word-o"></i>
                                            <span>Pancard</span>
                                        </a>
                                    @endif

                                    @if(strpos($profile_data["pancard_image_url"], '.jpg') || strpos($profile_data["pancard_image_url"], '.png') || strpos($profile_data["pancard_image_url"], '.jpeg'))
                                        <a href="{{ $profile_data['pancard_image_url'] }}" class="document_box gal_link" target="_blank">
                                            <i class="fa fa-image"></i>
                                            <span>Pancard</span>
                                        </a>

                                    @endif

                                    @if(strpos($profile_data["pancard_image_url"], '.pdf'))
                                        <a href="{{ $profile_data['pancard_image_url'] }}" class="document_box pdffile" target="_blank">
                                            <i class="fa fa-file-pdf-o"></i>
                                            <span>Pancard</span>
                                        </a>
                                    @endif
                                @else
                                    -
                                @endif

                                <!-- <i class="fa fa-image" style="color: #388E3C;"></i>
                                <a href="{{ isset($profile_data['company_certification_image_url']) ? $profile_data['company_certification_image_url'] : '-' }}">Comapny registration Cerificate</a>
                                
                                <a href="#" class="document_box docfile">
                                    <i class="fa fa-file-word-o"></i>
                                    <span>Company Doc file</span>
                                </a> -->
                           
                        </p>
                        <p><strong>Pan No :</strong> {{ isset($profile_data['pan_number']) ? $profile_data['pan_number'] : '-' }}</p>
                        

                        <!-- <p>
                            <strong>GST Image :</strong> 
                                @if(isset($profile_data["gst_certification_image_url"]))
                                    @if(strpos($profile_data["gst_certification_image_url"], '.doc') || strpos($profile_data["gst_certification_image_url"], '.docx'))
                                        <a href="{{ $profile_data['gst_certification_image_url'] }}" class="document_box docfile" target="_blank">
                                            <i class="fa fa-file-word-o"></i>
                                            <span>GST Certificate</span>
                                        </a>
                                    @endif

                                    @if(strpos($profile_data["gst_certification_image_url"], '.jpg') || strpos($profile_data["gst_certification_image_url"], '.png') || strpos($profile_data["gst_certification_image_url"], '.jpeg'))
                                        <a href="$profile_data['gst_certification_image_url']" class="document_box gal_link" target="_blank">
                                            <i class="fa fa-image"></i>
                                            <span>GST Certificate</span>
                                        </a>

                                    @endif

                                    @if(strpos($profile_data["gst_certification_image_url"], '.pdf'))
                                        <a href="{{ $profile_data['gst_certification_image_url'] }}" class="document_box pdffile" target="_blank">
                                            <i class="fa fa-file-pdf-o"></i>
                                            <span>GST Certificate</span>
                                        </a>
                                    @endif

                                @else
                                    -
                                @endif

                               
                           
                        </p> -->
                        <!-- <p><strong>GST No :</strong> {{ isset($profile_data['gst_number']) ? $profile_data['gst_number'] : '-' }}</p> -->
                        <!-- <p>
                            <strong>Certification Image:</strong> 
                            <i class="fa fa-image" style="color: #388E3C;"></i>
                            <a href="{{ isset($profile_data['company_certification_image_url']) ? $profile_data['company_certification_image_url'] : '-' }}">Comapny registration Cerificate</a>
                            
                            <a href="#" class="document_box docfile">
                                <i class="fa fa-file-word-o"></i>
                                <span>Company Doc file</span>
                            </a>
                        </p>

                        <p><strong>Pan Card Image:</strong> 
                            <i class="fa fa-image" style="color: #388E3C;"></i> 
                            <a href="{{ isset($profile_data['pancard_image_url']) ? $profile_data['pancard_image_url'] : '-' }}">Pancard</a>
                            
                            <a href="#" class="document_box gal_link">
                                <i class="fa fa-image"></i>
                                <span>Company Images</span>
                            </a>
                        </p>
                        <p><strong>GST Image:</strong> 
                            <i class="fa fa-image" style="color: #388E3C;"></i>
                            <a href="{{ isset($profile_data['gst_certification_image_url']) ? $profile_data['gst_certification_image_url'] : '-' }}">GST Certificate</a>
                            
                            <a href="#" class="document_box pdffile">
                                <i class="fa fa-file-pdf-o"></i>
                                <span>Company PDF file</span>
                            </a>
                        </p> -->
                     
                        <p><strong>Password :</strong> ******** 
                        @if(!isset($profile["master_vendor_id"]))
                            <a href="javascript:;" onclick="resetForm('update_password_form','change-password-popup')" data-toggle="modal" data-target="#change-password-popup" class="pull-right site-button-link blue">Change Password</a></p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div> 
                     

    <div class="my-profile-block">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="my-profile-content-block">
                    <div class="sub-title-block">
                        <h2 class="sub-title pull-left">Business Information </h2>
                        <!-- <a href="{{ route('supplier_profile_edit_view') }}" class="edit-profile-btn site-button outline"><i class="fa fa-edit"></i> Edit</a> -->
                    </div>
                    <div class="my-profile-details-block">
                        <!-- <p><strong>CST No :</strong> {{ isset($profile_data['cst']) ? $profile_data['cst'] : '-' }}</p>
                        <p><strong>HSE No :</strong> {{ isset($profile_data['hse_number']) ? $profile_data['hse_number'] : '-' }}</p> -->
                        <p><strong>Annual Turnover :</strong> {{ isset($profile_data['annual_turnover']) ? $profile_data['annual_turnover'] : '-' }}</p>
                        <p><strong>EHS Certificate No. :</strong> {{ isset($profile_data['ehs_as_per_iso']) ? $profile_data['ehs_as_per_iso'] : '-' }}</p>
                        <p><strong>Trade Capacity :</strong> {{ isset($profile_data['trade_capacity']) ? $profile_data['trade_capacity'] : '-' }}</p>
                        <p><strong>OHSAS Certificate No. :</strong> {{ isset($profile_data['ohsas_as_per_iso']) ? $profile_data['ohsas_as_per_iso'] : '-' }}</p>
                        <p><strong>No of Employees :</strong> {{ isset($profile_data['number_of_employee']) ? $profile_data['number_of_employee'] : '-' }}</p>
                        <p><strong>Working With Other e-commerce :</strong> {{ isset($profile_data['working_with_other_ecommerce']) ? ($profile_data['working_with_other_ecommerce'] == 'true' ? 'Yes' : 'No') : '-' }}</p>
                        <!-- <p><strong>Company Certification :</strong> {{ isset($profile_data['company_certification_number']) ? $profile_data['company_certification_number'] : '-' }}</p> -->
                        
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- my profile end -->
</div>

@endsection