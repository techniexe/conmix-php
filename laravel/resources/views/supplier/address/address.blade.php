@section('title', 'Plant Address')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Plant Address</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="address-detail-main-block wht-tble-bg">
        <div class="sub-title-block">
            <h2 class="sub-title pull-left">Plant Address</h2>
            <a href="{{ route('address_view') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
            @if(isset($data['data']) && count($data['data']) > 0)
                <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="address-detail-block add-address-detail-block">
                            <a href="{{ route('address_add') }}"><i>+</i></a>
                        </div>
                    </li>
                    @if(isset($data["data"]))
                    
                    @php
                        //dd($data["data"]);
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                        $lattitude = '';
                        $longitude = '';
                    @endphp
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                        @php
                            $lattitude = isset($value['location']['coordinates'][1]) ? $value['location']['coordinates'][1] : '';
                            $longitude = isset($value['location']['coordinates'][0]) ? $value['location']['coordinates'][0] : '';
                        @endphp
                            <li>
                                <a href="javascript:;" class="site-button gray view-map-icon" onclick="viewMap({{ $lattitude }}, {{ $longitude }})"><i class="fa fa-map-marker"></i></a>
                                <div class="address-detail-block p-a15">
                                    <div class="top-right-edit-link">
                                        <form action="{{ route('address_edit') }}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" value="{{ json_encode($value) }}" name="address_details" />
                                            <button class="site-button green button-sm"><i class="fa fa-pencil-square-o"></i></button>
                                        </form>
                                    </div>
                                    <p><span class="vTitle">Plant Name : </span> <span class="vName">{{ isset($value["business_name"]) ? $value["business_name"] : '' }}</span></p>
                                     
                                    <p><span class="vTitle">Address : </span> <span class="vName">{{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ isset($value["line2"]) ? $value["line2"] : '' }}</span></p>
                                    <!-- <p><span class="vTitle">Line 2 : </span> <span class="vName">{{ isset($value["line2"]) ? $value["line2"] : '' }}</span></p> -->
                                    <p><span class="vTitle">City : </span> <span class="vName">{{ isset($value["cityDetails"]["city_name"]) ? $value["cityDetails"]["city_name"] : '' }}</span></p>
                                    <p><span class="vTitle">State : </span> <span class="vName">{{ isset($value["stateDetails"]["state_name"]) ? $value["stateDetails"]["state_name"] : '' }}</span></p>
                                    
                                    <p><span class="vTitle">Pincode : </span> <span class="vName">{{ isset($value["pincode"]) ? $value["pincode"] : '' }}</span></p>
                                    <p><span class="vTitle">Address Type : </span> <span class="vName text-capitalize">{{ isset($value["address_type"]) ? $value["address_type"] : '' }}</span></p>
                                </div>
                            </li>
                        @endforeach
                    @endif
                    
                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection