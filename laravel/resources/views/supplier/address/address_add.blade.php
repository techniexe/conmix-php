@section('title', 'Add Plant Address')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Add Plant Address</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <?php 
        $billing = $data['billing_address']; 
        // dd($billing['billing_address']);
    ?>
    <div class="add-address-main-block wht-tble-bg">
        @if(isset($data["error"]))
        <div class="">{{ $data["error"]["message"] }}</div>
        @endif
        <form action="" name="add_address_form" method="post">
            @csrf
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Name <span class="str">*</span></label>
                                    <input type="text" name="business_name" class="form-control text-capitalize" placeholder="e.g. Ambika Plant">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- <div class="col-md-12 col-sm-12 col-xs-12"> -->

                            <div class="form-group">
                                <div class="input-group">
                                    <label>Search Location </label>
                                    <!-- <div class="location_field">
                                        <input type="text" class="form-control" placeholder="Enter city name">
                                        <button type="button" class="site-button umcl-btn">Use my current location</button>
                                    </div> -->
                                    <input type="text" placeholder="Search" required name="us2_address" id="us2_address" class="form-control" style="width: 200px" onkeydown="return event.key != 'Enter';"/>
                                </div>
                                <span class="error" id="address_location_error"></span>
                            </div>


                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Latitude</label>
                                            <input type="text" required name="us2_lat" id="us2_lat" class="form-control" readonly/>
                                            <p class="error" id="us2_lat_error"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Longitude</label>
                                            <input type="text" required name="us2_lon" id="us2_lon" class="form-control" readonly/>
                                            <p class="error" id="us2_lon_error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="mapnote">Please select location for accurate material pickup address.</div>
                                </div>
                            </div>
                            <div class="map-content-block">
                                <div id="us2" style=" height: 379px;"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Address Line 1 (Pickup Address) <span class="str">*</span></label>
                                    <input type="text" name="line1" id="address_line1" class="form-control text-capitalize" placeholder="e.g. Address line 1">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Address Line 2 (Pickup Address) <span class="str">*</span></label>
                                    <input type="text" name="line2" id="address_line2" class="form-control text-capitalize" placeholder="e.g. Address line 2">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group single-search-select2">
                                    <label>State Name <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                    
                                        <select id="add_site_state" readOnly name="state_id" class="form-control " data-placeholder="Select State Name">
                                            <option disabled="disabled" selected="selected">State</option>

                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group single-search-select2">
                                    <label>Select City Name <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        
                                        <select id="add_site_city" readOnly name="city_id" class="form-control" data-placeholder="Select City Name">
                                            <option disabled="disabled" selected="selected">City</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Pincode <span class="str">*</span></label>
                                    <input type="text" name="pincode" id="pincode" class="form-control" placeholder="e.g. 123456">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Address Type <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_type" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Address Type</option>
                                            <!-- <option value="warehouse">Warehouse</option>
                                            <option value="factory">Factory</option> -->
                                            <option value="quarry">Quarry</option>
                                            <!-- <option value="home">Home</option>
                                            <option value="office">Office</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Manager Full Name <span class="str">*</span></label>
                                    <input type="text" name="full_name" id="full_name" class="form-control text-capitalize" placeholder="e.g. John">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Mobile No. <span class="str">*</span></label>
                                    <input type="text" name="mobile" id="mobile_or_email" class="form-control" placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Email <span class="str">*</span></label>
                                    <input type="text" name="email" id="email" class="form-control" placeholder="e.g. johndoe@example.com">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Create Password <span class="str">*</span></label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="e.g. Enter Password">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group single-search-select2" id="billing_address_id_div">
                                    <label>Billing Address For This Plant <span class="str">*</span> (<b>Note:</b> <i>Be carefull to chose your billing address for this plant.</i>)</label>
                                    <div class="cstm-select-box">
                                        
                                        <select class="form-control" name="billing_address_id" id="billing_address_id_dropdown" class="form-control" data-placeholder="Select Billing Address">
                                            <option value="">Select Billing Address</option>                                            
                                                @if(isset($billing))
                                                    @foreach($billing as $value)
                                                    

                                                        <option value="{{ $value['_id'] }}" data-value="{{ json_encode($value) }}">{{ $value["company_name"] }}, {{ $value["line1"] }}, {{ isset($value["line2"]) ? $value["line2"] : '' }}, {{ isset($value["stateDetails"]["state_name"]) ? $value["stateDetails"]["state_name"] : '' }}, {{ $value["cityDetails"]["city_name"] }}</option>

                                                    @endforeach
                                                @endif
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Plant Capacity Per Hour (Cu.Mtr) <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="plant_capacity_per_hour" id="plant_capacity_per_hour">
                                                <option disabled="disabled" selected="selected">Select Plant Capacity</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                                <option value="60">60</option>
                                                <option value="75">75</option>
                                                <option value="120">120</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>No. of plants <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="no_of_plants" id="no_of_plants">
                                                <option disabled="disabled" selected="selected">Select No. of plants</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>No. Of Operating Hours <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="no_of_operation_hour" id="no_of_operation_hour">
                                                <option disabled="disabled" selected="selected">Select No. of operation hours</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label class="text-success" id="plant_capacity">Your total plant capacity is 0 Cu. Mtr./Day</label>
                                    </div>
                                    (<b>Note:</b> <i>Kindly note that you will not be able to accept orders beyond your plant capacity.</i>)
                                </div>
                                
                                
                            </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <p ><i><b>Note :</b> This plant will be treated as a Sub Partner Account under the Main Registered Account. Enter correct details such as Name, Email and Password. You shall be requires to login this Sub Partner Account with Email or Mobile No and Password to Operate this RMC Plant. </i></p>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="" value="add" id="add_address_form_type" />
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group m-t20">
                        <div class="input-group">
                            <button type="submit" class="site-button m-r10">Save</button>
                            <a href="{{ route('address_view') }}" class="site-button gray">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!--Contact Detail end -->


     <!--  <div class="form-group">
                                                <div class="input-group">

                                                     <div class="location_field">
                                                        <input type="text" class="form-control" placeholder="Enter city name">
                                                        <button type="button" class="site-button umcl-btn">Use my current location</button>
                                                    </div>
                                                   <input type="text" placeholder="Enter Your Location" required name="us2_address" id="us2_address" class="form-control" onkeydown="return event.key != 'Enter';"/ style=" margin-top: 15px;">
                                                </div>
                                            </div>
                                        <div class="map-content-block add-vehicle-map-block m-t20" style="height: 410px; margin-top: 0px;">

                                            <div id="us2" style="width: 100%; height: 400px;"></div>
                                        </div> -->

                                        <script>

                                            $('#us2').locationpicker({
                                                enableAutocomplete: true,
                                                enableReverseGeocode: true,
                                                radius: 0,
                                                inputBinding: {
                                                    latitudeInput: $('#us2_lat'),
                                                    longitudeInput: $('#us2_lon'),
                                                    radiusInput: $('#us2-radius'),
                                                    locationNameInput: $('#us2_address')
                                                },
                                                onchanged: function (currentLocation, radius, isMarkerDropped) {
                                                    // var addressComponents = $(this).locationpicker('map').location.addressComponents;
        console.log(currentLocation);  //latlon
        // updateControls(addressComponents); //Data
    }
});

                                            function updateControls(addressComponents) {
                                                console.log(addressComponents);
                                            }

                                        </script>

                                        @endsection

<!-- CP UnAvailable Module Over -->
<div id="Plant_otpModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="otpMessage">
        <label id="plant_OTP_label">Enter the 6 digits OTP sent on your given mobile no. and Email id <span id="plant_OTP_mob_no"></span><span class="str">*</span></label>
        <input type="text" id="plant_mobile_otp" required class="form-control" placeholder="Enter OTP sent on mobile">
        <p id="plant_mobile_otp_error" style="color:red;"></p>

        <input type="text" id="plant_email_otp" required class="form-control" placeholder="Enter OTP sent on Email">
        <p id="plant_email_otp_error" style="color:red;"></p>
        <div class="text-gray-dark resend_plant_otp_div text-left">
            <!-- <form action=""  name="" method="">
                @csrf -->
                <input class="form-control" name="" value="" required  type="hidden">
                <!-- <button type="submit" name="register_btn" value="register_btn" class="text-primary">Resend OTP</button> -->
                <!-- <div class="timer_otp mb-3">
                    <span id="timer_forgot_pass">
                        <span id="time_forgot_pass">02:00</span>
                    </span>
                </div> -->
            <!-- </form> -->
        </div>
      </div>
      <div class="modal-footer text-center">
          <button type="button" class="site-button green m-r15" id="plant_otpOk">Submit</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="plant_otpCancel">Cancel</button>
      </div>
    </div>
  </div>
</div>
