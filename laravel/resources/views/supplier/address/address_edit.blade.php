@section('title', 'Edit  Plant Address')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Edit Address</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="add-address-main-block wht-tble-bg">

        @if(isset($data["error"]))
        <div class="">{{ $data["error"]["message"] }}</div>
        @endif
        <form action="" name="add_address_form" method="post">
            @csrf
            @if(isset($data["data"]))
                @php
                    $all_data = $data["data"];
                @endphp
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Plant Name (Pickup Address)<span class="str">*</span></label>
                                        <input type="text" value="{{ isset($all_data->business_name) ? $all_data->business_name : '' }}" name="business_name" class="form-control text-capitalize" placeholder="e.g. ambika hardware">
                                    </div>
                                </div>
                            </div>
 
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Search Location</label>
                                        <!-- <div class="location_field">
                                            <input type="text" class="form-control" placeholder="Enter city name">
                                            <button type="button" class="site-button umcl-btn">Use my current location</button>
                                        </div> -->
                                        <input type="text" placeholder="Search" required name="us2_address" id="us2_address" class="form-control" style="width: 200px" onkeydown="return event.key != 'Enter';"/>
                                    </div>
                                </div>

                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Latitude</label>
                                            <input type="text" required name="us2_lat"  id="us2_lat" class="form-control" readonly/>
                                            <p class="error" id="us2_lat_error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Longitude</label>
                                        <input type="text" required name="us2_lon"  id="us2_lon" class="form-control" readonly/>
                                            <p class="error" id="us2_lon_error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="mapnote">Please select location for accurate material pickup address.</div>
                                    </div>
                            </div>
                            <div class="map-content-block">
                                <div id="us2" style=" height: 389px;"></div>
                            </div>
                            <script>
                                $('#us2').locationpicker({
                                    enableAutocomplete: true,
                                        enableReverseGeocode: true,
                                    radius: 0,
                                    inputBinding: {
                                        latitudeInput: $('#us2_lat'),
                                        longitudeInput: $('#us2_lon'),
                                        radiusInput: $('#us2-radius'),
                                        locationNameInput: $('#us2_address')
                                    },
                                    onchanged: function (currentLocation, radius, isMarkerDropped) {
                                            // var addressComponents = $(this).locationpicker('map').location.addressComponents;
                                        console.log(currentLocation);  //latlon
                                        // updateControls(addressComponents); //Data
                                        }
                                    });

                                    function updateControls(addressComponents) {
                                        console.log(addressComponents);
                                    }

                                    $('#us2').locationpicker("location", {latitude: '{{ $all_data->location->coordinates[1] }}', longitude: '{{ $all_data->location->coordinates[0] }}'});
                            </script>
                        </div>

                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Address Line 1 <span class="str">*</span></label>
                                        <input type="text" value="{{ isset($all_data->line1) ? $all_data->line1 : '' }}" name="line1" class="form-control text-capitalize" placeholder="e.g. address line 1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Address Line 2 <span class="str">*</span></label>
                                        <input type="text" value="{{ isset($all_data->line2) ? $all_data->line2 : '' }}" name="line2" class="form-control text-capitalize" placeholder="e.g. address line 2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group single-search-select2">
                                        <label>State Name <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <!-- <select class="form-control" readOnly id="state_name_city_address" name="state_id" class="form-control" data-placeholder="Select State Name"> -->
                                            <select class="form-control" readOnly id="add_site_state" name="state_id" class="form-control" data-placeholder="Select State Name">
                                                <option></option>
                                                @if(isset($data["state_data"]))
                                                    @foreach($data["state_data"] as $value)
                                                        @if($all_data->stateDetails->_id == $value['_id'])
                                                            <option value="{{ $value['_id'] }}" {{ $all_data->stateDetails->_id == $value['_id'] ? "selected" : '' }}>{{ $value["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group single-search-select2">
                                        <label>Select City Name <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <!-- <select class="form-control" readOnly id="state_name_city_filter" name="city_id" class="form-control" data-placeholder="Select city name"> -->
                                            <select class="form-control" readOnly id="add_site_city" name="city_id" class="form-control" data-placeholder="Select city name">
                                                <option></option>
                                                @if(isset($data["city_data"]))
                                                    @foreach($data["city_data"] as $value)
                                                        @if($all_data->cityDetails->_id == $value['_id'])
                                                            <option value="{{ $value['_id'] }}" {{ $all_data->cityDetails->_id == $value['_id'] ? "selected" : '' }}>{{ $value["city_name"] }}</option>
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Pincode <span class="str">*</span></label>
                                        <input type="text" name="pincode" id="pincode" value="{{ isset($all_data->pincode) ? $all_data->pincode : '' }}" class="form-control" placeholder="e.g. 123456">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Address Type <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select name="address_type" class="form-control">
                                                <option disabled="disabled" selected="selected">Select address type</option>
                                                <!-- <option value="warehouse" {{ $all_data->address_type == 'warehouse' ? "selected" : '' }}>warehouse</option>
                                                <option value="factory" {{ $all_data->address_type == 'factory' ? "selected" : '' }}>factory</option> -->
                                                <option value="quarry" {{ $all_data->address_type == 'quarry' ? "selected" : '' }}>Quarry</option>
                                                <!-- <option value="home" {{ $all_data->address_type == 'home' ? "selected" : '' }}>home</option>
                                                <option value="office" {{ $all_data->address_type == 'office' ? "selected" : '' }}>office</option> -->
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Plant Manager Full name <span class="str">*</span></label>
                                        <input type="text" name="full_name" id="full_name" value="{{ isset($all_data->vendorDetails->full_name) ? $all_data->vendorDetails->full_name : '' }}" class="form-control text-capitalize" placeholder="e.g. John">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Contact No. <span class="str">*</span></label>
                                        <input type="hidden" id="edit_address_mobile" value="{{ isset($all_data->vendorDetails->mobile_number) ? $all_data->vendorDetails->mobile_number : '' }}" />
                                        <input type="text" name="mobile" id="mobile_or_email" value="{{ isset($all_data->vendorDetails->mobile_number) ? $all_data->vendorDetails->mobile_number : '' }}" class="form-control" placeholder="e.g. John">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Email <span class="str">*</span></label>
                                        <input type="hidden" id="edit_address_email" value="{{ isset($all_data->vendorDetails->email) ? $all_data->vendorDetails->email : '' }}" />
                                        <input type="text" name="email" id="email" value="{{ isset($all_data->vendorDetails->email) ? $all_data->vendorDetails->email : '' }}" class="form-control" placeholder="e.g. John">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Create Password <span class="str">*</span></label>
                                        <input type="password" name="password" id="password" class="form-control" placeholder="e.g. *******">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group single-search-select2" id="billing_address_id_div">
                                        <label>Billing Address For This Plant<span class="str">*</span> (<b>Note:</b> <i>Be carefull to chose your billing address for this plant.</i>)</label>                                        
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="billing_address_id" id="billing_address_id_dropdown" class="form-control" data-placeholder="Select Billing Address">
                                                <option value="">Select Billing Address</option>
                                                @if(isset($data["billing_address"]))
                                                    @foreach($data["billing_address"] as $value)

                                                        <option value="{{ $value['_id'] }}" data-value="{{ json_encode($value) }}" {{ isset($all_data->billing_address_id) ? ($all_data->billing_address_id == $value['_id'] ? 'selected' : '') : ''  }}>{{ $value["company_name"] }}, {{ $value["line1"] }}, {{ isset($value["line2"]) ? $value["line2"] : '' }}, {{ isset($value["stateDetails"]["state_name"]) ? $value["stateDetails"]["state_name"] : '' }}, {{ $value["cityDetails"]["city_name"] }}</option>

                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Plant Capacity Per Hour (Cu.Mtr) <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="plant_capacity_per_hour" id="plant_capacity_per_hour">
                                                <option disabled="disabled" selected="selected">Select Plant Capacity</option>
                                                <option value="30" {{ isset($all_data->vendorDetails->plant_capacity_per_hour) ? (($all_data->vendorDetails->plant_capacity_per_hour == 30) ? 'selected' : '') : '' }}>30</option>
                                                <option value="45" {{ isset($all_data->vendorDetails->plant_capacity_per_hour) ? (($all_data->vendorDetails->plant_capacity_per_hour == 45) ? 'selected' : '') : '' }}>45</option>
                                                <option value="60" {{ isset($all_data->vendorDetails->plant_capacity_per_hour) ? (($all_data->vendorDetails->plant_capacity_per_hour == 60) ? 'selected' : '') : '' }}>60</option>
                                                <option value="75" {{ isset($all_data->vendorDetails->plant_capacity_per_hour) ? (($all_data->vendorDetails->plant_capacity_per_hour == 75) ? 'selected' : '') : '' }}>75</option>
                                                <option value="120" {{ isset($all_data->vendorDetails->plant_capacity_per_hour) ? (($all_data->vendorDetails->plant_capacity_per_hour == 120) ? 'selected' : '') : '' }}>120</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>No. of plants <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="no_of_plants" id="no_of_plants">
                                                <option disabled="disabled" selected="selected">Select No. of plants</option>
                                                <option value="1" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 1) ? 'selected' : '') : '' }}>1</option>
                                                <option value="2" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 2) ? 'selected' : '') : '' }}>2</option>
                                                <option value="3" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 3) ? 'selected' : '') : '' }}>3</option>
                                                <option value="4" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 4) ? 'selected' : '') : '' }}>4</option>
                                                <option value="5" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 5) ? 'selected' : '') : '' }}>5</option>
                                                <option value="6" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 6) ? 'selected' : '') : '' }}>6</option>
                                                <option value="7" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 7) ? 'selected' : '') : '' }}>7</option>
                                                <option value="8" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 8) ? 'selected' : '') : '' }}>8</option>
                                                <option value="9" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 9) ? 'selected' : '') : '' }}>9</option>
                                                <option value="10" {{ isset($all_data->vendorDetails->no_of_plants) ? (($all_data->vendorDetails->no_of_plants == 10) ? 'selected' : '') : '' }}>10</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>No. Of Operating Hours <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="no_of_operation_hour" id="no_of_operation_hour">
                                                <option disabled="disabled" selected="selected">Select No. of operation hours</option>
                                                <option value="1" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 1) ? 'selected' : '') : '' }}>1</option>
                                                <option value="2" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 2) ? 'selected' : '') : '' }}>2</option>
                                                <option value="3" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 3) ? 'selected' : '') : '' }}>3</option>
                                                <option value="4" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 4) ? 'selected' : '') : '' }}>4</option>
                                                <option value="5" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 5) ? 'selected' : '') : '' }}>5</option>
                                                <option value="6" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 6) ? 'selected' : '') : '' }}>6</option>
                                                <option value="7" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 7) ? 'selected' : '') : '' }}>7</option>
                                                <option value="8" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 8) ? 'selected' : '') : '' }}>8</option>
                                                <option value="9" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 9) ? 'selected' : '') : '' }}>9</option>
                                                <option value="10" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 10) ? 'selected' : '') : '' }}>10</option>
                                                <option value="11" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 11) ? 'selected' : '') : '' }}>11</option>
                                                <option value="12" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 12) ? 'selected' : '') : '' }}>12</option>
                                                <option value="13" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 13) ? 'selected' : '') : '' }}>13</option>
                                                <option value="14" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 14) ? 'selected' : '') : '' }}>14</option>
                                                <option value="15" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 15) ? 'selected' : '') : '' }}>15</option>
                                                <option value="16" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 16) ? 'selected' : '') : '' }}>16</option>
                                                <option value="17" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 17) ? 'selected' : '') : '' }}>17</option>
                                                <option value="18" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 18) ? 'selected' : '') : '' }}>18</option>
                                                <option value="19" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 19) ? 'selected' : '') : '' }}>19</option>
                                                <option value="20" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 20) ? 'selected' : '') : '' }}>20</option>
                                                <option value="21" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 21) ? 'selected' : '') : '' }}>21</option>
                                                <option value="22" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 22) ? 'selected' : '') : '' }}>22</option>
                                                <option value="23" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 23) ? 'selected' : '') : '' }}>23</option>
                                                <option value="24" {{ isset($all_data->vendorDetails->no_of_operation_hour) ? (($all_data->vendorDetails->no_of_operation_hour == 24) ? 'selected' : '') : '' }}>24</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label class="text-success" id="plant_capacity">
                                            <?php
                                            $capacity = 0;
                                            if(isset($all_data->vendorDetails->plant_capacity_per_hour) && isset($all_data->vendorDetails->no_of_operation_hour)){
                                                $capacity = ($all_data->vendorDetails->plant_capacity_per_hour * $all_data->vendorDetails->no_of_operation_hour);
                                            }

                                            ?>

                                            Your total plant capacity is {{ $capacity }} Cu. Mtr./Day
                                        </label>
                                    </div>
                                    (<b>Note:</b> <i>Kindly note that you will not be able to accept orders beyond your plant capacity.</i>)
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p ><i><b>Note :</b> This plant will be treated as a Sub Partner Account under the Main Registered Account. Enter correct details such as Name, Email and Password. You shall be requires to login this Sub Partner Account with Email or Mobile No and Password to Operate this RMC Plant. </i></p>
                            </div>
                    </div>
                    <input type="hidden" name="add_address_form_type" id="add_address_form_type" value="edit"/>
                    <input type="hidden" name="address_id" id="address_id" value="{{ $all_data->_id }}"/>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group m-t20">
                            <div class="input-group">
                                <button type="submit" class="site-button m-r10">Save</button>
                                <a href="{{ route('address_view') }}" class="site-button gray">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </form>

    </div>
    <!--Contact Detail end -->
</div>

<script>

    @if(isset($all_data->region_id))
        $('#select_region_id').val('<?php echo $all_data->region_id; ?>');



        $(window).on('load', function() {
            // code here
            $('#select_region_id').trigger("change");
        });

    @endif

</script>

@endsection

<div id="Plant_otpModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="otpMessage">
        <label id="plant_OTP_label">Enter the 6 digits OTP sent on your given mobile no. and Email id <span id="plant_OTP_mob_no"></span><span class="str">*</span></label>
        <input type="text" id="plant_mobile_otp" required class="form-control" placeholder="Enter OTP sent on mobile">
        <p id="plant_mobile_otp_error" style="color:red;"></p>

        <input type="text" id="plant_email_otp" required class="form-control" placeholder="Enter OTP sent on Email">
        <p id="plant_email_otp_error" style="color:red;"></p>
        <div class="text-gray-dark resend_plant_otp_div text-left">
            <!-- <form action=""  name="" method="">
                @csrf -->
                <input class="form-control" name="" value="" required  type="hidden">
                <!-- <button type="submit" name="register_btn" value="register_btn" class="text-primary">Resend OTP</button> -->
                <!-- <div class="timer_otp mb-3">
                    <span id="timer_forgot_pass">
                        <span id="time_forgot_pass">02:00</span>
                    </span>
                </div> -->
            <!-- </form> -->
        </div>
      </div>
      <div class="modal-footer text-center">
          <button type="button" class="site-button green m-r15" id="plant_otpOk">Submit</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="plant_otpCancel">Cancel</button>
      </div>
    </div>
  </div>
</div>