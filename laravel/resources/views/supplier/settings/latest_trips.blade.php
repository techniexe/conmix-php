@extends('logistics.layouts.logistics_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Order Reports</h1>
    <div class="clearfix"></div>
    <!--Latest Orders start-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="data-box wht-tble-bg">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Order List</h2>
                    <div class="filter-dd-block">
                        <!-- <span><i class="fa fa-filter"></i> Filter</span> -->
                        <div class="cstm-select-box">
                            <select class="form-control" id="order_status_report_filter">
                                <option disabled="disabled" selected="selected">Select Filter</option>
                                <option value="">All</option>
                                <option value="PROCESSING" {{ Request::get('delivery_status') == 'PROCESSING' ? 'selected' : ''  }}>PROCESSING</option>
                                <option value="PROCESSED" {{ Request::get('delivery_status') == 'PROCESSED' ? 'selected' : ''  }}>PROCESSED</option>
                                <option value="SHIPPED" {{ Request::get('delivery_status') == 'SHIPPED' ? 'selected' : ''  }}>SHIPPED</option>
                                <option value="DELIVERED" {{ Request::get('delivery_status') == 'DELIVERED' ? 'selected' : ''  }}>DELIVERED</option>
                                <option value="CANCELLED" {{ Request::get('delivery_status') == 'CANCELLED' ? 'selected' : ''  }}>CANCELLED</option>
                                <option value="FAILED" {{ Request::get('delivery_status') == 'FAILED' ? 'selected' : ''  }}>FAILED</option>
                                <option value="REFUNDED" {{ Request::get('delivery_status') == 'REFUNDED' ? 'selected' : ''  }}>REFUNDED</option>
                                <option value="PICKUP" {{ Request::get('delivery_status') == 'PICKUP' ? 'selected' : ''  }}>PICKUP</option>
                                <option value="INPROGRESS" {{ Request::get('delivery_status') == 'INPROGRESS' ? 'selected' : ''  }}>INPROGRESS</option>
                                <option value="CHECKOUT" {{ Request::get('delivery_status') == 'CHECKOUT' ? 'selected' : ''  }}>CHECKOUT</option>
                                <option value="DELAY" {{ Request::get('delivery_status') == 'DELAY' ? 'selected' : ''  }}>DELAY</option>
                                <option value="TRUCK_ASSIGNED" {{ Request::get('delivery_status') == 'TRUCK_ASSIGNED' ? 'selected' : ''  }}>TRUCK_ASSIGNED</option>
                                <option value="ACCEPTED" {{ Request::get('delivery_status') == 'ACCEPTED' ? 'selected' : ''  }}>ACCEPTED</option>
                                <option value="REJECTED" {{ Request::get('delivery_status') == 'REJECTED' ? 'selected' : ''  }}>REJECTED</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="data-box-body">
                    <div class="comn-table latest-order-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <!-- <th>Trip ID</th>
                                <th>Vehicle No</th>
                                <th>Pickup Location</th>
                                <th>Delivery Location</th>
                                <th>Quantity</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                                <th>Remark</th> -->
                                <th>Order Id</th>
                                <th>Category</th>
                                <!-- <th>Sub Category</th> -->
                                <!-- <th>Vehicle No</th> -->
                                <th>Pickup Location</th>
                                <th>Delivery Location</th>
                                <th>Qty</th>
                                <th>Order Date</th>
                                <!-- <th>Bill Status</th> -->
                                <th>Order Status</th>
                            </tr>
                        </thead>
                           <tbody>
                           @if(isset($data['data']) && count($data['data']) > 0)
                            @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @foreach($data["data"] as $value) 

                                @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp

                                <tr>
                                    <td><a href="{{ route('logistic_latest_trips_details', $value['_id']) }}" class="tc-orange">#{{ $value["_id"] }}</a></td>
                                    <td>{{ $value["product_category"]["category_name"] }} - {{ $value["product_sub_category"]["sub_category_name"] }}</td>
                                    <!-- <td>Dust Aggregate</td> -->
                                    <!-- <td>GJ/02/AB/0001</td> -->
                                    <td>{{ $value["pickup_address_city"]["city_name"] }}, {{ $value["pickup_address_state"]["state_name"] }}</td>
                                    <td>{{ $value["delivery_address_city"]["city_name"] }}, {{ $value["delivery_address_state"]["state_name"] }}</td>
                                    <td>{{ $value["quantity"] }} {{ $value["orderItem"][0]["quantity_unit"] }}</td>
                                    <td>{{ date('d M Y h:i:s a', strtotime($value["created_at"])) }}</td>
                                    <!-- <td></td> -->
                                    @if(isset($value["delivery_status"]))
                                        @if($value["delivery_status"] == 'DELIVERED')
                                            <td><div class="badge bg-green">{{ $value["delivery_status"] }}</div></td>
                                        @else @if($value["delivery_status"] == 'CANCELLED')
                                            <td><div class="badge bg-red">{{ $value["delivery_status"] }}</div></td>

                                            @else
                                                <td><div class="badge bg-yellow">{{ $value["delivery_status"] }}</div></td>
                                            @endif
                                        @endif
                                    @else
                                        <td>-</td>
                                    @endif
                            <!--       <td>
                                        <div class="bil-action-block">
                                            <div class="dropdown cstm-dropdown-select bid-rate-dropdown" style="display: block;">
                                                <a href="javascript:;" class="site-button gray dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Upload Bill</a>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li>
                                                        <form action="" method="get">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="file-browse">
                                                                        <span class="button-browse">
                                                                            Browse <input type="file">
                                                                        </span>
                                                                        <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button class="site-button blue">Upload</button>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="gallery" class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block" style="display: none;">
                                                <a href="images/bill.png" class="gal_link site-button blue button-sm" data-tooltip="View Bill"><i class="fa fa-eye"></i></a>
                                            </div>
                                            <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block" style="display: none;">
                                                <a href="javascript:;" class="site-button green button-sm" data-toggle="dropdown" aria-expanded="true" data-tooltip="Edit Bill"><i class="fa fa-edit"></i></a>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li>
                                                        <form action="" method="get">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="file-browse">
                                                                        <span class="button-browse">
                                                                            Browse <input type="file">
                                                                        </span>
                                                                        <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button class="site-button blue">Upload</button>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td> -->
                                <!--     <td>
                                        <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                            <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                <li>
                                                    <h3>Admin By :</h3>
                                                    <p>
                                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </td> -->
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="11">No Record Found</td>
                                </tr>
                            @endif
                            
                        </tbody>
                    </table>
                    </div>
                </div>
                <div class="data-box-footer clearfix">
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->


                    @if(isset($data['data']) && (count($data['data']) >=  ApiConfig::PAGINATION_LIMIT))
                    <div class="pagination justify-content-end m-0">
                            @if(isset($first_created_date))
                            <form action="{{ route('orders_filter') }}" method="get">
                                
                                <!-- @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif -->
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif 
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                
                                <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @if(isset($last_created_date))
                            <form action="{{ route('orders_filter') }}" method="get">
                                
                                
                                <!-- @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif -->
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                               
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif 
                                <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                        </div>
                    @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Latest Orders end-->
    
    <div class="clearfix"></div>
</div>


@endsection