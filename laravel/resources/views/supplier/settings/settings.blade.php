@section('title', 'Supplier Settings')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Rate Setting</h1>
    <div class="clearfix"></div>
    <div class="review-complaints-block wht-tble-bg">
    <?php

    $admixture_brand_data = $data["admixture_brand_data"];
    $admixture_settings = $data["admixture_settings"];
    $data = $data["data"];


    ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Enter Rate (<i class="fa fa-rupee"></i>) (Excl. GST)</h2>
                </div>
                <form name="setting_form">
                @csrf
                    <div class="rate_setting_block">
                        <div class="rate_setting_bx mb-3">
                            <h4>Rate For Transport (Excl. GST)</h4>
                            <div class="avlbl-sswich">
                                <div class="verified-switch-btn cstm-css-checkbox">
                                    <label class="new-switch1 switch-green">
                                        <input type="checkbox" name="with_TM" id="is_with_TM" class="switch-input" {{ isset($data['with_TM']) ? ($data['with_TM'] == true ? 'checked' : '') : '' }}>
                                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group m-b0">
                                <label>With Transit Mixer (Per Cu.Mtr/KM) (Excl. GST)</label>
                                <input type="text" value="{{ isset($data['TM_price']) ? $data['TM_price'] : 0 }}" name="TM_price" class="form-control" placeholder="Enter Price e.g. 500">
                            </div>

                            <script>

                                var with_TM_switch = '<?php echo $data['with_TM'] ?>';
                                // console.log("new_order_taken..."+new_order_taken);
                                if(with_TM_switch == 'true'){
                                    $("#is_with_TM").prop('checked',true);
                                }else{
                                    $("#is_with_TM").prop('checked',false);
                                }

                            </script>
                        </div>

                        <div class="rate_setting_bx mb-3">
                            <h4>Rate For Concrete Pump (Excl. GST)</h4>
                            <div class="avlbl-sswich">
                                <div class="verified-switch-btn cstm-css-checkbox">
                                    <label class="new-switch1 switch-green">
                                        <input type="checkbox" name="with_CP" id="is_with_CP" class="switch-input" {{ isset($data['with_CP']) ? ($data['with_CP'] == true ? 'checked' : '') : '' }}>
                                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group m-b0">
                                <label>With Concrete Pump (Per Cu.Mtr) (Excl. GST)</label>
                                <input type="text" value="{{ isset($data['CP_price']) ? $data['CP_price'] : 0 }}" name="CP_price" class="form-control" placeholder="Enter Price e.g. 500">
                            </div>

                            <script>

                                var with_CP_switch = '<?php echo $data['with_CP'] ?>';
                                // console.log("new_order_taken..."+new_order_taken);
                                if(with_CP_switch == 'true'){
                                    $("#is_with_CP").prop('checked',true);
                                }else{
                                    $("#is_with_CP").prop('checked',false);
                                }

                            </script>
                        </div>
                        <div class="rate_setting_bx mb-3">
                            <h4>Custom Design Mix Option Available </h4>
                            <div class="avlbl-sswich">
                                <div class="verified-switch-btn cstm-css-checkbox">
                                    <label class="new-switch1 switch-green">
                                        <input type="checkbox" name="is_customize_design_mix" id="is_customize_design_mix_switch" class="switch-input" {{ isset($data['is_customize_design_mix']) ? ($data['is_customize_design_mix'] == true ? 'checked' : '') : '' }}>
                                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>

                                <script>

                                    var with_custom_mix_switch = '<?php echo $data['is_customize_design_mix'] ?>';
                                    // console.log("new_order_taken..."+new_order_taken);
                                    if(with_custom_mix_switch == 'true'){
                                        $("#is_customize_design_mix_switch").prop('checked',true);
                                    }else{
                                        $("#is_customize_design_mix_switch").prop('checked',false);
                                    }

                                </script>
                            </div>
                        </div>
                        <p class="note_font"> <i><b>Note : </b> You will be able to cater custom design mix orders. You will be required to supply RMC as per Buyer's custome mix order. </i> <p>
                        <p class="note_font text-danger"><i><b>Note : </b> You will lose the custom mix RMC orders. We advice you to select 'Yes'  for maximum business at Conmix. </i></p>

                        <input type="hidden" name="setting_id"  value="{{ isset($data['_id']) ? $data['_id'] : '' }}"/>

                        <div class="input-group">
                            <button type="submit" class="site-button m-r10">Save</button>
                        </div>
                    </div>
                </form>
            </div>

            @if(false)
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form name="admixture_rate_setting_form">
                    @csrf
                        <div class="rate_setting_block mixer_rate_box">
                            <div class="sub-title-block">
                                <h2 class="sub-title pull-left">Admixture Rate Settings (Excl. GST)</h2>
                            </div>
                        <?php
                            // dd($admixture_settings["settings"]);

                            $admix_brand1_0_5_id = "";
                            $admix_category1_0_5_id = "";
                            $admix_category1_0_5_name = "";
                            $admix_brand2_0_5_id = "";
                            $admix_category2_0_5_id = "";
                            $admix_category2_0_5_name = "";
                            $rate_0_5_id = "";

                            $admix_brand1_6_10_id = "";
                            $admix_category1_6_10_id = "";
                            $admix_category1_6_10_name = "";
                            $admix_brand2_6_10_id = "";
                            $admix_category2_6_10_id = "";
                            $admix_category2_6_10_name = "";
                            $rate_6_10_id = "";

                            $admix_brand1_11_15_id = "";
                            $admix_category1_11_15_id = "";
                            $admix_category1_11_15_name = "";
                            $admix_brand2_11_15_id = "";
                            $admix_category2_11_15_id = "";
                            $admix_category2_11_15_name = "";
                            $rate_11_15_id = "";

                            $admix_brand1_16_20_id = "";
                            $admix_category1_16_20_id = "";
                            $admix_category1_16_20_name = "";
                            $admix_brand2_16_20_id = "";
                            $admix_category2_16_20_id = "";
                            $admix_category2_16_20_name = "";
                            $rate_16_20_id = "";

                            $admix_brand1_21_25_id = "";
                            $admix_category1_21_25_id = "";
                            $admix_category1_21_25_name = "";
                            $admix_brand2_21_25_id = "";
                            $admix_category2_21_25_id = "";
                            $admix_category2_21_25_name = "";
                            $rate_21_25_id = "";

                            $admix_brand1_26_30_id = "";
                            $admix_category1_26_30_id = "";
                            $admix_category1_26_30_name = "";
                            $admix_brand2_26_30_id = "";
                            $admix_category2_26_30_id = "";
                            $admix_category2_26_30_name = "";
                            $rate_26_30_id = "";
                        ?>

                    @if(isset($admixture_settings["settings"]))
                        @foreach($admixture_settings["settings"] as $admix_value)
                            @if(($admix_value["min_km"] == 0) && ($admix_value["max_km"] == 5))
                                <?php
                                    $admix_brand1_0_5_id = $admix_value["ad_mixture1_brand_id"];
                                    $admix_category1_0_5_id = $admix_value["ad_mixture1_category_id"];
                                    $admix_category1_0_5_name = $admix_value["ad_mixture1_category_name"];
                                    $admix_brand2_0_5_id = $admix_value["ad_mixture2_brand_id"];
                                    $admix_category2_0_5_id = $admix_value["ad_mixture2_category_id"];
                                    $admix_category2_0_5_name = $admix_value["ad_mixture2_category_name"];
                                    $rate_0_5_id = $admix_value["price"];
                                ?>
                            @endif

                            @if(($admix_value["min_km"] == 6) && ($admix_value["max_km"] == 10))

                                <?php
                                    $admix_brand1_6_10_id = $admix_value["ad_mixture1_brand_id"];
                                    $admix_category1_6_10_id = $admix_value["ad_mixture1_category_id"];
                                    $admix_category1_6_10_name = $admix_value["ad_mixture1_category_name"];
                                    $admix_brand2_6_10_id = $admix_value["ad_mixture2_brand_id"];
                                    $admix_category2_6_10_id = $admix_value["ad_mixture2_category_id"];
                                    $admix_category2_6_10_name = $admix_value["ad_mixture2_category_name"];
                                    $rate_6_10_id = $admix_value["price"];
                                ?>

                            @endif

                            @if(($admix_value["min_km"] == 11) && ($admix_value["max_km"] == 15))

                                <?php
                                    $admix_brand1_11_15_id = $admix_value["ad_mixture1_brand_id"];
                                    $admix_category1_11_15_id = $admix_value["ad_mixture1_category_id"];
                                    $admix_category1_11_15_name = $admix_value["ad_mixture1_category_name"];
                                    $admix_brand2_11_15_id = $admix_value["ad_mixture2_brand_id"];
                                    $admix_category2_11_15_id = $admix_value["ad_mixture2_category_id"];
                                    $admix_category2_11_15_name = $admix_value["ad_mixture2_category_name"];
                                    $rate_11_15_id = $admix_value["price"];
                                ?>

                            @endif

                            @if(($admix_value["min_km"] == 16) && ($admix_value["max_km"] == 20))

                                <?php
                                    $admix_brand1_16_20_id = $admix_value["ad_mixture1_brand_id"];
                                    $admix_category1_16_20_id = $admix_value["ad_mixture1_category_id"];
                                    $admix_category1_16_20_name = $admix_value["ad_mixture1_category_name"];
                                    $admix_brand2_16_20_id = $admix_value["ad_mixture2_brand_id"];
                                    $admix_category2_16_20_id = $admix_value["ad_mixture2_category_id"];
                                    $admix_category2_16_20_name = $admix_value["ad_mixture2_category_name"];
                                    $rate_16_20_id = $admix_value["price"];
                                ?>

                            @endif

                            @if(($admix_value["min_km"] == 21) && ($admix_value["max_km"] == 25))

                                <?php
                                    $admix_brand1_21_25_id = $admix_value["ad_mixture1_brand_id"];
                                    $admix_category1_21_25_id = $admix_value["ad_mixture1_category_id"];
                                    $admix_category1_21_25_name = $admix_value["ad_mixture1_category_name"];
                                    $admix_brand2_21_25_id = $admix_value["ad_mixture2_brand_id"];
                                    $admix_category2_21_25_id = $admix_value["ad_mixture2_category_id"];
                                    $admix_category2_21_25_name = $admix_value["ad_mixture2_category_name"];
                                    $rate_21_25_id = $admix_value["price"];
                                ?>


                            @endif

                            @if(($admix_value["min_km"] == 26) && ($admix_value["max_km"] == 30))

                                <?php
                                    $admix_brand1_26_30_id = $admix_value["ad_mixture1_brand_id"];
                                    $admix_category1_26_30_id = $admix_value["ad_mixture1_category_id"];
                                    $admix_category1_26_30_name = $admix_value["ad_mixture1_category_name"];
                                    $admix_brand2_26_30_id = $admix_value["ad_mixture2_brand_id"];
                                    $admix_category2_26_30_id = $admix_value["ad_mixture2_category_id"];
                                    $admix_category2_26_30_name = $admix_value["ad_mixture2_category_name"];
                                    $rate_26_30_id = $admix_value["price"];
                                ?>

                            @endif
                        @endforeach
                    @endif
                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <div class="rate_setting_bx mb-3">
                                        <h4>Less than 5 Km </h4>
                                        <input type="hidden" name="less_than_5_min_km" value="0" />
                                        <input type="hidden" name="less_than_5_max_km" value="5" />
                                        <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                    <label>Admixture Brand 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="less_than_5_ad_mixture_brand_id_1" id="less_than_5_ad_mixture_brand_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            <?php //dd($data); ?>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand1_0_5_id == $value['_id'] ? 'selected' : ''  }} >{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="less_than_5_ad_mixture_category_id_1" id="less_than_5_ad_mixture_category_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category1_0_5_id) && !empty($admix_category1_0_5_id))
                                                                <option value="{{ $admix_category1_0_5_id }}" selected="selected">{{ $admix_category1_0_5_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="less_than_5_ad_mixture_brand_id_2" id="less_than_5_ad_mixture_brand_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand2_0_5_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="less_than_5_ad_mixture_category_id_2" id="less_than_5_ad_mixture_category_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category2_0_5_id) && !empty($admix_category2_0_5_id))
                                                                <option value="{{ $admix_category2_0_5_id }}" selected="selected">{{ $admix_category2_0_5_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Rate For Admixture (<i class="fa fa-rupee"></i>/Kg) (Excl. GST)</label>
                                            <input type="text" value="{{ $rate_0_5_id }}" name="less_than_5_rate" class="form-control" placeholder="Enter Price e.g. 500">
                                        </div>

                                    </div>
                                </div>



                                <div class="col-md-4 col-sm-6">
                                    <div class="rate_setting_bx mb-3">
                                        <h4>6 Km - 10 Km</h4>
                                        <input type="hidden" name="5_10_min_km" value="6" />
                                        <input type="hidden" name="5_10_max_km" value="10" />
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 1</label>
                                                    <div class="cstm-select-box">
                                                    <select class="form-control" name="ad_5_10_ad_mixture_brand_id_1" id="5_10_ad_mixture_brand_id_1">
                                                        <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                        @if(isset($admixture_brand_data))
                                                            @foreach($admixture_brand_data as $value)
                                                                <option value="{{ $value['_id'] }}" {{ $admix_brand1_6_10_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_5_10_ad_mixture_category_id_1" id="5_10_ad_mixture_category_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category1_6_10_id) && !empty($admix_category1_6_10_id))
                                                                <option value="{{ $admix_category1_6_10_id }}" selected="selected">{{ $admix_category1_6_10_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_5_10_ad_mixture_brand_id_2" id="5_10_ad_mixture_brand_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand2_6_10_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_5_10_ad_mixture_category_id_2" id="5_10_ad_mixture_category_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category2_6_10_id) && !empty($admix_category2_6_10_id))
                                                                <option value="{{ $admix_category2_6_10_id }}" selected="selected">{{ $admix_category2_6_10_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Rate For Admixture (<i class="fa fa-rupee"></i>/Kg) (Excl. GST)</label>
                                            <input type="text" value="{{ $rate_6_10_id }}" name="ad_5_10_rate" class="form-control" placeholder="Enter Price e.g. 500">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-4 col-sm-6">
                                    <div class="rate_setting_bx mb-3">
                                        <h4>11 Km - 15 Km </h4>
                                        <input type="hidden" name="11_15_min_km" value="11" />
                                        <input type="hidden" name="11_15_max_km" value="15" />
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_11_15_ad_mixture_brand_id_1" id="11_15_ad_mixture_brand_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand1_11_15_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_11_15_ad_mixture_category_id_1" id="11_15_ad_mixture_category_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category1_11_15_id) && !empty($admix_category1_11_15_id))
                                                                <option value="{{ $admix_category1_11_15_id }}" selected="selected">{{ $admix_category1_11_15_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_11_15_ad_mixture_brand_id_2" id="11_15_ad_mixture_brand_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand2_11_15_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_11_15_ad_mixture_category_id_2" id="11_15_ad_mixture_category_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category2_11_15_id) && !empty($admix_category2_11_15_id))
                                                                <option value="{{ $admix_category2_11_15_id }}" selected="selected">{{ $admix_category2_11_15_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Rate For Admixture (<i class="fa fa-rupee"></i>/Kg) (Excl. GST)</label>
                                            <input type="text" value="{{ $rate_11_15_id }}" name="ad_11_15_rate" class="form-control" placeholder="Enter Price e.g. 500">
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-4 col-sm-6">
                                    <div class="rate_setting_bx mb-3">
                                        <h4>16 Km - 20 Km </h4>
                                        <input type="hidden" name="16_20_min_km" value="16" />
                                        <input type="hidden" name="16_20_max_km" value="20" />
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_16_20_ad_mixture_brand_id_1" id="16_20_ad_mixture_brand_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand1_16_20_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_16_20_ad_mixture_category_id_1" id="16_20_ad_mixture_category_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category1_16_20_id) && !empty($admix_category1_16_20_id))
                                                                <option value="{{ $admix_category1_16_20_id }}" selected="selected">{{ $admix_category1_16_20_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_16_20_ad_mixture_brand_id_2" id="16_20_ad_mixture_brand_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand2_16_20_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_16_20_ad_mixture_category_id_2" id="16_20_ad_mixture_category_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category2_16_20_id) && !empty($admix_category2_16_20_id))
                                                                <option value="{{ $admix_category2_16_20_id }}" selected="selected">{{ $admix_category2_16_20_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Rate For Admixture (<i class="fa fa-rupee"></i>/Kg) (Excl. GST)</label>
                                            <input type="text" value="{{ $rate_16_20_id }}" name="ad_16_20_rate" class="form-control" placeholder="Enter Price e.g. 500">
                                        </div>

                                    </div>
                                </div>



                                <div class="col-md-4 col-sm-6">
                                    <div class="rate_setting_bx mb-3">
                                        <h4>21 Km - 25 Km </h4>
                                        <input type="hidden" name="21_25_min_km" value="21" />
                                        <input type="hidden" name="21_25_max_km" value="25" />
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_21_25_ad_mixture_brand_id_1" id="21_25_ad_mixture_brand_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand1_21_25_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_21_25_ad_mixture_category_id_1" id="21_25_ad_mixture_category_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category1_21_25_id) && !empty($admix_category1_21_25_id))
                                                                <option value="{{ $admix_category1_21_25_id }}" selected="selected">{{ $admix_category1_21_25_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_21_25_ad_mixture_brand_id_2" id="21_25_ad_mixture_brand_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand2_21_25_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_21_25_ad_mixture_category_id_2" id="21_25_ad_mixture_category_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category2_21_25_id) && !empty($admix_category2_21_25_id))
                                                                <option value="{{ $admix_category2_21_25_id }}" selected="selected">{{ $admix_category2_21_25_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Rate For Admixture (<i class="fa fa-rupee"></i>/Kg) (Excl. GST)</label>
                                            <input type="text" value="{{ $rate_21_25_id }}" name="ad_21_25_rate" class="form-control" placeholder="Enter Price e.g. 500">
                                        </div>

                                    </div>
                                </div>



                                <div class="col-md-4 col-sm-6">
                                    <div class="rate_setting_bx mb-3">
                                        <h4>26 Km - 30 Km </h4>
                                        <input type="hidden" name="26_30_min_km" value="26" />
                                        <input type="hidden" name="26_30_max_km" value="30" />
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_26_30_ad_mixture_brand_id_1" id="26_30_ad_mixture_brand_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand1_26_30_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 1</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_26_30_ad_mixture_category_id_1" id="26_30_ad_mixture_category_id_1">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category1_26_30_id) && !empty($admix_category1_26_30_id))
                                                                <option value="{{ $admix_category1_26_30_id }}" selected="selected">{{ $admix_category1_26_30_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Brand 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_26_30_ad_mixture_brand_id_2" id="26_30_ad_mixture_brand_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                            @if(isset($admixture_brand_data))
                                                                @foreach($admixture_brand_data as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $admix_brand2_26_30_id == $value['_id'] ? 'selected' : ''  }}>{{ $value["name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Admixture Code 2</label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="ad_26_30_ad_mixture_category_id_2" id="26_30_ad_mixture_category_id_2">
                                                            <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                            @if(isset($admix_category2_26_30_id) && !empty($admix_category2_26_30_id))
                                                                <option value="{{ $admix_category2_26_30_id }}" selected="selected">{{ $admix_category2_26_30_name }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Rate For Admixture (<i class="fa fa-rupee"></i>/Kg) (Excl. GST)</label>
                                            <input type="text" value="{{ $rate_26_30_id }}" name="ad_26_30_rate" class="form-control" placeholder="Enter Price e.g. 500">
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <p class="note_font"> <i><b>Note : </b> You will be able to take RMC supply orders within 30 km distance from your RMC plant location.</i> </p>

                            <input type="hidden" name="ad_mix_setting_id"  value="{{ isset($admixture_settings['_id']) ? $admixture_settings['_id'] : ''}}"/>


                            <div class="input-group">
                                <button type="submit" class="site-button m-r10">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        </div>
    </div>

</div>

@endsection