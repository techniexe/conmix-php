<!-- Dashboard -->

<!-- ALERT MESSAGE START -->
<div id="accept-alert-popup" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15">
        Are you sure you want to accept bill?
      </div>
      <div class="modal-footer text-center">
          <button type="button" data-dismiss="modal" class="site-button green m-r15">Yes</button>
          <button type="button" data-dismiss="modal" class="site-button red">No</button>
      </div>
    </div>
  </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- ALERT MESSAGE START -->
<div id="billing-addr-state-alert-popup" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15">
        Your selected billing address and plant address are from a different state.
        Are you sure you want to continue?
        If 'Yes' then IGST billing may be applicable.
      </div>
      <div class="modal-footer text-center">
          <button type="button" data-dismiss="modal" class="site-button green m-r15">Yes</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="billing-addr-state-check-cancel">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- ALERT MESSAGE START -->
<div id="approve-alert-popup" class="alert-message modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body p-tb20 p-lr15">
            Are you sure you want to approve?
        </div>
        <div class="modal-footer text-center">
            <button type="button" data-dismiss="modal" class="site-button green m-r15">Yes</button>
            <button type="button" data-dismiss="modal" class="site-button red">No</button>
        </div>
        </div>
    </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- Dashboard Over -->

<!-- Confirmation Popup MESSAGE START -->
<div id="confirmModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="confirmMessage">
        Are you sure you want to accept bill?
      </div>
      <div class="modal-footer text-center">
          <button type="button" data-dismiss="modal" class="site-button green m-r15" id="confirmOk">Yes</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="confirmCancel">No</button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation Popup MESSAGE END -->

<!-- Confirmation Popup MESSAGE START -->
<div id="otpModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="otpMessage">
        <label id="OTP_label">Enter the 6 digits OTP sent on your registered mobile no. <span id="OTP_mob_no"></span><span class="str">*</span></label>
        <input type="text" id="otp" required class="form-control" placeholder="Enter OTP">
        <p id="otp_error" style="color:red;"></p>
        <div class="text-gray-dark resend_otp_div text-left">
            <!-- <form action=""  name="" method="">
                @csrf -->
                <input class="form-control" name="" value="" required  type="hidden">
                <!-- <button type="submit" name="register_btn" value="register_btn" class="text-primary">Resend OTP</button> -->
                <div class="timer_otp mb-3">
                    <span id="timer_forgot_pass">
                        <span id="time_forgot_pass">02:00</span>
                    </span>
                </div>
            <!-- </form> -->
        </div>
      </div>
      <div class="modal-footer text-center">
          <button type="button" class="site-button green m-r15" id="otpOk">Submit</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="otpCancel">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation Popup MESSAGE END -->

<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>



<!-- Contact Details Module Start -->
@if(Route::current()->getName() == 'contact_details')
<!-- MODAL ADD CONTACT DETAILS START-->
<div id="add-contact-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_contact_details_title">Add Contact Details</h4>
        </div>
        <form action="" name="add_contact_detail_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Contact Person Name <span class="str">*</span></label>
                                    <input type="text" name="person_name" id="person_name" class="form-control text-capitalize" placeholder="e.g. Johndoe">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Designation <span class="str">*</span></label>
                                    <input type="text" name="title" id="title" class="form-control text-capitalize" placeholder="e.g. Owner , Employee">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="mobile_number_div">
                                    <label>Mobile No. <span class="str">*</span></label>
                                    <input name="mobile_number" class="form-control mobile_validate" value="" id="person_mobile_no" placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Email <span class="str">*</span></label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="e.g. johndoe@example.com">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="alt_mobile_number_div">
                                    <label>Alternate No.</label>
                                    <input name="alt_mobile_number" class="form-control mobile_validate" value="" id="person_alt_no" placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="whatsapp_number_div">
                                    <label>Whatsapp No.</label>
                                    <input name="whatsapp_number" class="form-control mobile_validate" value="" id="person_whats_no" placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Landline No.</label>
                                    <input type="text" name="landline_number" id="landline_number" class="form-control" placeholder="e.g. 0791234567">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>

            <input type="hidden" name="add_contact_detail_form_type" id="add_contact_detail_form_type" value="add"/>
              <input type="hidden" name="contact_detail_id" id="contact_detail_id" value=""/>

            <div class="modal-footer text-left">
                <button type="submit" class="site-button">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD CONTACT DETAILS END-->
@endif
<!-- Contact Details Module Over -->

<!-- Bank Details Module Start -->

@if(Route::current()->getName() == 'bank_details')

<!-- ALERT MESSAGE START -->
<div id="make-default-alert-popup" class="alert-message modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body p-tb20 p-lr15">
            Are you sure to make this account as default?
        </div>
        <div class="modal-footer text-center">
            <input type="hidden" id="make_default_bank_detail_id"/>
            <input type="hidden" id="make_default_bank_detail_csrf"/>
            <button type="button" onclick="setBankAccountDefault()" class="site-button m-r15">Yes</button>
            <button type="button" data-dismiss="modal" class="site-button gray">No</button>
        </div>
        </div>
    </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-bank-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_bank_details_title">Add Bank Details</h4>
          
        </div>
        <form action="" name="add_bank_detail_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Account Holder Name <span class="str">*</span></label>
                                    <input type="text" name="account_holder_name" id="account_holder_name" class="form-control text-capitalize" placeholder="e.g. johndoe">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Bank Name <span class="str">*</span></label>
                                    <input type="text" name="bank_name" id="bank_name" class="form-control text-capitalize" placeholder="e.g. ICICI">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Account Number <span class="str">*</span></label>
                                    <input type="text" name="account_number" id="account_number" class="form-control" placeholder="e.g. 0000123456789">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>IFSC Code <span class="str">*</span></label>
                                    <input type="text" name="ifsc" id="ifsc" class="form-control" placeholder="e.g. SBI000123">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Branch Name <span class="str">*</span></label>
                                    <input type="text" name="branch_name" id="branch_name" class="form-control" placeholder="e.g. Branch Name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Account Type <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="account_type" id="account_type" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Account Type</option>
                                            <option value="current">Current</option>
                                            <option value="saving">Saving</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group m-b0">
                                <div class="input-group">
                                    <label>Cancelled Cheque Image</label>
                                    <input type="file" name="cancelled_cheque_image" id="cancelled_cheque_image" >
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="cancelled_cheque_image_div">
                                    <label>Cancelled Cheque Image <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="cancelled_cheque_image" id="cancelled_cheque_image" />
                                        </span>
                                        <input type="text" id="cancelled_cheque_image_text" class="form-control browse-input"  placeholder="e.g. jpg, jpeg" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        </div>
                </div>
                <div><b>Note:</b> <i>Kindly add current bank details</i></div>
            </div>
            <input type="hidden" name="add_bank_detail_form_type" id="add_bank_detail_form_type" value="add"/>
              <input type="hidden" name="bank_detail_id" id="bank_detail_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif

<!-- Bank Details Module Over -->

<!-- Product Module Start -->

@if(Route::current()->getName() == 'supplier_product')

<!-- MODAL ADD PRODUCT START-->
<div id="add-product-list" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_product_list_title">Add Product</h4>
        </div>
        <form action="" name="add_product_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Product Name <span class="str">*</span></label>
                                    <input type="text" name="product_name" id="product_name" class="form-control" placeholder="e.g. superflow jointagg">
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Category <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select class="form-control" name="category_id" id="category_id">
                                            <option disabled="disabled" selected="selected">Select Category</option>
                                            @if(isset($data["category"]))
                                                @foreach($data["category"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["category_name"] }}</option>
                                                @endforeach
                                            @endif
                                            <option>Sand</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Sub Category <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select class="form-control" name="sub_category_id" id="sub_category_id">

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Unit Price ( <i class="fa fa-rupee"></i> ) <span class="str">*</span></label>
                                    <input type="text" name="unit_price" id="unit_price" class="form-control" placeholder="e.g. 1000">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Qty In Stock (MT) <span class="str">*</span></label>
                                    <input type="text" name="quantity" id="product_qty" class="form-control" placeholder="e.g. 100 MT">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Min. Order Acceptance (MT) <span class="str">*</span></label>
                                    <input type="text" name="minimum_order" id="minimum_order" class="form-control" placeholder="e.g. 1 MT">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Contact Person <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select class="form-control" name="contact_person_id" id="contact_person">
                                            <option disabled="disabled" selected="selected">Select Contact Person</option>
                                            @if(isset($data["contact_data"]))
                                                @foreach($data["contact_data"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["person_name"] }} - ({{ $value["mobile_number"] }})</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Pickup Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select class="form-control" name="pickup_address_id" id="contact_address">
                                            <option disabled="disabled" selected="selected">Select Address</option>
                                            @if(isset($data["address_data"]))
                                                @foreach($data["address_data"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Upload Image <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse"> Browse <input type="file"></span>
                                        <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group avlbl-sswich m-t15">
                                    <label class="wid-60">Material Available :</label>
                                    <div class="verified-switch-btn cstm-css-checkbox">
                                        <label class="new-switch1 switch-green">
                                            <input type="checkbox" name="is_available" id="is_product_avail" class="switch-input" checked="">
                                            <span class="switch-label" data-on="Yes" data-off="No"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                              <div class="input-group avlbl-sswich m-t15">
                                  <label class="wid-60">Self Logistics :</label>
                                  <div class="verified-switch-btn cstm-css-checkbox">
                                      <label class="new-switch1 switch-green">
                                          <input type="checkbox" name="self_logistics" id="is_self_logistics" class="switch-input">
                                          <span class="switch-label" data-on="Yes" data-off="No"></span>
                                          <span class="switch-handle"></span>
                                      </label>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <input type="hidden" name="add_product_form_type" id="add_product_form_type" value="add"/>
                        <input type="hidden" name="add_product_id" id="add_product_id" value=""/>

                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD PRODUCT END-->


@endif

<!-- Product Module Over -->

<!-- Support Ticket Module Start -->

@if(Route::current()->getName() == 'support_ticket_view')

<!-- MODAL CREATE TICKET START-->
<div id="create-ticket-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Create Ticket</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15">
            <form action="" name="add_support_ticket_form" method="post" enctype="multipart/form-data">
            @csrf

                    <div class="compose-message-block">

                            <div class="row">

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Type Of Issue <span class="str">*</span></label>
                                            <div class="cstm-select-box">
                                                <select name="question_type" class="form-control">
                                                    <option disabled="disabled" selected="selected">Select Question</option>
                                                    <!-- <option value="Problem on payment">Problem on payment</option>
                                                    <option value="Problem on product">Problem on product</option>
                                                    <option value="Enable to order">Enable to order</option> -->
                                                    <option value="RMC Product listing display query">RMC Product listing display query</option>
                                                    <option value="Query related to Cement Brand, Admixture Brand, Type of Cement">Query related to Cement Brand, Admixture Brand, Type of Cement</option>
                                                    <option value="Query related to Source of Aggregates,Sand, Fly Ash">Query related to Source of Aggregates,Sand, Fly Ash</option>
                                                    <option value="Understnding of Conmix RMC Solution ">Understnding of Conmix RMC Solution </option>
                                                    <option value="Query related to type & Capacity of Transit Mixer">Query related to type & Capacity of Transit Mixer</option>
                                                    <option value="Query related to Category of Concrete Pump">Query related to Category of Concrete Pump</option>
                                                    <option value="Query related to Conmix Tracking App for Driver">Query related to Conmix Tracking App for Driver</option>
                                                    <option value="Dispute with Customer">Dispute with Customer</option>
                                                    <option value="Query related to Tax Invoice or Debit Note">Query related to Tax Invoice or Debit Note</option>
                                                    <option value="Query related to Damage/Accident/Theft of TM or Concrete Pump">Query related to Damage/Accident/Theft of TM or Concrete Pump</option>
                                                    <option value="Query related to incorrect delivery address or delivery address not found">Query related to incorrect delivery address or delivery address not found</option>
                                                    <option value="Query related to Payment">Query related to Payment</option>
                                                    <option value="Query related to Conmix E-commerce Software System">Query related to Conmix E-commerce Software System</option>
                                                    <option value="Query related to RMC Supplier Price vs Conmix RMC display price">Query related to RMC Supplier Price vs Conmix RMC display price</option>
                                                    <option value="Suggestions related to Conmix E-Commerce Service Terms & Conditions">Suggestions related to Conmix E-Commerce Service Terms & Conditions</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Priority  </label>
                                            <div class="cstm-select-box">
                                                <select name="severity" class="form-control">
                                                    <option disabled="disabled" selected="selected">Select Priority</option>
                                                    <option value="Urgent">Urgent</option>
                                                    <option value="High">High</option>
                                                    <option value="Low">Normal</option>
                                                    <option value="Normal">Low</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Subject <span class="str">*</span></label>
                                            <div class="cstm-input">
                                                <textarea name="subject" class="form-control text-capitalize" id="ctsubject" placeholder="Brief summary of the question or issue" data-maxchar="250"></textarea>
                                                <span class="character-text">Maximum 250 Characters (<span class="character-counter"></span> remains)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Description <span class="str">*</span></label>
                                            <div class="cstm-input">
                                                <textarea name="description" class="form-control text-capitalize" id="ctdescription" placeholder="Detailed of the question or issue" data-maxchar="5000"></textarea>
                                                <span class="character-text">Maximum 5000 Characters (<span class="character-counter"></span> remaining)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group" id="support_ticket_attachment_div">
                                            <label>Attachment </label>
                                            <div class="upload-file-block mt-0">
                                                <input type="file" name="attachments">
                                            </div>
                                            <!-- <span class="file-note-text">Up to 3 attachment, each less than 5MB.</span> -->
                                            <span class="file-note-text">Use one of the jpg, jpeg, mp4, doc, docx, pdf file format</span>
                                            <span class="file-error-text" style="display: none;"><i class="ion-alert-circled"></i> Maximum amount of files exceeded!</span>
                                        </div>
                                    </div>

                                    <input type="hidden" name="order_id" />
                                </div>

                            </div>

                    </div>
                </div>
                <div class="modal-footer text-left">
                    <button type="submit" class="site-button pull-left">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL CREATE TICKET END-->



<!-- MODAL RESOLVE ALERT START-->
<div id="resolve-alertmsg" class="alert-message modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body p-tb40 p-lr15">
            Thank you for contacting customer support
        </div>
        <div class="modal-footer text-center">
            <button type="button" data-dismiss="modal" class="site-button">Ok</button>
        </div>
        </div>
    </div>
</div>
<!-- MODAL RESOLVE ALERT END-->


@endif

<!-- Support Ticket Module Over -->

<!-- MODAL VIEW MAP START-->
<div id="view-map-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header p-a0 border-0">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body p-tb20 p-lr15">
            <div class="popup-map-content-block" id="map_iframe_div">
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5864.520301024851!2d72.51392826831795!3d23.048447656590255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e9cb1d6441583%3A0x99c9230212282810!2sSCC+Infrastructure+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1551245667065" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe> -->

            </div>
        </div>
        </div>
    </div>
</div>
<!-- MODAL VIEW MAP END-->

<!-- Profile Module Start -->

@if((Route::current()->getName() == 'supplier_profile_edit_view')  || (Route::current()->getName() == 'supplier_profile'))

<!-- MODAL EDIT PROFILE START-->
<div id="edit-profile-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Edit Profile</h4>
        </div>
        <form action="" name="update_profile_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                <div class="form-group">
                    <div class="input-group">
                        <label>Full Name</label>
                        <input type="text" name="full_name" id="full_name" class="form-control text-capitalize" value="" placeholder="e.g. jhon">
                    </div>
                </div>
                <!-- <div class="form-group">
                    <div class="input-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" value="Doe" placeholder="e.g. doe">
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="input-group">
                        <label>Email</label>
                        <input type="email" id="email" class="form-control" value="" disabled placeholder="e.g. johndoe@example.com">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <label>Mobile No.</label>
                        <input type="text" class="form-control" disabled value="" id="mobile_number">
                    </div>
                </div>

            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button pull-left">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- MODAL EDIT PROFILE END-->

<!-- MODAL EDIT PROFILE START-->
<div id="change-password-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Change Password</h4>
        </div>
        <form action="" name="update_password_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                    <div class="form-group">
                        <div class="input-group">
                            <label>Old Password</label>
                            <input type="password" name="old_password" class="form-control" value="" placeholder="e.g. Old Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>New Password</label>
                            <input type="password" name="password" id="new_password" class="form-control" value="" placeholder="e.g. New Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Confirm Password</label>
                            <input type="password" name="confirm_password" class="form-control" value="" placeholder="e.g. Confirm Password">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="site-button">Update</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- MODAL EDIT PROFILE END-->

@endif

<!-- Profile Module Over -->

@if((Route::current()->getName() == 'supplier_product') || (Route::current()->getName() == 'supplier_dashboard') )

<!-- MODAL CHANGES STOCKSTART-->
    <div id="change-stock" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Update Stock</h4>
          </div>
          <div id="stockupdate" class="modal-body p-tb20 p-lr15">
          <form name="stock_update_form" method="post">
        @csrf

                <div  class="form-group">
                        <div class="input-group">

                            <div class="row">
                                <div class="col-md-3 pd-r-5">
                                        <label>Current Stock</label>
                                    <input type="text" id="current_stock" class="form-control" placeholder="100">
                                </div>
                                    <!-- <div class="col-md-1 pd-r-5 equalicon" id="stock_plus_minus_sign_div"> + </div>                                                     -->


                                <div class="col-md-4 pd-r-5">
                                    <div class="input-group" id="stock_quanity_div">
                                    <label>Update Stock</label>
                                        <div class="qty-block">
                                            <div style="width: 25px;margin-right: 10px;">
                                                <button type="button" id="add" class="add"><i class="fa fa-plus"></i></button>
                                                <button type="button" id="sub" class="sub"><i class="fa fa-minus"></i></button>
                                            </div>
                                            <!-- <button type="button" id="sub" class="sub"><i class="fa fa-minus"></i></button> -->
                                            <input type="text" name="quantity" id="stock_update_input" id="1" value="1" min="1" class="form-control"     />
                                            <!-- <button type="button" id="add" class="add"><i class="fa fa-plus"></i></button> -->

                                        </div>
                                        <div class="error" id="qty_digit_error"></div>
                                    </div>
                                </div>
                                <input type="hidden" name="plus_minus" id="plus_or_minus" value="1"/>
                                <input type="hidden" name="stock_product_id" id="stock_product_id"/>
                                    <div class="col-md-1 pd-r-5 equalicon"> = </div>

                                <div class="col-md-3 pd-r-5">
                                        <label>Total Stock</label>
                                    <input type="text" id="total_stock" class="form-control" placeholder="100">
                                </div>



                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button pull-left">Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
<!-- MODAL CHANGES STOCK END-->

@endif

<!-- MODAL ADD PROPOSAL START-->
<div id="add-proposal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title">Suggestion For New Product</h4>
        </div>
        <form name="add_proposal_form" method="post">
        @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="contact_person_id_div">
                                    <label>Contact Person <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="contact_person_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Contact Person</option>
                                            @if(isset($data["contact_data"]))
                                                @foreach($data["contact_data"] as $value)
                                                    <option value="{{ $value["_id"] }}">{{ $value["person_name"] }} - ({{ $value["mobile_number"] }})</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Product Name <span class="str">*</span></label>
                                    <input type="text" class="form-control" placeholder="e.g. superflow jointagg">
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Category <span class="str">*</span></label>
                                    <input type="text" name="category_name" class="form-control" placeholder="e.g. Aggregate, Cement , Steel">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Sub Category</label>
                                    <input type="text" name="sub_category_name" class="form-control" placeholder="e.g. 10mm Aggregate ,PPC Cement , Steel">
                                </div>
                            </div>
                        </div>



                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Qty In Stock(MT) <span class="str">*</span></label>
                                    <input type="text" name="quantity" class="form-control" placeholder="e.g. 100">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="pickup_address_id_div">
                                    <label>Pickup Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="pickup_address_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Address</option>
                                            @if(isset($data["address_data"]))
                                                @foreach($data["address_data"] as $value)
                                                    <option value="{{ $value["_id"] }}">{{ $value["line1"] }}, {{ $value["line2"] }}, {{ $value["cityDetails"]["city_name"] }} - {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                        <div class="input-group">
                                            <label>Description Of New Product</label>
                                            <div class="cstm-input">
                                                <textarea name="description" class="form-control" id="ctdescription" placeholder="Detailed of New Product" data-maxchar="5000"></textarea>
                                                <span class="character-text">Maximum 5000 Characters (<span class="character-counter"></span> remaining)</span>
                                            </div>
                                        </div>
                                    </div>
                        </div>




                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button data-dismiss="modal" class="site-button gray">Cancel</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD PROPOSAL END-->



@if((Route::current()->getName() == 'supplier_orders_details'))
<!-- MODAL Order tracking START-->
<div id="order-track" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Order Tracking</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15" id="order_trackking_div">

            </div>
        </div>
    </div>
</div>
<!-- MODAL Order tracking END-->

<!-- MODAL Order tracking START-->
<div id="order-CP-track" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Concrete Pump (CP) Tracking</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15" id="order_CP_trackking_div">

            </div>
        </div>
    </div>
</div>
<!-- MODAL Order tracking END-->

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-7-report-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_7_report_title">Add 7th Day Cube Test Report</h4>
        </div>
        <form action="" name="add_7_report_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="pic_7_report_div">
                                    <label>7th Day Cube Test Report Image </label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="pic_7_report" />
                                        </span>
                                        <input type="text" class="form-control browse-input" placeholder="e.g. jpg only" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="report_7_track_id" id="report_7_track_id" value=""/>
                        <input type="hidden" name="report_7_tm_id" id="report_7_tm_id" value=""/>
                        <input type="hidden" name="report_7_order_id" id="report_7_order_id" value=""/>

                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-28-report-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_28_report_title">Add 28th Day Cube Test Report</h4>
        </div>
        <form action="" name="add_28_report_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="pic_28_report_div">
                                    <label>28th Day Cube Test Report Image </label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="pic_28_report" />
                                        </span>
                                        <input type="text" class="form-control browse-input" placeholder="e.g. jpg only" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="report_28_track_id" id="report_28_track_id" value=""/>
                        <input type="hidden" name="report_28_tm_id" id="report_28_tm_id" value=""/>
                        <input type="hidden" name="report_28_order_id" id="report_28_order_id" value=""/>

                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif


<!-- Driver Module Start -->

@if(Route::current()->getName() == 'supplier_driver_details')

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-bank-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_driver_form_title">Add TM Driver Details</h4>
        </div>
        <form action="" name="add_driver_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Driver Full Name <span class="str">*</span></label>
                                    <input type="text" name="driver_name" id="driver_name" class="form-control text-capitalize" placeholder="e.g. Johndoe">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Mobile No. <span class="str">*</span></label>
                                    <input name="driver_mobile_number"  id="driver_mobile_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Alternate No. </label>
                                    <input name="driver_alt_mobile_number"  id="driver_alt_mobile_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Whatsapp No. </label>
                                    <input name="driver_whatsapp_number"  id="driver_whatsapp_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address For This TM Driver <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="sub_vendor_id" class="form-control" id="diriver_sub_vendor_id">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false))
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif
                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>



                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="driver_pic_div">
                                    <label>Driver Image </label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="driver_pic" />
                                        </span>
                                        <input type="text" class="form-control browse-input" placeholder="e.g. jpg only" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="add_driver_form_type" id="add_driver_form_type" value="add"/>
                         <input type="hidden" name="driver_id" id="driver_id" value=""/>
                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif



<!-- Operator Module Start -->

@if(Route::current()->getName() == 'supplier_operator_details')

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-operator-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_operator_form_title">Add Operator Details</h4>
        </div>
        <form action="" name="add_operator_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Operator Full Name <span class="str">*</span></label>
                                    <input type="text" name="driver_name" id="driver_name" class="form-control text-capitalize" placeholder="e.g. Johndoe">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Mobile No. <span class="str">*</span></label>
                                    <input name="driver_mobile_number"  id="driver_mobile_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Alternate No. </label>
                                    <input name="driver_alt_mobile_number"  id="driver_alt_mobile_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Whatsapp No. </label>
                                    <input name="driver_whatsapp_number"  id="driver_whatsapp_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address For This CP Operator <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="sub_vendor_id" class="form-control" id="operator_sub_vendor_id">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false))
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif
                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="driver_pic_div">
                                    <label>Operator Image </label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="driver_pic" />
                                        </span>
                                        <input type="text" class="form-control browse-input" placeholder="e.g. jpg only" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="add_operator_form_type" id="add_operator_form_type" value="add"/>
                         <input type="hidden" name="driver_id" id="driver_id" value=""/>
                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif

<!-- Concrete Pump Module Start -->

<!-- Pump Gang Module Start -->

@if(Route::current()->getName() == 'supplier_pumpgang_details')

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-pumpgang-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_pumpgang_form_title">Add Pump Gang Person Detail</h4>
        </div>
        <form action="" name="add_pumpgang_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Person Full Name <span class="str">*</span></label>
                                    <input type="text" name="driver_name" id="driver_name" class="form-control text-capitalize" placeholder="e.g. Johndoe">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Mobile No. <span class="str">*</span></label>
                                    <input name="driver_mobile_number"  id="driver_mobile_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Alternate No. </label>
                                    <input name="driver_alt_mobile_number"  id="driver_alt_mobile_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Whatsapp No. </label>
                                    <input name="driver_whatsapp_number"  id="driver_whatsapp_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address For This CP Gang <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="sub_vendor_id" class="form-control" id="operator_sub_vendor_id">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false))
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif
                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : (isset($value['user']['user_id']) ? $value['user']['user_id'] : '') }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="driver_pic_div">
                                    <label>Person Image </label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="driver_pic" />
                                        </span>
                                        <input type="text" class="form-control browse-input" placeholder="e.g. jpg only" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="add_pumpgang_form_type" id="add_pumpgang_form_type" value="add"/>
                         <input type="hidden" name="driver_id" id="driver_id" value=""/>
                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif

<!-- Concrete Pump Module Start -->

@if(Route::current()->getName() == 'supplier_concrete_pump_details')



<!-- MODAL ADD Concrete Pump START-->
<div id="add-concrete-pump-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_concrete_pump_title">Add Concrete Pump</h4>
        </div>
        <form action="" name="add_concrete_pump_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" class="form-control" id="CP_address_id">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false))
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ isset($value['_id']) ? $value['_id'] : '' }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            
                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ isset($value['_id']) ? $value['_id'] : '' }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ isset($value['_id']) ? $value['_id'] : '' }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Concrete Pump Category <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="ConcretePumpCategoryId" id="ConcretePumpCategoryId" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Concrete Pump Category</option>
                                            @if(isset($data["category_data"]))
                                                @foreach($data["category_data"] as $cat_value)
                                                    <option value="{{ $cat_value['_id'] }}">{{ $cat_value["category_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Concrete Pump Make <span class="str">*</span></label>
                                    <input type="text" name="company_name" id="pump_make" class="form-control" placeholder="e.g. TATA">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Concrete Pump Model <span class="str">*</span></label>
                                    <input type="text" name="concrete_pump_model" id="concrete_pump_model" class="form-control" placeholder="e.g. 1234">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Manufacture Year <span class="str">*</span></label>
                                    <input type="text" name="manufacture_year" id="manufacture_year" class="form-control" placeholder="e.g. 2020">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>CP Serial No <span class="str">*</span></label>
                                    <input type="text" name="serial_number" id="CP_serial_number" class="form-control" placeholder="e.g. 2020">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Pipe Connection (Meter) <span class="str">*</span></label>
                                    <input type="text" name="pipe_connection" id="pipe_connection" class="form-control" placeholder="e.g. 100">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>CP Capacity (Cu.Mtr) / Hour <span class="str">*</span></label>
                                    <input type="text" name="concrete_pump_capacity" id="concrete_pump_capacity" class="form-control" placeholder="e.g. 4">
                                </div>
                            </div>
                        </div>



                        <div class="col-md-12 col-sm-12 col-xs-12" style="display:none;">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="checkbox" name="is_Operator" id="is_Operator" checked style="margin-top: 3px;margin-right: 6px;" /> With Operator
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12" id="operator_id_div">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Operator <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="operator_id" id="operator_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Operator</option>
                                            @if(isset($data["operator_data"]))
                                                @foreach($data["operator_data"] as $operator_value)
                                                    <option value="{{ $operator_value['_id'] }}">{{ $operator_value["operator_name"] }} - {{ isset($operator_value["addressDetails"]["business_name"]) ? $operator_value["addressDetails"]["business_name"] : '' }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="checkbox" name="is_helper" id="is_helper" style="margin-top: 3px;margin-right: 6px;" /> With Helper

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12" id="gang_id_div">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>CP Gang <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="gang_id" id="gang_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select CP Gang</option>
                                            @if(isset($data["gang_data"]))
                                                @foreach($data["gang_data"] as $gang_value)
                                                    <option value="{{ $gang_value['_id'] }}">{{ $gang_value["gang_name"] }} - {{ isset($gang_value["addressDetails"]["business_name"]) ? $gang_value["addressDetails"]["business_name"] : '' }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Transportation Charge (Per KM) <span class="str">*</span></label>
                                    <input type="text" name="transportation_charge" id="transportation_charge" class="form-control" placeholder="e.g. 10">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Concrete Pump Price (Cu.Mtr) <span class="str">*</span></label>
                                    <input type="text" name="concrete_pump_price" id="concrete_pump_price" class="form-control" placeholder="e.g. 10">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="concrete_pump_image_div">
                                    <label>Concrete Pump Image <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="concrete_pump_image" id="concrete_pump_image" />
                                        </span>
                                        <input type="text" id="concrete_pump_image_text" class="form-control browse-input"  placeholder="e.g. jpg,jpeg" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>
            <input type="hidden" name="add_concrete_pump_form_type" id="add_concrete_pump_form_type" value="add"/>
              <input type="hidden" name="concrete_pump_id" id="concrete_pump_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif



<!-- Design Mix Module Start -->

@if(Route::current()->getName() == 'design_mix_detail')



<!-- MODAL ADD Concrete Pump START-->
<div id="show-design-mix-details" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="design_mix_popup_title">Standard Design Mix</h4>
        </div>
        <div id="design_mix_body">

            
        </div>
        
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif


<!-- Media Design Mix Module Start -->

@if(Route::current()->getName() == 'media_design_mix_detail')

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-primary-media-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_primary_media_details_title">Add Primary Design Mix Media</h4>
        </div>
        <form action="" name="add_primary_media_detail_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="primary_image_div">
                                    <label>Primary Image (It will be your first image) <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="primary_image" id="primary_image" />
                                        </span>
                                        <input type="text" id="primary_image_text" class="form-control browse-input"  placeholder="e.g. jpg, jpeg" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>
            <!-- <input type="hidden" name="is_post_call" id="is_post_call" value="add"/> -->
            <!-- <input type="hidden" name="add_bank_detail_form_type" id="add_bank_detail_form_type" value="add"/> -->
              <!-- <input type="hidden" name="bank_detail_id" id="bank_detail_id" value=""/> -->
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->


<!-- MODAL ADD BANK DETAIL START-->
<div id="add-secondory-media-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_secondory_media_details_title">Add Secondory Design Mix Media</h4>
        </div>
        <form action="" name="add_secondory_media_detail_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="secondory_image_div">
                                    <label>Other Images <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="secondory_image[]" id="secondory_image" multiple/>
                                        </span>
                                        <input type="text" id="secondory_image_text" class="form-control browse-input"  placeholder="e.g. jpg, jpeg" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>
            <!-- <input type="hidden" name="is_post_call" id="is_post_call" value="add"/> -->
            <!-- <input type="hidden" name="add_bank_detail_form_type" id="add_bank_detail_form_type" value="add"/> -->
              <!-- <input type="hidden" name="bank_detail_id" id="bank_detail_id" value=""/> -->
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif

<!-- Media Design Mix Module Over -->



<!-- Cement Rate Module Start -->

@if(Route::current()->getName() == 'cement_rate')

<!-- MODAL ADD Cement Rate START-->
<div id="add-cement-rate-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_cement_rate_title">Add Cement Per KG Rate (Excl. GST)</h4>
        </div>
        <form action="" name="add_cement_rate_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                           <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                           @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Cement Brand <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="brand_id" id="brand_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Cement Brand</option>
                                            @if(isset($data["cement_brand_data"]))
                                                @foreach($data["cement_brand_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Cement Grade <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="grade_id" id="grade_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Cement Grade</option>
                                            @if(isset($data["cement_grade_data"]))
                                                @foreach($data["cement_grade_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Cement Per KG Rate <span class="str">*</span> (Excl. GST)</label>
                                    <input type="text" name="per_kg_rate" id="per_kg_rate" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add rate more than zero and your selected cement brand status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_cement_rate_form_type" id="add_cement_rate_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Cement Rate Module Over -->


<!-- Sand Rate Module Start -->

@if(Route::current()->getName() == 'sand_rate')

<!-- MODAL ADD Cement Rate START-->
<div id="add-sand-rate-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_sand_rate_title">Add Coarse Sand Per KG Rate (Excl. GST)</h4>
        </div>
        <form action="" name="add_sand_rate_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                           @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Coarse Sand Source <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="source_id" id="source_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                                            @if(isset($data["sand_source_data"]))
                                                @foreach($data["sand_source_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['sand_source_name'] }}">{{ $value["sand_source_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Coarse Sand Zone <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="sand_zone_id" id="sand_zone_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Coarse Sand Zone</option>
                                            @if(isset($data["sand_zone_data"]))
                                                @foreach($data["sand_zone_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['zone_name'] }}">{{ $value["zone_name"] }} ({{ $value["finess_module_range"] }})</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Coarse Sand Per KG Rate <span class="str">*</span> (Excl. GST)</label>
                                    <input type="text" name="per_kg_rate" id="per_kg_rate" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add rate more than zero and your selected coarse sand source status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_sand_rate_form_type" id="add_sand_rate_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Sand Rate Module Over -->



<!-- Aggregate Rate Module Start -->

@if(Route::current()->getName() == 'aggregate_rate')

<!-- MODAL ADD Cement Rate START-->
<div id="add-aggregate-rate-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_aggregate_rate_title">Add Aggregate Per KG Rate (Excl. GST)</h4>
        </div>
        <form action="" name="add_aggregate_rate_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Aggregate Source <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="source_id" id="source_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Aggregate Source</option>
                                            @if(isset($data["aggregate_source_data"]))
                                                @foreach($data["aggregate_source_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['aggregate_source_name'] }}">{{ $value["aggregate_source_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Aggregate Sub Category <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="sub_cat_id" id="sub_cat_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Aggregate Sub Category</option>
                                            @if(isset($data["aggregate_sub_cat_data"]))
                                                @foreach($data["aggregate_sub_cat_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Aggregate Per KG Rate <span class="str">*</span> (Excl. GST)</label>
                                    <input type="text" name="per_kg_rate" id="per_kg_rate" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add rate more than zero and your selected aggregate source status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_aggregate_rate_form_type" id="add_aggregate_rate_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Aggregate Rate Module Over -->


<!-- Flyash Rate Module Start -->

@if(Route::current()->getName() == 'flyash_rate')

<!-- MODAL ADD Cement Rate START-->
<div id="add-flyash-rate-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_flyash_rate_title">Add Fly Ash Per KG Rate (Excl. GST)</h4>
        </div>
        <form action="" name="add_flyash_rate_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Flyash Source <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="source_id" id="source_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Flyash Source</option>
                                            @if(isset($data["fly_ash_source_data"]))
                                                @foreach($data["fly_ash_source_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['fly_ash_source_name'] }}">{{ $value["fly_ash_source_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Fly Ash Per KG Rate <span class="str">*</span> (Excl. GST)</label>
                                    <input type="text" name="per_kg_rate" id="per_kg_rate" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add rate more than zero and your selected flyash source status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_flyash_rate_form_type" id="add_flyash_rate_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Flyash Rate Module Over -->

<!-- Admixture Rate Module Start -->

@if(Route::current()->getName() == 'admixture_rate')

<!-- MODAL ADD Cement Rate START-->
<div id="add-admixture-rate-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_admixture_rate_title">Add Admixture Per KG Rate (Excl. GST)</h4>
        </div>
        <form action="" name="add_admixture_rate_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Admixture Brand <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="brand_id" id="ad_mixture_brand_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                            @if(isset($data["admixture_brand_data"]))
                                                @foreach($data["admixture_brand_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Admixture Types <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="ad_mixture_category_id" id="ad_mixture_category_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Admixture Types</option>
                                            @if(isset($data["admixture_types_data"]))
                                                @foreach($data["admixture_types_data"] as $value)
                                                    @if((Request::get('brand_id') != null))
                                                        @if(Request::get('brand_id') == $value['brand_id'])
                                                            <option value="{{ $value['_id'] }}" >{{ $value["category_name"] }} - {{ $value["admixture_type"] }}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{ $value['_id'] }}" >{{ $value["category_name"] }} - {{ $value["admixture_type"] }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Admixture Per KG Rate <span class="str">*</span> (Excl. GST)</label>
                                    <input type="text" name="per_kg_rate" id="per_kg_rate" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add rate more than zero and your selected admixture brand status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_admixture_rate_form_type" id="add_admixture_rate_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Admixture Rate Module Over -->


<!-- Water Rate Module Start -->

@if(Route::current()->getName() == 'water_rate')

<!-- MODAL ADD Cement Rate START-->
<div id="add-water-rate-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_water_rate_title">Add Water Per LTR Rate (Excl. GST)</h4>
        </div>
        <form action="" name="add_water_rate_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Water Types <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="water_type" id="water_type_drop" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Water Type</option>
                                            
                                            <option value="Regular">Regular</option>
                                            <option value="Mineral">Mineral</option>
                                                
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Water Per LTR Rate <span class="str">*</span> (Excl. GST)</label>
                                    <input type="text" name="per_kg_rate" id="per_kg_rate" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add rate more than zero and your selected water status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_water_rate_form_type" id="add_water_rate_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Water Rate Module Over -->




<!-- Cement Rate Module Start -->

@if(Route::current()->getName() == 'cement_stock')

<!-- MODAL ADD Cement Rate START-->
<div id="add-cement-stock-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_cement_stock_title">Add Cement Stock</h4>
        </div>
        <form action="" name="add_cement_stock_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Cement Brand<span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="brand_id" id="brand_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Cement Brand</option>
                                            @if(isset($data["cement_brand_data"]))
                                                @foreach($data["cement_brand_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Cement Grade <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="grade_id" id="grade_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Cement Grade</option>
                                            @if(isset($data["cement_grade_data"]))
                                                @foreach($data["cement_grade_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Cement Stock (KG) <span class="str">*</span></label>
                                    <input type="text" name="quantity" id="quantity" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        
                    </div>
                    <div><b>Note:</b> <i>To activate, add stock more than zero and your selected cement brand status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_cement_stock_form_type" id="add_cement_stock_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Cement Rate Module Over -->


<!-- Sand Rate Module Start -->

@if(Route::current()->getName() == 'sand_stock')

<!-- MODAL ADD Cement Rate START-->
<div id="add-sand-stock-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_sand_stock_title">Add Coarse Sand Stock</h4>
        </div>
        <form action="" name="add_sand_stock_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Coarse Sand Source <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="source_id" id="source_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                                            @if(isset($data["sand_source_data"]))
                                                @foreach($data["sand_source_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['sand_source_name'] }}">{{ $value["sand_source_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Coarse Sand Zone <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="sand_zone_id" id="sand_zone_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Coarse Sand Zone</option>
                                            @if(isset($data["sand_zone_data"]))
                                                @foreach($data["sand_zone_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['zone_name'] }}">{{ $value["zone_name"] }} ({{ $value["finess_module_range"] }})</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Coarse Sand Stock (KG) <span class="str">*</span></label>
                                    <input type="text" name="quantity" id="quantity" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add stock more than zero and your selected coarse sand status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_sand_stock_form_type" id="add_sand_stock_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Sand Rate Module Over -->



<!-- Aggregate Rate Module Start -->

@if(Route::current()->getName() == 'aggregate_stock')

<!-- MODAL ADD Cement Rate START-->
<div id="add-aggregate-stock-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_aggregate_stock_title">Add Aggregate Stock </h4>
        </div>
        <form action="" name="add_aggregate_stock_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Aggregate Source <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="source_id" id="source_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Aggregate Source</option>
                                            @if(isset($data["aggregate_source_data"]))
                                                @foreach($data["aggregate_source_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['aggregate_source_name'] }}">{{ $value["aggregate_source_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Aggregate Sub Category <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="sub_cat_id" id="sub_cat_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Aggregate Sub Category</option>
                                            @if(isset($data["aggregate_sub_cat_data"]))
                                                @foreach($data["aggregate_sub_cat_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Aggregate Stock (KG) <span class="str">*</span></label>
                                    <input type="text" name="quantity" id="quantity" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add stock more than zero and your selected aggregate source status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_aggregate_stock_form_type" id="add_aggregate_stock_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Aggregate Rate Module Over -->


<!-- Flyash Rate Module Start -->

@if(Route::current()->getName() == 'flyash_stock')

<!-- MODAL ADD Cement Rate START-->
<div id="add-flyash-stock-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_flyash_stock_title">Add Fly Ash Stock </h4>
        </div>
        <form action="" name="add_flyash_stock_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Fly Ash Source <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="source_id" id="source_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Fly Ash Source</option>
                                            @if(isset($data["fly_ash_source_data"]))
                                                @foreach($data["fly_ash_source_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['fly_ash_source_name'] }}">{{ $value["fly_ash_source_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Fly Ash Stock (KG) <span class="str">*</span></label>
                                    <input type="text" name="quantity" id="quantity" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add stock more than zero and your selected flyash source status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_flyash_stock_form_type" id="add_flyash_stock_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Flyash Rate Module Over -->

<!-- Admixture Rate Module Start -->

@if(Route::current()->getName() == 'admixture_stock')

<!-- MODAL ADD Cement Rate START-->
<div id="add-admixture-stock-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_admixture_stock_title">Add Admixture Stock </h4>
        </div>
        <form action="" name="add_admixture_stock_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Admixture Brand <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="brand_id" id="ad_mixture_brand_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                            @if(isset($data["admixture_brand_data"]))
                                                @foreach($data["admixture_brand_data"] as $value)
                                                    <option value="{{ $value['_id'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Admixture Types <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="ad_mixture_category_id" id="ad_mixture_category_id" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Admixture Types</option>
                                            @if(isset($data["admixture_types_data"]))
                                                @foreach($data["admixture_types_data"] as $value)
                                                    @if((Request::get('brand_id') != null))
                                                        @if(Request::get('brand_id') == $value['brand_id'])
                                                            <option value="{{ $value['_id'] }}" data-value = "{{ json_encode($value) }}">{{ $value["category_name"] }} - {{ $value["admixture_type"] }}</option>
                                                        @endif
                                                    @else
                                                        <option value="{{ $value['_id'] }}" data-value = "{{ json_encode($value) }}">{{ $value["category_name"] }} - {{ $value["admixture_type"] }}</option>
                                                    @endif

                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Admixture Stock (KG) <span class="str">*</span></label>
                                    <input type="text" name="quantity" id="quantity" class="form-control" placeholder="e.g. 50">
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                        <div><b>Note:</b> <i>To activate, add stock more than zero and your selected admixture brand status will be active. </i></div>
                </div>
            </div>
            <input type="hidden" name="add_admixture_stock_form_type" id="add_admixture_stock_form_type" value="add"/>
              <input type="hidden" name="rate_id" id="rate_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

<!-- Admixture Rate Module Over -->


<!-- TM UnAvailable Module Start -->

@if(Route::current()->getName() == 'TM_detail')



<!-- MODAL ADD Concrete Pump START-->
<div id="show-TM-Unavail-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <button class="site-button pull-right m-r10" onclick="resetForm('add_TM_Unavail_form','add-TM-Unavail-details')" data-dismiss="modal" data-toggle="modal" data-target="#add-TM-Unavail-details">Add TM UnAvailability</button>
          <h4 class="modal-title" id="TM_Unavail_popup_title" >TM UnAvailability Date & Time</h4>
        </div>
        <div id="TM_Unavail_body">

        </div>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

<!-- MODAL ADD Cement Rate START-->
<div id="add-TM-Unavail-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_TM_Unavail_title">Add TM UnAvailability </h4>
        </div>
        <form action="" name="add_TM_Unavail_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>TM Unavailable Date <span class="str">*</span></label>
                                    <div class="text_box_icon">
                                        <input type="text" name="unavailable_at" class="form-control TM_unavail_date" readonly="" placeholder="e.g. dd-mm-yyyy">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Start Time <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <!-- <select class="form-control" name="ad_mixture_brand_id" id="ad_mixture_brand_id"> -->
                                        <select class="form-control" name="start_time" id="TM_unavailable_start_time">
                                            <option disabled="disabled" selected="selected">Select Start Time</option>

                                            <option value="00:00">00:00</option>
                                            <option value="01:00">01:00</option>
                                            <option value="02:00">02:00</option>
                                            <option value="03:00">03:00</option>
                                            <option value="04:00">04:00</option>
                                            <option value="05:00">05:00</option>
                                            <option value="06:00">06:00</option>
                                            <option value="07:00">07:00</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>

                                            <option value="12:00">12:00</option>
                                            <option value="13:00">13:00</option>
                                            <option value="14:00">14:00</option>
                                            <option value="15:00">15:00</option>
                                            <option value="16:00">16:00</option>
                                            <option value="17:00">17:00</option>
                                            <option value="18:00">18:00</option>
                                            <option value="19:00">19:00</option>
                                            <option value="20:00">20:00</option>
                                            <option value="21:00">21:00</option>
                                            <option value="22:00">22:00</option>
                                            <option value="23:00">23:00</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>End Time <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <!-- <select class="form-control" name="ad_mixture_brand_id" id="ad_mixture_brand_id"> -->
                                        <select class="form-control" name="end_time" id="TM_unavailable_end_time">
                                            <option disabled="disabled" selected="selected">Select End Time</option>



                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>

              <input type="hidden" name="TM_unavail_id" id="TM_unavail_id" value=""/>
              <input type="hidden" name="TM_sub_vendor_id" id="TM_sub_vendor_id" value=""/>
              <input type="hidden" name="TM_unavail_type" id="TM_unavail_type" value="add"/>

            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

<script>
	$('.TM_unavail_date').datepicker({
        autoclose: true,
        startDate: '1d',
        // format: 'yyyy-mm-dd'
        format: 'dd-mm-yyyy'
	});
</script>

@endif


<!-- TM UnAvailable Module Over -->

<!-- CP UnAvailable Module Start -->

@if(Route::current()->getName() == 'supplier_concrete_pump_details')



<!-- MODAL ADD Concrete Pump START-->
<div id="show-CP-Unavail-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <button class="site-button pull-right m-r10" onclick="resetForm('add_CP_Unavail_form','add-CP-Unavail-details')" data-dismiss="modal" data-toggle="modal" data-target="#add-CP-Unavail-details">Add CP UnAvailability</button>
          <h4 class="modal-title" id="CP_Unavail_popup_title">CP UnAvailability</h4>
        </div>
        <div id="CP_Unavail_body">



        </div>

      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

<!-- MODAL ADD Cement Rate START-->
<div id="add-CP-Unavail-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_CP_Unavail_title">Add CP UnAvailability </h4>
        </div>
        <form action="" name="add_CP_Unavail_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>CP Anavailable Date <span class="str">*</span></label>
                                    <div class="text_box_icon">
                                        <input type="text" name="unavailable_at" class="form-control CP_unavail_date" readonly="" placeholder="e.g. yyyy-mm-dd">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Start Time <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <!-- <select class="form-control" name="ad_mixture_brand_id" id="ad_mixture_brand_id"> -->
                                        <select class="form-control" name="start_time" id="CP_unavailable_start_time">
                                            <option disabled="disabled" selected="selected">Start Time</option>

                                            <option value="00:00">00:00</option>
                                            <option value="01:00">01:00</option>
                                            <option value="02:00">02:00</option>
                                            <option value="03:00">03:00</option>
                                            <option value="04:00">04:00</option>
                                            <option value="05:00">05:00</option>
                                            <option value="06:00">06:00</option>
                                            <option value="07:00">07:00</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>

                                            <option value="12:00">12:00</option>
                                            <option value="13:00">13:00</option>
                                            <option value="14:00">14:00</option>
                                            <option value="15:00">15:00</option>
                                            <option value="16:00">16:00</option>
                                            <option value="17:00">17:00</option>
                                            <option value="18:00">18:00</option>
                                            <option value="19:00">19:00</option>
                                            <option value="20:00">20:00</option>
                                            <option value="21:00">21:00</option>
                                            <option value="22:00">22:00</option>
                                            <option value="23:00">23:00</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>End Time <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <!-- <select class="form-control" name="ad_mixture_brand_id" id="ad_mixture_brand_id"> -->
                                        <select class="form-control" name="end_time" id="CP_unavailable_end_time">
                                            <option disabled="disabled" selected="selected">End Time</option>



                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>

                        </div>
                </div>
            </div>

              <input type="hidden" name="CP_unavail_id" id="CP_unavail_id" value=""/>
              <input type="hidden" name="CP_sub_vendor_id" id="CP_sub_vendor_id" value=""/>
              <input type="hidden" name="CP_unavail_type" id="CP_unavail_type" value="add"/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

<script>
	$('.CP_unavail_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'dd-mm-yyyy'
        // format: 'yyyy-mm-dd'
	});
</script>

@endif


@if(Route::current()->getName() == 'supplier_orders_details')

<!-- MODAL ADD BANK DETAIL START-->
<div id="assign-truck-popup" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_driver_form_title">Assign Transit Mixer </h4>
        </div>
        <form action="" name="truck_assigned_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="form-group">
                        <div class="input-group">
                            <label>Transit Mixer No <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="vehicle_id" id="TM_dropdown_assign">
                                    <option disabled="disabled" selected="selected">Select Transit Mixer</option>
                                    <?php //dd($vehicles); ?>
                                    @if(isset($vehicles))

                                        @foreach($vehicles as $value)
                                            @if($value['address_id'] == $data['address_id'])
                                                <option value="{{ $value['_id'] }}" data-address="{{ $value['address_id'] }}">{{ $value["business_name"] }} - {{ $value["TM_rc_number"] }} (Capacity - {{ $value["TM_sub_category"]["load_capacity"] }} Cu Mtr)</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Deliver Quantity (Cu.Mtr) <span class="str">*</span></label>
                            <input type="text" name="delivered_quantity" id="TM_assign_delivered_quantity" class="form-control" placeholder="Quntity Cu.Mtr">
                        </div>
                        <span class="error" style="display:block" id="TM_assign_delivered_quantity_error"></span>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Assign Transit Mixer Date <span class="str">*</span></label>
                            <!-- <input type="text" name="assigned_at" id="TM_assign_delivered_assign_date" class="form-control truck_assign_date" readonly="" placeholder="e.g. dd-mm-yyyy"> -->
                            <div class="text_box_icon">
                                <input type="text" name="assigned_at" id="TM_assign_delivered_assign_date" class="form-control" readonly="" placeholder="e.g. dd-mm-yyyy">
                                <i class="fa fa-calendar"></i>
                            </div>

                        </div>
                        <!-- <span class="error" id="TM_assign_delivered_assign_date_error"></span> -->
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Assign Transit Mixer Start Time <span class="str">*</span></label>
                            <input type="text" name="assigned_at_start_time" id="TM_assign_delivered_assign_start_time" class="form-control" readonly="" placeholder="e.g. 10:30">

                        </div>
                        <!-- <span class="error" id="TM_assign_delivered_assign_date_error"></span> -->
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Assign Transit Mixer End Time <span class="str">*</span></label>
                            <input type="text" name="assigned_at_end_time" id="TM_assign_delivered_assign_end_time" class="form-control" readonly="" placeholder="e.g. 15:30">

                        </div>
                        <!-- <span class="error" id="TM_assign_delivered_assign_date_error"></span> -->
                    </div>
                    <input type="hidden" id="order_assign_item_assign_qty" value="0" />
                    <input type="hidden" id="order_assign_item_to_be_assign_qty" value="0" />
                    <input type="hidden" name="order_item_part_id" id="order_assign_item_part_id" value="" />
                    <input type="hidden" name="order_id" value="{{ isset($data['_id']) ? $data['_id'] : '' }}" id="logistic_order_id"/>
                    <input type="hidden" name="order_item_id" value="{{ isset($order_item_data['buyer_order_item_id']) ? $order_item_data['buyer_order_item_id'] : '' }}" id="logistic_order_item_id"/>
                    <input type="hidden" name="order_item_temp_qty" value="{{ isset($final_data['data']['orderItem'][0]['temp_quantity']) ? $final_data['data']['orderItem'][0]['temp_quantity'] : '0' }}" id="logistic_order_item_qty"/>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <input type="submit" class="site-button m-r20" id="TM_assign_delivered_quantity_save" value="Save">
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

<script>
	$('.truck_assign_date').datepicker({
        autoclose: true,
        startDate: '1d',
        endDate: '+30d',
        format: 'dd-mm-yyyy',
        onSelect: function(dateText) {

        }
	});

    $('.truck_assign_date').change(function() {

        // assignQTY();
        // assignDeliveryDate();


    });

</script>


<!-- MODAL ADD BANK DETAIL START-->
<div id="assign-CP-popup" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_cp_form_title">Assign Concrete Pump </h4>
        </div>
        <form action="" name="cp_assigned_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="form-group">
                        <div class="check_blue m-0" style="display: flex;align-items: end;">
                            <input type="checkbox" name="is_already_delivered" id="CP_is_already_delivered" class="form-control" style="width: 3%;margin-right: 10px;" data-order-id="{{ isset($data['_id']) ? $data['_id'] : '' }}" data-order-item-id="{{ isset($order_item_data['buyer_order_item_id']) ? $order_item_data['buyer_order_item_id'] : '' }}" data-plant-address="{{ $data['address_id'] }}">
                            <label for="CP_is_already_delivered" class="checkbox">CP Already delivered at site? </label>

                        </div>
                        <!-- <span class="error" id="TM_assign_delivered_assign_date_error"></span> -->
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Concrete Pump <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="cp_id" id="CP_dropdown_assign">
                                    <option disabled="disabled" selected="selected">Select Concrete Pump</option>
                                    <?php //dd($vehicles); ?>
                                    @if(isset($cp_lists))

                                        @foreach($cp_lists as $value)
                                            @if($value['address_id'] == $data["address_id"])
                                                <option value="{{ $value['_id'] }}" data-address="{{ $value['address_id'] }}">
                                                    {{ $value["business_name"] }} - {{ $value["concrete_pump_category"]['category_name'] }} - {{ $value["concrete_pump_model"] }}
                                                </option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <select id="CP_dropdown_assign_hidden" style="display:none">
                                    <option disabled="disabled" selected="selected">Select Concrete Pump</option>
                                    <?php //dd($vehicles); ?>
                                    @if(isset($cp_lists))

                                        @foreach($cp_lists as $value)
                                            <option value="{{ $value['_id'] }}" data-address="{{ $value['address_id'] }}">
                                                {{ $value["concrete_pump_category"]['category_name'] }} - {{ $value["concrete_pump_model"] }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Select Assign Concrete Pump Date <span class="str">*</span></label>
                        <div class="text_box_icon">
                            <input type="text" name="assigned_at" id="CP_assign_delivered_assign_date" class="form-control" readonly="" placeholder="e.g. dd-mm-yyyy">
                            <i class="fa fa-calendar"></i>
                        </div>

                        <!-- <span class="error" id="TM_assign_delivered_assign_date_error"></span> -->
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Assign Concrete Pump Start Time <span class="str">*</span></label>
                            <input type="text" name="assigned_at_start_time" id="CP_assign_delivered_assign_start_time" class="form-control" readonly="" placeholder="e.g. 10:30">

                        </div>
                        <!-- <span class="error" id="TM_assign_delivered_assign_date_error"></span> -->
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Assign Concrete Pump End Time <span class="str">*</span></label>
                            <input type="text" name="assigned_at_end_time" id="CP_assign_delivered_assign_end_time" class="form-control" readonly="" placeholder="e.g. 15:30">

                        </div>
                        <!-- <span class="error" id="TM_assign_delivered_assign_date_error"></span> -->
                    </div>
                    <input type="hidden" name="order_item_part_id" id="cp_order_assign_item_part_id" value="" />
                    <input type="hidden" name="order_id" value="{{ isset($data['_id']) ? $data['_id'] : '' }}" id=""/>
                    <input type="hidden" name="order_item_id" value="{{ isset($order_item_data['buyer_order_item_id']) ? $order_item_data['buyer_order_item_id'] : '' }}" id=""/>


                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <input type="submit" class="site-button m-r20" id="CP_assign_delivered_quantity_save" value="Save">
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->



<script>
	$('.cp_assign_date').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        onSelect: function(dateText) {
            console.log("tesftesf");
        }
	});

    $('.cp_assign_date').change(function() {

        // assignQTY();
        // assignDeliveryDate();


    });

</script>


<!-- MODAL ADD BANK DETAIL START-->
<div id="reassign-order-popup" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-xl" style="max-width: 90%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_cp_form_title">Re-Assign Order </h4>
        </div>
        <form action="" name="order_reassigned_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                <input type="hidden" name="vendor_id" id="order_reassign_vendor_id_field" />
                    <!-- <div class="form-group">
                        <div class="check_blue m-0" style="display: flex;align-items: end;">
                            <input type="checkbox" name="is_already_delivered" id="CP_is_already_delivered" class="form-control" style="width: 3%;margin-right: 10px;" data-order-id="{{ isset($data['_id']) ? $data['_id'] : '' }}" data-order-item-id="{{ isset($order_item_data['buyer_order_item_id']) ? $order_item_data['buyer_order_item_id'] : '' }}" data-plant-address="{{ $data['address_id'] }}">
                            <label for="CP_is_already_delivered" class="checkbox">CP Already delivered at site? </label>

                        </div>
                        <span class="error" id="TM_assign_delivered_assign_date_error"></span>
                    </div> -->
                    <div class="form-group">
                        <div class="input-group">
                            <label>Reason <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select id="order_reassign_reason" readOnly="">
                                    <option disabled="disabled" selected="selected">Select Reason</option>
                                    <?php //dd($vehicles); ?>
                                    
                                    <option value="order_rejected">
                                        Due To Order Rejected
                                    </option>
                                    <option value="breakdown">
                                        Due To Any Breakdown
                                    </option>
                                    <option value="material_out_of_stock">
                                        Due To Material Out Of Stock
                                    </option>
                                            
                                </select>
                                <input type="hidden" name="order_reassign_reason" id="order_reassign_reason_field" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description Of Reason <span class="str">*</span></label>
                        <div class="text_box_icon">
                            <input type="text" name="order_reassign_desc" class="form-control" placeholder="Write Description Of Reason">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Select Duration <span class="str">*</span></label>
                        <div class="range_picker m-r10">
                            <input type="text" class="form-control" name="daterange" placeholder="e.g. 01/01/2018 - 01/15/2018">
                            
                            <i class="fa fa-calendar"></i>
                            <input type="hidden" id="reassign_start_date" name="reassign_start_date" value="">
                            <input type="hidden" id="reassign_end_date" name="reassign_end_date" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Affected Orders <span class="str">*</span></label>
                        <div class="comn-table latest-order-table" id="reassigned_order_popup_tbody">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Please select duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr>
                                        <td colspan="7">
                                            No Orders Available
                                        </td>
                                    </tr> -->

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <span class="error" id="order_selection_error"></span>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer text-left">
                
                <input type="submit" class="site-button m-r20" value="Save">
            </div>
        </form>
      </div>
    </div>
</div>
<!-- MODAL ADD BANK DETAIL END-->

<script>
$(function() {

    var today = new Date();
    var end = new Date();
    // today.setDate(today.getDate() + 2);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    end = dd + '/' + mm + '/' + yyyy;
    console.log(today);

    // @if(Request::get('start_date'))

    //     var date = new Date('{{Request::get('start_date')}}');

    //     var dd = String(date.getDate()).padStart(2, '0');
    //     var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    //     var yyyy = date.getFullYear();
    //     console.log(mm+"..."+yyyy);

    //     today = dd + '/' + mm + '/' + yyyy;
    //     // today = yyyy + '-' + mm + '-' + dd;
    //     console.log("start_date..."+today);
    // @endif

    // @if(Request::get('end_date'))

    //     var date = new Date('{{Request::get('end_date')}}');

    //     var dd = String(date.getDate()).padStart(2, '0');
    //     var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    //     var yyyy = date.getFullYear();
    //     console.log(mm+"..."+yyyy);

    //     end = dd + '/' + mm + '/' + yyyy;
    //     // end = yyyy + '-' + mm + '-' + dd;
    //     console.log("end_date..."+end);
    // @endif

  $('input[name="daterange"]').daterangepicker({
    opens: 'right',
    // autoUpdateInput: false,
    minDate: today,
    startDate: today,
    endDate: end,
    locale: {
            format: 'DD/MM/YYYY'
        }
  }, function(start, end, label) {
      $("#reassign_start_date").val(start.format('YYYY-MM-DD'));
      $("#reassign_end_date").val(end.format('YYYY-MM-DD'));

    //   $('form[name="supplier_report_form"]').submit();

        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));

        getAssignedOrdersBySelectedDateRange(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));
  });

//   $('#order_new_date').datepicker({
// 	    autoclose: true,
// 	    format: 'dd M yyyy'
// 		}).on('changeDate', function(selected){

// 	});
});
</script>

<!-- MODAL ADD BANK DETAIL START-->
<div id="reassign-reject-order-popup" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-xl" style="max-width: 90%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_cp_form_title">Re-Assign Order </h4>
        </div>
        <form action="" name="reject_order_reassigned_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                <input type="hidden" name="vendor_id" id="reject_order_reassign_vendor_id_field" />
                <input type="hidden" name="order_item_track_id" id="reject_order_reassign_order_item_track_id_field" />
                    <!-- <div class="form-group">
                        <div class="check_blue m-0" style="display: flex;align-items: end;">
                            <input type="checkbox" name="is_already_delivered" id="CP_is_already_delivered" class="form-control" style="width: 3%;margin-right: 10px;" data-order-id="{{ isset($data['_id']) ? $data['_id'] : '' }}" data-order-item-id="{{ isset($order_item_data['buyer_order_item_id']) ? $order_item_data['buyer_order_item_id'] : '' }}" data-plant-address="{{ $data['address_id'] }}">
                            <label for="CP_is_already_delivered" class="checkbox">CP Already delivered at site? </label>

                        </div>
                        <span class="error" id="TM_assign_delivered_assign_date_error"></span>
                    </div> -->
                    <div class="form-group">
                        <div class="input-group">
                            <label>Reason <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select id="reject_order_reassign_reason" readOnly="">
                                    <option disabled="disabled" selected="selected">Select Reason</option>
                                    <?php //dd($vehicles); ?>
                                    
                                    <option value="order_rejected">
                                        Due To Order Rejected
                                    </option>
                                    <option value="breakdown">
                                        Due To Any Breakdown
                                    </option>
                                    <option value="material_out_of_stock">
                                        Due To Material Out Of Stock
                                    </option>
                                            
                                </select>
                                <input type="hidden" name="order_reassign_reason" id="reject_order_reassign_reason_field" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description Of Reason <span class="str">*</span></label>
                        <div class="text_box_icon">
                            <input type="text" name="order_reassign_desc" class="form-control" placeholder="Write Description Of Reason">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Select New Order Date <span class="str">*</span></label>
                        <div class="input-group">
                            <div class="range_picker m-r10" 
                            style="">
                                <input type="text" autocomplete="off"
                                id="new_reject_order_delivery_start_date"
                                name="new_reject_order_delivery_start_date"
                                    class="form-control"  placeholder="Select New Order Date">
                                <i class="fa fa-calendar"></i>
                            </div>                                

                            
                        </div>
                        
                        <div class="input-group" style="margin-top:10px;">
                            <select class="form-control reject_order_delivery_start_time" 
                            id="new_reject_order_delivery_time" 
                            name="new_reject_order_delivery_time" 
                            style="">
                                <option disabled="disabled" selected="selected" value=''>Start Time</option>

                                <option value="00:00">00:00</option>
                                <option value="01:00">01:00</option>
                                <option value="02:00">02:00</option>
                                <option value="03:00">03:00</option>
                                <option value="04:00">04:00</option>
                                <option value="05:00">05:00</option>
                                <option value="06:00">06:00</option>
                                <option value="07:00">07:00</option>
                                <option value="08:00">08:00</option>
                                <option value="09:00">09:00</option>
                                <option value="10:00">10:00</option>
                                <option value="11:00">11:00</option>

                                <option value="12:00">12:00</option>
                                <option value="13:00">13:00</option>
                                <option value="14:00">14:00</option>
                                <option value="15:00">15:00</option>
                                <option value="16:00">16:00</option>
                                <option value="17:00">17:00</option>
                                <option value="18:00">18:00</option>
                                <option value="19:00">19:00</option>
                                <option value="20:00">20:00</option>
                                <option value="21:00">21:00</option>
                                <option value="22:00">22:00</option>
                                <option value="23:00">23:00</option>

                            </select>

                        </div>
                    </div>
                    
                    
                    <input type="hidden" name="new_order_delivery_start_date" id="new_order_delivery_start_date"  />
                    <input type="hidden" name="new_order_delivery_end_date" id="new_order_delivery_end_date"  />
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer text-left">
                
                <input type="submit" class="site-button m-r20" value="Save">
            </div>
        </form>
      </div>
    </div>
</div>
<!-- MODAL ADD BANK DETAIL END-->
<script>
    $(function() {

        var today = new Date();
        var end = new Date();
        // today.setDate(today.getDate() + 2);
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = dd + '/' + mm + '/' + yyyy;
        end = dd + '/' + mm + '/' + yyyy;
        console.log(today);

        

        $('#new_reject_order_delivery_start_date').datepicker({
                autoclose: true,
                format: 'dd M yyyy'
                }).on('changeDate', function(selected){
                    
            
                    var selected_date_value = $('#new_reject_order_delivery_start_date').val();
                    var selected_time_value = $('#new_reject_order_delivery_time').val();
                    
                    console.log("selected_date_value......."+selected_date_value);
                    
                    if(selected_date_value != '' && selected_time_value != null){
                        RejectOrdercalcNewDeliveryEndData(selected_date_value,selected_time_value);

                        
                    }
                    // reassignMaterialValdation();
                    
            });

            $('.reject_order_delivery_start_time').change(function(){

                // var order_item_part_id = $(this).attr('data-item-part-id');

                var selected_time_value = $('option:selected', this).val();
                var selected_date_value = $('#new_reject_order_delivery_start_date').val();

                console.log("selected_date_value......."+selected_date_value);

                if(selected_date_value != ''){
                    RejectOrdercalcNewDeliveryEndData(selected_date_value,selected_time_value);

                    
                }


            });

            function RejectOrdercalcNewDeliveryEndData(selected_date,selected_start_time){


                // $previous_order_delivery_start_date = $("#previous_order_delivery_start_date_"+order_item_part_id).val();
                // $previous_order_delivery_end_date = $("#previous_order_delivery_end_date_"+order_item_part_id).val();

                // date1 = new Date($previous_order_delivery_start_date);
                // date2 = new Date($previous_order_delivery_end_date);

                // var diffHours = parseInt((date2 - date1) / (1000 * 60 * 60), 10); 
                var diffHours = 4; 

                // console.log("$previous_order_delivery_start_date......."+$previous_order_delivery_start_date);
                // console.log("$previous_order_delivery_end_date......."+$previous_order_delivery_end_date);
                // console.log("diffHours......."+diffHours);

                var delivery_date = new Date(selected_date);

                var dd = String(delivery_date.getDate()).padStart(2, '0');
                var mm = String(delivery_date.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = delivery_date.getFullYear();

                delivery_date = yyyy+"-"+mm+"-"+dd;

                var start_date_time = dd + '-' + mm + '-' + yyyy+" "+selected_start_time;

                var end_date_time = new Date(delivery_date+"T"+selected_start_time+":00+05:30");

                end_date_time.setHours(end_date_time.getHours() + parseInt(diffHours));

                var dd = String(end_date_time.getDate()).padStart(2, '0');
                var mm = String(end_date_time.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = end_date_time.getFullYear();
                var hours = end_date_time.getHours();
                var min = end_date_time.getMinutes();

                end_date_time = dd + '-' + mm + '-' + yyyy+" "+hours+":00";

                $("#new_order_delivery_start_date").val(start_date_time);
                $("#new_order_delivery_end_date").val(end_date_time);
                }
        });


</script>
@endif


<!-- Cement Rate Module Start -->

@if(Route::current()->getName() == 'profit_margin')

<!-- MODAL ADD Cement Rate START-->
<div id="add-profit-margin-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_profit_margin_title">Add Profit Margin</h4>
        </div>
        <form action="" name="add_profit_margin_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Profit Margin Percentage <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="margin_percentage" id="margin_percentage" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Profit Margin Percentage</option>
                                                                                        
                                            <option value="1">1.0 %</option>                                            
                                            <option value="2">2.0 %</option>                                            
                                            <option value="3">3.0 %</option>                                            
                                            <option value="4">4.0 %</option>                                            
                                            <option value="5">5.0 %</option>                                            
                                            <option value="6">6.0 %</option>                                            
                                            <option value="7">7.0 %</option>                                            
                                            <option value="8">8.0 %</option>                                            
                                            <option value="9">9.0 %</option>                                            
                                            <option value="10">10.0 %</option>                                            
                                            <option value="11">11.0 %</option>                                            
                                            <option value="12">12.0 %</option>                                            
                                            <option value="13">13.0 %</option>                                            
                                            <option value="14">14.0 %</option>                                            
                                            <option value="15">15.0 %</option>                                            
                                            
                                            <!-- <option value="0.5">0.5 %</option>
                                            <option value="1.0">1.0 %</option>
                                            <option value="1.5">1.5 %</option>
                                            <option value="2.0">2.0 %</option>
                                            <option value="2.5">2.5 %</option>
                                            <option value="3.0">3.0 %</option>
                                            <option value="3.5">3.5 %</option>
                                            <option value="4.0">4.0 %</option>
                                            <option value="4.5">4.5 %</option>
                                            <option value="5.0">5.0 %</option>
                                            <option value="5.5">5.5 %</option>
                                            <option value="6.0">6.0 %</option>
                                            <option value="6.5">6.5 %</option>
                                            <option value="7.0">7.0 %</option>
                                            <option value="7.5">7.5 %</option>
                                            <option value="8.0">8.0 %</option>
                                            <option value="8.5">8.5 %</option>
                                            <option value="9.0">9.0 %</option>
                                            <option value="9.5">9.5 %</option>
                                            <option value="10.0">10.0 %</option> -->
                                                
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>
                        
                    </div>
                    <!-- <div><b>Note:</b> <i>To activate, add stock more than zero and your selected cement brand status will be active. </i></div> -->
                </div>
            </div>
            <input type="hidden" name="add_profit_margin_form_type" id="add_profit_margin_form_type" value="add"/>
              <input type="hidden" name="profit_margin_id" id="profit_margin_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif

@if(Route::current()->getName() == 'overhead_margin')

<!-- MODAL ADD Cement Rate START-->
<div id="add-overhead-margin-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_overhead_margin_title">Add Overhead Margin</h4>
        </div>
        <form action="" name="add_overhead_margin_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Plant Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="address_id" id="address_id" class="form-control">
                                            <!-- <option disabled="disabled" selected="selected">Select Plant Address</option> -->
                                            @if(false)
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                    @endforeach
                                                @endif
                                            @endif

                                            @php

                                                $profile = session('supplier_profile_details', null);

                                            @endphp
                                            @if(!isset($profile["master_vendor_id"]))
                                                <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($data["address_data"]))
                                                    @foreach($data["address_data"] as $value)
                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                            <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Overhead Margin Percentage <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="margin_percentage" id="margin_percentage" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Overhead Margin Percentage</option>
                                                     
                                            
                                            <option value="0.5">0.5 %</option>
                                            <option value="1.0">1.0 %</option>
                                            <option value="1.5">1.5 %</option>
                                            <option value="2.0">2.0 %</option>
                                            <option value="2.5">2.5 %</option>
                                            <option value="3.0">3.0 %</option>
                                            <option value="3.5">3.5 %</option>
                                            <option value="4.0">4.0 %</option>
                                            <option value="4.5">4.5 %</option>
                                            <option value="5.0">5.0 %</option>
                                            <option value="5.5">5.5 %</option>
                                            <option value="6.0">6.0 %</option>
                                            <option value="6.5">6.5 %</option>
                                            <option value="7.0">7.0 %</option>
                                            <option value="7.5">7.5 %</option>
                                            <option value="8.0">8.0 %</option>
                                            <option value="8.5">8.5 %</option>
                                            <option value="9.0">9.0 %</option>
                                            <option value="9.5">9.5 %</option>
                                            <option value="10.0">10.0 %</option>
                                                
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="clearfix"></div>
                        
                    </div>
                    <!-- <div><b>Note:</b> <i>To activate, add stock more than zero and your selected cement brand status will be active. </i></div> -->
                </div>
            </div>
            <input type="hidden" name="add_overhead_margin_form_type" id="add_overhead_margin_form_type" value="add"/>
              <input type="hidden" name="overhead_margin_id" id="overhead_margin_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD Cement Rate END-->

@endif
