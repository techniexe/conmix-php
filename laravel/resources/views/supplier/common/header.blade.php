<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-dark toggle_btn">
            <i class="fa fa-bars"></i>
        </button>
        @php
            $profile = session('supplier_profile_details', null);
            //dd($profile);
        @endphp
        <div class="user_block">
            <div class="verify_un_image float-left">
                @if(isset($profile["verified_by_admin"]) && ($profile["verified_by_admin"] == true))
                    <img src="{{asset('assets/supplier/images/ic_verified.webp')}}" alt="Account Verified">
                @else
                    <img src="{{asset('assets/supplier/images/ic_unverified.webp')}}" alt="Account Verified">
                @endif
            </div>
        <div class="notification float-left position-relative">
                <a href="javascript:;" onclick="notificationClick()" class="notificationClick">
                    <i class="fa fa-bell"></i>
                    @if(isset($supplier_noti_count_data["data"]["unseen_message_count"]) && $supplier_noti_count_data["data"]["unseen_message_count"] > 0)
                    <span class="note_badge position-absolute" id="noti_count">
                        {{ isset($supplier_noti_count_data["data"]["unseen_message_count"]) ? ($supplier_noti_count_data["data"]["unseen_message_count"] > 9 ? '9+' : $supplier_noti_count_data["data"]["unseen_message_count"] ) : 0 }}
                    </span>
                    @endif
                </a>

                <div class="more_notification position-absolute pullDown" id="noti_details">
                    <div class="more_header p-3 float-left w-100">
                        <span class="float-left">You have {{ isset($supplier_noti_count_data["data"]["unseen_message_count"]) ? $supplier_noti_count_data["data"]["unseen_message_count"] : 0 }} new notifications</span>
                    </div>
                    <div id="noti_list_div">
                        <div class="overlay">
                            <span class="spinner"></span>
                        </div>

                    </div>
                    <div class="notification_footer text-center float-left w-100">
                        <a href="{{ route('supplier_notifications') }}" class="view_all position-relative float-left w-100 text-center">See all Notifications</a>
                    </div>
                </div>
            </div>

            <div class="user_icon float-left">
                <a href="javascript:;" class="userdropdownClick">

                    <input type="hidden" id="OTP_mob_number" value="{{ isset($profile["mobile_number"]) ? $profile["mobile_number"] : '' }}"/>
                    <img src="{{asset('assets/supplier/images/user-img.webp')}}">
                    <div class="header_com_name_outer">
                        <span class="header_com_name">{{ isset($profile["company_name"]) ? $profile["company_name"] : '' }}</span><br/>
                        <span class="float-left">{{ $profile["full_name"] }} </span>
                    </div>
                </a>
                <ul class="user_menu m-0 pullDown" style="display: none;">
                    <!--<li><a href="#"><i class="fa fa-cog"></i>Setting</a></li>-->

                    <li><a href="{{ route('supplier_profile') }}"><i class="fa fa-user"></i>My Profile</a></li>


                    <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i>Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>