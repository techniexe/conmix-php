
@if((Route::current()->getName() == 'supplier_dashboard') )

<script>

$('.from').datepicker({
	    autoclose: true,
	    minViewMode: 1,
	    format: 'M yyyy'
		}).on('changeDate', function(selected){
	        startDate = new Date(selected.date.valueOf());
	        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});


  var month_array = {

    "01" : 0,
    "02" : 1,
    "03" : 2,
    "04" : 3,
    "05" : 4,
    "06" : 5,
    "07" : 6,
    "08" : 7,
    "09" : 8,
    "10" : 9,
    "11" : 10,
    "12" : 11,

  };

var all_month_array = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

/*montly chart script start*/
var current_date = new Date();
var monthly_chart_selected_year = current_date.getFullYear();

var monthly_chart_order_status = "";

var product_monthly_chart_selected_year = current_date.getFullYear();

var product_monthly_chart_order_status = "";

var daily_chart_selected_year = all_month_array[current_date.getMonth()] + ", "+ current_date.getFullYear();

var daily_chart_order_status = "";

var product_daily_chart_selected_year = all_month_array[current_date.getMonth()] + ", "+ current_date.getFullYear();

var product_daily_chart_order_status = "";

function setDataInChart(order_data, sales_amount_data, quantity_data, div_id, title, categories,selected_year,order_status){

    var options = {
        chart: {
          fontFamily: 'Roboto',
          height: 350,
          type: 'bar',
          zoom: {
            enabled: false
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: [0.1, 0.1, 0.1],
          curve: 'straight',
          dashArray: [0, 0, 0]
        },
        series: [{
            name: "Orders",
            data: order_data
          },
          {
            name: "Quantity",
            data: quantity_data
          },
          {
            name: 'Sales Amount',
            data: sales_amount_data
          }
        ],
        title: {
          text: ''+title,
          align: 'center'
        },
        markers: {
          size: 0,
          hover: {
            sizeOffset: 5
          }
        },
        xaxis: {
          categories: categories,
          title: {
            text: ''
          }
        },
        yaxis: {
          title: {
            text: ''
          }
        },
        tooltip: {
          x: {
            show: true,            
            formatter: function (val) {
              // console.log(title);
              // console.log("val..."+val+"..."+selected_year+"..."+order_status);
              var mystatus = "";
              if(order_status.length > 0){
                mystatus = " | "+order_status;
              }
              return val + " " + selected_year + " " + mystatus;
            }
          

          },
          y: [{
            title: {
              formatter: function (val) {
                // console.log("val..."+val);
                return val + ""
              }
            }
          }, {
            title: {
              formatter: function (val) {
                // console.log("val..."+val);
                return val + ""
              }
            }
          }, {
            title: {
              formatter: function (val) {
                // console.log("val..."+val);
                return val;
              }
            }
          }]
        },
        grid: {
          borderColor: '#d3e0e9',
        },
        colors: ['#008ffb', '#00e396', '#feb019'],
        legend: {
          show: false
        }
      }
      
      // var chart = new ApexCharts(
      //   document.querySelector("#"+div_id),
      //   options

      // );
      // chart.render();
  
    return options;
}

var monthly_cat = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
var daily_cat = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];


var monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];

var product_monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var product_monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var product_monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];

var daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];


var product_daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var product_daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var product_daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];


// setDataInChart(daily_order_data, daily_sales_data, daily_quantity_data, 'daily-report-chart', "Daily Average");




var monthly_chart_option = setDataInChart(monthly_order_data, monthly_sales_data, monthly_quantity_data, 'monthly-report-chart', "Monthly Average", monthly_cat,monthly_chart_selected_year,monthly_chart_order_status);

var monthly_chart = new ApexCharts(
    document.querySelector("#monthly-report-chart"),
    monthly_chart_option

  );
  monthly_chart.render();

  checkMonthlyChartLegends();

  function checkMonthlyChartLegends() {
    var allLegends = document.querySelectorAll("#monthly_chart input[type='checkbox']")

    for(var i = 0; i < allLegends.length; i++) {
      if(!allLegends[i].checked) {
        console.log(allLegends[i].value);
        monthly_chart.toggleSeries(allLegends[i].value);
      }
    }
  }

  // toggleSeries accepts a single argument which should match the series name you're trying to toggle
  function toggleMonthlyChartSeries(checkbox) {
    monthly_chart.toggleSeries(checkbox.value);
  }
  
  
var daily_chart_option = setDataInChart(daily_order_data, daily_sales_data, daily_quantity_data, 'daily-report-chart', "Daily Average",daily_cat,daily_chart_selected_year,daily_chart_order_status);

var daily_chart = new ApexCharts(
    document.querySelector("#daily-report-chart"),
    daily_chart_option

  );
  daily_chart.render();

  checkDailyChartLegends();

  function checkDailyChartLegends() {
    var allLegends = document.querySelectorAll("#daily_chart_legend input[type='checkbox']")

    for(var i = 0; i < allLegends.length; i++) {
      if(!allLegends[i].checked) {
        console.log(allLegends[i].value);
        daily_chart.toggleSeries(allLegends[i].value);
      }
    }
  }

  // toggleSeries accepts a single argument which should match the series name you're trying to toggle
  function toggleDailyChartSeries(checkbox) {
    daily_chart.toggleSeries(checkbox.value);
  }


var product_monthly_chart_option = setDataInChart(product_monthly_order_data, product_monthly_sales_data, daily_quantity_data, 'product-monthly-report-chart', "Design Mix Monthly Average",monthly_cat,product_monthly_chart_selected_year,product_monthly_chart_order_status);

var product_monthly_chart = new ApexCharts(
    document.querySelector("#product-monthly-report-chart"),
    product_monthly_chart_option

  );
  product_monthly_chart.render();

  checkProductMonthlyChartLegends();

function checkProductMonthlyChartLegends() {
  var allLegends = document.querySelectorAll("#product_monthly_chart_legend input[type='checkbox']")

  for(var i = 0; i < allLegends.length; i++) {
    if(!allLegends[i].checked) {
      console.log(allLegends[i].value);
      product_monthly_chart.toggleSeries(allLegends[i].value);
    }
  }
}

// toggleSeries accepts a single argument which should match the series name you're trying to toggle
function toggleProductMonthlyChartSeries(checkbox) {
  product_monthly_chart.toggleSeries(checkbox.value);
}

  var product_daily_chart_option = setDataInChart(product_daily_order_data, product_daily_sales_data, product_daily_quantity_data, 'product-daily-report-chart', "Design Mix Daily Average",monthly_cat,product_daily_chart_selected_year,product_daily_chart_order_status);

var product_daily_chart = new ApexCharts(
    document.querySelector("#product-daily-report-chart"),
    product_daily_chart_option

  );
  product_daily_chart.render();

  checkProductDailyChartLegends();

function checkProductDailyChartLegends() {
  var allLegends = document.querySelectorAll("#product_daily_chart_legend input[type='checkbox']")

  for(var i = 0; i < allLegends.length; i++) {
    if(!allLegends[i].checked) {
      console.log(allLegends[i].value);
      product_daily_chart.toggleSeries(allLegends[i].value);
    }
  }
}

// toggleSeries accepts a single argument which should match the series name you're trying to toggle
function toggleProductDailyChartSeries(checkbox) {
  product_daily_chart.toggleSeries(checkbox.value);
}

var product_daily_chart;

function getMonthlyChartDetails(year,status){

  if(year != undefined){
    monthly_chart_selected_year = year;
  }
  
  if(status != undefined){
    monthly_chart_order_status = status;
  }
  
  

    $("#monthly_chart_loader").fadeIn(300);

    var request_data = {
      'year': year,
      'status': status
    };
    

    customResponseHandler(
        "supplier/getMonthlyChartData", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
          monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];
            // data = JSON.parse(data);
            console.log(data["data"]);
            
            $.each(data["data"], function(key,value){

              console.log(value);
              if(value["_id"] != undefined){
                var month = value["_id"].split("-")[1];
              }
              
              if(value["month"] != undefined){
                var month = value["month"];

                if(month != 10 && month != 11 && month != 12){
                    month = '0'+month;
                }
              }

              month = month_array[month];
              monthly_order_data[month] = value["count"];
              monthly_sales_data[month] = Math.round(value["total_amount"]);
              monthly_quantity_data[month] = Math.round(value["quantity"]);

            });

            if(data["data"] != undefined && data["data"].length == 0){
                monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];
            }

            $('#monthly_chart_order').prop('checked',true);
            $('#monthly_chart_quntity').prop('checked',true);
            $('#monthly_chart_salse').prop('checked',true);
            
            monthly_chart_option = setDataInChart(monthly_order_data, monthly_sales_data, monthly_quantity_data, 'monthly-report-chart', "Monthly Average", monthly_cat,monthly_chart_selected_year,monthly_chart_order_status);
          
            monthly_chart.updateOptions(monthly_chart_option,false,true);
            
            $("#monthly_chart_loader").fadeOut(300);
            
            

        }
    );

}


getMonthlyChartDetails();

$("#monthly_chart_data_year_filter").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  var monthly_chart_status = $("#monthly_chart_data_status_filter").val();

  // console.log("valueSelected..."+valueSelected+"..."+monthly_chart_status);

  getMonthlyChartDetails(valueSelected,monthly_chart_status);

});



$("#monthly_chart_data_status_filter").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  var monthly_chart_year = $("#monthly_chart_data_year_filter").val();

  console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year);

  getMonthlyChartDetails(monthly_chart_year,valueSelected);

});

function getDailyChartDetails(year,month,status){

  if(year != undefined){
    daily_chart_selected_year = month+ ", "+year;
  }
  
  if(status != undefined){
    daily_chart_order_status = status;
  }

  $("#daily_chart_loader").fadeIn(300);

  var request_data = {
      'year': year,
      'month': month,
      'status': status
    };

    customResponseHandler(
        "supplier/getDailyChartData", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
          daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            // data = JSON.parse(data);
            console.log(data["data"]);
            $.each(data["data"], function(key,value){

              console.log(value);
              if(value["_id"] != undefined){
                var day = value["_id"]["day"];
              }
              
              if(value["day"] != undefined){
                var day = value["day"];
              }

              if(day > 0){
                  day = day -1;
              }
              daily_order_data[day] = value["count"];
              daily_sales_data[day] = Math.round(value["total_amount"]);
              daily_quantity_data[day] = Math.round(value["quantity"]);

            });

            if(data["data"] != undefined && data["data"].length == 0){
              daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            }

            $('#daily_chart_legend_order').prop('checked',true);
            $('#daily_chart_legend_quantity').prop('checked',true);
            $('#daily_chart_legend_sales').prop('checked',true);

            daily_chart_option = setDataInChart(daily_order_data, daily_sales_data, daily_quantity_data, 'daily-report-chart', "Daily Average",daily_cat,daily_chart_selected_year,daily_chart_order_status);
            
            daily_chart.updateOptions(daily_chart_option,false,true);
            
            $("#daily_chart_loader").fadeOut(300);

        }
    );

}


getDailyChartDetails();


// $("#daily_chart_year_filter").change(function() {
//   var optionSelected = $("option:selected", this);
//   var valueSelected = this.value;
  
//   var monthly_chart_month = $("#daily_chart_month_filter").val();
//   var monthly_chart_status = $("#daily_chart_status_filter").val();

//   console.log("valueSelected..."+valueSelected+"..."+monthly_chart_month+"..."+monthly_chart_status);

//   getDailyChartDetails(valueSelected,monthly_chart_month, monthly_chart_status);

// });

var daily_month;
var daily_year;
$("#daily_chart_year_filter").datepicker().on('changeMonth', function(e){ 
   daily_month = new Date(e.date).getMonth() + 1;
   daily_month = all_month_array[daily_month-1];
   daily_year = String(e.date).split(" ")[3];
   console.log("daily_year..."+daily_year+"..."+daily_month);

   var monthly_chart_status = $("#daily_chart_status_filter").val();

   getDailyChartDetails(daily_year,daily_month, monthly_chart_status);
 });

// $("#daily_chart_month_filter").change(function() {
//   var optionSelected = $("option:selected", this);
//   var valueSelected = this.value;
  
//   var monthly_chart_year = $("#daily_chart_year_filter").val();
//   var monthly_chart_status = $("#daily_chart_status_filter").val();

//   console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year+"..."+monthly_chart_status);

//   getDailyChartDetails(monthly_chart_year,valueSelected, monthly_chart_status);

// });

$("#daily_chart_status_filter").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  // var monthly_chart_year = $("#daily_chart_year_filter").val();
  // var monthly_chart_month = $("#daily_chart_month_filter").val();

  console.log("valueSelected..."+valueSelected+"..."+daily_month+"..."+daily_year);

  getDailyChartDetails(daily_year,daily_month, valueSelected);

});


function getProductMonthlyChartDetails(year,concrete_grade_id,concrete_grade_name,product_monthly_chart_data_type_filter){

  if(year != undefined){
    product_monthly_chart_selected_year = year;
  }
  
  if(concrete_grade_name != undefined){
    product_monthly_chart_order_status = concrete_grade_name;
  }
  
  // if(product_monthly_chart_data_type_filter != undefined){
  //   product_monthly_chart_data_type_filter = product_monthly_chart_data_type_filter;
  // }

  $("#product_monthly_chart_loader").fadeIn(300);

  var request_data = {
      'year': year,
      'concrete_grade_id': concrete_grade_id,
      'design_mix_type': product_monthly_chart_data_type_filter
    };
    console.log(request_data);
  customResponseHandler(
      "supplier/getPorductBaseMonthlyChartData", // Ajax URl
      'GET', // Method call
      request_data, // Request data
      function success(data){ // onSuccess

        product_monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
            product_monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
            product_monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];
          // data = JSON.parse(data);
          console.log(data["data"]);
          $.each(data["data"], function(key,value){

            console.log(value);
            if(value["_id"] != undefined){
              var month = value["_id"].split("-")[1];
            }

            if(value["month"] != undefined){
              var month = value["month"];

              if(month != 10 && month != 11 && month != 12){
                  month = '0'+month;
              }
            }

            month = month_array[month];
            product_monthly_order_data[month] = value["count"];
            product_monthly_sales_data[month] = Math.round(value["total_amount"]);
            product_monthly_quantity_data[month] = Math.round(value["quantity"]);

          });

          if(data["data"] != undefined && data["data"].length == 0){
            product_monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
            product_monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
            product_monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];
          }

          $('#pro_month_order').prop('checked',true);
            $('#pro_month_quantity').prop('checked',true);
            $('#pro_month_sales').prop('checked',true);

          product_monthly_chart_option = setDataInChart(product_monthly_order_data, product_monthly_sales_data, product_monthly_quantity_data, 'product-monthly-report-chart', "Design Mix Monthly Average", monthly_cat,product_monthly_chart_selected_year,product_monthly_chart_order_status);
          
          product_monthly_chart.updateOptions(product_monthly_chart_option,false,true);
          
          $("#product_monthly_chart_loader").fadeOut(300);

      }
  );

}


getProductMonthlyChartDetails();


$("#product_monthly_chart_data_year_filter").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  var monthly_chart_sub_category_id = $("#concrete_grade_id").val();
  var monthly_chart_concrete_grade_name = $("#concrete_grade_id").find(':selected').attr('data-name');
  var product_monthly_chart_data_type_filter = $("#product_monthly_chart_data_type_filter").val();
  

  // console.log("valueSelected..."+valueSelected+"..."+monthly_chart_status);

  getProductMonthlyChartDetails(valueSelected,monthly_chart_sub_category_id,monthly_chart_concrete_grade_name,product_monthly_chart_data_type_filter);

});



$("#concrete_grade_id").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  var monthly_chart_year = $("#product_monthly_chart_data_year_filter").val();
  var monthly_chart_concrete_grade_name = $("#concrete_grade_id").find(':selected').attr('data-name');
  var product_monthly_chart_data_type_filter = $("#product_monthly_chart_data_type_filter").val();

  console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year);

  getProductMonthlyChartDetails(monthly_chart_year,valueSelected,monthly_chart_concrete_grade_name,product_monthly_chart_data_type_filter);

});

$("#product_monthly_chart_data_type_filter").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  var monthly_chart_year = $("#product_monthly_chart_data_year_filter").val();
  var monthly_chart_concrete_grade_name = $("#concrete_grade_id").val();
  var product_monthly_chart_data_type_filter = $("#product_monthly_chart_data_type_filter").val();

  console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year);

  getProductMonthlyChartDetails(monthly_chart_year,monthly_chart_concrete_grade_name,monthly_chart_concrete_grade_name,valueSelected);

});

// $("#sub_category_id").change(function() {
//   var optionSelected = $("option:selected", this);
//   var valueSelected = this.value;
  
//   var monthly_chart_year = $("#product_monthly_chart_data_year_filter").val();

//   console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year);

//   getProductMonthlyChartDetails(monthly_chart_year,valueSelected);

// });


function getProductDailyChartDetails(year,concrete_grade_id,month,concrete_grade_name,product_daily_chart_data_type_filter){

  if(year != undefined){
    product_daily_chart_selected_year = month+ ", "+year;
  }
  
  if(concrete_grade_name != undefined){
    product_daily_chart_order_status = concrete_grade_name;
  }

  $("#product_daily_chart_loader").fadeIn(300);

  var request_data = {
      'year': year,
      'concrete_grade_id': concrete_grade_id,
      'month': month,
      'design_mix_type' : product_daily_chart_data_type_filter
    };

  customResponseHandler(
      "supplier/getPorductBaseDailyChartData", // Ajax URl
      'GET', // Method call
      request_data, // Request data
      function success(data){ // onSuccess

        product_daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        product_daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        product_daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

          // data = JSON.parse(data);
          console.log(data["data"]);
          $.each(data["data"], function(key,value){

            console.log(value);

            if(value["_id"] != undefined){
              var day = value["_id"]["day"];
            }
            
            if(value["day"] != undefined){
              var day = value["day"];
            }

              if(day > 0){
                  day = day -1;
              }
              product_daily_order_data[day] = value["count"];
              product_daily_sales_data[day] = Math.round(value["total_amount"]);
              product_daily_quantity_data[day] = Math.round(value["quantity"]);

          });          
          

          if(data["data"] == undefined){
              product_daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              product_daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              product_daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
          }else if(data["data"].length == 0){
              product_daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              product_daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
              product_daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            }

            $('#pro_daily_order').prop('checked',true);
            $('#pro_daily_qty').prop('checked',true);
            $('#pro_daily_sales').prop('checked',true);

            product_daily_chart_option = setDataInChart(product_daily_order_data, product_daily_sales_data, product_daily_quantity_data, 'product-daily-report-chart', "Design Mix Daily Average", daily_cat,product_daily_chart_selected_year,product_daily_chart_order_status);
            
            product_daily_chart.updateOptions(product_daily_chart_option,false,true);
            
            $("#product_daily_chart_loader").fadeOut(300);
          

      }
  );

}


getProductDailyChartDetails();


// $("#product_daily_chart_data_year_filter").change(function() {
//   var optionSelected = $("option:selected", this);
//   var valueSelected = this.value;
  
//   var monthly_chart_sub_cat_id = $("#daily_sub_category_id").val();
//   // var monthly_chart_status = $("#daily_chart_status_filter").val();

//   console.log("valueSelected..."+valueSelected+"..."+product_daily_month+"..."+product_daily_year);

//   getProductDailyChartDetails(valueSelected,monthly_chart_sub_cat_id);

// });

// $("#daily_sub_category_id").change(function() {
//   var optionSelected = $("option:selected", this);
//   var valueSelected = this.value;
  
//   // var monthly_chart_year = $("#product_daily_chart_data_year_filter").val();

//   // console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year);

//   getProductDailyChartDetails(product_daily_year,valueSelected,product_daily_month);

// });

$("#daily_concrete_grade_id").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  var monthly_chart_concrete_grade_name = $("#daily_concrete_grade_id").find(':selected').attr('data-name');
  var product_daily_chart_data_type_filter = $("#daily_concrete_grade_id").val();

  // var monthly_chart_year = $("#product_daily_chart_data_year_filter").val();

  // console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year);

  getProductDailyChartDetails(product_daily_year,valueSelected,product_daily_month,monthly_chart_concrete_grade_name,product_daily_chart_data_type_filter);

});

var product_daily_month;
var product_daily_year;
$("#product_daily_chart_data_year_filter").datepicker().on('changeMonth', function(e){ 
   product_daily_month = new Date(e.date).getMonth() + 1;
   product_daily_month = all_month_array[product_daily_month-1];
   product_daily_year = String(e.date).split(" ")[3];
   console.log("product_daily_year..."+product_daily_year+"..."+product_daily_month);

   var monthly_chart_sub_cat_id = $("#daily_concrete_grade_id").val();
   var monthly_chart_concrete_grade_name = $("#daily_concrete_grade_id").find(':selected').attr('data-name');
   var product_daily_chart_data_type_filter = $("#daily_concrete_grade_id").val();

  getProductDailyChartDetails(product_daily_year,monthly_chart_sub_cat_id,product_daily_month,monthly_chart_concrete_grade_name,product_daily_chart_data_type_filter);
 });
 
 $("#product_daily_chart_data_type_filter").change(function() {
  //  product_daily_month = new Date(e.date).getMonth() + 1;
  //  product_daily_month = all_month_array[product_daily_month-1];
  //  product_daily_year = String(e.date).split(" ")[3];
  //  console.log("product_daily_year..."+product_daily_year+"..."+product_daily_month);

   var monthly_chart_sub_cat_id = $("#daily_concrete_grade_id").val();
   var monthly_chart_concrete_grade_name = $("#daily_concrete_grade_id").find(':selected').attr('data-name');
   var product_daily_chart_data_type_filter = $("#product_daily_chart_data_type_filter").val();

  getProductDailyChartDetails(product_daily_year,monthly_chart_sub_cat_id,product_daily_month,monthly_chart_concrete_grade_name,product_daily_chart_data_type_filter);
 });


</script>

@endif