<!-- SIDBAR NAV MENU START  -->
<nav id="sidebar" class="NW-sidebar-wrapper">
    <div class="sidebar-header">
        <h3 class="text-center"><a href="{{ route('supplier_dashboard') }}"><img src="{{asset('assets/supplier/images/logo3.webp') }}"><sup style="color: #fff;left: -40px;top: 0px;">BETA</sup></a></h3>
        <strong><a href="{{ route('supplier_dashboard') }}">
          <img src="{{asset('assets/supplier/images/small_logo.webp') }}"><sup style="color: #fff;">BETA</sup></a></strong>
          <div style="justify-content: center;display: grid;width: 100%;padding: 10px;"> 
            <span> Your RMC Partner! </span> 
          </div>
    </div>
    <div class="NW-sidebar-menu">
      
      @php

        $profile = session('supplier_profile_details', null);
        //dd($profile);
        //if(isset($profile["master_vendor_id"])){
          //  $data["master_vendor_id"] = $profile["master_vendor_id"];
        //}

        $fill_details = array();
        if(isset($vendors_fill_details['data'])){
          $fill_details = $vendors_fill_details['data'];
          
        }
        //dd($fill_details);

      @endphp
        <ul>
          <li class="{{ Route::current()->getName() == 'supplier_dashboard' ? 'active' : '' }}">
            <a href="{{ route('supplier_dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span>
            </a>

          </li>
          <li class="{{ Route::current()->getName() == 'orders_view' ? 'active' : '' }}">
            <a href="{{ route('orders_view') }}"><i class="fa fa-shopping-bag"></i><span>Order List</span>
            </a>
          </li>
          <li class="{{ Route::current()->getName() == 'surprise_orders_view' ? 'active' : '' }}">
            <a href="{{ route('surprise_orders_view') }}"><i class="fa fa-gift"></i><span>Surprise Order</span>
            </a>
          </li>
          @if(!isset($profile["master_vendor_id"]))
          <!-- background-color:#ff7682c2' -->
          <li class="{{ Route::current()->getName() == 'suppllier_billing_address_view' ? 'active' : '' }}" style="{{ isset($fill_details['billing_addr_count']) && $fill_details['billing_addr_count'] == 0 ? 'background-color:#ff7682c2' : '' }}">
            <a href="{{ route('suppllier_billing_address_view') }}"><i class="fa fa-map-signs"></i><span>Billing Address</span>
            </a>
          </li>
          <li class="{{ Route::current()->getName() == 'address_view' ? 'active' : '' }}" style="{{ isset($fill_details['plant_addr_count']) && $fill_details['plant_addr_count'] == 0 ? 'background-color:#ff7682c2' : '' }}">
            <a href="{{ route('address_view') }}"><i class="fa fa-map-signs"></i><span>Plant Address</span>
            </a>
          </li>
          @endif
          <li class="{{ Route::current()->getName() == 'design_mix_detail' ? 'active' : '' }}" style="{{ isset($fill_details['design_mix_count']) && $fill_details['design_mix_count'] == 0 ? 'background-color:#ff7682c2' : '' }}">
            <a href="{{ route('design_mix_detail') }}"><i class="fa fa-cubes"></i><span>Standard Design Mix (Add)</span>
            </a>
          </li>
          <li class="{{ Route::current()->getName() == 'supplier_driver_details' ? 'active' : '' }}" style="{{ isset($fill_details['TM_driver_count']) && $fill_details['TM_driver_count'] == 0 ? 'background-color:#ff7682c2' : '' }}">
            <a href="{{ route('supplier_driver_details') }}"><i class="fa fa-drivers-license"></i><span>TM Driver</span>
            </a>
          </li>
          <li class="{{ Route::current()->getName() == 'supplier_operator_details' ? 'active' : '' }}" style="{{ isset($fill_details['CP_operator_count']) && $fill_details['CP_operator_count'] == 0 ? 'background-color:#ff7682c2' : '' }}">
            <a href="{{ route('supplier_operator_details') }}"><i class="fa fa-drivers-license"></i><span>Concrete Pump Operator</span>
            </a>
          </li>
          <li class="{{ Route::current()->getName() == 'supplier_pumpgang_details' ? 'active' : '' }}" style="{{ isset($fill_details['CP_operator_count']) && $fill_details['CP_operator_count'] == 0 ? 'background-color:#ff7682c2' : '' }}">
            <a href="{{ route('supplier_pumpgang_details') }}"><i class="fa fa-drivers-license"></i><span>Concrete Pump Gang</span>
            </a>
          </li>
          <li class="{{ Route::current()->getName() == 'TM_detail' ? 'active' : '' }}" style="{{ isset($fill_details['TM_count']) && $fill_details['TM_count'] == 0 ? 'background-color:#ff7682c2' : '' }}">
            <a href="{{ route('TM_detail') }}"><i class="fa fa-truck"></i><span>Transit Mixer (TM)</span>
            </a>
          </li>
          <li class="{{ Route::current()->getName() == 'supplier_concrete_pump_details' ? 'active' : '' }}" style="{{ isset($fill_details['CP_count']) && $fill_details['CP_count'] == 0 ? 'background-color:#ff7682c2' : '' }}">
            <a href="{{ route('supplier_concrete_pump_details') }}"><i class="fa fa-truck"></i><span>Concrete Pump (CP)</span>
            </a>
          </li>
           
              @php

                $stock_color = "";
              @endphp
                @if ((isset($fill_details['cement_stock_count']))
                    && (isset($fill_details['sand_stock_count']))
                    && (isset($fill_details['aggregate_stock_count']))
                    && (isset($fill_details['fly_ash_stock_count']))
                    && (isset($fill_details['admixture_stock_count']))
                )
                
                  @if (($fill_details['cement_stock_count'] == 0)
                      || ($fill_details['sand_stock_count'] == 0)
                      || ($fill_details['aggregate_stock_count'] == 0)
                      || ($fill_details['fly_ash_stock_count'] == 0)
                      || ($fill_details['admixture_stock_count'] == 0)
                  )
                      @php $stock_color = 'background-color:#ff7682c2'; @endphp
                  @endif
                @endif

              

              @php $is_stock_active = ''; @endphp
                @if ((Route::current()->getName() == 'cement_stock')
                    || (Route::current()->getName() == 'sand_stock')
                    || (Route::current()->getName() == 'aggregate_stock')
                    || (Route::current()->getName() == 'flyash_stock')
                    || (Route::current()->getName() == 'admixture_stock')
                )
                    @php $is_stock_active = 'display:block'; @endphp
                @endif
          <li class="NW-sidebar-dropdown {{ $is_stock_active != '' ? 'active' : '' }}" style="{{ $stock_color }}">
              <a href="javascript:;"><i class="fa fa-line-chart"></i> <span>Material Stock Update</span></a>
                <!-- Submenu /-->
                <div class="NW-sidebar-submenu" style="{{ $is_stock_active }}">
                    <ul>
                      <li class="{{ Route::current()->getName() == 'cement_stock' ? 'subActive' : '' }}" style="{{  isset($fill_details['cement_stock_count']) ? ($fill_details['cement_stock_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('cement_stock') }}"><i class="fa fa-circle"></i> Cement Stock</a></li>
                      <li class="{{ Route::current()->getName() == 'sand_stock' ? 'subActive' : '' }}" style="{{  isset($fill_details['sand_stock_count']) ? ($fill_details['sand_stock_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('sand_stock') }}"><i class="fa fa-circle"></i> Coarse Sand Stock</a></li>
                      <li class="{{ Route::current()->getName() == 'aggregate_stock' ? 'subActive' : '' }}" style="{{  isset($fill_details['aggregate_stock_count']) ? ($fill_details['aggregate_stock_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('aggregate_stock') }}"><i class="fa fa-circle"></i> Aggregate Stock</a></li>
                      <li class="{{ Route::current()->getName() == 'flyash_stock' ? 'subActive' : '' }}" style="{{  isset($fill_details['fly_ash_stock_count']) ? ($fill_details['fly_ash_stock_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('flyash_stock') }}"><i class="fa fa-circle"></i> Fly Ash Stock</a></li>
                      <li class="{{ Route::current()->getName() == 'admixture_stock' ? 'subActive' : '' }}" style="{{  isset($fill_details['admixture_stock_count']) ? ($fill_details['admixture_stock_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('admixture_stock') }}"><i class="fa fa-circle"></i> Admixture Stock</a></li>


                    </ul>
                </div>
                <!-- Submenu end /-->
          </li>

              @php

                $rate_color = "";
              @endphp
                @if ((isset($fill_details['cement_rate_count']))
                    && (isset($fill_details['sand_rate_count']))
                    && (isset($fill_details['aggregate_rate_count']))
                    && (isset($fill_details['flyash_rate_count']))
                    && (isset($fill_details['admixture_rate_count']))
                    && (isset($fill_details['water_rate_count']))
                )
                
                  @if (($fill_details['cement_rate_count'] == 0)
                      || ($fill_details['sand_rate_count'] == 0)
                      || ($fill_details['aggregate_rate_count'] == 0)
                      || ($fill_details['flyash_rate_count'] == 0)
                      || ($fill_details['admixture_rate_count'] == 0)
                      || ($fill_details['water_rate_count'] == 0)
                  )
                      @php $rate_color = 'background-color:#ff7682c2'; @endphp
                  @endif
                @endif

              

          @php $is_rate_active = ''; @endphp
                @if ((Route::current()->getName() == 'cement_rate')
                    || (Route::current()->getName() == 'sand_rate')
                    || (Route::current()->getName() == 'aggregate_rate')
                    || (Route::current()->getName() == 'flyash_rate')
                    || (Route::current()->getName() == 'admixture_rate')
                    || (Route::current()->getName() == 'water_rate')
                )
                    @php $is_rate_active = 'display:block'; @endphp
                @endif

          <li class="NW-sidebar-dropdown {{ $is_rate_active != '' ? 'active' : '' }}" style="{{ $rate_color }}">
              <a href="javascript:;"><i class="fa fa-cube"></i> <span>RMC Raw Material Rate (Excl. GST)</span></a>
                <!-- Submenu /-->
                <div class="NW-sidebar-submenu" style="{{ $is_rate_active }}">
                    <ul>
                      <li class="{{ Route::current()->getName() == 'cement_rate' ? 'subActive' : '' }}" style="{{  isset($fill_details['cement_rate_count']) ? ($fill_details['cement_rate_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('cement_rate') }}"><i class="fa fa-circle"></i> Cement Per KG Rate</a></li>
                      <li class="{{ Route::current()->getName() == 'sand_rate' ? 'subActive' : '' }}" style="{{  isset($fill_details['sand_rate_count']) ? ($fill_details['sand_rate_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('sand_rate') }}"><i class="fa fa-circle"></i> Coarse Sand Per KG Rate</a></li>
                      <li class="{{ Route::current()->getName() == 'aggregate_rate' ? 'subActive' : '' }}" style="{{  isset($fill_details['aggregate_rate_count']) ? ($fill_details['aggregate_rate_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('aggregate_rate') }}"><i class="fa fa-circle"></i> Aggregate Per KG Rate</a></li>
                      <li class="{{ Route::current()->getName() == 'flyash_rate' ? 'subActive' : '' }}" style="{{  isset($fill_details['flyash_rate_count']) ? ($fill_details['flyash_rate_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('flyash_rate') }}"><i class="fa fa-circle"></i> Fly Ash Per KG Rate</a></li>
                      <li class="{{ Route::current()->getName() == 'admixture_rate' ? 'subActive' : '' }}" style="{{  isset($fill_details['admixture_rate_count']) ? ($fill_details['admixture_rate_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('admixture_rate') }}"><i class="fa fa-circle"></i> Admixture Per KG Rate</a></li>
                      <li class="{{ Route::current()->getName() == 'water_rate' ? 'subActive' : '' }}" style="{{  isset($fill_details['water_rate_count']) ? ($fill_details['water_rate_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('water_rate') }}"><i class="fa fa-circle"></i> Water Per LTR Rate</a></li>

                    </ul>
                </div>
                <!-- Submenu end /-->
            </li>

            @php

              $profit_overhead_margin_color = "";
              @endphp
              @if ((isset($fill_details['profit_margin_count']))
                  && (isset($fill_details['overhead_profit_count']))                  
              )

                @if (($fill_details['profit_margin_count'] == 0)
                    || ($fill_details['overhead_profit_count'] == 0)                    
                )
                    @php $profit_overhead_margin_color = 'background-color:#ff7682c2'; @endphp
                @endif
              @endif

            @php $is_margin_active = ''; @endphp
                @if ((Route::current()->getName() == 'profit_margin')
                    || (Route::current()->getName() == 'overhead_margin')
                )
                    @php $is_margin_active = 'display:block'; @endphp
                @endif
              <li class="NW-sidebar-dropdown {{ $is_margin_active != '' ? 'active' : '' }}" style="{{ $profit_overhead_margin_color }}">
                  <a href="javascript:;"><i class="fa fa-line-chart"></i> <span>Profit Overhead Margin</span></a>
                    <!-- Submenu /-->
                    <div class="NW-sidebar-submenu" style="{{ $is_margin_active }}">
                        <ul>
                          <li class="{{ Route::current()->getName() == 'profit_margin' ? 'subActive' : '' }}" style="{{  isset($fill_details['profit_margin_count']) ? ($fill_details['profit_margin_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('profit_margin') }}"><i class="fa fa-circle"></i> Profit Margin</a></li>
                          <li class="{{ Route::current()->getName() == 'overhead_margin' ? 'subActive' : '' }}" style="{{  isset($fill_details['overhead_profit_count']) ? ($fill_details['overhead_profit_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}"><a href="{{ route('overhead_margin') }}"><i class="fa fa-circle"></i> Overhead Margin</a></li>
                          


                        </ul>
                    </div>
                    <!-- Submenu end /-->
              </li>

              @php

                $setting_color = "";
              @endphp
                @if ((isset($fill_details['admix_setting_count']))
                    && (isset($fill_details['TM_price']))
                    && (isset($fill_details['CP_price']))
                )
                
                  @if (($fill_details['admix_setting_count'] == 0)
                      || ($fill_details['TM_price'] == 0)
                      || ($fill_details['CP_price'] == 0)
                  )
                      @php $setting_color = 'background-color:#ff7682c2'; @endphp
                  @endif
                @endif

              

            @if(isset($profile["master_vendor_id"]))
              <li class="{{ Route::current()->getName() == 'supplier_settings' ? 'active' : '' }}" style="{{ $setting_color }}">
                <a href="{{ route('supplier_settings') }}"><i class="fa fa-cogs"></i><span>Rate Setting</span>
                </a>
              </li>
            @endif
            @if(!isset($profile["master_vendor_id"]))
          <li class="{{ Route::current()->getName() == 'media_design_mix_detail' ? 'active' : '' }}">
            <a href="{{ route('media_design_mix_detail') }}"><i class="fa fa-file-photo-o"></i><span>Media Of Design Mix</span>
            </a>
          </li>
          
          <li class="{{ Route::current()->getName() == 'bank_details' ? 'active' : '' }}" style="{{  isset($fill_details['bank_info_count']) ? ($fill_details['bank_info_count'] == 0 ? 'background-color:#ff7682c2' : '') : '' }}">
            <a href="{{ route('bank_details') }}"><i class="fa fa-bank"></i><span>Bank Detail</span>
            </a>
          </li>
          @endif
          <!-- <li class="{{ Route::current()->getName() == 'supplier_payment_history' ? 'active' : '' }}">
            <a href="{{ route('supplier_payment_history') }}"><i class="fa fa-money"></i><span>Transaction
</span>
            </a>
          </li> -->
          <li class="{{ Route::current()->getName() == 'support_ticket_view' ? 'active' : '' }}">
            <a href="{{ route('support_ticket_view') }}"><i class="fa  fa-life-bouy"></i><span>Customer Support</span>
            </a>
          </li>
          <!-- <li class="{{ Route::current()->getName() == 'supplier_proposal' ? 'active' : '' }}">
            <a href="{{ route('supplier_proposal') }}"><i class="fa fa-edit"></i><span>Suggestion For New Product</span>
            </a>
          </li> -->

          @if(!isset($profile["master_vendor_id"]))
            <li class="{{ Route::current()->getName() == 'supplier_reports' ? 'active' : '' }}">
              <a href="{{ route('supplier_reports') }}"><i class="fa fa-file-text-o "></i><span>Report</span>
              </a>
            </li>
          @endif
          <li class="{{ Route::current()->getName() == 'supplier_faq' ? 'active' : '' }}">
            <a href="{{ route('supplier_faq') }}"><i class="fa fa-question-circle"></i><span>FAQs</span>
            </a>
          </li>

          <!-- <li class="{{ Route::current()->getName() == 'supplier_how_it_works' ? 'active' : '' }}">
            <a href="{{ route('supplier_how_it_works') }}"><i class="fa fa-lightbulb-o"></i><span>How it works</span>
            </a>
          </li> -->

          <!-- <li class="{{ Route::current()->getName() == 'supplier_product' ? 'active' : '' }}">
            <a href="{{ route('supplier_product') }}"><i class="fa fa-cubes"></i><span>Product</span>
            </a>
          </li>
          <li class="{{ Route::current()->getName() == 'contact_details' ? 'active' : '' }}">
            <a href="{{ route('contact_details') }}"><i class="fa fa-users"></i><span>Contact Detail</span>
            </a>
          </li>
           -->













        </ul>
      </div>
</nav>
<!-- SIDBAR NAV MENU END  -->