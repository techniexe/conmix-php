<?php use App\Http\Controllers\Admin\AdminController;?>
<?php 

    $notification_types = array(

        // 1 => "Dear, {{NAME}} Truck has been assigned to your order of id #{{ORDER_ID}}", 
        1 => "Hi {{SUPPLIER_NAME}}, you have received an order request, with an order Id #{{ORDER_ID}} from {{CLIENT_COMPANY_NAME}}. You may accept the order for further process.", //orderCreate
        2 => "Hi {{SUPPLIER_NAME}}, you have rejected the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by client {{CLIENT_COMPANY_NAME}}", //truckAssigned
        3 => "Hi {{SUPPLIER_NAME}}, you have assigned a Transit mixer for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by client {{CLIENT_COMPANY_NAME}}", //orderPickup
        4 => "Hi {{SUPPLIER_NAME}}, you have picked up the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by client {{CLIENT_COMPANY_NAME}}. This Product has to be delivered at their {{SITE_NAME}} site.", //orderReject
        5 => "Hi {{SUPPLIER_NAME}}, sorry to inform you that your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been rejected by your client {{CLIENT_COMPANY_NAME}}.", //orderDelay
        6 => "Hi {{SUPPLIER_NAME}}, the supply of product id #{{ORDER_ITEM}}, for client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}, has been delayed by {{DELAY_HOURS}} owing to {{REASON_FOR_DELAY}}.", //orderDelivered
        7 => "Hi {{SUPPLIER_NAME}}, you have partially delivered the product id #{{ORDER_ITEM}}, for your client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}. Payment for the delivered product will be processed soon.", //orderDelivered
        8 => "Hi {{SUPPLIER_NAME}}, you have fully delivered and successfully complete the order with an order id #{{ORDER_ID}} for your client {{CLIENT_COMPANY_NAME}}", //orderDelivered
        9 => "Hi {{SUPPLIER_NAME}}, your order with an order id #{{ORDER_ID}} has been fully cancelled by the client {{CLIENT_COMPANY_NAME}}", //orderDelivered
        10 => "Hi {{SUPPLIER_NAME}}, your supply order with product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} for the client {{CLIENT_COMPANY_NAME}} has been shortclosed by the Admin as the balance quantity is less than 3 cum.", //orderDelivered
        11 => "Hi {{SUPPLIER_NAME}}, you have assigned a CP for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by client {{CLIENT_COMPANY_NAME}}", //orderDelivered
        // 12 => "Hi {{SUPPLIER_NAME}}, you have picked up the CP to be delivered at {{SITE_NAME}} site for your client {{CLIENT_COMPANY_NAME}} for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}}.", //orderDelivered
        12 => "Hi {{SUPPLIER_NAME}}, you have picked up the CP to be delivered for your client {{CLIENT_COMPANY_NAME}} for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}}.", //orderDelivered
        // 13 => "Hi {{SUPPLIER_NAME}}, you have delivered the CP at {{SITE_NAME}} site for your client {{CLIENT_COMPANY_NAME}} for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}}.", //orderDelivered
        13 => "Hi {{SUPPLIER_NAME}}, you have delivered the CP for your client {{CLIENT_COMPANY_NAME}} for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}}.", //orderDelivered
        14 => "Hi {{SUPPLIER_NAME}} your client {{CLIENT_COMPANY_NAME}} has assigned the RMC Qty {{ASSIGNED_QTY}} cum for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}}.", //orderDelivered
        15 => "Hi {{SUPPLIER_NAME}}, CP has been delayed for the supply of product id #{{ORDER_ITEM}}, for client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}} by {{DELAY_HOURS}} owing to {{REASON_FOR_DELAY}}", //orderDelivered
        16 => "Hi {{SUPPLIER_NAME}}, your order for product id #{{ORDER_ITEM}} has been lapsed as your client {{CLIENT_COMPANY_NAME}} has failed to assign the qty.", //orderDelivered
        17 => "Hi {{SUPPLIER_NAME}}, your uploaded debit note for product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been verified and approved by Conmix.", //orderDelivered
        18 => "Hi {{SUPPLIER_NAME}}, your uploaded debit note for product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been rejected by Conmix. Kindly upload the correct debit note for the order.", //orderDelivered
        19 => "Hi {{SUPPLIER_NAME}}, Admin has replied to your support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        20 => "Hi {{SUPPLIER_NAME}}, Admin has replied to your support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        21 => "Hi {{SUPPLIER_NAME}}, you have reassigned the order, for  Product Id #{{ORDER_ITEM}} having an Order ID #{{ORDER_ID}}, owing to either breakdown of vehicle/plant, material out of stock or material rejected. You have sent a request to the buyer {{CLIENT_COMPANY_NAME}} to reassign his order. Please wait to accept or reject the request by buyer. Kindly note if rejected then order will be rejected and cancelled", //orderReassignRequest
        22 => "Hi {{SUPPLIER_NAME}}, your request for reassigning the order, for  Product Id #{{ORDER_ITEM}} having an Order ID #{{ORDER_ID}}, has been accepted by your buyer {{CLIENT_COMPANY_NAME}}", //orderReassignRequest Accepted
        23 => "Hi {{SUPPLIER_NAME}}, your request for reassigning the order for your buyer {{CLIENT_COMPANY_NAME}} with  Product Id #{{ORDER_ITEM}} having an Order ID #{{ORDER_ID}}, owing to either breakdown of vehicle/plant, material out of stock or material rejected has been rejected by your buyer {{CLIENT_COMPANY_NAME}}. The said affected part of the Product/Order now stands cancelled", //orderReassignRequest Rejected
        24 => "Hi {{SUPPLIER_NAME}}, your 7 days cube test report for the  Product Id #{{ORDER_ITEM}} having an  Order ID #{{ORDER_ID}} has been accepted by your client {{CLIENT_COMPANY_NAME}}", //7 days cube test report aceepted by Buyer
        25 => "Hi {{SUPPLIER_NAME}}, your 7 days cube test report for the Product Id #{{ORDER_ITEM}} having an  Order ID #{{ORDER_ID}}  has been rejected by your client {{CLIENT_COMPANY_NAME}} and has uploaded his own 7 days cube test report or has put his remarks. If the Cube Test Report of 28 days strenght is also rejected by the Buyer then your due payment for the said product/order will be cancelled and shall not be paid.", //7 days cube test report rejected by Buyer
        26 => "Hi {{SUPPLIER_NAME}}, Your 28 days cube test report for the  Product Id #{{ORDER_ITEM}} having an  Order ID #{{ORDER_ID}} has been accepted by your client {{CLIENT_COMPANY_NAME}}", //28 days cube test report accepted by Buyer
        26 => "Hi {{SUPPLIER_NAME}}, Kindly Attention! Your 28 days cube test report for the Product Id #{{ORDER_ITEM}} having an  Order ID #{{ORDER_ID}} has been rejected by your client {{CLIENT_COMPANY_NAME}} and has uploaded his own 28 days cube test report or has put his remarks. So as per Vendor Policy and its terms and conditions, your due payment for the said Product/Order shall not be paid.", //28 days cube test report rejected by Buyer

    );

    $notification_types_name = array(

        1 => "Order Placed",
        2 => "Order Rejected",
        3 => "TM Assigned",
        4 => "Order Pickup",
        5 => "Order Product Reject",
        6 => "Order Delay",
        7 => "Partially Product Delivered",
        8 => "Order Delivered",
        9 => "Order Cancelled",
        10 => "Order Short Close",
        11 => "CP Assigned",
        12 => "CP Pickup",
        13 => "CP Delivered",
        14 => "Assign Qty By Buyer",
        15 => "CP Delay",
        16 => "Order Item Lapsed",
        17 => "Debit Note Accept",
        18 => "Debit Note Reject",
        19 => "Reply Of Support Ticket By Admin",
        20 => "Resolve Support Ticket By Admin",
        21 => "Order Reassigning Request",
        22 => "Order Reassigning Request Accepted By Buyer",
        23 => "order Reassigning Request Rejected By Buyer",
        24 => "7 Days Cube Test Report Accepted By Buyer",
        25 => "7 Days Cube Test Report Rejected By Buyer",
        26 => "28 Days Cube Test Report Accepted By Buyer",
        27 => "28 Days Cube Test Report Rejected By Buyer",

    );

?>
<div class="more_header p-3 float-left w-100">
    <span class="float-left">
        <?php //dd($data["data"]["unseen_message_count"]); ?>
        @if($data["data"]["unseen_message_count"] > 0)
            You have {{ isset($data["data"]["unseen_message_count"]) ? $data["data"]["unseen_message_count"] : 0 }} new notifications
        @else
            Notifications
        @endif
    </span>
</div>
<div id="noti_list_div">
    
    <ul class="mCustomScrollbar list-unstyled float-left w-100 m-0">
        <?php //dd($data["data"]["notifications"]); ?>
        @if(isset($data["data"]["notifications"]) && count($data["data"]["notifications"]) > 0)
            @foreach($data["data"]["notifications"] as $value)
            
                <?php 
                    $msg = "";
                    $is_order = 1;
                    $msg = $notification_types[$value["notification_type"]];

                    if(isset($value["to_user"]["full_name"])){
                        $msg = str_replace("{{SUPPLIER_NAME}}",$value["to_user"]["full_name"],$msg);
                    }
                    
                    if(isset($value["vendor_order"]["_id"])){
                        $msg = str_replace("{{ORDER_ID}}",$value["vendor_order"]["_id"],$msg);
                    }
                    
                    if(isset($value["vendor_order"]["buyer_order_display_item_id"])){
                        $msg = str_replace("{{ORDER_ITEM}}",$value["vendor_order"]["buyer_order_display_item_id"],$msg);
                    }
                    
                    if(isset($value["order_item"]["display_item_id"])){
                        $msg = str_replace("{{ORDER_ITEM}}",$value["order_item"]["display_item_id"],$msg);
                    }

                    if(isset($value["assigned_quantity"])){
                        $msg = str_replace("{{ASSIGNED_QTY}}",$value["assigned_quantity"],$msg);
                    }
                    
                    if(isset($value["site"]["site_name"])){
                        $msg = str_replace("{{SITE_NAME}}",$value["site"]["site_name"],$msg);
                    }
                    
                    if(isset($value["CP_order_track"]["reasonForDelay"])){
                        $msg = str_replace("{{REASON_FOR_DELAY}}",$value["CP_order_track"]["reasonForDelay"],$msg);
                    }
                    
                    if(isset($value["CP_order_track"]["delayTime"])){
                        $msg = str_replace("{{DELAY_HOURS}}",$value["CP_order_track"]["delayTime"],$msg);
                    }

                    if(isset($value["client"])){

                        $account_type = $value["client_details"]["account_type"];

                        $client_name = "";

                        if($account_type == "Individual"){

                            $client_name = $value["client"]["full_name"];

                        }else{

                            $client_name = $value["client_details"]["company_name"];

                        }

                        $msg = str_replace("{{CLIENT_COMPANY_NAME}}",$client_name,$msg);
                        
                    }

                    if(isset($value["support_ticket"]["ticket_id"])){
                        $is_order = 0;
                        $msg = str_replace("{{TICKET_NO}}",$value["support_ticket"]["ticket_id"],$msg);
                    }
                    
                    // $created = date('d M Y, h:i', strtotime($value["created_at"]));

                    $created = AdminController::dateTimeFormat($value["created_at"]);

                    
                ?>

                <li class="float-left w-100" style="<?php echo $value['is_seen'] == 0 ? 'background: #e8f1fb;' : '' ?>">
                    @if(isset($value['vendor_order']['_id']))
                        <a href="{{route('supplier_orders_details',$value['vendor_order']['_id'])}}" onclick="updateNotiStatus('{{ $value['_id'] }}')" class="float-left w-100">                                        
                            <i class="fa fa-info-circle"></i> {{$msg}} {{$created}}                                        
                        </a>
                    @else
                        <a href="javascript:void(0)" onclick="updateNotiStatus('{{ $value['_id'] }}')" class="float-left w-100">                                        
                            <i class="fa fa-info-circle"></i> {{$msg}} {{$created}}                                        
                        </a>   
                    @endif
                </li>
            @endforeach
        @else

            <li class="float-left w-100">
                
                Notifications Empty

            </li>       

        @endif
        
    </ul>
</div>
<div class="notification_footer text-center float-left w-100">
    <a href="{{ route('supplier_notifications') }}" class="view_all position-relative float-left w-100 text-center">See all Notifications</a>
</div>