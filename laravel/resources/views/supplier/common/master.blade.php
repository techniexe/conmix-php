<!doctype html>
<html lang="en">
<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />
    <meta name="description" content="" />
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
    
    <!-- FAVICONS ICON -->
    <link rel="icon" type="image/webp" href="{{asset('assets/supplier/images/favicon-32x32.webp') }}" sizes="32x32" />
    <link rel="icon" type="image/webp" href="{{asset('assets/supplier/images/favicon-16x16.webp') }}" sizes="16x16" />
    
    <title>ConMix | @yield('title')</title>
    
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/supplier/css/css_cache.php')}}" />
    @if(false)
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/common/css/shimmer.css')}}" async>
        <!-- BOOTSTRAP STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/supplier/css/bootstrap.min.css') }}">
        <!-- FONTAWESOME STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/supplier/css/font-awesome.min.css') }}"/>
        <!-- CUSTOM SCROLLBAR STYLE SHEET -->
        <link rel="preload" href="{{asset('assets/supplier/css/jquery.mCustomScrollbar.css') }}">
        <!-- MAIN STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/supplier/css/style.css') }}">
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/supplier/css/custom.css')}}">
        
        <!-- NEW THEME STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/supplier/css/new-theme-style.css') }}">
    
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/common/css/jquery.toast.css')}}">

        <!-- SELECT2 CHOSEN STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/select2-chosen.css')}}">

        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/daterangepicker.css')}}" />
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
    @endif
    
    @if(Route::current()->getName() == 'address_view'
         || Route::current()->getName() == 'address_add'
         || Route::current()->getName() == 'address_edit'
    )
    
    <script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDgc0AfDO-QPVxfx03eXhLBphZ3CKwYp5o&sensor=false&libraries=places&callback=Function.prototype'></script>
    <!-- <script type="text/javascript" src="{{asset('assets/common/js/locationpicker.jquery.js')}}"></script> -->

    @endif

    <script type="text/javascript" src="{{asset('assets/supplier/js/js_cache.php')}}" ></script>

    @if(Route::current()->getName() == 'address_view'
         || Route::current()->getName() == 'address_add'
         || Route::current()->getName() == 'address_edit'
    )
    
    <!-- <script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDgc0AfDO-QPVxfx03eXhLBphZ3CKwYp5o&sensor=false&libraries=places&callback=Function.prototype'></script> -->
    <script type="text/javascript" src="{{asset('assets/common/js/locationpicker.jquery.js')}}"></script>

    @endif

    @if(false)
    <script type="text/javascript" src="{{asset('assets/supplier/js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDgc0AfDO-QPVxfx03eXhLBphZ3CKwYp5o&sensor=false&libraries=places&callback=Function.prototype'></script>
    <script type="text/javascript" src="{{asset('assets/common/js/locationpicker.jquery.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/supplier/js/jquery.lineProgressbar.js') }}"></script>

    
    <script type="text/javascript" src="{{asset('assets/admin/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/daterangepicker.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/supplier/js/jquery.lightbox.js') }}"></script>

    
    <script type="text/javascript" src="{{asset('assets/common/js/utils.js') }}"></script>
    <script type="text/javascript" src="{{asset('assets/supplier/js/supplier.js') }}"></script>

    <script language="javascript">
        // setTimeout(function(){
        //     window.location.reload(1);
        // }, 30000);
    </script>
    @endif


    @if(env('APP_ENV', 'production') == 'production')

         <!-- Google tag (gtag.js) -->
         <script async src="https://www.googletagmanager.com/gtag/js?id=G-L18WBJ16M8"></script>
         <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'G-L18WBJ16M8');
         </script>

        <script type="text/javascript">
            (function(c,l,a,r,i,t,y){
                c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
                t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
                y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
            })(window, document, "clarity", "script", "m223lx17us");
        </script>

    @endif
</head>
<script>

var BASE_URL = '{{ url('/') }}';
// console.log("BASE_URL..."+BASE_URL);

</script>