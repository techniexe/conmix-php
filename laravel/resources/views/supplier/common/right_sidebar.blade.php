

@if((Route::current()->getName() == 'orders_view') || (Route::current()->getName() == 'surprise_orders_view'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="order_status">
                            <option disabled="disabled" selected="selected">Select Order status</option>
                            <option {{ Request::get('order_status') == 'RECEIVED' ? 'selected' : '' }} value="RECEIVED">RECEIVED</option>
                            <option {{ Request::get('order_status') == 'ACCEPTED' ? 'selected' : '' }} value="ACCEPTED">ACCEPTED</option>
                            <option {{ Request::get('order_status') == 'REJECTED' ? 'selected' : '' }} value="REJECTED">REJECTED</option>
                            <option {{ Request::get('order_status') == 'TM_ASSIGNED' ? 'selected' : '' }} value="TM_ASSIGNED">TM_ASSIGNED</option>
                            <option {{ Request::get('order_status') == 'PICKUP' ? 'selected' : '' }} value="PICKUP">PICKUP</option>
                            <option {{ Request::get('order_status') == 'DELAYED' ? 'selected' : '' }} value="DELAYED">DELAYED</option>
                            <option {{ Request::get('order_status') == 'DELIVERED' ? 'selected' : '' }} value="DELIVERED">DELIVERED</option>
                            <option {{ Request::get('order_status') == 'CANCELLED' ? 'selected' : '' }} value="CANCELLED">CANCELLED</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_status">
                            <option disabled="disabled" selected="selected">Select Payment Status</option>
                            <option value="PAID" {{ Request::get('payment_status') == 'PAID' ? 'selected' : '' }}>PAID</option>
                            <option value="UNPAID" {{ Request::get('payment_status') == 'UNPAID' ? 'selected' : '' }}>UNPAID</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Name</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="order_id" value="{{ Request::get('order_id') ? Request::get('order_id') : ''  }}" class="form-control" placeholder="My Order Id">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="buyer_id" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : ''  }}" class="form-control" placeholder="Buyer Order Id">
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <input type="text" autocomplete="off" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" class="form-control" placeholder="Buyer Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" autocomplete="off" id="order_year_filter" name="year" class="form-control"  placeholder="Select Year">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" autocomplete="off" id="order_month_filter" name="month" class="form-control"  placeholder="Select Month">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" autocomplete="off" id="order_date_filter" name="date" class="form-control"  placeholder="Select Date">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->
<script>

$('#order_year_filter').datepicker({
	    autoclose: true,
        minViewMode: "years",
	    format: 'yyyy'
		}).on('changeDate', function(selected){
	        // startDate = new Date(selected.date.valueOf());
	        // startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});

@if(Request::get('year'))

    $("#order_year_filter").datepicker("setDate",new Date({{Request::get('year')}}, 1)).trigger('change');

@endif

    $('#order_month_filter').datepicker({
	    autoclose: true,
        minViewMode: "months",
	    format: 'M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('month'))

    var date = new Date('{{Request::get('month')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_month_filter").datepicker("setDate",new Date(yyyy,mm, 1)).trigger('change');

@endif

    $('#order_date_filter').datepicker({
	    autoclose: true,
	    format: 'dd M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('date'))

    var date = new Date('{{Request::get('date')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_date_filter").datepicker("setDate",new Date(yyyy,mm, dd, 1)).trigger('change');

@endif

</script>


@endif

@if((Route::current()->getName() == 'suppllier_billing_address_view'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="city_id">
                            <option disabled="disabled" selected="selected">Select City</option>
                            @if($city_data["data"])
                                @foreach($city_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('city_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["city_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_billing_address_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="city_id">
                            <option disabled="disabled" selected="selected">Select City</option>
                            @if($city_data["data"])
                                @foreach($city_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('city_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["city_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_bank_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf
            
            
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'address_view'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="search" value="{{ Request::get('search') ? Request::get('search') : ''  }}" class="form-control" placeholder="Plant Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="city_id">
                            <option disabled="disabled" selected="selected">Select City</option>
                            @if($city_data["data"])
                                @foreach($city_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('city_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["city_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_plant_address_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="search" value="{{ Request::get('search') ? Request::get('search') : ''  }}" class="form-control" placeholder="Plant Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="city_id">
                            <option disabled="disabled" selected="selected">Select City</option>
                            @if($city_data["data"])
                                @foreach($city_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('city_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["city_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif


@if((Route::current()->getName() == 'design_mix_detail'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'TM_detail_1'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_concrete_pump_details'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="concrete_pump_category_id">
                            <option disabled="disabled" selected="selected">Select Concrete Pump Category</option>
                            @if($data["category_data"])
                                @foreach($data["category_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('concrete_pump_category_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["category_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_CP_listing_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="concrete_pump_category_id">
                            <option disabled="disabled" selected="selected">Select Concrete Pump Category</option>
                            @if($data["category_data"])
                                @foreach($data["category_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('concrete_pump_category_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["category_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'cement_stock'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="cement_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="brand_id" id="right_side_cement_brand_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Cement Brand</option>
                            @if(isset($data["cement_brand_data"]))
                                @foreach($data["cement_brand_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="grade_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Cement Grade</option>
                            @if(isset($data["cement_grade_data"]))
                                @foreach($data["cement_grade_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_cement_stock_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="brand_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Cement Brand</option>
                            @if(isset($data["cement_brand_data"]))
                                @foreach($data["cement_brand_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="grade_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Cement Grade</option>
                            @if(isset($data["cement_grade_data"]))
                                @foreach($data["cement_grade_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('grade_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'sand_stock'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="sand_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" id="right_side_sand_source_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                            @if(isset($data["sand_source_data"]))
                                @foreach($data["sand_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['sand_source_name'] }}">{{ $value["sand_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_sand_stock_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                            @if(isset($data["sand_source_data"]))
                                @foreach($data["sand_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sand_source_name'] }}">{{ $value["sand_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'aggregate_stock'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="aggregate_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" id="right_side_aggregate_source_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Source</option>
                            @if(isset($data["aggregate_source_data"]))
                                @foreach($data["aggregate_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['aggregate_source_name'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["aggregate_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="sub_cat_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Sub Category</option>
                            @if(isset($data["aggregate_sub_cat_data"]))
                                @foreach($data["aggregate_sub_cat_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_agg_stock_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Source</option>
                            @if(isset($data["aggregate_source_data"]))
                                @foreach($data["aggregate_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['aggregate_source_name'] }}">{{ $value["aggregate_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="sub_cat_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Sub Category</option>
                            @if(isset($data["aggregate_sub_cat_data"]))
                                @foreach($data["aggregate_sub_cat_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'flyash_stock'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="fly_ash_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" id="right_side_fly_ash_source_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Fly Ash Source</option>
                            @if(isset($data["fly_ash_source_data"]))
                                @foreach($data["fly_ash_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['fly_ash_source_name'] }}">{{ $value["fly_ash_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif


@if((Route::current()->getName() == 'supplier_flyAsh_stock_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Fly Ash Source</option>
                            @if(isset($data["fly_ash_source_data"]))
                                @foreach($data["fly_ash_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['fly_ash_source_name'] }}">{{ $value["fly_ash_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'admixture_stock')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="admix_brand_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="brand_id" id="right_side_admix_brand_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                            @if(isset($data["admixture_brand_data"]))
                                @foreach($data["admixture_brand_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="category_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Admixture Types</option>
                            @if(isset($data["admixture_types_data"]))
                                @foreach($data["admixture_types_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('category_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["category_name"] }} - {{ $value["admixture_type"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_admix_stock_report')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="brand_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                            @if(isset($data["admixture_brand_data"]))
                                @foreach($data["admixture_brand_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('brand_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="category_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Admixture Types</option>
                            @if(isset($data["admixture_types_data"]))
                                @foreach($data["admixture_types_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('category_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["category_name"] }} - {{ $value["admixture_type"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="quantity_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" placeholder="Quantity">
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'cement_rate')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="cement_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="brand_id" id="right_side_cement_brand_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Cement Brand</option>
                            @if(isset($data["cement_brand_data"]))
                                @foreach($data["cement_brand_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="grade_id"  class="form-control">
                            <option disabled="disabled" selected="selected">Select Cement Grade</option>
                            @if(isset($data["cement_grade_data"]))
                                @foreach($data["cement_grade_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="per_kg_rate_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="per_kg_rate" value="{{ Request::get('per_kg_rate') ? Request::get('per_kg_rate') : ''  }}" class="form-control" placeholder="Per KG Rate">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'sand_rate'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="sand_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" id="right_side_sand_source_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                            @if(isset($data["sand_source_data"]))
                                @foreach($data["sand_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['sand_source_name'] }}">{{ $value["sand_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="per_kg_rate_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="per_kg_rate" value="{{ Request::get('per_kg_rate') ? Request::get('per_kg_rate') : ''  }}" class="form-control" placeholder="Per KG Rate">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'aggregate_rate')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="aggregate_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" id="right_side_aggregate_source_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Source</option>
                            @if(isset($data["aggregate_source_data"]))
                                @foreach($data["aggregate_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['aggregate_source_name'] }}">{{ $value["aggregate_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="sub_cat_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Sub Category</option>
                            @if(isset($data["aggregate_sub_cat_data"]))
                                @foreach($data["aggregate_sub_cat_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="per_kg_rate_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="per_kg_rate" value="{{ Request::get('per_kg_rate') ? Request::get('per_kg_rate') : ''  }}" class="form-control" placeholder="Per KG Rate">
                    </div>
                </div>
            </div>
            

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'flyash_rate'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="fly_ash_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="source_id" id="right_side_fly_ash_source_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Flyash Source</option>
                            @if(isset($data["fly_ash_source_data"]))
                                @foreach($data["fly_ash_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('source_id') == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['fly_ash_source_name'] }}">{{ $value["fly_ash_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="per_kg_rate_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="per_kg_rate" value="{{ Request::get('per_kg_rate') ? Request::get('per_kg_rate') : ''  }}" class="form-control" placeholder="Per KG Rate">
                    </div>
                </div>
            </div>
            

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'admixture_rate')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="admix_brand_filter_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="brand_id" id="right_side_admix_brand_selection" class="form-control">
                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                            @if(isset($data["admixture_brand_data"]))
                                @foreach($data["admixture_brand_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="category_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Admixture Types</option>
                            @if(isset($data["admixture_types_data"]))
                                @foreach($data["admixture_types_data"] as $value)
                                    <option value="{{ $value['_id'] }}" >{{ $value["category_name"] }} - {{ $value["admixture_type"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="per_kg_rate_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="per_kg_rate" value="{{ Request::get('per_kg_rate') ? Request::get('per_kg_rate') : ''  }}" class="form-control" placeholder="Per KG Rate">
                    </div>
                </div>
            </div>
            

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif


@if((Route::current()->getName() == 'water_rate')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="per_kg_rate_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" name="per_kg_rate" value="{{ Request::get('per_kg_rate') ? Request::get('per_kg_rate') : ''  }}" class="form-control" placeholder="Per KG Rate">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_driver_details')
    || (Route::current()->getName() == 'supplier_operator_details')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($sub_vendors["data"])
                            
                                @foreach($sub_vendors["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'supplier_operator_listing_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($sub_vendors["data"])                            
                                @foreach($sub_vendors["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif


@if((Route::current()->getName() == 'supplier_TM_driver_report')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($sub_vendors["data"])
                            
                                @foreach($sub_vendors["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if((Route::current()->getName() == 'TM_detail'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($sub_vendors["data"])
                                @foreach($sub_vendors["data"] as $value)
                                    <option value="{{ $value['plant_address_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" id="">
                    <div class="cstm-select-box">
                        <select name="TM_category_id">
                            <option disabled="disabled" selected="selected">Select TM Category</option>
                            @if(isset($data["vehicle_categoies"]["data"]))
                                @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group" id="">
                    <div class="cstm-select-box">
                        <select name="TM_sub_category_id">
                            <option disabled="disabled" selected="selected">Select TM Sub Category</option>
                            @if(isset($data["subcategories"]["data"]))
                                @foreach($data["subcategories"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_sub_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('TM_rc_number') ? Request::get('TM_rc_number') : ''  }}" name="TM_rc_number" class="form-control" placeholder="Vehicle Number">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif


@if((Route::current()->getName() == 'supplier_TM_listing_report'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($sub_vendors["data"])
                                @foreach($sub_vendors["data"] as $value)
                                    <option value="{{ $value['plant_address_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" id="">
                    <div class="cstm-select-box">
                        <select name="TM_category_id">
                            <option disabled="disabled" selected="selected">Select TM Category</option>
                            @if(isset($data["vehicle_categoies"]["data"]))
                                @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" id="">
                    <div class="cstm-select-box">
                        <select name="TM_sub_category_id">
                            <option disabled="disabled" selected="selected">Select TM Sub Category</option>
                            @if(isset($data["subcategories"]["data"]))
                                @foreach($data["subcategories"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_sub_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('TM_rc_number') ? Request::get('TM_rc_number') : ''  }}" name="TM_rc_number" class="form-control" placeholder="Vehicle Number">
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif




<!-- Support Ticket Module Start -->

@if(Route::current()->getName() == 'support_ticket_view')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="severity">
                            <option disabled="disabled" name="severity" selected="selected">Select Priority</option>
                            <option value="Urgent" {{ Request::get('severity') == 'Urgent' ? 'selected' : ''  }}>Urgent</option>
                            <option value="High" {{ Request::get('severity') == 'High' ? 'selected' : ''  }}>High</option>
                            <option value="Low" {{ Request::get('severity') == 'Low' ? 'selected' : ''  }}>Normal</option>
                            <option value="Normal" {{ Request::get('severity') == 'Normal' ? 'selected' : ''  }}>Low</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="subject" value="{{ Request::get('subject') ? Request::get('subject') : ''  }}" placeholder="Subject">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="ticket_id" value="{{ Request::get('ticket_id') ? Request::get('ticket_id') : ''  }}" placeholder="Ticket Id">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select>
                            <option disabled="disabled" selected="selected">Filter by status</option>
                            <option>Unassigned</option>
                            <option>Pending action</option>
                            <option>Work in progress</option>
                            <option>Resolved</option>
                            <option>Reopened</option>
                        </select>
                    </div>
                </div>
            </div> -->

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button btn_bg_color">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->





@endif


@if(Route::current()->getName() == 'supplier_design_mix_report')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="grade_id">
                            <option disabled="disabled" selected="selected">Select Concrete Grade</option>
                            @if($concrete_grade_data)
                                @foreach($concrete_grade_data as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="cement_brand_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Cement Brand</option>
                            @if(isset($data["cement_brand_data"]))
                                @foreach($data["cement_brand_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="cement_grade_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Cement Grade</option>
                            @if(isset($data["cement_grade_data"]))
                                @foreach($data["cement_grade_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="sand_source_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                            @if(isset($data["sand_source_data"]))
                                @foreach($data["sand_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sand_source_name'] }}">{{ $value["sand_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="aggregate_source_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Source</option>
                            @if(isset($data["aggregate_source_data"]))
                                @foreach($data["aggregate_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['aggregate_source_name'] }}">{{ $value["aggregate_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="aggregate1_sub_category_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Sub Category 1</option>
                            @if(isset($data["aggregate_sub_cat_data"]))
                                @foreach($data["aggregate_sub_cat_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="aggregate2_sub_category_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Aggregate Sub Category 2</option>
                            @if(isset($data["aggregate_sub_cat_data"]))
                                @foreach($data["aggregate_sub_cat_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="fly_ash_source_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Fly Ash Source</option>
                            @if(isset($data["fly_ash_source_data"]))
                                @foreach($data["fly_ash_source_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['fly_ash_source_name'] }}">{{ $value["fly_ash_source_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="ad_mixture_brand_id" class="form-control">
                            <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                            @if(isset($data["admixture_brand_data"]))
                                @foreach($data["admixture_brand_data"] as $value)
                                    <option value="{{ $value['_id'] }}" data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button btn_bg_color">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->





@endif


<!-- Support Ticket Module Over -->
<!-- Report Module Over -->

@if(Route::current()->getName() == 'supplier_order_list_report')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="order_status">
                            <option disabled="disabled" selected="selected">Select Order status</option>
                            <option {{ Request::get('order_status') == 'RECEIVED' ? 'selected' : '' }} value="RECEIVED">RECEIVED</option>
                            <option {{ Request::get('order_status') == 'ACCEPTED' ? 'selected' : '' }} value="ACCEPTED">ACCEPTED</option>
                            <option {{ Request::get('order_status') == 'REJECTED' ? 'selected' : '' }} value="REJECTED">REJECTED</option>
                            <option {{ Request::get('order_status') == 'TM_ASSIGNED' ? 'selected' : '' }} value="TM_ASSIGNED">TM_ASSIGNED</option>
                            <option {{ Request::get('order_status') == 'PICKUP' ? 'selected' : '' }} value="PICKUP">PICKUP</option>
                            <option {{ Request::get('order_status') == 'DELAYED' ? 'selected' : '' }} value="DELAYED">DELAYED</option>
                            <option {{ Request::get('order_status') == 'DELIVERED' ? 'selected' : '' }} value="DELIVERED">DELIVERED</option>
                            <option {{ Request::get('order_status') == 'CANCELLED' ? 'selected' : '' }} value="CANCELLED">CANCELLED</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_status">
                            <option disabled="disabled" selected="selected">Select Payment Status</option>
                            <option value="PAID" {{ Request::get('payment_status') == 'PAID' ? 'selected' : '' }}>PAID</option>
                            <option value="UNPAID" {{ Request::get('payment_status') == 'UNPAID' ? 'selected' : '' }}>UNPAID</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="address_id">
                            <option disabled="disabled" selected="selected">Select Plant Name</option>
                            @if($plant_address["data"])
                                @foreach($plant_address["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('address_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }},{{ $value["line1"] }},{{ $value["line2"] }},{{ $value["cityDetails"]["city_name"] }},{{ $value["stateDetails"]["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="order_id" value="{{ Request::get('order_id') ? Request::get('order_id') : ''  }}" class="form-control" placeholder="My Order Id">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="buyer_id" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : ''  }}" class="form-control" placeholder="Buyer Order Id">
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <input type="text" autocomplete="off" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" class="form-control" placeholder="Buyer Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" autocomplete="off" id="order_year_filter" name="year" class="form-control"  placeholder="Select Year">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" autocomplete="off" id="order_month_filter" name="month" class="form-control"  placeholder="Select Month">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" autocomplete="off" id="order_date_filter" name="date" class="form-control"  placeholder="Select Date">
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->
<script>

$('#order_year_filter').datepicker({
	    autoclose: true,
        minViewMode: "years",
	    format: 'yyyy'
		}).on('changeDate', function(selected){
	        // startDate = new Date(selected.date.valueOf());
	        // startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});

@if(Request::get('year'))

    $("#order_year_filter").datepicker("setDate",new Date({{Request::get('year')}}, 1)).trigger('change');

@endif

    $('#order_month_filter').datepicker({
	    autoclose: true,
        minViewMode: "months",
	    format: 'M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('month'))

    var date = new Date('{{Request::get('month')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_month_filter").datepicker("setDate",new Date(yyyy,mm, 1)).trigger('change');

@endif

    $('#order_date_filter').datepicker({
	    autoclose: true,
	    format: 'dd M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('date'))

    var date = new Date('{{Request::get('date')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_date_filter").datepicker("setDate",new Date(yyyy,mm, dd, 1)).trigger('change');

@endif

</script>

@endif

@if(Route::current()->getName() == 'supplier_product_list_report')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
            @csrf
                <!-- <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search Product Name">
                    </div>
                </div> -->
                <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select id="product_category_subcategory_list" name="categoryId" onchange="this.form.submit()" class="" data-placeholder="Select Product Category">
                            <option>Select product Category</option>
                            @if(isset($data["category"]))
                                @foreach($data["category"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('categoryId') == $value['_id'] ? 'selected' : '' }} >{{ $value["category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <div class="input-group single-search-select2">
                        <select id="product__sub_category_list" name="subcategoryId" class="form-control-chosen" data-placeholder="Select Product Sub Category">
                            <option></option>
                            @if(isset($data["sub_category"]))
                                @foreach($data["sub_category"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('subcategoryId') == $value['_id'] ? 'selected' : '' }} >{{ $value["sub_category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div> -->

                <!-- <div class="form-group">
                    <div class="input-group">
                        <button class="site-button">Search</button>
                    </div>
                </div> -->
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

<!-- Report Module Over -->



