@section('title', 'Supplier Support Ticket Detail')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums"><span>Customer Support Details</span></h1>
    <div class="clearfix"></div>
    
    <div class="review-complaints-block wht-tble-bg m-b30">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php
            //dd($data["data"]); 
            $ticket_data_status = ""; ?>
            @if(isset($data["data"]["ticketData"][0]))
                @php
                    $ticket_data_status = $data["data"]["ticketData"][0]["support_ticket_status"];
                @endphp
            @endif
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Ticket Id - <span>#{{ isset($data["ticket_id"]) ? $data["ticket_id"] : '' }}</span></h2>
                    <form name="ticket_resolve_form">
                        @csrf
                        <input type="hidden" name="support_ticket_id" value="{{ isset($data["ticket_id"]) ? $data["ticket_id"] : '' }}" />
                        <input type="hidden" name="support_ticket_status" value="SOLVED" />
                      <!--   <button type="submit" class="site-button pull-right">Resolve case</button> -->
                    </form>
                </div>

                <div class="support-ticket-details-block">
                    <div class="comn-table1 pull-left w-100">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Support Ticket No</th>
                                    <th>Subject</th>
                                    <th>Created On</th>
                                    <th>Priority</th>
                                    <th>User Type</th>
                                    <th>User Name</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($data["data"]))
                               @php
                                    $ticket_data = $data["data"]["ticketData"];
                               @endphp
                                @foreach($ticket_data as $value)  
                                    
                                <tr>
                                    <td>#{{ isset($value["ticket_id"]) ? $value["ticket_id"] : '' }}</td>
                                    <td>{{ isset($value["subject"]) ? $value["subject"] : '' }}</td>
                                    <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                    <td>{{ isset($value["severity"]) ? $value["severity"] : '' }}</td>
                                    <td>{{ isset($value['client_type']) ? $value['client_type'] : '' }}</td>
                                    <td>
                                    {{ isset($value["vendor"]["company_name"]) ? $value["vendor"]["company_name"] . ' - ' : '' }} 
                                    {{ isset($value["buyer"]["company_name"]) ? $value["buyer"]["company_name"] . ' - ' : '' }}
                                    {{ isset($value['vendor']['full_name']) ? $value['vendor']['full_name'] : '' }}
                                    {{ isset($value['buyer']['full_name']) ? $value['buyer']['full_name'] : '' }}
                                    {{ isset($value['logistics']['full_name']) ? $value['logistics']['full_name'] : '' }}
                                    </td>
                                    <td>
                                        @if($value["support_ticket_status"] == 'OPEN')
                                            <div class="badge bg-green">{{ isset($value["support_ticket_status"]) ? $value["support_ticket_status"] : '' }}</div></td>
                                        @else
                                            <div class="badge bg-red">{{ isset($value["support_ticket_status"]) ? $value["support_ticket_status"] : '' }}</div>
                                        @endif
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                      <td colspan="7" style="text-align: center;">No Record Found</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
    <div class="review-complaints-block wht-tble-bg msg_support_ticket">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Correspondence</h2>
                    @if($ticket_data_status != "CLOSED")
                        <button class="site-button gray showButton rplyshow pull-right">Reply</button>
                    @endif
                </div>

                <div class="correspondence-reply-block">
                    <div class="rply-block">
                        <div class="rply-form rplyshowhide" style="display: none">
                            <div class="rply-form-block">
                                <form id="upload" name="support_ticket_reply_form" method="post" action="" enctype="multipart/form-data">
                                    @csrf
                                    <div class="input-field">
                                        <label>Reply</label>
                                        <div class="cstm-input">
                                            <textarea name="comment" class="form-control" id="ctdescription" placeholder="Detailed of the question or issue" data-maxchar="5000"></textarea>
                                            <span class="character-text">Maximum 5000 characters (<span class="character-counter"></span> remaining)</span>
                                        </div>
                                    </div>
                                    <div class="input-field" id="reply_attachment_div">
                                        <label>Attachment</label>
                                        <div class="upload-file-block mt-0">
                                            <input type="file" name="attachments" multiple/>
                                        </div>
                                        <span class="file-note-text">Use one of the jpg, jpeg, mp4, doc, docx, pdf file format</span>
                                        <span class="file-error-text" style="display: none;"><i class="ion-alert-circled"></i> Maximum amount of files exceeded!</span>
                                    </div>
                                    <input type="hidden" name="ticket_id" id="support_ticket_id" value="{{ isset($data['ticket_id']) ? $data['ticket_id'] : '' }}" />
                                    <div class="input-field">
                                        <button type="submit" class="site-button m-r10">Submit</button>
                                        <a href="javascript:void(0)" class="site-button gray hideButton rplyshowhide" onclick="resetForm('support_ticket_reply_form','create-ticket-popup')" style="display: none">Cancel</a>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>

                    <div class="chat-area mCustomScrollbar">
                        <div class="chats">
                            <div class="chats">
                                
                                @if(isset($data["ticket_messages"]))
                                <?php //dump($data["data"]); ?>
                                        @foreach($data["ticket_messages"] as $value)
                                        <?php //dump($value); ?>
                                            @if($value["reply_by_type"] == 'admin')
                                                <div class="chat">
                                                    <div class="chat-body">
                                                        <div class="chat-tms-details">
                                                            <h5>Conmix {{ $value["reply_by_type"] }}</h5>
                                                            <p>{{ AdminController::dateTimeFormat($value["created_at"]) }}</p>
                                                        </div>
                                                        <div class="chat-text">
                                                            <p>{{ $value["comment"] }}</p>
                                                            
                                                            @if(isset($value["attachments"]) && !empty($value["attachments"]))
                                                                <div class="et-attachment-block">
                                                                    <h3><i class="fa fa-paperclip"></i> Attachments <span>({{ count($value["attachments"]) }})</span></h3>
                                                                    <ul>
                                                                        <?php $file_count = 1; //dd($value["attachments"])?>
                                                                        @foreach($value["attachments"] as $attchment_value)

                                                                            <li>
                                                                                <div class="mailbox-attachment-info">
                                                                                    @if(strpos($attchment_value["url"], '.doc') || strpos($attchment_value["url"], '.docx'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-word-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.jpg') || strpos($attchment_value["url"], '.png') || strpos($attchment_value["url"], '.jpeg'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-photo-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.pdf'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.mp4'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-video-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif
                                                                                    <!-- <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> Sep2014-report.pdf</a>
                                                                                    <span class="mailbox-attachment-size">1,245 KB</span> -->
                                                                                </div>
                                                                            </li>
                                                                        @endforeach
                                                                        <!-- <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-word-o"></i> App Description.docx</a>
                                                                                <span class="mailbox-attachment-size">1,245 KB</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-photo-o"></i> photo1.png</a>
                                                                                <span class="mailbox-attachment-size">2.67 MB</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-video-o"></i> video.mp4</a>
                                                                                <span class="mailbox-attachment-size">2.67 MB</span>
                                                                            </div>
                                                                        </li> -->
                                                                    </ul>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="chat chat-right">
                                                    <div class="chat-body">
                                                        <div class="chat-tms-details">
                                                            <h5>{{ $value["reply_by_type"] }}</h5>
                                                            <p>{{ AdminController::dateTimeFormat($value["created_at"]) }}</p>
                                                        </div>
                                                        <div class="chat-text">
                                                            <p>{{ $value["comment"] }}</p>

                                                            @if(isset($value["attachments"]) && !empty($value["attachments"]))
                                                                <div class="et-attachment-block text-left">
                                                                    <h3><i class="fa fa-paperclip"></i> Attachments <span>({{ count($value["attachments"]) }})</span></h3>
                                                                    <ul>
                                                                        <?php $file_count = 1; //dd($value["attachments"])?>
                                                                        @foreach($value["attachments"] as $attchment_value)

                                                                            <li>
                                                                                <div class="mailbox-attachment-info">
                                                                                    @if(strpos($attchment_value["url"], '.doc') || strpos($attchment_value["url"], '.docx'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-word-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.jpg') || strpos($attchment_value["url"], '.png') || strpos($attchment_value["url"], '.jpeg'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-photo-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.pdf'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.mp4'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-video-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif
                                                                                    <!-- <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> Sep2014-report.pdf</a>
                                                                                    <span class="mailbox-attachment-size">1,245 KB</span> -->
                                                                                </div>
                                                                            </li>
                                                                        @endforeach
                                                                        <!-- <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-word-o"></i> App Description.docx</a>
                                                                                <span class="mailbox-attachment-size">1,245 KB</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-photo-o"></i> photo1.png</a>
                                                                                <span class="mailbox-attachment-size">2.67 MB</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-video-o"></i> video.mp4</a>
                                                                                <span class="mailbox-attachment-size">2.67 MB</span>
                                                                            </div>
                                                                        </li> -->
                                                                    </ul>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                            
                                        @endforeach
                                    @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection