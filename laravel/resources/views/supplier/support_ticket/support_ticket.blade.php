@section('title', 'Supplier Support Ticket')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="main-title">Customer Support</h1>
    <div class="clearfix"></div>

    <div class="review-complaints-block wht-tble-bg m-b30">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Customer Support List</h2>
                    <button class="site-button pull-right m-r10" onclick="resetForm('add_support_ticket_form','create-ticket-popup')" data-toggle="modal" data-target="#create-ticket-popup">Create Ticket</button>
                    <a href="{{ route('support_ticket_view') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    @if(isset($data['data']) && count($data['data']) > 0)
                        <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    @endif
                </div>

                <div class="support-ticket-list-block">
                    <div class="comn-table1 pull-left w-100 m-b20">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Support Ticket No</th>
                                    <th>Subject</th>
                                    <th>Created On</th>
                                    <th>Priority</th>
                                    <!-- <th>User Type</th> -->
                                    <th>User/Company Name</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data['data']) && count($data['data']) > 0)
                                @php

                                    $total_record = count($data["data"]);
                                @endphp
                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php
                                            $first_created_date = $value["created_at"];
                                            $count++;
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                <tr>
                                    <td><a href="{{ url('supplier/support_ticket/ticket_detail/'.$value['ticket_id']) }}">#{{ isset($value["ticket_id"]) ? $value["ticket_id"] : '' }}</a></td>
                                    <td>{{ isset($value["subject"]) ? $value["subject"] : '' }}</td>
                                    <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                    <td>{{ isset($value["severity"]) ? $value["severity"] : '' }}</td>
                                    <!-- <td>{{ isset($value["client_type"]) ? $value["client_type"] : '' }}</td> -->
                                    <td>
                                    <!-- {{ isset($value["vendor"]["company_name"]) ? $value["vendor"]["company_name"] : '' }} -  -->
                                    {{ isset($value["vendor"]["full_name"]) ? $value["vendor"]["full_name"] : '' }}

                                    </td>
                                    <td>
                                        @if($value["support_ticket_status"] == 'OPEN')
                                            <div class="badge bg-green">{{ isset($value["support_ticket_status"]) ? $value["support_ticket_status"] : '' }}</div>
                                        @else
                                            <div class="badge bg-red">{{ isset($value["support_ticket_status"]) ? $value["support_ticket_status"] : '' }}</div>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach

                            @else
                                <tr>
                                      <td colspan="7" style="text-align: center;">No Record Found</td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                    </div>

                    <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                    <div class="data-box-footer clearfix">
                        <div class="pagination-block">
                            <!-- <ul class="pagination justify-content-end m-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            </ul>
                            <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                            <div class="pagination justify-content-end m-0">
                                @if($is_previous_avail == 1)
                                    @if(isset($first_created_date))
                                    <form action="{{route(Route::current()->getName())}}" method="get">

                                        @if(Request::get('severity'))
                                            <input type="hidden" name="severity" value="{{ Request::get('severity') ? Request::get('severity') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('subject'))
                                            <input type="hidden" value="{{ Request::get('subject') ? Request::get('subject') : '' }}" name="subject"/>
                                        @endif
                                        @if(Request::get('ticket_id'))
                                            <input type="hidden" value="{{ Request::get('ticket_id') ? Request::get('ticket_id') : '' }}" name="ticket_id"/>
                                        @endif


                                        <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                        <button type="submit" class="site-button">Previous</button>
                                    </form>
                                    @endif
                                    @endif
                                    @if($is_next_avail == 1)
                                    @if(isset($last_created_date))
                                    <form action="{{route(Route::current()->getName())}}" method="get">


                                        @if(Request::get('severity'))
                                            <input type="hidden" name="severity" value="{{ Request::get('severity') ? Request::get('severity') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('subject'))
                                            <input type="hidden" value="{{ Request::get('subject') ? Request::get('subject') : '' }}" name="subject"/>
                                        @endif
                                        @if(Request::get('ticket_id'))
                                            <input type="hidden" value="{{ Request::get('ticket_id') ? Request::get('ticket_id') : '' }}" name="ticket_id"/>
                                        @endif


                                        <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                        <button type="submit" class="site-button">Next</button>
                                    </form>
                                    @endif
                                    @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  <!--   <div class="customer-care-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="customer-care-content-block">
                    <div class="customer-care-left-block">
                        <img src="{{asset('assets/supplier/images/customer-service.png')}}">

                        <h3>Call Us On: <a href="#">+91 98989 14789</a></h3>
                        <h4>Write Us On: <a href="#">info@example.com</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div>

@endsection