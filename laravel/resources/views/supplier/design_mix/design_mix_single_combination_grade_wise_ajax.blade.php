
<!--Contact Detail start -->
<?php //dd($data); ?>
@if(isset($data["combination"]) && (count($data["combination"]) > 0))
    <?php $data_count = (int)$data["rmc_div_id"]; ?>
    @foreach($data["combination"] as $value)
    <?php //dd($value); 
        $rmc_data = $value;
    ?>
        
        <div class="col-md-12">
            <div class="custom_rmc_bx mb-3">
                @if(false)
                <div class="top-right-edit-link">
                    <a href="{{ route('design_mix_edit_view',$value['_id']) }}" class="site-button green button-sm"><i class="fa fa-pencil-square-o"></i></a>
                </div>
                @endif
                
                <div class="custom_rmc_info">
                    <div class="top-right-edit-link" style="top: 10px;right: 10px;color: #DC3839;">
                        Price : <b>₹{{ isset($value["selling_price"]) ? number_format($value["selling_price"],2) : 0 }}/</b> Cu.Mtr
                    </div>
                    <h5>{{ isset($value["concrete_grade_name"]) ? $value["concrete_grade_name"] : '' }} (Combination {{ $data_count }})</h5>
                    <!-- <h6 class="mb-1"><i class="fa fa-map-marker"></i> Plant Name OR Address</h6> -->
                    <hr class="my-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Cement ( Kg ) : <span>{{ isset($value["cement_quantity"]) ? $value["cement_quantity"] : '' }}</span></p>
                            <p>Coarse Sand ( Kg ) : <span>{{ isset($value["sand_quantity"]) ? $value["sand_quantity"] : '' }}</span></p>
                            <p>Fly Ash ( Kg ) : <span>{{ isset($value["fly_ash_quantity"]) ? $value["fly_ash_quantity"] : '' }}</span></p>
                            <p>Admixture ( Kg ) : <span>{{ isset($value["admix_quantity"]) ? $value["admix_quantity"] : '' }}</span></p>
                        </div>
                        <div class="col-sm-6">
                            <p>{{ $value["aggregate1_sub_category_name"] }} ( Kg ) : <span>{{ isset($value["aggregate1_quantity"]) ? $value["aggregate1_quantity"] : '' }}</span></p>
                            <p>{{ $value["aggregate2_sub_category_name"] }} ( Kg ) : <span>{{ isset($value["aggregate2_quantity"]) ? $value["aggregate2_quantity"] : '' }}</span></p>
                            
                            <p>Water ( Ltr ) : <span>{{ isset($value["water_quantity"]) ? $value["water_quantity"] : '' }}</span></p>
                            <p>Grade : <span>{{ isset($value["concrete_grade_name"]) ? $value["concrete_grade_name"] : '' }}</span></p>
                        </div>
                    </div>
                </div>
                
                <div class="custom_rmc_info custom_rmc_dec p-a10">
                    <b>Design Mix Description</b>
                    <div class="row">
                        @if(isset($data["brand_details"]) && (count($data["brand_details"]) > 0))
                            <?php 
                                $brands = $data["brand_details"];
                            ?>
                            <div class="col-sm-12">
                                <p>Cement Brand : 
                                    <span>{{ isset($rmc_data["cement_brand_name"]) ? $rmc_data["cement_brand_name"] : '' }}
                                        <a href="javascript:void(0)" id="cement_brand_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                                    </span>
                                    <div class="cstm-select-box" id="cement_brand_{{ $data_count }}" style="display:none;margin-top:5px;">
                                        <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                        <select class="mix_cement_brnad" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Cement Brand</option>
                                            @if((isset($brands["cementData"])) && (count($brands["cementData"]) > 0))
                                                @foreach($brands["cementData"] as $value)
                                                    @if((isset($value['cement_brand'])) && (count($value["cement_brand"]) > 0))
                                                        @if((isset($value['cement_grade'])) && (count($value["cement_grade"]) > 0))
                                                            <?php
                                                                $is_selected = '';
                                                                if(($rmc_data["final_cement_brand_id"] == $value['cement_brand']['_id']) && ($rmc_data["final_cement_grade_id"] == $value['cement_grade']['_id'])){
                                                                    $is_selected = 'selected';
                                                                }
                                                            ?>
                                                            <option {{ $is_selected }} value="{{ $value['cement_brand']['_id'] }}_{{ $value['cement_grade']['_id'] }}" 
                                                                data-cement-brand-id="{{ $value['cement_brand']['_id'] }}"
                                                                data-cement-grade-id="{{ $value['cement_grade']['_id'] }}" 
                                                                data-rmc-id="{{ $data_count }}">
                                                                {{ $value['cement_brand']["name"] }} - {{ $value['cement_grade']["name"] }}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </p>
                                <p> Coarse Sand Source : 
                                    <span>
                                        {{ isset($rmc_data["sand_source_name"]) ? $rmc_data["sand_source_name"] : '' }}
                                        <a href="javascript:void(0)" id="sand_source_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                                    </span>
                                    <div class="cstm-select-box" id="sand_source_{{ $data_count }}" style="display:none;margin-top:5px;">
                                        <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                        <select class="mix_sand_source" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                                            @if((isset($brands["sandData"])) && (count($brands["sandData"]) > 0))
                                                @foreach($brands["sandData"] as $value)
                                                    @if((isset($value['sand_source'])) && (count($value["sand_source"]) > 0))
                                                        @if((isset($value['sand_zone'])) && (count($value["sand_zone"]) > 0))
                                                        <?php
                                                                $is_selected = '';
                                                                if(($rmc_data["final_sand_source_id"] == $value['sand_source']['_id']) && ($rmc_data["final_sand_zone_id"] == $value['sand_zone']['_id'])){
                                                                    $is_selected = 'selected';
                                                                }
                                                            ?>
                                                            <option {{ $is_selected }} value="{{ $value['sand_source']['_id'] }}_{{ $value['sand_zone']['_id'] }}" 
                                                                data-sand-source-id="{{ $value['sand_source']['_id'] }}"
                                                                data-sand-zone-id="{{ $value['sand_zone']['_id'] }}" data-rmc-id="{{ $data_count }}">{{ $value['sand_source']["sand_source_name"] }} - ({{ $value['sand_zone']["zone_name"] }} - {{ $value['sand_zone']["finess_module_range"] }})</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </p>
                                <!-- <p>Coarse Sand Zone ( mm ) : <span>{{ isset($value["sand_zone_name"]) ? $value["sand_zone_name"] : '' }}</span></p> -->
                                <p>Aggregate Source & Category 1 & Category 2 : 
                                    <span>
                                        {{ isset($rmc_data["aggregate_source_name"]) ? $rmc_data["aggregate_source_name"] : '' }}
                                        <a href="javascript:void(0)" id="aggregate_source_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                                    </span>
                                    <div class="cstm-select-box" id="aggregate_source_{{ $data_count }}" style="display:none;margin-top:5px;">
                                        <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                        <select class="mix_aggregate_source_cat_1_2" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Aggregate Source & Category 1 & Category 2</option>
                                            @if((isset($brands["aggData"])) && (count($brands["aggData"]) > 0))
                                                @foreach($brands["aggData"] as $value)
                                                    @if((isset($value['aggregate_source'])) && (count($value["aggregate_source"]) > 0))
                                                        @if((isset($value['aggregate_sand_sub_category'])) && (count($value["aggregate_sand_sub_category"]) > 0))
                                                            @foreach($value["sub_cat_data_2"] as $sub_cat_2_value)

                                                            <?php
                                                                $is_selected = '';
                                                                if(($rmc_data["final_agg_source_id"] == $value['aggregate_source']['_id']) && 
                                                                    ($rmc_data["final_aggregate1_sub_cat_id"] == $value['aggregate_sand_sub_category']['_id']) &&
                                                                    ($rmc_data["final_aggregate2_sub_cat_id"] == $sub_cat_2_value['aggregate_sand_sub_category']['_id'])
                                                                    ){
                                                                    $is_selected = 'selected';
                                                                }
                                                            ?>

                                                                <option {{ $is_selected }} value="{{ $value['aggregate_source']['_id'] }}_{{ $value['aggregate_sand_sub_category']['_id'] }}_{{ $sub_cat_2_value['aggregate_sand_sub_category']['_id'] }}" 
                                                                data-aggr-source-id="{{ $value['aggregate_source']['_id'] }}"
                                                                data-sub-cat-1-id="{{ $value['aggregate_sand_sub_category']['_id'] }}" 
                                                                data-sub-cat-2-id="{{ $sub_cat_2_value['aggregate_sand_sub_category']['_id'] }}"
                                                                data-rmc-id="{{ $data_count }}">
                                                                {{ $value['aggregate_source']["aggregate_source_name"] }} - 
                                                                {{ $value['aggregate_sand_sub_category']["sub_category_name"] }} - 
                                                                {{ $sub_cat_2_value['aggregate_sand_sub_category']['sub_category_name'] }}
                                                                </option>
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </p>
                                <p>Admixture Brand & Type ( Kg ) : 
                                    <span>
                                        {{ isset($rmc_data["admix_brand_name"]) ? $rmc_data["admix_brand_name"] : '' }}
                                        <a href="javascript:void(0)" id="admix_source_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                                    </span>
                                    <div class="cstm-select-box" id="admix_source_{{ $data_count }}" style="display:none;margin-top:5px;">
                                        <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                        <select class="mix_admix_source" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Admixture Brand & Type</option>
                                            @if((isset($brands["admixData"])) && (count($brands["admixData"]) > 0))
                                                @foreach($brands["admixData"] as $value)
                                                    @if((isset($value['admixture_brand'])) && (count($value["admixture_brand"]) > 0))
                                                        @if((isset($value['admixture_category'])) && (count($value["admixture_category"]) > 0))

                                                        <?php
                                                                $is_selected = '';
                                                                if(($rmc_data["final_admix_brand_id"] == $value['admixture_brand']['_id']) && ($rmc_data["final_admix_cat_id"] == $value['admixture_category']['_id'])){
                                                                    $is_selected = 'selected';
                                                                }
                                                            ?>

                                                            <option {{ $is_selected }} value="{{ $value['admixture_brand']['_id'] }}_{{ $value['admixture_category']['_id'] }}" 
                                                            data-admix-brand-id="{{ $value['admixture_brand']['_id'] }}"
                                                                data-admix-cat-id="{{ $value['admixture_category']['_id'] }}" data-rmc-id="{{ $data_count }}">
                                                                {{ $value['admixture_brand']["name"] }} - ({{ $value['admixture_category']["category_name"] }} - {{ $value['admixture_category']["admixture_type"] }})</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </p>
                                <!-- <p>Admixture Type ( Kg ) : <span>{{ isset($value["admix_category_name"]) ? $value["admix_category_name"] : '' }}</span></p> -->
                                <p>Fly Ash Source : 
                                    <span>
                                        {{ isset($rmc_data["fly_ash_source_name"]) ? $rmc_data["fly_ash_source_name"] : '' }}
                                        <a href="javascript:void(0)" id="flyash_source_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                                    </span>
                                    <div class="cstm-select-box" id="flyash_source_{{ $data_count }}" style="display:none;margin-top:5px;">
                                        <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                        <select class="mix_flyash_source" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Fly Ash Source</option>
                                            @if((isset($brands["flyashData"])) && (count($brands["flyashData"]) > 0))
                                                @foreach($brands["flyashData"] as $value)
                                                    @if((isset($value['fly_ash_source'])) && (count($value["fly_ash_source"]) > 0))
                                                        <?php
                                                                $is_selected = '';
                                                                if(isset($rmc_data["final_fly_ash_source_id"])){
                                                                    if(($rmc_data["final_fly_ash_source_id"] == $value['fly_ash_source']['_id'])){
                                                                        $is_selected = 'selected';
                                                                    }
                                                                }
                                                            ?>
                                                        <option {{ $is_selected }} value="{{ $value['fly_ash_source']['_id'] }}" 
                                                        data-flyash-source-id="{{ $value['fly_ash_source']['_id'] }}"
                                                        data-rmc-id="{{ $data_count }}"
                                                            >{{ $value['fly_ash_source']["fly_ash_source_name"] }} 
                                                        </option>
                                                        
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </p>
                                <p>Water : 
                                    <span>
                                        {{ isset($rmc_data["water_type"]) ? $rmc_data["water_type"] : 'Regular' }}
                                        <a href="javascript:void(0)" id="water_type_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>    
                                        
                                    </span>
                                    <div class="cstm-select-box" id="water_type_{{ $data_count }}" style="display:none;margin-top:5px;">
                                        <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                        <select class="mix_water_type" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Water Type</option>
                                            @if((isset($brands["waterData"])) && (count($brands["waterData"]) > 0))
                                                @foreach($brands["waterData"] as $value)
                                                    @if((isset($value['water_type'])) && (!empty($value["water_type"])))
                                                        <?php
                                                                $is_selected = '';
                                                                if(isset($rmc_data["final_water_type"])){
                                                                    if(($rmc_data["final_water_type"] == $value['water_type'])){
                                                                        $is_selected = 'selected';
                                                                    }
                                                                }
                                                            ?>
                                                        <option {{ $is_selected }} value="{{ $value['water_type'] }}" 
                                                        data-water-type="{{ $value['water_type'] }}"
                                                        data-rmc-id="{{ $data_count }}"
                                                            >{{ $value['water_type'] }} 
                                                        </option>
                                                        
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                            
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
                
                <!-- <div class="custom_rmc_info custom_rmc_dec p-a10">
                    <b>Design Mix Description</b>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Cement Brand : <span>{{ isset($value["cement_brand_name"]) ? $value["cement_brand_name"] : '' }}</span></p>
                            <p> CoarseSand Source : <span>{{ isset($value["sand_source_name"]) ? $value["sand_source_name"] : '' }}</span></p>
                            <p>Coarse Sand Zone ( mm ) : <span>{{ isset($value["sand_zone_name"]) ? $value["sand_zone_name"] : '' }}</span></p>
                            <p>Aggregate Source : <span>{{ isset($value["aggregate_source_name"]) ? $value["aggregate_source_name"] : '' }}</span></p>
                            <p>Admixture Brand ( Kg ) : <span>{{ isset($value["admix_brand_name"]) ? $value["admix_brand_name"] : '' }}</span></p>
                            <p>Admixture Type ( Kg ) : <span>{{ isset($value["admix_category_name"]) ? $value["admix_category_name"] : '' }}</span></p>
                            <p>Fly Ash Source Name : <span>{{ isset($value["fly_ash_source_name"]) ? $value["fly_ash_source_name"] : '' }}</span></p>
                            <p>Water : <span>Regular</span></p>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div>
            <form id="rmc_combination_form_{{ $data_count }}">

                <input type="hidden" name="design_mix_id" value="{{ $data['design_mix_id'] }}" />
                <input type="hidden" name="rmc_div_id" value="{{ $data_count }}" />
                
                <input type="hidden" id="mix_rmc_cement_brand_id_{{ $data_count }}" name="cement_brand_id" value="{{ $rmc_data['final_cement_brand_id'] }}" />
                <input type="hidden" id="mix_rmc_cement_grade_id_{{ $data_count }}" name="cement_grade_id" value="{{ $rmc_data['final_cement_grade_id'] }}" />
                
                <input type="hidden" id="mix_rmc_sand_source_id_{{ $data_count }}" name="sand_source_id" value="{{ $rmc_data['final_sand_source_id'] }}" />
                <input type="hidden" id="mix_rmc_sand_zone_id_{{ $data_count }}" name="sand_zone_id" value="{{ $rmc_data['final_sand_zone_id'] }}" />
                
                <input type="hidden" id="mix_rmc_aggr_source_id_{{ $data_count }}" name="aggr_source_id" value="{{ $rmc_data['final_agg_source_id'] }}" />
                <input type="hidden" id="mix_rmc_aggr_cat_1_id_{{ $data_count }}" name="aggr_cat_1_id" value="{{ $rmc_data['final_aggregate1_sub_cat_id'] }}" />
                <input type="hidden" id="mix_rmc_aggr_cat_2_id_{{ $data_count }}" name="aggr_cat_2_id" value="{{ $rmc_data['final_aggregate2_sub_cat_id'] }}" />
                
                <input type="hidden" id="mix_rmc_admix_brand_id_{{ $data_count }}" name="admix_brand_id" value="{{ $rmc_data['final_admix_brand_id'] }}" />
                <input type="hidden" id="mix_rmc_admix_cat_id_{{ $data_count }}" name="admix_cat_id" value="{{ $rmc_data['final_admix_cat_id'] }}" />
                
                <input type="hidden" id="mix_water_type_{{ $data_count }}" name="water_type" value="{{ isset($rmc_data['final_water_type']) ? $rmc_data['final_water_type'] : '' }}" />

                <input type="hidden" id="mix_rmc_fly_ash_id_{{ $data_count }}" name="fly_ash_id" value="{{ $rmc_data['final_fly_ash_source_id'] }}" />

            </form>
        </div>
        
        <script>
                $(document).ready(function(){

                    $("#cement_brand_change_{{ $data_count }}").click(function(){
                        
                        // console.log("Clicked");
                        $("#cement_brand_{{ $data_count }}").toggle();

                    });
                    
                    $("#sand_source_change_{{ $data_count }}").click(function(){
                        
                        // console.log("Clicked");
                        $("#sand_source_{{ $data_count }}").toggle();

                    });
                    
                    $("#aggregate_source_change_{{ $data_count }}").click(function(){
                        
                        // console.log("Clicked");
                        $("#aggregate_source_{{ $data_count }}").toggle();

                    });
                    
                    $("#admix_source_change_{{ $data_count }}").click(function(){
                        
                        // console.log("Clicked");
                        $("#admix_source_{{ $data_count }}").toggle();

                    });
                    
                    $("#flyash_source_{{ $data_count }}").click(function(){
                        
                        // console.log("Clicked");
                        $("#flyash_source_{{ $data_count }}").toggle();

                    });

                    $("#water_type_change_{{ $data_count }}").click(function(){
                            
                        // console.log("Clicked");
                        $("#water_type_{{ $data_count }}").toggle();

                    });

                });
                
            </script>

        <?php $data_count++ ?>
    @endforeach
    @else
        <div class="col-md-6"> No Design Mix Combination Available</div>

@endif

<!--Contact Detail end -->
