@section('title', 'Supplier RMC Detail')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Standard Design Mix</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="bank-detail-main-block design_mix_bx wht-tble-bg">
        <div class="sub-title-block">
            <h2 class="sub-title pull-left">Standard Design Mix</h2>
            <a href="{{ route('design_mix_detail') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
            @if(isset($data['data']) && count($data['data']) > 0)
                <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="bank-detail-block add-bank-detail-block">
                            <!-- <a href="javascript:;" onclick="resetForm('add_bank_detail_form','add-bank-details')" data-toggle="modal" data-target="#add-bank-details"><i>+</i></a> -->
                            <a href="{{ route('design_mix_add_view') }}"><i>+</i></a>
                        </div>
                    </li>
                    @if(isset($data["data"]))
                    @php
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)
                        @if($count == 0)
                            @php
                                $first_created_date = $value["created_at"];
                                $count++;
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                        <li>
                            <div class="bank-details-select">
                                <!-- <div class="make-default-btn site-button green" onclick="viewDesignMixOfGrade('{{ $value['_id'] }}')">View {{ isset($value["concrete_grade"]["name"]) ? $value["concrete_grade"]["name"] : '' }} Mix</div> -->

                                <div class="bank-detail-block p-a15 flex-column" onclick="viewDesignMixOfGrade('{{ $value['_id'] }}')">
                                    <span>{{ isset($value["concrete_grade"]["name"]) ? $value["concrete_grade"]["name"] : '' }}</span>
                                    <h5><b>Plant Name : </b> {{ isset($value["addressDetails"]["business_name"]) ? $value["addressDetails"]["business_name"] : '' }}</h5>
                                    <p>( 
                                        @if(isset($value["designMixVariantDetails"]) && !empty($value["designMixVariantDetails"]))
                                            @foreach($value["designMixVariantDetails"] as $varient_value)
                                                @if(isset($varient_value["product_name"]))
                                                    {{ $varient_value["product_name"] }}
                                                @endif

                                            @endforeach

                                        @else

                                            No variant found

                                        @endif
                                        
                                        )</p>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    @endif



                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection