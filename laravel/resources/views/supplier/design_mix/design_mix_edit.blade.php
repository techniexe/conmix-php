@section('title', 'Edit Supplier Standard RMC')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Design Mix</h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="vehicle-block vehicle-category-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="data-box-header m-b20">
                    <h2 class="box-title">Edit Design Mix</h2>
                </div>
                <div class="vehicle-list-block">
                    @if(isset($data["error"]))
                        <div class="error">{{ $data["error"]["message"] }}</div>
                    @endif
                    <?php $final_data = $data["data"]; 
                        // dd($final_data);
                    ?>
                    <div class="vehicle-block vehicle-category-block">
                        <span>Concrete Density (Should be between 2400 - 2550 KG) <span id="concrete_density" style="display:none">: <b>0 KG</b></span>
                        <span class="error" id="concrete_density_error"></span>
                        <span class="success" id="concrete_density_success"></span>
                        </span>
                        <form action="" method="post" name="add_mix_form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="grade_id_div">
                                                <label>Design Mix Grade <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="grade_id" id="grade_id"> -->
                                                    <select name="grade_id" id="concrete_grade_id" class="form-control" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Design Mix Grade</option>
                                                        @if(isset($data["concrete_grade_data"]))
                                                            @foreach($data["concrete_grade_data"] as $value)
                                                            <option value="{{ $value['_id'] }}" {{ $final_data['grade_id'] == $value['_id'] ? 'selected' : '' }}>{{ $value["name"] }}</option>
                                                            @endforeach
                                                        @endif

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Product Name <span class="str">*</span></label>
                                                <input type="text" name="product_name_1" readonly value="{{ $final_data['product_name'] }}" class="form-control" placeholder="Design Mix 1">
                                                    <!-- <div class="col-md-6 pd-lr-5">
                                                        <div class="cstm-select-box">
                                                            <select name="design_mix_number" id="design_mix_number">
                                                                <option disabled="disabled" selected="selected">Select Design Mix No</option>

                                                                    @for($i=1; $i<=100 ;$i++ )
                                                                        <option value="{{ $i }}" >{{ $i }}</option>
                                                                    @endfor

                                                            </select>
                                                        </div>
                                                    </div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Cement -->
                                    @if(false)
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="cement_brand_id_div">
                                                <label>Cement Brand <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="cement_brand_id" id="cement_brand_id"> -->
                                                    <select name="cement_brand_id" id="mix_cement_brnad" class="form-control" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Cement Brand</option>
                                                        @if(isset($data["cement_brand_data"]))
                                                            @foreach($data["cement_brand_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ $final_data['cement_brand_id'] == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="cement_grade_id_div">
                                                <label>Cement Grade <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="cement_grade_id" id="cement_grade_id"> -->
                                                    <select name="cement_grade_id" id="cement_grade_id" class="form-control" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Cement Grade</option>
                                                        @if(isset($data["cement_grade_data"]))
                                                            @foreach($data["cement_grade_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ $final_data['cement_grade_id'] == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    @endif
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Cement Qty (KG) <span class="str">*</span></label>
                                                <input type="text" name="cement_quantity" id="custom_cement_qty" onkeyup="concreteDensity()" value="{{ $final_data['cement_quantity'] }}" class="form-control" placeholder="e.g. 500">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Sand source -->
                                    @if(false)
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="sand_source_id_div">
                                                <label>Coarse Sand Source <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="sand_source_id" id="sand_source_id"> -->
                                                    <select name="sand_source_id" id="mix_sand_source" class="form-control" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                                                        @if(isset($data["sand_source_data"]))
                                                            @foreach($data["sand_source_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ $final_data['sand_source_id'] == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['sand_source_name'] }}">{{ $value["sand_source_name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="sand_source_id_div">
                                                <label>Coarse Sand Zone <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="sand_source_id" id="sand_source_id" class="form-control"> -->
                                                    <select name="sand_zone_id" id="mix_sand_zone" class="form-control" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Coarse Sand Zone</option>
                                                        @if(isset($data["sand_zone_data"]))
                                                            @foreach($data["sand_zone_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ $final_data['sand_zone_id'] == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['zone_name'] }}">{{ $value["zone_name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Coarse Sand Qty (KG) <span class="str">*</span></label>
                                                <input type="text" name="sand_quantity" id="custom_sand_qty" onkeyup="concreteDensity()" value="{{ $final_data['sand_quantity'] }}" class="form-control" placeholder="e.g. 500">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Aggregate Source -->
                                    @if(false)
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="aggregate_source_id_div">
                                                <label>Aggregate Source <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="aggregate_source_id" id="aggregate_source_id"> -->
                                                    <select name="aggregate_source_id" id="mix_aggregate_source" class="form-control" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Aggregate Source</option>
                                                        @if(isset($data["aggregate_source_data"]))
                                                            @foreach($data["aggregate_source_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ $final_data['aggregate_source_id'] == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['aggregate_source_name'] }}">{{ $value["aggregate_source_name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Aggregate Category 1 -->
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="aggregate1_sub_category_id_div">
                                                <label>Aggregate Category 1 <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="aggregate1_sub_category_id" id="aggregate1_sub_category_id"> -->
                                                    <select name="aggregate1_sub_category_id" id="mix_sub_cat_1" class="form-control" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Aggregate Category 1</option>
                                                        @if(isset($data["aggregate_sub_cat_data"]))
                                                            @foreach($data["aggregate_sub_cat_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ $final_data['aggregate1_sub_category_id'] == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Aggregate Category 1 Qty (KG) <span class="str">*</span></label>
                                                <input type="text" name="aggregate1_quantity" id="custom_aggregate1_qty" onkeyup="concreteDensity()" value="{{ $final_data['aggregate1_quantity'] }}" class="form-control" placeholder="e.g. 500">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Aggregate Category 2 -->
                                    @if(false)
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="aggregate2_sub_category_id_div">
                                                <label>Aggregate Category 2 <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="aggregate2_sub_category_id" id="aggregate2_sub_category_id"> -->
                                                    <select class="form-control" name="aggregate2_sub_category_id" id="mix_sub_cat_2" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Aggregate Category 2</option>
                                                        @if(isset($data["aggregate_sub_cat_data"]))
                                                            @foreach($data["aggregate_sub_cat_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ $final_data['aggregate2_sub_category_id'] == $value['_id'] ? 'selected' : '' }} data-name="{{ $value['sub_category_name'] }}">{{ $value["sub_category_name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Aggregate Category 2 Qty (KG) <span class="str">*</span></label>
                                                    <input type="text" name="aggregate2_quantity" id="custom_aggregate2_qty" onkeyup="concreteDensity()" value="{{ $final_data['aggregate2_quantity'] }}" class="form-control" placeholder="e.g. 500">
                                                </div>
                                            </div>
                                        </div>
                                    <!-- Fly Ash Source -->
                                    @if(false)
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="fly_ash_source_id_div">
                                                <label>Fly Ash Source </label>
                                                <div class="cstm-select-box">
                                                    <!-- <select name="fly_ash_source_id" id="fly_ash_source_id"> -->
                                                    <select class="form-control" name="fly_ash_source_id" id="mix_fly_ash_brand" onchange="concreteDensity()">
                                                        <option selected="selected">Select Fly Ash Source</option>
                                                        @if(isset($data["fly_ash_source_data"]))
                                                            @foreach($data["fly_ash_source_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ isset($final_data['fly_ash_source_id']) ? ($final_data['fly_ash_source_id'] == $value['_id'] ? 'selected' : '') : '' }}  data-name="{{ $value['fly_ash_source_name'] }}">{{ $value["fly_ash_source_name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Fly Ash Source Qty (KG)</label>
                                                <input type="text" name="fly_ash_quantity" id="custom_flyash_qty" onkeyup="concreteDensity()" value="{{ $final_data['fly_ash_quantity'] }}" class="form-control" placeholder="e.g. 500">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Admixture Brand -->
                                    @if(false)
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="ad_mixture_brand_id_div">
                                                <label>Admixture Brand <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    
                                                    <select class="form-control" name="ad_mixture_brand_id" id="ad_mixture_brand_id" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Admixture Brand</option>
                                                        @if(isset($data["admixture_brand_data"]))
                                                            @foreach($data["admixture_brand_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ isset($final_data['ad_mixture_brand_id']) ? ($final_data['ad_mixture_brand_id'] == $value['_id'] ? 'selected' : '') : '' }} data-name="{{ $value['name'] }}">{{ $value["name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="ad_mixture_category_id_div">
                                                <label>Admixture Code <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                   
                                                    <select class="form-control" name="ad_mixture_category_id" id="ad_mixture_category_id" onchange="concreteDensity()">
                                                        <option disabled="disabled" selected="selected">Select Admixture Code</option>
                                                        @if(isset($data["admixture_types_data"]))
                                                            @foreach($data["admixture_types_data"] as $value)
                                                                <option value="{{ $value['_id'] }}" {{ isset($final_data['ad_mixture_category_id']) ? ($final_data['ad_mixture_category_id'] == $value['_id'] ? 'selected' : '') : '' }} data-name="{{ $value['category_name'] }}">{{ $value["category_name"] }} - {{ $value["admixture_type"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="form-label">Admixture (%) <span id="add_mix_qty"></span></label>
                                                    <input name="admix_quantity_per" id="custom_admixture_qty_per" onkeyup="calculateAdMixSupplier()" type="text" class="form-control" value="{{ isset($final_data['admix_quantity_per']) ? $final_data['admix_quantity_per'] : '' }}" placeholder="Value in %">
                                                    <span class="error" id="admix_quantity_error"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Admixture QTY (KG)<span class="str">*</span></label>
                                                    <input type="text" id="custom_admixture_qty" onkeyup="concreteDensity()"  readOnly name="ad_mixture_quantity" value="{{ isset($final_data['ad_mixture_quantity']) ? $final_data['ad_mixture_quantity'] : '' }}" class="form-control" placeholder="e.g. 500">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Water Quantity -->
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Water Qty (Ltr) (Water / Cement Ratio 0.35 - 0.55) <span class="str">*</span></label>
                                                <input type="text" id="custom_water_qty" onkeyup="concreteDensity()" name="water_quantity" value="{{ $final_data['water_quantity'] }}" class="form-control" placeholder="e.g. 500">
                                            </div>
                                            <span class="error" id="water_quantity_error"></span>
                                            <span class="text-success" id="water_quantity_success"></span>
                                        </div>
                                    </div>
                                    @if(false)
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Selling Price <span class="str">*</span></label>
                                                <input type="text" name="selling_price" onkeyup="concreteDensity()" value="{{ $final_data['selling_price'] }}" id="selling_price" class="form-control" placeholder="e.g. 500">
                                            </div>
                                            <label for="" class="error"><i><b>Note : </b> (i) Enter Selling Price of Concrete excluding Transport (TM) and Admixture. About Price will be excluding GST. (ii) Listing price of your RMC on Conmix website will different than your selling price after adding Conmix Margin. (iii) You can enter price for Transport (TM) and Admixture from Rate Setting Module in sub vendor dashboard. (iv) Kindly note that 'Conmix' will add it's commission on this selling price.</i></label>
                                        </div>
                                    </div>
                                    @endif
                                    <!-- Product Description -->
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Product Description <span class="str">*</span></label>
                                                <div class="cstm-input">
                                                    <textarea class="form-control" name="description" rows="4" cols="50" data-maxchar="5000" onkeyup="concreteDensity()" id="product_description" placeholder="Product Description">{{ $final_data['description'] }}</textarea>
                                                    <span class="character-text">Maximum 5000 Characters (<span class="character-counter"></span> remaining)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="address_id_div">
                                                <label>Design Mix Address <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <select name="address_id" id="address_id" class="form-control" onchange="concreteDensity()">
                                                        
                                                        @php

                                                            $profile = session('supplier_profile_details', null);

                                                        @endphp
                                                        @if(!isset($profile["master_vendor_id"]))
                                                            <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                            @if(isset($data["address_data"]))
                                                                @foreach($data["address_data"] as $value)
                                                                    <option value="{{ $value['_id'] }}" {{ $final_data['address_id'] == $value['_id'] ? 'selected' : '' }}>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            @if(isset($data["address_data"]))
                                                                @foreach($data["address_data"] as $value)
                                                                    @if($profile["user_id"] == $value["sub_vendor_id"])
                                                                        <option value="{{ $value['_id'] }}" selected>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group" id="mix_image_div">
                                                <label>Upload Mix Image <span class="str">*</span></label>
                                                <div class="file-browse">
                                                    <span class="button-browse"> Browse <input type="file" name="mix_image"></span>
                                                    <input type="text" id="mix_image_text"  class="form-control browse-input" placeholder="e.g. jpg" readonly="">
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                <div class="col-md-12">
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="form-group m-b0">
                                        <div class="input-group avlbl-sswich m-t15" id="is_availble_div">
                                            <label class="wid-120">Available :</label>
                                            <div class="verified-switch-btn cstm-css-checkbox">
                                                <label class="new-switch1 switch-green">
                                                    <input type="checkbox" name="is_availble" class="switch-input" {{ isset($final_data['is_available']) ? ($final_data['is_available'] == 'true'  ? "checked" : '') : '' }}>
                                                    <span class="switch-label" data-on="Yes" data-off="No"></span>
                                                    <span class="switch-handle"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="form-group m-b0">
                                        <div class="input-group avlbl-sswich m-t15">
                                            <label>Customization Available :</label>
                                            <div class="verified-switch-btn cstm-css-checkbox">
                                                <label class="new-switch1 switch-green">
                                                    <input type="checkbox" name="is_custom" class="switch-input" >
                                                    <span class="switch-label" data-on="Yes" data-off="No"></span>
                                                    <span class="switch-handle"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
</div>
                                <input type="hidden" id="is_supplier" value="yes"/>
                                <input type="hidden" id="is_supplier_add_edit" value="edit"/>
                                <input type="hidden" name="add_mix_form_type" id="add_mix_form_type" value="edit"/>
                                <!-- <input type="hidden" name="cement_brand_name" id="cement_brand_name" value="{{ $final_data['cement_brand_name'] }}"/>
                                <input type="hidden" name="sand_source_name" id="sand_source_name" value="{{ $final_data['sand_source_name'] }}"/>
                                <input type="hidden" name="aggregate_source_name" id="aggregate_source_name" value="{{ $final_data['aggregate_source_name'] }}"/>
                                <input type="hidden" name="fly_ash_source_name" id="fly_ash_source_name" value="{{ $final_data['fly_ash_source_name'] }}"/>
                                <input type="hidden" name="aggregate1_sub_category_name" id="aggregate1_sub_category_name" value="{{ $final_data['aggregate1_sub_category_name'] }}"/>
                                <input type="hidden" name="aggregate2_sub_category_name" id="aggregate2_sub_category_name" value="{{ $final_data['aggregate2_sub_category_name'] }}"/>
                                <input type="hidden" name="admix_brand_name" id="admix_brand_name" value="{{ isset($final_data['admix_brand_name']) ? $final_data['admix_brand_name'] : '' }}"/> -->
                                <input type="hidden" name="mix_id" id="mix_id" value="{{ $final_data['_id'] }}"/>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group m-t20">

                                        <input type="hidden" id="is_concrete_density_error" value="0" />
                                        <input type="hidden" id="is_WC_ratio_error" value="0" />
                                        <input type="hidden" id="is_admixture_percent_error" value="0" />
                                     


                                            <div class="progress_form mb-4">
                                                <div id="custom_mix_progress" class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>

                                        <span class="error" id="concrete_density_error_span"></span>
                                        <div class="input-group">
                                            <button type="submit" id="custom_design_mix_button" class="site-button m-r10">Save</button>
                                            <a href="{{ route('design_mix_detail') }}" class="site-button gray">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

<script>

$('#us2').locationpicker({
    enableAutocomplete: true,
        enableReverseGeocode: true,
    radius: 0,
    inputBinding: {
        latitudeInput: $('#us2_lat'),
        longitudeInput: $('#us2_lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2_address')
    },
    onchanged: function (currentLocation, radius, isMarkerDropped) {

            // var addressComponents = $(this).locationpicker('map').location.addressComponents;
        console.log(currentLocation);  //latlon
        // updateControls(addressComponents); //Data
        }
    });

    function updateControls(addressComponents) {
        console.log(addressComponents);
    }

</script>


@endsection