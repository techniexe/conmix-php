
<!--Contact Detail start -->
<div class="bank-detail-main-block design_mix_popup_bx wht-tble-bg">
    <div class="row">
        <?php //dd($data["designMixVariantDetails"]); ?>
        @if(isset($data["designMixVariantDetails"]) && (count($data["designMixVariantDetails"]) > 0))

            @foreach($data["designMixVariantDetails"] as $value)
                <div class="col-md-6">
                    <div class="custom_rmc_bx mb-3">
                        <div class="top-right-edit-link">
                            <a href="{{ route('design_mix_edit_view',$value['_id']) }}" class="site-button green button-sm"><i class="fa fa-pencil-square-o"></i></a>
                        </div>
                        <div class="custom_rmc_info">
                            <h5>{{ isset($value["concrete_grade_name"]) ? $value["concrete_grade_name"] : '' }} - {{ isset($value["product_name"]) ? $value["product_name"] : '' }}</h5>
                            <!-- <h6 class="mb-1"><i class="fa fa-map-marker"></i> Plant Name OR Address</h6> -->
                            <hr class="my-2">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>Cement ( Kg ) : <span>{{ isset($value["cement_quantity"]) ? $value["cement_quantity"] : '' }}</span></p>
                                    <p>Coarse Sand ( Kg ) : <span>{{ isset($value["sand_quantity"]) ? $value["sand_quantity"] : '' }}</span></p>
                                    <p>Fly Ash ( Kg ) : <span>{{ isset($value["fly_ash_quantity"]) ? $value["fly_ash_quantity"] : '' }}</span></p>
                                    <p>Admixture ( Kg ) : <span>{{ isset($value["ad_mixture_quantity"]) ? $value["ad_mixture_quantity"] : '' }}</span></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>{{ $value["aggregate1_sub_category_name"] }} ( Kg ) : <span>{{ isset($value["aggregate1_quantity"]) ? $value["aggregate1_quantity"] : '' }}</span></p>
                                    <p>{{ $value["aggregate2_sub_category_name"] }} ( Kg ) : <span>{{ isset($value["aggregate2_quantity"]) ? $value["aggregate2_quantity"] : '' }}</span></p>
                                    
                                    <p>Water ( Ltr ) : <span>{{ isset($value["water_quantity"]) ? $value["water_quantity"] : '' }}</span></p>
                                    <p>Grade : <span>{{ isset($value["concrete_grade_name"]) ? $value["concrete_grade_name"] : '' }}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="custom_rmc_info custom_rmc_dec p-a10">
                            <button class="site-button pull-right m-r10" onclick="viewDesignMixCombination('{{ $value['_id'] }}','{{ $value["concrete_grade_name"] }}')">View Rate</button>
                        </div>
                        @if(false)
                            <div class="custom_rmc_info custom_rmc_dec p-a10">
                                <b>Design Mix Description</b>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p>Cement Brand : <span>{{ isset($value["cement_brand_name"]) ? $value["cement_brand_name"] : '' }}</span></p>
                                        <p> CoarseSand Source : <span>{{ isset($value["sand_source_name"]) ? $value["sand_source_name"] : '' }}</span></p>
                                        <p>Coarse Sand Zone ( mm ) : <span>{{ isset($value["sand_zone_name"]) ? $value["sand_zone_name"] : '' }}</span></p>
                                        <p>Aggregate Source : <span>{{ isset($value["aggregate_source_name"]) ? $value["aggregate_source_name"] : '' }}</span></p>
                                        <p>Admixture Brand ( Kg ) : <span>{{ isset($value["admix_brand_name"]) ? $value["admix_brand_name"] : '' }}</span></p>
                                        <p>Admixture Type ( Kg ) : <span>{{ isset($value["admix_category_name"]) ? $value["admix_category_name"] : '' }}</span></p>
                                        <p>Fly Ash Source Name : <span>{{ isset($value["fly_ash_source_name"]) ? $value["fly_ash_source_name"] : '' }}</span></p>
                                        <p>Water : <span>Regular</span></p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
            @else
                <div class="col-md-6"> No Design Mix Available</div>

        @endif


    </div>
</div>
<!--Contact Detail end -->
