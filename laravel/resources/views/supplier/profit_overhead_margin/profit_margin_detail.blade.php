@section('title', 'Supplier Profit Margin')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">

    <h1 class="breadcrums"><a href="#">Profit</a> / <span>Profit Margin</span></h1>

    <div class="clearfix"></div>
    <?php //dd($data['data']); ?>
    <div class="review-complaints-block wht-tble-bg m-b30">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Profit Margin List</h2>
                    
                    <button class="site-button pull-right m-r10" onclick="resetForm('add_profit_margin_form','add-profit-margin-details')" data-toggle="modal" data-target="#add-profit-margin-details">Add Profit Margin</button>
                    <a href="{{ route('profit_margin') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    @if(isset($data['data']) && count($data['data']) > 0)
                        <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    @endif
                    
                </div>

                

                <div class="support-ticket-list-block">
                    <div class="comn-table1 pull-left w-100 m-b20">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Plant Address</th>
                                    <th>Profit Margin (%)</th>
                                    <th>Created On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data['data']) && count($data['data']) > 0)
                                @php
                                    $total_record = count($data["data"]);
                                @endphp
                                @foreach($data["data"] as $value)  
                                    @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                <tr>
                                    
                                    <td>{{ isset($value["address"]["business_name"]) ? $value["address"]["business_name"] : '' }} - {{ isset($value["address"]["city_name"]) ? $value["address"]["city_name"] : '' }}, {{ isset($value["address"]["state_name"]) ? $value["address"]["state_name"] : '' }}</td>
                                    <td>{{ isset($value["margin_percentage"]) ? $value["margin_percentage"] : '' }}</td>
                                    
                                    <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                    
                                    <td class="text-nowrap">
                                        <a href="javascript:;" onclick="showProfitMarginEditDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-edit"></i></a> 
                                        <a href="javascript:;" onclick="requestOTPToMaster(4,'{{ $value['_id'] }}','')" class="site-button red button-sm" data-tooltip="Delete"><i class="fa fa-trash-o"></i></a>
                                        
                                    </td>
                                    
                                </tr>
                                @endforeach

                            @else
                                <tr>
                                      <td colspan="7" style="text-align: center;">No Record Found</td>
                                </tr>
                            @endif
                                
                            </tbody>
                        </table>
                    </div>

                    <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                    <div class="data-box-footer clearfix">
                        <div class="pagination-block">
                            <!-- <ul class="pagination justify-content-end m-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            </ul>
                            <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                            <div class="pagination justify-content-end m-0">
                                @if($is_previous_avail == 1)
                                    @if(isset($first_created_date))
                                    <form action="{{route(Route::current()->getName())}}" method="get">
                                        
                                        @if(Request::get('address_id'))
                                            <input type="hidden" name="address_id" value="{{ Request::get('address_id') ? Request::get('address_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('brand_id'))
                                            <input type="hidden" name="brand_id" value="{{ Request::get('brand_id') ? Request::get('brand_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('grade_id'))
                                            <input type="hidden" name="grade_id" value="{{ Request::get('grade_id') ? Request::get('grade_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('quantity_value'))
                                            <input type="hidden" name="quantity_value" value="{{ Request::get('quantity_value') ? Request::get('quantity_value') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('quantity'))
                                            <input type="hidden" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" >
                                        @endif
                                        
                                        
                                        
                                        <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                        <button type="submit" class="site-button">Previous</button>                                    
                                    </form>
                                    @endif
                                    @endif
                                    @if($is_next_avail == 1)
                                    @if(isset($last_created_date))
                                    <form action="{{route(Route::current()->getName())}}" method="get">
                                        
                                        
                                        @if(Request::get('address_id'))
                                            <input type="hidden" name="address_id" value="{{ Request::get('address_id') ? Request::get('address_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('brand_id'))
                                            <input type="hidden" name="brand_id" value="{{ Request::get('brand_id') ? Request::get('brand_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('grade_id'))
                                            <input type="hidden" name="grade_id" value="{{ Request::get('grade_id') ? Request::get('grade_id') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('quantity_value'))
                                            <input type="hidden" name="quantity_value" value="{{ Request::get('quantity_value') ? Request::get('quantity_value') : ''  }}" class="form-control" >
                                        @endif
                                        @if(Request::get('quantity'))
                                            <input type="hidden" name="quantity" value="{{ Request::get('quantity') ? Request::get('quantity') : ''  }}" class="form-control" >
                                        @endif
                                    
                                        
                                        <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                        <button type="submit" class="site-button">Next</button>
                                    </form>
                                    @endif
                                    @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
  <!--   <div class="customer-care-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="customer-care-content-block">
                    <div class="customer-care-left-block">
                        <img src="{{asset('assets/supplier/images/customer-service.png')}}">
                        
                        <h3>Call Us On: <a href="#">+91 98989 14789</a></h3>
                        <h4>Write Us On: <a href="#">info@example.com</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div>

@endsection