@section('title', 'Supplier Reports')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Reports</h1>
    <div class="clearfix"></div>
    <div class="review-complaints-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Reports List</h2>
                </div>
                <div class="reports_block">
                    <!-- <div class="reportsList_box">
                        <h3>Finances</h3>
                        <ul>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                        </ul>
                    </div> -->
                    <div class="reportsList_box">
                        <h3>Inventory</h3>
                        <ul>
                            <li><a href="{{ route('supplier_plant_address_report') }}">Plant Address List</a></li>
                            <li><a href="{{ route('supplier_billing_address_report') }}">Billing Address List</a></li>
                            <li><a href="{{ route('supplier_TM_driver_report') }}">TM Driver List</a></li>
                            <li><a href="{{ route('supplier_operator_listing_report') }}">CP Operator List</a></li>
                            <li><a href="{{ route('supplier_TM_listing_report') }}">TM List</a></li>
                            <li><a href="{{ route('supplier_CP_listing_report') }}">CP List</a></li>
                            <li><a href="{{ route('supplier_admix_stock_report') }}">Admix Stock</a></li>
                            <li><a href="{{ route('supplier_agg_stock_report') }}">Aggregate Stock</a></li>
                            <li><a href="{{ route('supplier_cement_stock_report') }}">Cement Stock</a></li>
                            <li><a href="{{ route('supplier_flyAsh_stock_report') }}">Flyash Stock</a></li>
                            <li><a href="{{ route('supplier_sand_stock_report') }}">Sand Stock</a></li>
                            <li><a href="{{ route('supplier_bank_report') }}">Bank Details</a></li>
                            <li><a href="{{ route('supplier_design_mix_report') }}">Design Mix</a></li>
                            
                        </ul>
                    </div>
                    <div class="reportsList_box">
                        <h3>Sales</h3>
                        <ul>
                            <li><a href="{{ route('supplier_order_list_report') }}">Orders</a></li>
                        </ul>
                    </div>
                    <!-- <div class="reportsList_box">
                        <h3>Clients</h3>
                        <ul>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection