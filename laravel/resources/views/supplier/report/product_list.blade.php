@section('title', 'Supplier Product List Report')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="main-title">Product</h1>
    <div class="clearfix"></div>
    <!--Product start -->
    <div class="product-list-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Product List</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <!-- <button class="site-button pull-right" onclick="resetForm('add_product_form','add-product-list')" data-toggle="modal" data-target="#add-product-list">Add Product</button> -->
                </div>
                <div class="product-list-block">
                @php
                        
                            $first_created_date = '';
                            $last_created_date = '';
                            $count = 0;
                        @endphp
                @if(isset($data['data']) && count($data['data']) > 0)
                        <?php //dd($data['data']); ?>
                        
                        @foreach($data['data'] as $key => $value)
                            @if($count == 0)
                                @php 
                                    $first_created_date = $value["created_at"];
                                    $count++; 
                                @endphp
                            @endif
                            @php
                                $last_created_date = $value["created_at"];
                            @endphp
                            @php
                                $selling_unit = '';
                                $lattitude = '';
                                $longitude = '';
                                
                            @endphp
                            @if(isset($value["selling_unit"][0]))
                                @php
                                    $selling_unit = $value["productSubCategory"]["selling_unit"][0];
                                    
                                @endphp
                            @endif
                            @php
                                $lattitude = isset($value['address']['location']['coordinates'][1]) ? $value['address']['location']['coordinates'][1] : '';
                                $longitude = isset($value['address']['location']['coordinates'][0]) ? $value['address']['location']['coordinates'][0] : '';

                                $source_name = isset($value['address']['source_name']) ? $value['address']['source_name'] : '';
                            @endphp

                                <div class="product-list-contet-block m-b20">
                                    <div class="row">
                                         <!-- <a href="javascript:;" onclick="editProductStock('{{ $value["quantity"] }}', '{{ $value["_id"] }}')" class="site-button gray button-sm change-stock-link stockbtn" data-toggle="modal" data-target="#change-stock" >Update Stock</a> -->
                                        <!-- <div class="top-right-edit-link">
                                            <a href="javascript:;" class="site-button green button-sm" onclick="showEditProductDialog('{{ json_encode($value) }}')" data-tooltip="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                        </div> -->
                                        <div class="product-img-block">
                                            <img src="{{ isset($value['productCategory']['image_url']) ? $value['productCategory']['image_url'] : '' }}" alt="" />
                                        </div>
                                        <div class="col-md-8 col-sm-6 col-xs-12 product-list-details-block">
                                            <h3 class="product-list-title">{{ isset($value['productCategory']['category_name']) ? $value['productCategory']['category_name'] : '' }} - {{ isset($value['productSubCategory']['sub_category_name']) ? $value['productSubCategory']['sub_category_name'] : '' }} - {{ $source_name }}</h3>
                                        

                                            <ul>
                                                <li>
                                                    <p><span class="vTitle">Category : </span> <span class="vName">{{ isset($value['productCategory']['category_name']) ? $value['productCategory']['category_name'] : '' }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Unit Price : </span> <span class="vName"><i class="fa fa-rupee"></i> {{ isset($value['unit_price']) ? $value['unit_price'] : '' }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Quantity In Stock : </span> <span class="vName">{{ isset($value['quantity']) ? $value['quantity'] : '' }} MT</span></p>

                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Min. Order Acceptance : </span> <span class="vName">{{ isset($value['minimum_order']) ? $value['minimum_order'] : '' }} MT</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Date & Time : </span> <span class="vName">{{ isset($value['created_at']) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span></p>

                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Verified By Admin : </span> <span class="vName"><span class="badge {{ $value['verified_by_admin'] ? 'bg-green' : 'bg-red' }} float-none">{{ $value["verified_by_admin"] ? "Yes" : "No" }}</span></span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Available : </span> <span class="vName"><span class="badge {{ $value['is_available'] ? 'bg-green' : 'bg-red' }} float-none">{{ $value["is_available"] ? "Yes" : "No" }}</span></span></p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <h3 class="product-list-title"><i class="fa fa-map-marker"></i> Pickup Address <a href="javascript:;" onclick="viewMap({{ $lattitude }}, {{ $longitude }})" data-tooltip="View Map" class="pl-vw-mp"></a></h3>
                                            <div class="product-address-details">
                                                <h4>{{ isset($value['supplier']['full_name']) ? $value['supplier']['full_name'] : '' }}</h4>
                                                <p>{{ isset($value['supplier']['mobile_number']) ? $value['supplier']['mobile_number'] : '' }}</p>
                                                <p>{{ isset($value['address']['line1']) ? $value['address']['line1'] : '' }} <br> {{ isset($value['address']['line2']) ? $value['address']['line2'] : '' }}, {{ isset($value['address']['city_name']) ? $value['address']['city_name'] : '' }} {{ isset($value['address']['pincode']) ? $value['address']['pincode'] : '' }}, {{ isset($value['address']['state_name']) ? $value['address']['state_name'] : '' }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    
                         @endforeach

                    @else
                        <p style="text-align: center;">No Record Found</p>
                    @endif

                    
                    <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif
                    
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        ApiConfig::PAGINATION_LIMIT<p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                       
                            
                                <div class="pagination justify-content-end m-0">
                                @if($is_previous_avail == 1)
                                    @if(isset($first_created_date))
                                    <form action="{{route(Route::current()->getName())}}" method="get">
                                        
                                        
                                        
                                        <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                        <button type="submit" class="site-button">Previous</button>                                    
                                    </form>
                                    @endif
                                    @endif
                                    @if($is_next_avail == 1)
                                    @if(isset($last_created_date))
                                    <form action="{{route(Route::current()->getName())}}" method="get">
                                        
                                        
                                        
                                        <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                        <button type="submit" class="site-button">Next</button>
                                    </form>
                                    @endif
                                    @endif
                                </div>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Product end -->
</div>

@endsection
