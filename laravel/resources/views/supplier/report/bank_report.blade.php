@section('title', 'Supplier Bank Detail Report')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums">Reports / <span>Bank Detail</span></h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <!-- <h2 class="sub-title pull-left">Orders</h2> -->
                    <div class="d-flex justify-content-between align-items-center flex-wrap w-100">
                        <h2 class="sub-title pull-left">Bank Detail</h2>
                        <div class="d-flex align-items-center">
                            <div class="range_picker m-r10">
                                <input type="text" class="form-control" name="daterange" value="01/01/2018 - 01/15/2018">
                                
                                <i class="fa fa-calendar"></i>
                            </div>

                            <div class="dropdown cstm-dropdown-select exprt-drpdwn pull-right">
                                <button class="site-button m-r10 icn-btn" type="button" data-toggle="dropdown" aria-expanded="true"><span class="dropdown-label">Export <i class="fa fa-download"></i></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                    <!-- <li><a class="dropdown-item" href="javascript:;" onclick="downloadSupplierOrders('pdf')">PDF</a></li> -->
                                    <li><a class="dropdown-item" href="javascript:;" onclick="downloadBankDetails('excel')">Excel</a></li>
                                </ul>
                            </div>
                            <a href="{{route(Route::current()->getName())}}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                            <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                        </div>
                    </div>
                </div>
                <div class="reports_details_block">

                    
                    <div class="comn-table1 report_table pull-left w-100">
                        <table class="table">
                        <thead>
                            <tr>
                                <th>Bank Name</th>
                                <th>Account Holder Name</th>
                                <th>Account Number</th>
                                <th>IFSC Code</th>
                                <th>Account Type</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php //dd($data['data']); ?>
                            @php
                                $first_created_date = '';
                                $last_created_date = '';
                                $count = 0;
                            @endphp
                        @if(isset($data['data']) && count($data['data']) > 0)

                            @foreach($data["data"] as $value)
                                @if($count == 0)
                                    @php
                                        $first_created_date = $value["created_at"];
                                        $count++;
                                    @endphp
                                @endif
                                @php
                                    $last_created_date = $value["created_at"];
                                @endphp

                                    <tr>

                                        <td>{{ isset($value["bank_name"]) ? ($value["bank_name"]) : '' }}</td>
                                        <td>{{ isset($value["account_holder_name"]) ? ($value["account_holder_name"]) : '' }}</td>
                                        <td>{{ isset($value["account_number"]) ? ($value["account_number"]) : '' }}</td>
                                        
                                        <td>{{ isset($value["ifsc"]) ? ($value["ifsc"]) : '' }}</td>
                                        <td>{{ isset($value["account_type"]) ? ($value["account_type"]) : '' }}</td>
                                        
                                        <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                    </tr>

                            @endforeach

                        @else
                            <tr>
                                    <td colspan="12" align="center">No Record Found</td>
                            </tr>
                        @endif

                        </tbody>
                        </table>
                    </div>
                   

                </div>

                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="pagination-block m-t20">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                    <div class="pagination justify-content-end m-0">
                    @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{route(Route::current()->getName())}}" method="get">

                                <!-- @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif -->
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif

                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{route(Route::current()->getName())}}" method="get">


                                <!-- @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif -->
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif

                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>

                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>

<script>
$(function() {

    var today = new Date();
    var end = new Date();
    // today.setDate(today.getDate() + 2);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    end = dd + '/' + mm + '/' + yyyy;
    console.log(today);

    @if(Request::get('start_date'))

        var date = new Date('{{Request::get('start_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        today = dd + '/' + mm + '/' + yyyy;
        // today = yyyy + '-' + mm + '-' + dd;
        console.log("start_date..."+today);
    @endif

    @if(Request::get('end_date'))

        var date = new Date('{{Request::get('end_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        end = dd + '/' + mm + '/' + yyyy;
        // end = yyyy + '-' + mm + '-' + dd;
        console.log("end_date..."+end);
    @endif

  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
    startDate: today,
    endDate: end,
    locale: {
            format: 'DD/MM/YYYY'
        }
  }, function(start, end, label) {
      $("#report_start_date").val(start.format('YYYY-MM-DD'));
      $("#report_end_date").val(end.format('YYYY-MM-DD'));

      $('form[name="supplier_report_form"]').submit();

        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>

@endsection