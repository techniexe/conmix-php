@section('title', 'Supplier Transit Mixture')
@extends('supplier.layouts.supplier_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="main-title">Transit Mixer (TM)</h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="vehicle-block vehicle-category-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Transit Mixer (TM) List</h2>
                    <a href="{{ route('TM_add_view') }}" class="site-button m-r10">Add TM</a>
                    <a href="{{ route('TM_detail') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    @if(isset($data['data']) && count($data['data']) > 0)
                        <button id="filterCollapse" class="site-button outline"><i class="fa fa-filter"></i></button>
                    @endif
                </div>
                
                <div class="vehicle-list-block">
                @php
                            $first_created_date = '';
                            $last_created_date = '';
                            $count = 0;
                            //dd($data['data']);
                        @endphp
                @if(isset($data['data']) && count($data['data']) > 0)
                        
                        @foreach($data["data"] as $value)  
                            @if($count == 0)
                                @php 
                                   $first_created_date = $value["created_at"];
                                    $count++; 
                                @endphp
                            @endif
                            @php
                               $last_created_date = $value["created_at"];
                            @endphp
                    <div class="vehicle-list-contet-block m-b20">
                        <div class="edit_unawail_btn">
                            <a href="#" onclick="viewTMunAvailability('{{$value['_id']}}','{{ isset($value['vendorDetails']['_id']) ? $value['vendorDetails']['_id'] : $value['_id'] }}')" class="site-button red">UnAvailable On</a>
                            <a href="{{ route('TM_edit_view', $value['_id']) }}" class="site-button green" data-tooltip="Edit"><i class="fa fa-edit"></i></a>
                        </div>
                        <!-- <form action="" method="post">
                        @csrf
                            <input type="hidden" value="{{ json_encode($value) }}" name="vehicle_details"/>
                            <button type="submit" class="site-button green pull-right vehical-edit-icon" data-tooltip="Edit"><i class="fa fa-edit"></i></button>
                        </form> -->
                        <ul>
                            <li>
                                <p><span class="vTitle">Category : </span> <span class="vName">{{ isset($value["TM_category"]['category_name']) ? $value["TM_category"]['category_name'] : '' }}</span></p>
                                <p><span class="vTitle">Sub Category : </span> <span class="vName">{{ isset($value["TM_sub_category"]['sub_category_name']) ? $value["TM_sub_category"]['sub_category_name'] : '' }}</span></p>
                                <p><span class="vTitle">TM No : </span> <span class="vName text-uppercase">{{ isset($value["TM_rc_number"]) ? $value["TM_rc_number"] : '' }}</span></p>
                                <p><span class="vTitle">Per Cu.Mtr/KM Rate : </span> <span class="vName"><i class="fa fa-rupee"></i> {{ isset($value["per_Cu_mtr_km"]) ? $value["per_Cu_mtr_km"] : '' }}</span></p>
                                <p><span class="vTitle">Min Trip Price (Per Cu.Mtr/KM) : </span> <span class="vName"><i class="fa fa-rupee"></i> {{ isset($value["min_trip_price"]) ? $value["min_trip_price"] : '' }}</span></p>
                            </li>
                            <li>
                                <p><span class="vTitle">Make : </span> <span class="vName">{{ isset($value["manufacturer_name"]) ? $value["manufacturer_name"] : '' }}</span></p>
                                <p><span class="vTitle">Type Model : </span> <span class="vName">{{ isset($value["TM_model"]) ? $value["TM_model"] : '' }}</span></p>
                                <p><span class="vTitle">Manufacture Year : </span> <span class="vName">{{ isset($value["manufacture_year"]) ? $value["manufacture_year"] : '' }}</span></p>
                                <p><span class="vTitle">Created On : </span> <span class="vName">{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span></p>
                                
                               
                            </li>
                            <li>
                                <p><span class="vTitle">Delivery Range : </span> <span class="vName">{{ isset($value["delivery_range"]) ? $value["delivery_range"] : '' }} KM</span></p>
                                 <p><span class="vTitle">Driver 1 : </span> <span class="vName">{{ isset($value["driver1_info"]["driver_name"]) ? $value["driver1_info"]["driver_name"] : '' }} - {{ isset($value["driver1_info"]["driver_mobile_number"]) ? $value["driver1_info"]["driver_mobile_number"] : '' }}</span></p>
                                <p><span class="vTitle">Driver 2 : </span> <span class="vName">{{ isset($value["driver2_info"]["driver_name"]) ? $value["driver2_info"]["driver_name"] : '' }} - {{ isset($value["driver2_info"]["driver_mobile_number"]) ? $value["driver2_info"]["driver_mobile_number"] : '' }}</span></p>
                                <!-- <p><span class="vTitle">Available : </span> <span class="vName">{{isset($value["is_active"]) ? ($value["is_active"] ? 'Yes' : 'No') : '' }}</span></p> -->
                                <p><span class="vTitle">Is Insurance Active : </span> <span class="vName">{{isset($value["is_insurance_active"]) ? ($value["is_insurance_active"] ? 'Yes' : 'No') : '' }}</span></p>
                                
                            </li>
                            <li>
                                <p><span class="vTitle">Plant Address : </span> <span class="vName">
                                    {{ isset($value["business_name"]) ? $value["business_name"] : '' }},
                                    {{ isset($value["line1"]) ? $value["line1"] : '' }},
                                    {{ isset($value["line2"]) ? $value["line2"] : '' }},
                                    {{ isset($value["city_name"]) ? $value["city_name"] : '' }} - {{ isset($value["pincode"]) ? $value["pincode"] : '' }},
                                    {{ isset($value["state_name"]) ? $value["state_name"] : '' }},
                                    </span>
                                </p>
                            </li>
                        </ul>
                        <ul class="mt-3 vehicle_gallary">
                            <li>
                                <div id="gallery">
                                    @if(isset($value["rc_book_image_url"]))
                                        <a href="{{ $value['rc_book_image_url'] }}" class="vehicle-img-block gal_link"><img src="{{ $value['rc_book_image_url'] }}"></a>
                                    @endif

                                    @if(isset($value["insurance_image_url"]))
                                        <a href="{{ $value['insurance_image_url'] }}" class="vehicle-img-block gal_link"><img src="{{ $value['insurance_image_url'] }}"></a>
                                    @endif
                                    
                                    @if(isset($value["TM_image_url"]))
                                        <a href="{{ $value['TM_image_url'] }}" class="vehicle-img-block gal_link"><img src="{{ $value['TM_image_url'] }}"></a>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </div>
                        @endforeach

                    @else
                        <p style="text-align: center;">No Record Found</p>
                    @endif
                    

                    <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        <div class="pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if($first_created_date)
                            <form action="{{ route('logistic_vehicles') }}" method="get">
                                @if(Request::get('address_id'))
                                    <input type="hidden" name="address_id" value="{{ Request::get('address_id') ? Request::get('address_id') : ''  }}" class="form-control" placeholder="Name">
                                @endif
                                @if(Request::get('TM_category_id'))
                                    <input type="hidden" name="TM_category_id" value="{{ Request::get('TM_category_id') ? Request::get('TM_category_id') : ''  }}" class="form-control" placeholder="Mobile No">
                                @endif
                                @if(Request::get('TM_sub_category_id'))
                                    <input type="hidden" name="TM_sub_category_id" value="{{ Request::get('TM_sub_category_id') ? Request::get('TM_sub_category_id') : ''  }}" class="form-control" placeholder="Email">
                                @endif
                                @if(Request::get('TM_rc_number'))
                                    <input type="hidden" name="TM_rc_number" value="{{ Request::get('TM_rc_number') ? Request::get('TM_rc_number') : ''  }}" class="form-control" placeholder="Email">
                                @endif
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if($last_created_date)
                            <form action="{{ route('logistic_vehicles') }}" method="get">
                                @if(Request::get('address_id'))
                                    <input type="hidden" name="address_id" value="{{ Request::get('address_id') ? Request::get('address_id') : ''  }}" class="form-control" placeholder="Name">
                                @endif
                                @if(Request::get('TM_category_id'))
                                    <input type="hidden" name="TM_category_id" value="{{ Request::get('TM_category_id') ? Request::get('TM_category_id') : ''  }}" class="form-control" placeholder="Mobile No">
                                @endif
                                @if(Request::get('TM_sub_category_id'))
                                    <input type="hidden" name="TM_sub_category_id" value="{{ Request::get('TM_sub_category_id') ? Request::get('TM_sub_category_id') : ''  }}" class="form-control" placeholder="Email">
                                @endif
                                @if(Request::get('TM_rc_number'))
                                    <input type="hidden" name="TM_rc_number" value="{{ Request::get('TM_rc_number') ? Request::get('TM_rc_number') : ''  }}" class="form-control" placeholder="Email">
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

@endsection