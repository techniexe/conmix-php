@section('title', 'Edit Supplier Transit Mixture')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Transit Mixer (TM)</h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="vehicle-block vehicle-category-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="data-box-header m-b20">
                    <h2 class="box-title">Edit Transit Mixer (TM)</h2>
                </div>
                <div class="vehicle-list-block">
                    @if(isset($data["error"]))
                        <div class="error">{{ $data["error"]["message"] }}</div>
                    @endif
                    @php
                        $final_data = $data["data"];
                        //dd($final_data);
                        $latitude = isset($final_data["pickup_location"]["coordinates"][1]) ? $final_data["pickup_location"]["coordinates"][1] : 0;
                        $longitude = isset($final_data["pickup_location"]["coordinates"][0]) ? $final_data["pickup_location"]["coordinates"][0] : 0;
                    @endphp
                    <div class="vehicle-block vehicle-category-block">
                        <form action="" method="post" name="add_vehicles_form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group" >
                                        <label>TM Category <span class="str">*</span></label>
                                        <div class="cstm-select-box" id="vehicleCategoryId_div">
                                            <select class="form-control" name="vehicleCategoryId" id="vehicleCategoryId" disabled>
                                                <option disabled="disabled" selected="selected">Select vehicle category</option>
                                                @if(isset($data["vehicle_categoies"]))
                                                    @foreach($data["vehicle_categoies"] as $cat_value)
                                                    <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == $final_data['TM_category_id'] ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                                                    @endforeach
                                                @endif
                                                
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" >
                                        <label>TM Sub Category <span class="str">*</span></label>
                                        <div class="cstm-select-box" id="vehicleSubCategoryId_div">
                                            <select class="form-control" name="vehicleSubCategoryId" id="vehicleSubCategoryId" disabled>
                                                <option disabled="disabled" selected="selected">Select vehicle sub category</option>
                                                @if(isset($data["subcategories"]))
                                                    @foreach($data["subcategories"] as $cat_value)
                                                        <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == $final_data['TM_sub_category_id'] ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label>TM Pick Up Address <span class="str">*</span></label>
                                        <div class="cstm-select-box" id="address_id_div">
                                            <!-- <select class="form-control" name="address_id" id="address_id"> -->
                                            <select class="form-control" name="address_id" id="TM_address_id">
                                                <!-- <option disabled="disabled" selected="selected">Select TM Pick Up Address</option> -->
                                                @if(isset($data["address_data"]))
                                                    @if(false)
                                                        @foreach($data["address_data"] as $value)
                                                            <!-- <option value="{{ $value['_id'] }}">{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option> -->
                                                        @endforeach
                                                    @endif
                                                @endif

                                                @php

                                                    $profile = session('supplier_profile_details', null);
                        
                                                @endphp
                                                @if(!isset($profile["master_vendor_id"]))
                                                    <option disabled="disabled" selected="selected">Select Plant Address</option>
                                                    @if(isset($data["address_data"]))
                                                        @foreach($data["address_data"] as $value)
                                                            <option value="{{ $value['_id'] }}" {{ $value['_id'] == $final_data['address_id'] ? 'selected' : '' }}>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @if(isset($data["address_data"]))
                                                        @foreach($data["address_data"] as $value)
                                                            @if($profile["user_id"] == $value["sub_vendor_id"])
                                                                <option value="{{ $value['_id'] }}" {{ $value['_id'] == $final_data['address_id'] ? 'selected' : '' }}>{{ $value["business_name"] }}, {{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                       
                                    <div class="form-group">
                                        <label>TM Registration No <span class="str">*</span></label>
                                        <div class="row">
                                            <?php
                                                $rc_number = $final_data['TM_rc_number'];
                                                $rc_number_arr = explode("-",$rc_number);
                                            ?>
                                            <div class="col-md-2 pd-r-5">
                                                    <input type="text" value="{{ isset($rc_number_arr[0]) ? $rc_number_arr[0] : '' }}" name="vehicle_rc_number_1" class="form-control text-uppercase" placeholder="GJ">
                                                </div>
                                            <div class="col-md-2 pd-lr-5">
                                                <input type="text" value="{{ isset($rc_number_arr[1]) ? $rc_number_arr[1] : '' }}" name="vehicle_rc_number_2" class="form-control" placeholder="01">
                                            </div>
                                            <div class="col-md-2 pd-lr-5">
                                                <input type="text" value="{{ isset($rc_number_arr[2]) ? $rc_number_arr[2] : '' }}" name="vehicle_rc_number_3" class="form-control text-uppercase" placeholder="AB">
                                            </div>
                                            <div class="col-md-6 pd-l-5">
                                                <input type="text" value="{{ isset($rc_number_arr[3]) ? $rc_number_arr[3] : '' }}" name="vehicle_rc_number_4" class="form-control" placeholder="1234">
                                            </div>
                                        </div>
                                    </div>
                                    

                                    <div class="row">
                                        <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Per MT/KM Rate <span class="notinfo" data-tooltip="&#8377; 3 to 5 Allowed"><i class="fa fa-info-circle"></i></span></label>
                                                    <div class="input-group-prepend m--l1"><span class="input-group-text"><i class="fa fa-rupee"></i></span></div>
                                                    <input type="text" name="per_metric_ton_per_km_rate" id="per_metric_ton_per_km_rate" value="{{ isset($final_data['per_metric_ton_per_km_rate']) ? $final_data['per_metric_ton_per_km_rate'] : '' }}" class="form-control" placeholder="e.g 9.99">
                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Min Trip Price <span class="str">*</span></label>
                                                    <div class="input-group-prepend m--l1"><span class="input-group-text"><i class="fa fa-rupee"></i></span></div>
                                                    <input type="text" name="min_trip_price" value="{{ isset($final_data['min_trip_price']) ? $final_data['min_trip_price'] : '' }}" class="form-control" placeholder="e.g. 500">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Per Cu.Mtr/KM Price <span class="str">*</span> 
                                                    <!-- <span class="notinfo" id="" data-tooltip=""><i class="fa fa-info-circle"></i> -->
                                                    </label>
                                                    <div class="input-group-prepend m--l1"><span class="input-group-text"><i class="fa fa-rupee"></i></span></div>
                                                    <input type="text" name="per_Cu_mtr_km" value="{{ isset($final_data['per_Cu_mtr_km']) ? $final_data['per_Cu_mtr_km'] : '' }}" class="form-control" placeholder="e.g. 500">

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>TM Make <span class="str">*</span></label>
                                                <input type="text" name="manufacturer_name" value="{{ $final_data['manufacturer_name'] }}" class="form-control" placeholder="e.g. TATA Hyva">
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>TM Type Modal <span class="str">*</span></label>
                                                <input type="text" name="vehicle_model" value="{{ isset($final_data['TM_model']) ? $final_data['TM_model']: '' }}" class="form-control" placeholder="e.g. 2523">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="form-group">
                                        <div class="input-group">
                                            <label>Driver name <span class="str">*</span></label>
                                            <input type="text" name="driver_name" value="{{ isset($final_data['driver_name']) ? $final_data['driver_name']: '' }}" class="form-control" placeholder="e.g. Driver Name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Driver Mobile Number <span class="str">*</span></label>
                                            <input type="text" name="driver_mobile_number" value="{{ isset($final_data['driver_mobile_number']) ? $final_data['driver_mobile_number']: '' }}" class="form-control" placeholder="e.g. +919874563210">
                                        </div>
                                    </div> -->
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>TM Manufacture Year <span class="str">*</span></label>
                                                <input type="text" name="manufacture_year" value="{{ isset($final_data['manufacture_year']) ? $final_data['manufacture_year']: '' }}" class="form-control" placeholder="e.g. 2019">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group" id="delivery_range_div">
                                                    <label>Delivery Range <span class="str">*</span></label>
                                                    <input type="text" name="delivery_range" value="{{ isset($final_data['delivery_range']) ? $final_data['delivery_range']: '' }}" class="form-control brd-rds-rit" placeholder="e.g. 100">
                                                    <div class="input-group-prepend m--l1"><span class="input-group-text">KM</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" id="rc_book_image_div">
                                            <label>Upload RC Book Image </label>
                                            <div class="file-browse">
                                                <span class="button-browse"> Browse <input type="file" name="rc_book_image"></span>
                                                <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group" id="insurance_image_div">
                                            <label>Upload Insurance Image </label>
                                            <div class="file-browse">
                                                <span class="button-browse"> Browse <input type="file" name="insurance_image"></span>
                                                <input type="text"  class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" id="vehicle_image_div">
                                            <label>Upload TM Image </label>
                                            <div class="file-browse">
                                                <span class="button-browse"> Browse <input type="file" name="vehicle_image"></span>
                                                <input type="text"  class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group" id="driver1_id_div">
                                                    <label>Driver 1 <span class="str">*</span></label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="driver1_id" id="add_TM_driver_1">
                                                            <!-- <option disabled="disabled" selected="selected">Select Driver</option> -->
                                                            @if(isset($data["driver_data"]))
                                                                @foreach($data["driver_data"] as $value)
                                                                    <!-- <option value="{{ $value['_id'] }}" {{ $final_data['driver1_info']['_id'] == $value['_id'] ? 'selected' : ''  }}>{{ $value["driver_name"] }}</option> -->
                                                                @endforeach
                                                            @endif

                                                            <?php 
                                                                $driver_Data = array();
                                                            ?>
                                                            @if(!isset($profile["master_vendor_id"]))

                                                                <option disabled="disabled" selected="selected">Select Driver</option>
                                                                @if(isset($data["driver_data"]))
                                                                    @foreach($data["driver_data"] as $value)
                                                                        @if($final_data['address_id'] == $value["addressDetails"]["_id"])
                                                                            <option value="{{ $value['_id'] }}" data-plant-id="{{ $value["addressDetails"]["_id"] }}" {{ $final_data['driver1_info']['_id'] == $value['_id'] ? 'selected' : ''  }}>{{ $value["driver_name"] }} - {{ $value["addressDetails"]["business_name"] }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                    <?php
                                                                        $driver_Data = $data["driver_data"];
                                                                    ?>
                                                                @endif
                                                            @else
                                                                <option disabled="disabled" selected="selected">Select Driver</option>
                                                                @if(isset($data["driver_data"]))
                                                                    @foreach($data["driver_data"] as $value)
                                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                                            <option value="{{ $value['_id'] }}" data-plant-id="{{ $value["addressDetails"]["_id"] }}" {{ $final_data['driver1_info']['_id'] == $value['_id'] ? 'selected' : ''  }}>{{ $value["driver_name"] }} - {{ $value["addressDetails"]["business_name"] }}</option>
                                                                            <?php
                                                                                array_push($driver_Data,$value);
                                                                            ?>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            
                                                            @endif

                                                        </select>

                                                        @if(isset($data["driver_data"]))
                                                            <!-- <input type="hidden" id="add_TM_dirver_1_data" value="{{ json_encode($data['driver_data']) }}" /> -->
                                                        @endif
                                                        @if(isset($driver_Data))
                                                            <input type="hidden" id="add_TM_dirver_1_data" value="{{ json_encode($driver_Data) }}" />
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group" id="driver2_id_div">
                                                    <label>Driver 2 <span class="str">*</span></label>
                                                    <div class="cstm-select-box">
                                                        <select class="form-control" name="driver2_id" id="add_TM_driver_2">
                                                            <!-- <option disabled="disabled" selected="selected">Select Driver</option> -->
                                                            @if(isset($data["driver_data"]))
                                                                @foreach($data["driver_data"] as $value)
                                                                    <!-- <option value="{{ $value['_id'] }}" {{ isset($final_data['driver2_info']) ? ($final_data['driver2_info']['_id'] == $value['_id'] ? 'selected' : '') : ''  }}>{{ $value["driver_name"] }}</option> -->
                                                                @endforeach
                                                            @endif

                                                            <?php 
                                                                $driver_Data = array();
                                                            ?>
                                                            @if(!isset($profile["master_vendor_id"]))

                                                                <option disabled="disabled" selected="selected">Select Driver</option>
                                                                @if(isset($data["driver_data"]))
                                                                    @foreach($data["driver_data"] as $value)
                                                                        @if($final_data['address_id'] == $value["addressDetails"]["_id"])
                                                                            <option value="{{ $value['_id'] }}" data-plant-id="{{ $value["addressDetails"]["_id"] }}" {{ isset($final_data['driver2_info']) ? ($final_data['driver2_info']['_id'] == $value['_id'] ? 'selected' : '') : ''  }}>{{ $value["driver_name"] }} - {{ $value["addressDetails"]["business_name"] }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                    <?php
                                                                        $driver_Data = $data["driver_data"];
                                                                    ?>
                                                                @endif
                                                            @else
                                                                <option disabled="disabled" selected="selected">Select Driver</option>
                                                                @if(isset($data["driver_data"]))
                                                                    @foreach($data["driver_data"] as $value)
                                                                        @if($profile["user_id"] == $value["sub_vendor_id"])
                                                                            <option value="{{ $value['_id'] }}" data-plant-id="{{ $value["addressDetails"]["_id"] }}" {{ isset($final_data['driver2_info']) ? ($final_data['driver2_info']['_id'] == $value['_id'] ? 'selected' : '') : ''  }}>{{ $value["driver_name"] }} - {{ $value["addressDetails"]["business_name"] }}</option>
                                                                            <?php
                                                                                array_push($driver_Data,$value);
                                                                            ?>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            
                                                            @endif
                                                        </select>

                                                        @if(isset($data["driver_data"]))
                                                            <!-- <input type="hidden" id="add_TM_dirver_2_data" value="{{ json_encode($data['driver_data']) }}" /> -->
                                                        @endif
                                                        @if(isset($driver_Data))
                                                            <input type="hidden" id="add_TM_dirver_2_data" value="{{ json_encode($driver_Data) }}" />
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="form-group m-b0">
                                                <div class="avlbl-sswich m-t15 d-flex">
                                                    <label>Is Insurance Active :</label>
                                                    <div class="verified-switch-btn cstm-css-checkbox">
                                                        <label class="new-switch1 switch-green">
                                                            <input type="checkbox" name="is_insurance_active" class="switch-input" {{ $final_data['is_insurance_active'] == 'on' ? 'checked' : '' }}>
                                                            <span class="switch-label" data-on="Yes" data-off="No"></span>
                                                            <span class="switch-handle"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="form-group m-b0">
                                                <div class="avlbl-sswich m-t15 d-flex">
                                                    <label>Is GPS Active :</label>
                                                    <div class="verified-switch-btn cstm-css-checkbox">
                                                        <label class="new-switch1 switch-green">
                                                            <input type="checkbox" name="is_gps_enabled" class="switch-input" {{ $final_data['is_gps_enabled'] == 'on' ? 'checked' : '' }}>
                                                            <span class="switch-label" data-on="Yes" data-off="No"></span>
                                                            <span class="switch-handle"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <input type="hidden" name="add_vehicle_form_type" id="add_vehicle_form_type" value="edit"/>
                                <input type="hidden" name="vehicleId"  value="{{ $final_data['_id'] }}"/>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group m-t20">
                                        <div class="input-group">
                                            <button type="submit" id="TM_save_button" class="site-button m-r10">Save</button>
                                            <a href="{{ route('TM_detail') }}" class="site-button gray">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

<script>

$('#us2').locationpicker({
    enableAutocomplete: true,
        enableReverseGeocode: true,
    radius: 0,
    inputBinding: {
        latitudeInput: $('#us2_lat'),
        longitudeInput: $('#us2_lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2_address')
    },
    onchanged: function (currentLocation, radius, isMarkerDropped) {
            // var addressComponents = $(this).locationpicker('map').location.addressComponents;
        console.log(currentLocation);  //latlon  
        // updateControls(addressComponents); //Data
        }
    });

    function updateControls(addressComponents) {
        console.log(addressComponents);
    }

    $('#us2').locationpicker("location", {latitude: '{{ isset($latitude) ? $latitude : 0 }}', longitude: '{{ isset($longitude) ? $longitude : 0 }}'});
</script>


@endsection