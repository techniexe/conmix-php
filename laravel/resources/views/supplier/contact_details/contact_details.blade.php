@section('title', 'Supplier Contact Person')
@extends('supplier.layouts.supplier_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Contact Detail</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="contact-detail-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="contact-detail-block add-contact-detail-block">
                            <a href="javascript:;" onclick="resetForm('add_contact_detail_form','add-contact-details')" data-toggle="modal" data-target="#add-contact-details"><i>+</i></a>
                        </div>
                    </li>
                    @if(isset($data["data"]))
                    @php
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                            <li>
                                <div class="mCustomScrollbar contact-detail-block p-a15">
                                    <div class="top-right-edit-link">
                                        <a href="javascript:;" onclick="showContactDetailEditDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-toggle="modal" data-target="#add-product-list" ><i class="fa fa-pencil-square-o"></i></a>
                                    </div>
                                    <p><span class="vTitle"><i class="fa fa-user"></i> Person Name : </span> <span class="vName">{{ isset($value["person_name"]) ? $value["person_name"] : '' }}</span></p>
                                    <p><span class="vTitle"><i class="fa fa-suitcase"></i> Designation : </span> <span class="vName">{{ isset($value["title"]) ? $value["title"] : '' }}</span></p>
                                    <p><span class="vTitle"><i class="fa fa-phone-square"></i> Mobile No. : </span> <span class="vName">{{ isset($value["mobile_number"]) ? $value["mobile_number"] : '' }}</span></p>
                                    <p><span class="vTitle"><i class="fa fa-envelope"></i> Email : </span> <span class="vName">{{ isset($value["email"]) ? $value["email"] : '' }}</span></p>
                                    <p><span class="vTitle"><i class="fa fa-phone-square"></i> Alternate No. : </span> <span class="vName">{{ isset($value["alt_mobile_number"]) ? $value["alt_mobile_number"] : '' }}</span></p>
                                    <p><span class="vTitle"><i class="fa  fa-whatsapp"></i> Whatsapp No. : </span> <span class="vName">{{ isset($value["whatsapp_number"]) ? $value["whatsapp_number"] : '' }}</span></p>
                                    <p><span class="vTitle"><i class="fa fa-phone-square"></i> Landline No. : </span> <span class="vName">{{ isset($value["landline_number"]) ? $value["landline_number"] : '' }}</span></p>
                                </div>
                            </li>
                        @endforeach
                    @endif 
                    
                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection