@include('buyer.common.master')

<body class="ofrSldm ofrSldm--right">
    <div id="loadingDiv" style="position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 99999999999;
    background-color: #fff;
    display: block;
    align-items: center;
    justify-content: center;">

        @include('buyer.layouts.shimmer.simple_text')
    </div>
    <div class="page-wrap">
        @include('buyer.common.header')





            <!-- <div class="middle-container-wrap"> -->
                @yield('content')
            <!-- </div> -->
            </div>
@include('buyer.common.footer')
