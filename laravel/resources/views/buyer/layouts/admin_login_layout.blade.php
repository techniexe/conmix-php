@include('admin.common.master')

<body>
    <div class="wrapper lsf-main-block">
        <!-- SIDBAR NAV MENU START  -->
        <nav id="sidebar" class="NW-sidebar-wrapper">

        </nav>
        <!-- SIDBAR NAV MENU END  -->
        <!-- CONTENT BLOCK START  -->
        <div id="content">

            <!-- <div class="middle-container-wrap"> -->
                @yield('content')
            <!-- </div> -->
        </div>
        <!-- CONTENT BLOCK END  -->
        <footer class="w-100 bg-gray py-3 px-4 position-fixed">
            <aside class="left float-lg-left text-center">
                <p class="m-0">© Copyright {{date("Y")}}. All Rights Reserved. Conmix Pvt. Ltd.</p>
            </aside>
        </footer>
    </div>



@include('admin.common.footer')
<script type="text/javascript" src="{{asset('assets/admin/js/intlTelInput.js')}}"></script>
<script>
$(document).ready(function() {
    /*input mobile no with flag*/
    var input = document.querySelector("#mobileno");
    window.intlTelInput(input, {
      utilsScript: "js/utils.js" // just for formatting/placeholders etc
    });
    /*input mobile no with flag end*/
});

</script>
