
<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>

<!-- Confirmation Popup MESSAGE START -->



<!-- <div class="modal fade" id="otpModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">OTP</h5>
                <div class="modal-body p-tb20 p-lr10" id="otpMessage">
                    <label>Enter the 6 digits OTP sent on your registered mobile no. <span id="OTP_mob_no"></span> <span class="str">*</span></label>
                    <input type="text" id="otp" required class="form-control" placeholder="One Time Password">
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="site-button green m-r15" id="otpOk">Submit</button>
                    <button type="button" data-dismiss="modal" class="site-button red" id="otpCancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div> -->


<div class="modal fade" id="otpModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">OTP</h5>
                <p class="error" id="otpModal_error"></p>

                    <div class="login_fild otp_screen">
                        <div class="login_fild_inner">
                            <h6>OTP Confirmation Code</h6>
                            <p id="code_confirm_message"></p>
                            <label>Enter the 6 digits OTP sent on your registered mobile no. <span id="OTP_mob_no"></span> <span class="str">*</span></label>
                            <!-- <div class="timer_otp mb-3">
                                <span id="timer">
                                    00:<span id="time">60</span>
                                </span>
                            </div> -->
                        </div>

                        <div class="otp_textbox flex-row d-flex mb-4 justify-content-evenly">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" id="otp_code_1" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" id="otp_code_2" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" id="otp_code_3" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" id="otp_code_4" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" id="otp_code_5" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" id="otp_code_6" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                        </div>




                        <div class="d-grid">

                            <!-- <button class="btn btn-primary mt-2" name="otp_btn" value="otp_btn" type="submit">Continue</button> -->
                            <button class="btn btn-primary btn-success mt-2" name="otp_btn" id="otpOk" value="otp_btn" type="submit">VERIYFY</button>
                        </div>
                    </div>


            </div>
        </div>
    </div>
</div>

<!-- Confirmation Popup MESSAGE END -->

 <!-- select location popup start -->
 <div class="modal fade" id="Select_location_guest" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content mt-5">
        @php
            $profile = session('buyer_profile_details', null);
        @endphp
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <form action="{{ route('product_listing_form') }}" method="post" name="user_location_form" autocomplete="off">
                @csrf
                <input type="hidden" name="product_cat_id" id="product_cat_id" />
                <input type="hidden" name="custom_mix_save_id" id="custom_mix_save_id" />
                <input type="hidden" name="rmc_grade_id" id="rmc_grade_id" />
                <input type="hidden" name="lat" id="product_lat" />
                <input type="hidden" name="long" id="product_long" />
                <input type="hidden" name="state_name" id="location_state_name" />
                <input type="hidden" name="city_name" id="location_city_name" />
                <input type="hidden" name="type" id="open_page_type" />
                <input type="hidden" name="state_id" id="location_state_id" />
                <input type="hidden" name="city_id" id="location_city_id" />
                <input type="hidden" name="selected_delivery_date" id="selected_delivery_date" />
                <input type="hidden" name="selected_end_delivery_date" id="selected_end_delivery_date" />
                <input type="hidden" name="delivery_qty" id="location_delivery_qty" />
                <input type="hidden" name="selected_range_diff" id="selected_range_diff" />

                <input Type="hidden" name="is_site_selected" value="0" id="is_location_site_selected" />
                <input Type="hidden" name="selected_site_address" value="" id="selected_site_address" />
                <input Type="hidden" name="type_of_rmc" value="" id="type_of_rmc" />


                <button type="submit" id="location_dialog_submit_button" style="display:none">Submit</button>

            </form>
            @if(!isset($profile))

                <div class="modal-body">
                    <div class="text-center">( Guest User )</div>
                    <h5 class="title mb-3">Choose Location </h5>
                    <label class="form-label">Enter city/locality to choose the supplier for delivery</label>
                    <div class="location_pop">
                        <div class="dicect_loca">
                            <div class="mb-3 search_detect">
                                <div class="search_area">
                                    <!-- <input type="text" class="form-control" placeholder="Search for area"> -->
                                    <input id="searchTextField" class="form-control" type="text" size="50" placeholder="Search city/locality" autocorrect="off" spellcheck="false" autocomplete="off" runat="server">
                                    <i class="fas fa-search" aria-hidden="true"></i>
                                    <span class="remove_serch" id="clear_location" style="display:none"><i class="fas fa-times"></i></span>
                                    <span class="error" id="location_error"></span>
                                </div>
                                <a href="javascript:void(0)" onclick="placeCurrentLocation()">Get Current Location</a>
                            </div>
                            <!-- <p>You have blocked tracking your location. To use this, change your location settings in browser.</p> -->
                            <div class="mb-3" id="">
                                <label class="form-label">Enter RMC Qty (Cu.Mtr)</label>
                                <input type="text" id="deliver_qty" class="form-control" placeholder="e.g. 50">
                                <span class="error" id="deliver_qty_error"></span>
                            </div>
                            <div class="mb-3" id="location_select_grade_div">
                                <label class="form-label">Select Grade Of Concrete<sup>*</sup></label>
                                <select id="location_dialog_grade_dropdown" class="form-select">
                                <option value="" disabled="true" selected>Select Concrete Grade</option>
                                @if(isset($concrete_grade_data))
                                    @foreach($concrete_grade_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                                <span class="error" id="location_popup_grade_error"></span>
                            </div>
                            <?php 
                                $rmc_info = "This is a standard RMC with default design mix parameters as available and set by RMC supplier. If you want to purchase your own design mix RMC then choose custom RMC option."
                            ?>      
                            <div class="mb-3" id="location_select_type_rmc_div">
                                <label class="form-label">Select Type Of RMC<sup>*</sup></label>
                                <div class="with_mixer_pump">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="rmc_type" value="product_rmc" id="product_rmc">
                                        <label class="form-check-label" for="product_rmc">
                                            RMC
                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{ $rmc_info }}"><i class="fas fa-info-circle"></i></a>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline me-0">
                                        <input class="form-check-input" type="radio" name="rmc_type" value="product_custom_rmc" id="product_custom_rmc">
                                        <label class="form-check-label" for="product_custom_rmc">
                                            Custom RMC
                                        </label>
                                    </div>
                                </div>
                                <span class="error" id="location_popup_type_rmc_error"></span>
                            </div>
                            <div class="mb-3" id="selected_delivery_date_div">
                                <label class="form-label">Choose Delivery Date</label>

                                <!-- <input type="date" class="form-control" placeholder="Delivery Date & Time"> -->
                                <!-- <input type="text" name="delivery_date" class="form-control CP_unavail_date" readonly="" placeholder="e.g. DD-MM-YYYY"> -->


                                <div class="range_picker m-r10">
                                    <input class="range_picker_input form-control" type="text" name="daterange" value="" />
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <span class="text-success d-block" id="delivery_date_msg">
                                    <i class="fa fa-info-circle"></i>You have to choose delivery date range of within 1 days
                                </span>
                                <p class="error" id="delivery_date_error"></p>

                            </div>
                        </div>
                        <button class="btn btn-primary btn-secondary mt-2" id="choose_location_popup_done_btn" type="button" onclick="submitLocation()">DONE</button>
                    </div>
                </div>

            @endif
            @if(isset($profile))
                <div class="modal-body">
                <!-- <div class="text-center">( Login User )</div> -->
                    <h5 class="title mb-3">Delivery Address </h5>
                    <div class="location_pop">
                        <div class="dicect_loca">
                            <div class="mb-3 search_addsite">
                                <label class="form-label">Select Delivery Address</label>
                                <select class="form-select" id="delivery_site_location_drop">
                                    <option value="" disabled="true" selected>Select Delivery Address</option>
                                    @if(isset($site_data["data"]))
                                        @foreach($site_data["data"] as $value)
                                            <option value="{{ $value['_id'] }}" data="{{ json_encode($value) }}">
                                                {{ $value["company_name"] }},
                                                {{ $value["site_name"] }},
                                                {{ $value["address_line1"] }}, {{ $value["address_line2"] }}, {{ $value["city_name"] }} - {{ $value["pincode"] }}, {{ $value["state_name"] }}, ({{ $value["country_name"] }})
                                            </option>
                                        @endforeach
                                    @endif

                                </select>
                                <span class="error" id="location_error"></span>
                                <p class="d-flex justify-content-between">
                                    <a href="javascript:void(0)" onclick="placeCurrentLocation()">Get Current Location</a>
                                    <a href="javascript:void(0)" onclick="resetForm('add_site_form','add_site_address_popup')" data-bs-toggle="modal" data-bs-target="#add_site_address_popup">+ Add Delivery Address</a>
                                    
                                </p>
                                <span id="searchTextField"></span>
                                
                            </div>
                            <div class="mb-3" id="">
                                <label class="form-label">Enter RMC Qty (Cu.Mtr)</label>
                                <input type="text" name="" id="deliver_qty" class="form-control" placeholder="e.g. 50" value="">
                                <span class="error" id="deliver_qty_error"></span>
                            </div>
                            <div class="mb-3" id="location_select_grade_div">
                                <label class="form-label">Select Grade Of Concrete<sup>*</sup></label>
                                <select id="location_dialog_grade_dropdown" class="form-select">
                                <option value="" disabled="true" selected>Select Concrete Grade</option>
                                @if(isset($concrete_grade_data))
                                    @foreach($concrete_grade_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                                <span class="error" id="location_popup_grade_error"></span>
                            </div>
                            <?php 
                                $rmc_info = "This is a standard RMC with default design mix parameters as available and set by RMC supplier. If you want to purchase your own design mix RMC then choose custom RMC option."
                            ?>      
                            <div class="mb-3" id="location_select_type_rmc_div">
                                <label class="form-label">Select Type Of RMC<sup>*</sup></label>
                                <div class="with_mixer_pump">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="rmc_type" value="product_rmc" id="product_rmc">
                                        <label class="form-check-label" for="product_rmc">
                                            RMC
                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{ $rmc_info }}"><i class="fas fa-info-circle"></i></a>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline me-0">
                                        <input class="form-check-input" type="radio" name="rmc_type" value="product_custom_rmc" id="product_custom_rmc">
                                        <label class="form-check-label" for="product_custom_rmc">
                                            Custom RMC
                                        </label>
                                    </div>
                                </div>
                                <span class="error" id="location_popup_type_rmc_error"></span>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Choose Delivery Date</label>

                                 <!-- <input type="text" name="delivery_date" class="form-control CP_unavail_date" readonly="" placeholder="e.g. DD-MM-YYYY"> -->

                                <div class="range_picker m-r10">
                                    <input class="range_picker_input form-control" type="text" name="daterange" value="" />
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <span class="text-success d-block" id="delivery_date_msg">
                                    <i class="fa fa-info-circle"></i>You have to choose delivery date range of within 1 days</span>
                                <p class="error" id="delivery_date_error"></p>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-secondary mt-2" type="button" onclick="submitLocation()">DONE</button>
                    </div>

                    @if(isset($all_site_arr[Session::get("selected_site_address")]))

                        <script>

                            $(document).ready(function(){

                                $('#delivery_site_location_drop').val('{{Session::get("selected_site_address")}}');

                                selectSiteLocation($('option:selected', "#delivery_site_location_drop").attr("data"));

                            });


                        </script>

                @endif
                </div>
            @endif



        </div>
    </div>
</div>
<?php
$selected_delivery_date = "";
$selected_delivery_end_date = "";
$selected_delivery_qty = "";
$selected_type_of_rmc = "";
$selected_product_cat_id = "";
$selected_rmc_grade_id = "";
if(Session::get("selected_display_delivery_date") != null){
    $selected_delivery_date = Session::get("selected_display_delivery_date");
}

if(Session::get("selected_end_display_delivery_date") != null){
    $selected_delivery_end_date = Session::get("selected_end_display_delivery_date");
}

if(Session::get("selected_type_of_rmc") != null){
    $selected_type_of_rmc = Session::get("selected_type_of_rmc");
}

if(Session::get("selected_product_cat_id") != null){
    $selected_product_cat_id = Session::get("selected_product_cat_id");
}

if(Session::get("selected_rmc_grade_id") != null){
    $selected_rmc_grade_id = Session::get("selected_rmc_grade_id");
}

if(Session::get("selected_delivery_qty") != null){
    $selected_delivery_qty = Session::get("selected_delivery_qty");
}else{
    // $selected_delivery_qty = 3;
    $selected_delivery_qty = '';
}

?>
<input type="hidden" id="popup_selected_delivery_qty" value="{{ $selected_delivery_qty }}" />
<input type="hidden" id="popup_selected_delivery_date" value="{{ $selected_delivery_date }}" />
<input type="hidden" id="popup_selected_delivery_end_date" value="{{ $selected_delivery_end_date }}" />
<input type="hidden" id="popup_selected_type_of_rmc" value="{{ $selected_type_of_rmc }}" />
<input type="hidden" id="popup_selected_product_cat_id" value="{{ $selected_product_cat_id }}" />
<input type="hidden" id="popup_selected_rmc_grade_id" value="{{ $selected_rmc_grade_id }}" />
<script>
// $(document).ready(function(){
//     $("#deliver_qty").val('{{ $selected_delivery_qty }}');
//     $("#location_delivery_qty").val('{{ $selected_delivery_qty }}');

// 	$('.CP_unavail_date').datepicker({
//         autoclose: true,
//         startDate: '1d',
//         format: 'dd M yyyy'
//         // format: 'yyyy-mm-dd'
// 	});

//     $(".CP_unavail_date").on('change', function(event) {
//         event.preventDefault();
//         // alert(this.value);
//         $("#selected_delivery_date").val(this.value);
//         $("#delivery_date_error").html("");
//         /* Act on the event */
//     });

//     $('.CP_unavail_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

//     var selected_delivery_date = '{{ $selected_delivery_date }}';
//     var selected_delivery_end_date = '{{ $selected_delivery_end_date }}';

//     var today = new Date();
//     var today_to_30 = new Date();

//     today.setDate(today.getDate() + 2);
//     var dd = String(today.getDate()).padStart(2, '0');
//     var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
//     var yyyy = today.getFullYear();

//     today = dd + '/' + mm + '/' + yyyy;
//     today_request = yyyy + '/' + mm + '/'+dd;
//     // console.log("today..."+today);

//     today_to_30.setDate(today_to_30.getDate() + 32);
//     var dd_30 = String(today_to_30.getDate()).padStart(2, '0');
//     var mm_30 = String(today_to_30.getMonth() + 1).padStart(2, '0'); //January is 0!
//     var yyyy_30 = today_to_30.getFullYear();

//     today_to_30 = dd_30 + '/' + mm_30 + '/' + yyyy_30;
//     today_to_30_request =  yyyy_30 + '/' + mm_30 + '/' + dd_30;

//     var end_date = today;
//     $("#selected_delivery_date").val(today_request);

//         if(selected_delivery_date != ''){
//             var selected_start_date = new Date(selected_delivery_date);
//             var dd = String(selected_start_date.getDate()).padStart(2, '0');
//             var mm = String(selected_start_date.getMonth() + 1).padStart(2, '0'); //January is 0!
//             var yyyy = selected_start_date.getFullYear();

//             $("#selected_delivery_date").val(''+selected_delivery_date);

//             selected_delivery_date = dd + '/' + mm + '/' + yyyy;

//             today = selected_delivery_date;
//         }


//     $("#selected_end_delivery_date").val(today_to_30_request);


//         if(selected_delivery_end_date != ''){
//             var selected_end_date = new Date(selected_delivery_end_date);
//             var dd = String(selected_end_date.getDate()).padStart(2, '0');
//             var mm = String(selected_end_date.getMonth() + 1).padStart(2, '0'); //January is 0!
//             var yyyy = selected_end_date.getFullYear();

//             $("#selected_end_delivery_date").val(''+selected_delivery_end_date);

//             selected_delivery_end_date = dd + '/' + mm + '/' + yyyy;

//             end_date = selected_delivery_end_date;

//         }

//         console.log("today...."+today);
//         console.log("end_date...."+end_date);

//     $('input[name="daterange"]').daterangepicker({
//         startDate: today,
//         endDate: end_date,
//         minDate: today,
//         maxDate: today_to_30,
//         locale: {
//             format: 'DD/MM/YYYY'
//         }

//     });

//     // $('input[name="daterange"]').on('change.datepicker', function(ev){
//     //     var picker = $(ev.target).data('daterangepicker');
//     //     console.log(picker.startDate); // contains the selected start date
//     //     console.log(picker.endDate); // contains the selected end date

//     //     // ... here you can compare the dates and call your callback.
//     // });

//     $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {

//         // var start_date = picker.startDate.format('DD/MM/YYYY');
//         var start_date = picker.startDate.format('YYYY/MM/DD');
//         var end_date = picker.endDate.format('YYYY/MM/DD');

//         $("#selected_delivery_date").val(start_date);
//         $("#selected_end_delivery_date").val(end_date);
//         $("#delivery_date_error").html("");

//         console.log(start_date);
//         console.log(end_date);

//         var diff = picker.endDate.diff(picker.startDate, 'days');

//         $("#selected_range_diff").val(diff);

//         console.log(diff);

//         checkDeliveryQtyDays();

//     });
// });
    function checkDeliveryQtyDays(){

        let deliver_qty = $("#deliver_qty").val();
        let selected_range_diff = $("#selected_range_diff").val();

        var is_error = 0;

        if(deliver_qty.length > 0){
            if(!isNaN(deliver_qty)){
                if((deliver_qty >= 3) && (deliver_qty <= 100)){


                    var total_days = (parseInt(deliver_qty) / 3);
                    total_days = Math.floor(total_days);

                    if(selected_range_diff > total_days ){
                        $("#delivery_date_msg").html("");
                        // $("#delivery_date_error").html("You have to choose delivery date range of within "+total_days+" days");
                        $("#delivery_date_error").html("You can choose delivery date range of only "+total_days+" day(s) for the entered RMC Qty of "+deliver_qty+" Cum");
                        $("#choose_location_popup_done_btn").attr('disabled','disabled');
                        is_error = 1;
                    }else{
                        $("#delivery_date_msg").html("<i class='fa fa-info-circle'></i>You have to choose delivery date range of within "+total_days+" days");
                        $("#delivery_date_error").html("");
                        $("#choose_location_popup_done_btn").removeAttr('disabled');
                    }


                }
            }
        }

        return is_error;

    }

</script>
<!-- select location popup end -->

<!-- Login Modal start -->
<div class="modal fade" id="nearest-vendors" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Available grade of concrete in the selected location</h5>
                <?php

                    if(session('nearest_product') != null){
                        $nearest_product = session('nearest_product');
                    }

                ?>
                @if(isset($nearest_product) && count($nearest_product) > 0)
                    @foreach($nearest_product as $value)
                    <div class="" style="width: 30%;margin-right: 20px;float: left;display: inline-block;">
                        <div class="item">
                            @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null) && (Session::get("selected_display_delivery_date") != null))
                                <a href="{{ route('buyer_product_listing',$value['_id']) }}" class="home_our_product_bx mb-4">
                                    <div class="home_our_product_bx_img">
                                        <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                    </div>
                                    <p>{{$value["name"]}}</p>
                                </a>

                            @else

                                <a href="javascript:void(0)" onclick="openLocationDialog('{{ $value['_id'] }}','product_listing')" class="home_our_product_bx mb-4">
                                    <div class="home_our_product_bx_img">
                                        <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                    </div>
                                    <p>{{$value["name"]}}</p>
                                </a>
                            @endif
                        </div>
                    </div>
                    @endforeach
                @else
                    <div style="text-align: center;">
                        <p> No RMC supplier found in selected region. </p>
                        <p><a href="javascript:void(0)" onclick="openLocationDialog('','change_location')" style="color:#136e9d">Change Location</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- Login Modal end -->

<?php

if(session('is_show_nearest_vendor_popup') != null){
    $is_show_nearest_vendor_popup = session('is_show_nearest_vendor_popup');
}

?>

<script>
// $(document).ready(function() {

    @if(isset($is_show_nearest_vendor_popup) && $is_show_nearest_vendor_popup == 'yes')
        $("#nearest-vendors").modal('show');
    @endif
// });

</script>
<!-- Login Modal start -->
<div class="modal fade" id="Login-form" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Log In</h5>
                <p class="error" id="login_error"></p>
                <form name="login-form" id="log-form">
                @csrf
                    <div class="login_fild">
                        <div class="login_fild_inner">
                            <div class="mb-3">
                                <label for="MobileNo" class="form-label">Mobile No. / Email Id</label>
                                <input type="text" id="mobile_or_email_buyer_signup" name="email_mobile" class="form-control" id="MobileNo" placeholder="e.g. 9898989898 / johndoe@example.com">
                            </div>
                            <div class="mb-3">
                                <label for="Password_login" class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" id="Password_login" placeholder="e.g. Enter Password">
                            </div>
                        </div>
                        <input type="hidden" id="login_open_from" value="normal" />
                        <input type="hidden" id="add_to_cart_button_id" value="" />
                        <div class="d-grid gap-2 text-center">
                            <button name="login_btn" value="login_btn" class="btn btn-primary mt-2" type="submit">LOG IN</button>

                            <a data-bs-toggle="modal" onclick="resetForm('forgot_pass_form','forgot_pass_pop')" href="#forgot_pass_pop" role="button" data-bs-dismiss="modal">Forgot Password?</a>
                        </div>
                    </div>
                    <p class="footer_line_link">Don't have an account ? <a href="javascript:;" data-bs-dismiss="modal" aria-label="Close" id="show_singup_m">Register</a></p>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Login Modal end -->

<!-- sinup Modal start -->
<div class="modal fade" id="sign-up-form-1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Registration</h5>
                <span class="error" id="sign-up-form-1_error"></span>
                <form name="signup-emailverify-form" id="signup-emailverify-form" style="display: block;">
                @csrf
                    <div class="login_fild">
                        <div class="login_fild_inner">
                            <div class="mb-3">
                                <label for="mobile_or_email" class="form-label">Mobile No.</label>
                                <input type="text" id="mobile_or_email" name="email_mobile" class="form-control" placeholder="e.g. 9898989898">
                            </div>
                        </div>
                        <div class="d-grid">
                            <button class="btn btn-primary mt-2" name="register_btn" value="register_btn" type="submit" aria-label="Close" >REGISTER</button>
                        </div>
                    </div>
                    <p class="footer_line_link">Already have an account ? <a href="#" data-bs-dismiss="modal" aria-label="Close" id="show_login_m">Log In</a></p>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- sinup Modal end -->

<!-- otp Modal start -->
<div class="modal fade" id="sign-up-form-2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Registration</h5>
                <p class="error" id="sign-up-form-2_error"></p>
                <form name="signup-emailverify-otp-form" id="signup-emailverify-otp-form" style="display: block;" autocomplete="off">
                @csrf
                    <div class="login_fild otp_screen">
                        <div class="login_fild_inner">
                            <h6>OTP Confirmation Code</h6>
                            <p id="oyp_ode_confirm_message"></p>
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#sign-up-form-1" data-bs-dismiss="modal">Change Mobile No.</a>
                            <div class="timer_otp mb-3">
                                <span id="timer">
                                    <span id="time">02:60</span>
                                </span>
                            </div>
                        </div>
                        <input type="hidden" name="email_mobile" id="verify_email_mobile" />
                        <input type="hidden" id="verify_sign_up_type" name="signup_type" />
                        <div class="otp_textbox flex-row d-flex mb-4 justify-content-evenly">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_1" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_2" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_3" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_4" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_5" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_6" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                        </div>

                    <!-- <div id="SMSArea" class="row SMSArea">
                        <div class="col-2">
                            <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="smsCode text-center rounded-lg" />
                        </div>
                        <div class="col-2">
                            <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="smsCode text-center rounded-lg" />
                        </div>
                        <div class="col-2">
                            <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="smsCode text-center rounded-lg" />
                        </div>
                        <div class="col-2">
                            <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="smsCode text-center rounded-lg" />
                        </div>
                        <div class="col-2">
                            <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="smsCode text-center rounded-lg" />
                        </div>
                        <div class="col-2">
                            <input type="text" maxlength="1" size="1" min="0" max="9" pattern="[0-9]{1}" class="smsCode text-center rounded-lg" />
                        </div>
                    </div> -->

                        <span id="OTP_error_div"></span>

                        <div class="d-grid">

                            <!-- <button class="btn btn-primary mt-2" name="otp_btn" value="otp_btn" type="submit">Continue</button> -->
                            <button class="btn btn-primary btn-success mt-2" name="otp_btn" value="otp_btn" type="submit">VERIFY</button>
                        </div>
                    </div>

                </form>

                <form name="signup-emailverify-form-resend" id="signup-emailverify-form" style="display: block;">
                    @csrf
                        <!-- <input type="hidden" name="send_otp_type" value="resend" /> -->
                        <input type="hidden" name="email_mobile" id="resend_email_mobile" />
                        <input type="hidden" name="register_btn" value="register_btn">
                </form>

            </div>
        </div>
    </div>
</div>
<!-- otp Modal end -->

<!-- otp Modal start -->
<div class="modal fade" id="sign-up-form-3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Registration</h5>
                <form name="signup-form" id="signup-form" style="display: block;">
                @csrf
                    <div class="login_fild">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="" class="form-label">Select Account Type <sup>*</sup></label>
                                    <div class="custome_select">
                                        <select name="account_type" id="register_account_type" class="form-control form-select">
                                            <option value="">Select Account Type</option>
                                            <option value="Builder">Builder</option>
                                            <option value="Contractor">Contractor</option>
                                            <option value="Individual">Individual</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="" class="form-label">Company Type <sup>*</sup></label>
                                    <div class="custome_select">
                                        <select name="company_type" id="register_company_type" class="form-control form-select">
                                            <option value="">Select Company Type</option>
                                            <option value="Partnership">Partnership</option>
                                            <option value="Proprietor">Proprietor</option>
                                            <option value="One Person Company">One Person Company</option>
                                            <option value="LLP.">LLP</option>
                                            <option value="LTD.">LTD.</option>
                                            <option value="PVT LTD">PVT LTD</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="" class="form-label">Full Company Name <sup>*</sup></label>
                                    <input type="text" name="company_name" class="form-control text-capitalize" id="" placeholder="e.g. SCC Infrastructure Pvt. Ltd.">
                                </div>
                                <div class="mb-3">
                                    <label for="" class="form-label">Owner Full Name <sup>*</sup></label>
                                    <input type="text" name="full_name" class="form-control text-capitalize" id="" placeholder="e.g. John">
                                </div>
                                <div class="mb-3">
                                    <label for="" class="form-label">Email <sup>*</sup></label>
                                    <input type="text" name="email" id="register_email" class="form-control" id="" placeholder="e.g. johndoe@example.com">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label for="" class="form-label">Mobile No.</label>
                                    <input type="text" class="form-control" name="mobile_number" id="signup_phone" placeholder="e.g. 9898989898">
                                </div>
                                <div class="mb-3">
                                    <label for="" class="form-label">Password <sup>*</sup></label>
                                    <div class="text_right_icon">
                                        <input type="password" name="password" id="sign_up_password" class="form-control" id="" placeholder="e.g. Enter Password">
                                        <span class="text_icon">
                                            <i class="fa fa-eye" id="pass_eye_on" aria-hidden="true"></i>
                                            <i class="fa fa-eye-slash" id="pass_eye_off" style="display:none;" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- <div class="mb-3">
                                    <label for="" class="form-label">GST No</label>
                                    <input type="text" name="gst_number" class="form-control" id="" placeholder="e.g. 00AABBC0000A1BB">
                                </div> -->
                                <div class="mb-3">
                                    <label for="" class="form-label">PAN No</label>
                                    <input type="text" name="pan_number" class="form-control text-uppercase" id="" placeholder="e.g. AAABB0000C">
                                </div>
                                <!-- <div class="mb-3">
                                    <label for="" class="form-label">Payment Method <sup>*</sup></label>
                                    <div class="custome_select">
                                        <select name="payment_method" class="form-control form-select">
                                            <option value="" readOnly>Select Payment method</option>
                                            @if(isset($payment_method["data"]))
                                                @foreach($payment_method["data"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="form-check" id="terms_condition_div">
                            <input class="form-check-input" name="terms_condition" type="checkbox" id="terms_condition">
                            <label class="form-check-label"> I've read and accept the
                                <a href="{{route('buyer_terms_of_use')}}" target="_blank" class="color_orange">Terms of Usage</a>, <a href="{{route('buyer_privacy_policy')}}" target="_blank" class="color_orange">Privacy Policy</a> and <a href="{{route('buyer_return_policy')}}" target="_blank" class="color_orange">Cancellations, Returns, and Refund Policy</a> 
                            </label>
                        </div>

                        <div class="d-grid">
                            <input type="hidden" name="signup_type" id="signup_form_type" />
                            <button class="btn btn-primary mt-3" type="submit" name="register_btn" value="register_btn">REGISTER</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- otp Modal end -->

        <div class="modal fade" id="forgot_pass_pop" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
		    <div class="modal-dialog modal-dialog400">
		    	<div class="modal-content">
		            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		            <div class="modal-body">
		            	<h5 class="title mb-4">Forgot Password</h5>
                        <p class="error" id="forgot_pass_otp_send_error"></p>
                        <form name="forgot_pass_form" id="" style="display: block;">
                            @csrf
                            <div class="login_fild">
                                <div class="login_fild_inner">
                                    <div class="mb-3">
                                        <label for="MobileNo" class="form-label">Mobile No.</label>
                                        <input type="text" class="form-control mobile_validate" name="email_mobile" placeholder="e.g. 9898989898">
                                    </div>
                                </div>
                                <div class="d-grid">
                                    <button type="submit" name="otp_send_btn" value="otp_send_btn" class="btn btn-primary mt-2">Send OTP</button>
                                    <!-- <button class="btn btn-primary mt-2" data-bs-target="#forgot_otp_popup" data-bs-toggle="modal" data-bs-dismiss="modal">SEND</button> -->
                                </div>
                            </div>
                        </form>
		            </div>
		        </div>
		    </div>
		</div>
		<div class="modal fade" id="forgot_otp_popup" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
			<div class="modal-dialog modal-dialog400">
		        <div class="modal-content">
		            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <div class="modal-body">
                            <h5 class="title mb-4">Forgot Password</h5>
                            <p class="error" id="password-form-1_error"></p>
                            <form name="forgot_otp_form" id="forgot-otp-form">
                            @csrf
                                <div class="login_fild otp_screen">
                                    <div class="login_fild_inner">
                                        <h6>OTP Confirmation Code</h6>
                                        <p id="forgot_code_confirm_message">We will send a link on your registered email or One Time OTP on your mobile to reset your password.
                                        <a data-bs-toggle="modal" href="#forgot_pass_pop" role="button" data-bs-dismiss="modal">Change Mobile No.</a></p>
                                        <div class="timer_otp mb-3">
                                            <span id="timer_forgot_pass">
                                                00:<span id="time_forgot_pass">60</span>
                                            </span>
                                        </div>
                                    </div>
                                    <input type="hidden" name="email_mobile" id="forgot_verify_email_mobile" />
                                    <div class="otp_textbox flex-row d-flex mb-4 justify-content-evenly">
                                        <!-- <input type="text" class="form-control" name="otp_code_1">
                                        <input type="text" class="form-control" name="otp_code_2">
                                        <input type="text" class="form-control" name="otp_code_3">
                                        <input type="text" class="form-control" name="otp_code_4">
                                        <input type="text" class="form-control" name="otp_code_5">
                                        <input type="text" class="form-control" name="otp_code_6"> -->

                                        <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_1" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                                        <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_2" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                                        <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_3" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                                        <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_4" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                                        <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_5" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                                        <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_6" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                                    </div>
                                    <span id="OTP_error_div_forgot"></span>
                                    <div class="d-grid">
                                        <!-- <button class="btn btn-primary btn-success mt-2" data-bs-dismiss="modal">VERIYFY</button> -->
                                        <button class="btn btn-primary btn-success mt-2" name="otp_btn" value="otp_btn" type="submit">VERIFY</button>
                                    </div>
                                </div>
                            </form>

                            <form name="forgotpass-emailverify-form-resend" id="forgotpass-emailverify-form-resend" style="display: block;">
                                @csrf
                                    <!-- <input type="hidden" name="send_otp_type" value="resend" /> -->
                                    <input type="hidden" name="email_mobile" id="forgot_resend_email_mobile" />
                                    <input type="hidden" name="otp_send_btn" value="otp_send_btn">
                            </form>
                        </div>

		        </div>
		    </div>
		</div>
        <div class="modal fade" id="forgot_pass_pop_1" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
		    <div class="modal-dialog modal-dialog400">
		    	<div class="modal-content">
		            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		            <div class="modal-body">
		            	<h5 class="title mb-4">Forgot Password</h5>
                        <p class="error" id="forgot_pass_pop_1_error"></p>
                        <form name="forgot_pass_form_1" id="" style="display: block;">
                            @csrf
                            <div class="login_fild">
                                <div class="login_fild_inner">
                                    <div class="mb-3">
                                        <label class="form-label">New Password</label>
                                        <input type="password" id="new_password" class="form-control" name="new_pass" placeholder="e.g. 123456">
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Confirm Password</label>
                                        <input type="password" class="form-control" id="new_password" name="confirm_pass" placeholder="e.g. 123456">
                                    </div>
                                </div>
                                <div class="d-grid">
                                    <button type="submit" name="otp_send_btn" value="otp_send_btn" class="btn btn-primary mt-2">Reset</button>
                                    <!-- <button class="btn btn-primary mt-2" data-bs-target="#forgot_otp_popup" data-bs-toggle="modal" data-bs-dismiss="modal">SEND</button> -->
                                </div>
                            </div>
                        </form>
		            </div>
		        </div>
		    </div>
		</div>


<!-- Add site popup start -->
<div class="modal fade" id="add_site_address_popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-3"> Add Delivery Address</h5>
                <form name="add_site_form" class="cust_design_form" method="post" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label class="form-label">Full Company Name <sup>*</sup></label>
                                    <input type="text" class="form-control text-capitalize" name="company_name" id="company_name"  placeholder="e.g. SCC Infrastructure Pvt. Ltd.">
                                </div>
                                <div class="col-md-12 mb-3" id="site_name_div">
                                    <label class="form-label">Site Name ( Real Estate Scheme/Infrastructure ) <sup>*</sup></label>
                                    <div class="text_right_icon">
                                        <input type="text" class="form-control text-capitalize" name="site_name" id="site_name"  placeholder="e.g. Vastral">
                                        <span class="text_icon" id="real_time_check_loader" style="display:none"><i class="fas fa-circle-notch fa-spin"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="form-label">Contact Person Full Name ( At Site ) <sup>*</sup></label>
                                    <input type="text" class="form-control text-capitalize" name="person_name" id="person_name"  placeholder="e.g. Johndoe">
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="form-label">Email <sup>*</sup></label>
                                    <input type="text" class="form-control" name="email" id="email"  placeholder="e.g. johndoe@example.com">
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label class="form-label">Mobile No. <sup>*</sup></label>
                                    <input type="text" class="form-control mobile_validate" name="mobile_number" id="phone1"  placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <div class="mb-1" role="alert"><b>Note :</b>  Enter your delivery address below.</div>
                                        <div class="current_location">
                                            <div class="search_area">
                                                <input type="text" placeholder="Enter Your delivery address" required="" name="us2_address" id="us2_address" class="form-control pac-target-input" onkeydown="return event.key != 'Enter';" autocomplete="off">
                                                <i class="fas fa-search" aria-hidden="true"></i>
                                                <span class="remove_serch" id="clear_address_location"><i class="fas fa-times"></i></span>
                                            </div>
                                            <a href="javascript:void(0)" class="current_location_btn" onclick="getCurrentLocation()">Get Current Location</a>
                                        </div>
                                        <span class="error" id="address_location_error"></span>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="us2_lat" id="us2_lat" class="form-control" placeholder="Latitude" readonly/>
                                                        <p class="error" id="us2_lat_error"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" name="us2_lon" id="us2_lon" class="form-control" placeholder="Longitude" readonly/>
                                                        <p class="error" id="us2_lon_error"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="alert alert-success mb-3 text-center" role="alert">Please set precise delivery address</div>
                                        <div class="map-content-block add-vehicle-map-block m-t20" style="height: 240px; margin-top: 0px;">
                                            <div id="us2" style="width: 100%; height: 230px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="mt-0"/>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="form-label">Enter Building/Society/Locality <sup>*</sup></label>
                            <input type="text" class="form-control" name="address_line1" id="address_line1"  placeholder="e.g. Building Name">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="form-label">Landmark <sup>*</sup></label>
                            <input type="text" class="form-control" name="address_line2" id="address_line2"  placeholder="e.g. Landmark">
                        </div>
                        <div class="col-md-6 col-lg-3 mb-3" >
                            <span id="add_address_country_id_div">
                                <label class="form-label">Country ( Autofill )</label>
                                <!-- <select class="form-select">
                                    <option>Gujarat</option>
                                </select> -->
                                <select id="add_site_country" readOnly name="country_id" class="form-control" data-placeholder="Select Country">


                                </select>
                                <!-- <input type="text" id="add_site_country" readOnly name="country_id" class="form-control" data-placeholder="Country" /> -->
                            </span>
                        </div>
                        <div class="col-md-6 col-lg-3 mb-3">
                            <span id="add_address_state_id_div">
                                <label class="form-label">State ( Autofill )</label>
                                <!-- <select class="form-select">
                                    <option>Gujarat</option>
                                </select> -->
                                <select id="add_site_state" name="state_id" class="form-control" data-placeholder="Select State">


                                </select>
                                <!-- <input id="add_site_state" readOnly name="state_id" class="form-control" data-placeholder="State"/> -->
                            </span>
                        </div>
                        <div class="col-md-6 col-lg-3 mb-3">
                            <span id="add_address_city_id_div">
                                <label class="form-label">City ( Autofill )</label>
                                <!-- <input type="text" class="form-control" name=""  placeholder="City"> -->
                                <select id="add_site_city" name="city_id" class="form-control" data-placeholder="Select City">


                                </select>
                                <!-- <input id="add_site_city" readOnly name="city_id" class="form-control" data-placeholder="City"/> -->
                            </span>
                        </div>
                        <div class="col-md-6 col-lg-3 mb-3">
                            <label class="form-label">Pincode <sup>*</sup></label>
                            <input type="text" class="form-control" name="pincode" id="pincode"  placeholder="e.g. 123456">
                        </div>
                        <div class="col-md-6 col-lg-6 mb-3">
                            <span id="billing_address_state_id_div">
                                <label class="form-label">Billing Address</label>

                                <select name="billing_address_id" id="billing_address_id" class="form-select" data-placeholder="Select Billing Address">
                                    <option value="">Select Billing Address</option>
                                    @if(isset($billing_address["data"]))
                                        @foreach($billing_address["data"] as $value)
                                        <option value="{{ $value['_id'] }}">{{ $value["company_name"] }}, {{ $value["line1"] }}, {{ isset($value["line2"]) ? $value["line2"] : '' }}, {{ $value["stateDetails"]["state_name"] }}, {{ $value["cityDetails"]["city_name"] }}</option>
                                        @endforeach
                                    @endif
                                </select>


                            </span>
                        </div>

                        <input type="hidden" name="lattitude" id="site_lat" />
                        <input type="hidden" name="longitude" id="site_long" />
                        <input type="hidden" name="site_id" id="site_id" />
                    </div>
                    <input type="hidden" name="add_site_type" id="add_site_type" value="add" />
                    <button class="btn btn-primary btn-secondary mt-2" type="submit">DONE</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add site popup end -->

<!-- Add site popup start -->
<div class="modal fade" id="add_billing_address_popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-3 modal-title" id="add_billing_address_form_title"> Add Billing Address</h5>
                <form name="add_billing_address_form" class="cust_design_form" method="post" autocomplete="off">
                    @csrf
                    <div class="row">
                    @php
                        $profile = session('buyer_profile_details', null);
                        
                    @endphp
                        @if(isset($profile["account_type"]) && $profile["account_type"] == "Individual")
                            <div class="col-md-12 mb-3">
                                <label class="form-label">Full Name <sup>*</sup></label>
                                <input type="text" class="form-control text-capitalize" name="full_name" id="billing_full_name"  placeholder="e.g. Your Full Name">
                            </div>
                        @else
                            <div class="col-md-12 mb-3">
                                <label class="form-label">Full Company Name <sup>*</sup></label>
                                <input type="text" class="form-control text-capitalize" name="company_name" id="billing_company_name"  placeholder="e.g. SCC Infrastructure Pvt. Ltd.">
                            </div>
                        @endif
                        
                        <div class="col-md-12 mb-3">
                            <label class="form-label">Registered Address Line 1 <sup>*</sup></label>
                            <input type="text" class="form-control text-capitalize" name="address_line1" id="billing_address_line1"  placeholder="e.g. Registered Address Line 1">
                        </div>
                        <div class="col-md-12 mb-3">
                            <label class="form-label">Registered Address Line 2 <sup>*</sup></label>
                            <input type="text" class="form-control text-capitalize" name="address_line2" id="billing_address_line2"  placeholder="e.g. Registered Address Line 1">
                        </div>

                        <div class="col-md-6 col-lg-6 mb-3">
                            <span id="billing_address_state_id_div">
                                <label class="form-label">State</label>
                                <!-- <select class="form-select">
                                    <option>Gujarat</option>
                                </select> -->
                                <!-- <select id="add_site_state" name="state_id" class="form-control" data-placeholder="Select State">


                                </select> -->

                                <select id="state_name_city_address" name="state_id" class="form-select" data-placeholder="Select State Name">
                                    <option value="">Select State</option>
                                    @if(isset($state_data["data"]))
                                    @foreach($state_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                    @endforeach
                                    @endif
                                </select>

                                <!-- <input id="add_site_state" readOnly name="state_id" class="form-control" data-placeholder="State"/> -->
                            </span>
                        </div>
                        <div class="col-md-6 col-lg-6 mb-3">
                            <span id="billing_address_city_id_div">
                                <label class="form-label">City</label>
                                <!-- <input type="text" class="form-control" name=""  placeholder="City"> -->
                                <!-- <select id="add_site_city" name="city_id" class="form-control" data-placeholder="Select City">


                                </select> -->

                                <select id="state_name_city_filter" name="city_id" class="form-select" data-placeholder="Select city name">
                                    <option value="">Select City</option>
                                    @if(isset($city_data["data"]))
                                    @foreach($city_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["city_name"] }}</option>
                                    @endforeach
                                    @endif
                                </select>

                                <!-- <input id="add_site_city" readOnly name="city_id" class="form-control" data-placeholder="City"/> -->
                            </span>
                        </div>
                        <div class="col-md-6 col-lg-6 mb-3">
                            <label class="form-label">Pincode <sup>*</sup></label>
                            <input type="text" class="form-control" name="pincode" id="billing_pincode"  placeholder="e.g. 123456">
                        </div>
                        <div class="col-md-6 col-lg-6 mb-3">
                            <label class="form-label">GST Number <sup>*</sup></label>
                            <input type="text" class="form-control text-uppercase" name="gst_number" id="gst_number"  placeholder="e.g. 00AABBC0000A1BB">
                        </div>


                        <input type="hidden" name="billing_address_id" id="billing_address_form_id" />
                    </div>
                    <input type="hidden" name="billing_address_form_type" id="billing_address_form_type" value="add" />
                    <button class="btn btn-primary btn-secondary mt-2" type="submit">DONE</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add site popup end -->

<!-- Confirmation Popup MESSAGE START -->
<!-- <div id="confirmModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="confirmMessage">
        Are you sure you want to accept delete?
      </div>
      <div class="modal-footer text-center">
          <button type="button" data-dismiss="modal" class="site-button green m-r15" id="confirmOk">Yes</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="confirmCancel">No</button>
      </div>
    </div>
  </div>
</div> -->

<div class="modal fade" id="site_change_Modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <div class="delet_confor_pop_inner">
                    <p class="text-center mt-2" id="">Your chosen delivery address and material source is from different states or city. On choosing this you will be redirected to home screen as prices may differ. Do you want to continue?</p>
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-primary mt-2 mx-3" type="button" data-bs-dismiss="modal" id="change_site_yes">Yes</button>
                        <button class="btn btn-primary btn-secondary mt-2 mx-3" type="button" data-bs-dismiss="modal" id="change_site_no">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <div class="delet_confor_pop_inner">
                    <p class="text-center mt-2" id="confirmMessage">Are you sure you want to delete?</p>
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-primary mt-2 mx-3" type="button" data-bs-dismiss="modal" id="confirmOk">Yes</button>
                        <button class="btn btn-primary btn-secondary mt-2 mx-3" type="button" data-bs-dismiss="modal" id="confirmCancel">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Confirmation Popup MESSAGE END -->


<div class="modal fade" id="TM_confirmModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <div class="delet_confor_pop_inner">
                    <p class="text-center mt-2" id="confirmTMMessage">Are you sure you want to purchase RMC without Transit Mixer</p>
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-primary mt-2 mx-3" type="button" data-bs-dismiss="modal" id="confirmTMOk">Yes</button>
                        <button class="btn btn-primary btn-secondary mt-2 mx-3" type="button" data-bs-dismiss="modal" id="confirmTMCancel">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cust_design_mix">
    @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null))
        <!-- <a href="javascript:void(0)" class="cust_design_mix_btn btn btn-primary" onclick="openCustomMixDialog(0)"> Custom Design Mix</a> -->
    @else
        <!-- <a href="javascript:void(0)" class="cust_design_mix_btn btn btn-primary" onclick="openLocationDialog('','change_location')"> Custom Design Mix</a> -->
    @endif
    <!-- custome mix start -->
    <div class="modal fade" id="cust_design_pop" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body">
                @php
                    $profile = session('buyer_profile_details', null);
                @endphp
                @if(!isset($profile))
                    <div class="text-center">( Guest User )</div>
                @endif
                    <h5 class="title ">Custom Design Mix</h5>
                    <label class="mb-2"><i class="fas fa-map-marker-alt" ></i>
                    @if((Session::get("is_site_selected") != null) && (Session::get("is_site_selected") == '1' ) )

                        <?php
                            if(isset($all_site_arr[Session::get("selected_site_address")])){
                                $site_detail = $all_site_arr[Session::get("selected_site_address")];

                                echo $site_detail["company_name"] .' , '.$site_detail["site_name"] .' , '. $site_detail["state_name"];
                            }
                        ?>



                        @else

                        @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null) && (Session::get("selected_display_delivery_date") != null))

                            {{ Session::get("selected_city_name") }}, {{ Session::get("selected_state_name") }}

                        @else
                            Choose Location
                        @endif

                        @endif
                    </label><br/>
                    <span class="mb-2" style="display:block;">Concrete Density Should be between 2400 - 2550 KG 
                        <span id="concrete_density" style="display:none"><b>0 KG</b>
                    </span>
                    </span>
                    <span class="error" id="concrete_density_error"></span>
                    <span class="text-success" id="concrete_density_success"></span>
                    </span>
                    <form name="custom_mix_form" action="{{ route('buyer_custom_mix') }}" method="post" class="cust_design_form">
                    @csrf
                        <div class="row">
                            <div class="col-md-12 mb-3" id="custom_mix_delivery_site_location_drop_div">
                                <label class="form-label">Select Delivery Address<sup>*</sup></label>
                                <select class="form-select" name="site_id" id="custom_mix_delivery_site_location_drop">
                                    <option value="" disabled="true" selected>Select Delivery Address</option>
                                    @if(isset($site_data["data"]))
                                        @foreach($site_data["data"] as $value)
                                            <option value="{{ $value['_id'] }}" data="{{ json_encode($value) }}">
                                                {{ $value["company_name"] }},
                                                {{ $value["site_name"] }},
                                                {{ $value["address_line1"] }}, {{ $value["address_line2"] }}, {{ $value["city_name"] }} - {{ $value["pincode"] }}, {{ $value["state_name"] }}, ({{ $value["country_name"] }})
                                            </option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>
                            <div class="col-md-6 mb-3" id="concrete_grade_id_div">
                                <label class="form-label">Select Concrete Grade<sup>*</sup></label>
                                <select name="concrete_grade_id" id="concrete_grade_id" class="form-select" onchange="concreteDensity()">
                                <option value="">Select Concrete Grade</option>
                                @if(isset($concrete_grade_data))
                                    @foreach($concrete_grade_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Design Mix ( In Cubic Meter )</label>
                                <input type="text" class="form-control" name="" readOnly="" placeholder="1 Cu.Mtr">
                            </div>
                            <div class="col-md-6 mb-3" id="mix_cement_brnad_div">
                                <label class="form-label">Select Cement Brand ( Choose Max Two )<sup>*</sup></label>
                                <select name="cement_brand_id[]" id="mix_cement_brnad" onchange="concreteDensity()" class="form-select multiple_select" multiple="multiple">
                                @if(isset($cement_brand_data))
                                    @foreach($cement_brand_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>
                            <div class="col-md-6 mb-3" id="cement_grade_id_div">
                                <label class="form-label">Select Cement Grade<sup>*</sup></label>
                                <select name="cement_grade_id[]" onchange="concreteDensity()" id="cement_grade_id" class="form-select">
                                <option disabled="disabled">Select Cement Grade</option>
                                @if(isset($cement_grade_data))
                                    @foreach($cement_grade_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Cement ( Kg )<sup>*</sup></label>
                                <input name="cement_quantity" id="custom_cement_qty" onkeyup="concreteDensity()" type="text" class="form-control" placeholder="e.g. 150">
                            </div>
                            <div class="col-md-6 mb-3" id="mix_sand_source_div">
                                <label class="form-label">Select Coarse Sand Source Name ( Choose Max Two )<sup>*</sup> </label>
                                <select name="sand_source_id[]" id="mix_sand_source" onchange="concreteDensity()" class="form-select multiple_select" multiple="multiple">
                                @if(isset($sand_source_data))
                                    @foreach($sand_source_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>
                            <div class="col-md-6 mb-3" id="sand_zone_div">
                                <label class="form-label">Select Coarse Sand Zone<sup>*</sup></label>
                                <select name="sand_zone_id[]" id="mix_sand_zone" onchange="concreteDensity()" class="form-select" >
                                <option disabled="disabled" selected="selected">Select Coarse Sand Zone</option>
                                @if(isset($sand_zone_data))
                                    @foreach($sand_zone_data as $value)
                                        <option value="{{ $value['_id'] }}">{{ $value["zone_name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Coarse Sand ( Kg )<sup>*</sup></label>
                                <input name="sand_quantity" id="custom_sand_qty" onkeyup="concreteDensity()" type="text" class="form-control" placeholder="e.g. 840">
                            </div>
                            <div class="col-md-6 mb-3" id="aggregate_source_id_div">
                                <label class="form-label">Select Aggregate Source (VSI) ( Choose Max Two )<sup>*</sup></label>
                                <select name="aggregate_source_id[]" id="mix_aggregate_source" onchange="concreteDensity()" class="form-select multiple_select" multiple="multiple">
                                @if(isset($aggregate_source_data))
                                    @foreach($aggregate_source_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>

                            <div class="col-md-6 mb-3" id="aggregate1_sub_cat_id_div">
                                <label class="form-label">Select Aggregate type 1 (VSI)<sup>*</sup></label>
                                <select name="aggregate1_sub_cat_id[]" id="mix_sub_cat_1" onchange="concreteDensity()" class="form-select" >
                                <option disabled="disabled" selected="selected">Select Aggregate category 1</option>
                                @if(isset($aggregate_sub_cat_data))
                                    @foreach($aggregate_sub_cat_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>

                            <div class="col-md-6 mb-3" id="aggregate2_sub_cat_id_div">
                                <label class="form-label">Select Aggregate type 2 (VSI)<sup>*</sup></label>
                                <select name="aggregate2_sub_cat_id[]" id="mix_sub_cat_2" onchange="concreteDensity()" class="form-select" >
                                    <option disabled="disabled" selected="selected">Select Aggregate category 2</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" id="aggreagte_cat_1">10mm ( Kg )<sup>*</sup></label>
                                <input name="aggregate1_quantity" id="custom_aggregate1_qty" onkeyup="concreteDensity()" type="text" class="form-control" placeholder="e.g. 400">
                            </div>

                            <div class="col-md-6 mb-3">
                                <label class="form-label" id=aggreagte_cat_2>20mm ( Kg )<sup>*</sup></label>
                                <input name="aggregate2_quantity" id="custom_aggregate2_qty" onkeyup="concreteDensity()" type="text" class="form-control" placeholder="e.g. 840">
                            </div>
                            <div class="col-md-6 mb-3" id="ad_mixture_brand_id_div">
                                <label class="form-label">Chemical Admixture Brand 1 <sup>*</sup></label>
                                <select name="admix_brand_id[]" id="ad_mixture_brand_id" onchange="concreteDensity()" class="form-select">
                                <option value="">Select Chemical Admixture Brand 1</option>
                                @if(isset($admixture_brand_data))
                                    @foreach($admixture_brand_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>

                            <div class="col-md-6 mb-3" id="admix_code_id_div">
                                <label class="form-label">Chemical Admixture Code 1 <sup>*</sup></label>
                                <select name="admix_code_id[]" id="ad_mixture_category_id" onchange="concreteDensity()" class="form-select">

                                    <option disabled="disabled">Select Chemical Admixture Code 1</option>

                                </select>
                            </div>
                            
                            <div class="col-md-6 mb-3" id="ad_mixture_brand_2_id_div">
                                <label class="form-label">Chemical Admixture Brand 2 ( Optional )</label>
                                <select name="admix_brand_id_2[]" id="ad_mixture_brand_2_id" onchange="concreteDensity()" class="form-select">
                                <option value="">Select Admixture Brand 2</option>
                                @if(isset($admixture_brand_data))
                                    @foreach($admixture_brand_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>

                            <div class="col-md-6 mb-3" id="admix_code_id_2_div">
                                <label class="form-label">Chemical Admixture Code 2 ( Optional )</label>
                                <select name="admix_code_id_2[]" id="ad_mixture_category_2_id" onchange="concreteDensity()" class="form-select">

                                    <option disabled="disabled">Select Chemical Admixture Code 2</option>

                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="row">
                                    <div class="col-6">
                                        <label class="form-label">Chemical Admixture ( % )<sup>*</sup></label>
                                        <input name="admix_quantity_per" id="custom_admixture_qty_per" onkeyup="calculateAdMix()" type="text" class="form-control" placeholder="Value in %">
                                        <span class="error" id="admix_quantity_error"></span>
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label">Chemical Admixture ( kg )<sup>*</sup></label>
                                        <input name="admix_quantity" id="custom_admixture_qty" readOnly onkeyup="concreteDensity()" type="text" class="form-control" placeholder="0 KG">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3" id="fly_ash_source_id_div">
                                <label class="form-label">Fly Ash Source Name ( Optional )</label>
                                <select name="fly_ash_source_id[]" id="mix_fly_ash_brand" onchange="concreteDensity()" class="form-select">
                                <option value="">Select Fly Ash Source</option>
                                @if(isset($fly_ash_source_data))
                                    @foreach($fly_ash_source_data as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Fly Ash ( Kg )</label>
                                <input name="fly_ash_quantity" id="custom_flyash_qty" readOnly onkeyup="concreteDensity()" type="text" class="form-control" placeholder="e.g. 150">
                            </div>
                            <div class="col-md-6 mb-3" id="water_type_div">
                                <label class="form-label">Water Type<sup>*</sup></label>
                                <select name="water_type[]" id="mix_water_type" onchange="concreteDensity()" class="form-select">
                                    <option value="">Select Water Type</option>
                                    <option value="Regular">Regular</option>
                                    <option value="Mineral">Mineral</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Water ( Ltr ) ( Water/Cement Ratio 0.35 - 0.55 )<sup>*</sup></label>
                                <input name="water_quantity" id="custom_water_qty" onkeyup="concreteDensity()" type="text" class="form-control" placeholder="e.g. 190">
                                <span class="error" id="water_quantity_error"></span>
                                <span class="text-success" id="water_quantity_success"></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">With Transit Mixer<sup>*</sup></label>
                                <!-- <input name="with_TM" type="text" class="form-control" placeholder="Yes"> -->
                                <select name="with_TM" class="form-select" onchange="concreteDensity()" id="custom_mix_with_TM" readOnly>
                                    <!-- <option value="" disabled>Select With Transit Mixer</option> -->
                                    <option value="yes" selected>Yes</option>
                                    <!-- <option value="no">No</option> -->
                                </select>
                                <label id="custome_mix_TM_no_warning" class="text-success" style="font-style:italic;display:none">Your RMC order will be delivered without Transit Mixer</label>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label class="form-label">With Concrete Pump<sup>*</sup></label>
                                <select name="with_CP" class="form-select" onchange="concreteDensity()" id="custom_mix_with_CP">
                                    <!-- <option value="" disabled>Select With Concrete Pump</option> -->
                                    <option value="no">No</option>
                                    <option value="yes">Yes</option>


                                </select>
                            </div>
                            <input type="hidden" id="is_concrete_density_error" value="0" />
                            <input type="hidden" id="is_WC_ratio_error" value="0" />
                            <input type="hidden" id="is_admixture_percent_error" value="0" />

                            <span class="error" id="concrete_density_error_span"></span>

                            <input type="hidden" id="is_custom_mix_open_from_vendor_detail" value="0" />
                            <input type="hidden" name="is_custom_mix_save" id="is_custom_mix_save" value="0" />
                            <input type="hidden" name="is_custom_mix_add_edit" id="is_custom_mix_add_edit" value="0" />
                            <input type="hidden" name="custom_mix_edit_id" id="custom_mix_edit_id" value="0" />
                            
                            <input type="hidden" name="is_custom_mix_open_from_save" id="is_custom_mix_open_from_save" value="0" />
                            <input type="hidden" name="custom_mix_selected_lat" id="custom_mix_selected_lat" value="0" />
                            <input type="hidden" name="custom_mix_selected_long" id="custom_mix_selected_long" value="0" />
                            <input type="hidden" name="custom_mix_selected_state_id" id="custom_mix_selected_state_id" value="0" />

                            <div class="col-md-12">
                                <div class="progress_form mb-4">
                                    <div id="custom_mix_progress" class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="col-md-12" style="display: flex;align-items: baseline;">
                                <button type="submit" id="custom_design_mix_button" name="custom_design_mix" disabled value="custom_design_mix" class="btn btn-primary btn-secondary mt-2">SUBMIT</button>
                                <span class="mb-2" style="display:block;margin-left: 20px;">Concrete Density Should be between 2400 - 2550 KG 
                                    <span id="concrete_density_footer" style="display:none"><b>0 KG</b>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- custome mix end -->
</div>

@if(Route::current()->getName() == 'buyer_mix_detail')

<?php //dd($cart_details);
$total_mix = 0;
if(isset($data["data"])){
    $total_mix = count($data["data"]);

}
$min_order_quantity = 3;

$product_id = 1;
?>
@if($total_mix > 0)
    @if(isset($data["data"][0]))
    <?php $first_record = $data["data"][0]; ?>
<!-- Choose time slot start -->
<div class="modal fade" id="choose_time_popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Modify Delivery Date</h5>
                <form name="TM_CP_choose_date_form">
                    <div class="choose_time_list">
                        <div class="choose_time_bx d-flex mb-3 pb-3">

                            <div class="choose_time_img">
                                <img src="{{asset('assets/buyer/images/category_truck.webp') }}" alt="img">
                                <!-- <input class="form-check-input" type="checkbox" id="checkboxNoLabel" checked="" value="" aria-label="..."> -->
                            </div>
                            <div class="choose_time_info">
                                <p class="mb-1"><b>Rate of Transit Mixer :</b> ₹ {{ number_format($first_record["TM_price"],2) }} Per Cu.Mtr/Km</p>
                                @if($first_record["with_CP"] == true)
                                    <div id="CP_in_choose_date_popup">
                                        <p class="mb-1"><b>Rate of Concrete Pump :</b> ₹ {{ number_format($first_record["CP_price"],2) }} Per Cu.Mtr</p>
                                    </div>
                                @endif
                                <div class="mb-1">
                                    <label class="form-label">Choose your delivery date below </label>
                                    <!-- <input type="date" class="form-control" placeholder="TM Available Date & time"> -->

                                    <input type="text" name="delivery_date" class="form-control TM_CP_change_date" readonly="" placeholder="e.g. yyyy-mm-dd">
                                    <p class="error" id="TM_CP_change_date_error"></p>
                                    <p class="error" id="TM_change_date_error"></p>
                                    <p class="error" id="CP_change_date_error"></p>
                                </div>
                                <!-- <div class="alert alert-primary mb-0" role="alert"><b>Info!</b> Shipment within 48 Hours</div>
                                <div class="alert alert-danger mb-0" role="alert"><b>Info!</b> Pump maintence</div> -->
                            </div>
                        </div>
                        <!-- <div class="choose_time_bx d-flex mb-3 pb-3">
                            <div class="choose_time_img">
                                <img src="images/category_truck.webp" alt="img">
                                <input class="form-check-input" type="checkbox" id="checkboxNoLabel" checked="" value="" aria-label="...">
                            </div>
                            <div class="choose_time_info">
                                <p class="mb-1"><b>With Concrete Pump :</b> ₹ 500 / Cu. mtr</p>
                                <div class="mb-2">
                                    <label class="form-label">Concrete Pump Available Date & Time</label>
                                    <input type="date" class="form-control" placeholder="Concrete Pump Available Date & Time">
                                </div>
                                <div class="mb-1">
                                    <label class="form-label">Pump Type</label>
                                    <select class="form-select">
                                        <option>500 Horsepower</option>
                                        <option>1000 Horsepower</option>
                                        <option>1500 Horsepower</option>
                                    </select>
                                </div>
                                <div class="alert alert-danger mb-0" role="alert"><b>Info!</b> Pump maintence</div>
                            </div>
                        </div> -->
                    </div>
                    <input type="hidden" name="time_is_design_mix" id="time_is_design_mix" value="0" />
                    <button type="submit" class="btn btn-primary btn-secondary" type="button">DONE</button>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Choose time slot end -->
<?php
$selected_delivery_date = "";
if(Session::get("selected_display_delivery_date") != null){
    $selected_delivery_date = Session::get("selected_display_delivery_date");
}

?>
<script>
	$('.TM_CP_change_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'd M yyyy'
        // format: 'yyyy-mm-dd'
	});

    // $('.TM_CP_change_date').datepicker("setDate", {{ $selected_delivery_date }} );

    $('.TM_CP_change_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

    $(".TM_CP_change_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });
</script>

    @endif
@endif



<div class="modal fade" id="Buyer_tm_detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" id="Buyer_tm_detail_close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Buyer Transit Mixer Detail</h5>
                <label class="mb-2">Enter Your Transit Mixer Detail</label>
                <form name="buyer_TM_detail">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Choose Delivery Date <sup>*</sup></label>
                            <input type="text" name="delivery_date" class="form-control buyer_detail_change_date" readonly="" placeholder="e.g. DD-MM-YYYY">
                            <p class="error" id="buyer_detail_change_date_error"></p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Transit Mixer No <sup>*</sup></label>
                            <input type="text" name="TM_no" class="form-control" placeholder="e.g. GJ-00-AA-0000">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Driver Name <sup>*</sup></label>
                            <input type="text" name="driver_name" class="form-control" placeholder="e.g. John">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Driver Mobile No. <sup>*</sup></label>
                            <input type="text" name="driver_mobile" class="form-control" placeholder="e.g. 9898989898">
                        </div>
                    </div>
                    <!-- <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">TM Operator Name <sup>*</sup></label>
                            <input type="text" name="TM_operator_name" class="form-control" placeholder="e.g. Johndoe">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">TM Operator Mobile No <sup>*</sup></label>
                            <input type="text" name="TM_operator_mobile_no" class="form-control" placeholder="e.g. 989898989">
                        </div>
                    </div> -->
                </div>
                <input type="hidden" name="buyer_tm_is_design_mix" id="buyer_tm_is_design_mix" value="0" />
                <button type="submit" class="btn btn-primary btn-secondary" type="button">DONE</button>

            </div>
            </form>
        </div>
    </div>
</div>
<?php
$selected_delivery_date = "";
if(Session::get("selected_display_delivery_date") != null){
    $selected_delivery_date = Session::get("selected_display_delivery_date");
}

?>
<script>
	$('.buyer_detail_change_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'd M yyyy'
        // format: 'yyyy-mm-dd'
	});

    // $('.buyer_detail_change_date').datepicker("setDate",  );

    $('.buyer_detail_change_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

    $(".buyer_detail_change_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });
</script>

@endif


@if(Route::current()->getName() == 'buyer_product_detail')

<?php
//dd($data["data"]);

$min_order_quantity = 3;

$product_id = 1;
?>
@if(isset($data["data"]))
    <?php $first_record = $data["data"]; ?>
<!-- Choose time slot start -->
<div class="modal fade" id="choose_time_popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" id="choose_time_popup_close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Modify Delivery Date</h5>
                <form name="TM_CP_choose_date_form">
                    <div class="choose_time_list">
                        <div class="choose_time_bx d-flex mb-3 pb-3">

                            <div class="choose_time_img">
                                <img src="{{asset('assets/buyer/images/category_truck.webp') }}" alt="img">
                                <!-- <input class="form-check-input" type="checkbox" id="checkboxNoLabel" checked="" value="" aria-label="..."> -->
                            </div>
                            <div class="choose_time_info">
                                <p class="mb-1"><b>Rate of Transit Mixer :</b> ₹ {{ number_format($first_record["TM_price"],2) }} Per Cu.Mtr/Km</p>
                                @if($first_record["with_CP"] == true)
                                    <div id="CP_in_choose_date_popup">
                                        <p class="mb-1"><b>Rate of Concrete Pump :</b> ₹ {{ number_format($first_record["CP_price"],2) }} Per day</p>
                                    </div>
                                @endif
                                <div class="mb-1">
                                    <label class="form-label">Choose your delivery date below</label>
                                    <!-- <input type="date" class="form-control" placeholder="TM Available Date & time"> -->

                                    <input type="text" name="delivery_date" class="form-control TM_CP_change_date" readonly="" placeholder="e.g. yyyy-mm-dd">
                                    <p class="error" id="TM_CP_change_date_error"></p>
                                    <p class="error" id="TM_change_date_error"></p>
                                    <p class="error" id="CP_change_date_error"></p>
                                </div>
                                <!-- <div class="alert alert-primary mb-0" role="alert"><b>Info!</b> Shipment within 48 Hours</div>
                                <div class="alert alert-danger mb-0" role="alert"><b>Info!</b> Pump maintence</div> -->
                            </div>
                        </div>
                        <!-- <div class="choose_time_bx d-flex mb-3 pb-3">
                            <div class="choose_time_img">
                                <img src="images/category_truck.webp" alt="img">
                                <input class="form-check-input" type="checkbox" id="checkboxNoLabel" checked="" value="" aria-label="...">
                            </div>
                            <div class="choose_time_info">
                                <p class="mb-1"><b>With Concrete Pump :</b> ₹ 500 / Cu. mtr</p>
                                <div class="mb-2">
                                    <label class="form-label">Concrete Pump Available Date & Time</label>
                                    <input type="date" class="form-control" placeholder="Concrete Pump Available Date & Time">
                                </div>
                                <div class="mb-1">
                                    <label class="form-label">Pump Type</label>
                                    <select class="form-select">
                                        <option>500 Horsepower</option>
                                        <option>1000 Horsepower</option>
                                        <option>1500 Horsepower</option>
                                    </select>
                                </div>
                                <div class="alert alert-danger mb-0" role="alert"><b>Info!</b> Pump maintence</div>
                            </div>
                        </div> -->
                    </div>
                    <input type="hidden" name="time_is_design_mix" id="time_is_design_mix" value="1" />
                    <button type="submit" class="btn btn-primary btn-secondary" type="button">DONE</button>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Choose time slot end -->
<?php
$selected_delivery_date = "";
if(Session::get("selected_display_delivery_date") != null){
    $selected_delivery_date = Session::get("selected_display_delivery_date");
}

?>
<script>
	$('.TM_CP_change_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'd M yyyy'
        // format: 'yyyy-mm-dd'
	});

    // $('.TM_CP_change_date').datepicker("setDate", {{ $selected_delivery_date }} );

    $('.TM_CP_change_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

    $(".TM_CP_change_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });
</script>

@endif



<div class="modal fade" id="Buyer_tm_detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" id="Buyer_tm_detail_close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title">Buyer Transit Mixer Detail</h5>
                <label class="mb-2">Enter Your Transit Mixer Detail</label>
                <form name="buyer_TM_detail">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Choose Delivery Date <sup>*</sup></label>
                            <input type="text" name="delivery_date" class="form-control buyer_detail_change_date" readonly="" placeholder="e.g. DD-MM-YYYY">
                            <p class="error" id="buyer_detail_change_date_error"></p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Transit Mixer No <sup>*</sup></label>
                            <input type="text" name="TM_no" class="form-control" placeholder="e.g. GJ-00-AA-0000">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Driver Name <sup>*</sup></label>
                            <input type="text" name="driver_name" class="form-control" placeholder="e.g. John">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Driver Mobile No. <sup>*</sup></label>
                            <input type="text" name="driver_mobile" class="form-control" placeholder="e.g. 9898989898">
                        </div>
                    </div>
                    <!-- <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">TM Operator Name <sup>*</sup></label>
                            <input type="text" name="TM_operator_name" class="form-control" placeholder="e.g. Johndoe">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">TM Operator Mobile No <sup>*</sup></label>
                            <input type="text" name="TM_operator_mobile_no" class="form-control" placeholder="e.g. 9898989898">
                        </div>
                    </div> -->
                </div>
                <input type="hidden" name="buyer_tm_is_design_mix" id="buyer_tm_is_design_mix" value="1" />
                <button type="submit" class="btn btn-primary btn-secondary" type="button">DONE</button>

            </div>
            </form>
        </div>
    </div>
</div>
<?php
$selected_delivery_date = "";
if(Session::get("selected_display_delivery_date") != null){
    $selected_delivery_date = Session::get("selected_display_delivery_date");
}

?>
<script>
	$('.buyer_detail_change_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'd M yyyy'
        // format: 'yyyy-mm-dd'
	});

    // $('.buyer_detail_change_date').datepicker("setDate",  );

    $('.buyer_detail_change_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

    $(".buyer_detail_change_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });
</script>

@endif


<!-- PAYMENT POPUP START -->
<div id="payment_fail_popup" class="payment-popup-block" style="display: none;">
    <div class="payment-goto-text" style="display: none;">
        Go to Payment Gateway
    </div>
    <div class="payment-thanku-popup" style="display: block">
        <img src="{{ asset('assets/buyer/images/check-icon.webp') }}" class="img-responsive" alt="" />
        <p class="thanku-text"><span>Payment Failed</span> Please try again later</p>
        <p class="orderid-text" id="pay_error_msg"></p>
        <!-- <a href="{{ route('buyer_home') }}" class="site-button yellow">Ok</a> -->
    </div>
    <div style="display: none;">

    </div>
</div>
<!-- PAYMENT POPUP END -->

<!-- PAYMENT POPUP START -->
<div id="payment_success_popup" class="payment-popup-block" style="display: none;">
    <div class="payment-goto-text" style="display: none;">
        Go to Payment Gateway
    </div>
    <div class="payment-thanku-popup" style="display: block">
        <img src="{{ asset('assets/buyer/images/check-icon.webp') }}" class="img-responsive" alt="" />
        <p class="thanku-text"><span>Thank You!</span> We have received payment</p>
        <p class="orderid-text">Please wait...</p>
        <!-- <a href="{{ route('buyer_home') }}" class="site-button yellow">Ok</a> -->
    </div>
    <div style="display: none;">

    </div>
</div>
<!-- PAYMENT POPUP END -->

<!-- PAYMENT POPUP START -->
<div id="order_success_popup" class="payment-popup-block" style="display: none;">

    <div class="payment-thanku-popup" style="display: block">
        <img src="{{ asset('assets/buyer/images/check-icon.webp') }}" class="img-responsive" alt="" />
        <p class="thanku-text"><span>Thank You!</span> We have received your order. It will be confirmed within 3 hours</p>
        <p class="orderid-text">Order Id: #<span id="success_order_id"></span></p>
        <a href="{{ route('buyer_home') }}" class="btn btn-primary btn-secondary mt-2">Ok</a>
    </div>

</div>
<!-- PAYMENT POPUP END -->



@if(Route::current()->getName() == 'buyer_product_detail_no')

<?php //dd($cart_details);

$min_order_quantity = 3;

?>
@if(isset($data["data"]))
    <?php $first_record = $data["data"]; ?>
<!-- Choose time slot start -->
<div class="modal fade" id="choose_time_popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Modify Delivery Date</h5>
                <form name="TM_CP_choose_date_form">
                    <div class="choose_time_list">
                        <div class="choose_time_bx d-flex mb-3 pb-3">

                            <div class="choose_time_img">
                                <img src="{{asset('assets/buyer/images/category_truck.webp') }}" alt="img">
                                <!-- <input class="form-check-input" type="checkbox" id="checkboxNoLabel" checked="" value="" aria-label="..."> -->
                            </div>
                            <div class="choose_time_info">
                                <p class="mb-1"><b>Rate of Transit Mixer :</b> ₹ {{ number_format($first_record["TM_price"],2) }} Per Cu.Mtr/Km</p>
                                @if($first_record["with_CP"] == true)
                                    <div id="CP_in_choose_date_popup">
                                        <p class="mb-1"><b>Rate of Concrete Pump :</b> ₹ {{ number_format($first_record["CP_price"],2) }} Per day</p>
                                    </div>
                                @endif
                                <div class="mb-1">
                                    <label class="form-label">Choose your delivery date below</label>
                                    <!-- <input type="date" class="form-control" placeholder="TM Available Date & time"> -->

                                    <input type="text" name="delivery_date" class="form-control TM_CP_change_date" readonly="" placeholder="e.g. yyyy-mm-dd">
                                    <p class="error" id="TM_CP_change_date_error"></p>
                                </div>
                                <!-- <div class="alert alert-primary mb-0" role="alert"><b>Info!</b> Shipment within 48 Hours</div>
                                <div class="alert alert-danger mb-0" role="alert"><b>Info!</b> Pump maintence</div> -->
                            </div>
                        </div>
                        <!-- <div class="choose_time_bx d-flex mb-3 pb-3">
                            <div class="choose_time_img">
                                <img src="images/category_truck.webp" alt="img">
                                <input class="form-check-input" type="checkbox" id="checkboxNoLabel" checked="" value="" aria-label="...">
                            </div>
                            <div class="choose_time_info">
                                <p class="mb-1"><b>With Concrete Pump :</b> ₹ 500 / Cu. mtr</p>
                                <div class="mb-2">
                                    <label class="form-label">Concrete Pump Available Date & Time</label>
                                    <input type="date" class="form-control" placeholder="Concrete Pump Available Date & Time">
                                </div>
                                <div class="mb-1">
                                    <label class="form-label">Pump Type</label>
                                    <select class="form-select">
                                        <option>500 Horsepower</option>
                                        <option>1000 Horsepower</option>
                                        <option>1500 Horsepower</option>
                                    </select>
                                </div>
                                <div class="alert alert-danger mb-0" role="alert"><b>Info!</b> Pump maintence</div>
                            </div>
                        </div> -->
                    </div>
                    <button type="submit" class="btn btn-primary btn-secondary" type="button">DONE</button>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Choose time slot end -->
<?php
$selected_delivery_date = "";
if(Session::get("selected_display_delivery_date") != null){
    $selected_delivery_date = Session::get("selected_display_delivery_date");
}

?>
<script>
	$('.TM_CP_change_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'd M yyyy'
        // format: 'yyyy-mm-dd'
	});

    // $('.TM_CP_change_date').datepicker("setDate", {{ $selected_delivery_date }} );

    $('.TM_CP_change_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

    $(".TM_CP_change_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });
</script>

@endif



<div class="modal fade" id="Buyer_tm_detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Buyer Transit Mixer Detail</h5>
                <label class="mb-2">Enter Your Transit Mixer Detail</label>
                <form name="buyer_TM_detail">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Choose Delivery Date <sup>*</sup></label>
                            <input type="text" name="delivery_date" class="form-control buyer_detail_change_date" readonly="" placeholder="e.g. DD-MM-YYYY">
                            <p class="error" id="buyer_detail_change_date_error"></p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Transit Mixer No <sup>*</sup></label>
                            <input type="text" name="TM_no" class="form-control" placeholder="e.g. GJ-00-AA-0000">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Driver Name <sup>*</sup></label>
                            <input type="text" name="driver_name" class="form-control" placeholder="e.g. John">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Driver Mobile No. <sup>*</sup></label>
                            <input type="text" name="driver_mobile" class="form-control" placeholder="e.g. 9898989898">
                        </div>
                    </div>
                    <!-- <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">TM Operator Name <sup>*</sup></label>
                            <input type="text" name="TM_operator_name" class="form-control" placeholder="e.g. Johndoe">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">TM Operator Mobile No <sup>*</sup></label>
                            <input type="text" name="TM_operator_mobile_no" class="form-control" placeholder="e.g. 9898989898">
                        </div>
                    </div> -->
                </div>
                <button type="submit" class="btn btn-primary btn-secondary" type="button">DONE</button>

            </div>
            </form>
        </div>
    </div>
</div>

<script>
	$('.buyer_detail_change_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'yyyy-mm-dd'
	});

    // $('.buyer_detail_change_date').datepicker("setDate", {{ $selected_delivery_date }} );

    $('.buyer_detail_change_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

    $(".buyer_detail_change_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });
</script>

@endif



@if(Route::current()->getName() == 'buyer_cart_checkout')

<?php
//dd($data["data"]);

$min_order_quantity = 3;

$product_id = 1;
?>
<input type="hidden" id="is_time_popup_submit" />
<!-- Choose time slot start -->
<div class="modal fade" id="choose_time_popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Modify Delivery Date</h5>
                <!-- <form name="TM_CP_choose_date_form_cart"> -->
                    <div class="choose_time_list">
                        <div class="choose_time_bx d-flex mb-3 pb-3">

                            <div class="choose_time_img">
                                <img src="{{asset('assets/buyer/images/category_truck.webp') }}" alt="img">
                                <!-- <input class="form-check-input" type="checkbox" id="checkboxNoLabel" checked="" value="" aria-label="..."> -->
                            </div>
                            <div class="choose_time_info">
                                <div id="time_popup_TM_details">

                                </div>
                                <div class="mb-1">
                                    <label class="form-label" id="time_popup_TM_and_CP">Choose your delivery date below</label>
                                    <!-- <input type="date" class="form-control" placeholder="TM Available Date & time"> -->

                                    <!-- <input type="text" name="delivery_date" class="form-control TM_CP_change_date" readonly="" placeholder="e.g. DD-MM-YYYY">
                                    <p class="error" id="TM_CP_change_date_error"></p>
                                    <p class="error" id="TM_change_date_error"></p>
                                    <p class="error" id="CP_change_date_error"></p> -->

                                    <div class="range_picker m-r10">
                                        <input class="range_picker_input form-control" type="text" name="modify_daterange" value="" />
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <span class="text-success d-block" id="modify_delivery_date_msg"></span>
                                    <p class="error" id="modify_delivery_date_error"></p>

                                </div>
                                <!-- <div class="alert alert-primary mb-0" role="alert"><b>Info!</b> Shipment within 48 Hours</div>
                                <div class="alert alert-danger mb-0" role="alert"><b>Info!</b> Pump maintence</div> -->
                            </div>
                        </div>

                    </div>
                    <input type="hidden" name="time_product_details" id="time_product_details" />
                    <input type="hidden" name="time_item_id" id="time_item_id" />
                    <input type="hidden" name="time_design_mix_id" id="time_design_mix_id" />
                    <input type="hidden" name="time_quantity" id="time_quantity" />
                    <input type="hidden" name="time_cart_place" id="time_cart_place" />
                    <input type="hidden" name="time_is_custom_mix" id="time_is_custom_mix" />
                    <input type="hidden" name="time_csrf_token" id="time_csrf_token" />
                    <input type="hidden" name="time_cart_start_delivery_date" id="time_cart_start_delivery_date" />
                    <input type="hidden" name="time_cart_end_delivery_date" id="time_cart_end_delivery_date" />
                    <input type="hidden" name="time_cart_design_mix_detail_id" id="time_cart_design_mix_detail_id" />
                    <button onclick="updateCartItemDate()" class="btn btn-primary btn-secondary" type="button" id="modify_date_range_button" disabled="disabled">DONE</button>

                <!-- </form> -->
            </div>
        </div>
    </div>
</div>
<!-- Choose time slot end -->

<script>
	$('.TM_CP_change_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'yyyy-mm-dd'
	});

    // $('.TM_CP_change_date').datepicker("setDate", {{ $selected_delivery_date }} );



    $(".TM_CP_change_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });

    // var today = new Date();
    // var today_to_30 = new Date();

    // today.setDate(today.getDate() + 2);
    // var dd = String(today.getDate()).padStart(2, '0');
    // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var yyyy = today.getFullYear();

    // today = dd + '/' + mm + '/' + yyyy;
    // today_request = yyyy + '/' + mm + '/'+dd;
    // // console.log("today..."+today);

    // today_to_30.setDate(today_to_30.getDate() + 32);
    // var dd_30 = String(today_to_30.getDate()).padStart(2, '0');
    // var mm_30 = String(today_to_30.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var yyyy_30 = today_to_30.getFullYear();

    // today_to_30 = dd_30 + '/' + mm_30 + '/' + yyyy_30;
    // today_to_30_request =  yyyy_30 + '/' + mm_30 + '/' + dd_30;

    // var end_date = today;

    // $('input[name="modify_daterange"]').daterangepicker({
    //     startDate: today,
    //     endDate: end_date,
    //     minDate: today,
    //     maxDate: today_to_30,
    //     locale: {
    //         format: 'DD/MM/YYYY'
    //     }

    // });

</script>




<div class="modal fade" id="Buyer_tm_detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Buyer Transit Mixer Detail</h5>
                <label class="mb-2">Enter Your Transit Mixer Detail</label>
                <!-- <form name="buyer_TM_detail"> -->
                <p class="error" id="cart_buyer_tm_detail_error"></p>
                <div class="row">

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Choose Delivery Date <sup>*</sup></label>
                            <input type="text" name="delivery_date" class="form-control buyer_detail_change_date" readonly="" placeholder="e.g. DD-MM-YYYY">
                            <p class="error" id="buyer_detail_change_date_error"></p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Transit Mixer No <sup>*</sup></label>
                            <input type="text" name="TM_no" id="cart_TM_no" class="form-control" placeholder="e.g. GJ-00-AA-0000">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Driver Name <sup>*</sup></label>
                            <input type="text" name="driver_name" id="cart_driver_name" class="form-control" placeholder="e.g. John">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">Driver Mobile No. <sup>*</sup></label>
                            <input type="text" name="driver_mobile" id="cart_driver_mobile" class="form-control" placeholder="e.g. 9898989898">
                        </div>
                    </div>
                    <!-- <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">TM Operator Name <sup>*</sup></label>
                            <input type="text" name="TM_operator_name" id="cart_TM_operator_name" class="form-control" placeholder="e.g. Johndoe    ">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="" class="form-label">TM Operator Mobile No <sup>*</sup></label>
                            <input type="text" name="TM_operator_mobile_no" id="cart_TM_operator_mobile_no" class="form-control" placeholder="e.g. 9898989898">
                        </div>
                    </div> -->
                </div>
                <input type="hidden" name="TM_detail_product_details" id="TM_detail_product_details" />
                <input type="hidden" name="TM_detail_item_id" id="TM_detail_item_id" />
                <input type="hidden" name="TM_detail_design_mix_id" id="TM_detail_design_mix_id" />
                <input type="hidden" name="TM_detail_quantity" id="TM_detail_quantity" />
                <input type="hidden" name="TM_detail_cart_place" id="TM_detail_cart_place" />
                <input type="hidden" name="TM_detail_is_custom_mix" id="TM_detail_is_custom_mix" />
                <input type="hidden" name="TM_detail_csrf_token" id="TM_detail_csrf_token" />
                <button type="submit" onclick="updateCartItemTMDetails()" class="btn btn-primary btn-secondary" type="button">DONE</button>

            </div>
            <!-- </form> -->
        </div>
    </div>
</div>

<script>
	$('.buyer_detail_change_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'yyyy-mm-dd'
	});

    // $('.buyer_detail_change_date').datepicker("setDate",  );



    $(".buyer_detail_change_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });
</script>

@endif


 <!-- MULTI TRUCK MODAL START -->
    <div id="multi-truck-popup" class="modal fade multi-truck-modal-block" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body p-a0">
                    <h5 class="title mb-4">Transit Mixer (TM) Tracking</h5>
                    <div class="truck_list_track" id="multi_truck_details_div">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MULTI TRUCK MODAL END -->

    <!-- MODAL CREATE TICKET START-->



<div class="modal fade" id="create-ticket-popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-3">Create Ticket</h5>
                <form name="add_support_ticket_form" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3" id="question_type_div">
                                <label for="" class="form-label">Type Of Issue</label>
                                <div class="custome_select">
                                    <select name="question_type" class="form-control form-select valid" aria-invalid="false">
                                        <option disabled="disabled" selected="selected">Select Question</option>
                                        <!-- <option value="Problem on payment">Problem On Payment</option>
                                        <option value="Problem on product">Problem On Product</option>
                                        <option value="Enable to order">Enable To Order</option> -->
                                        <option value="Rejection Of Order">Rejection Of Order</option>
                                        <option value="Quality of RMC">Quality of RMC</option>
                                        <option value="Dispute with RMC Supplier">Dispute with RMC Supplier</option>
                                        <option value="Delay in RMC Supply">Delay in RMC Supply</option>
                                        <option value="Non-Delivery of Concrete Pump">Non-Delivery of Concrete Pump</option>
                                        <option value="Damage / Theft of RMC Supplier's Concrete Pump">Damage / Theft of RMC Supplier's Concrete Pump</option>
                                        <option value="Accident of TM at Customer's Site">Accident of TM at Customer's Site</option>
                                        <option value="Payment Related issue : Payment Transaction Failed">Payment Related issue : Payment Transaction Failed</option>
                                        <option value="Payment Related issue : Payment duplication for the Order">Payment Related issue : Payment duplication for the Order</option>
                                        <option value="Payment Related issue : Payment  refund for cancelled/rejected/short close">Payment Related issue : Payment  refund for cancelled/rejected/short closed/Lapsed order</option>
                                        <option value="Order Confirmation Pending">Order Confirmation Pending</option>
                                        <option value="Payment processed but order not placed">Payment processed but order not placed</option>
                                        <option value="RMC supply not as per Design Mix requirement">RMC supply not as per Design Mix requirement</option>
                                        <option value="Suggestion for Concrete Grade">Suggestion for Concrete Grade</option>
                                        <option value="Querry related to Concrete Cube test Report">Querry related to Concrete Cube test Report</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <div id="severity_div">
                                    <label class="form-label">Priority</label>
                                    <div class="custome_select">
                                        <select name="severity" class="form-control form-select valid" aria-invalid="false">
                                            <option disabled="disabled" selected="selected">Select Priority</option>
                                            <option value="Urgent">Urgent</option>
                                            <option value="High">High</option>
                                            <option value="Normal">Normal</option>
                                            <option value="Low">Low</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="mb-3">
                                    <label class="form-label">Subject</label>
                                    <div class="cstm-input">
                                        <textarea name="subject" class="form-control" id="ctsubject" placeholder="Brief summary of the question or issue" data-maxchar="250"></textarea>
                                        <!-- <span class="character-text">Maximum 250 Characters (<span class="character-counter"></span> remaining)</span> -->
                                    </div>
                            </div>
                            <div class="mb-3">
                                    <label class="form-label">Description </label>
                                    <div class="cstm-input">
                                        <textarea name="description" class="form-control" id="ctdescription" placeholder="Detailed of the question or issue" data-maxchar="5000"></textarea>
                                        <!-- <span class="character-text">Maximum 5000 Characters (<span class="character-counter"></span> remaining)</span> -->
                                    </div>
                            </div>
                            <div class="mb-3">
                                <div class="" id="support_ticket_attachment_div">
                                    <label class="form-label">Attachment</label>

                                    <div class="file-browse">

                                        <div class="button-browse">
                                        <!-- <input type="file" id="myInput" accept="*/*"> -->
                                            Browse <input name="attachments[]" id="attachments_support" type="file" accept="*/*" multiple>
                                        </div>
                                        <input type="text" class="form-control browse-input" id="selected_ticket_doc" placeholder="Select Document" readonly="" >
                                    </div>
                                    <span class="file-note-text">Please use (jpg, jpeg, mp4, doc, docx, pdf, png) file format.</span>
                                    <span class="file-error-text" style="display: none;"><i class="ion-alert-circled"></i> Maximum amount of files exceeded!</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- <div class="modal-footer text-left">
                        <button type="submit" class="site-button pull-left">Submit</button>
                    </div> -->

                    <button class="btn btn-primary btn-secondary mt-3" type="submit">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
// $(documnet).on('ready', function(){
// $("#attachments_support").on('change', function (e){

//         // var fileName = $(this).val();
//         var fileName = e.target.files[0].name;
//         console.log("tset..."+fileName);
//         $("#selected_ticket_doc").val(fileName);
//       });
//       });

    </script>

<!-- MODAL CREATE TICKET END-->


<div class="modal fade" id="order_assign_qty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" id="order_assign_qty_close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Order Qty Assign</h5>
                <!-- <label class="mb-2">Enter Your Transit Mixer Detail</label> -->
                <form name="order_assign_qty">
                @csrf
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="" class="form-label">Choose Delivery Date <sup>*</sup></label>
                                <input type="text" name="delivery_date" id="order_assign_qty_change_date" class="form-control order_assign_qty_change_date" readonly="" placeholder="e.g. DD-MM-YYYY">
                                <span class="error" id="order_assign_qty_change_date_error"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="" class="form-label">Deliver Qty <sup>*</sup></label>
                                <input type="text" name="assigned_quantity" id="assigned_quantity" class="form-control" placeholder="e.g. 50" disabled>
                                <label id="assigned_quantity-error" class="error" for="assigned_quantity"></label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="" class="form-label">Delivery Start Time <sup>*</sup></label>
                                <select class="form-control" name="delivery_start_time" id="delivery_start_time" disabled>
                                    <option disabled="disabled" selected="selected">Start Time</option>

                                    <option value="00:00">00:00</option>
                                    <option value="01:00">01:00</option>
                                    <option value="02:00">02:00</option>
                                    <option value="03:00">03:00</option>
                                    <option value="04:00">04:00</option>
                                    <option value="05:00">05:00</option>
                                    <option value="06:00">06:00</option>
                                    <option value="07:00">07:00</option>
                                    <option value="08:00">08:00</option>
                                    <option value="09:00">09:00</option>
                                    <option value="10:00">10:00</option>
                                    <option value="11:00">11:00</option>

                                    <option value="12:00">12:00</option>
                                    <option value="13:00">13:00</option>
                                    <option value="14:00">14:00</option>
                                    <option value="15:00">15:00</option>
                                    <option value="16:00">16:00</option>
                                    <option value="17:00">17:00</option>
                                    <option value="18:00">18:00</option>
                                    <option value="19:00">19:00</option>
                                    <option value="20:00">20:00</option>
                                    <option value="21:00">21:00</option>
                                    <option value="22:00">22:00</option>
                                    <option value="23:00">23:00</option>

                                </select>
                            </div>
                        </div>

                        <span class="error mb-1" id="order_assign_qty_error"></span>
                        <span class="text-success mb-1" id="order_assign_qty_success"></span>
                        <span class="text-success mb-1" id="order_short_close_error"></span>
                        <!-- <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="" class="form-label">TM Operator Name <sup>*</sup></label>
                                <input type="text" name="TM_operator_name" class="form-control" placeholder="e.g. Johndoe">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="mb-3">
                                <label for="" class="form-label">TM Operator Mobile No <sup>*</sup></label>
                                <input type="text" name="TM_operator_mobile_no" class="form-control" placeholder="e.g. 989898989">
                            </div>
                        </div> -->
                    </div>
                    <input type="hidden" id="order_assign_part_qty" value="0" />
                    <input type="hidden" id="order_remain_part_qty" value="0" />
                    <input type="hidden" name="order_start_date_time" id="order_start_date_time" value="" />
                    <input type="hidden" name="order_end_date_time" id="order_end_date_time" value="" />
                    <input type="hidden" name="order_id" id="order_assign_qty_order_id" value="0" />
                    <input type="hidden" name="order_item_id" id="order_assign_qty_order_item_id" value="0" />
                    <button type="submit" disabled class="btn btn-primary btn-secondary" id="order_assign_qty_button" type="button">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>
<?php
$selected_delivery_date = "";
if(Session::get("selected_display_delivery_date") != null){
    $selected_delivery_date = Session::get("selected_display_delivery_date");
}

?>
<script>
    // $(document).ready(async function(){
    //     $('.order_assign_qty_change_date').datepicker({
    //         autoclose: true,
    //         startDate: '1d',
    //         format: 'd M yyyy'
    //         // format: 'yyyy-mm-dd'
    //     });

    //     // $('.order_assign_qty_change_date').datepicker("setDate",  );

    //     // $('.order_assign_qty_change_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

    //     $(".order_assign_qty_change_date").on('change', function(event) {
    //         event.preventDefault();
    //         // alert(this.value);
    //         // $("#selected_delivery_date").val(this.value);
    //         // $("#delivery_date_error").html("");
    //         /* Act on the event */
    //     });
    // });
</script>

<!-- MODAL Order tracking START-->
<div id="order-CP-track" class="modal fade" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
                <h5 class="title mb-4">Concrete Pump (CP) Tracking</h5>
                <div id="order_CP_trackking_div">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL Order tracking END-->




<!-- otp Modal start -->
<div class="modal fade" id="update_mobile_otp" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Update</h5>
                <p class="error" id="update_mobile_otp_error"></p>
                <form name="update-mobile-otp-form" style="display: block;" autocomplete="off">
                @csrf
                    <div class="login_fild otp_screen">
                        <div class="login_fild_inner">
                            <h6>OTP Confirmation Code</h6>
                            <p id="update_mobile_otp_confirm_message"></p>
                            <div class="timer_otp mb-3">
                                <span id="time_update_mobile">
                                    <span id="times">01:59</span>
                                </span>
                            </div>
                        </div>
                        <input type="hidden" name="mobile_number" id="verify_update_mobile" />
                        <input type="hidden" name="mobile_or_email_type" id="mobile_type" />
                        
                        <div class="otp_textbox flex-row d-flex mb-4 justify-content-evenly">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_1" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_2" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_3" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_4" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_5" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_6" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                        </div>

                        <span id="OTP_error_div_mobile"></span>

                        <div class="d-grid">
                            <!-- <button class="btn btn-primary mt-2" name="otp_btn" value="otp_btn" type="submit">Continue</button> -->
                            <button class="btn btn-primary btn-success mt-2" name="otp_btn" value="otp_btn" type="submit">VERIFY</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
<!-- otp Modal end -->
<div class="modal fade" id="new_mobile_no_popup" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">New Mobile Number</h5>
                <p class="error" id=""></p>
                <form name="new_mobile_form" id="" style="display: block;">
                    @csrf
                    <div class="login_fild">
                        <div class="login_fild_inner">
                            <div class="mb-3">
                                <label for="MobileNo" class="form-label">New Mobile No.</label>
                                <input type="text" class="form-control mobile_validate" name="mobile" id="update_new_mobile_no" placeholder="e.g. 9898989898">
                            </div>
                        </div>
                        <div class="d-grid">
                            <button type="submit" name="otp_send_btn" value="otp_send_btn" class="btn btn-primary mt-2">Send OTP</button>
                            <!-- <button class="btn btn-primary mt-2" data-bs-target="#forgot_otp_popup" data-bs-toggle="modal" data-bs-dismiss="modal">SEND</button> -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="update_email_otp" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Update</h5>
                <p class="error" id="update_email_otp_error"></p>
                <form name="update-email-otp-form" style="display: block;" autocomplete="off">
                @csrf
                    <div class="login_fild otp_screen">
                        <div class="login_fild_inner">
                            <h6>OTP Confirmation Code</h6>
                            <p id="update_email_otp_confirm_message"></p>
                            <div class="timer_otp mb-3">
                                <span id="time_update_email">
                                    <span id="times">01:59</span>
                                </span>
                            </div>
                        </div>
                        <input type="hidden" name="email" id="verify_update_email" />
                        <input type="hidden" name="mobile_or_email_type" id="email_type" />
                        
                        <div class="otp_textbox flex-row d-flex mb-4 justify-content-evenly">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_1" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_2" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_3" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_4" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_5" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                            <input type="text" class="form-control smsCode" pattern="[0-9]{1}" name="otp_code_6" maxlength="1" size="1" min="0" max="9" autocomplete="off">
                        </div>

                        <span id="OTP_error_div_email"></span>

                        <div class="d-grid">
                            <!-- <button class="btn btn-primary mt-2" name="otp_btn" value="otp_btn" type="submit">Continue</button> -->
                            <button class="btn btn-primary btn-success mt-2" name="otp_btn" value="otp_btn" type="submit">VERIFY</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="new_email_no_popup" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
    <div class="modal-dialog modal-dialog400">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">New Email</h5>
                <p class="error" id=""></p>
                <form name="new_email_form" id="" style="display: block;">
                    @csrf
                    <div class="login_fild">
                        <div class="login_fild_inner">
                            <div class="mb-3">
                                <label for="emailNo" class="form-label">New Email</label>
                                <input type="text" class="form-control" name="email" id="update_new_email" placeholder="e.g. johndoe@example.com">
                            </div>
                        </div>
                        <div class="d-grid">
                            <button type="submit" name="otp_send_btn" value="otp_send_btn" class="btn btn-primary mt-2">Send OTP</button>
                            <!-- <button class="btn btn-primary mt-2" data-bs-target="#forgot_otp_popup" data-bs-toggle="modal" data-bs-dismiss="modal">SEND</button> -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="feedback-popup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-body">
                <h5 class="title mb-4">Feedback</h5>
                <form name="feedback-form" id="feedback-form" style="display: block;">
                @csrf
                    <div class="login_fild">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="mb-3">
                                    <label for="" class="form-label">Select Feedback <sup>*</sup></label>
                                    <div class="custome_select">
                                        <select name="feedback_type" class="form-control form-select">
                                            <option value="">Select Feedback</option>
                                            <option value="Good">Good</option>
                                            <option value="Better">Better</option>
                                            <option value="Best">Best</option>
                                            <option value="Bad">Bad</option>
                                        </select>
                                    </div>
                                </div>                               
                                <div class="mb-3">
                                    <label class="form-label">Description <sup>*</sup></label>
                                    <div class="cstm-input">
                                        <textarea name="feedback_description" class="form-control" id="feedback_description" placeholder="Detailed of the feedback" data-maxchar="5000"></textarea>
                                        <!-- <span class="character-text">Maximum 5000 Characters (<span class="character-counter"></span> remaining)</span> -->
                                    </div>
                                </div>
                            </div>                            

                            <div class="d-grid">
                                <input type="hidden" name="order_id" id="feedback_order_id" value="{{ isset($order_id) ? $order_id : '' }}"/>
                                <button class="btn btn-primary mt-3" type="submit" name="feedback_btn" value="feedback_btn">SAVE</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-7-report-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- <div class="modal-header"> -->
          <!-- <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button> -->
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        <!-- </div> -->
        <div class="modal-body">
            <h5 class="title mb-4" id="add_7_report_title">Add Your 7th Day Cube Test Report</h5>
            <form action="" name="add_7_report_form" method="post">
                @csrf
                <div class="login_fild">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- <div class="form-group"> -->
                                <div class="mb-3">
                                    <label class="form-label">Reason Of Report Rejection </label>
                                    <div class="cstm-input">
                                        <input type="text" class="form-control" name="qube_test_report_7days_reject_reason" placeholder="e.g. Write reason of report rejection">
                                    
                                    </div>
                                </div>
                            <!-- </div> -->
                            <!-- </div>
                            <div class="col-md-12 col-sm-12 col-xs-12"> -->
                            <!-- <div class="form-group"> -->
                                <div class="mb-3" id="pic_7_report_div">
                                    <label class="form-label">Upload Your 7th Day Cube Test Report </label>
                                    <div class="cstm-input">
                                        <div class="file-browse">
                                            <div class="button-browse">
                                            Browse <input type="file" name="buyer_qube_test_report_7days" />
                                            </div>
                                            <input type="text" class="form-control browse-input" placeholder="e.g. jpg, jpeg, doc, docx, pdf" readonly>
                                        </div>
                                    </div>
                                </div>
                            <!-- </div> -->
                        </div>
                        <input type="hidden" name="track_id" id="report_7_track_id" value=""/>
                        <input type="hidden" name="TM_Id" id="report_7_tm_id" value=""/>
                        <input type="hidden" name="vendor_order_id" id="report_7_order_id" value=""/>
                        <input type="hidden" name="qube_test_report_7days_accept_reject" value="2"/>

                        <div class="clearfix"></div>

                    </div>
                    
                </div>
                <div class="modal-footer text-left">
                    <button class="btn btn-primary mt-3">Save</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-28-report-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- <div class="modal-header"> -->
          <!-- <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button> -->
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        <!-- </div> -->
        <div class="modal-body">
            <h5 class="title mb-4" id="add_28_report_title">Add 28th Day Cube Test Report</h5>
            <form action="" name="add_28_report_form" method="post">
                @csrf
                <div class="login_fild">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <!-- <div class="form-group"> -->
                                    <div class="mb-3">
                                        <label class="form-label">Reason Of Report Rejection </label>
                                        
                                        <input type="text" class="form-control" name="qube_test_report_28days_reject_reason" placeholder="e.g. Write reason of report rejection">
                                        
                                    </div>
                                <!-- </div> -->
                                <!-- </div>
                                <div class="col-md-12 col-sm-12 col-xs-12"> -->
                                <!-- <div class="form-group"> -->
                                    <div class="mb-3" id="pic_28_report_div">
                                        <label class="form-label">28th Day Cube Test Report Image </label>
                                        <div class="file-browse">
                                            <div class="button-browse">
                                            Browse <input type="file" name="buyer_qube_test_report_28days" />
                                            </div>
                                            <input type="text" class="form-control browse-input" placeholder="e.g. jpg only" readonly>
                                        </div>
                                    </div>
                                <!-- </div> -->
                            </div>
                            <input type="hidden" name="track_id" id="report_28_track_id" value=""/>
                            <input type="hidden" name="TM_Id" id="report_28_tm_id" value=""/>
                            <input type="hidden" name="vendor_order_id" id="report_28_order_id" value=""/>
                            <input type="hidden" name="qube_test_report_28days_accept_reject" value="2"/>
                            
                            <div class="clearfix"></div>

                        </div>
                    
                </div>
                <div class="modal-footer text-left">
                    <button class="btn btn-primary mt-3">Save</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

