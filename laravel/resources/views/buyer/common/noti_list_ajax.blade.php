<?php 

    $notification_types = array(

        // 12 => "Dear, {{NAME}} Truck has been assigned to your order of id #{{ORDER_ID}}",
        // 1 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Placed. It will be confirm soon.", //orderPlaced
        // 2 => "Dear, {{NAME}} for item #{{ORDER_ITEM}} Your order has been Processed.", //orderProcess 
        // 3 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Accepted by vendor.", //orderAccept 
        // 4 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Rejected by vendor.", //orderRejected 
        // 5 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Confirmed.", //orderConfirm
        // 6 => "Dear, {{NAME}} TM has been assigned to your order for item #{{ORDER_ITEM}}.", //truckAssigned
        // 7 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been picked up.", //orderPickup
        // 8 => "Dear, {{NAME}} Your Order item for item #{{ORDER_ITEM}} has been rejected.", //orderItemReject
        // 9 => "Dear, {{NAME}} Your Order for item for item #{{ORDER_ITEM}} has been rejected.", //orderReject
        // 10 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delayed.", //orderDelay
        // 11 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delivered partially.", //partiallyOrderDelivered
        // 12 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delivered.", //orderDelivered

        1 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been received & will be confirmed within 3 hours.", //orderPlaced
        2 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Processed.", //orderProcess 
        3 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Accepted by vendor name.", //orderAccept 
        4 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Rejected & thus cancelled. Your refund of Rs will be processed in 3 working days", //orderRejected 
        5 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Confirmed by vendor name.", //orderConfirm
        6 => "Transit Mixer has been assigned to your prduct id #{{ORDER_ITEM}}  for order no #{{ORDER_ID}}.", //truckAssigned
        7 => "Your Order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been picked up.", //orderPickup
        8 => "Your Order item with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been rejected & thus cancelled. Your refund of Rs will be processed in 3 working days", //orderItemReject
        9 => "Your Order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been rejected & thus cancelled. Your refund of Rs will be processed in 3 working days", //orderReject
        10 => "Transit Mixer with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} is on the way and delayed by 1 hours owing to Reason. Kindly bear with us", //orderDelay
        11 => "Your Order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been delivered partially.", //partiallyOrderDelivered
        12 => "Your Order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been delivered.", //orderDelivered

    );

?>
<ul class="header_noti">
    <?php //dd($data["data"]["notifications"]); ?>
    @if(isset($data["data"]["notifications"]) && count($data["data"]["notifications"]) > 0)
        @foreach($data["data"]["notifications"] as $value)
        
            <?php 
                $msg = "";

                if(isset($notification_types[$value["notification_type"]])){
                    $msg = $notification_types[$value["notification_type"]];
                }else{
                    $msg = "Notification message";
                }
                

                if(isset($value["to_user"]["full_name"])){
                    $msg = str_replace("{{NAME}}",$value["to_user"]["full_name"],$msg);
                }
                
                if(isset($value["order"]["display_id"])){
                    $msg = str_replace("{{ORDER_ID}}",$value["order"]["display_id"],$msg);
                }
                
                if(isset($value["order_item"]["display_item_id"])){
                    $msg = str_replace("{{ORDER_ITEM}}",$value["order_item"]["display_item_id"],$msg);
                }
                
                $created = date('d M Y', strtotime($value["created_at"]));

                
            ?>

            <li>            
                <a href="{{isset($value['order']['_id']) ? route('buyer_order_detail',$value['order']['_id']) : ''}}">                                        
                    {{$msg}} {{$created}}                                        
                </a>
            </li>
        @endforeach
    @else

        <li class="float-left w-100">
            
            Notifications Empty

        </li>       

    @endif
    
</ul>