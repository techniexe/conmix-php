<!doctype html>
<html lang="en">
<head>
      <!-- META -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="keywords" content="" />
      <meta name="author" content="" />
      <meta name="robots" content="" />
      <meta name="description" content="" />
      
      <!-- FAVICONS ICON -->
      <link rel="icon" type="image/webp" href="{{asset('assets/buyer/images/favicon-32x32.webp')}}" sizes="32x32" async/>
      <link rel="icon" type="image/webp" href="{{asset('assets/buyer/images/favicon-16x16.webp')}}" sizes="16x16" async/>
      
      <!-- PAGE TITLE HERE -->
      <title>ConMix | @yield('title')</title>
      <!-- MOBILE SPECIFIC -->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
      <!-- Roboto font -->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<!-- <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'"> -->
		<!-- fontawesome -->
		
      
      <!-- <link href="{{asset('assets/buyer/css/css_cache.php')}}" rel="stylesheet" type="text/css" /> -->
      <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/css_cache.php')}}" />
      @if(false)
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/common/css/shimmer.css')}}" async>
         <!-- BOOTSTRAP STYLE SHEET -->
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/google_fonts_css.css')}}" async>
         
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/bootstrap.css')}}" async>
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/bootstrap-multiselect.css')}}" async>
         
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/daterangepicker.css')}}" async>

         <!-- OWL CAROUSEL STYLE SHEET -->
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/owl.carousel.css')}}" async>
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/offcanvas.css')}}" async>
         <!-- MAIN STYLE SHEET -->
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/style.css')}}" async>
         
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/custom.css')}}" async>
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/buyer/css/responsive.css')}}" async>
         
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/common/css/jquery.toast.css')}}" async>
      
         <!-- SELECT2 CHOSEN STYLE SHEET -->
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/admin/css/select2-chosen.css')}}" async>

         <!-- STAR RATING STYLE SHEET -->
         <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/admin/css/star-rating-svg.css')}}" async>

         <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
      @endif

      <!-- <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/common/css/toastr.css')}}"> -->
      <script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDgc0AfDO-QPVxfx03eXhLBphZ3CKwYp5o&sensor=false&libraries=places&types=address' defer></script>
      <script type="text/javascript" src="{{asset('assets/buyer/js/js_cache.php')}}"></script>
      <!-- <link rel="preload" href="{{asset('assets/buyer/js/js_cache.php')}}" as="script" crossorigin="anonymous" /> -->
      <!-- <script type="text/javascript" src="{{asset('assets/admin/js/daterangepicker.min.js')}}"></script>
      <script src="{{asset('assets/buyer/js/bootstrap-datepicker.min.js') }}" ></script> -->

      @if(false)

         <!-- <script src="https://kit.fontawesome.com/f88acebe39.js" crossorigin="anonymous"></script> -->
         <script src="{{asset('assets/buyer/js/font_awesome.js')}}" crossorigin="anonymous"></script>

         <script type="text/javascript" src="{{asset('assets/buyer/js/jquery-3.5.1.min.js')}}"></script>

         <script type="text/javascript" src="{{asset('assets/admin/js/jquery.star-rating-svg.js')}}"></script>

         <script type="text/javascript" src="{{asset('assets/common/js/locationpicker.jquery.js')}}"></script>

         <script type="text/javascript" src="{{asset('assets/common/js/utils.js') }}"></script>

         <script type="text/javascript" src="{{asset('assets/buyer/js/buyer.js') }}"></script>

         <script type="text/javascript" src="{{asset('assets/admin/js/moment.min.js')}}"></script>
         
         
         
      @endif
        
      

      
      <!-- <script type="text/javascript" src="{{asset('assets/buyer/js/jspdf.js')}}"></script>
      <script type="text/javascript" src="{{asset('assets/buyer/js/from_html.js')}}"></script> -->
      

      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/rasterizehtml/1.3.0/rasterizeHTML.allinone.js" integrity="" crossorigin="anonymous"></script> -->
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script> -->
      
      
      

      
      

      <!-- STAR RATING STYLE SHEET -->
      

      
      

      <!-- Roboto font -->
		<!-- <link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet"> -->
		<!-- fontawesome -->
		<!-- <script src="https://kit.fontawesome.com/f88acebe39.js" crossorigin="anonymous"></script> -->

      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script> -->
   
      @if(env('APP_ENV', 'production') == 'production')

         <!-- Google tag (gtag.js) -->
         <script async src="https://www.googletagmanager.com/gtag/js?id=G-L18WBJ16M8"></script>
         <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'G-L18WBJ16M8');
         </script>

         <script type="text/javascript">
            (function(c,l,a,r,i,t,y){
               c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
               t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
               y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
            })(window, document, "clarity", "script", "m223lx17us");
         </script>

      @endif
   </head>
<script>

var BASE_URL = '{{ url('/') }}';
// console.log("BASE_URL..."+BASE_URL);

</script>