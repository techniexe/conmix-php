<!-- FOOTER START -->
@if((Route::current()->getName() != 'buyer_my_mobile_support_ticket'))
@if((Route::current()->getName() != 'buyer_ticket_mobile_ticket_detail'))
<footer>
@php
        $profile = session('buyer_profile_details', null);
    @endphp
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <!-- <div class="col-lg-3 col-sm-6 col-12">
                    <div class="logo_text">
                        <a href="#"><img src="{{asset('assets/buyer/images/footer_logo.png')}}" alt="conmix"></a>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    </div>
                </div> -->
                <div class="col-lg-4 col-sm-9 col-12">
                    <div class="footer_link">
                        <h5>About Us</h5>
                        <p>Conmix is a digital platform revolutionizing the construction industry's Ready-Mix Concrete (RMC) procurement process. Our platform connects buyers with verified suppliers, ensuring quality, reliability, and efficiency throughout the procurement journey.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-6">
                    <div class="footer_link">
                        <h5>POLICY</h5>
                        <p><a href="{{route('buyer_terms_of_use')}}">Terms of Usage</a></p>
                        <p><a href="{{route('buyer_privacy_policy')}}">Privacy Policy</a></p>
                        <p><a href="{{route('buyer_return_policy')}}">Cancellations, Returns, and Refund Policy</a></p>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-6 col-12">
                    <div class="footer_subscribe">
                        <h5>Contact Us</h5>
                        <p>Call Us On: <a href="tel:+919979016486"> +91 9979016486</a> </p>
                        <p>Write Us On: <a href="mailto:support@conmix.in" >support@conmix.in</a></p>
                    </div>
                    <div class="app_download_btn">
                        @if(!isset($profile))
                            <a href="{{ route('supplier') }}" class="sell_with_btn me-2">Sell RMC With US</a>
                        @endif

                        <!-- <a href="#" class="me-2"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{asset('assets/buyer/images/apple_play.webp')}}" defer alt="app icon"></a> -->
                        <a href="#"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{asset('assets/buyer/images/google_play.webp')}}" defer alt="app icon"></a>
                    </div>
                </div>
                <!-- <div class="col-lg-5 col-sm-6 col-12">
                    <div class="footer_subscribe">
                        <h5>SUBSCRIBE</h5>
                        <p>Subscribe to our newsletter, so that you can be the first to know about new offers and promotions.</p>
                        <div class="subscribe_box">
                            <input type="text" class="form-control" name="" placeholder="Enter Email Address">
                            <button type="button">SUBSCRIBE</button>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="footer_copyright">
        <div class="container">
            <div class="footer_copyright_inner">
                <p>Copyright © <?php echo date('Y'); ?> Conmix. All Rights Reserved.</p>
               
            </div>
        </div>
    </div>
</footer>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="floating-container">
  <div class="floating-button"><i class="material-icons">phone
      </i></div>
  <div class="element-container">
  
    
    <a href="tel:+919979016486">    
        <span class="float-element tooltip-left">         
            <i class="material-icons">phone
            </i>
        </span>
    </a>
    <a href="mailto:support@conmix.in">
      <span class="float-element">
            <i class="material-icons">email
            </i>
        </span>
    </a>
      <!-- <span class="float-element">
      <i class="material-icons">chat</i>
    </span> -->
  </div>
</div>

<!-- FOOTER END -->
@endif
@endif

<!-- Enquiry sticky form start -->

<!-- <div class="enquiryblock" id="EnquiryForm_ID">
	<div class="enquirytab">
		<p class="enquiryhorizontal">Post Requirement</p>
	</div>
	<div class="enquiryform">
        <div class="enquiryform_inner">
            <div class="enquiryform_header">
                <p>For any enquiry call</p>
                <h3>98989 14789</h3>
            </div>
            <div class="enquiryform_middel">
                <h3>Post Requirement</h3>
                <p>Fill the following details. Our executives will get back to you shortly.</p>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name" />
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" />
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Mobile No" />
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Delivery Location" />
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="yourmessage" placeholder="Please describe your requirements"></textarea>
                </div>
                <div class="form-group">
                    <a href="javascript:void(0);" class="site-button yellow btn-block text-center">Submit</a>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- Enquiry sticky form end -->

<!-- SCROLL TOP BUTTON -->
<!-- <button class="scroltop"><span class="fa fa-angle-up relative" id="btn-vibrate"></span>Top</button> -->



@include('buyer.common.popups')
@if(false)
<!-- JAVASCRIPT  FILES -->
<script type="text/javascript" src="{{asset('assets/common/js/jquery.validate.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/common/js/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/buyer/js/bootstrap.bundle.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/buyer/js/owl.carousel.js')}}"></script>

<!-- <script type="text/javascript" src="{{asset('assets/buyer/js/daterangepicker.js')}}"></script> -->

<script type="text/javascript" src="{{asset('assets/buyer/js/offcanvas.js')}}"></script>
    

    <script type="text/javascript" src="{{asset('assets/buyer/js/custome.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/buyer/js/bootstrap-multiselect.min.js')}}"></script>


    <!-- Toast -->
    <script type="text/javascript" src="{{asset('assets/common/js/jquery.toast.js')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('assets/common/js/toastr.min.js')}}"></script> -->

    <script type="text/javascript" src="{{asset('assets/admin/js/select2.jquery.min.js') }}"></script>
@endif


  <!-- LOADING AREA START -->
  <!-- <div class="loading-area">
      <div class="loading-box"></div>
      <div class="loading-pic">
        <div class="loader">
            <span class="block-1"></span>
            <span class="block-2"></span>
            <span class="block-3"></span>
            <span class="block-4"></span>
            <span class="block-5"></span>
            <span class="block-6"></span>
            <span class="block-7"></span>
            <span class="block-8"></span>
            <span class="block-9"></span>
            <span class="block-10"></span>
            <span class="block-11"></span>
            <span class="block-12"></span>
            <span class="block-13"></span>
            <span class="block-14"></span>
            <span class="block-15"></span>
            <span class="block-16"></span>
        </div>
      </div>
  </div> -->
  <!-- LOADING AREA  END -->


  <script>
//$('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
        // $( function() {
        //     setTimeout(removeLoader); //wait for page load PLUS two seconds.
        // });
        // function removeLoader(){
        //     $( "#loadingDiv" ).fadeOut(500, function() {
        //         // fadeOut complete. Remove the loading div
        //         $( "#loadingDiv" ).remove(); //makes page more lightweight
        //     });
        // }
    </script>


   </body>


<script>
//     $( function() {
//     var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
//     var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
//     return new bootstrap.Tooltip(tooltipTriggerEl)
//     });

//     $('.home_our_product .owl-carousel').owlCarousel({
//         loop: true,
//         margin:20,
//         nav: true,
//         dots: false,
//         items:4,
//         autoplay:true,
//         autoplayTimeout:3000,
//         navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
//         responsive:{
//             0:{
//                 items:2
//             },
//             600:{
//                 items:3
//             },
//             1000:{
//                 items:4
//             }
//         }
//     });
//     $('.home_our_client .owl-carousel').owlCarousel({
//         loop:false,
//         margin:10,
//         nav: true,
//         dots:false,
//         items:4,
//         navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
//         responsive:{
//             0:{
//                 items:2
//             },
//             600:{
//                 items:3
//             },
//             1000:{
//                 items:4
//             }
//         }
//     });
// });

// $( function() {
//     $(".enquirytab").on("click", function() {
//         $(".enquiryblock").toggleClass("open", 1000);
//     });

//     $('.form-control-chosen').chosen({
//         width: '100% !important'
//     });
// });



//   $(document).ready(function() {
    /*input mobile no with flag*/
    // var input = document.querySelector("#mobileno");
    // window.intlTelInput(input, {
    //   utilsScript: "{{asset('assets/admin/js/utils.js')}}" // just for formatting/placeholders etc
    // });
    /*input mobile no with flag end*/

    /*character limit script start*/
    //$('textarea#ctsubject, textarea#ctdescription').characterlimit();
    /*character limit script end*/

    /*drag N Drop file upload script start*/
    // $('input[name="files"]').fileuploader({changeInput:'<div class="fileuploader-input"><div class="fileuploader-input-inner"><img src="images/fileuploader-dragdrop-icon.png"><h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3><p>or</p><div class="site-button gray"><span>Browse Files</span></div></div></div>',theme:"dragdrop",upload:{url:"",data:null,type:"POST",enctype:"multipart/form-data",start:!0,synchron:!0,beforeSend:null,onSuccess:function(e,a){var n=JSON.parse(e);n.isSuccess&&n.files[0]&&(a.name=n.files[0].name),a.html.find(".column-actions").append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>'),setTimeout(function(){a.html.find(".progress-bar2").fadeOut(400)},400)},onError:function(e){var a=e.html.find(".progress-bar2");a.length>0&&(a.find("span").html("0%"),a.find(".fileuploader-progressbar .bar").width("0%"),e.html.find(".progress-bar2").fadeOut(400)),"cancelled"!=e.upload.status&&0==e.html.find(".fileuploader-action-retry").length&&e.html.find(".column-actions").prepend('')},onProgress:function(e,a){var n=a.html.find(".progress-bar2");n.length>0&&(n.show(),n.find("span").html(e.percentage+"%"),n.find(".fileuploader-progressbar .bar").width(e.percentage+"%"))},onComplete:null},onRemove:function(e){$.post("",{file:e.name})},captions:{feedback:"Drag and drop files here",feedback2:"Drag and drop files here",drop:"Drag and drop files here"}});
            /*drag N Drop file upload script end -- //<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>*/


    // $('input[name="attachments"]').fileuploader({
    //       limit: 20,
    //       maxSize: 50,
    //     });
// });


function showToast(message,type){

    var hideAfter = 1500;
    if(type.toLowerCase() == 'success'){
        hideAfter = 1500;
    }else{
        hideAfter = 2000;
    }

  $.toast({
    heading: ''+type,
    text: ''+message,
    showHideTransition: 'fade',
    icon: ''+type.toLowerCase(),
    loader: false,
    position: 'top-right',
    hideAfter: hideAfter
  });

//  bootoast.toast({
//     message: 'test',
//      type: ''+type.toLowerCase()
//   });

// toastr.options = {
//   "closeButton": false,
//   "debug": false,
//   "newestOnTop": false,
//   "progressBar": false,
//   "positionClass": "toast-top-center",
//   "preventDuplicates": false,
//   "onclick": null,
//   "showDuration": "300",
//   "hideDuration": "1000",
//   "timeOut": "5000",
//   "extendedTimeOut": "1000",
//   "showEasing": "swing",
//   "hideEasing": "linear",
//   "showMethod": "fadeIn",
//   "hideMethod": "fadeOut"
// }

// toastr[''+type.toLowerCase()](''+message);


}

function reload(){

  setTimeout(function() { location.reload(); }, 1000);

}

function confirmDialog(message, onConfirm){

  var fClose = function(){
    modal.modal("hide");
  };
  var modal = $("#confirmModal");
  modal.modal("show");
  $("#confirmMessage").empty().append(message);
  $("#confirmOk").unbind().one('click', onConfirm).one('click', fClose);
  $("#confirmCancel").unbind().one("click", fClose);

}

function otpDialog(onConfirm){

  var fClose = function(){
    modal.modal("hide");
  };
  var modal = $("#otpModal");
  modal.modal("show");

  $("#otpOk").unbind().one('click', onConfirm);
  $("#otpCancel").unbind().one("click", fClose);

}

function plantOTPDialog(onConfirm){

    var fClose = function(){
    modal.modal("hide");
    };
    var modal = $("#Plant_otpModal");
    modal.modal("show");

    $("#plant_otpOk").unbind().one('click', onConfirm);
    $("#plant_otpCancel").unbind().one("click", fClose);

}

@if(isset($data["error"]["message"]))

    @if($data["status"] = 401)
        // alert('okkk');
        // setTimeout(function() {
        //     $('#login_error').html('{{ $data["error"]["message"] }}');
        //     $("#Login-form").modal('show');
        // }, 500);

        @if($data["error"]["message"] != '')
            showToast('{{ $data["error"]["message"] }}','error');
        @endif

    @endif

@endif



</script>


<script>

// $( function() {
// $('#us2').locationpicker({
//     enableAutocomplete: true,
//     enableReverseGeocode: true,
//   radius: 0,
//   inputBinding: {
//     latitudeInput: $('#us2_lat'),
//     longitudeInput: $('#us2_lon'),
//     radiusInput: $('#us2-radius'),
//     locationNameInput: $('#us2_address')
//   },
//   panControl: true,
//         panControlOptions: {
//             position: google.maps.ControlPosition.RIGHT_CENTER
//         },
//   onchanged: function (currentLocation, radius, isMarkerDropped) {
//         var addressComponents = $(this).locationpicker('map').location.addressComponents;
//         console.log("test...");
//         console.log(addressComponents);
//         console.log("test...");
//         console.log(currentLocation);  //latlon
//     // updateControls(addressComponents); //Data
//     }
// });
// });

function updateControls(addressComponents) {
  console.log(addressComponents);
}

<?php
    if(session('data_change_loc') != null){

        $data_change_loc = session('data_change_loc');

        if(isset($data_change_loc["custom_mix_dialog_on"]) && $data_change_loc["custom_mix_dialog_on"] != null){
            if($data_change_loc["custom_mix_dialog_on"] == 'custom_mix_dialog_on'){
                if(isset($data_change_loc["custom_mix_save_id"])){
                //dd($data_change_loc["custom_mix_dialog_on"]);
                ?>
                    $("#custom_rmc_view_btn_".$data_change_loc["custom_mix_save_id"]).click();
                <?php
                }else{
                ?>
                    setTimeout(
                    function() {
                        $("#custom_mix_rmc_button").click();
                    }, 500);

                <?php

                }
            }
        }
    }
?>

$( function() {
  
  var url_param = getParamsForFilter(window.location.href);
  
  console.log("url_param..."+url_param);
  
  if(url_param == 'from_landing=yes'){
    console.log("url_param..."+url_param);
    // $(".clear_filter_button").hide();
    let stateObj = { id: "100" }; 
    window.history.replaceState(stateObj, 
                        "ConMix", location.protocol + '//' + location.host + location.pathname); 
  }else{
    // $(".clear_filter_button").show();
  }
  
  });

</script>


@if(Route::current()->getName() == 'buyer_cart_checkout')

@endif
</html>
