
<!-- HEADER START -->
@if((Route::current()->getName() != 'buyer_my_mobile_support_ticket'))
@if((Route::current()->getName() != 'buyer_ticket_mobile_ticket_detail'))
<header>
    @php
        $profile = session('buyer_profile_details', null);
    @endphp
    <div class="container">
        <div class="top_header">
            <div class="logo">
                <a href="{{ route('buyer_home') }}"><img src="{{asset('assets/buyer/images/logo.webp')}}" alt="conmix"></a><sup style="top: -1em;">BETA</sup>
                
            </div>
            <div class="top_header_right">
                <!-- <div class="header_search">
                    <div class="header_search_inner">
                        <input type="text" class="form-control" placeholder="Serch" name="">
                        <button type="button" class="header_search_btn"><i class="fas fa-search"></i></button>
                    </div>
                </div> -->
                <div class="header_right_menu d-flex">


                        <div class="dropdown">
                            @if(!isset($profile))
                                <a class="dropdown-toggle position-relative" onclick="resetForm('login-form','Login-form')" href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#Login-form" >
                                    <i class="fas fa-bell"></i>
                                    <span class="count">{{ isset($noti_count_data["data"]["unseen_message_count"]) ? ($noti_count_data["data"]["unseen_message_count"] > 9 ? '9+' : $noti_count_data["data"]["unseen_message_count"]) : 0 }}</span>
                                </a>
                            @else
                                <a class="dropdown-toggle position-relative" href="{{ route('buyer_notification') }}">
                                    <i class="fas fa-bell"></i>
                                    <span class="count">{{ isset($noti_count_data["data"]["unseen_message_count"]) ? ($noti_count_data["data"]["unseen_message_count"] > 9 ? '9+' : $noti_count_data["data"]["unseen_message_count"]) : 0 }}</span>
                                </a>
                            @endif
                            <div class="dropdown-menu header_cart dropdown-menu-end py-3" aria-labelledby="dropdownMenuLink1">


                            <div class="wishlist-dropdown-item-wraper clearfix">
                                <div class="nav-cart-content">
                                    <div id="noti_list_div">
                                        <!-- <div class="overlay">
                                            <span class="spinner"></span>
                                        </div> -->

                                        <!-- <ul class="header_noti">
                                            <li class="unseen">
                                                <a href="#">
                                                    <h6>ORDER PROCESS <small>17 hours ago</small></h6>
                                                    <p>Dear, Dhwani shah Your order for item #53824555555 has been Processed</p>
                                                </a>
                                            </li>
                                            <li class="unseen">
                                                <a href="#">
                                                    <h6>ORDER PROCESS <small>17 hours ago</small></h6>
                                                    <p>Dear, Dhwani shah Your order for item #53824555555 has been Processed</p>
                                                </a>
                                            </li>
                                            <li class="unseen">
                                                <a href="#">
                                                    <h6>ORDER PROCESS <small>17 hours ago</small></h6>
                                                    <p>Dear, Dhwani shah Your order for item #53824555555 has been Processed</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <h6>ORDER PROCESS <small>17 hours ago</small></h6>
                                                    <p>Dear, Dhwani shah Your order for item #53824555555 has been Processed</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <h6>ORDER PROCESS <small>17 hours ago</small></h6>
                                                    <p>Dear, Dhwani shah Your order for item #53824555555 has been Processed</p>
                                                </a>
                                            </li>
                                        </ul> -->

                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <a href="#" class="btn btn-primary">View all</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                        </div>


                    <div class="dropdown header_cart_drop">
                        @if(!isset($profile))
                            <a class="dropdown-toggle position-relative" onclick="resetForm('login-form','Login-form')" href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#Login-form" >
                                <i class="fas fa-shopping-cart"></i> <span class="count">0



                                </span>
                            </a>
                        @else
                            <a class="dropdown-toggle position-relative" href="{{ route('buyer_cart_checkout') }}" role="button" id="dropdownMenuLink">
                                <i class="fas fa-shopping-cart"></i>
                                <span class="count" id="cart_item_count">
                                    @if(isset($cart_details["data"]["items"]))
                                        <?php
                                            if(count($cart_details["data"]["items"]) > 0){
                                                echo count($cart_details["data"]["items"]);
                                            }else{
                                                echo 0;
                                            }
                                        ?>
                                    @else

                                    0

                                    @endif

                                </span>
                            </a>
                        @endif
                        <div class="dropdown-menu header_cart dropdown-menu-end p-3" id="cart_content" aria-labelledby="dropdownMenuLink">

                            @if(isset($cart_details["data"]) && count($cart_details["data"]) > 0)
                                @if(isset($cart_details["data"]["items"]) && count($cart_details["data"]["items"]) > 0)
                                    <div class="cart_menu">

                                            @foreach($cart_details["data"]["items"] as $value)
                                                @if(isset($value["design_mix_id"]))
                                                <!-- Design Mix -->
                                                    <div class="cart_menu_item d-flex mb-3 pb-3">
                                                        <div class="cart_menu_item_img">
                                                        @if(isset($value["vendor_media"]) && !empty($value["vendor_media"]))
                                                            @foreach($value["vendor_media"] as $media_value)
                                                                @if($media_value["type"] == "PRIMARY")
                                                                    <img src="{{$media_value['media_url']}}" alt="item">
                                                                @endif
                                                            @endforeach
                                                        @else
                                                        <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="" alt="item">
                                                        @endif
                                                        </div>
                                                        <div class="cart_menu_item_dtl">
                                                            <h6 class="mb-1">{{ $value["concrete_grade_name"] }}</h6>
                                                            <p> <span>{{ $value["company_name"] }}</span></p>
                                                            <p class="mb-1">Qty : {{ $value["quantity"] }} Cu.Mtr</p>
                                                            <h6 class="mb-1">₹{{ number_format($value["selling_price_With_Margin"],2) }} <small class="text-danger">(Excl. GST)</small></h6>
                                                            <a href="javascript:void(0)" onclick="deleteCartItem('{{ csrf_token() }}','{{ $value["_id"] }}','cartslide_{{ $value["_id"] }}','{{ count($cart_details["data"]["items"]) }}','slide_cart')" ><i class="far fa-trash-alt"></i></a>
                                                        </div>
                                                    </div>
                                                @else
                                                <!-- Custom Mix -->

                                                @if(isset($value["custom_mix"]))
                                                <?php $custom_mix = $value["custom_mix"]

                                                ?>
                                                    <div class="cart_menu_item d-flex mb-3 pb-3">
                                                        <div class="cart_menu_item_img">
                                                            @if(isset($custom_mix["vendor_media"]) && !empty($custom_mix["vendor_media"]))
                                                                @foreach($custom_mix["vendor_media"] as $media_value)
                                                                    @if($media_value["type"] == "PRIMARY")
                                                                        <img src="{{$media_value['media_url']}}" alt="item">
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="" alt="item">
                                                            @endif
                                                        </div>
                                                        <div class="cart_menu_item_dtl">
                                                            <h6 class="mb-1">{{ isset($custom_mix["concrete_grade_name"]) ? $custom_mix["concrete_grade_name"] : '' }}</h6>
                                                            <p> <span>{{ isset($custom_mix["company_name"]) ? $custom_mix["company_name"] : '' }}</span></p>
                                                            <p class="mb-1">Qty : {{ $value["quantity"] }} Cu.Mtr</p>
                                                            <h6 class="mb-1">₹{{ number_format($value["selling_price_With_Margin"],2) }} <small class="text-danger">(Excl. GST)</small></h6>
                                                            <a href="javascript:void(0)" onclick="deleteCartItem('{{ csrf_token() }}','{{ $value["_id"] }}','cartslide_{{ $value["_id"] }}','{{ count($cart_details["data"]["items"]) }}','slide_cart')" ><i class="far fa-trash-alt"></i></a>
                                                        </div>
                                                    </div>
                                                @endif

                                                @endif

                                            @endforeach

                                        <div class="cart_menu_total d-flex justify-content-between align-items-center">
                                            <p class="mb-0">Cart Total :</p>
                                            <h6 class="mb-0">₹ {{ number_format($cart_details["data"]["total_amount"],2) }} <small class="text-success">(Inc. GST)</small></h6>
                                        </div>
                                        <!-- <div class="d-flex justify-content-evenly">
                                            <a href="{{ route('buyer_cart_checkout') }}" class="btn btn-primary btn-secondary mb-1">VIEW CART</a>

                                        </div> -->
                                    </div>
                                @else
                                <div class="empty_cart">Your cart is currently empty.</div>
                                @endif
                            @else
                                <div class="empty_cart">Your cart is currently empty.</div>
                            @endif
                        </div>
                    </div>
                    <div class="login_sign">
                        @if(!isset($profile))
                            <a href="javascript:void(0);" onclick="resetForm('login-form','Login-form')" data-bs-toggle="modal" data-bs-target="#Login-form"><i class="far fa-user-circle"></i> Log In / Register</a>
                        @else
                            <?php //dd($profile); ?>
                            <input type="hidden" id="OTP_mob_number" value="{{ isset($profile["mobile_number"]) ? $profile["mobile_number"] : '' }}"/>
                            <div class="dropdown">
                                <!-- <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false"><i class="far fa-user-circle"></i> My Profile</a> -->
                                <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="far fa-user-circle"></i>
                                    <?php //dd($profile); ?>
                                    @if(isset($profile["account_type"]) && $profile["account_type"] == "Individual")
                                        @if(isset($profile["company_name"]))
                                            {{ $profile["company_name"] }}
                                        @else
                                            {{ $profile["full_name"] }}
                                        @endif
                                    @else
                                        {{ $profile["full_name"] }}
                                    @endif

                                </a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                    <li><a class="dropdown-item" href="{{ route('buyer_profile') }}">My Account</a></li>
                                    <li><a class="dropdown-item" href="{{ route('buyer_order') }}">My Order</a></li>
                                    <li><a class="dropdown-item" href="{{ route('buyer_my_support_ticket') }}">Customer Support</a></li>
                                    <li><a class="dropdown-item" href="{{ route('buyer_logout') }}">Logout</a></li>
                                </ul>
                            </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_menu">
        <div class="container">
            <div class="header_menu_inner">
                <ul class="header_menu_item">
                    <?php 
                        $rmc_info = "This is a standard RMC with default design mix parameters as available and set by RMC supplier. If you want to purchase your own design mix RMC then choose custom RMC option."
                    ?>                     
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            RMC 
                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{ $rmc_info }}"><i class="fas fa-info-circle"></i></a>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            @if(isset($concrete_grade["data"]) && count($concrete_grade["data"]) > 0)
                            @foreach($concrete_grade["data"] as $value)
                            <?php //dd($value); ?>
                                <li>
                                @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null) && (Session::get("selected_display_delivery_date") != null))
                                    <a class="dropdown-item" href="{{ route('buyer_product_listing',$value['_id']) }}">{{ $value["name"] }}</a>
                                @else
                                    <a class="dropdown-item" href="javascript:void(0)" onclick="openLocationDialog('{{ $value['_id'] }}','product_listing')">{{$value["name"]}}</a>
                                @endif
                                </li>
                                @endforeach
                            @endif
                            <!-- <li><a class="dropdown-item" href="#">M20</a></li>
                            <li><a class="dropdown-item" href="#">M30</a></li>
                            <li><a class="dropdown-item" href="#">M40</a></li> -->
                        </ul>
                    </li>


                    @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null) && (Session::get("selected_display_delivery_date") != null))
                    <?php //dd(session('data_change_loc')); ?>
                        <li class="header_cust_rmc">
                            <a href="javascript:void(0)" onclick="openCustomMixDialog(0)" id="custom_mix_rmc_button">Custom RMC</a>
                        </li>

                    @else

                        <li class="header_cust_rmc">
                            <a href="javascript:void(0)" onclick="openLocationDialog('custom_mix_dialog_on','change_location')">Custom RMC</a>
                        </li>

                    @endif




                </ul>
                <div class="header_menu_right">
                    <a href="javascript:void(0)" onclick="openLocationDialog('','change_location')"><i class="fas fa-map-marker-alt"></i>

                    @if((Session::get("is_site_selected") != null) && (Session::get("is_site_selected") == '1' ) )

                        <?php
                            if(isset($all_site_arr[Session::get("selected_site_address")])){
                                $site_detail = $all_site_arr[Session::get("selected_site_address")];
                                // dd($site_detail);
                                // echo $site_detail["company_name"] .' , '.$site_detail["site_name"] .' , '. $site_detail["state_name"];
                                echo $site_detail["site_name"] .' , '.$site_detail["address_line2"].' , '.$site_detail["city_name"] .' , '. $site_detail["state_name"];
                            }
                        ?>



                        @else

                        @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null) && (Session::get("selected_display_delivery_date") != null))
                            @if((Session::get("selected_state_name") != '') && (Session::get("selected_city_name") != '') && (Session::get("selected_display_delivery_date") != ''))

                                {{ Session::get("selected_city_name") }}, {{ Session::get("selected_state_name") }}
                            @else
                                Choose Location
                            @endif

                        @else
                            Choose Location
                        @endif

                    @endif

                    <i class="fas fa-angle-down"></i></a>
                    <!-- @if(!isset($profile))
                        <a href="{{ route('supplier') }}" target="_blank" class="sell_with_btn">SELL WITH US</a>
                    @endif -->
                </div>
            </div>
        </div>
    </div>
</header>
@endif
@endif
    <!-- HEADER END -->
