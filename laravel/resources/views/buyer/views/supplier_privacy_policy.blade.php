@section('title', 'Supplier Privacy Policy')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Supplier Privacy Policy</span>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">
                <div class="mb-2">
                    <h4>Supplier Privacy Policy for Supplier Management Software </h4>
                    <p>Last Updated:  19th April 2024</p>
                    <p>ConMix - A product of Conmate E-Commerce Pvt. Ltd.</p>
                    <p>ConMix ("us", "we", or "our") operates the ConMix platform, which consists of a website and mobile application (the "Service"). This policy outlines how we collect, use, protect and disclose personal data when you use our Service. By accessing or using the ConMix platform as a registered user as a RMC ‘Supplier’ then you shall also be using our RMC Order Management Software. Thus, by accessing or using ConMix platform you agree to the terms of this Privacy Policy as under:
</p>
                </div>
                <div class="mb-2">
                    <h6>1. Information We Collect</h6>

                    <h6>Personal & Business Information:</h6>
                    <p>When you register with ConMix as a RMC Supplier, wherein you may also use its RMC Order Management Software, we may collect personal information such as your Name, Permanent Account Number, Bank Account details, Registered Address, RMC Plant Address, phone contact number, contact emails, vehicles and assets detail provided by you, manpower details provided by you and other details of your company.

                    </p>
                    <h6>Login Credentials</h6>
                    <p>We collect login credentials (username and password) to authenticate your access to the ConMix platform.

                    </p>
                    <h6>Order Information</h6>
                    <p>We will collect all information about RMC orders placed and received on the ConMix platform, including complete order details, delivery schedules, and payment information.
                    </p>
                    <h6>Usage Data</h6>
                    <p>We may collect usage data such as IP addresses, device information, and browsing activity within the ConMix platform (ConMix Website, ConMix Mobile Application and ConMix Tracker Application)  for analytics and our product and service improvement purposes.

                    </p>
                    
                </div>
                <div class="mb-2">
                    <h6>2. How We Use Your Information</h6>
                    <h6>To Provide Services:</h6>
                    <p>We use the information collected to provide you with access to our ConMix platform including RMC Order Management Software and its features, including order management and RMC plant management modules.


                    </p>
                    <h6>To Improve Our Services:</h6>
                    <p>We may analyse usage data to understand how users and registered RMC Suppliers interact with the ConMix platform including its RMC Order Management Software and to identify areas for improvement of our Product and Services.

                    </p>
                    <h6>To Communicate with You:</h6>
                    <p>We may use your contact information to communicate with you about updates, new features, or important notices related to the software and also to share as leads to our other business associates.
                    </p>
                    

                </div>
                <div class="mb-2">
                    <h6>3. Data Security</h6>
                    <h6>Encryption:</h6>
                    <p>We implement industry-standard encryption protocols to secure your personal information and data transmitted through the software.

                    </p>
                    
                    <h6>Access Controls:</h6>
                    <p>Access to your personal information is restricted to authorised personnel only, and strict access controls are enforced to prevent unauthorised access.
                    </p>
                    
                    <h6>Data Storage:</h6>
                    <p>Your information is stored securely on servers with appropriate security measures in place to prevent unauthorised access or data breaches.
                    </p>
                    
                    <p>However, please be aware that no method of transmission over the internet or electronic storage is 100% secure, and hence we cannot guarantee its absolute security whatsoever.
                    </p>
                    
                    
                    

                </div>
                <div class="mb-2">
                    <h6>4. Third-Party Services</h6>
                    <h6>Integration Partners</h6>
                    <p>We may integrate with third-party services or platforms to enhance the functionality of our ConMix platform and its software. In such cases, your information may be shared with these third parties strictly for the purpose of providing the integrated services. 

                    </p>
                    
                    <h6>Data Sharing</h6>
                    <p>We do not sell or share your personal information with third parties for general marketing purposes without your consent.However, we may share the same as leads to our business associate.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>5. Your Rights</h6>
                    <h6>Access and Correction:</h6>
                    <p>You have the right to access and update your personal information stored in the ConMix Supplier account including its RMC Order Management Software.

                    </p>
                    
                    <h6>Data Deletion:</h6>
                    <p>You can request the deletion of your personal information from our systems post deletion of your account with ConMix permanently, subject to any legal obligations or legitimate interests we may have in retaining the data.

                    </p>
                    
                    <h6>Opt-Out:</h6>
                    <p>You have the right to opt-out of receiving promotional communications from us by following the unsubscribe instructions provided in the communication.
                    </p>

                </div>
                <div class="mb-2">
                    <h6>6. Policy Changes</h6>
                    <h6>Updates:</h6>
                    <p>We reserve the right to update or modify this Privacy Policy at any time. Any changes will be communicated to you on your registered email id. We may notify you of any changes by posting or publishing the new Privacy Policy on this page on our website. However, you are advised to visit the website page to review the Privacy Policy periodically for any changes


                    </p>
                    <h6>Effective Date:</h6>
                    <p>This Privacy Policy is effective as of the last updated date indicated above.

                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>7. Contact Us</h6>
                    <p>If you have any questions or concerns about our Privacy Policy or the use of your personal information, please contact us at <a href="mailto:support@conmix.in">support@conmix.in</a> .

                    </p>
                    
                </div>
                <!-- <div class="mb-2">
                    <h6>Lorem Ipsum is simply dummy</h6>
                    <ul class="mb-3">
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>Policy</h6>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div> -->
            </div>
        </div>
    </section>
</div>
<!-- CONTENT END -->

@endsection