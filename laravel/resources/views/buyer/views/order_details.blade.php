@section('title', 'Order Details')
    @extends('buyer.layouts.buyer_layout')

    @section('content')
    <?php use App\Http\Controllers\Admin\AdminController;?>
    <!-- CONTENT START -->
    <div class="page-content">
        <section class="breadcrumbs-fs">
            <div class="container-fluid">
                <div class="breadcrumbs my-4">
                    <a href="{{ route('buyer_home') }}">Home</a>
                    <span>Order</span>
                </div>
            </div>
        </section>
        <!-- SECTION CONTENT -->
        <section>
            <div class="container-fluid">
                <div class="section-content wht_bx p-4 mb-5">
                    <div class="order-list-block" id="order_details_div">
                       @if(isset($data["data"]))
                       <?php $final_data = $data["data"];
                       $progress_per = 0;
                        // dd($final_data);
                       ?>
                            @if(isset($final_data["orderData"]))
                            <?php
                            $order_value = $final_data["orderData"];

                            $order_id = $order_value["_id"];
                            $order_status = $order_value["order_status"];
                            $base_amount = isset($order_value["selling_price_With_Margin"]) ? $order_value["selling_price_With_Margin"] : 0;
                            $gst_amount = isset($order_value["gst_price"]) ? $order_value["gst_price"] : 0;
                            $total_amount = isset($order_value["total_amount"]) ? $order_value["total_amount"] : 0;
                            // dd($order_value["order_status"]);

                            ?>
                                    <input type="hidden" id="order_detail_vendor_id" />
                                    <input type="hidden" id="order_detail_item_qty" value="0"/>
                                    <div class="order-list-head-block">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div class="ordlst-id-date-block">
                                                    <div class="ordlst-id-block">
                                                        Order Id:
                                                        <span class="color_orange">#{{ $order_value["display_id"] }}</span>
                                                        @if($order_status == 'DELIVERED')
                                                            <span class="badge bg-success">{{ $order_status }}</span>
                                                        @else @if($order_status == 'CANCELLED' || $order_status == 'LAPSED' || $order_status == 'REJECTED')
                                                            <span class="badge bg-danger">{{ $order_status }}</span>
                                                            @if($order_status == 'LAPSED')
                                                                (Qty not assigned)
                                                            @endif

                                                        @else
                                                            <span class="badge bg-warning text-dark">{{ str_replace('_',' ',$order_status) }}</span>
                                                            
                                                            
                                                        @endif
                                                        @endif

                                                        <div class="">
                                                            <div class="progress_form mb-3 mt-2">
                                                                <div id="custom_mix_progress" class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ordlst-date-block">
                                                        Order Date: <span>{{ isset($order_value["created_at"]) ? AdminController::dateTimeFormat($order_value["created_at"]) : '' }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div class="ordlst-ship-dlvry-date-block">
                                                    <!-- <div class="ordlst-sd-text-block">
                                                        Delivery To: <span>Builders Mart</span>
                                                    </div> -->
                                                    @if(isset($order_value["delivered_at"]))
                                                        <div class="ordlst-sd-text-block">
                                                            Delivery Date: <span>{{ date('d M Y h:i:s a', strtotime($order_value["delivered_at"])) }}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div class="ordlst-hlink-block">
                                                    <div class="dropdown order_dtl_drop">
                                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="fas fa-ellipsis-v"></i>
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
                                                            <li><a class="dropdown-item" href="#">Download Invoice </a></li>
                                                            <li><a class="dropdown-item" href="#">Cancel Order</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>

                                    <div class="order-list-middle-top-block">
                                        <div class="ordlst-details-block">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-4 col-xs-12">
                                                    <div class="olstd-site-name-block">
                                                        <h3>Delivery Address :</h3>
                                                        <h2>{{ $order_value["site_name"] }}</h2>
                                                        <p>
                                                            {{ $order_value["address_line1"] }},<br/>
                                                            {{ $order_value["address_line2"] }},<br/>
                                                            {{ $order_value["city_name"] }} {{ $order_value["pincode"] }},
                                                            {{ $order_value["state_name"] }},
                                                            ({{ $order_value["country_name"] }})
                                                            <!-- 104, Sumel II, Near GuruDwara, SG Highway Ahmedabad 380054, Gujarat - (INDIA) -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-4 col-xs-12">
                                                    <div class="olstd-payment-block">
                                                        <h3>Payment : </h3>
                                                        <p>
                                                            <!-- Pay on Delivery (Cash/Card). Cash on delivery (COD) available. Card/Net banking acceptance subject to device availability. -->
                                                            {{ isset($order_value["payment_mode"]) ? $order_value["payment_mode"] : '' }}
                                                        </p>
                                                        @if(isset($order_value["gateway_transaction_id"]))
                                                            <p>
                                                                <!-- Pay on Delivery (Cash/Card). Cash on delivery (COD) available. Card/Net banking acceptance subject to device availability. -->
                                                                (Transaction Id: {{ $order_value["gateway_transaction_id"] }} )
                                                            </p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-4 col-xs-12">
                                                    <div class="olstd-btn-block text-end">
                                                        @if($order_status == 'DELIVERED')
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-secondary" onclick="resetForm('feedback-form','feedback-form')" data-bs-toggle="modal" data-bs-target="#feedback-popup">Feedback</a>
                                                        @endif
                                                        <!-- <a href="#" class="btn btn-primary btn-secondary" data-toggle="modal" data-target="#complain-popup">Complain</a> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            @endif

                            @if(isset($final_data["orderItemData"]) && !empty($final_data["orderItemData"]))
                                <div class="order-list-middle-block order_list_tbl order_dtl_tbl">
                                    <div class="table-responsive">
                                        <table class="table table-bordered mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="ord-wd-10 text-nowrap">Image</th>
                                                    <th class="text-nowrap">Product Id</th>
                                                    <th class="text-nowrap">Design Mix</th>
                                                    <th>Delivery Range</th>
                                                    <th class="text-nowrap">Total Qty (Cu.Mtr)</th>
                                                    <th class="tble_heding_color_red text-nowrap">Pending Qty<br/>To Be Assigned</th>
                                                    <th class="tble_heding_color_red text-nowrap">Balance Qty<br/>To Be Delivered</th>
                                                    <th class="tble_heding_color_green text-nowrap">Delivered Qty</th>
                                                    <th class="text-nowrap">Unit Price ( <i class="fa fa-rupee"></i> / Cu.Mtr )</th>
                                                    <th class="text-nowrap">TM & CP </th>
                                                    <!-- <th class="text-nowrap">TM Price ( <i class="fa fa-rupee"></i> )</th>
                                                    <th class="text-nowrap">CP Price ( <i class="fa fa-rupee"></i> )</th> -->
                                                    <!-- <th class="text-nowrap">Review</th> -->
                                                    <th class="text-nowrap">Track</th>
                                                    <th class="text-nowrap">Product Status</th>
                                                    <th class="text-nowrap">Payment Status</th>
                                                    <th class="text-end text-nowrap">Amount ( <i class="fas fa-rupee-sign"></i> )</th>
                                                    <th class="text-nowrap">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $total_item = count($final_data["orderItemData"]);
                                                    $total_devide_percentage = 0;
                                                    if($total_item > 0){
                                                        $total_devide_percentage = 100 / $total_item;
                                                    }
                                                    $is_with_cp = 0;
                                                ?>
                                                @foreach($final_data["orderItemData"] as $item_value)
                                                <?php //dd($item_value); ?>
                                                    @if($item_value["with_CP"] == true)

                                                        <?php 
                                                            $is_with_cp = 1;
                                                        ?>
                                                    @endif
                                                <tr>
                                                    <td>
                                                        <div class="ordrlst-img-block">
                                                        <?php
                                                            $primary_img = "";

                                                            if(isset($item_value["vendor_media"])){
                                                                foreach($item_value["vendor_media"] as $media_value){
                                                                    if($media_value["type"] == "PRIMARY"){
                                                                        $primary_img = $media_value["media_url"];
                                                                    }

                                                                }
                                                            }

                                                        ?>
                                                        @if($primary_img == "")
                                                            <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                                        @else
                                                            <img src="{{ $primary_img }}" alt="" />
                                                        @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        #{{ isset($item_value['display_item_id']) ? $item_value['display_item_id'] : '' }}

                                                    </td>
                                                    <td>
                                                        <!-- {{ isset($item_value['concrete_grade']) ? $item_value['concrete_grade']['name'] : '' }} - {{ isset($item_value['design_mix']["product_name"]) ? $item_value['design_mix']["product_name"] : 'Custom Mix' }} -->
                                                    <div class="dropdown tm_info_dropdown custom_rmc_dropdown">
                                                        <a href="#" class="dropdown-toggle site-button gray button-sm text-nowrap" data-bs-toggle="dropdown" aria-expanded="true"><span class="dropdown-label">
                                                            {{ isset($item_value['concrete_grade']) ? $item_value['concrete_grade']['name'] : (isset($item_value['concrete_grade_name']) ? $item_value['concrete_grade_name'] : '') }} - {{ isset($item_value['design_mix']["product_name"]) ? $item_value['design_mix']["product_name"] : (isset($item_value["product_name"]) ? $item_value["product_name"] : 'Custom Mix') }} <i class="fa fa-sort-down"></i></span></a>
                                                            <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <div class="custom_rmc_bx">
                                                                    <h4 class="mt-2 mb-1">Supplier : {{ isset($item_value['vendor']['vendor_detail']['company_name']) ? $item_value['vendor']['vendor_detail']['company_name'] : '' }}</h4>
                                                                    <p class="small">
                                                                        <span>
                                                                            <i class="fas fa-map-marker-alt" aria-hidden="true"></i> 
                                                                            {{ isset($item_value['line1']) ? $item_value['line1'] : '' }},
                                                                            {{ isset($item_value['line2']) ? $item_value['line2'] : '' }},
                                                                            {{ isset($item_value['city_name']) ? $item_value['city_name'] : '' }} - {{ isset($item_value['pincode']) ? $item_value['pincode'] : '' }},
                                                                            {{ isset($item_value['state_name']) ? $item_value['state_name'] : '' }}
                                                                        </span>
                                                                    </p>
                                                                    <div class="custom_rmc_info">
                                                                        <h5>{{ isset($item_value['concrete_grade']) ? $item_value['concrete_grade']['name'] : (isset($item_value['concrete_grade_name']) ? $item_value['concrete_grade_name'] : '') }} - {{ isset($item_value['design_mix']["product_name"]) ? $item_value['design_mix']["product_name"] : (isset($item_value["product_name"]) ? $item_value["product_name"] : 'Custom Mix') }}</h5>
                                                                        <div class="row">
                                                                            <div class="col-sm-6">
                                                                                <p>Cement ( Kg ) : <span>{{ isset($item_value['cement_quantity']) ? $item_value['cement_quantity'] : '' }}</span></p>
                                                                                <p>Coarse Sand ( Kg ) : <span>{{ isset($item_value['sand_quantity']) ? $item_value['sand_quantity'] : '' }}</span></p>
                                                                                <p>Fly Ash ( Kg ) : <span>{{ isset($item_value['fly_ash_quantity']) ? $item_value['fly_ash_quantity'] : '' }}</span></p>
                                                                                <p>Admixture ( Kg ) : <span>{{ isset($item_value['admix_quantity']) ? $item_value['admix_quantity'] : '' }}</span></p>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <p>{{ isset($value["aggregate1_sub_cat"]["sub_category_name"]) ? $value["aggregate1_sub_cat"]["sub_category_name"] : (isset($item_value["aggregate1_sub_cat_name"]) ? $item_value["aggregate1_sub_cat_name"] : '-') }} ( Kg ) : <span>{{ isset($item_value['aggregate1_quantity']) ? $item_value['aggregate1_quantity'] : '' }}</span></p>
                                                                                <p>{{ isset($value["aggregate2_sub_cat"]["sub_category_name"]) ? $value["aggregate2_sub_cat"]["sub_category_name"] : (isset($item_value["aggregate2_sub_cat_name"]) ? $item_value["aggregate2_sub_cat_name"] : '-') }} ( Kg ) : <span>{{ isset($item_value['aggregate2_quantity']) ? $item_value['aggregate2_quantity'] : '' }}</span></p>
                                                                                <p>Water ( Ltr ) : <span>{{ isset($item_value['water_quantity']) ? $item_value['water_quantity'] : '' }}</span></p>
                                                                                <p>Grade : <span>{{ isset($item_value['concrete_grade']) ? $item_value['concrete_grade']['name'] : (isset($item_value['concrete_grade_name']) ? $item_value['concrete_grade_name'] : '') }}</span></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <div class="custom_rmc_dec"> -->
                                                                    <div class="custom_rmc_info">
                                                                        <b>Design Mix Description</b>
                                                                        <div class="row">
                                                                            
                                                                            <div class="col-sm-12">
                                                                                <p>Cement Brand : <span>{{ isset($item_value["cement_brand"]["name"]) ? $item_value["cement_brand"]["name"] : (isset($item_value["cement_brand_name"]) ? $item_value["cement_brand_name"] : '-') }}</span></p>
                                                                                <p>Coarse Sand Source : <span>{{ isset($item_value["sand_source"]["sand_source_name"]) ? $item_value["sand_source"]["sand_source_name"] : (isset($item_value["sand_source_name"]) ? $item_value["sand_source_name"] : '-') }}</span></p>
                                                                                <p>Coarse Sand Zone : <span>{{ (isset($item_value["sand_zone_name"]) ? $item_value["sand_zone_name"] : '-') }}</span></p>
                                                                                <p>Aggregate Source : <span>{{ isset($item_value["aggregate_source"]["aggregate_source_name"]) ? $item_value["aggregate_source"]["aggregate_source_name"] : (isset($item_value["agg_source_name"]) ? $item_value["agg_source_name"] : '-') }}</span></p>
                                                                                <!-- <p>Admixture Brand : <span>{{ isset($item_value["admix_brand"]["name"]) ? $item_value["admix_brand"]["name"] : (isset($item_value["admix_brand"]) ? $item_value["admix_brand"] : '-') }} - {{ isset($item_value["admix_cat"]["category_name"]) ? $item_value["admix_cat"]["category_name"] : (isset($item_value["admix_category_name"]) ? $item_value["admix_category_name"] : '-') }}</span></p> -->
                                                                                <p>Admixture Brand : <span>{{ isset($item_value["ad_mixture1_brand_name"]) ? $item_value["ad_mixture1_brand_name"] : '-' }} - {{ isset($item_value["ad_mixture1_category_name"]) ? $item_value["ad_mixture1_category_name"] : '-' }}</span></p>
                                                                                <p>Fly Ash Source Name : <span>{{ isset($item_value["fly_ash_source"]["fly_ash_source_name"]) ? $item_value["fly_ash_source"]["fly_ash_source_name"] : (isset($item_value["fly_ash_source_name"]) ? $item_value["fly_ash_source_name"] : '-') }}</span></p>
                                                                                <p>Water : <span>{{ isset($item_value["water_type"]) ? $item_value["water_type"] : 'Regular' }}</span></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td>
                                                            {{ date('d M Y',strtotime($item_value["delivery_date"])) }} to
                                                            {{ date('d M Y',strtotime($item_value["end_date"])) }}

                                                    </td>
                                                    <td>
                                                        <div class="qty-text-block">
                                                            <span>{{ $item_value['quantity'] }}</span> <br/>
                                                            @if(isset($item_value['part_quantity']))
                                                                <?php
                                                                    $today_date = strtotime(date("Y-m-d"));
                                                                    $delivery_date = strtotime($item_value["delivery_date"]);
                                                                    $end_date = strtotime($item_value["end_date"]);

                                                                    // dd($delivery_date."....".$end_date);
                                                                    $is_show_assign_btn = false;
                                                                    if($item_value["item_status"] == 'PROCESSING'){

                                                                        if(isset($item_value["vendor_order"]["order_status"])){

                                                                            if(($item_value["vendor_order"]["order_status"] != 'RECEIVED')){

                                                                                $is_show_assign_btn = true;

                                                                                if(($item_value["vendor_order"]["order_status"] == 'REJECTED')){
                                                                                    $is_show_assign_btn = false;
                                                                                }
                                                                                
                                                                                if(($item_value["vendor_order"]["order_status"] == 'LAPSED')){
                                                                                    $is_show_assign_btn = false;
                                                                                }
                                                                            }
                                                                            

                                                                        }

                                                                    }

                                                                    if(($item_value["item_status"] == 'CONFIRMED') || ($item_value["item_status"] == 'TM_ASSIGNED')
                                                                        || ($item_value["item_status"] == 'PICKUP') || ($item_value["item_status"] == 'DELAYED') || ($is_show_assign_btn)){

                                                                        if($today_date <= $end_date){

                                                                            if($item_value['part_quantity'] >= 3){
                                                                ?>
                                                                            <!-- <input type="hidden" id="order_remain_part_qty" value="{{ $item_value['part_quantity'] }}" /> -->
                                                                            <a href="javascript:void(0)" class="assign_qty_btn" type="button" onclick="openAssignQtyPopup('{{ $order_id }}','{{ $item_value['_id'] }}','{{ $item_value['vendor_id'] }}','{{ $item_value['quantity'] }}','{{ $item_value['part_quantity'] }}','{{ date('d-m-Y',strtotime($item_value['delivery_date'])) }}','{{ date('d-m-Y',strtotime($item_value['end_date'])) }}')">Assign Qty & Date</a>
                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                ?>
                                                            @endif
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <div class="qty-text-block">
                                                            <span>{{ isset($item_value['part_quantity']) ? $item_value['part_quantity'] : 0 }}</span>


                                                        </div>

                                                    </td>
                                                    <td class="tble_heding_color_red">{{ isset($item_value['remaining_quantity']) ? $item_value['remaining_quantity'] : 0 }}

                                                    </td>
                                                    <td class="tble_heding_color_green">{{ isset($item_value['pickup_quantity']) ? $item_value['pickup_quantity'] : 0 }} </td>
                                                    <td>{{ isset($item_value["unit_price_With_Margin"]) ? number_format($item_value["unit_price_With_Margin"],2) : 0 }}</td>
                                                    <td>{{ ($item_value["with_TM"] == 'true') ? (($item_value["with_CP"] == 'true') ? 'TM & CP both' : 'TM Only' ): (($item_value["with_TM"] == 1) ? (($item_value["with_CP"] == 1) ? 'TM & CP both' : 'TM Only' ) : "Buyer's TM") }}
                                                        @if(!$item_value["with_TM"])
                                                            <div class="dropdown tm_info_dropdown">
                                                                <a href="#" class="dropdown-toggle" role="button" id="dropdownMenuLink2" data-bs-toggle="dropdown" aria-expanded="true">Buyer TM Info</a>
                                                                <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <div class="tm_info_bx">
                                                                        <h4>Buyer TM Detail</h4>
                                                                        <div class="tm_info_list"> <span>Delivery Date & Time :</span> 04 Jan 2021, 10:50 AM </div>
                                                                        <div class="tm_info_list"> <span>Transit Mixer No :</span> GJ-01-AA-0000 </div>
                                                                        <div class="tm_info_list"> <span>Driver Name :</span> Jhon </div>
                                                                        <div class="tm_info_list"> <span>Mobile No. :</span> +91 9979016486 </div>
                                                                        <div class="tm_info_list"> <span>TM Operator Name :</span> Jhondoe </div>
                                                                        <div class="tm_info_list"> <span>TM Operator Mobile No. :</span> +91 9979016486 </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </td>

                                                    <!-- <td> {{ isset($item_value['TM_price']) ? number_format($item_value['TM_price'],2) : 0 }}</td>
                                                    <td> {{ isset($item_value['CP_price']) ? number_format($item_value['CP_price'],2) : 0 }}</td> -->

                                                    <!-- <td>
                                                        @if(isset($item_value["review"]) && (count($item_value["review"]) > 0))
                                                            <a href="javascript:void(0)" onclick="showReviewDialog('{{ json_encode($item_value['review']) }}','{{ $item_value['vendor_id'] }}','edit')">Review & Feedback</a>
                                                        @else
                                                            <a href="javascript:void(0)" onclick="showReviewDialog('','{{ $item_value['vendor_id'] }}','add')">Review & Feedback</a>
                                                        @endif
                                                    </td> -->
                                                    <td>
                                                        <div class="ord-track-block">
                                                            <!-- <a href="{{ route('buyer_order_track', $item_value['_id']) }}" class="site-button yellow button-sm track-btn">Track <i class="fa fa-truck fa-flip-horizontal"></i></a> -->
                                                            <a href="javascript:void(0)" onclick="itemTrack('{{ $item_value['_id'] }}')" class="site-button yellow button-sm track-btn">Track <i class="fa fa-truck fa-flip-horizontal"></i></a>
                                                        </div>
                                                    </td>
                                                    <td>

                                                        @if($item_value["item_status"] == 'DELIVERED')
                                                            <span class="badge bg-success">{{ $item_value["item_status"] }}</span>
                                                            <?php
                                                                $progress_per = $progress_per + $total_devide_percentage;
                                                            ?>
                                                        @else @if($item_value["item_status"] == 'CANCELLED' || $item_value["item_status"] == 'REJECTED' || $item_value["item_status"] == 'LAPSED')
                                                            <span class="badge bg-danger">{{ $item_value["item_status"] }}</span>
                                                            @if($item_value["item_status"] == 'LAPSED')
                                                                (Qty not assigned)
                                                            @endif
                                                            
                                                        @else @if($item_value["item_status"] == 'PICKUP')
                                                            <span class="badge bg-purple">{{ $item_value["item_status"] }}</span>
                                                            
                                                        @else
                                                            <span class="badge bg-warning text-dark">{{ str_replace('_',' ',$item_value["item_status"]) }}</span>
                                                            @if($item_value["item_status"] == 'RECEIVED')
                                                                (Confirmation pending from RMC supplier)
                                                            @endif
                                                            
                                                        @endif
                                                        @endif
                                                        @endif

                                                    </td>
                                                    <td>

                                                        @if($item_value["payment_status"] == 'PAID')
                                                            @if($item_value["refund_id"] != null)
                                                                <span class="badge bg-success">Refunded</span>
                                                                (Refunded Id: {{ $item_value["refund_id"] }})
                                                            @else
                                                                <span class="badge bg-success">{{ $item_value["payment_status"] }}</span>
                                                            @endif
                                                        @else @if($item_value["payment_status"] == 'UNPAID')
                                                            <span class="badge bg-danger">{{ $item_value["payment_status"] }}</span>

                                                        
                                                        @endif
                                                        @endif

                                                    </td>
                                                    <td>
                                                        <div class="ordlst-sub-total-block">
                                                            <div class="ordrlst-total-text-block">
                                                                <span>{{ isset($item_value['selling_price_With_Margin']) ? number_format($item_value['selling_price_With_Margin'],2) : 0 }}</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="order_action">
                                                            <?php 
                                                                $is_show_action_menu = 0;
                                                            ?>
                                                            @if(($item_value["item_status"] == 'PLACED') || ($item_value["item_status"] == 'PROCESSING'))
                                                                @if(isset($item_value["orderItemPartData"]) && empty($item_value["orderItemPartData"]))
                                                                    <?php $is_show_action_menu = 1; ?>
                                                                @endif
                                                            @endif
                                                            @if(isset($item_value["review"]) && (count($item_value["review"]) > 0))
                                                                <?php $is_show_action_menu = 1; ?>
                                                            @else
                                                                @if($item_value["item_status"] == 'DELIVERED')
                                                                <?php $is_show_action_menu = 1; ?>
                                                                @endif
                                                            @endif
                                                            @if($item_value["item_status"] == 'DELIVERED')
                                                                @if(isset($item_value['concrete_grade']['_id']))
                                                                    <?php $is_show_action_menu = 1; ?>
                                                                @endif
                                                                
                                                                @if(isset($item_value['concrete_grade_id']))
                                                                    <?php $is_show_action_menu = 1; ?>
                                                                @endif
                                                            @endif

                                                            @if($is_show_action_menu == 1)
                                                                <a href="#" class="dropdown-toggle text-center d-block" data-bs-toggle="dropdown" aria-expanded="true"><span class="dropdown-label"> <i class="fa fa-ellipsis-h"></i></span></a>
                                                            @else
                                                                -
                                                            @endif
                                                            <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            
                                                                @if(($item_value["item_status"] == 'PLACED') || ($item_value["item_status"] == 'PROCESSING'))
                                                                    @if(isset($item_value["orderItemPartData"]) && empty($item_value["orderItemPartData"]))
                                                                        <a href="javascript:void(0)" onclick="confirmPopup(1,'{{ json_encode($item_value) }}','{{ csrf_token() }}','Are you sure you want to cancel this order')" class="d-block">Cancel Order</a>
                                                                    @endif
                                                                @endif
                                                                @if(isset($item_value["review"]) && (count($item_value["review"]) > 0))
                                                                    <a href="javascript:void(0)" onclick="showReviewDialog('{{ json_encode($item_value['review']) }}','{{ $item_value['vendor_id'] }}','{{ $item_value['_id'] }}','edit')" class="d-block">Review & Feedback</a>
                                                                @else
                                                                    @if($item_value["item_status"] == 'DELIVERED')
                                                                        <a href="javascript:void(0)" onclick="showReviewDialog('','{{ $item_value['vendor_id'] }}','{{ $item_value['_id'] }}','add')" class="d-block">Review & Feedback</a>
                                                                    @endif
                                                                @endif
                                                                @if($item_value["item_status"] == 'DELIVERED')
                                                                    @if(isset($item_value['concrete_grade']['_id']))
                                                                        <!-- <a href="{{ route('buyer_product_listing',$item_value['concrete_grade']['_id']) }}" class="d-block">Repeat Order</a> -->
                                                                    @endif
                                                                    
                                                                    @if(isset($item_value['concrete_grade_id']))
                                                                        <!-- <a href="{{ route('buyer_product_listing',$item_value['concrete_grade_id']) }}" class="d-block">Repeat Order</a> -->
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="14" class="inner_tbl">
                                                        <h6 class="my-3">Assigned Qty & Date</h6>
                                                        <table class="table table-bordered mb-0">
                                                            <thead>
                                                                <tr>
                                                                    <th class="ord-wd-10 text-nowrap">Assigned Date</th>
                                                                    <th class="text-nowrap">Assigned Start Time</th>
                                                                    <th class="text-nowrap">Assigned End Time</th>
                                                                    <!-- <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th> -->
                                                                    <th class="text-nowrap">Deliver Qty (Cu.Mtr)</th>
                                                                    <th class="text-nowrap">CP Track</th>
                                                                    <th class="text-nowrap">Reassign Request</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(isset($item_value["orderItemPartData"]))
                                                                    @foreach($item_value["orderItemPartData"] as $item_part_value)

                                                                    <tr>
                                                                        <td>
                                                                        {{ isset($item_part_value['start_time']) ? date('d M Y',strtotime($item_part_value['start_time'])) : '' }}
                                                                        </td>

                                                                        <td>
                                                                        {{ isset($item_part_value['start_time']) ? date('d M Y, h:i a',strtotime($item_part_value['start_time'])) : '' }}
                                                                        </td>

                                                                        <td>
                                                                        {{ isset($item_part_value['end_time']) ? date('d M Y, h:i a',strtotime($item_part_value['end_time'])) : '' }}
                                                                        </td>

                                                                        <td>
                                                                        {{ isset($item_part_value['assigned_quantity']) ? $item_part_value['assigned_quantity'] : '' }}
                                                                        </td>
                                                                        <td>
                                                                            <div class="ord-track-block">
                                                                                <!-- <a href="{{ route('buyer_order_track', $item_value['_id']) }}" class="site-button yellow button-sm track-btn">Track <i class="fa fa-truck fa-flip-horizontal"></i></a> -->
                                                                                @if($is_with_cp == 1)
                                                                                    <a href="javascript:void(0)" onclick="CPTrack('{{ $item_part_value['_id'] }}')" class="site-button yellow button-sm track-btn">Track <i class="fa fa-truck fa-flip-horizontal"></i></a>
                                                                                @else

                                                                                    Order Placed Without CP

                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            @if(isset($item_part_value['order_reassign']) && count($item_part_value['order_reassign']) > 0)
                                                                                <div class="dropdown tm_info_dropdown">
                                                                                    <a href="#" class="color_orange" class="dropdown-toggle" role="button" id="dropdownMenuLink2" data-bs-toggle="dropdown" aria-expanded="true">Reassign Request</a>
                                                                                    <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                                        <?php
                                                                                            $accept_reject = 0;
                                                                                            $order_item_part_id = 0;
                                                                                        ?>
                                                                                        @foreach($item_part_value['order_reassign'] as $order_reassign_value)
                                                                                        <?php
                                                                                            $accept_reject = $order_reassign_value['accept_reject'];
                                                                                            $order_item_part_id = $order_reassign_value['order_item_part_id'];

                                                                                        ?>
                                                                                            <div class="tm_info_bx">
                                                                                                <h4 class="color_orange">Reassign Request</h4>
                                                                                                <div class="tm_info_list"> <span>Reassign Reason :</span> {{ $order_reassign_value['reassign_reason'] }} </div>
                                                                                                <div class="tm_info_list"> <span>Reassign Desc :</span> {{ $order_reassign_value['reassign_desc'] }} </div>
                                                                                                <div class="tm_info_list"> <span>Previous Delivery Date :</span> {{ AdminController::dateTimeFormat($order_reassign_value["previous_start_time"]) }} - {{ AdminController::dateTimeFormat($order_reassign_value["previous_end_time"]) }} </div>
                                                                                                <div class="tm_info_list"> <span>New Delivery Date :</span> {{ AdminController::dateTimeFormat($order_reassign_value["new_start_time"]) }} - {{ AdminController::dateTimeFormat($order_reassign_value["new_end_time"]) }} </div>
                                                                                                <div class="tm_info_list"> <span>Status :</span> 
                                                                                                                    @if($order_reassign_value['accept_reject'] == 0) 
                                                                                                                        <span class="badge bg-yellow">No Status</span>
                                                                                                                    @elseif($order_reassign_value['accept_reject'] == 1)
                                                                                                                        <span class="badge bg-yellow">Accepted</span>
                                                                                                                    @elseif($order_reassign_value['accept_reject'] == 2)
                                                                                                                        <span class="badge bg-yellow">Rejected</span>
                                                                                                                    @endif
                                                                                                
                                                                                                @if(isset($order_reassign_value['order_track']) && count($order_reassign_value['order_track']) > 0)
                                                                                                    <div class="tm_info_list">
                                                                                                        <h4 class="color_orange">Track Details</h4> 
                                                                                                        <div class="tm_info_list">
                                                                                                            <span>TM No. :</span> {{ $order_reassign_value['order_track']['TM_rc_number'] }} 
                                                                                                        </div>
                                                                                                        <div class="tm_info_list">
                                                                                                            <span>Qty. :</span> {{ $order_reassign_value['order_track']['pickup_quantity'] }} 
                                                                                                        </div>
                                                                                                        <!-- <div class="tm_info_list">
                                                                                                            <span>Status :</span> {{ $order_reassign_value['order_track']['event_status'] }} 
                                                                                                        </div> -->
                                                                                                        
                                                                                                        
                                                                                                    </div>
                                                                                                @endif
                                                                                                
                                                                                                
                                                                                            </div>
                                                                                            <hr/>
                                                                                        @endforeach

                                                                                        @if($accept_reject == 0) 
                                                                                            <div class="acept_rej_btn" style="margin-top:10px;">
                                                                                                <!-- <span class="badge bg-yellow">Test</span> -->

                                                                                                <a href="javascript:;" onclick="orderReassignAcceptReject('1','{{ $order_item_part_id }}')" ><span class="badge bg-success">Accept</span></a>
                                                                                                <a href="javascript:;" onclick="orderReassignAcceptReject('2','{{ $order_item_part_id }}')"> <span class="badge bg-danger">Reject & Cancel</span></a>
                                                                                            

                                                                                            </div>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            @else
                                                                                -
                                                                            @endif
                                                                        </td>

                                                                    </tr>

                                                                    @endforeach

                                                                @endif


                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="14">
                                                        <div class="ordlst-sub-total-block">
                                                            <div class="ordrlst-total-text-block">
                                                                <strong>Sub Total:</strong> <span> {{ number_format($base_amount,2) }}</span>
                                                            </div>
                                                            <div class="ordrlst-total-text-block">
                                                                <strong>GST:</strong> <span> {{ number_format($gst_amount,2) }}</span>
                                                            </div>
                                                            <div class="ordrlst-total-text-block ordrlst-mani-list">
                                                                <strong>Total Amount:</strong> <span> {{ number_format($total_amount,2) }}</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                    <p class="small"><i>Note: All Quantity in Cu.Mtr</i></p>
                </div>
            </div>
        </section>
        <!-- PRODUCT LISTING BLOCK END -->

    </div>
    <!-- CONTENT END -->





    <!-- COMPLAIN MODAL START -->
    @if(isset($order_id))
    <div id="complain-popup" class="modal fade review-modal-block" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Complain</h4>
            </div>
            <form name="order_complain_form">
                <div class="modal-body">
                @csrf
                    <input type="hidden" name="order_id" value="{{ $order_id }}" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" id="complaint_type_div">
                                <div class="cstm-select-box">
                                    <select name="complaint_type">
                                        <option value="">Select Complaint Type</option>
                                        <option value="wrong product deliever">Wrong Product Deliever</option>
                                        <option value="product quality is too low">Product Quality Is Too Low</option>
                                        <option value="delayed delievery">Delayed Delievery</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <textarea name="complaint_text" class="form-control" placeholder="Write a Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="file" name="files">
                        </div> -->
                        <!-- <div class="col-md-6">
                            <button type="submit" class="site-button yellow m-b10">Submit</button>
                        </div> -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="site-button outline gray m-r10" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="site-button m-b10">Submit</button>
                </div>
            </form>
            </div>
        </div>
    </div>


    <div id="review_popup" class="modal fade review-modal-block" role="dialog">
        <div class="modal-dialog modal-dialog400">
            <div class="modal-content">
                <!-- <div class="modal-header"> -->
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>

                <!-- </div> -->
                <form name="order_product_review_form">
                    <div class="modal-body">
                    <h5 class="modal-title">Product Review</h5>
                        @csrf
                        <div class="star_rating mb-2">
                            <!-- <p>Your Rating</p> -->
                            <fieldset>
                                <div class="my-rating"></div>

                                <script>

                                    $(".my-rating").starRating({
                                        starSize: 20,
                                        disableAfterRate: false,
                                        useFullStars: true,
                                        callback: function(currentRating, $el){
                                            // make a server call here
                                            console.log("tetst rating...");

                                            $("#review_rating_number").val(currentRating);
                                        }
                                    });

                                </script>
                                <!-- <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Outstanding"><span>5</span></label>
                                <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Very Good"><span>4</span></label>
                                <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Good"><span>3</span></label>
                                <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Poor"><span>2</span></label>
                                <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Very Poor"><span>1</span></label> -->
                            </fieldset>
                        </div>
                        <input type="hidden" name="rating" id="review_rating_number" />
                        <input type="hidden" id="review_vendor_id" name="vendor_id" value="" />
                        <input type="hidden" id="review_form_type" name="review_form_type" value="add" />
                        <input type="hidden" id="review_id" name="review_id" value="" />
                        <input type="hidden" id="order_item_id" name="order_item_id" value="" />
                        <!-- <div class="row"> -->
                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                <div class="input-group">
                                    <input class="form-control" placeholder="Name" type="text">
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <div class="input-group">
                                    <input class="form-control" placeholder="Email" type="email">
                                </div>
                                </div>
                            </div> -->
                            <div class="mb-3">

                                <label for="review_pro" class="form-label">Review For Product</label>
                                <textarea class="form-control" id="review_comment" name="review_text" placeholder="Write a product review"></textarea>

                            </div>
                        <!-- </div> -->
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-secondary mt-2" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary btn-secondary mt-2">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- FEEDBACK MODAL END -->

    @endif
    <!-- COMPLAIN MODAL END -->

<script>
@if(isset($progress_per))
    $("#custom_mix_progress").css("width","{{ $progress_per }}%")
@endif

</script>



    @endsection