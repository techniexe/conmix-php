@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <!-- <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="{{ route('buyer_home') }}">Home</a></li>
                <li>Support Ticket</li>
            </ul>
        </div>
    </div> -->
    <!-- <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Support Ticket</span>
            </div>
        </div>
    </section> -->
    <!-- BREADCRUMB ROW END -->
    <!-- SUPPORT TICKET SECTION START -->

<!-- SECTION CONTENT -->
    <section>
        <div class="container">
            <div class="order_list_tbl wht_bx p-3 mb-4">
                <div class="d-flex justify-content-between mb-3">
                    <h5 class="mb-1">Support Ticket List</h5>
                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#create-ticket-popup" onclick="resetForm('add_support_ticket_form','create-ticket-popup')">Create Ticket</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Support Ticket No</th>
                                <th>Subject</th>
                                <th>Created On</th>
                                <th>Priority</th>
                                <th>User Type</th>
                                <th>User Name</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($data['data']) && count($data['data']) > 0)
                            @php
                                $first_created_date = '';
                                $last_created_date = '';
                                $count = 0;
                            @endphp
                            @foreach($data["data"] as $value)  
                                @if($count == 0)
                                    @php 
                                        $first_created_date = $value["created_at"];
                                        $count++; 
                                    @endphp
                                @endif
                                @php
                                    $last_created_date = $value["created_at"];
                                @endphp
                                <tr>
                                    <td><a href="{{ url('buyer/my_support_ticket/mobile/ticket_detail/'.$value['ticket_id']) }}">#{{ isset($value["ticket_id"]) ? $value["ticket_id"] : '' }}</a></td>
                                    <td>{{ isset($value["subject"]) ? $value["subject"] : '' }}</td>
                                    <td>{{ isset($value["created_at"]) ? date('d M Y', strtotime($value["created_at"])) : '' }}</td>
                                    <td>{{ isset($value["severity"]) ? $value["severity"] : '' }}</td>
                                    <td>{{ isset($value["client_type"]) ? $value["client_type"] : '' }}</td>
                                    <td>{{ isset($value["buyer"]["full_name"]) ? $value["buyer"]["full_name"] : '' }}</td>
                                    <td><div class="badge bg-yellow">{{ isset($value["support_ticket_status"]) ? $value["support_ticket_status"] : '' }}</div></td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8" style="text-align: center;">No Record Found</td>
                                </tr>
                            @endif
                                
                        </tbody>
                    </table>
                </div>
                <div class="data-box-footer clearfix">
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        @if(isset($data['data']) && (count($data['data']) >=  ApiConfig::PAGINATION_LIMIT))
                        <div class="pagination cust_pagination justify-content-end m-0">
                            @if(isset($first_created_date))
                            <form action="{{route(Route::current()->getName(),session('buyer_custom_token', null))}}" method="get">
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="btn btn-primary btn-secondary me-3">Previous</button>                                    
                            </form>
                            @endif
                            @if(isset($last_created_date))
                            <form action="{{route(Route::current()->getName(),session('buyer_custom_token', null))}}" method="get" class="m-l10">
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="btn btn-primary btn-secondary">Next</button>
                            </form>
                            @endif
                        </div>
                        @endif
                    </div>
                </div> 
                
                
            </div>
        </div>
    </section>

    
    <!-- SUPPORT TICKET SECTION END -->
    <!-- REQUEST A QUOTE SECTION START -->
<!--     <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                    <p>Have any ideas in your mind?</p>
                    <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                </div>
                <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                    <div class="button_request">
                        <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- REQUEST A QUOTE SECTION END -->
</div>
<!-- CONTENT END -->

@endsection