@section('title', 'Ticket Detail')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <!-- <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="{{ route('buyer_home') }}">Home</a></li>
                <li><a href="{{ route('buyer_my_support_ticket') }}">Support Ticket</a></li>
                <li>Support Ticket Details</li>
            </ul>
        </div>
    </div> -->
    @if((Route::current()->getName() != 'buyer_ticket_mobile_ticket_detail'))
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Support Ticket Details</span>
            </div>
        </div>
    </section>
    @endif
    <!-- BREADCRUMB ROW END -->
    <!-- SUPPORT TICKET SECTION START -->

    <section>
        <div class="container">
        <?php
            //dd($data["data"]); 
            $ticket_data_status = ""; ?>
            @if(isset($data["data"]["ticketData"][0]))
                @php
                    $ticket_data_status = $data["data"]["ticketData"][0]["support_ticket_status"];
                @endphp
            @endif
        <div class="order_list_tbl wht_bx p-3 mb-4">
                <h2 class="sub-title pull-left">Ticket Id - <span>#{{ isset($data["ticket_id"]) ? $data["ticket_id"] : '' }}</span></h2>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Support Ticket No</th>
                                <th>Subject</th>
                                <th>Description</th>
                                <th>Created On</th>
                                <th>Priority</th>
                                <th>User Type</th>
                                <th>User Name</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($data["data"]))
                            @php
                                    $ticket_data = $data["data"]["ticketData"];
                            @endphp
                                @foreach($ticket_data as $value) 
                                <tr>
                                    <td>#{{ isset($value["ticket_id"]) ? $value["ticket_id"] : '' }}</td>
                                    <td>{{ isset($value["subject"]) ? $value["subject"] : '' }}</td>
                                    <td>{{ isset($value["description"]) ? $value["description"] : '' }}</td>
                                    <td>{{ isset($value["created_at"]) ? date('d M Y, h:m',strtotime($value["created_at"])) : '' }}</td>
                                    <td>{{ isset($value["severity"]) ? $value["severity"] : '' }}</td>
                                    <td>{{ isset($value['client_type']) ? $value['client_type'] : '' }}</td>
                                    <td>{{ isset($value['buyer']['full_name']) ? $value['buyer']['full_name'] : '' }}</td>
                                    <td>
                                        @if($value["support_ticket_status"] == 'OPEN')
                                            <div class="badge bg-green">{{ isset($value["support_ticket_status"]) ? $value["support_ticket_status"] : '' }}</div>
                                        @else
                                            <div class="badge bg-red">{{ isset($value["support_ticket_status"]) ? $value["support_ticket_status"] : '' }}</div>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="8" style="text-align: center;">No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="data-box-footer clearfix">
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        @if(isset($data['data']) && (count($data['data']) >=  ApiConfig::PAGINATION_LIMIT))
                        <div class="pagination cust_pagination justify-content-end m-0">
                            @if(isset($first_created_date))
                            <form action="{{route(Route::current()->getName())}}" method="get">
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="btn btn-primary btn-secondary me-3">Previous</button>                                    
                            </form>
                            @endif
                            @if(isset($last_created_date))
                            <form action="{{route(Route::current()->getName())}}" method="get" class="m-l10">
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="btn btn-primary btn-secondary">Next</button>
                            </form>
                            @endif
                        </div>
                        @endif
                    </div>
                </div> 
                
                
            </div>
            <div class="wht_bx px-4 mb-4 py-2">
                <div class="review-complaints-block wht-tble-bg">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="sub-title-block d-flex justify-content-between align-items-center mt-2 mb-3">
                                <h5 class="sub-title">Correspondence</h5>
                                @if($ticket_data_status != "CLOSED")
                                    <button class="btn btn-primary btn-secondary showButton rplyshow" onclick="resetForm('support_ticket_reply_form','create-ticket-popup')">Reply</button>
                                @endif
                            </div>

                            <div class="correspondence-reply-block">
                                <div class="rply-block">
                                    <div class="rply-form rplyshowhide" style="display: none;">
                                        <div class="rply-form-block">
                                            <form id="upload" name="support_ticket_reply_form" method="post" action="" enctype="multipart/form-data">
                                            @csrf
                                                <div class="input-field">
                                                    <label>Reply</label>
                                                    <div class="cstm-input">
                                                        <textarea name="comment" class="form-control" id="ctdescription" placeholder="Detailed of the question or issue"></textarea>
                                                        <!-- <span class="character-text">Maximum 5000 characters (<span class="character-counter">5000</span> remaining)</span> -->
                                                    </div>
                                                </div>
                                                <div class="input-field">
                                                    <label>Attachment</label>
                                                    <div class="upload-file-block mt-0">
                                                        <input type="file" name="attachments">
                                                    </div>
                                                    <span class="file-note-text">Use one of the jpg, jpeg, mp4, doc, docx, pdf file format</span>
                                                    <span class="file-error-text" style="display: none;"><i class="ion-alert-circled"></i> Maximum amount of files exceeded!</span>
                                                </div>
                                                <input type="hidden" name="ticket_id" id="support_ticket_id" value="{{ isset($data['ticket_id']) ? $data['ticket_id'] : '' }}" />
                                                <div class="input-field">
                                                    <button type="submit" class="btn btn-primary btn-secondary m-r10">Submit</button>
                                                    <button class="btn btn-primary hideButton rplyshowhide" >Cancel</button>
                                                </div>
                                            </form>

                                            <!-- <form id="upload" name="support_ticket_reply_form" method="post" action="" enctype="multipart/form-data">
                                            @csrf
                                                <div class="input-field">
                                                    <label>Reply</label>
                                                    <div class="cstm-input">
                                                        <textarea name="comment" class="form-control" id="ctdescription" placeholder="Detailed of the question or issue" data-maxchar="5000"></textarea>
                                                        <span class="character-text">Maximum 5000 characters (<span class="character-counter"></span> remaining)</span>
                                                    </div>
                                                </div>
                                                <div class="input-field">
                                                    <label>Attachment</label>
                                                    <div class="upload-file-block mt-0">
                                                        <input type="file" name="attachments">
                                                    </div>
                                                    <span class="file-note-text">Use one of the jpg, jpeg, mp4, doc, docx, pdf file format</span>
                                                    <span class="file-error-text" style="display: none;"><i class="ion-alert-circled"></i> Maximum amount of files exceeded!</span>
                                                </div>
                                                <input type="hidden" name="ticket_id" id="support_ticket_id" value="{{ isset($data['ticket_id']) ? $data['ticket_id'] : '' }}" />
                                                <div class="input-field">
                                                    <button type="submit" class="site-button m-r10">Submit</button>
                                                    <button class="site-button gray hideButton rplyshowhide" style="display: none">Cancel</button>
                                                </div>
                                            </form> -->


                                        </div>

                                    </div>
                                </div>

                                <div class="chat-area">
                                    <div class="chats">
                                        <div class="chats">
                                        @if(isset($data["ticket_messages"]))
                                        @foreach($data["ticket_messages"] as $value)
                                            <?php //dd($value); ?>
                                            @if($value["reply_by_type"] == 'admin')
                                                <div class="chat">
                                                    <div class="chat-body">
                                                        <div class="chat-tms-details">
                                                            <h5>Conmate {{ $value["reply_by_type"] }}</h5>
                                                            <p>{{ date('d M Y, h:i:s a', strtotime($value["created_at"])) }}</p>
                                                        </div>
                                                        <div class="chat-text">
                                                            <p>{{ $value["comment"] }}</p>
                                                            
                                                            @if(isset($value["attachments"]) && !empty($value["attachments"]))
                                                                <div class="et-attachment-block">
                                                                    <h3><i class="fa fa-paperclip"></i> Attachments <span>({{ count($value["attachments"]) }})</span></h3>
                                                                    <ul>
                                                                        <?php $file_count = 1; //dd($value["attachments"])?>
                                                                        @foreach($value["attachments"] as $attchment_value)

                                                                            <li>
                                                                                <div class="mailbox-attachment-info">
                                                                                    @if(strpos($attchment_value["url"], '.doc') || strpos($attchment_value["url"], '.docx'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-word-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.jpg') || strpos($attchment_value["url"], '.png') || strpos($attchment_value["url"], '.jpeg'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-photo-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.pdf'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.mp4'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-video-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif
                                                                                    <!-- <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> Sep2014-report.pdf</a>
                                                                                    <span class="mailbox-attachment-size">1,245 KB</span> -->
                                                                                </div>
                                                                            </li>
                                                                        @endforeach
                                                                        <!-- <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-word-o"></i> App Description.docx</a>
                                                                                <span class="mailbox-attachment-size">1,245 KB</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-photo-o"></i> photo1.png</a>
                                                                                <span class="mailbox-attachment-size">2.67 MB</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-video-o"></i> video.mp4</a>
                                                                                <span class="mailbox-attachment-size">2.67 MB</span>
                                                                            </div>
                                                                        </li> -->
                                                                    </ul>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="chat chat-right">
                                                    <div class="chat-body">
                                                        <div class="chat-tms-details">
                                                            <h5>{{ $value["reply_by_type"] }}</h5>
                                                            <p>{{ date('d M Y, h:i:s a', strtotime($value["created_at"])) }}</p>
                                                        </div>
                                                        <div class="chat-text">
                                                            <p>{{ $value["comment"] }}</p>

                                                            @if(isset($value["attachments"]) && !empty($value["attachments"]))
                                                                <div class="et-attachment-block text-left">
                                                                    <h3><i class="fa fa-paperclip"></i> Attachments <span>({{ count($value["attachments"]) }})</span></h3>
                                                                    <ul>
                                                                        <?php $file_count = 1; //dd($value["attachments"])?>
                                                                        @foreach($value["attachments"] as $attchment_value)

                                                                            <li>
                                                                                <div class="mailbox-attachment-info">
                                                                                    @if(strpos($attchment_value["url"], '.doc') || strpos($attchment_value["url"], '.docx'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-word-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.jpg') || strpos($attchment_value["url"], '.png') || strpos($attchment_value["url"], '.jpeg'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-photo-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif

                                                                                    @if(strpos($attchment_value["url"], '.pdf'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif
                                                                                    
                                                                                    @if(strpos($attchment_value["url"], '.mp4'))
                                                                                        <a href="{{ $attchment_value['url'] }}" class="mailbox-attachment-name"><i class="fa fa-file-video-o"></i> attachment {{ $file_count }}</a>
                                                                                    @endif
                                                                                    <!-- <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> Sep2014-report.pdf</a>
                                                                                    <span class="mailbox-attachment-size">1,245 KB</span> -->
                                                                                </div>
                                                                            </li>
                                                                        @endforeach
                                                                        <!-- <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-word-o"></i> App Description.docx</a>
                                                                                <span class="mailbox-attachment-size">1,245 KB</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-photo-o"></i> photo1.png</a>
                                                                                <span class="mailbox-attachment-size">2.67 MB</span>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="mailbox-attachment-info">
                                                                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-file-video-o"></i> video.mp4</a>
                                                                                <span class="mailbox-attachment-size">2.67 MB</span>
                                                                            </div>
                                                                        </li> -->
                                                                    </ul>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif


                                        @endforeach
                                    @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <!-- SUPPORT TICKET SECTION END -->
    <!-- REQUEST A QUOTE SECTION START -->
  <!--   <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                    <p>Have any ideas in your mind?</p>
                    <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                </div>
                <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                    <div class="button_request">
                        <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- REQUEST A QUOTE SECTION END -->
</div>
<!-- CONTENT END -->

@endsection