@if(isset($cart_details["data"]["items"]) && count($cart_details["data"]["items"]) > 0)
    @if(isset($cart_details["data"]["items"]) && count($cart_details["data"]["items"]) > 0)
    <div class="cart_menu">
        
            @foreach($cart_details["data"]["items"] as $value)
                @if(isset($value["design_mix_id"]))
                <!-- Design Mix -->
                    <div class="cart_menu_item d-flex mb-3 pb-3">
                        <div class="cart_menu_item_img">
                        @if(isset($value["vendor_media"]) && !empty($value["vendor_media"]))
                            @foreach($value["vendor_media"] as $media_value)
                                @if($media_value["type"] == "PRIMARY")
                                    <img src="{{$media_value['media_url']}}" alt="item">
                                @else
                                    <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="" alt="item">
                                @endif
                            @endforeach
                        @else
                             <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="" alt="item">
                            @endif
                        </div>
                        <div class="cart_menu_item_dtl">
                            <h6 class="mb-1">{{ $value["concrete_grade_name"] }}</h6>
                            <p> <span>{{ $value["vendor_name"] }}</span></p>
                            <p class="mb-1">Qty : {{ $value["quantity"] }} Cu.Mtr</p>
                            <h6 class="mb-1">₹{{ number_format($value["selling_price_With_Margin"],2) }}</h6>
                            <a href="javascript:void(0)" onclick="deleteCartItem('{{ csrf_token() }}','{{ $value["_id"] }}','cartslide_{{ $value["_id"] }}','{{ count($cart_details["data"]["items"]) }}','slide_cart')" ><i class="far fa-trash-alt"></i></a>
                        </div>
                    </div>
                @else
                    <!-- Custom Mix -->
                    <?php $custom_mix = $value["custom_mix"] ?>
                <div class="cart_menu_item d-flex mb-3 pb-3">
                    <div class="cart_menu_item_img">
                    @if(isset($custom_mix["vendor_media"]) && !empty($custom_mix["vendor_media"]))
                        @foreach($custom_mix["vendor_media"] as $media_value)
                            @if($media_value["type"] == "PRIMARY")
                                <img src="{{$media_value['media_url']}}" alt="item">
                            @endif
                        @endforeach
                        @else
                             <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="" alt="item">
                            
                        @endif
                    </div>
                    <div class="cart_menu_item_dtl">
                        <h6 class="mb-1">{{ isset($custom_mix["concrete_grade_name"]) ? $custom_mix["concrete_grade_name"] : '' }}</h6>
                        <p> <span>{{ isset($custom_mix["vendor_name"]) ? $custom_mix["vendor_name"] : '' }}</span></p>
                        <p class="mb-1">Qty : {{ $value["quantity"] }} Cu.Mtr</p>
                        <h6 class="mb-1">₹{{ number_format($value["selling_price_With_Margin"],2) }}</h6>
                        <a href="javascript:void(0)" onclick="deleteCartItem('{{ csrf_token() }}','{{ $value["_id"] }}','cartslide_{{ $value["_id"] }}','{{ count($cart_details["data"]["items"]) }}','slide_cart')" ><i class="far fa-trash-alt"></i></a>
                    </div>
                </div>

                @endif
                
            @endforeach
        
        <!-- <div class="cart_menu_item d-flex mb-3 pb-3">
            <div class="cart_menu_item_img">
                <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="item">
            </div>
            <div class="cart_menu_item_dtl">
                <h6 class="mb-1">M10 - Mix 1</h6>
                <p>UltraTech by <span>AK Group</span></p>
                <p class="mb-1">Quantity : 6 Cu.mtr</p>
                <h6 class="mb-1">₹4956.00</h6>
                <a href="#" class="btn-close"></a>
            </div>
        </div>
        <div class="cart_menu_item d-flex mb-3 pb-3">
            <div class="cart_menu_item_img">
                <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="item">
            </div>
            <div class="cart_menu_item_dtl">
                <h6 class="mb-1">M10 - Mix 1</h6>
                <p>UltraTech by <span>AK Group</span></p>
                <p class="mb-1">Quantity : 6 Cu.mtr</p>
                <h6 class="mb-1">₹4956.00</h6>
                <a href="#" class="btn-close"></a>
            </div>
        </div> -->
        <div class="cart_menu_total d-flex justify-content-between align-items-center pb-3 mb-3">
            <p class="mb-0">Cart Total :</p>
            <h6 class="mb-0">₹ {{ number_format($cart_details["data"]["total_amount"],2) }}</h6>
        </div>
        <!-- <div class="d-flex justify-content-evenly">
            <a href="{{ route('buyer_cart_checkout') }}" class="btn btn-primary btn-secondary mb-1">VIEW CART</a>
            <a href="#" class="btn btn-primary btn-secondary mb-1">CHECK OUT</a>
        </div> -->
    </div>
    @else
    <div class="empty_cart">Your cart is currently empty.</div>
    @endif
@else
<div class="empty_cart">Your cart is currently empty.</div>
@endif