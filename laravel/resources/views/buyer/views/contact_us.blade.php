@section('title', 'Contact Us')
@extends('buyer.layouts.buyer_layout')

@section('content')

 <!-- CONTENT START -->
 <div class="page-content">
    <!-- BREADCRUMB ROW -->
    <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="{{ route('buyer_home') }}">Home</a></li>
                <li>Contact Us</li>
            </ul>
        </div>
    </div>
    <!-- BREADCRUMB ROW END -->

    <div class="section-full p-tb50 contact-us-block">
        <div class="container">
            <!-- CONTACT DETAIL BLOCK -->
            <div class="section-content m-b30">
                <div class="row">
                    <div class="col-md-4 col-sm-4 m-b30 wow fadeInDown" data-wow-delay="0.3s">
                        <div class="wt-icon-box-wraper center p-a30 bg-secondry">
                            <div class="icon-sm text-yellow m-b10"><i class="icon icon-map-pin"></i></div>
                            <div class="icon-content">
                                <h5 class="text-yellow">Address info</h5>
                                <p class="text-white">07B, Thilaj Town, Ahmedabad city, India</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 m-b30 wow fadeInDown" data-wow-delay="0.5s">
                        <div class="wt-icon-box-wraper center p-a30 bg-secondry">
                            <div class="icon-sm text-yellow m-b10"><i class="icon icon-envelope"></i></div>
                            <div class="icon-content">
                                <h5 class="text-yellow">Email address</h5>
                                <p class="text-white">contact.conmat@example.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 m-b30 wow fadeInDown" data-wow-delay="0.7s">
                        <div class="wt-icon-box-wraper center p-a30 bg-secondry">
                            <div class="icon-sm text-yellow m-b10"><i class="icon icon-phone2"></i></div>
                            <div class="icon-content">
                                <h5 class="text-yellow">Phone number</h5>
                                <p class="text-white">+ 91 45678 12345</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- GOOGLE MAP & CONTACT FORM -->
            <div class="section-content">
                <div class="row">

                    <!-- LOCATION BLOCK-->
                    <div class="wt-box col-md-6 wow fadeInLeft" data-wow-delay="0.9s">
                        <h4 class="text-uppercase">Location</h4>
                        <div class="gmap-outline m-b30">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d29368.433348441886!2d72.48761356619606!3d23.058475585821526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1552560690126" width="100%" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <!-- CONTACT FORM-->
                    <div class="wt-box col-md-6 wow fadeInRight" data-wow-delay="1.2s">
                        <h4 class="text-uppercase">Contact Form</h4>
                        <div class="p-a30 bg-gray">
                            <form class="cons-contact-form" method="post" action="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <input name="username" type="text" required="" class="form-control" placeholder="Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input name="email" type="text" class="form-control" required="" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon v-align-t"><i class="fa fa-pencil"></i></span>
                                                <textarea name="message" rows="2" class="form-control " required="" placeholder="Message"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button name="submit" type="submit" value="Submit" class="site-button">Submit </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- REQUEST A QUOTE SECTION START -->
<!--     <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                    <p>Have any ideas in your mind?</p>
                    <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                </div>
                <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                    <div class="button_request">
                        <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- REQUEST A QUOTE SECTION END -->
</div>
<!-- CONTENT END -->

@endsection