@section('title', 'My Orders')
@extends('buyer.layouts.buyer_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <!-- <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="{{ route('buyer_home') }}">Home</a></li>
                <li>Order List</li>
            </ul>
        </div>
    </div> -->
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Order List</span>
            </div>
        </div>
    </section>
    <!-- BREADCRUMB ROW END -->

    <!-- PRODUCT LISTING BLOCK -->
    <!-- SECTION CONTENT -->
    <section>
        <div class="container">
            <div class="order_list_tbl wht_bx p-3 mb-4">
                <div class="d-flex align-items-center justify-content-between mb-3">
                    <h5 class="mb-1">My Order</h5>
                    <div class="fillter_right d-flex">
                        @if(isset($data["data"]) && count($data['data']) > 0)
                            <div class="dropdown me-2">
                                <a href="#" class="dropdown-toggle btn btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">Export</a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                    <li><a class="dropdown-item" href="javascript:;" onclick="downloadOrders('excel')">Excel</a></li>
                                </ul>
                            </div>
                        @endif
                        <a href="{{route(Route::current()->getName())}}" class="btn btn-primary btn-secondary me-3 mb-2 clear_filter_button">Clear</a>
                        <a href="#" class="btn btn-primary fillter_btn ofrSldm-toggle"><i class="fas fa-filter" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Order Id</th>
                                <th>Order Date</th>
                                <th class="text-right">Sub Total ( <i class="fa fa-rupee"></i> )</th>
                                <th class="text-right">GST ( <i class="fa fa-rupee"></i> )</th>
                                <th class="text-right">Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th class="text-right">Order Status</th>
                                <th class="text-right">Item Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                                $first_created_date = '';
                                $last_created_date = '';
                                $count = 0;
                            @endphp
                        @if(isset($data["data"]) && count($data['data']) > 0)
                        <?php //dd($data["data"]);  ?>

                            @foreach($data["data"] as $value)
                                @if($count == 0)
                                    @php
                                        $first_created_date = $value["created_at"];
                                        $count++;
                                    @endphp
                                @endif
                                @php
                                    $last_created_date = $value["created_at"];
                                @endphp
                                <tr>
                                    <td><a class="color_orange" href="{{ route('buyer_order_detail', $value['_id']) }}">#{{ $value["display_id"] }}</a></td>
                                    <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                    <td class="text-right">{{ isset($value["selling_price_With_Margin"]) ? number_format($value["selling_price_With_Margin"],2) : 0 }}</td>
                                    <td class="text-right">{{ isset($value["gst_price"]) ? number_format($value["gst_price"],2) : 0 }}</td>
                                    <td class="text-right">{{ isset($value["total_amount"]) ? number_format($value["total_amount"],2) : 0 }}</td>
                                    <td class="text-right">
                                        @if($value["order_status"] == 'DELIVERED')
                                            <span class="badge bg-success">{{ str_replace("_"," ",$value["order_status"]) }}</span>
                                        @else @if($value["order_status"] == 'CANCELLED' || $value["order_status"] == 'LAPSED' || $value["order_status"] == 'REJECTED')
                                            <span class="badge bg-danger">{{ str_replace("_"," ",$value["order_status"]) }}</span>

                                        @else
                                            <span class="badge bg-warning text-dark">{{ str_replace("_"," ",$value["order_status"]) }}</span>
                                        @endif
                                        @endif

                                    </td>
                                    <td class="text-right">

                                        <div class="ordlst-hlink-block">
                                            <div class="dropdown order_dtl_drop">
                                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
                                                    
                                                    <table class="table table-bordered mb-0">
                                                        <thead>
                                                            <th class="text-nowrap">
                                                                Item Id
                                                            </th>
                                                            <th class="text-nowrap">
                                                                Item Status
                                                            </th>
                                                        </thead>
                                                        @if(isset($value["order_item"]))
                                                            @foreach($value["order_item"] as $item_value)
                                                                <tr>
                                                                    <td>
                                                                        #{{ $item_value['display_item_id'] }}
                                                                    </td>
                                                                    <td>
                                                                        @if($item_value["item_status"] == 'DELIVERED')
                                                                            <span class="badge bg-success">{{ $item_value["item_status"] }}</span>
                                                                            
                                                                        @else @if($item_value["item_status"] == 'CANCELLED' || $item_value["item_status"] == 'REJECTED' || $item_value["item_status"] == 'LAPSED')
                                                                            <span class="badge bg-danger">{{ $item_value["item_status"] }}</span>
                                                                            @if($item_value["item_status"] == 'LAPSED')
                                                                                (Qty not assigned)
                                                                            @endif
                                                                            
                                                                        @else @if($item_value["item_status"] == 'PICKUP')
                                                                            <span class="badge bg-purple">{{ $item_value["item_status"] }}</span>
                                                                            
                                                                        @else
                                                                            <span class="badge bg-warning text-dark">{{ str_replace('_',' ',$item_value["item_status"]) }}</span>
                                                                            @if($item_value["item_status"] == 'RECEIVED')
                                                                                (Confirmation pending from RMC supplier)
                                                                            @endif
                                                                            
                                                                        @endif
                                                                        @endif
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </table>
                                                    <!-- <li><a class="dropdown-item" href="#">7th day Cube test report</a></li>
                                                    <li><a class="dropdown-item" href="#">28th day Cube test report</a></li> -->
                                                </ul>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" style="text-align: center;">No Record Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="data-box-footer clearfix">

                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        
                        <div class="pagination cust_pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{route(Route::current()->getName())}}" method="get">
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="btn btn-primary btn-secondary me-3">Previous</button>
                            </form>
                            @endif
                        @endif
                        @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{route(Route::current()->getName())}}" method="get" class="m-l10">
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="btn btn-primary btn-secondary">Next</button>
                            </form>
                            @endif
                        @endif
                        </div>
                        
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- PRODUCT LISTING BLOCK END -->

 <!-- Offcanvas Coupon start-->
 <div class="ofcnvs_coupn">
    <button type="button" class="ofrSldm-toggle ofrSldm-hamburger">
        <span class="sr-only">toggle navigation</span>
        <span class="ofrSldm-hamburger-icon"></span>
    </button>

    <nav class="ofrSldm-nav" role="navigation">
        <div class="ofrSldm-menu">
            <div class="ofr_list_blk">
                <h3>Fillter</h3>
                <form action="{{route(Route::current()->getName())}}" method="GET">
                    <div class="fillter_list my-5">
                        <div class="mb-3">
                            <div class="custome_select">
                                <select name="site_id" class="form-control profile_ele form-select valid" aria-invalid="false">
                                    <option value="">Select Delivery Site</option>
                                    @if(isset($site_data["data"]))
                                        @foreach($site_data["data"] as $value)
                                            <option value="{{ $value['_id'] }}" {{ Request::get('site_id') == $value['_id'] ? 'selected' : '' }} data="{{ json_encode($value) }}">
                                                {{ $value["company_name"] }},
                                                {{ $value["site_name"] }},
                                                {{ $value["address_line1"] }}, {{ $value["address_line2"] }}, {{ $value["city_name"] }} - {{ $value["pincode"] }}, {{ $value["state_name"] }}, ({{ $value["country_name"] }})
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="mb-3">
                            <!-- <label for="" class="form-label">Order Status</label> -->
                            <div class="custome_select">
                                <select name="order_status" class="form-control profile_ele form-select valid" aria-invalid="false">
                                    <option value="">Selected Order Status</option>
                                    <option value="PLACED" {{ Request::get('order_status') == 'PLACED' ? 'selected' : '' }} >PLACED</option>
                                    <option value="PROCESSING" {{ Request::get('order_status') == 'PROCESSING' ? 'selected' : '' }} >PROCESSING</option>
                                    <option value="CONFIRMED" {{ Request::get('order_status') == 'CONFIRMED' ? 'selected' : '' }} >CONFIRMED</option>
                                    <option value="TM_ASSIGNED" {{ Request::get('order_status') == 'TM_ASSIGNED' ? 'selected' : '' }} >TM ASSIGNED</option>
                                    <option value="PICKUP" {{ Request::get('order_status') == 'PICKUP' ? 'selected' : '' }} >PICKUP</option>
                                    <option value="DELAYED" {{ Request::get('order_status') == 'DELAYED' ? 'selected' : '' }} >DELAYED</option>
                                    <option value="DELIVERED" {{ Request::get('order_status') == 'DELIVERED' ? 'selected' : '' }} >DELIVERED</option>
                                    <option value="CANCELLED" {{ Request::get('order_status') == 'CANCELLED' ? 'selected' : '' }} >CANCELLED</option>
                                    <option value="REJECTED" {{ Request::get('order_status') == 'REJECTED' ? 'selected' : '' }} >REJECTED</option>
                                    <option value="LAPSED" {{ Request::get('order_status') == 'LAPSED' ? 'selected' : '' }} >LAPSED</option>
                                </select>
                            </div>
                        </div>
                        <div class="range_picker mb-3">
                            <input class="range_picker_input form-control" id="filter_date_range" type="text"" value="" />
                            <i class="fa fa-calendar"></i>
                                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                        </div>
                        <div class="mb-3">
                            <div class="custome_select">
                                <select name="total_amount_range" class="form-control profile_ele form-select valid" aria-invalid="false">
                                    <option value="less" {{ Request::get('site_id') == 'less' ? 'selected' : '' }}>Lessthan</option>
                                    <option value="more" {{ Request::get('site_id') == 'more' ? 'selected' : '' }}>Greaterthan</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3">
                            <input class="form-control" placeholder="Total Amount" type="text" name="total_amount" value="{{ Request::get('total_amount') ? Request::get('total_amount') : '' }}" />
                        </div>
                        <button type="submit" class="btn btn-primary btn-secondary">SEARCH</button>
                    </div>
                </form>
            </div>
        </div>
    </nav>
</div>
<!-- Offcanvas Coupon end-->

</div>
<!-- CONTENT END -->
<script>
$(function() {

    var today = new Date();
    var end = new Date();
    // today.setDate(today.getDate() + 2);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    end = dd + '/' + mm + '/' + yyyy;
    console.log(today);

    @if(Request::get('start_date'))

        var date = new Date('{{Request::get('start_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        today = dd + '/' + mm + '/' + yyyy;
        // today = yyyy + '-' + mm + '-' + dd;
        console.log("start_date..."+today);
    @endif

    @if(Request::get('end_date'))

        var date = new Date('{{Request::get('end_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        end = dd + '/' + mm + '/' + yyyy;
        // end = yyyy + '-' + mm + '-' + dd;
        console.log("end_date..."+end);
    @endif

  $('#filter_date_range').daterangepicker({
    opens: 'left',
    startDate: today,
    endDate: end,
    locale: {
            format: 'DD/MM/YYYY'
        }
  }, function(start, end, label) {
      $("#report_start_date").val(start.format('YYYY-MM-DD'));
      $("#report_end_date").val(end.format('YYYY-MM-DD'));

        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>

@endsection