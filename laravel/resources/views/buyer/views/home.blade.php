@section('title', 'Home')
@extends('buyer.layouts.buyer_layout')

@section('content')
<?php //dd($cart_details["data"]); ?>
<!-- CONTENT START -->

<div id="carouselExampleIndicators" class="carousel slide mb-5" data-bs-ride="carousel">
    <ol class="carousel-indicators">
        <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
        <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>
        <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{asset('assets/buyer/images/banner1.webp')}}" defer class="d-block w-100" alt="banner">
        </div>
        <div class="carousel-item">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{asset('assets/buyer/images/banner2.webp')}}" defer class="d-block w-100" alt="banner">
        </div>
        <div class="carousel-item">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{asset('assets/buyer/images/banner.webp')}}" defer class="d-block w-100" alt="banner">
        </div>
    </div>
</div>
<section id="home_product_section">
    <div class="container">
        <div class="home_our_product mb-4">
            <h5 class="home_title">OUR PRODUCT</h5>
            <div class="owl-carousel owl-theme">
                @if(isset($data["concrete_grade"]) && count($data["concrete_grade"]) > 0)
                    @foreach($data["concrete_grade"] as $value)
                    <div class="item">
                        @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null) && (Session::get("selected_display_delivery_date") != null))
                            <a href="{{ route('buyer_product_listing',$value['_id']) }}" class="home_our_product_bx mb-4">
                                <div class="home_our_product_bx_img">
                                    <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                </div>
                                <p>{{$value["name"]}}</p>
                            </a>

                        @else

                            <a href="javascript:void(0)" onclick="openLocationDialog('{{ $value['_id'] }}','product_listing')" class="home_our_product_bx mb-4">
                                <div class="home_our_product_bx_img">
                                    <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                </div>
                                <p>{{$value["name"]}}</p>
                            </a>
                        @endif
                    </div>
                    @endforeach
                @endif
            </div>
            <!-- <div class="row">
                @if(isset($data["concrete_grade"]) && count($data["concrete_grade"]) > 0)
                    @foreach($data["concrete_grade"] as $value)
                        <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                        @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null) && (Session::get("selected_display_delivery_date") != null))
                            <a href="{{ route('buyer_product_listing',$value['_id']) }}" class="home_our_product_bx mb-4">
                                <div class="home_our_product_bx_img">
                                    <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                </div>
                                <p>{{$value["name"]}}</p>
                            </a>

                        @else

                            <a href="javascript:void(0)" onclick="openLocationDialog('{{ $value['_id'] }}','product_listing')" class="home_our_product_bx mb-4">
                                <div class="home_our_product_bx_img">
                                    <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                </div>
                                <p>{{$value["name"]}}</p>
                            </a>
                        @endif
                        </div>
                    @endforeach
                @endif
            </div> -->
        </div>
        <div class="add_banner mb-5" style="display:none;">
            <!-- <a href="#"><img src="{{asset('assets/buyer/images/add_banner.png')}}" alt="add banner"></a> -->
        </div>
        <div class="home_our_client mb-5 d-none">
            <h5 class="home_title">OUR CLIENT</h5>
            <!-- <div class="owl-carousel owl-theme">
                <div class="item">
                    <div class="client_bx"><img src="{{asset('assets/buyer/images/client1.png')}}" alt="client"></div>
                </div>
                <div class="item">
                    <div class="client_bx"><img src="{{asset('assets/buyer/images/client2.png')}}" alt="client"></div>
                </div>
                <div class="item">
                    <div class="client_bx"><img src="{{asset('assets/buyer/images/client3.png')}}" alt="client"></div>
                </div>
                <div class="item">
                    <div class="client_bx"><img src="{{asset('assets/buyer/images/client4.png')}}" alt="client"></div>
                </div>
                <div class="item">
                    <div class="client_bx"><img src="{{asset('assets/buyer/images/client1.png')}}" alt="client"></div>
                </div>
                <div class="item">
                    <div class="client_bx"><img src="{{asset('assets/buyer/images/client2.png')}}" alt="client"></div>
                </div>
                <div class="item">
                    <div class="client_bx"><img src="{{asset('assets/buyer/images/client3.png')}}" alt="client"></div>
                </div>
                <div class="item">
                    <div class="client_bx"><img src="{{asset('assets/buyer/images/client4.png')}}" alt="client"></div>
                </div>
            </div> -->
        </div>
    </div>
</section>

    <!-- CONTENT END -->

@endsection