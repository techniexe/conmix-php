<?php //dd($data["data"]); ?>
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="track-block">
    @if(isset($data["data"]))
        <?php $final_data = $data["data"];
        //dd($final_data)?>
    <div class="truck-number-text p-b10"></div>
        @if(false)
        <div class="row p-b20">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="ordlst-id-date-block mb-3">
                    <div class="ordlst-id-block">
                        Order Id: <span>#{{ $final_data["vendor_order_id"] }}</span>
                    </div>
                    <div class="ordlst-date-block">
                        Order Date: <span>{{ date('d M Y, h:i:s a', strtotime($final_data["created_at"])) }}</span>
                    </div>
                    <div class="ordlst-date-block">
                        Delivered Qty : <span>{{ $final_data["pickup_quantity"] }} MT </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="ordlst-id-date-block mb-3">
                    <div class="ordlst-date-block">
                        Operator Name : <span>{{ $final_data["driver_name"] }}</span>
                    </div>
                    <div class="ordlst-date-block">
                        Mobile No. : <span>{{ $final_data["driver_mobile_number"] }}</span>
                    </div>
                    <div class="ordlst-date-block">
                        Truck No : <span>{{ $final_data["TM_rc_number"] }}</span>
                    </div>
                </div>
            </div>
        </div>
        @endif
    <div class="track-process-step1">
        <ol class="progress progress--medium">
            <?php
                $ordered_status_class = 'is-complete';
                $processed_status_class = '';
                $truck_assigned_status_class = '';
                $pickup_status_class = '';
                $delivered_status_class = '';

                $cancel_delivered_status = 'Delivered';

                if(isset($final_data["event_status"])){

                    if($final_data["event_status"] == 'CP_ASSIGNED'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-active';
                        $delivered_status_class = 'progress__last';

                    }else if($final_data["event_status"] == 'PICKUP'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-active';

                    }else if($final_data["event_status"] == 'DELAYED'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-active';

                    }else if($final_data["event_status"] == 'DELAY'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-active';

                    }else if($final_data["event_status"] == 'DELIVERED'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-complete';
                        $pickup_status_class = 'is-complete';
                        $delivered_status_class = 'is-complete';

                    }else{

                        $ordered_status_class = 'progress__last';
                        $processed_status_class = 'progress__last';
                        $truck_assigned_status_class = 'progress__last';
                        $pickup_status_class = 'progress__last';
                        $delivered_status_class = 'progress__last';

                    }


                }else{

                    if($final_data["order_status"] == 'PROCESSING'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-active';
                        $truck_assigned_status_class = 'progress__last';
                        $pickup_status_class = 'progress__last';
                        $delivered_status_class = 'progress__last';

                    }else if($final_data["order_status"] == 'PROCESSED'){

                        $ordered_status_class = 'is-complete';
                        $processed_status_class = 'is-complete';
                        $truck_assigned_status_class = 'is-active';
                        $pickup_status_class = 'progress__last';
                        $delivered_status_class = 'progress__last';

                    }else{

                        $ordered_status_class = 'progress__last';
                        $processed_status_class = 'progress__last';
                        $truck_assigned_status_class = 'progress__last';
                        $pickup_status_class = 'progress__last';
                        $delivered_status_class = 'progress__last';

                    }


                }

            ?>

            <!-- <li class="is-complete" data-step="2">Processed</li> -->
            <li class="{{ $truck_assigned_status_class }}" data-step="1">CP Assigned</li>
            <li class="{{ $pickup_status_class }}" data-step="2">Pickup</li>
            <li class="{{ $delivered_status_class }}" data-step="3">Delivered</li>
        </ol>
    </div>
    <div class="track-details-block">
        <table class="table">
            <thead>
                <tr>
                    <th>Date/Time (As Par Status)</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>

                @if(isset($final_data["assigned_at"]))
                    <tr>
                        <td>
                            {{ AdminController::dateTimeFormat($final_data["assigned_at"]) }}
                        </td>
                        <td>
                            Concrete Pump has been assigned.
                        </td>
                    </tr>
                @endif
                @if(isset($final_data["pickedup_at"]))
                    <tr>
                        <td>
                            {{ AdminController::dateTimeFormat($final_data["pickedup_at"]) }}
                        </td>
                        <td>
                            Concrete Pump has been picked up.
                        </td>
                    </tr>
                @endif
                @if(isset($final_data["delayed_at"]))
                    <tr>
                        <td>
                        {{ AdminController::dateTimeFormat($final_data["delayed_at"]) }}
                        </td>
                        <td style="color: #f00;">
                            Concrete Pump has been delaied {{ $final_data["delayTime"] }} due to {{ $final_data["reasonForDelay"] }}.
                        </td>
                    </tr>
                @endif
                @if(isset($final_data["delivered_at"]))
                <tr>
                    <td>
                        {{ AdminController::dateTimeFormat($final_data["delivered_at"]) }}
                    </td>
                    <td>
                        Concrete Pump has been delivered.
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>


    @else
        <p style="text-align: center;">Concrete pump not assigned yet.</p>
    @endif
</div>