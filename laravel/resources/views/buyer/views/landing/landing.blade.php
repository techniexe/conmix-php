
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <base href="https://conmix.in/" >
   
    <meta name="description" content=" Ready-Mix Concrete supplier | RMC supplier directory | Concrete delivery Services partnership | Concrete supplier network | https://conmix.in/ | https://www.conmix.in/ | Best RMC Material | Buyer | Supplier">
    <meta name="author" content="">
	
	
	
		
		<meta property="og:site_name" content="Conmix RMC Material" />
	
<meta property="og:title" content="ConMix | Book Your RMC Online" />
<meta property="og:description" content="Conmix RMC Material" />
<meta property="og:description" content="Lowest Price | Timely Deliveries | Assured Quality " />

<meta property="og:image" content="{{asset('assets/buyer/landing/assets/images/whatapp.webp')}}" />
 <meta property="og:image:width" content="600">
  <meta property="og:image:height" content="400"> 
 
<meta property="og:url" content="http://conmix.in/" />  
	
	
	
	
	
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!--<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet" defer="defer">-->

    <!-- FAVICONS ICON -->
    <link rel="icon" type="image/png" href="{{asset('assets/buyer/landing/assets/images/app-icon.png')}}" sizes="32x32" async/>
      <link rel="icon" type="image/png" href="{{asset('assets/buyer/landing/assets/images/app-icon.png')}}" sizes="16x16" async/>

    <title>ConMix RMC</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/buyer/landing/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" defer="defer">

 

    <!-- Additional CSS Files --
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous" defer="defer"> 
    <!-- <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/animated.css">
    <link rel="stylesheet" href="assets/css/owl.css"> -->
    <link rel="stylesheet" href="{{ asset('assets/buyer/landing/assets/css/style.css') }}" defer="defer">
    <link rel="stylesheet" href="{{ asset('assets/buyer/landing/assets/css/animated.css') }}" defer="defer">
    <link rel="stylesheet" href="{{ asset('assets/buyer/landing/assets/css/owl.css') }}" defer="defer">

    @if(env('APP_ENV', 'production') == 'production')

         <!-- Google tag (gtag.js) -->
         <script async src="https://www.googletagmanager.com/gtag/js?id=G-L18WBJ16M8"></script>
         <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'G-L18WBJ16M8');
         </script>

        <script type="text/javascript">
            (function(c,l,a,r,i,t,y){
                c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
                t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
                y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
            })(window, document, "clarity", "script", "m223lx17us");
        </script>


      @endif

  </head>

<body>

  <!-- ***** Preloader Start ***** --
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->

  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="main-nav">
            <!-- ***** Logo Start ***** -->
            <a href="index.html" class="logo" style="height:100px;width:120px;">
              <img   src="{{ asset('assets/buyer/landing/assets/images/conmix-logo.webp') }}"    alt="ConMix RMC">
            </a>
            <!-- ***** Logo End ***** -->
            <!-- ***** Menu Start ***** -->
            <ul class="nav">
              <li class="scroll-to-section"><a href="#top" class="active">Home</a></li>
              <li class="scroll-to-section"><a href="#why">Why ConMix?</a></li>
              <li class="scroll-to-section"><a href="#how">How it works?</a></li>
              <li class="scroll-to-section"><a href="#clients">Testimonial</a></li>
              <li class="scroll-to-section"><a href="#contact">Contact Us</a></li>
              <li><div class="gradient-button"><a id="modal_trigger" href="tel:+919979016486"><i class="fa fa-sign-in-alt"></i> Call Now</a></div></li> 
			   <li><div class="gradient-button"><a id="modal_trigger" href="#supplier"><i class="fa fa-user"></i>&nbsp; For Supplier</a></div></li> 
            </ul>        
            </ul>        
            <div class='menu-trigger'>
                <span>Menu</span>
            </div>
            <!-- ***** Menu End ***** -->
          </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- ***** Header Area End ***** -->
  
  <div id="modal" class="popupContainer" style="display:none;">
    <div class="popupHeader">
        <span class="header_title">Login</span>
        <span class="modal_close"><i class="fa fa-times"></i></span>
    </div>

    <section class="popupBody">
        <!-- Social Login -->
        <div class="social_login">
            <div class="">
                <a href="#" class="social_box fb">
                    <span class="icon"><i class="fab fa-facebook"></i></span>
                    <span class="icon_title">Connect with Facebook</span>

                </a>

                <a href="#" class="social_box google">
                    <span class="icon"><i class="fab fa-google-plus"></i></span>
                    <span class="icon_title">Connect with Google</span>
                </a>
            </div>

            <div class="centeredText">
                <span>Or use your Email address</span>
            </div>

            <div class="action_btns">
                <div class="one_half"><a href="#" id="login_form" class="btn">Login</a></div>
                <div class="one_half last"><a href="#" id="register_form" class="btn">Sign up</a></div>
            </div>
        </div>

        <!-- Username & Password Login form -->
        <div class="user_login">
            <form>
                <label>Email / Username</label>
                <input type="text" />
                <br />

                <label>Password</label>
                <input type="password" />
                <br />

                <div class="checkbox">
                    <input id="remember" type="checkbox" />
                    <label for="remember">Remember me on this computer</label>
                </div>

                <div class="action_btns">
                    <div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i> Back</a></div>
                    <div class="one_half last"><a href="#" class="btn btn_red">Login</a></div>
                </div>
            </form>

            <a href="#" class="forgot_password">Forgot password?</a>
        </div>

        <!-- Register Form -->
        <div class="user_register">
            <form>
                <label>Full Name</label>
                <input type="text" />
                <br />

                <label>Email Address</label>
                <input type="email" />
                <br />

                <label>Password</label>
                <input type="password" />
                <br />

                <div class="checkbox">
                    <input id="send_updates" type="checkbox" />
                    <label for="send_updates">Send me occasional email updates</label>
                </div>

                <div class="action_btns">
                    <div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i> Back</a></div>
                    <div class="one_half last"><a href="#" class="btn btn_red">Register</a></div>
                </div>
            </form>
        </div>
    </section>
</div>

  <div class="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-6 align-self-center">
              <div class="left-content show-up header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                <div class="row">
                  <div class="col-lg-12" >
                    <h2 >Streamline Your RMC Procurement Process Today</h2>
                    <!--<p>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>-->
                  </div>
                  <div class="col-lg-12">
                    <!--<div class="white-button first-button scroll-to-section">
                      <a href="#contact">Call to Action <i class="fab fa-contact"></i></a>
                    </div>-->
                    <div class="gradient-button scroll-to-section" >
                      <!-- <a href="{{ route('buyer_landing_home') }}" ><font class="whitefont">Order Now <i class="fas fa-arrow-alt-circle-right"></i></font></a> -->
                      <a href="{{ route('buyer_home')}}?from_landing=yes" ><font class="whitefont">Order Now <i class="fas fa-arrow-alt-circle-right"></i></font></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                <img src="{{ asset('assets/buyer/landing/assets/images/slider4.webp') }}" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div id="why" class="about-us section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 align-self-center">
          <div class="section-heading">
           <h4>Why Choose <em>ConMix?</em></h4>
            <img src="{{ asset('assets/buyer/landing/assets/images/heading-line-dec.webp') }}" alt="">
           <!--<p>Lorem Ipsum Text</p>-->
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="box-item">
                <h4><img src="{{ asset('assets/buyer/landing/assets/images/competitive-price.webp') }}" style="height:40px;width:40px;">&nbsp;&nbsp;<a href="#">Competitive Pricing Across the city</a></h4>
                <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
            <div class="col-lg-12">
              <div class="box-item">
                <h4><img src="{{ asset('assets/buyer/landing/assets/images/timely-delivery.webp') }}" style="height:40px;width:40px;">&nbsp;&nbsp;<a href="#">Assured timely deliveries &nbsp; </a> </h4>
                 <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
            <div class="col-lg-12">
              <div class="box-item">
                <h4><img src="{{ asset('assets/buyer/landing/assets/images/quality&reliability.webp') }}" style="height:40px;width:40px;">&nbsp;&nbsp;<a href="#">Ensure Quality &amp; Reliability</a></h4>
                 <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
            <div class="col-lg-12">
              <div class="box-item">
                <h4><img src="{{ asset('assets/buyer/landing/assets/images/flexiblepayment.webp') }}" style="height:40px;width:40px;">&nbsp;&nbsp;<a href="#">Flexible Payment Option</a></h4>
                <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
			   <div class="col-lg-12">
              <div class="box-item">
                <h4><img src="{{ asset('assets/buyer/landing/assets/images/accessnetwork.webp') }}" style="height:40px;width:40px;">&nbsp;&nbsp;<a href="#">Access a network of trusted suppliers nationwide</a></h4>
                <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
            <div class="col-lg-12">
              <!--<p>Lorem Ipsum Text</p>--
              <div class="gradient-button">
                <a href="#">Call to Action</a>
              </div>
            <!--  <span>*bbbbbbbbbbbbbbbbbbb</span>-->
            </div>
          </div>
        </div>
         <div class="col-lg-6">
          <div class="right-image">
           <!-- <img src="assets/images/why.png" alt="">-->
			<video controls autoplay muted   class="conmixvideo">
  <source src="{{ asset('assets/buyer/landing/assets/images/conmixintroduction.mp4') }}" type="video/mp4">
 <!-- <source src="movie.ogg" type="video/ogg">-->
  Your browser does not support the video tag.
</video>
          </div>
        </div>
      </div>
    </div>
  </div>

<div id="how" class="services section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 offset-lg-2">
          <div class="section-heading  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
            <h4 style="font-size:48px;">How it <em >Works?</em></h4>
           <img src="{{ asset('assets/buyer/landing/assets/images/heading-line-dec.webp') }}" alt="">
           <!---------- <p>If you need the greatest collection of HTML templates for your business, please visit <a rel="nofollow" href=" " target="_blank">TooCSS</a> Blog. If you need to have a contact form PHP script, go to <a href=" " target="_parent">our contact page</a> for more information.</p>----->
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">

      <div class="col-lg-3">
          <div class="service-item first-service" align="center">
            <div class="icon howitdiv"></div>
            <h4>Submit Your RMC Requirements</h4>
         <!--   <p>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>
            <div class="text-button">
              <a href="#">Read More <i class="fa fa-arrow-right"></i></a>
            </div>-->
          </div>
        </div>

 <div class="col-lg-3">
          <div class="service-item second-service" align="center">
            <div class="icon howitdiv"></div>
            <h4>Choose verified suppliers instantly</h4>
          <!--   <p>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>
            <div class="text-button">
              <a href="#">Read More <i class="fa fa-arrow-right"></i></a>
            </div>-->
          </div>
        </div>
        <div class="col-lg-3">
          <div class="service-item third-service" align="center">
            <div class="icon howitdiv"></div>
            <h4>Assured Quality & hassle free RMC delivery</h4>
            <!--   <p>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>
            <div class="text-button">
              <a href="#">Read More <i class="fa fa-arrow-right"></i></a>
            </div>-->
          </div>
        </div>
        <div class="col-lg-3">
          <div class="service-item fourth-service" align="center">
            <div class="icon howitdiv" align="center"></div>
            <h4>Choose desired payment option</h4>
            <!--   <p>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>
            <div class="text-button">
              <a href="#">Read More <i class="fa fa-arrow-right"></i></a>
            </div>-->
          </div>
        </div>
		
		</div>
    </div>
  </div>

  <div id="supplier" class="about-us section">
  
     <div class="container" align="center">
      <div class="row">
        <div class="col-lg-8 offset-lg-2">
          <div class="section-heading  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
            <h4 style="font-size:48px;">Sell <em >RMC</em> with ConMix</h4>
           <img src="{{ asset('assets/buyer/landing/assets/images/heading-line-dec.webp') }}" alt="">
         
          </div>
        </div>
      </div>
    </div>
  
  <div class="container">
      <div class="row">
        <div class="col-lg-6 align-self-center">
          <div class="section-heading">
           <h3>  <em>"Be Competitive with pricing, Get Paid the Next Day"</em></h3>
            <img src="{{ asset('assets/buyer/landing/assets/images/heading-line-dec.webp') }}" alt="">
           <!--<p>Lorem Ipsum Text</p>-->
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="box-item">
                <h4><i class="fas fa-expand-arrows-alt"></i>&nbsp;&nbsp;<a href="#">Expand market reah across the city</a></h4>
                <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
            <div class="col-lg-12">
              <div class="box-item">
                <h4><i class="fas fas fas fa-chart-line"></i>&nbsp;&nbsp;<a href="#">Higher ROI with assured business opportunities &nbsp; </a> </h4>
                 <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
            <div class="col-lg-12">
              <div class="box-item">
                <h4><i class="fas fa-business-time"></i>&nbsp;&nbsp;<a href="#">Assured timely payments</a></h4>
                 <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
            <div class="col-lg-12">
              <div class="box-item">
                <h4><i class="far fa-check-square"></i>&nbsp;&nbsp;<a href="#">Showcase quality through rigorous testing</a></h4>
                <!--<p>Lorem Ipsum Text</p>-->
              </div>
            </div>
			 
            <div class="col-lg-12">
              <!--<p>Lorem Ipsum Text</p>-->
              <div class="gradient-button">
                &nbsp; <a href="{{ route('supplier_login') }}?from_landing=yes">Become a Supplier</a>
              </div>
            <!--  <span>*bbbbbbbbbbbbbbbbbbb</span>-->
            </div>
          </div>
        </div>
        <div class="col-lg-6 hide">
          <div class="right-image">
            <img src="{{ asset('assets/buyer/landing/assets/images/supplier1.webp') }}"  alt="">
          </div>
        </div>
      </div>
    </div>
  </div>

<div id="clients" class="the-clients" >
    <div class="container">
      <div class="row">
        <div class="col-lg-8 offset-lg-2">
          <div class="section-heading">
             <h4 style="font-size:48px;">What our Customer Say</h4>
            <img src="{{ asset('assets/buyer/landing/assets/images/heading-line-dec.webp') }}" alt="">
           <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eismod tempor incididunt ut labore et dolore magna.</p>-->
          </div>
        </div>
		
		</div>
  </div>
 </div>
 
 <section class="container" >
      <div class="testimonial mySwiper" align="center" >
        <div class="testi-content swiper-wrapper" align="center">
          <div class="slide swiper-slide">
            <img src="{{ asset('assets/buyer/landing/assets/images/client.webp') }}" alt="" class="image" />
            <p>
              ConMix revolutionized how we procure RMC.   Efficient, reliable and hassle-free!
            </p>

            <i class="bx bxs-quote-alt-left quote-icon"></i>

            <div class="details">
              <span class="name">Sagar</span>
              <span class="job">Construction Manager</span>
            </div>
          </div>
          <div class="slide swiper-slide">
            <img src="{{ asset('assets/buyer/landing/assets/images/fclient.webp' ) }}" alt="" class="image" />
            <p>
            Thanks to ConMix, we've saved time and money on our construction projects. Highly recommend!
            </p>

            <i class="bx bxs-quote-alt-left quote-icon"></i>

            <div class="details">
              <span class="name">Sarah</span>
              <span class="job">Project Engineer</span>
            </div>
          </div>
       
        </div>
        <div class="swiper-button-next nav-btn"></div>
        <div class="swiper-button-prev nav-btn"></div>
        <div class="swiper-pagination"></div>
      </div>
   

    <!-- Swiper JS -->
    <script src="{{ asset('assets/buyer/landing/testimonial/swiper-bundle.min.js') }}"></script>

    <!-- JavaScript -->
    <script src="{{ asset('assets/buyer/landing/testimonial/script.js') }}"></script>
	 <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{ asset('assets/buyer/landing/testimonial/swiper-bundle.min.css') }}" />

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('assets/buyer/landing/testimonial/style.css') }}" />

    <!-- Boxicons CSS --
    <link
      href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet"/>-->
		</section>
 
 <footer id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 offset-lg-2">
          <div class="section-heading">
            <h4>Get in Touch with ConMix</h4>
          </div>
        </div>
		 
        <div class="col-lg-6 offset-lg-3">
          <form id="search" action="#" method="GET">
            <div class="row">
              <div class="col-lg-6 col-sm-6">
               <!-- <fieldset>
                  <input type="address" name="address" class="email" placeholder="Email Address..." autocomplete="on" required>
                </fieldset>-->
              </div>
              <div class="col-lg-6 col-sm-6">
                <!--<fieldset>
                  <button type="submit" class="main-button">Subscribe Now <i class="fa fa-angle-right"></i></button>
                </fieldset>-->
              </div>
            </div>
          </form>
        </div> 
		 
      </div>
      <div class="row mob">
        <div class="col-lg-3">
          <div class="footer-widget">
            <h4>Contact Us</h4>
            <p>SCC House, Opp. Nirma University, </p>
			<p>Nr. Balaji Temple,S. G. Highway, Chharodi,</p>
			<p> Ahmedabad-382481,Gujarat, INDIA.</p>
            <p><a href="tel:+919979016486">+91 9979016486</a></p>
            <p><a href="mailto:support@conmix.in">support@conmix.in</a></p>
			<p><a href="https://play.google.com/store/search?q=conmix&c=apps&hl=en_IN&gl=US"><img src="{{ asset('assets/buyer/landing/assets/images/playstore.webp') }}" style="height:50px;width:200px;"/></a></p>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="footer-widget">
            <h4>ConMix</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">Why ConMix?</a></li>
              <li><a href="#">How it Works?</a></li>
              <li><a href="#">Testimonials</a></li>
              <li><a href="#">Contact Us</a></li>
            </ul>
            <ul>
             <!-- <li><a href="#">About</a></li>
              <li><a href="#">Testimonials</a></li>
              <li><a href="#">Pricing</a></li>-->
            </ul>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="footer-widget">
            <h4>Legal</h4>
            <ul>
              <li><a href="https://conmix.in/terms_of_use">Terms of Usage</a></li>
              <li><a href="https://conmix.in/privacy_policy">Privacy Policy</a></li>
              <li><a href="https://conmix.in/return_policy">Cancellations, Returns, and Refund Policy</a></li>
               
            </ul>
           <!-- <ul>
              <li><a href="#">ffff</a></li>
              <li><a href="#">ggggg</a></li>
              <li><a href="#">hhhhh</a></li>
            </ul>-->
          </div>
        </div>
        <div class="col-lg-3">
          <div class="footer-widget">
            <h4>About ConMix</h4>
            <!--<div class="logo"  >
              <img src="assets/images/conmix-logo.png"   alt="ConMix RMC">
            </div>-->
            <p>ConMix is a digital platform revolutionizing the construction industry's Ready-Mix Concrete (RMC) procurement process. Our platform connects buyers with verified suppliers, ensuring quality, reliability, and efficiency throughout the procurement journey.</p>
			
	 	  <p>
		  <a href="#" target="_blank"><img src="{{ asset('assets/buyer/landing/assets/images/instagram.svg') }}" style="height:30px;width:30px;" /></a>&nbsp;&nbsp;
		   <a href="https://www.facebook.com/profile.php?id=61559346689816" target="_blank"> <img src="{{ asset('assets/buyer/landing/assets/images/facebook.svg') }}" style="height:30px;width:30px;" /></a>&nbsp;&nbsp;
		    <a href="https://www.linkedin.com/company/conmixrmc/about/" target="_blank"> <img src="{{ asset('assets/buyer/landing/assets/images/linkedin.svg') }}" style="height:30px;width:30px;" /></a>&nbsp;&nbsp;
			 <a href="https://www.youtube.com/channel/UCOuZD9gxBWSJkPWCPhL94nA" target="_blank"> <img src="{{ asset('assets/buyer/landing/assets/images/youtube.svg') }}" style="height:40px;width:40px;" /></a>&nbsp;&nbsp;
			 <!--<img src="{{ asset('assets/buyer/landing/assets/images/whatsapp.svg') }}" style="height:40px;width:40px;" />-->
		  </p>
			
			
			
			
			
			
			
			
			
          </div>
        </div>
        <div class="col-lg-12">
          <div class="copyright-text">
            <p>Copyright © 2024 ConMix. All Rights Reserved. 
          <br>Design: <a href="https://techniexe.com/" target="_blank" title="css templates">Techniexe</a></p>
          </div>
        </div>
      </div>
    </div>
  </footer>
 

  <!-- Scripts -->
  <script src="{{ asset('assets/buyer/landing/vendor/jquery/jquery.min.js') }}" defer="defer"></script>
  <script src="{{ asset('assets/buyer/landing/vendor/bootstrap/js/bootstrap.bundle.min.js') }}" defer="defer"></script>
  <script src="{{ asset('assets/buyer/landing/assets/js/owl-carousel.min.js') }}" defer="defer"></script>
  <script src="{{ asset('assets/buyer/landing/assets/js/animation.js') }}" defer="defer"></script>
  <script src="{{ asset('assets/buyer/landing/assets/js/imagesloaded.js') }}" defer="defer"></script>
  <script src="{{ asset('assets/buyer/landing/assets/js/popup.js') }}" defer="defer"></script>
  <script src="{{ asset('assets/buyer/landing/assets/js/custom.js') }}" defer="defer"></script>
</body>
</html>