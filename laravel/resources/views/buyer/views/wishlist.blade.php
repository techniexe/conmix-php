@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- SECTION CONTENT -->
<div class="section-full p-tb50">
    <div class="container">
        <div class="section-content product-listing-tab-block">
        <?php //dd($wish_list["data"]); ?>
            <h2 style="margin: 0px 0px 0px 0px; font-size: 18px;">Wishlist (<?php  echo count($wish_list["data"]); ?>)</h2>

            <div class="tabs-container style-1">
                <div class="swiper-tabs tabs-switch">
                    <div class="title">Product info</div>
                    
                </div>
                <div>
                    <div class="tabs-entry">
                        <div class="article-container style-1">
                            <div class="ptabs-content-block wow fadeIn" data-wow-delay="0.2s">
                                <div class="product-shw-ml">
                                
                                @if(isset($wish_list["data"]))
                                    @if(count($wish_list["data"]) > 0)

                                        @foreach($wish_list["data"] as $value)
                                        <?php //dd($value); ?>
                                            <div class="product-lst-sllr-block">
                                                <a href="javascript:;" onclick="deleteWishListItem('{{ csrf_token() }}','{{ $value['product_id'] }}','item_{{ $value['_id'] }}','{{ count($wish_list["data"]) }}')" class="addtowishlist active" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                <a href="{{ route('buyer_product_detail',$value['product_id']) }}" class="plst-sllr-blk">
                                                    <div class="plst-img-block">
                                                        <img src="{{ $value['productCategory']['image_url'] }}" alt="" class="img-responsive" />
                                                    </div>
                                                    <div class="plst-detls-block">
                                                        <div class="ptabs-details-block">

                                                            <h3>Seller : {{ $value["supplier"]["full_name"] }}</h3>
                                                                <h6>{{ $value['productCategory']['category_name'] }} : {{ $value['productSubCategory']['sub_category_name'] }} 
                                                                   <span class="st-rating verygood"><i class="fa fa-star"></i> {{ $value["product"]["rating_count"] > 0 ? number_format(($value["product"]["rating_sum"] / $value["product"]["rating_count"]),1) : 0 }} </span></h6>
                                                                <h6><span>Source : </span>  <span>{{ $value["address"]["source_name"] }}</span></h6>
                                                                <div class="prd-price-block price" style="margin-bottom: 0px;">
                                                                    <span class="prd-amunt"><i class="fa fa-rupee"></i>{{ number_format($value["product"]["price_with_margin_and_logistics"],2) }}</span>
                                                                    <span style="font-size: 14px;">Per {{ $value["productSubCategory"]["selling_unit"][0] }}</span>
                                                                    <span style="font-size: 12px;color: #f00;">(Excl. GST)</span>
                                                                </div>
                                                                @if($value["quantity"] > 0)
                                                                <div class="availability_stock">Availability: <span class="in_stock">In Stock</span> ({{ $value["product"]["quantity"] }} {{ $value["productSubCategory"]["selling_unit"][0] }})</div>
                                                                @else
                                                                    <div class="availability_stock">Availability: <span class="out_stock">Out Of Stock</span> (0 {{ $value["productSubCategory"]["selling_unit"][0] }})</div>
                                                                @endif


                                                     <div class="product-lst-info-block m-l50" style="margin-top: 10px;">
                                                     <input type="hidden" value="{{ $value['product']['minimum_order'] }}" id="product_page_minimum_order" />
                                                    <?php 
                                                        $min_order_quantity = $value['product']["minimum_order"];
                                                            
                                                    ?>    
                                                    <button type="button" onclick="addToCart('{{ csrf_token() }}','{{ $value['product_id'] }}','{{ $min_order_quantity }}','slide_cart')" class="site-button yellow m-r15">ADD TO CART</button>
                                                </div>
                                            </div>


                                                           <!--  <h6> {{ $value['productCategory']['category_name'] }} - {{ $value['productSubCategory']['sub_category_name'] }} </h6>
                                                            <h3>Seller: {{ $value["supplier"]["full_name"] }}</h3>
                                                            <div>
                                                                <span class="nav-cart-item-price">{{ $value["quantity"] }} Ton x <i class="fa fa-rupee"></i> {{ $value["product"]["unit_price"] }} <span class="nav-cart-item-total">= <i class="fa fa-rupee"></i> <?php echo ($value["quantity"] * $value["product"]["unit_price"]); ?></span></span>
                                                            </div>
                                                            <br/>                -->                                            
                                                                
                                                        </div>
                                                    </div>
                                                </a>
                                              
                                        @endforeach
                                        @else
                            
                                        <div class="empty_cart_sidebar">
                                            <div class="empty_cart_sidebar_img">
                                                <img src="{{asset('assets/buyer/images/wishlist.png')}}" alt="Empty screen">
                                            </div>
                                            <div class="empty_cart_sidebar_text">No Item in wishlist</div>
                                        </div>

                                        @endif
                                    @else
                                    
                                    <div class="empty_cart_sidebar">
                                        <div class="empty_cart_sidebar_img">
                                            <img src="{{asset('assets/buyer/images/wishlist.png')}}" alt="Empty screen">
                                        </div>
                                        <div class="empty_cart_sidebar_text">No Item in wishlist</div>
                                    </div>

                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PRODUCT LISTING BLOCK END -->


@endsection