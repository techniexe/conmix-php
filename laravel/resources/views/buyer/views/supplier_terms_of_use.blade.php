@section('title', 'Supplier Terms and Conditions')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Supplier Terms of Use and Conditions</span>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">
                <div class="mb-2">
                    <h6>Supplier Terms of Use and Conditions 
 </h6>
                    <p>Last Updated: 25th April 2024</p>
                    <p>ConMix - A product of Conmate E-Commerce Pvt. Ltd.</p>
                    <p>
                    Welcome to ConMix, an online platform delivering Ready Mix Concrete (RMC) supply to its Customers through registered and verified RMC Suppliers. These Terms and Conditions ("Terms") govern your access to and use of the ConMix Platform, including any content, functionality, and services offered on or through the Platform. Please read these Terms carefully before using our Platform.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>Definitions:</h6>
                    <p><b>Company:</b> (referred to as either "the Company", "We", "Us", or "Our" in this Agreement) refers to ConMix, the provider of the online Ready Mix Concrete (RMC) booking platform.
                    </p>
                    
                    <p><b>RMC (Ready Mix Concrete):</b> refers to concrete that is manufactured in a batching plant, according to a set engineered mix design as per IS Codes (IS10262, IS456), and then delivered to a construction site by truck-mounted transit mixers.
                    </p>
                    
                    <p><b>Customer:</b> (Referred to as either ”Client”,”User” Or “Buyer”) refers to any individual or entity that accesses or uses the ConMix platform to book RMC requirements.
                    </p>
                    
                    <p><b>RMC Partner:</b> refers to any supplier or vendor that provides RMC through the ConMix platform online.
                    </p>
                    
                    <p><b>Service:</b> refers to the online platform provided by ConMix for ordering RMC requirements. Customer can also order RMC through the official ConMix mobile application available on the Google Play store, to be downloaded through the link available on the ConMix Platform
                    </p>
                    
                    <p><b>Booking:</b> refers to an order of RMC placed by a Buyer through the ConMix platform. 
                    </p>
                    
                    <p><b>Parties:</b> refer to ConMix’s Customer and RMC Partner.
                    </p>
                    
                    <p><b>ConMix Platform:</b> refers to ConMix website and ConMix mobile app available on Google Playstore and Apple Appstore downloaded through the link available on our ConMix website. ConMix Tracking App is also a part of ConMix Platform.
                    </p>
                    
                    <p><b>ConMix Supplier Account:</b> consists of RMC Order Management Software designed by ConMix.
                    </p>
                    
                    <p><b>Website:</b> refers to the ConMix website, accessible from <a href="www.ConMix.in">www.ConMix.in</a>.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>1. Acceptance of Terms</h6>
                    <p>By accessing or using the ConMix platform, you agree to be bound by these Terms of Use and Conditions. If you do not agree to these terms, you may not access or use the software provided through the ConMix platform.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>2. Use of Software & Platform</h6>
                    <p>You may use the RMC Order Management Software for Suppliers available through the ConMix platform solely for the purpose of managing orders from buyers as provided by the ConMix. You agree not to use the software for any unlawful, illegal or unauthorised purpose or in any manner that violates applicable laws and regulations.
                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>3. User Accounts</h6>
                    <p>You may be required to create an authentic user account for Supplier to access features of the ConMix RMC order management software and thereby to conduct business with ConMix. 
You are responsible for maintaining the confidentiality of your account credentials and for any activity that occurs under your account at all times.



                    </p>
                </div>
                <div class="mb-2">
                    <h6>4. Intellectual Property</h6>
                    <p>The said RMC Order Management Software by ConMix and its content, including but not limited to text, graphics, logos, and software code, are the sole property of ConMix and are protected by copyright and other applicable intellectual property laws in India.
                    </p>
                    <p>You may not modify, reproduce, tamper, hack, distribute, or create derivative works based on the said RMC order management software or its services without prior written duly signed consent from ConMix.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>5. Data Security</h6>
                    <p>You agree to take reasonable measures to protect your account credentials and personal information from unauthorised access or disclosure and will be your sole responsibility.

                    </p>
                    <p>ConMix employs industry-standard security measures to protect the information stored in the Supplier Order Management Software at ConMix platform, but cannot guarantee its absolute security.

                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>6. Limitation of Liability</h6>
                    <p>ConMix shall not be liable for any direct, indirect, incidental, special, or consequential damages arising out of or in connection with your use of the RMC Order Management Software or its services available through Supplier Account on ConMix platform.
                    </p>
                    
                    <p>In no event shall ConMix's total liability exceed the amount paid by you, if any, for registration of the Supplier Account at ConMix platform.
                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>7. Indemnification</h6>
                    <p>You agree to indemnify and hold harmless ConMix, its directors, its affiliates, its associates and their respective officers, employees, and agents from any claims, losses, damages, liabilities, costs, and expenses arising out of or in connection with your use of the Supplier Account on ConMix platform.

                    </p>
                    
                </div>
                <div class="mb-2">
                    <h6>8. Termination</h6>
                    <p>ConMix reserves the right to terminate your Supplier account or suspend your access to the RMC Order Management Software at any time and for any reason without prior notice. Upon termination and/or suspension, your rights to use the ConMix Supplier Account and its RMC Order Management Software will cease immediately.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>9. Governing Law</h6>
                    <p>These Terms of Use and Conditions shall be governed by and construed by the laws of the courts of Ahmedabad, without regard to its conflict of law provisions.


                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>10. Changes to Terms</h6>
                    <p>ConMix reserves the right to update or modify these Terms of Use and Conditions at any time without prior notice. Any changes will be effective immediately upon posting or publishing on the ConMix website. 
                    </p>
                </div>
                <div class="mb-2">
                    <h6>11. Contact Us</h6>
                    <p>If you have any questions or concerns about these Terms of Use and Conditions, please contact us at <a href="mailto:support@conmix.in">support@conmix.in</a>.

                    </p>
                </div>
                
                <!-- <div class="mb-2">
                    <h6>2. Use of Website</h6>
                    <ul class="mb-3">
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>Lorem Ipsum is simply dummy</h6>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div> -->
                <!-- <div class="mb-2">
                    <h6>Terms & Conditions</h6>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div> -->
            </div>
        </div>
    </section>
    <!-- ABOUT COMPANY SECTION END -->
    
    <!-- REQUEST A QUOTE SECTION START -->
    <!-- <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                    <p>Have any ideas in your mind?</p>
                    <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                </div>
                <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                    <div class="button_request">
                        <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- REQUEST A QUOTE SECTION END -->
</div>
<!-- CONTENT END -->

@endsection