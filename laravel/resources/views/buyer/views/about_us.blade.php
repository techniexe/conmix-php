@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
            <!-- BREADCRUMB ROW -->
            <div class="bg-gray-light p-tb20">
                <div class="container">
                    <ul class="wt-breadcrumb breadcrumb-style-2">
                        <li><a href="{{ route('buyer_home') }}">Home</a></li>
                        <li>About Us</li>
                    </ul>
                </div>
            </div>
            <!-- BREADCRUMB ROW END -->
            <!-- ABOUT COMPANY SECTION START -->
            <div class="section-full p-tb80 about-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-sm-7 col-xs-12 wow fadeInLeft" data-wow-delay="0.3s">
                            <div class="section-head text-justify p-r15">
                                <h2>Our Story</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat consequat enim, non auctor massa ultrices non. Morbi sed odio massa. Quisque at vehicula tellus, sed tincidunt augue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas varius egestas diam, eu sodales metus scelerisque congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas gravida justo eu arcu egestas convallis. Nullam eu erat bibendum, tempus ipsum eget, dictum enim. Donec non neque ut enim dapibus tincidunt vitae nec augue. Suspendisse potenti. Proin ut est diam. Donec condimentum euismod tortor,
                                </p>
                                <p>
                                    Donec gravida lorem elit, quis condimentum ex semper sit amet. Fusce eget ligula magna. Aliquam aliquam imperdiet sodales. Ut fringilla turpis in vehicula vehicula. Pellentesque congue ac orci ut gravida. Aliquam erat volutpat. Donec iaculis lectus a arcu facilisis, eu sodales lectus sagittis. Etiam pellentesque, magna vel dictum rutrum, neque justo eleifend elit, vel tincidunt erat arcu ut sem. Sed rutrum, turpis ut commodo efficitur, quam velit convallis ipsum, et maximus enim ligula ac ligula.
                                </p>
                                <p>
                                    Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879 
                                </p>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  wow fadeInRight" data-wow-delay="0.3s">
                            <div class="about-com-pic">
                                <img src="{{ asset('assets/buyer/images/about-pic.jpg') }}" alt="" class="img-responsive"/>
                            </div>
                        </div>
                        
                        <div class="clearfix spacign100"></div>
                        
                        <div class="col-md-7 col-sm-7 col-xs-12 col-md-push-5 col-sm-push-5 col-xs-push-0 wow fadeInRight" data-wow-delay="0.5s">
                            <div class="section-head text-justify">
                                <h2>Our Mission</h2>
                                <p>
                                    Mauris non lacinia magna. Sed nec lobortis dolor. Vestibulum rhoncus dignissim risus, sed consectetur erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam maximus mauris sit amet odio convallis, in pharetra magna gravida. Praesent sed nunc fermentum mi molestie tempor. Morbi vitae viverra odio. Pellentesque ac velit egestas, luctus arcu non, laoreet mauris. Sed in ipsum tempor, consequat odio in, porttitor ante. Ut mauris ligula, volutpat in sodales in, porta non odio. Pellentesque tempor urna vitae mi vestibulum, nec venenatis nulla lobortis. Proin at gravida ante. Mauris auctor purus at lacus maximus euismod. Pellentesque vulputate massa ut nisl hendrerit, eget elementum libero iaculis. 
                                </p>
                                <p class="stylis-text">
                                    Creativity is just connecting things. When you ask creative people how they did something, they feel a little guilty because they didn't really do it, they just saw something. It seemed obvious to them after a while.
                                    
                                    <strong>Ceo Name</strong>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12 col-md-pull-7 col-sm-pull-7 col-xs-pull-0 wow fadeInLeft" data-wow-delay="0.5s">
                            <div class="about-com-pic">
                                <img src="{{ asset('assets/buyer/images/about-pic-2.jpg') }}" alt="" class="img-responsive"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
            <!-- ABOUT COMPANY SECTION END -->
            

            <!-- REQUEST A QUOTE SECTION START -->
            <!-- <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                            <p>Have any ideas in your mind?</p>
                            <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                        </div>
                        <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                            <div class="button_request">
                                <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- REQUEST A QUOTE SECTION END -->
        </div>
        <!-- CONTENT END -->

@endsection