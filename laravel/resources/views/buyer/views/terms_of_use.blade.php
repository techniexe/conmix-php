@section('title', 'Terms and Conditions')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Terms and Conditions</span>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">
                <div class="mb-2">
                    <h6>Terms & Conditions </h6>
                    <p>Last updated: 25th April 2024</p>
                    <p>ConMix - A product of Conmate E-Commerce Pvt. Ltd.</p>
                    <p>
                    Welcome to ConMix, an online platform delivering Ready Mix Concrete (RMC) supply to its Customers through registered and verified RMC Suppliers. 
                    These Terms and Conditions ("Terms") govern your access to and use of the ConMix Platform, including any content, functionality, and services offered on or through the Platform. Please read these Terms carefully before using our Platform.

                    </p>
                </div>
                <div class="mb-2">
                    <h6>Definitions:</h6>
                    <p><b>Company:</b> (referred to as either "the Company", "We", "Us", or "Our" in this Agreement) refers to ConMix, the provider of the online Ready Mix Concrete (RMC) booking platform.
                    </p>
                    
                    <p><b>RMC (Ready Mix Concrete):</b>  refers to concrete that is manufactured in a batching plant, according to a set engineered mix design as per IS Codes (IS10262, IS456), and then delivered to a construction site by truck-mounted transit mixers.
                    </p>
                    
                    <p><b>Customer:</b>  (Referred to as either ”Client”,”User” Or “Buyer”) refers to any individual or entity that accesses or uses the ConMix platform to book RMC requirements.
                    </p>
                    
                    <p><b>RMC Partner:</b>   refers to any supplier or vendor that provides RMC through the ConMix platform online.
                    </p>
                    
                    <p><b>Service:</b> refers to the online platform provided by ConMix for ordering RMC requirements. Customer can also order RMC through the official ConMix mobile application available on the Google Play store, to be downloaded through the link available on the ConMix Platform
                    </p>
                    
                    <p><b>Booking:</b> refers to an order of RMC placed by a Buyer through the ConMix platform.
                    </p>
                    
                    <p><b>Parties:</b> refer to ConMix’s Customer and RMC Partner.
                    </p>
                    
                    <p><b>ConMix Platform:</b> refers to ConMix website and ConMix mobile app available on Google Playstore and Apple Appstore downloaded through the link available on our ConMix website.
                    </p>
                    
                    <p><b>Website:</b> refers to the ConMix website, accessible from <a href="www.ConMix.in">www.ConMix.in</a>.
                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>1. Acceptance of Terms</h6>
                    <p>By accessing or using the ConMix Platform, you agree to be bound by these Terms and all applicable laws and regulations in India. If you do not agree to these Terms, you may not use our ConMix platform.

                    </p>
                </div>
                <div class="mb-2">
                    <h6>2. Use of Website</h6>
                    <p>You agree to use the ConMix platform solely for lawful purposes and in accordance with these Terms. You may not use the Platform in any manner that could damage, disable, overburden, or impair the Platform or interfere with any other party's use of the Platform.

                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>3. User Accounts</h6>
                    <p>To access certain features of the ConMix Platform, you will be required to create an authentic user account. You are responsible for maintaining the confidentiality of your account credentials and for all activities that occur under your account at all times. You agree to provide accurate and complete information while creating an user Account i.e., either Buyer or Supplier account at ConMix. ConMix reserves the right to suspend or terminate your account at its sole discretion if any information provided is found to be inaccurate, incomplete, or misleading or found to be conducting malpractices.


                    </p>
                </div>
                <div class="mb-2">
                    <h6>4. Booking RMC Services</h6>
                    <p>ConMix provides a platform for users (i.e.,  Buyers) to book RMC requirements offered by our registered partners. By booking RMC requirements through our Platform, you agree to abide by the terms and conditions set forth by our partners. This may include but is not limited to pricing, cancellation policies, refunds, and service agreements.

                    </p>
                </div>
                <div class="mb-2">
                    <h6>5. Payment</h6>
                    <p>Payment for RMC services booked through the ConMix Platform is processed electronically. By booking RMC services, you agree to pay the total amount due as specified at the time of booking. Payment methods accepted may vary and are subject to the terms and conditions of our payment processing partners. Failure to make timely payment may result in the cancellation of your booking and additional charges as per the cancellation policy of our ConMix’s Partners.
                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>6. Cancellations and Refunds</h6>
                    <p>Cancellations and refunds for RMC services booked through the ConMix Platform are subject to the cancellation and refund policies of ConMix as defined in the ‘Cancellation, Refund and Return Policy’. Please refer to the specific terms and conditions, if any, provided by our partners for more information. ConMix may facilitate refunds on behalf of partners in accordance with the above said ‘Cancellation, Refund and Return Policy’.
                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>7. Intellectual Property</h6>
                    <p>All content and materials on the ConMix Platform, including but not limited to text, graphics, logos, images, and software, are the property of ConMix or its licensors and are protected by copyright and other intellectual property laws. You may not use, reproduce, tamper, hack or distribute any content from the ConMix Platform without prior written and duly signed permission from ConMix.
                    </p>
                    <p>You are granted a limited, non-exclusive, and non-transferable licence to access and use the ConMix Platform and its content for personal, non-commercial purposes only. This licence does not include the right to:

                    </p>
                    
                    <ul class="mb-3">
                        <li>A. Use the ConMix Platform or its content for any commercial purpose without the express written consent of ConMix.
                        <li>B. Modify, adapt, translate, reverse engineer, decompile, disassemble, or create derivative works based on the ConMix Platform or its content.
                        <li>C. Remove, alter, or obscure any copyright, trademark, or other proprietary notices from the ConMix Platform or its content.</li>
                        
                    </ul>
                    <p>Hyperlinking to content on the ConMix Platform is permitted, provided that the hyperlink:


                    </p>
                    <ul class="mb-3">
                        <li>A. Is not used in a way that implies endorsement, approval, or sponsorship by ConMix.</li>
                        <li>B. Does not portray ConMix in a false, misleading, derogatory, or otherwise offensive manner.</li>
                        <li>C. Is not used for any commercial or fundraising purposes without the express written and duly signed consent of ConMix.</li>
                        
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>8. Limitation of Liability</h6>
                    <p>In no event shall ConMix, its directors, its affiliates, associates or their respective officers, employees, or agents be liable for any direct, indirect, incidental, special, or consequential damages arising out of or in any way connected with your use of the ConMix Platform or the RMC services booked/ordered through our ConMix Platform. This includes, but is not limited to, damages for loss of profits, goodwill, data, or other intangible losses. The total liability of ConMix, its directors, its affiliates, associates, or their respective officers, employees, or agents shall not exceed the amount paid by you for the RMC services booked through our Platform.

                    </p>
                </div>
                <div class="mb-2">
                    <h6>9. Indemnification</h6>
                    <p>You agree to indemnify and hold harmless ConMix, its directors, its affiliates, and their respective officers, employees, and agents from and against any claims, liabilities, damages, losses, or expenses arising out of or in any way connected with your use of the ConMix Platform or the RMC services booked through our Platform. This includes, but is not limited to, your violation of these Terms or any applicable laws or regulations. You agree to cooperate fully in the defence of any claim and to promptly notify ConMix of any such claims.


                    </p>
                </div>
                <div class="mb-2">
                    <h6>10. Changes to Terms</h6>
                    <p>ConMix reserves the right to modify or revise these Terms at any time without prior notice. Your continued use of the ConMix Platform following the posting or publishing of any changes to these Terms constitutes acceptance of those changes by default. It is your responsibility to review these Terms periodically for any updates or changes.


                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>11. Governing Law and Jurisdiction</h6>
                    <p>These Terms shall be governed by and construed in accordance with the laws of the courts of Ahmedabad. Any dispute arising out of or in connection with these Terms shall be subject to the exclusive jurisdiction of the courts of Ahmedabad. 



                    </p>
                </div>
                <div class="mb-2">
                    <h6>12. Contact Information</h6>
                    <p>If you have any questions or concerns about these Terms, please contact us at 
                        <a href="mailto:support@conmix.in">support@conmix.in</a>
                    </p>
                </div>
                <div class="mb-2">
                    <h6>13. Entire Agreement</h6>
                    <p>These Terms constitute the entire agreement between you and ConMix regarding your use of the ConMix Platform and supersede all prior and contemporaneous understandings, agreements, representations, and warranties, both written and oral, regarding such subject matter.


                    </p>
                </div>
                
                <!-- <div class="mb-2">
                    <h6>2. Use of Website</h6>
                    <ul class="mb-3">
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>Lorem Ipsum is simply dummy</h6>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div> -->
                <!-- <div class="mb-2">
                    <h6>Terms & Conditions</h6>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div> -->
            </div>
        </div>
    </section>
    <!-- ABOUT COMPANY SECTION END -->
    
    <!-- REQUEST A QUOTE SECTION START -->
    <!-- <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                    <p>Have any ideas in your mind?</p>
                    <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                </div>
                <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                    <div class="button_request">
                        <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- REQUEST A QUOTE SECTION END -->
</div>
<!-- CONTENT END -->

@endsection