@section('title', 'My Profile')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>My Account</span>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="pro_left_menu wht_bx mb-3">
                    <ul>
                        <li><a href="#" class="active"><i class="far fa-user-circle"></i> My Profile </a></li>
                        <li><a href="{{ route('buyer_show_billing_address') }}"><i class="fas fa-map-marker-alt"></i>Billng Address</a></li>
                        <li><a href="{{ route('buyer_get_site') }}"><i class="fas fa-map-marker-alt"></i>Delivery Address</a></li>
                        <li><a href="{{ route('buyer_show_custom_rmc') }}"><i class="fas fa-map-marker-alt"></i> Custom RMC</a></li>
                        <li><a href="{{ route('buyer_get_password') }}"><i class="fas fa-lock"></i> Change Password</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="profile_bx wht_bx p-4 mb-5">
                    <h5 class="home_title d-flex justify-content-between">
                        <span>My Profile</span>
                        <a href="javascript:void(0)" id="edit_profile"><i class="fas fa-edit"></i> Edit Profile</a>
                    </h5>

                    @php
                        $profile = session('buyer_profile_details', null);
                        //dd($profile);
                    @endphp
                    <form name="account_details_update_form" method="post" enctype="multipart/form-data"  id="account-details" style="display: block;" autocomplete="off">
                    @csrf
                        <div class="profile_fild">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="" class="form-label">Select Account Type <sup>*</sup></label>
                                        <div class="custome_select">
                                            <select name="account_type" id="profile_account_type" class="form-control profile_ele form-select" disabled>
                                                <option value="">Select Account Type</option>
                                                <option value="Builder" {{ $profile["account_type"] == 'Builder' ? 'selected' : '' }}>Builder</option>
                                                <option value="Contractor" {{ $profile["account_type"] == 'Contractor' ? 'selected' : '' }}>Contractor</option>
                                                <option value="Individual" {{ $profile["account_type"] == 'Individual' ? 'selected' : '' }}>Individual</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="" class="form-label">Company Type <sup>*</sup></label>
                                        <div class="custome_select">
                                            <select name="company_type" class="form-control profile_ele form-select" disabled>
                                                <option value="">Select Company Type</option>
                                                <option {{ $profile["company_type"] == 'Partnership' ? 'selected' : '' }} value="Partnership">Partnership</option>
                                                <option {{ $profile["company_type"] == 'Proprietor' ? 'selected' : '' }} value="Proprietor">Proprietor</option>
                                                <option {{ $profile["company_type"] == 'One Person Company' ? 'selected' : '' }} value="One Person Company">One Person Company</option>
                                                <option {{ $profile["company_type"] == 'LLP.' ? 'selected' : '' }} value="LLP.">LLP</option>
                                                <option {{ $profile["company_type"] == 'PVT LTD' ? 'selected' : '' }} value="PVT LTD">PVT LTD</option>
                                                <option {{ $profile["company_type"] == 'LTD.' ? 'selected' : '' }} value="LTD.">LTD.</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="" class="form-label">Full Company Name <sup>*</sup></label>
                                        <input type="text" name="company_name" value="{{ $profile['company_name'] }}" disabled class="form-control profile_ele" id="" placeholder="e.g. Conmix Pvt. Ltd. " >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="" class="form-label">Owner Full Name <sup>*</sup></label>
                                        <input type="text" name="full_name" value="{{ $profile['full_name'] }}" disabled class="form-control profile_ele" id="" placeholder="e.g. Johndoe">
                                        <!-- <span class="error">Please enter full name</span> -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <label for="" class="form-label">Email <sup>*</sup></label>
                                            <a href="javascript:void(0);" id="email_update_label" style="display:none;" class="color_sky" >Update</a>
                                        </div>
                                        <input type="hidden" id="edit_address_email" value="{{ isset($profile['email']) ? $profile['email'] : '' }}" />
                                        <input type="text" name="email" readOnly value="{{ $profile['email'] }}" disabled class="form-control profile_ele" id="edit_profile_email" placeholder="e.g. johndoe@example.com">
                                        <!-- <span class="error">Please enter valid email address</span> -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <label for="" name="mobile_number"  class="form-label">Mobile No. <sup>*</sup></label>
                                            <a href="javascript:void(0);" id="mobile_update_label" style="display:none;" class="color_sky" >Update</a>
                                        </div>
                                        <input type="hidden" id="edit_address_mobile" value="{{ isset($profile['mobile_number']) ? $profile['mobile_number'] : '' }}" />
                                        <input type="text" readOnly name="mobile_number" value="{{ $profile['mobile_number'] }}" id="mobile_or_email" disabled class="form-control profile_ele" placeholder="e.g. 9898989898">
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="" class="form-label">GST No <sup>*</sup></label>
                                        <input type="text" name="gst_number" value="" disabled class="form-control profile_ele" id="" placeholder="e.g. 00AABBC0000A1BB">
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="" class="form-label">PAN No <sup>*</sup></label>
                                        <input type="text" name="pan_number" value="{{ $profile['pan_number'] }}" disabled class="form-control profile_ele" id="" placeholder="e.g. AAABB0000C">
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="" class="form-label">Payment Method <sup>*</sup></label>
                                        <div class="custome_select">
                                            <select name="payment_method" disabled class="form-control form-select profile_ele">
                                                <option value="" readOnly>Select Payment method</option>
                                                @if(isset($payment_method["data"]))
                                                    @foreach($payment_method["data"] as $value)
                                                        <option value="{{ $value['_id'] }}" {{ isset($profile["pay_method_id"]) ? ($profile["pay_method_id"] == $value['_id'] ? 'selected' : '') : '' }}>{{ $value["name"] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="profile_fild_footer pt-4">
                            <button type="submit" name="update_profile" value="update_profile" class="btn btn-primary btn-secondary mb-2 me-2 profile_ele" disabled>SAVE</button>
                            <button type="button" class="btn btn-primary mb-2 profile_cancel" id="profile_cancel" style="display:none">CANCEL</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- CONTENT END -->


@endsection

<!-- <div id="Plant_otpModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="otpMessage">
        <label id="plant_OTP_label">Enter the 6 digits OTP sent on your given mobile no., Email id and current registered number <span id="plant_OTP_mob_no"></span><span class="str">*</span></label>
        <input type="text" id="plant_mobile_otp" class="form-control" placeholder="Enter OTP sent on mobile">
        <p id="plant_mobile_otp_error" style="color:red;"></p>

        <input type="text" id="plant_email_otp" class="form-control" placeholder="Enter OTP sent on Email">
        <p id="plant_email_otp_error" style="color:red;"></p>

        <input type="text" id="old_mobile_otp" class="form-control" placeholder="Enter OTP sent on your registered mobile number">
        <p id="old_mobile_otp_error" style="color:red;"></p>
        <div class="text-gray-dark resend_plant_otp_div text-left">
         
                <input class="form-control" name="" value="" required  type="hidden">
                
        </div>
      </div>
      <div class="modal-footer text-center">
          <button type="button" class="site-button green m-r15" id="plant_otpOk">Submit</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="plant_otpCancel">Cancel</button>
      </div>
    </div>
  </div>
</div> -->