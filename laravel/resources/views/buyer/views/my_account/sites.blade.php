@section('title', 'My Sites')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>My Account</span>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="pro_left_menu wht_bx mb-3">
                    <ul>
                        <li><a href="{{ route('buyer_profile') }}"><i class="far fa-user-circle"></i> My Profile </a></li>
                        <li><a href="{{ route('buyer_show_billing_address') }}"><i class="fas fa-map-marker-alt"></i>Billng Address</a></li>
                        <li><a href="#" class="active"><i class="fas fa-map-marker-alt"></i> Delivery Address</a></li>
                        <li><a href="{{ route('buyer_show_custom_rmc') }}"><i class="fas fa-map-marker-alt"></i> Custom RMC</a></li>
                        <li><a href="{{ route('buyer_get_password') }}"><i class="fas fa-lock"></i> Change Password</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="profile_bx wht_bx p-4 mb-5">
                    <h5 class="home_title"> Delivery Address 
                    <a href="javascript:void(0)" class="add_site_link" onclick="resetForm('add_site_form','add_site_address_popup')" data-bs-toggle="modal" data-bs-target="#add_site_address_popup">+ Add Delivery Address</a></h5>
                    <div class="row">
                        @if(isset($data["data"]) && !empty($data["data"]))
                            @foreach($data["data"] as $value)
                                <div class="col-md-6">
                                    <div class="site_add_bx p-3 mb-4">
                                        <label>Company Name :</label>
                                        <h6>{{ $value["company_name"] }}</h6>
                                        <label>Site Name :</label>
                                        <h6>{{ $value["site_name"] }}</h6>
                                        <label>Contact Person Name (At Site) :</label>
                                        <p class="color_orange mb-0">{{ $value["person_name"] }}</p>
                        
                                        <p class="mb-0">{{ $value["address_line1"] }}, {{ $value["address_line2"] }}, {{ $value["city_name"] }} - {{ $value["pincode"] }}, {{ $value["state_name"] }}, ({{ $value["country_name"] }}) </p>
                                        <p>Mobile No. : {{ $value["mobile_number"] }}</p>
                                        <div class="site_add_opration">
                                            <a href="javascript:void(0)" onclick="editSiteView('{{ json_encode($value) }}')"><i class="far fa-edit"></i></a>
                                            <a href="javascript:void(0)" onclick="siteDeleteConfirmPopup('{{ $value['_id'] }}')"><i class="far fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        @else

                        <div class="empty_cart_sidebar wht_bx">
                            <div class="empty_cart_sidebar_img">
                                <img src="{{asset('assets/buyer/images/nosite.jpg')}}" alt="Empty screen">
                            </div>
                            <div class="empty_cart_sidebar_text">No Record found</div>
                            <!-- <div class="button_request">
                                <a href="#" class="btn btn-primary">Continue Shopping</a>
                            </div> -->
                        </div>

                        @endif
                        
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</section>
<!-- CONTENT END -->


@endsection