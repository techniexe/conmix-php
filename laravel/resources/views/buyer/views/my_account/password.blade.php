@section('title', 'Change Password')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>My Account</span>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="pro_left_menu wht_bx mb-3">
                    <ul>
                        <li><a href="{{ route('buyer_profile') }}"><i class="far fa-user-circle"></i> My Profile </a></li>
                        <li><a href="{{ route('buyer_show_billing_address') }}"><i class="fas fa-map-marker-alt"></i>Billng Address</a></li>
                        <li><a href="{{ route('buyer_get_site') }}"><i class="fas fa-map-marker-alt"></i>Delivery Address</a></li>
                        <li><a href="{{ route('buyer_show_custom_rmc') }}"><i class="fas fa-map-marker-alt"></i> Custom RMC</a></li>
                        <li><a href="#" class="active"><i class="fas fa-lock"></i> Change Password</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="profile_bx wht_bx p-4 mb-5">
                    <h5 class="home_title">Change Password</h5>
                    <form name="change_pass_form" id="change-password-form" autocomplete="off">
                    @csrf
                        <div class="profile_fild">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label for="" class="form-label">Old Password</label>
                                                <div class="text_right_icon">
                                                    <input type="password" name="old_password" id="old_password" class="form-control" placeholder="e.g. Old Password" autocomplete="off">
                                                    <span class="text_icon">
                                                        <i class="fa fa-eye" id="old_pass_eye_on" aria-hidden="true"></i>
                                                        <i class="fa fa-eye-slash" id="old_pass_eye_off" style="display:none;" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label for="" class="form-label">New Password</label>
                                                <div class="text_right_icon">
                                                    <input type="password" name="password" id="password" class="form-control" placeholder="e.g. New Password" autocomplete="off">
                                                    <span class="text_icon">
                                                        <i class="fa fa-eye" id="new_pass_eye_on" aria-hidden="true"></i>
                                                        <i class="fa fa-eye-slash" id="new_pass_eye_off" style="display:none;" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label for="" class="form-label">Confirm Password</label>
                                                <div class="text_right_icon">
                                                    <input type="password" name="change_pass"  readOnly  class="form-control" onpaste="return false" ondrop="return false" id="change_pass" placeholder="e.g. Confirm Password" autocomplete="off">
                                                    <span class="text_icon">
                                                        <i class="fa fa-eye" id="confirm_pass_eye_on" aria-hidden="true"></i>
                                                        <i class="fa fa-eye-slash" id="confirm_pass_eye_off" style="display:none;" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="profile_fild_footer pt-4">
                            <button type="submit" name="update_profile" value="update_profile" class="btn btn-primary btn-secondary mb-2 me-2">SAVE</button>
                            <!-- <button type="button" class="btn btn-primary mb-2">CANCEL</button> -->
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- CONTENT END -->


@endsection