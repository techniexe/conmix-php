@section('title', 'My Custom RMC')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>My Account</span>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="pro_left_menu wht_bx mb-3">
                    <ul>
                        <li><a href="{{ route('buyer_profile') }}"><i class="far fa-user-circle"></i> My Profile </a></li>
                        <li><a href="{{ route('buyer_show_billing_address') }}"><i class="fas fa-map-marker-alt"></i>Billng Address</a></li>
                        <li><a href="{{ route('buyer_get_site') }}"><i class="fas fa-map-marker-alt"></i> Delivery Address</a></li>
                        <li><a href="#" class="active"><i class="fas fa-map-marker-alt"></i> Custom RMC</a></li>
                        <li><a href="{{ route('buyer_get_password') }}"><i class="fas fa-lock"></i> Change Password</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="profile_bx wht_bx p-4 mb-5">
                    <h5 class="home_title"> Custom RMC 
                    <a href="javascript:void(0)" class="add_site_link" onclick="openCustomMixDialogToSave(0,1)">+ Add Custom RMC</a></h5>
                    <div class="custom_rmc_list mb-4">
                        <div class="row">
                            @if(isset($data["data"]) && !empty($data["data"]))
                            <?php $count = 1; ?>
                                @foreach($data["data"] as $value)

                                    <div class="col-md-6" style="position: relative;">
                                        <!-- <a class="custom_rmc_bx mb-4" href=""> -->
                                            <!-- <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category"> -->
                                            <div class="custom_rmc_heading p-3 d-flex justify-content-between align-items-center">
                                                <b>Custom RMC {{ $count++ }}</b> 
                                                <?php //$count++; ?>
                                                <div class="site_add_opration" style="position: absolute;right: 15px;bottom: unset;">
                                                    @if((Session::get("selected_state_name") != null) && (Session::get("selected_city_name") != null) && (Session::get("selected_display_delivery_date") != null))
                                                        <a href="javascript:void(0)" id="custom_rmc_view_btn_{{ $value['_id'] }}" onclick="openCustomMixDialogToSaveEdit(0,3,'{{ json_encode($value) }}')"><i class="fas fa-search"></i></a>
                                                    @else
                                                        <a href="javascript:void(0)" onclick="openLocationDialog('custom_mix_dialog_on','change_location','{{ $value["_id"] }}')"><i class="fas fa-search"></i></a>
                                                    @endif
                                                    <a href="javascript:void(0)" onclick="openCustomMixDialogToSaveEdit(0,2,'{{ json_encode($value) }}')"><i class="far fa-edit"></i></a>
                                                    <a href="javascript:void(0)" onclick="customRMCDeleteConfirmPopup('{{ $value["_id"] }}')"><i class="far fa-trash-alt"></i></a>
                                                </div>
                                            </div>
                                            <div class="custom_rmc_info px-3">
                                                <p>
                                                    <small class="color_orange"><i class="fas fa-map-marker-alt" aria-hidden="true"></i> 
                                                    @if(isset($value["site_details"]["address_line1"]) && !empty($value["site_details"]["address_line1"]))

                                                        {{ $value["site_details"]["address_line1"] }}, 
                                                        {{ $value["site_details"]["address_line2"] }}, 
                                                        {{ $value["site_details"]["city_details"]["city_name"] }}, 
                                                        {{ $value["site_details"]["state_details"]["state_name"] }}



                                                    @endif
                                                    </small>
                                                </p>    
                                            </div>
                                            <div class="custom_rmc_info px-3">
                                                <h6 class="color_sky my-3">{{ $value["concrete_grade"]["name"] }}</h6>
                                                <div class="row gx-5">
                                                    <div class="col-sm-6">
                                                        <p>Cement ( Kg ) : <span>{{ $value["cement_quantity"] }}</span></p>
                                                        <p>Coarse Sand ( Kg ) : <span>{{ $value["sand_quantity"] }}</span></p>
                                                        <p>Fly Ash ( Kg ) : <span>{{ isset($value["fly_ash_quantity"]) ? $value["fly_ash_quantity"] : 0 }}</span></p>
                                                        <p>Admixture ( Kg ) : <span>{{ isset($value["admix_quantity"]) ? $value["admix_quantity"] : 0 }}</span></p>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <p>{{ $value["aggregate1_sub_category"]["sub_category_name"] }} ( Kg ) : <span>{{ $value["aggregate1_quantity"] }}</span></p>
                                                        <p>{{ $value["aggregate2_sub_category"]["sub_category_name"] }} ( Kg ) : <span>{{ $value["aggregate2_quantity"] }}</span></p>
                                                        <p>Water ( Ltr ) : <span>{{ $value["water_quantity"] }}</span></p>
                                                        <p>Grade : <span>{{ $value["concrete_grade"]["name"]  }}</span></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="desi_mix_dec px-3">
                                                <p class="pt-3"><b>Design Mix Description</b><br/></p>
                                                <span><b>Cement Brand :</b> {{ $value["cement_brand1"]["name"] }} 
                                                                {{ isset($value["cement_brand2"]["name"]) ? ' , '.$value["cement_brand2"]["name"] : ''  }}

                                                </span><br/>
                                                <span><b>Cement Grade :</b> {{ $value["cement_grade"]["name"] }} 

                                                </span><br/>
                                                <span><b>Coarse Sand Source :</b> 
                                                    {{ isset($value["sand_source1"]["sand_source_name"]) ? $value["sand_source1"]["sand_source_name"] : ''}}
                                                    {{ isset($value["sand_source2"]["sand_source_name"]) ? ' , '.$value["sand_source2"]["sand_source_name"] : ''}}
                                                </span><br/>
                                                <span><b>Coarse Sand Zone :</b> 
                                                    {{ isset($value["sand_zone"]["zone_name"]) ? $value["sand_zone"]["zone_name"].'( '.$value["sand_zone"]["finess_module_range"].' )' : ''}}
                                                </span><br/>
                                                <span><b>Aggregate Source : </b>
                                                    {{ isset($value["aggregate_source1"]["aggregate_source_name"]) ? $value["aggregate_source1"]["aggregate_source_name"] : '' }}
                                                    {{ isset($value["aggregate_source2"]["aggregate_source_name"]) ? ' , '.$value["aggregate_source2"]["aggregate_source_name"] : '' }}
                                                </span><br/>
                                                <span><b>Admixture Brand :</b>
                                                    {{ isset($value["admix_brand1"]["name"]) ? $value["admix_brand1"]["name"] .' - ( '.$value["admix_category1"]["category_name"].' - '.$value["admix_category1"]["admixture_type"].' )' : '' }}
                                                    {{ isset($value["admix_brand2"]["name"]) ? ' , '.$value["admix_brand2"]["name"] .' - ( '.$value["admix_category2"]["category_name"].' - '.$value["admix_category2"]["admixture_type"].' )' : '' }}
                                                </span><br/>
                                                <span><b>Fly Ash Source Name :</b>
                                                    {{ isset($value["fly_ash_source"]["fly_ash_source_name"]) ? $value["fly_ash_source"]["fly_ash_source_name"] : '' }}
                                                </span><br/>
                                                <span><b>Water :</b> 
                                                    {{ isset($value["water_type"]) ? $value["water_type"] : 'Regular' }}
                                                </span>
                                            </div>
                                        <!-- </a> -->

                                    </div>

                                @endforeach

                            @else

                            <div class="empty_cart_sidebar wht_bx">
                                <div class="empty_cart_sidebar_img">
                                    <img src="{{asset('assets/buyer/images/nosite.jpg')}}" alt="Empty screen">
                                </div>
                                <div class="empty_cart_sidebar_text">No Record found</div>
                                <!-- <div class="button_request">
                                    <a href="#" class="btn btn-primary">Continue Shopping</a>
                                </div> -->
                            </div>

                            @endif
                            
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</section>
<!-- CONTENT END -->


@endsection