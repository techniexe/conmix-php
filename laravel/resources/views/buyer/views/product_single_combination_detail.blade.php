
<?php //dd($cart_details); ?>
@php
        $profile = session('buyer_profile_details', null);
    @endphp
<?php 
    $final_data = $data["data"]; //dd($final_data);
    $rmc_data = $data["data"]; //dd($final_data);
    $data_count = 3000;
    ?>

<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>
            <?php //dd($all_concrete_data); ?>
            @if(isset($final_data["concrete_grade"]["_id"]))
                @if(isset($all_concrete_data[$final_data["concrete_grade"]["_id"]]["name"]))
                    {{$all_concrete_data[$final_data["concrete_grade"]["_id"]]["name"]}}
                @endif
            @endif
            </span>
        </div>
    </div>
</section>
<section>
<?php //dd($cart_details);
$total_mix = 0;

$min_order_quantity = 3;
$order_quantity = 3;

if(Session::get("selected_delivery_qty") != null){
    $order_quantity = Session::get("selected_delivery_qty");
}

$product_id = 1;
?>
    @php
        $profile = session('buyer_profile_details', null);
    @endphp
    @if(isset($data["data"]))

    <input Type="hidden" name="vendor_id" id="detail_vendor_id" value="{{ $final_data['vendor']['_id'] }}">
    <input Type="hidden" name="product_id" id="detail_product_id" value="{{ $final_data['_id'] }}">
    <input Type="hidden" name="address_id" id="detail_address_id" value="{{ $final_data['address']['_id'] }}">
    <?php //dd(Session::get("buyer_TM_details",null));  ?>
    @if(Session::get("buyer_TM_details",null) != null)

        <input Type="hidden" name="is_TM_detail_avail" id="is_TM_detail_avail" value="1">

    @else

        <input Type="hidden" name="is_TM_detail_avail" id="is_TM_detail_avail" value="0">

    @endif
    <div class="container">
        <div class="prod_dtl_top wht_bx px-4 mb-4">
            <div class="row">
                <div class="col-md-6">
                    <div class="prod_dtl_slider pt-4">
                        <div id="carouselExampleIndicators" class="carousel slide mb-4" data-bs-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php $count = 0; ?>
                                @if(isset($final_data["vendor_media"]) && !empty($final_data["vendor_media"]))
                                    @foreach($final_data["vendor_media"] as $media_value)
                                        @if($media_value['media_type'] == "image")
                                            @if($count == 0)
                                                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{ $count }}" class="active"></li>
                                            @else
                                                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{ $count }}"></li>
                                            @endif

                                            <?php $count++; ?>
                                        @endif
                                    @endforeach
                                @else
                                    <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>

                                @endif

                            </ol>
                            <div class="carousel-inner">
                                @if(isset($final_data["vendor_media"]) && !empty($final_data["vendor_media"]))
                                    @foreach($final_data["vendor_media"] as $media_value)
                                        @if($media_value['media_type'] == "image")
                                            <div class="carousel-item {{ $media_value['type'] == 'PRIMARY' ? 'active' : '' }}">
                                                <img src="{{ $media_value['media_url'] }}" class="d-block w-100" alt="banner">
                                            </div>
                                        @endif

                                    @endforeach
                                @else
                                    <div class="carousel-item active">
                                        <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="d-block w-100" alt="banner">
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="prod_dtl_top_right">
                        <div class="custom_rmc_heading pt-4 d-flex justify-content-between align-items-center mb-2">
                            <h4 class="d-grid mb-1"><small>{{ $final_data["vendor"]["company_name"]}}</small>
                            <!-- <p class="mb-1">Mix Type : <span class="color_orange">{{$final_data["product_name"]}}</span></p> -->
                            

                            </h4>
                            
                            <div class="custom_rmc_price text-end">
                                <p>
                                    <span class="product_distance d-block mb-1"> <img src="{{asset('assets/buyer/images/ic_arrow.svg')}}" alt="direction arrow"> {{ number_format($final_data["distance"],2) }} km</span>
                                    ₹{{ number_format($final_data["selling_price_with_margin"],2) }} / <span>Cu.Mtr</span> <small class="text-danger text-nowrap">(Excl. GST)</small>
                                </p>
                            </div>
                        </div>
                        <div class="d-flex avail_stock_pump justify-content-between mb-3">
                            <!-- <p class="avail_stock mb-0 me-3">Availability : <span class="text-success">In stock</span> -->
                            <small class="color_orange"><i class="fas fa-map-marker-alt" aria-hidden="true"></i>
                                @if(isset($final_data["address"]["line1"]) && !empty($final_data["address"]["line1"]))

                                    {{ $final_data["address"]["line1"] }}, {{ $final_data["address"]["city_name"] }}, {{ $final_data["address"]["state_name"] }}



                                @endif
                            </small>
                            <p><span id="with_TM_or_CP_check"> {{ ($final_data["with_TM"] == 'true') ? (($final_data["with_CP"] == 'true') ? 'With TM & Pump' : 'With TM') : 'RMC only'  }} </span></p>
                            <div class="with_mixer_pump">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" disabled type="checkbox" id="TM_checkbox" {{ $final_data["with_TM"] == 'true' ? 'checked' : (isset($final_data["ErrMsgForTM"]) ? 'checked' : '') }}>
                                    <input type="hidden" value="" id="TM_checkbox_price" />
                                    <label class="form-check-label">
                                        With Transit Mixer (TM)
                                    </label>
                                    @if($final_data["TM_price"] > 0)
                                        <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="₹ {{ number_format($final_data["TM_price"],2) }} / Cu.Mtr Per KM"><i class="fas fa-info-circle"></i></a>
                                    @endif
                                </div>
                                <div class="form-check form-check-inline me-0">
                                    <input class="form-check-input" {{ $final_data["is_CP_available_by_vendor"] == 'false' ? 'disabled' : ''  }} type="checkbox" id="CP_checkbox" {{ $final_data["with_CP"] == 'true' ? 'checked' : (isset($final_data["ErrMsgForTM"]) ? 'checked' : '') }}>
                                    <input type="hidden" value="" id="CP_checkbox_price" />
                                    <label class="form-check-label">
                                        With Concrete Pump (CP)
                                    </label>
                                    @if($final_data["CP_price"] > 0)
                                        <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="₹ {{ number_format($final_data["CP_price"],2) }} / Cu.Mtr"><i class="fas fa-info-circle"></i></a>
                                    @endif
                                </div>
                                @if($final_data["is_CP_available_by_vendor"] == 'false')
                                    <div class="custom_rmc_qty d-flex align-items-center justify-content-end">  <i>Concrete pump available with Transit Mixer only</i></div>
                                @endif
                            </div>

                        </div>
                        <div class="custom_rmc_qty d-flex align-items-center text-danger mb-1 justify-content-end">  <i>Delivery Date:
                                <?php
                                    if(Session::get("selected_display_delivery_date") != null){
                                        echo date('d M Y', strtotime(Session::get("selected_display_delivery_date")));
                                    }
                                ?>
                                </i></div>

                            @if(isset($final_data["ErrMsgForTM"]))
                                <p class="error">{{ $final_data["ErrMsgForTM"] }}</p>
                            @endif

                            @if(isset($final_data["ErrMsgForCp"]))
                                <p class="error">{{ $final_data["ErrMsgForCp"] }}</p>
                            @endif


                        <div class="custom_rmc_info px-3">
                            <h6 class="color_sky my-3">{{ $final_data["concrete_grade"]["name"] }} - {{$final_data["product_name"]}}</h6>
                            <div class="row gx-5">
                                <div class="col-sm-6">
                                    <p>Cement ( Kg ) : <span>{{ $final_data["cement_quantity"] }}</span></p>
                                    <p>Coarse Sand ( Kg ) : <span>{{ $final_data["sand_quantity"] }}</span></p>
                                    @if(isset($final_data["fly_ash_quantity"]))
                                        <p>Fly Ash ( Kg ) : <span>{{ $final_data["fly_ash_quantity"] }}</span></p>
                                    @endif
                                    <p>Admixture ( Kg ) : <span>{{ isset($final_data["ad_mixture_quantity"]) ? $final_data["ad_mixture_quantity"] : '-' }}</span></p>
                                </div>
                                <div class="col-sm-6">
                                    <p>{{ $final_data["aggregate1_sand_category"]["sub_category_name"] }} ( Kg ) : <span>{{ $final_data["aggregate1_quantity"] }}</span></p>
                                    <p>{{ $final_data["aggregate2_sand_category"]["sub_category_name"] }} ( Kg ) : <span>{{ $final_data["aggregate2_quantity"] }}</span></p>
                                    <p>Water ( Ltr ) : <span>{{ $final_data["water_quantity"] }}</span></p>
                                    <p>Grade : <span>{{ $final_data["concrete_grade"]["name"] }}</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4 mb-1 custom_rmc_qty d-flex align-items-center">
                            <span class="me-3">Qty :</span>
                            <div class="qty-block">
                                <button type="button" class="sub1 qty_sub"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                <input type="number" id="qty_input1" readOnly class="form-control" value="{{ $order_quantity }}" min="{{ $min_order_quantity }}" max="100">
                                <button type="button" class="add1 qty_add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                            </div>
                            <span class="ms-2">Cu.Mtr</span>


                            <input type="hidden" value="{{ $min_order_quantity }}" id="product_page_minimum_order" />
                        </div>

                        <div class="mb-4 custom_rmc_qty d-flex align-items-center">  <i>Minimum Order Qty 3 Cu.Mtr</i></div>
                        <span class="error" id="product_detail_qty_error"></span>
                        <div class="pro_dtl_btn d-flex mb-3 flex-wrap">

                            <input type="hidden" value="{{ json_encode($final_data) }}" id="design_mix_product_details" />
                            @if(!isset($profile))
                                <a href="javascript:void(0)" onclick="loginAddTocart('product_add_to_cart')" data-bs-toggle="modal" data-bs-target="#Login-form" class="btn btn-primary btn-secondary me-3 mb-2">Add To Cart</a>
                                <a href="javascript:void(0)" onclick="addToCart('{{ csrf_token() }}','{{ $final_data['_id'] }}','{{ $min_order_quantity }}','slide_cart',0)" id="product_add_to_cart" class="" style="display:none;"></a>
                                <!-- <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#Login-form" class="btn btn-primary btn-secondary mb-2">BUY NOW</a> -->
                            @else
                                <a href="javascript:void(0)" onclick="addToCart('{{ csrf_token() }}','{{ $final_data['_id'] }}','{{ $min_order_quantity }}','slide_cart',0)" class="btn btn-primary btn-secondary me-3 mb-2">ADD TO CART</a>
                                <!-- <a href="javascript:void(0)" onclick="buynow('{{ csrf_token() }}','{{ $final_data['_id'] }}','{{ $min_order_quantity }}','slide_cart',0,'')"  class="btn btn-primary btn-secondary mb-2">BUY NOW</a> -->
                            @endif
                            <!-- <a href="#" class="btn btn-primary btn-secondary mb-2">BUY NOW</a> -->
                            <input type="hidden" id="buy_now_button_clicked" value="0" />
                            @if(isset($final_data["is_custom"]) && $final_data["is_custom"])
                                <a href="javascript:void(0)" onclick="openCustomMixDialog(1)" class="btn btn-primary me-3 mb-2">Cutstom Design Mix</a>
                                <div class="mb-4 custom_rmc_qty d-flex align-items-center">  <i>Choose custom design mix for customised RMC</i></div>
                            @endif


                        </div>


                    </div>
                </div>
            </div>
        </div>
        
        <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">

            <div class="mb-2">
                <h6>Material source for design mix</h6>
                <ul class="row">
                    @if(isset($data["brand_details"]["data"]) && (count($data["brand_details"]["data"]) > 0))
                        <?php 
                                $brands = $data["brand_details"]["data"];
                            ?>
                        <li class="col-6 mb-1">Cement Brand : 
                            <span>{{ isset($final_data["cement_brand"]["name"]) ? $final_data["cement_brand"]["name"] : '' }}
                                <a href="javascript:void(0)" id="cement_brand_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                            </span> 
                            <div class="cstm-select-box" id="cement_brand_{{ $data_count }}" style="display:none;">
                                <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                <select class="form-select mix_cement_brnad" class="form-control">
                                    <option disabled="disabled" selected="selected">Select Cement Brand</option>
                                    @if((isset($brands["cementData"])) && (count($brands["cementData"]) > 0))
                                        @foreach($brands["cementData"] as $value)
                                            @if((isset($value['cement_brand'])) && (count($value["cement_brand"]) > 0))
                                                @if((isset($value['cement_grade'])) && (count($value["cement_grade"]) > 0))
                                                    <?php
                                                        $is_selected = '';
                                                        if(($rmc_data["cement_brand_id"] == $value['cement_brand']['_id']) && ($rmc_data["cement_grade_id"] == $value['cement_grade']['_id'])){
                                                            $is_selected = 'selected';
                                                        }
                                                    ?>
                                                    <option {{ $is_selected }} value="{{ $value['cement_brand']['_id'] }}_{{ $value['cement_grade']['_id'] }}" 
                                                        data-cement-brand-id="{{ $value['cement_brand']['_id'] }}"
                                                        data-cement-grade-id="{{ $value['cement_grade']['_id'] }}" 
                                                        data-rmc-id="{{ $data_count }}">
                                                        {{ $value['cement_brand']["name"] }} - {{ $value['cement_grade']["name"] }}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </li>
                        <li class="col-6 mb-1">Coarse Sand Source : 
                            <span>
                                {{ isset($final_data["sand_source"]["sand_source_name"]) ? $final_data["sand_source"]["sand_source_name"] : '' }}
                                <a href="javascript:void(0)" id="sand_source_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                            </span>
                            <div class="cstm-select-box" id="sand_source_{{ $data_count }}" style="display:none;margin-top:5px;">
                                <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                <select class="form-select mix_sand_source" class="form-control">
                                    <option disabled="disabled" selected="selected">Select Coarse Sand Source</option>
                                    @if((isset($brands["sandData"])) && (count($brands["sandData"]) > 0))
                                        @foreach($brands["sandData"] as $value)
                                            @if((isset($value['sand_source'])) && (count($value["sand_source"]) > 0))
                                                @if((isset($value['sand_zone'])) && (count($value["sand_zone"]) > 0))
                                                <?php
                                                        $is_selected = '';
                                                        if(($rmc_data["sand_source_id"] == $value['sand_source']['_id']) && ($rmc_data["sand_zone_id"] == $value['sand_zone']['_id'])){
                                                            $is_selected = 'selected';
                                                        }
                                                    ?>
                                                    <option {{ $is_selected }} value="{{ $value['sand_source']['_id'] }}_{{ $value['sand_zone']['_id'] }}" 
                                                        data-sand-source-id="{{ $value['sand_source']['_id'] }}"
                                                        data-sand-zone-id="{{ $value['sand_zone']['_id'] }}" data-rmc-id="{{ $data_count }}">{{ $value['sand_source']["sand_source_name"] }} - ({{ $value['sand_zone']["zone_name"] }} - {{ $value['sand_zone']["finess_module_range"] }})</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </li>
                        <!-- <li class="col-6 mb-1">Coarse Sand Zone : 
                            {{ isset($final_data["sand_zone"]["zone_name"]) ? $final_data["sand_zone"]["zone_name"].' ( '. $final_data["sand_zone"]["finess_module_range"].' ) ' : '' }} 
                        </li> -->
                        <li class="col-6 mb-1">Aggregate Source (VSI) : 
                            <span>
                                {{ isset($final_data["aggregate_source"]["aggregate_source_name"]) ? $final_data["aggregate_source"]["aggregate_source_name"] : '' }}
                                <a href="javascript:void(0)" id="aggregate_source_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                            </span>
                            <div class="cstm-select-box" id="aggregate_source_{{ $data_count }}" style="display:none;margin-top:5px;">
                                <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                <select class="form-select mix_aggregate_source_cat_1_2" class="form-control">
                                    <option disabled="disabled" selected="selected">Select Aggregate Source & Category 1 & Category 2</option>
                                    @if((isset($brands["aggData"])) && (count($brands["aggData"]) > 0))
                                        @foreach($brands["aggData"] as $value)
                                            @if((isset($value['aggregate_source'])) && (count($value["aggregate_source"]) > 0))
                                                @if((isset($value['aggregate_sand_sub_category'])) && (count($value["aggregate_sand_sub_category"]) > 0))
                                                    @foreach($value["sub_cat_data_2"] as $sub_cat_2_value)

                                                    <?php
                                                        $is_selected = '';
                                                        if(($rmc_data["aggregate_source_id"] == $value['aggregate_source']['_id']) && 
                                                            ($rmc_data["aggregate1_sub_category_id"] == $value['aggregate_sand_sub_category']['_id']) &&
                                                            ($rmc_data["aggregate2_sub_category_id"] == $sub_cat_2_value['aggregate_sand_sub_category']['_id'])
                                                            ){
                                                            $is_selected = 'selected';
                                                        }
                                                    ?>

                                                        <option {{ $is_selected }} value="{{ $value['aggregate_source']['_id'] }}_{{ $value['aggregate_sand_sub_category']['_id'] }}_{{ $sub_cat_2_value['aggregate_sand_sub_category']['_id'] }}" 
                                                        data-aggr-source-id="{{ $value['aggregate_source']['_id'] }}"
                                                        data-sub-cat-1-id="{{ $value['aggregate_sand_sub_category']['_id'] }}" 
                                                        data-sub-cat-2-id="{{ $sub_cat_2_value['aggregate_sand_sub_category']['_id'] }}"
                                                        data-rmc-id="{{ $data_count }}">
                                                        {{ $value['aggregate_source']["aggregate_source_name"] }} - 
                                                        {{ $value['aggregate_sand_sub_category']["sub_category_name"] }} - 
                                                        {{ $sub_cat_2_value['aggregate_sand_sub_category']['sub_category_name'] }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </li>
                        <li class="col-6 mb-1">Admixture Brand : 
                            <!-- {{ isset($final_data["admixture_brand"]["name"]) ? $final_data["admixture_brand"]["name"]. ' - '. $final_data["admixture_category"]["category_name"] .' '. $final_data["admixture_category"]["admixture_type"] : '' }}  -->
                            <span>
                                {{ isset($final_data["admixture_brand"]["name"]) ? $final_data["admixture_brand"]["name"]. ' - '. $final_data["admixture_category"]["category_name"] .' '. $final_data["admixture_category"]["admixture_type"] : '' }}
                                <a href="javascript:void(0)" id="admix_source_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                            </span>
                            <div class="cstm-select-box" id="admix_source_{{ $data_count }}" style="display:none;margin-top:5px;">
                                <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                <select class="form-select mix_admix_source" class="form-control">
                                    <option disabled="disabled" selected="selected">Select Admixture Brand & Type</option>
                                    @if((isset($brands["admixData"])) && (count($brands["admixData"]) > 0))
                                        @foreach($brands["admixData"] as $value)
                                            @if((isset($value['admixture_brand'])) && (count($value["admixture_brand"]) > 0))
                                                @if((isset($value['admixture_category'])) && (count($value["admixture_category"]) > 0))

                                                <?php
                                                        $is_selected = '';
                                                        if(($rmc_data["ad_mixture_brand_id"] == $value['admixture_brand']['_id']) && ($rmc_data["ad_mixture_category_id"] == $value['admixture_category']['_id'])){
                                                            $is_selected = 'selected';
                                                        }
                                                    ?>

                                                    <option {{ $is_selected }} value="{{ $value['admixture_brand']['_id'] }}_{{ $value['admixture_category']['_id'] }}" 
                                                    data-admix-brand-id="{{ $value['admixture_brand']['_id'] }}"
                                                        data-admix-cat-id="{{ $value['admixture_category']['_id'] }}" data-rmc-id="{{ $data_count }}">
                                                        {{ $value['admixture_brand']["name"] }} - ({{ $value['admixture_category']["category_name"] }} - {{ $value['admixture_category']["admixture_type"] }})</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        </li>
                        <li class="col-6 mb-1">Fly Ash Source Name : 
                            <!-- {{ isset($final_data["fly_ash_source"]["fly_ash_source_name"]) ? $final_data["fly_ash_source"]["fly_ash_source_name"] : '' }}  -->
                            <span>
                                {{ isset($final_data["fly_ash_source"]["fly_ash_source_name"]) ? $final_data["fly_ash_source"]["fly_ash_source_name"] : '' }}
                                <a href="javascript:void(0)" id="flyash_source_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>
                            </span>
                            <div class="cstm-select-box" id="flyash_source_{{ $data_count }}" style="display:none;margin-top:5px;">
                                <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                <select class="form-select mix_flyash_source" class="form-control">
                                    <option disabled="disabled" selected="selected">Select Fly Ash Source</option>
                                    @if((isset($brands["flyashData"])) && (count($brands["flyashData"]) > 0))
                                        @foreach($brands["flyashData"] as $value)
                                            @if((isset($value['fly_ash_source'])) && (count($value["fly_ash_source"]) > 0))
                                                <?php
                                                        $is_selected = '';
                                                        if(isset($rmc_data["fly_ash_source_id"])){
                                                            if(($rmc_data["fly_ash_source_id"] == $value['fly_ash_source']['_id'])){
                                                                $is_selected = 'selected';
                                                            }
                                                        }
                                                    ?>
                                                <option {{ $is_selected }} value="{{ $value['fly_ash_source']['_id'] }}" 
                                                data-flyash-source-id="{{ $value['fly_ash_source']['_id'] }}"
                                                data-rmc-id="{{ $data_count }}"
                                                    >{{ $value['fly_ash_source']["fly_ash_source_name"] }} 
                                                </option>
                                                
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        </li>
                        <li class="col-6 mb-1">Water : 
                            
                            <span>
                                {{ isset($rmc_data["water_detail"]["water_type"]) ? $rmc_data["water_detail"]["water_type"] : 'Regular' }}
                                <a href="javascript:void(0)" id="water_type_change_{{ $data_count }}" data-id="{{ $data_count }}" class="" style="color:#687ae8;text-decoration: underline;">Change</a>    
                                
                            </span>
                            <div class="cstm-select-box" id="water_type_{{ $data_count }}" style="display:none;margin-top:5px;">
                                <!-- <select name="cement_brand_id" id="cement_brand_id" class="form-control"> -->
                                <select class="form-select mix_water_type" class="form-control">
                                    <option disabled="disabled" selected="selected">Select Water Type</option>
                                    @if((isset($brands["waterData"])) && (count($brands["waterData"]) > 0))
                                        @foreach($brands["waterData"] as $value)
                                            @if((isset($value['water_type'])) && (!empty($value["water_type"])))
                                                <?php
                                                        $is_selected = '';
                                                        if(isset($rmc_data["water_detail"]["water_type"])){
                                                            if(($rmc_data["water_detail"]["water_type"] == $value['water_type'])){
                                                                $is_selected = 'selected';
                                                            }
                                                        }
                                                    ?>
                                                <option {{ $is_selected }} value="{{ $value['water_type'] }}" 
                                                data-water-type="{{ $value['water_type'] }}"
                                                data-rmc-id="{{ $data_count }}"
                                                    >{{ $value['water_type'] }} 
                                                </option>
                                                
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        
                        </li>

                        <div>
                            <form id="rmc_combination_form_{{ $data_count }}">

                                <input type="hidden" name="design_mix_id" value="{{ $final_data['_id'] }}" />
                                <!-- <input type="hidden" name="rmc_div_id" value="{{ $data_count }}" /> -->
                                
                                <input type="hidden" id="mix_rmc_cement_brand_id_{{ $data_count }}" name="cement_brand_id" value="{{ $rmc_data['cement_brand_id'] }}" />
                                <input type="hidden" id="mix_rmc_cement_grade_id_{{ $data_count }}" name="cement_grade_id" value="{{ $rmc_data['cement_grade_id'] }}" />
                                
                                <input type="hidden" id="mix_rmc_sand_source_id_{{ $data_count }}" name="sand_source_id" value="{{ $rmc_data['sand_source_id'] }}" />
                                <input type="hidden" id="mix_rmc_sand_zone_id_{{ $data_count }}" name="sand_zone_id" value="{{ $rmc_data['sand_zone_id'] }}" />
                                
                                <input type="hidden" id="mix_rmc_aggr_source_id_{{ $data_count }}" name="aggr_source_id" value="{{ $rmc_data['aggregate_source_id'] }}" />
                                <input type="hidden" id="mix_rmc_aggr_cat_1_id_{{ $data_count }}" name="aggr_cat_1_id" value="{{ $rmc_data['aggregate1_sub_category_id'] }}" />
                                <input type="hidden" id="mix_rmc_aggr_cat_2_id_{{ $data_count }}" name="aggr_cat_2_id" value="{{ $rmc_data['aggregate2_sub_category_id'] }}" />
                                
                                <input type="hidden" id="mix_rmc_admix_brand_id_{{ $data_count }}" name="admix_brand_id" value="{{ $rmc_data['ad_mixture_brand_id'] }}" />
                                <input type="hidden" id="mix_rmc_admix_cat_id_{{ $data_count }}" name="admix_cat_id" value="{{ $rmc_data['ad_mixture_category_id'] }}" />
                                
                                <input type="hidden" id="mix_water_type_{{ $data_count }}" name="water_type" value="{{ isset($rmc_data['water_detail']['water_type']) ? $rmc_data['water_detail']['water_type'] : '' }}" />

                                <input type="hidden" id="mix_rmc_fly_ash_id_{{ $data_count }}" name="fly_ash_id" value="{{ $rmc_data['fly_ash_source_id'] }}" />

                            </form>
                        </div>

                        <script>
                            $(document).ready(function(){

                                $("#cement_brand_change_{{ $data_count }}").click(function(){
                                    
                                    // console.log("Clicked");
                                    $("#cement_brand_{{ $data_count }}").toggle();

                                });
                                
                                $("#sand_source_change_{{ $data_count }}").click(function(){
                                    
                                    // console.log("Clicked");
                                    $("#sand_source_{{ $data_count }}").toggle();

                                });
                                
                                $("#aggregate_source_change_{{ $data_count }}").click(function(){
                                    
                                    // console.log("Clicked");
                                    $("#aggregate_source_{{ $data_count }}").toggle();

                                });
                                
                                $("#admix_source_change_{{ $data_count }}").click(function(){
                                    
                                    // console.log("Clicked");
                                    $("#admix_source_{{ $data_count }}").toggle();

                                });
                                
                                $("#flyash_source_change_{{ $data_count }}").click(function(){
                                    
                                    // console.log("Clicked");
                                    $("#flyash_source_{{ $data_count }}").toggle();

                                });

                                $("#water_type_change_{{ $data_count }}").click(function(){
                            
                                    // console.log("Clicked");
                                    $("#water_type_{{ $data_count }}").toggle();
            
                                });

                            });
                            
                        </script>

                    @else
                        <li class="col-6 mb-1">
                            Material Details not available
                        </li>
                    @endif
                </ul>
            </div>
            <!-- <div class="mb-2">

            </div> -->
            <!--<div class="mb-2">
                <h6>Lower Pollution </h6>
                <p>The use of RMC reduces air pollution in and around the worksite as the mixing is done at the plant..</p>
            </div>
            <div class="mb-2">
                <h6>Key Features (from the manufacturer)</h6>
                <ul class="mb-3">
                    <li>RMC produced from fully automated batching plants guarantees quality since:</li>
                    <li>The raw materials are subject to stringent quality and quantity measures.</li>
                    <li>The concrete is subject to quality control throughout the manufacturing</li>
                    <li>A wide variety of ready mixed concrete can be produced, with ease and on demand, by varying the proportions/combinations of the cement, aggregates and admixtures.</li>
                </ul>
            </div>
            <div class="mb-2">
                <h6>Cost-effectiveness</h6>
                <p>Ready mixed concrete is more cost effective because:</p>
                <p>Basic materials are now not stored on site and this reduces the need for storage space.</p>
                <p>Plant and machinery for mixing concrete are not required.</p>
                <p>Wastage of basic materials is avoided.</p>
                <p>Labour associated with production of concrete is reduced.</p>
                <p>Time required for the entire process is greatly reduced</p>
            </div>
            <div class="mb-2">
                <h6>Lower Pollution </h6>
                <p>The use of RMC reduces air pollution in and around the worksite as the mixing is done at the plant..</p>
            </div> -->
        </div>
        <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">
            <div class="mb-2">
                <h6>Product Details</h6>
                <p>{{ $final_data["description"] }}</p>
            </div>
        </div>
        <div class="product_review wht_bx p-4 mb-4">
            <div class="product_review_head d-flex justify-content-between">
                <h6>Reviews ( {{ isset($data["reviews"]["reviews"]) ? count($data["reviews"]["reviews"]) : 0 }} )</h6>
                <!-- <a href="#" class="btn btn-secondary btn-primary" data-bs-toggle="modal" data-bs-target="#write_review_popup">WRITE REVIEW</a> -->
            </div>
            <!-- <h6 class="avrage_rating"><i class="far fa-star" aria-hidden="true"></i> 4 <sub>/ 5</sub> <span>Average Rating</span></h6> -->
            <div class="review_list">
                @if(isset($data["reviews"]["reviews"]) && (count($data["reviews"]["reviews"]) > 0))
                <?php //dd($data["reviews"]); ?>
                    @foreach($data["reviews"]["reviews"] as $review_value)

                        <div class="review_bx">
                            <h6 class="mb-1">{{ $review_value["buyer"]["full_name"] }}</h6>
                            <div class="star_rating mb-1">
                                <fieldset class="rating">

                                    <div class="my-rating" id="{{ $review_value['_id'] }}_rating"></div>
                                    <script>

                                        $("#{{ $review_value['_id'] }}_rating").starRating({
                                            starSize: 17,
                                            ratedColor: '#ff9600',
                                            readOnly:true,
                                            callback: function(currentRating, $el){
                                                // make a server call here
                                            }
                                        });

                                        @if(isset($review_value["rating"]))

                                            $('#{{ $review_value['_id'] }}_rating').starRating('setRating', '{{ $review_value['rating'] }}');

                                        @endif

                                    </script>

                                </fieldset>
                            </div>
                            <div class="date mb-1">{{ date('d M y', strtotime($review_value["created_at"])) }}</div>
                            <p>{{ $review_value["review_text"] }}</p>
                        </div>
                    @endforeach
                @else

                    <div class="empty_cart_sidebar">
                        <!-- <div class="empty_cart_sidebar_img">
                            <img src="{{asset('assets/buyer/images/no_review.png')}}" alt="Empty screen">
                        </div> -->
                        @if(!isset($profile))
                            <!-- <div class="empty_cart_sidebar_text">Login to view review</div> -->
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#Login-form" class="btn btn-primary btn-secondary me-3 mb-2">Add To Cart</a>
                        @else
                            <div class="empty_cart_sidebar_text">No Review</div>
                        @endif
                        <!-- <div class="button_request">
                            <a href="javascript:;" data-toggle="modal" data-target="#feedback-popup" class="site-button black"><span>WRITE A REVIEW</span></a>
                        </div> -->
                    </div>
                @endif

            </div>
            <!-- <div class="view_all"><a href="#">View All</a></div> -->
        </div>
    </div>
    @endif
</section>