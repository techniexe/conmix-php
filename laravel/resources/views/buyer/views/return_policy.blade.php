@section('title', 'Cancellations, Returns, and Refund Policy')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Cancellations, Returns, and Refund Policy</span>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">
                <div class="mb-2">
                    <h6>Cancellations, Returns, and Refund Policy </h6>
                    <p>Last updated: 25th April 2024</p>
                    <p>ConMix - A Product of Conmate E-Commerce Pvt. Ltd. </p>
                    <p>Thank you for choosing Conmix.</p>
                    <p>If, for any reason, you are not completely satisfied with a purchase made through our platform, we invite you to review our policy on Cancellations, refunds, and returns. </p>
                </div>
                <div class="mb-2">
                    <h6>Interpretation and Definitions</h6>
                    <p>Interpretation: The words of which the initial letter is capitalised have meanings defined under the following conditions. The following definitions shall have the same meaning regardless of whether they appear in singular or plural.
</p>
                    
                </div>
                
                <div class="mb-2">
                    <h6>Definitions:</h6>
                    <p>
                        <b>Company:</b> (referred to as either "the Company", "We", "Us", or "Our" in this Agreement) refers to ConMix- (A Product of Conmate E-Commerce Pvt. Ltd.)), the provider of the online Ready Mix Concrete (RMC) booking platform.
                    </p>
                    <p>
                        <b>RMC (Ready Mix Concrete):</b> refers to concrete that is manufactured in a batching plant, according to a set engineered mix design as per IS Codes (IS10262, IS456), and then delivered to a construction site by truck-mounted transit mixers.
                    </p>
                    <p>
                        <b>Customer:</b> (Referred to as either ”Client”, Or “Buyer”) refers to any individual or entity that accesses or uses the ConMix platform to order RMC requirements.
                    </p>
                    <p>
                        <b>RMC Partner:</b> refers to any supplier or vendor that supplies RMC through the ConMix platform online.
                    </p>
                    <p>
                        <b>Service:</b> refers to the online platform provided by ConMix for ordering RMC requirements. Customer can also order RMC through the official ConMix mobile application available on the Google Play store, to be downloaded through the link available on the ConMix website.
                    </p>
                    <p>
                        <b>Booking:</b> refers to an order of RMC placed by a Buyer through ConMix platform.
                    </p>
                    <p>
                        <b>Website:</b> This refers to the ConMix website, accessible from <a href="www.conmix.in">www.conmix.in</a>.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>1. Cancellation Policy:</h6>
                    <p>Customer-initiated Cancellations:</p>
                    <p>
                    Customers may cancel their booking for an RMC requirements through the ConMix platform subject to the following conditions:
                    </p>
                    <ul class="mb-3">
                        <li>A. Cancellations made within 24 hours of the Booking service may be eligible for a full or partial refund, unless the supplier has assigned Transit Mixer for the placed order, as determined by the terms agreed between ConMix and the RMC partner.</li>
                        <li>B. Cancellations made outside of the above specified timeframe may be subject to cancellation fees or may not be eligible for a refund.</li>
                        <li>C. Customers must initiate cancellations through the ConMix platform or contact ConMix customer support for assistance. However, ConMix does not guarantee cancellation of the RMC order.

                        
                    </ul>
                    <p>
                    RMC Partner-initiated Cancellations:
                    </p>
                    <ul class="mb-3">
                        <li>A. If an RMC partner needs to cancel a booked service due to unforeseen circumstances (e.g., equipment failure, logistical issues), ConMix will notify the customer promptly either through registered email ID, SMS, or notification on ConMix platform.
</li>
                        <li>B. ConMix will work with the customer to offer alternative booking options or provide a full refund, as per the customer's preference. However, ConMix does not guarantee such alternative RMC Service.</li>
                        <li>C. ConMix will endeavour to minimise the inconvenience caused to the customer and facilitate a smooth resolution.</li>
                        
                    </ul>
                </div>
                
                <div class="mb-2">
                    <h6>2. Return of RMC Service:</h6>
                    <p>Service Quality Concerns:</p>

                    <ul class="mb-3">
                        <li>A. If a customer is dissatisfied with the quality of the RMC service delivered (e.g., Slump Test readings, concrete cube strength), they may report the issue to ConMix customer support through the platform.</li>
                        <li>B. ConMix will investigate the reported issue promptly and shall work with the customer and the RMC partner to resolve the matter satisfactorily. However, ConMix does not take responsibility of the quality of concrete supplied.</li>
                        <li>C. Depending on the circumstances, ConMix may offer a return or replacement of the service, a partial refund, or other appropriate resolution measures as decided by ConMix Administration.</li>
                        
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>3. Refund Policy:</h6>
                    <p>Eligibility for Refunds:</p>

                    <ul class="mb-3">
                        <li>Refunds may be issued under the following circumstances:</li>
                        <li>A. Cancellations made within the specified timeframe as described in the cancellation policy.
</li>
                        <li>B. RMC partner-initiated cancellations resulting in the unavailability of the booked service.</li>
                        <li>C. Service quality concerns reported by the customer and validated through investigation and if the concern found is only related to RMC supply quality & no other external factors.</li>
                        <li>D. Refund eligibility and amount will be determined based on the terms agreed between ConMix and the RMC partner.</li>
                        
                    </ul>
                    <p>Refund Process:</p>

                    <ul class="mb-3">
                        <li>A. Refund requests must be submitted through the ConMix platform or by contacting ConMix customer support through registered email.</li>
                        <li>B. Refunds will be processed within 15 business days of approval, and the amount will be credited back to the original payment method used for the booking.</li>
                        <li>C. Customers will receive email confirmation once the refund has been processed.</li>
                        
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>4. Dispute Resolution:</h6>
                    <p>Mediation and Resolution:</p>

                    <ul class="mb-3">
                        <li>A. In the event of disputes regarding cancellations, returns, or refunds, ConMix will act as a mediator to facilitate resolution between the customer and the RMC partner.</li>
                        <li>B. ConMix will conduct a fair and impartial investigation into the dispute and work towards reaching a mutually satisfactory resolution.</li>
                        
                        
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>5. Policy Review and Updates:</h6>
                    <p>ConMix reserves the right to review and update this policy periodically and from time to time to ensure alignment with business practices, customer needs, and regulatory requirements.
Any changes to the policy will be communicated to customers  either through email or sms and shall be available on the official ConMix website.

</p>
                </div>
                <div class="mb-2">
                    <h6>6. Contact Us</h6>
                    <p>If you have any questions about our Returns and Refunds Policy, please contact us through email:
</p>                <p>
                        <b>By email:</b> <a href="mailto:support@conmix.in">support@conmix.in</a>                        
                    </p>
                </div>
                <!-- <div class="mb-2">
                    <h6>Lorem Ipsum is simply dummy</h6>
                    <ul class="mb-3">
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>Lorem Ipsum is simply dummy</h6>
                    <p class="mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
                <div class="mb-2">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div> -->
            </div>
        </div>
    </section>
    <!-- ABOUT COMPANY SECTION END -->
    
    <!-- REQUEST A QUOTE SECTION START -->
    <!-- <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                    <p>Have any ideas in your mind?</p>
                    <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                </div>
                <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                    <div class="button_request">
                        <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- REQUEST A QUOTE SECTION END -->
</div>
<!-- CONTENT END -->

@endsection