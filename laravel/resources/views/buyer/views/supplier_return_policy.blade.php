@section('title', 'Supplier Policy: Cancellation, Refund, and Rejection')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Supplier Policy: Cancellation, Refund, and Returns</span>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">
                <div class="mb-2">
                    <h6>Supplier Policy: Cancellation, Refund, and Rejection </h6>
                    <h6>ConMix - A Product of Conmate E-Commerce Pvt. Ltd. </h6>
                    <p>Last Updated: 25th April 2024</p>
                    <p>Thank you for partnering with ConMix as a supplier. This policy outlines the procedures and responsibilities regarding the cancellation, refund, and Return of Ready Mix Concrete (RMC) orders placed by ConMix’s customers.</p>
                    
                </div>
                <div class="mb-2">
                    <h6>Definitions:</h6>
                    <p><b>Company:</b>  (referred to as either "the Company", "We", "Us", or "Our" in this Agreement) refers to ConMix, the provider of the online Ready Mix Concrete (RMC) booking platform.

                    </p>
                    
                    <p><b>RMC (Ready Mix Concrete):</b> refers to concrete that is manufactured in a batching plant, according to a set engineered mix design as per IS Codes (IS10262, IS456), and then delivered to a construction site by truck-mounted transit mixers.

                    </p>
                    
                    <p><b>Customer:</b> (Referred to as either ”Client”, Or “Buyer”) refers to any individual or entity that accesses or uses the ConMix platform to book RMC requirements.
                    </p>
                    
                    <p><b>RMC Partner:</b>  refers to any supplier or vendor that provides RMC through the ConMix platform online.
                    </p>
                    
                    <p><b>Service:</b>  refers to the online platform provided by ConMix for ordering RMC requirements. Customer can also order RMC through the official ConMix mobile application available on the Google Play store, to be downloaded through the link available on the ConMix website

                    </p>
                    
                    <p><b>Booking:</b>  refers to an order of RMC placed by a Buyer through ConMix platform. 

                    </p>
                    
                    <p><b>Parties:</b> refer to ConMix’s Customer and RMC Partner.
                    </p>
                    
                    <p><b>Website:</b> refers to the ConMix website, accessible from <a href="www.ConMix.in">www.ConMix.in</a>.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>1. Cancellation Policy:</h6>
                    <h6>1.1 Cancellation by Supplier:</h6>
                    <p>
                    Suppliers may cancel accepted RMC orders under exceptional circumstances, such as technical issues at the plant, force majeure events, or breakdown of assigned Transit Mixer. However, such cancellation may be penalised as decided by ConMix.Suppliers must notify ConMix and the buyer immediately upon cancellation and provide valid reasons for the cancellation, which may also be verified by ConMix Administration through sources. 


                    </p>
                    <p>
                    Late Delivery by Supplier: If the supplier fails to deliver the order within the agreed-upon timeframe as set by the buyer, resulting in delays at their construction site, then the buyer may request for compensation or cancellation with a full refund. In such case, Supplier agrees to borne the compensation or full refund. ConMix will facilitate communication between the parties but will not bear any financial responsibility whatsoever.


                    </p>
                    
                    <h6>1.2 Cancellation Fee:</h6>
                    <p>
                    Suppliers may be subject to a cancellation fee if orders are cancelled by them before the set delivery date or are undelivered without any valid reason. In such a case, Supplier is subjected to either blacklisting or termination from the ConMix platform upon two or more such instances. ConMix shall charge a cancellation fee as decided by the ConMix Administration for such cancellation or undelivered orders and will be the sole responsibility of the Supplier.


                    </p>
                    
                </div>
                
                <div class="mb-2">
                    <h6>2. Refund Policy:</h6>

                    <h6>2.1 Refund for Cancelled Orders:</h6>
                    <p>
                    If an RMC order is cancelled by the supplier or cannot be fulfilled due to unforeseen circumstances, ConMix will facilitate communication between the supplier and the buyer either through a phone call or an email. However, any refunds or penalties resulting from the cancellation will be the sole responsibility of the Supplier.

                    </p>
                    
                    <h6>2.2 Refund for Quality Issues:</h6>
                    <p>
                    In the event that delivered RMC fails to meet required quality standards (i.e., slump test and cube strength test) and is rejected by the buyer, then the supplier is responsible for providing a full refund to the buyer through ConMix. ConMix will assist in coordinating the resolution but will not bear any financial responsibility whatsoever. In such an event pertaining to Quality Issues, the supplier shall be subjected to blacklisting or termination on the ConMix platform for two or more such instances.

                    </p>
                    
                    <h6>2.3 Refund for Changes in Order Specifications:</h6>
                    <p>
                    If the buyer modifies the order specifications after placing the order, resulting in a cancellation by the supplier, ConMix may facilitate communication between the parties. In such a scenario, no payment shall be processed to the Supplier. However, if any payment is done against the said order then any resulting refund will be the responsibility of the supplier.

                    </p>
                </div>
                <div class="mb-2">
                    <h6>3. Return Policy:</h6>

                    <h6>3.1 Return Due to Quality Issues:</h6>
                    <p>
                    If RMC delivered by the supplier is rejected by the buyer on-site due to any type of quality issues, then the supplier bears all costs associated with the rejected order, including transportation and labour charges if any. ConMix may facilitate communication between the parties but will not be liable financially whatsoever.

                    </p>
                    
                    <h6>3.2 Return Due to Non-compliance:</h6>
                    <p>
                    If the delivered RMC does not comply with the specifications agreed upon in the order, the supplier must take responsibility for the Return and provide a replacement or full refund as per the buyer's preference. ConMix will assist in the process but will not bear any financial responsibility whatsoever. ConMix shall warn the supplier for such non-compliance practices and may also include termination or blacklisting from the ConMix platform.

                    </p>
                    
                    <h6>3.3 Return Due to Miscommunication or Errors:</h6>
                    <p>
                    In case of miscommunication or errors in order details, such as incorrect quantity or mix design, ConMix will work with both parties to rectify the situation. Any resulting refunds or penalties will be the responsibility of the buyer and supplier as per their mutual decision in such matters.

                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>4. Dispute Resolution:</h6>
                    <h6>4.1 Mediation and Resolution:</h6>
                    <p>In the event of disputes regarding cancellations, refunds, or Returns, ConMix will act as a mediator to facilitate resolution between the supplier and the buyer. However, ConMix will not be liable financially for any outcome whatsoever.
</p>

                </div>
                <div class="mb-2">
                    <h6>5. Policy Review and Updates:</h6>
                    <h6>5.1 Policy Review:</h6>
                    <p>ConMix reserves the right to review and update its policies periodically or from time to time to ensure alignment with business practices, regulatory requirements, and feedback from stakeholders. Any changes to the policy will be communicated to suppliers either through email or SMS and shall be available on the official ConMix website.


                    </p>
                    
                    <p>By adhering to these policies, suppliers can ensure transparency, accountability, and customer satisfaction in their dealings with ConMix and its buyers using the ConMix platform.


                    </p>
                    <p>For any questions or concerns about this policy, please contact ConMix at <a href="mailto:support@conmix.in">support@conmix.in</a>   .

                    </p>

                </div>
                
                <!-- <div class="mb-2">
                    <h6>Lorem Ipsum is simply dummy</h6>
                    <ul class="mb-3">
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>Lorem Ipsum is simply dummy</h6>
                    <p class="mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
                <div class="mb-2">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div> -->
            </div>
        </div>
    </section>
    <!-- ABOUT COMPANY SECTION END -->
    
    <!-- REQUEST A QUOTE SECTION START -->
    <!-- <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                    <p>Have any ideas in your mind?</p>
                    <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                </div>
                <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                    <div class="button_request">
                        <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- REQUEST A QUOTE SECTION END -->
</div>
<!-- CONTENT END -->

@endsection