@section('title', 'Custom RMC List')
@extends('buyer.layouts.buyer_layout')

@section('content')

<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>Custom Design Mix</span>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <!-- <div class="custom_rmc_tag my-4 d-flex align-items-center">
            <span>Custom RMC :</span>
            <div class="tag">Rajpardi <a href="#"><i class="fas fa-times"></i></a></div>
            <div class="tag">UltraTech <a href="#"><i class="fas fa-times"></i></a></div>
            <div class="tag">Vadagam <a href="#"><i class="fas fa-times"></i></a></div>
            <div class="tag">Regular <a href="#"><i class="fas fa-times"></i></a></div>
        </div> -->
        <div class="custom_rmc_list mb-4">
            <div class="row">
                @if(isset($data["data"]) && count($data["data"]) > 0)
                <?php //dd($data["data"]); ?>
                    @foreach($data["data"] as $value)

                            <div class="col-md-6">
                                <a class="custom_rmc_bx mb-4" href="{{ route('buyer_mix_detail',[$value['vendor_id'],$value['address_id']]) }}">

                                    <div class="custom_rmc_heading p-3 d-flex justify-content-between align-items-center">
                                        <h4 class="d-grid">
                                            <div>Supplier : <span class="color_orange">{{ isset($value["company_name"]) ? $value["company_name"] : $value["vendor_name"]}}</span></div>
                                        </h4>
                                        
                                        <div class="custom_rmc_price text-end">
                                            <p>₹{{ number_format($value["selling_price_with_margin"]) }} / Cu.Mtr <br><span> {{ ($value["with_TM"] == 'true') ? (($value["with_CP"] == 'true') ? 'With TM & Pump' : 'With TM') : 'RMC only'  }} </span></p>
                                            <div class="product_distance"> <img src="{{asset('assets/buyer/images/ic_arrow.svg')}}" alt="direction arrow"> {{ number_format($value["distance"],2) }} km</div>
                                        </div>
                                    </div>
                                    <div class="custom_rmc_info px-3">
                                        <p>
                                            <small class="color_orange"><i class="fas fa-map-marker-alt" aria-hidden="true"></i> 
                                            @if(isset($value["line1"]) && !empty($value["line1"]))

                                                {{ $value["line1"] }}, {{ $value["city_name"] }}, {{ $value["state_name"] }}



                                            @endif
                                            </small>
                                        </p>    
                                    </div>
                                    <div class="custom_rmc_info px-3">
                                        <h6 class="color_sky my-3">{{ $value["concrete_grade_name"] }}</h6>
                                        <div class="row gx-5">
                                            <div class="col-sm-6">
                                                <p>Cement ( Kg ) : <span>{{ $value["cement_quantity"] }}</span></p>
                                                <p>Coarse Sand ( Kg ) : <span>{{ $value["sand_quantity"] }}</span></p>
                                                <p>Fly Ash ( Kg ) : <span>{{ isset($value["fly_ash_quantity"]) ? $value["fly_ash_quantity"] : 0 }}</span></p>
                                                <p>Admixture ( Kg ) : <span>{{ isset($value["admix_quantity"]) ? $value["admix_quantity"] : 0 }}</span></p>
                                            </div>
                                            <div class="col-sm-6">
                                                <p>{{ $value["aggregate1_sub_category_name"] }} ( Kg ) : <span>{{ $value["aggregate1_quantity"] }}</span></p>
                                                <p>{{ $value["aggregate2_sub_category_name"] }} ( Kg ) : <span>{{ $value["aggregate2_quantity"] }}</span></p>
                                                <p>Water ( Ltr ) : <span>{{ $value["water_quantity"] }}</span></p>
                                                <p>Grade : <span>{{ $value["concrete_grade_name"] }}</span></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="desi_mix_dec px-3">
                                        <p class="pt-3"><b>Design Mix Description</b><br/>
                                        Cement Brand : {{ $value["cement_brand_name"] }},
                                        Coarse Sand Source : {{ isset($value["sand_source_name"]) ? $value["sand_source_name"] : ''}},
                                        Coarse Sand Zone : {{ isset($value["sand_zone_name"]) ? $value["sand_zone_name"] : ''}},
                                        {{ isset($value["aggregate_source_name"]) ? 'Aggregate Source :'. $value["aggregate_source_name"].',' : '' }}
                                        {{ isset($value["admix_brand_name"]) ? 'Admixture Brand :'. $value["admix_brand_name"].',' : '' }}
                                        {{ isset($value["fly_ash_source_name"]) ? 'Fly Ash Source Name :'. $value["fly_ash_source_name"].',' : '' }}
                                        Water : {{ isset($value["final_water_type"]) ? $value["final_water_type"] : ''}}</p>
                                    </div>
                                </a>

                            </div>

                    @endforeach

                @else

                    <div class="empty_cart_sidebar wht_bx">
                        <div class="empty_cart_sidebar_img">
                            <img src="{{asset('assets/buyer/images/norecord.jpg')}}" alt="Empty screen">
                        </div>
                        <div class="empty_cart_sidebar_text">No RMC Supplier Found In Your Location </div>
                        <div class="button_request">
                            <a href="{{ route('buyer_home') }}" class="btn btn-primary">Continue Shopping</a>
                        </div>
                    </div>

                @endif

            </div>
        </div>
    </div>
</section>

@endsection