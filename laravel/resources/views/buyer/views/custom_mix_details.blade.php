@section('title', 'Custom RMC Detail')
@extends('buyer.layouts.buyer_layout')

@section('content')
<?php //dd($cart_details);
$total_mix = 0;
if(isset($data["data"])){
    $total_mix = count($data["data"]);

}
$min_order_quantity = 3;

$order_quantity = 3;

if(Session::get("selected_delivery_qty") != null){
    $order_quantity = Session::get("selected_delivery_qty");
}

$product_id = 1;
?>
@php
    $profile = session('buyer_profile_details', null);
@endphp
<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>Custom Design Mix</span>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <?php //dd($data); ?>
        @if(isset($data["data"]) && (count($data["data"]) > 0))
        <div class="prod_dtl_top wht_bx px-4 mb-4">
            <div class="row">
                <div class="col-md-6">
                    <div class="prod_dtl_slider pt-4">
                        <div id="carouselExampleIndicators" class="carousel slide mb-4" data-bs-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
                                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>
                                <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                @if($total_mix > 0)
                                    @if(isset($data["data"][0]))
                                        <?php $first_record = $data["data"][0]; ?>
                                        @if(isset($first_record["vendor_media"]) && !empty($first_record["vendor_media"]))
                                        @foreach($first_record["vendor_media"] as $media_value)
                                            @if($media_value['media_type'] == "image")
                                                <div class="carousel-item {{ $media_value['type'] == 'PRIMARY' ? 'active' : '' }}">
                                                    <img src="{{ $media_value['media_url'] }}" class="d-block w-100" alt="banner">
                                                </div>
                                            @endif
                                        @endforeach
                                        @else
                                            <div class="carousel-item active">
                                                <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="d-block w-100" alt="banner">
                                            </div>
                                        @endif
                                    @else
                                        <div class="carousel-item active">
                                            <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="d-block w-100" alt="banner">
                                        </div>
                                    @endif
                                @else
                                    <div class="carousel-item active">
                                        <img src="{{asset('assets/buyer/images/category_truck.webp')}}" class="d-block w-100" alt="banner">
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    @if($total_mix > 0)
                        @if(isset($data["data"][0]))
                            <?php $first_record = $data["data"][0]; ?>
                            <input Type="hidden" name="vendor_id" id="detail_vendor_id" value="{{ $first_record['vendor_id'] }}">
                            <input Type="hidden" name="address_id" id="detail_address_id" value="{{ $first_record['address_id'] }}">
                            <?php //dd(Session::get("buyer_TM_details",null));  ?>
                            @if(Session::get("buyer_TM_details",null) != null)

                                <input Type="hidden" name="is_TM_detail_avail" id="is_TM_detail_avail" value="1">

                            @else

                                <input Type="hidden" name="is_TM_detail_avail" id="is_TM_detail_avail" value="0">

                            @endif
                            <div class="prod_dtl_top_right">
                                <div class="custom_rmc_heading pt-4 d-flex justify-content-between align-items-center mb-2">
                                    <h4 class="d-grid mb-1"><small>{{isset($first_record["company_name"]) ? $first_record["company_name"] : $first_record["vendor_name"]}}</small>
                                    
                                    </h4>
                                    <div class="custom_rmc_price text-end">
                                        <p>
                                            <span class="product_distance d-block mb-1"> <img src="{{asset('assets/buyer/images/ic_arrow.svg')}}" alt="direction arrow"> {{ number_format($first_record["distance"]) }} km</span>
                                            ₹{{ number_format($first_record["selling_price_with_margin"], 2) }}/ <span>Cu.Mtr</span> <small class="text-danger text-nowrap">(Excl. GST)</small>
                                        </p>
                                    </div>
                                </div>
                                <div class="d-flex avail_stock_pump justify-content-between mb-1">
                                    <!-- <p class="avail_stock mb-0 me-3">Availability : <span class="text-success">In stock</span> -->
                                    <small class="color_orange"><i class="fas fa-map-marker-alt" aria-hidden="true"></i>
                                    @if(isset($first_record["line1"]) && !empty($first_record["line1"]))

                                        {{ $first_record["line1"] }}, {{ $first_record["city_name"] }}, {{ $first_record["state_name"] }}



                                    @endif
                                    </small>
                                    <p><span id="with_TM_or_CP_check"> {{ ($first_record["with_TM"] == 'true') ? (($first_record["with_CP"] == 'true') ? 'With TM & Pump' : 'With TM') : 'RMC only'  }} </span></p>
                                    <div class="with_mixer_pump">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" disabled type="checkbox" value="" id="TM_checkbox" {{ $first_record["with_TM"] == 'true' ? 'checked' : ''}}>
                                            <input type="hidden" value="{{ number_format($first_record["TM_price"],2) }}" id="TM_checkbox_price" />
                                            <label class="form-check-label">
                                                With Transit Mixer (TM)
                                            </label>
                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="₹ {{ number_format($first_record["TM_price"],2) }} / Cu.Mtr Per KM"><i class="fas fa-info-circle"></i></a>
                                        </div>
                                        <div class="form-check form-check-inline me-0">
                                            <input class="form-check-input" type="checkbox" value="" id="CP_checkbox" {{ $first_record["with_CP"] == 'true' ? 'checked' : ''}}>
                                            <input type="hidden" value="{{ number_format($first_record["CP_price"],2) }}" id="CP_checkbox_price" />
                                            <label class="form-check-label">
                                                With Concrete Pump (CP)
                                            </label>
                                            <a href="javascript:void(0);" data-bs-toggle="tooltip" data-bs-placement="top" title="₹ {{ number_format($first_record["CP_price"],2) }} / day"><i class="fas fa-info-circle"></i></a>
                                        </div>
                                        <!-- <div class="custom_rmc_qty d-flex align-items-center justify-content-end">  <i>Concrete pump available with Transit Mixer only</i></div> -->
                                    </div>

                                </div>
                                <div class="custom_rmc_qty d-flex align-items-center text-danger mb-1 justify-content-end">  <i>Delivery Date:
                                <?php
                                    if(Session::get("selected_display_delivery_date") != null){
                                        echo date('d M Y', strtotime(Session::get("selected_display_delivery_date")));
                                    }
                                ?>
                                </i></div>
                                @if(isset($data["suggestion"]) && (count($data["suggestion"]) > 0))

                                    @if(isset($data["suggestion"]["ErrMsgForTM"]))
                                        <p class="error">{{ $data["suggestion"]["ErrMsgForTM"] }}</p>
                                    @endif

                                    @if(isset($data["suggestion"]["ErrMsgForCp"]))
                                        <p class="error">{{ $data["suggestion"]["ErrMsgForCp"] }}</p>
                                    @endif


                                @endif
                                <div class="custom_rmc_info px-3">
                                    <h6 class="color_sky my-3">{{ $first_record["concrete_grade_name"] }}</h6>
                                    <div class="row gx-5">
                                        <div class="col-sm-6">
                                            <p>Cement ( Kg ) : <span>{{ $first_record["cement_quantity"] }}</span></p>
                                            <p>Coarse Sand ( Kg ) : <span>{{ $first_record["sand_quantity"] }}</span></p>
                                            <p>Fly Ash ( Kg ) : <span>{{ isset($first_record["fly_ash_quantity"]) ? $first_record["fly_ash_quantity"] : 0 }}</span></p>
                                            <p>Admixture ( Kg ) : <span>{{ isset($first_record["admix_quantity"]) ? $first_record["admix_quantity"] : 0 }}</span></p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ $first_record["aggregate1_sub_category_name"] }} ( Kg ) : <span>{{ $first_record["aggregate1_quantity"] }}</span></p>
                                            <p>{{ $first_record["aggregate2_sub_category_name"] }} ( Kg ) : <span>{{ $first_record["aggregate2_quantity"] }}</span></p>
                                            <p>Water ( Ltr ) : <span>{{ $first_record["water_quantity"] }}</span></p>
                                            <p>Grade : <span>{{ $first_record["concrete_grade_name"] }}</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 mb-1 custom_rmc_qty d-flex align-items-center">
                                    <span class="me-3">Qty :</span>
                                    <div class="qty-block">
                                        <button type="button"  class="sub1 qty_sub"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                        <input type="number" id="qty_input1" class="form-control qty_input1 class_{{ $product_id }}" data-product-id="{{ $product_id }}_custom_product_id" value="{{ $order_quantity }}" min="{{ $min_order_quantity }}" max="9999">
                                        <button type="button" class="add1 qty_add"><i class="fa fa-plus" aria-hidden="true"></i></button>

                                    </div>
                                    <span class="ms-2">Cu.Mtr</span>

                                    <span class="error" id="product_detail_qty_error_{{ $product_id }}_custom_product_id"></span>
                                    <input type="hidden" value="{{ $min_order_quantity }}" id="product_page_minimum_order" />
                                </div>
                                <div class="mb-4 custom_rmc_qty d-flex align-items-center">  <i>Minimum Order Qty 3 Cu.Mtr</i></div>
                                <div class="pro_dtl_btn d-flex mb-3 flex-wrap">
                                    <!-- <a href="#" class="btn btn-primary me-3 mb-2">Cutstom Design Mix</a> -->
                                    @if(!isset($profile))
                                        <a href="javascript:void(0)" onclick="loginAddTocart('product_add_to_cart_{{ $product_id }}')" data-bs-toggle="modal" data-bs-target="#Login-form" class="btn btn-primary btn-secondary me-3 mb-2">Add To Cart</a>
                                        <a href="javascript:void(0)" onclick="addToCart('{{ csrf_token() }}','{{ json_encode($first_record) }}','{{ $min_order_quantity }}','slide_cart',1)" id="product_add_to_cart_{{ $product_id }}" class="" style="display:none;"></a>
                                        <!-- <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#Login-form" class="btn btn-primary btn-secondary mb-2">BUY NOW</a> -->
                                    @else
                                        <a href="javascript:void(0)" onclick="addToCart('{{ csrf_token() }}','{{ json_encode($first_record) }}','{{ $min_order_quantity }}','slide_cart',1,'{{ $product_id }}')" class="btn btn-primary btn-secondary me-3 mb-2">ADD TO CART</a>
                                        <!-- <a href="javascript:void(0)" onclick="buynow('{{ csrf_token() }}','{{ json_encode($first_record) }}','{{ $min_order_quantity }}','slide_cart',1,'{{ $product_id }}')"  class="btn btn-primary btn-secondary mb-2">BUY NOW</a> -->
                                    @endif
                                    <input type="hidden" id="buy_now_button_clicked" value="0" />
                                    <input type="hidden" id="clicked_product_id" value="" />
                                </div>


                                <div class="prod_dtl_des wht_bx">

                                    <div class="mb-2">
                                        <h6>Material source for design mix</h6>
                                        <ul class="row">
                                            <li class="col-6 mb-1">Cement Brand : {{ $first_record["cement_brand_name"] }} </li>
                                            <li class="col-6 mb-1">Coarse Sand Source : {{ isset($first_record["sand_source_name"]) ? $first_record["sand_source_name"] : '-' }} </li>
                                            <li class="col-6 mb-1">Coarse Sand Zone : {{ isset($first_record["sand_zone_name"]) ? $first_record["sand_zone_name"] : '-' }} </li>
                                            @if(isset($first_record["aggregate_source_name"]))
                                                 <li class="col-6 mb-1">Aggregate Source : {{ $first_record["aggregate_source_name"] }}</li>
                                            @endif
                                            @if(isset($first_record["admix_brand_name"]))
                                                <li class="col-6 mb-1">Admixture Brand :{{ $first_record["admix_brand_name"].' '.$first_record["admix_category_name"] }}</li>
                                            @endif
                                            @if(isset($first_record["fly_ash_source_name"]))
                                                <li class="col-6 mb-1">Fly Ash Source Name :{{ $first_record["fly_ash_source_name"] }} </li>
                                            @endif
                                            <li class="col-6 mb-1">Water : {{ isset($first_record["final_water_type"]) ? $first_record["final_water_type"] : ''}} </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
             @if($total_mix > 0)
             <?php $product_id++; ?>
                @if(isset($data["data"]))
                    <?php $count = 0; ?>
                    @foreach($data["data"] as $value)
                        @if ($count == 0)
                            <? $count++; ?>
                            @continue

                        @endif

                        <div class="col-md-6">
                            <div class="custom_rmc_bx mb-4">
                                <div class="custom_rmc_heading p-3 d-flex justify-content-between align-items-center flex-wrap">
                                    <h4 class="d-grid mb-0">{{$value["concrete_grade_name"]}}</h4>
                                    <p>Seller Name : <span class="color_orange">{{isset($value["company_name"]) ? $value["company_name"] : $value["vendor_name"]}}</span></p></h4>
                                    <div class="custom_rmc_price text-end">
                                        <p>₹{{ number_format($value["selling_price_with_margin"], 2) }} <span>Cu.Mtr</span> <small class="text-danger">(Excl. GST)</small>
                                        <span class="with_tm_cp d-block">With TM &amp; Pump</span></p>
                                    </div>
                                </div>
                                <div class="custom_rmc_info px-3">
                                    <h6 class="color_orange my-3">{{ $value["concrete_grade_name"] }}</h6>
                                    <div class="row gx-5">
                                        <div class="col-sm-6">
                                            <p>Cement ( Kg ) : <span>{{ $value["cement_quantity"] }}</span></p>
                                            <p>Coarse Sand ( Kg ) : <span>{{ $value["sand_quantity"] }}</span></p>
                                            <p>Fly Ash ( Kg ) : <span>{{ isset($value["fly_ash_quantity"]) ? $value["fly_ash_quantity"] : 0 }}</span></p>
                                            <p>Admixture ( Kg ) : <span>{{ isset($value["admix_quantity"]) ? $value["admix_quantity"] : 0 }}</span></p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>{{ $value["aggregate1_sub_category_name"] }} ( Kg ) : <span>{{ $value["aggregate1_quantity"] }}</span></p>
                                            <p>{{ $value["aggregate2_sub_category_name"] }} ( Kg ) : <span>{{ $value["aggregate2_quantity"] }}</span></p>
                                            <p>Water ( Ltr ) : <span>{{ $value["cement_quantity"] }}</span></p>
                                            <p>Grade : <span>{{ $value["concrete_grade_name"] }}</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="px-3 my-4 d-flex align-items-center justify-content-between flex-wrap">
                                    <div class="custom_rmc_qty d-flex align-items-center pe-2 mb-2">
                                        <span class="me-3">Qty :</span>
                                        <div class="qty-block">
                                            <button type="button" class="sub1 qty_sub"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                            <input type="number" class="form-control qty_input1 class_{{ $product_id }}" data-product-id="{{ $product_id }}_custom_product_id" value="{{ $order_quantity }}" min="{{ $min_order_quantity }}" max="9999">
                                            <button type="button" class="add1 qty_add"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                        </div>
                                        <span class="ms-2">Cu.Mtr</span>

                                        <span class="error" id="product_detail_qty_error_{{ $product_id }}_custom_product_id"></span>
                                    </div>
                                    <div class="mb-4 custom_rmc_qty d-flex align-items-center">  <i>Minimum Order Qty 3 Cu.Mtr</i></div>
                                    <div class="custom_rmc_buy_btn">
                                        @if(!isset($profile))

                                            <a href="javascript:void(0)" onclick="loginAddTocart('product_add_to_cart_{{ $product_id }}')" data-bs-toggle="modal" data-bs-target="#Login-form" class="btn btn-primary btn-secondary mb-2">ADD TO CART</a>
                                            <a href="javascript:void(0)" onclick="buynow('{{ csrf_token() }}','{{ json_encode($value) }}','{{ $min_order_quantity }}','slide_cart',1,'{{ $product_id }}')"  class="" style="display:none;"></a>
                                        @else

                                            <a href="javascript:void(0)" onclick="buynow('{{ csrf_token() }}','{{ json_encode($value) }}','{{ $min_order_quantity }}','slide_cart',1,'{{ $product_id }}')"  class="btn btn-primary btn-secondary mb-2">ADD TO CART</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="mb-4 custom_rmc_qty d-flex align-items-center text-danger">  <i>Delivery Date:
                                <?php
                                    if(Session::get("selected_display_delivery_date") != null){
                                        echo date('d M Y', strtotime(Session::get("selected_display_delivery_date")));
                                    }
                                ?>
                                </i></div>


                                <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">

                                    <div class="mb-2">
                                        <h6>Material source for design mix</h6>
                                        <ul class="row">
                                            <li class="col-6 mb-1">Cement Brand : {{ $value["cement_brand_name"] }} </li>
                                            <li class="col-6 mb-1">Coarse Sand Source : {{ isset($value["sand_source_name"]) ? $value["sand_source_name"] : '-' }} </li>
                                            <li class="col-6 mb-1">Coarse Sand Zone : {{ isset($value["sand_zone_name"]) ? $value["sand_zone_name"] : '-' }} </li>
                                            @if(isset($value["aggregate_source_name"]))
                                                 <li class="col-6 mb-1">Aggregate Source : {{ $value["aggregate_source_name"] }}</li>
                                            @endif
                                            @if(isset($value["admix_brand_name"]))
                                                <li class="col-6 mb-1">Admixture Brand :{{ $value["admix_brand_name"].' '.$value["admix_category_name"] }}</li>
                                            @endif
                                            @if(isset($value["fly_ash_source_name"]))
                                                <li class="col-6 mb-1">Fly Ash Source Name :{{ $value["fly_ash_source_name"] }} </li>
                                            @endif
                                            <li class="col-6 mb-1">Water : {{ isset($value["final_water_type"]) ? $value["final_water_type"] : ''}} </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php $product_id++; ?>
                    @endforeach
                @endif
            @endif

        </div>
        @else

        <p>No Data Found</p>

        @endif
    </div>
</section>

<script>



@if(isset($data["suggestion"]) && (count($data["suggestion"]) > 0))

$(document).ready(function(){

    <?php $is_dialog_open = 0; ?>

    @if(isset($data["suggestion"]["ErrMsgForTM"]))
        $("#TM_change_date_error").html('{{$data["suggestion"]["ErrMsgForTM"]}} Please change delivery date');
        <?php $is_dialog_open = 1; ?>
    @endif

    @if(isset($data["suggestion"]["ErrMsgForCp"]))
        $("#CP_change_date_error").html('{{$data["suggestion"]["ErrMsgForCp"]}} Please change delivery date');
        <?php $is_dialog_open = 1; ?>
    @endif

    @if($is_dialog_open == 1)
        $("#choose_time_popup").modal("show");
    @endif

});

@endif


</script>

@endsection