@section('title', 'My Notifications')
@extends('buyer.layouts.buyer_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<?php

    $notification_types = array(

        // 12 => "Dear, {{NAME}} Truck has been assigned to your order of id #{{ORDER_ID}}",
        // 1 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Placed. It will be confirm soon.", //orderPlaced
        // 2 => "Dear, {{NAME}} for item #{{ORDER_ITEM}} Your order has been Processed.", //orderProcess
        // 3 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Accepted by vendor.", //orderAccept
        // 4 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Rejected by vendor.", //orderRejected
        // 5 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Confirmed.", //orderConfirm
        // 6 => "Dear, {{NAME}} TM has been assigned to your order for item #{{ORDER_ITEM}}.", //truckAssigned
        // 7 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been picked up.", //orderPickup
        // 8 => "Dear, {{NAME}} Your Order item for item #{{ORDER_ITEM}} has been rejected.", //orderItemReject
        // 9 => "Dear, {{NAME}} Your Order for item for item #{{ORDER_ITEM}} has been rejected.", //orderReject
        // 10 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delayed.", //orderDelay
        // 11 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delivered partially.", //partiallyOrderDelivered
        // 12 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delivered.", //orderDelivered

        // 1 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been received & will be confirmed within 2 hours.", //orderPlaced
        // 2 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Processed.", //orderProcess
        // 3 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Accepted by vendor name.", //orderAccept
        // 4 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Rejected & thus cancelled. Your refund of Rs will be processed in 3 working days", //orderRejected
        // 5 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Confirmed by vendor name.", //orderConfirm
        // 6 => "Transit Mixer has been assigned to your prduct id #{{ORDER_ITEM}}  for order no #{{ORDER_ID}}.", //truckAssigned
        // 7 => "Your Order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been picked up.", //orderPickup
        // 8 => "Your Order item with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been rejected & thus cancelled. Your refund of Rs will be processed in 3 working days", //orderItemReject
        // 9 => "Your Order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been rejected & thus cancelled. Your refund of Rs will be processed in 3 working days", //orderReject
        // 10 => "Transit Mixer with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} is on the way and delayed by 1 hours owing to Reason. Kindly bear with us", //orderDelay
        // 11 => "Your Order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been delivered partially.", //partiallyOrderDelivered
        // 12 => "Your Order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been delivered.", //orderDelivered

        1 => "Hi {{NAME}}, your order with order Id #{{ORDER_ID}} has been placed. We will intimate you once your order is confirmed. ", //orderPlaced
        2 => "Your order with prduct id #{{ORDER_ITEM}} for order no #{{ORDER_ID}} has been Processed.", //orderProcess
        3 => "Hi {{NAME}}, your product id #{{ORDER_ITEM}} having order id #{{ORDER_ID}} has been accepted & confirmed by RMC Supplier {{SUPPLIER_NAME}}.", //orderAccept
        4 => "Hi {{NAME}}, delivery of your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date ({{DELIVERY_DATE}}). Thus, kindly assign quantity from order detail page.", //Assign Qty for the product
        5 => "Hi {{NAME}}, your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been rejected by the RMC Supplier {{SUPPLIER_NAME}}.", //orderRejected
        6 => "Hi {{NAME}}, your order #{{ORDER_ID}} has been confirmed by  the RMC supplier {{SUPPLIER_NAME}}. Your order delivery will begin from 'Delivery date'.", //orderConfirm
        7 => "Hi {{NAME}}, Transit Mixer has been assigned to your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} by RMC Supplier {{SUPPLIER_NAME}}", //truckAssigned
        8 => "Hi {{NAME}}, your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been picked up by RMC Supplier {{SUPPLIER_NAME}}", //orderPickup
        9 => "Hi {{NAME}}, you have rejected the product id #{{ORDER_ITEM}} supplied by {{SUPPLIER_NAME}} having an order id #{{ORDER_ID}}", //orderItemReject
        10 => "Hi {{NAME}}, you have rejected the order having an order id #{{ORDER_ID}} supplied by {{SUPPLIER_NAME}}.", //orderReject
        11 => "Hi {{NAME}}, your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been delayed by ‘No. Of hours’ owing to ‘reason for delay’.", //orderDelay
        12 => "Hi {{NAME}}, your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been partially delieverd by the RMC Supplier {{SUPPLIER_NAME}}.", //partiallyOrderDelivered
        13 => "Hi {{NAME}}, your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been fully delieverd by the RMC Supplier {{SUPPLIER_NAME}}.", //partiallyOrderDelivered
        14 => "Hi {{NAME}}, your order with an order id #{{ORDER_ITEM}} has been fully delivered and successfully completed by the RMC Supplier {{SUPPLIER_NAME}}", //orderDelivered
        15 => "Hi {{NAME}}, you have cancelled the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}}. Your order is thus partially cancelled.", //orderDelivered
        16 => "Hi {{NAME}}, you have cancelled your order fully having an order id #{{ORDER_ID}}", //orderDelivered
        17 => "Hi {{NAME}}, your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} has been shortclosed by the RMC supplier {{SUPPLIER_NAME}} as the balance quantity is less than 3 cum.", //orderDelivered
        18 => "Hi {{NAME}}, CP has been assigned to your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} by RMC Supplier {{SUPPLIER_NAME}}", //orderDelivered
        19 => "Hi {{NAME}}, this is a reminder for you to kindly assign the RMC Qty for product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} to be supplied by RMC Supplier {{SUPPLIER_NAME}}.", //orderDelivered
        20 => "Hi {{NAME}}, this is a final reminder for you to kindly assign the RMC Qty for product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} to be supplied by RMC Supplier {{SUPPLIER_NAME}}. If you fail to assign the qty now then your order will be lapsed as per Buyer Purchase Policy.", //orderDelivered
        21 => "Hi {{NAME}}, CP has been picked up by RMC supplier {{SUPPLIER_NAME}} for delivery at your {{SITE_NAME}} site for your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}}", //orderDelivered
        22 => "Hi {{NAME}}, CP has been delivered by RMC supplier {{SUPPLIER_NAME}} at your {{SITE_NAME}} site for your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}}", //orderDelivered
        23 => "Hi {{NAME}}, delivery of your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} will begin once you assign its quantity, date and time before 24 hours of the chosen end delivery date. ", //orderDelivered
        24 => "Hi {{NAME}}, your order having an order id #{{ORDER_ID}} has been lapsed as you failed to assign its qty.", //orderDelivered
        25 => "Hi {{NAME}}, CP has been delayed for your product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} by {{DELAY_HOURS}} owing to {{REASON_FOR_DELAY}}.", //orderDelivered
        26 => "Hi {{NAME}}, Admin has replied to your support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        27 => "Hi {{NAME}}, Admin has resolved and closed your support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        28 => "Hi {{NAME}}, you have received a request for reassigning your product having a product id #{{ORDER_ITEM}} for an order having an order id #{{ORDER_ID}} by RMC Supplier {{SUPPLIER_NAME}} at your {{SITE_NAME}} site. You shall accept or may reject his Request. You are required to take action within 24 hours to avoid cancellation of such affected part of the product or its order.", //orderReassignRequest

    );

    $notification_types_name = array(

        // 12 => "Dear, {{NAME}} Truck has been assigned to your order of id #{{ORDER_ID}}",
        // 1 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Placed. It will be confirm soon.", //orderPlaced
        // 2 => "Dear, {{NAME}} for item #{{ORDER_ITEM}} Your order has been Processed.", //orderProcess
        // 3 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Accepted by vendor.", //orderAccept
        // 4 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Rejected by vendor.", //orderRejected
        // 5 => "Dear, {{NAME}} Your order for item #{{ORDER_ITEM}} has been Confirmed.", //orderConfirm
        // 6 => "Dear, {{NAME}} TM has been assigned to your order for item #{{ORDER_ITEM}}.", //truckAssigned
        // 7 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been picked up.", //orderPickup
        // 8 => "Dear, {{NAME}} Your Order item for item #{{ORDER_ITEM}} has been rejected.", //orderItemReject
        // 9 => "Dear, {{NAME}} Your Order for item for item #{{ORDER_ITEM}} has been rejected.", //orderReject
        // 10 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delayed.", //orderDelay
        // 11 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delivered partially.", //partiallyOrderDelivered
        // 12 => "Dear, {{NAME}} Your Order for item #{{ORDER_ITEM}} has been delivered.", //orderDelivered

        1 => "Order Placed",
        2 => "Order Process" ,
        3 => "Order Accept" ,
        4 => "Assign Qty for the product" ,
        5 => "Order Rejected",
        6 => "Order Confirm",
        7 => "TM Assigned",
        8 => "Order Pickup",
        9 => "Order Product Reject",
        10 => "Order Reject",
        11 => "Order Delay",
        12 => "Partially Product Delivered",
        13 => "Fully Product Delivered",
        14 => "Order Delivered",
        15 => "Partially Order Cancelled",
        16 => "Order Cancelled",
        17 => "Order Short Close",
        18 => "CP Assigned",
        19 => "Reminder Befor Seven Day For Client Assigned Qty",
        20 => "Reminder Befor One Day For Client Assigned Qty",
        21 => "CP Pickup",
        22 => "CP Delivered",
        23 => "Order Item Lapsed",
        24 => "Order Lapsed",
        25 => "CP Delay",
        26 => "Reply Of Support Ticket By Admin",
        27 => "Resolve Support Ticket By Admin",
        28 => "Order Reassigning Request Sent By Supplier",


    );

?>

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <!-- <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="{{ route('buyer_home') }}">Home</a></li>
                <li>Order List</li>
            </ul>
        </div>
    </div> -->
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Notifications list</span>
            </div>
        </div>
    </section>
    <!-- BREADCRUMB ROW END -->

    <!-- PRODUCT LISTING BLOCK -->
    <!-- SECTION CONTENT -->
    <?php //dd($data["data"]["notifications"]); ?>
    <section>
        <div class="container">
            <div class="order_list_tbl wht_bx p-3 mb-4">
                <h5 class="mb-3">Notifications</h5>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Notifications</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                                $first_created_date = '';
                                $last_created_date = '';
                                $count = 0;
                            @endphp
                        @if(isset($data["data"]["notifications"]) && count($data["data"]["notifications"]) > 0)
                            @foreach($data["data"]["notifications"] as $value)
                                @if($count == 0)
                                    @php
                                        $first_created_date = $value["created_at"];
                                        $count++;
                                    @endphp
                                @endif
                                @php
                                    $last_created_date = $value["created_at"];
                                @endphp
                                <?php
                                    $msg = "";
                                    $is_order = 1;
                                    if(isset($notification_types[$value["notification_type"]])){
                                        $msg = $notification_types[$value["notification_type"]];
                                    }else{
                                        $msg = "Notification message";
                                    }


                                    if(isset($value["to_user"]["full_name"])){
                                        $msg = str_replace("{{NAME}}",$value["to_user"]["full_name"],$msg);
                                    }

                                    if(isset($value["order"]["display_id"])){
                                        $msg = str_replace("{{ORDER_ID}}",$value["order"]["display_id"],$msg);
                                    }

                                    if(isset($value["order_item"]["display_item_id"])){
                                        $msg = str_replace("{{ORDER_ITEM}}",$value["order_item"]["display_item_id"],$msg);
                                    }
                                    
                                    if(isset($value["order_item"]["delivery_date"])){
                                        $msg = str_replace("{{DELIVERY_DATE}}",$value["order_item"]["delivery_date"],$msg);
                                    }

                                    if(isset($value["vendor"]["full_name"])){
                                        $msg = str_replace("{{SUPPLIER_NAME}}",$value["vendor"]["full_name"],$msg);
                                    }

                                    if(isset($value["site"]["site_name"])){
                                        $msg = str_replace("{{SITE_NAME}}",$value["site"]["site_name"],$msg);
                                    }

                                    if(isset($value["CP_order_track"]["reasonForDelay"])){
                                        $msg = str_replace("{{REASON_FOR_DELAY}}",$value["CP_order_track"]["reasonForDelay"],$msg);
                                    }

                                    if(isset($value["CP_order_track"]["delayTime"])){
                                        $msg = str_replace("{{DELAY_HOURS}}",$value["CP_order_track"]["delayTime"],$msg);
                                    }

                                    if(isset($value["support_ticket"]["ticket_id"])){
                                        $is_order = 0;
                                        $msg = str_replace("{{TICKET_NO}}",$value["support_ticket"]["ticket_id"],$msg);
                                    }

                                    $created = date('d M Y', strtotime($value["created_at"]));


                                ?>
                                <tr>
                                    <td>{{ isset($notification_types_name[$value["notification_type"]]) ? $notification_types_name[$value["notification_type"]] : "No Type Defined" }}</td>
                                    <td>
                                        <a href="{{isset($value['order']['_id']) ? route('buyer_order_detail',$value['order']['_id']) : ''}}">
                                            {{$msg}}
                                        </a>
                                    </td>
                                    <td class="text-nowrap">{{ AdminController::time_elapsed_string('@'.strtotime($value["created_at"])) }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">Notifications Empty</td>
                            </tr>


                        @endif
                            <!-- <tr>
                                <td>ORDER PROCESS</td>
                                <td>Dear, Dhwani shah Your order for item #53824555555 has been Processed</td>
                                <td>17 hours ago</td>
                            </tr>
                            <tr>
                                <td>ORDER PROCESS</td>
                                <td>Dear, Dhwani shah Your order for item #53824555555 has been Processed</td>
                                <td>17 hours ago</td>
                            </tr>
                            <tr>
                                <td>ORDER PROCESS</td>
                                <td>Dear, Dhwani shah Your order for item #53824555555 has been Processed</td>
                                <td>17 hours ago</td>
                            </tr>
                            <tr>
                                <td>ORDER PROCESS</td>
                                <td>Dear, Dhwani shah Your order for item #53824555555 has been Processed</td>
                                <td>17 hours ago</td>
                            </tr>
                            <tr>
                                <td>ORDER PROCESS</td>
                                <td>Dear, Dhwani shah Your order for item #53824555555 has been Processed</td>
                                <td>17 hours ago</td>
                            </tr>
                            <tr>
                                <td>ORDER PROCESS</td>
                                <td>Dear, Dhwani shah Your order for item #53824555555 has been Processed</td>
                                <td>17 hours ago</td>
                            </tr>
                            <tr>
                                <td>ORDER PROCESS</td>
                                <td>Dear, Dhwani shah Your order for item #53824555555 has been Processed</td>
                                <td>17 hours ago</td>
                            </tr> -->
                        </tbody>
                    </table>
                </div>
                <div class="data-box-footer clearfix">

                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]["notifications"]) && count($data["data"]["notifications"]) > 0)

                        @if(isset($data["data"]["notifications"]) && count($data["data"]["notifications"]) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        @if(isset($data["data"]["notifications"]) && (count($data["data"]["notifications"]) >=  ApiConfig::PAGINATION_LIMIT))
                        <div class="pagination cust_pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{route(Route::current()->getName())}}" method="get">
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="btn btn-primary btn-secondary me-3">Previous</button>
                            </form>
                            @endif
                        @endif
                        @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{route(Route::current()->getName())}}" method="get" class="m-l10">
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="btn btn-primary btn-secondary">Next</button>
                            </form>
                            @endif
                        @endif
                        </div>
                        @endif
                    </div>
                </div>


            </div>
            </div>
        </div>
    </section>
    <!-- PRODUCT LISTING BLOCK END -->

</div>
<!-- CONTENT END -->


@endsection