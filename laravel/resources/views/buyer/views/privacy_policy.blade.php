@section('title', 'Privacy Policy')
@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <section class="breadcrumbs-fs">
        <div class="container">
            <div class="breadcrumbs my-4">
                <a href="{{ route('buyer_home') }}">Home</a>
                <span>Privacy Policy</span>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="prod_dtl_des wht_bx px-4 mb-4 py-2">
                <div class="mb-2">
                    <h6>Privacy Policy </h6>
                    <p>Last Updated:  9th April 2024</p>
                    <p>ConMix - A Product of Conmate E-Commerce Pvt. Ltd.</p>
                </div>
                <div class="mb-2">
                    <h6>1. Introduction</h6>
                    <p>Conmix ("us", "we", or "our") operates the Conmix website (the "Service"). This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>2. Information Collection and Use</h6>
                    <p>We collect several different types of information for various purposes to provide and improve our Service to you.


                    </p>
                    <h6>Types of Data Collected</h6>
                    <p>Personal Data: While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you ("Personal Data") or to generate leads for our associates and various customers. Personally identifiable information may include, but is not limited to:

                    </p>
                    
                    <ul class="mb-3">
                        <li>A. Email address</li>
                        <li>B. First name and last name</li>
                        <li>C. Phone number</li>
                        <li>D. Address, State, Province, ZIP/Postal code, City</li>
                    </ul>

                    <p>Usage Data: We may also collect information on how the Service is accessed and used (“Usage Data”). This Usage Data may include information such as your computer’s Internet Protocol address (e.g., IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers, and other diagnostic data.
                    </p>
                    
                    <p>Tracking & Cookies Data: We use cookies and similar tracking technologies to track the activity on our Service and we hold certain information. Cookies are files with a small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Other tracking technologies are also used such as beacons, tags, and scripts to collect and track information and to improve and analyse our Product and Service.

                    </p>      
                    


                    <h6>Location Information:</h6>
                    <p>We may collect and process information about your actual location when you access or use our Service, such as through GPS signals sent by your mobile device.
                    </p>
                </div>
                <div class="mb-2">
                    <h6>3. How We Use Your Information</h6>
                    <p>We use the information we collect for various purposes, including:


                    </p>
                    
                    <ul class="mb-3">
                        <li>A. To provide and maintain our Service.</li>
                        <li>B. To process bookings and transactions.</li>
                        <li>C. To communicate with you about your account, bookings, and updates to our Service.</li>
                        <li>D. To personalize your experience and tailor content and advertisements to your interests.</li>
                        <li>E. To improve our Product and Service and develop new features.</li>
                        <li>F. To monitor and analyse usage trends and preferences.</li>
                        <li>G. To detect, prevent, and address technical issues or fraudulent activity.</li>
                    </ul>

                </div>
                <div class="mb-2">
                    <h6>4. Information Sharing and Disclosure</h6>
                    <p>We may share your information in the following circumstances:

                    </p>
                    
                    <ul class="mb-3">
                        <li>A. With service providers, contractors, and other third parties who assist us in operating our Service, conducting our business, or serving our users.</li>
                        <li>B. With affiliates, associates, subsidiaries, or other companies under common control with us.</li>
                        <li>C. In response to a subpoena, court order, or other legal process.</li>
                        <li>D. To protect our rights, property, or safety, or the rights, property, or safety of others.</li>
                        <li>E. With your consent or at your direction.</li>
                    </ul>
                    
                    

                </div>
                <div class="mb-2">
                    <h6>5. Data Security</h6>
                    <p>We take appropriate measures to protect your personal information from unauthorised access, alteration, disclosure, or destruction. However, please be aware that no method of transmission over the internet or electronic storage is 100% secure, and we cannot guarantee its absolute security whatsoever.

                    </p>
                </div>
                <div class="mb-2">
                    <h6>6. Your Rights and Choices</h6>
                    <p>You have certain rights regarding your personal information, including:


                    </p>

                    <ul class="mb-3">
                        <li>A. The right to access, update, modify, or delete your personal information.</li>
                        <li>B. The right to object to the processing of your personal information.</li>
                        <li>C. The right to restrict the processing of your personal information.</li>
                        <li>D. The right to data portability.</li>
                    </ul>


                    <p>You can exercise these rights by contacting us using the information provided below. We may take up to 15 working days to respond to your such requests.

                    </p>
                </div>
                <div class="mb-2">
                    <h6>7. Changes to This Privacy Policy</h6>
                    <p>We may update our Privacy Policy from time to time. We may notify you of any changes by posting or publishing the new Privacy Policy on this page on our website. However, you are advised to visit the website page to review the Privacy Policy periodically for any changes.

                    </p>
                </div>
                
                <div class="mb-2">
                    <h6>8. Contact Us</h6>
                    <p>If you have any questions about this Privacy Policy, please contact us at:
                    </p>
                    <p>
                        <b>Email:</b> <a href="mailto:support@conmix.in">support@conmix.in</a>                        
                    </p>
                </div>
                <!-- <div class="mb-2">
                    <h6>Lorem Ipsum is simply dummy</h6>
                    <ul class="mb-3">
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                    </ul>
                </div>
                <div class="mb-2">
                    <h6>Policy</h6>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div> -->
            </div>
        </div>
    </section>
</div>
<!-- CONTENT END -->

@endsection