@section('title', 'Order Item Track')
@extends('buyer.layouts.buyer_layout')
<?php use App\Http\Controllers\Admin\AdminController;?>
@section('content')

<!-- CONTENT START -->
    <div class="page-content">
        <!-- BREADCRUMB ROW -->
        <section class="breadcrumbs-fs">
            <div class="container">
                <div class="breadcrumbs my-4">
                    <a href="#">Home</a>
                    <span>Order List</span>
                </div>
            </div>
        </section>
        <!-- BREADCRUMB ROW END -->

        <!-- PRODUCT LISTING BLOCK -->
        <!-- SECTION CONTENT -->
        <section>
            <div class="section-full p-tb50">
                <div class="container">
                	<div class="section-content wht_bx p-3 mb-4">
                        <h5 class="mb-3">My Order</h5>
                        @if(isset($data["data"]))
                            <?php $final_data = $data["data"];
                                // dd($final_data);
                            ?>
                            <div class="track-block p-a20">
                                <div class="d-flex align-items-center justify-content-between mb-2">
                                    @if(isset($final_data["concrete_grade_name"]))
                                        <div class="truck-number-text p-b10">{{ $final_data["concrete_grade_name"] }} - {{ isset($final_data["designMixDetails"]["product_name"]) ? $final_data["designMixDetails"]["product_name"] : 'Custom Mix' }} </div>
                                    @endif
                                    @if(isset($final_data["order_items"]))
                                        <div class="truck-number-text p-b10">{{ $final_data["order_items"]["concrete_grade_name"] }} - {{ isset($final_data["order_items"]["product_name"]) ? $final_data["order_items"]["product_name"] : 'Custom Mix' }}</div>
                                    @endif
                                    <div class="ordlst-hlink-block">
                                        <div class="dropdown order_dtl_drop">
                                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
                                                @if(isset($final_data["invoice_url"]))
                                                    <li><a class="dropdown-item" href="{{ $final_data['invoice_url'] }}" id="download">Download Invoice </a></li>
                                                @endif
                                                @if(isset($final_data["buyer_credit_note"]))
                                                    <li><a class="dropdown-item" href="{{ $final_data['buyer_credit_note'] }}" id="download">Download Credit Note </a></li>
                                                @endif
                                                <!-- @if(isset($final_data["qube_test_report_7days"]))
                                                    <li><a class="dropdown-item" href="{{ $final_data['qube_test_report_7days'] }}" id="download">Download 7th day Cube test report </a></li>
                                                @endif
                                                @if(isset($final_data["qube_test_report_28days"]))
                                                    <li><a class="dropdown-item" href="{{ $final_data['qube_test_report_28days'] }}" id="download">Download 28th day Cube test report </a></li>
                                                @endif -->
                                                <!-- <li><a class="dropdown-item" href="#">7th day Cube test report</a></li>
                                                <li><a class="dropdown-item" href="#">28th day Cube test report</a></li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row p-b20">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="ordlst-id-date-block mb-3">

                                            <div class="ordlst-date-block">
                                                Order Id : <span>#{{ isset($final_data["display_id"]) ? $final_data["display_id"] : ($final_data["order"]["display_id"] ? $final_data["order"]["display_id"] : '') }}</span>
                                            </div>
                                            <div class="ordlst-date-block">
                                                Order Date : <span><!--{{ date('d M Y, h:i:s a', strtotime($final_data["created_at"])) }}-->{{ AdminController::dateTimeFormat($final_data["created_at"]) }}</span>
                                            </div>
                                            <div class="ordlst-date-block">
                                                Pickup Qty : <span>{{ $final_data["pickup_quantity"] }} Cu.Mtr </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="ordlst-id-date-block mb-3">
                                            <div class="ordlst-date-block">
                                                Driver Name : 
                                                @if(isset($final_data["driver_name"]))
                                                    <span>{{ $final_data["driver_name"] }}</span>
                                                @endif
                                                
                                                @if(isset($final_data["TM_driver1_name"]))
                                                    <span>{{ $final_data["TM_driver1_name"] }}</span>
                                                @endif
                                            </div>
                                            <div class="ordlst-date-block">
                                                @if(isset($final_data["driver_mobile_number"]))
                                                    Mobile No.: <span>{{ $final_data["driver_mobile_number"] }}</span>
                                                @endif
                                                
                                                @if(isset($final_data["TM_driver1_mobile_number"]))
                                                    Mobile No.: <span>{{ $final_data["TM_driver1_mobile_number"] }}</span>
                                                @endif
                                            </div>
                                            <div class="ordlst-date-block">
                                                Transit Mixer Reg. No. : <span>{{ isset($final_data["TM_rc_number"]) ? $final_data["TM_rc_number"] : '' }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="ordlst-id-date-block mb-3">
                                            @if(isset($final_data['qube_test_report_7days']))
                                            <div class="ordlst-date-block">
                                                7th Day Cube Test Report : 
                                                <span>
                                                    <!-- <img src="{{ $final_data['qube_test_report_7days'] }}" width="50"/> -->
                                                    <!-- <div class="dropdown order_dtl_drop"> -->
                                                        <a class="dropdown-toggle" style="color:#136e9d;" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <!-- <i class="fas fa-ellipsis-v"></i> -->
                                                            View
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
                                                            <li class="dropdown-item">
                                                                <a href="{{ $final_data['qube_test_report_7days'] }}">Download </a>
                                                                @if(isset($final_data['qube_test_report_7days_accept_reject']) && $final_data['qube_test_report_7days_accept_reject'] == 1)
                                                                    <i class="fas fa-check-circle" style="color:#2ECC71" title="Accepted By You"></i>
                                                                @elseif(isset($final_data['qube_test_report_7days_accept_reject']) && $final_data['qube_test_report_7days_accept_reject'] == 2)
                                                                    <i class="fas fa-times-circle" style="color:#dc3545" 
                                                                    title="Reason Of Rejection: {{ isset($final_data['qube_test_report_7days_reject_reason']) ? $final_data['qube_test_report_7days_reject_reason'] : 'Rejected By You' }}"></i>
                                                                @endif
                                                                
                                                            </li>
                                                            <li class="dropdown-item">
                                                                @if(isset($final_data['qube_test_report_7days_accept_reject']) && $final_data['qube_test_report_7days_accept_reject'] == 0)
                                                                    <a href="javascript:;" onclick="confirmPopup(2,'{{ json_encode($final_data) }}','','Are you sure you want to accept this report?')"><span class="badge bg-success">Accept</span></a>
                                                                    <a href="javascript:;" onclick="confirmPopup(3,'{{ json_encode($final_data) }}','','Are you sure you want to reject this report?')"> <span class="badge bg-danger">Reject</span></a>
                                                                @endif

                                                                @if(isset($final_data['buyer_qube_test_report_7days']) && $final_data['qube_test_report_7days_accept_reject'] == 2)
                                                                    <a href="{{ $final_data['buyer_qube_test_report_7days'] }}">Download My Report </a>
                                                                @endif
                                                            </li>
                                                        </ul>
                                                        
                                                    <!-- </div> -->
                                                </span>
                                            </div>
                                            @endif
                                            @if(isset($final_data['qube_test_report_28days']))
                                            <div class="ordlst-date-block">
                                                28th Day Cube Test Report : 
                                                <span>
                                                    <!-- <img src="{{ $final_data['qube_test_report_7days'] }}" width="50"/> -->
                                                    <!-- <div class="dropdown order_dtl_drop"> -->
                                                        <a class="dropdown-toggle" style="color:#136e9d;" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <!-- <i class="fas fa-ellipsis-v"></i> -->
                                                            View
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
                                                            <li class="dropdown-item">
                                                                <a  href="{{ $final_data['qube_test_report_28days'] }}">Download </a>
                                                                @if(isset($final_data['qube_test_report_28days_accept_reject']) && $final_data['qube_test_report_28days_accept_reject'] == 1)
                                                                    <i class="fas fa-check-circle" style="color:#2ECC71" title="Accepted By You"></i>
                                                                @elseif(isset($final_data['qube_test_report_28days_accept_reject']) && $final_data['qube_test_report_28days_accept_reject'] == 2)
                                                                    <i class="fas fa-times-circle" style="color:#dc3545" 
                                                                    title="Reason Of Rejection: {{ isset($final_data['qube_test_report_28days_reject_reason']) ? $final_data['qube_test_report_28days_reject_reason'] : 'Rejected By You' }}"></i>
                                                                @endif
                                                            </li>
                                                            <li class="dropdown-item">
                                                                @if(isset($final_data['qube_test_report_28days_accept_reject']) && $final_data['qube_test_report_28days_accept_reject'] == 0)
                                                                    <a href="javascript:;" onclick="confirmPopup(4,'{{ json_encode($final_data) }}','','Are you sure you want to accept this report?')"><span class="badge bg-success">Accept</span></a>
                                                                    <a href="javascript:;" onclick="confirmPopup(5,'{{ json_encode($final_data) }}','','Are you sure you want to reject this report?')"> <span class="badge bg-danger">Reject</span></a>
                                                                @endif

                                                                @if(isset($final_data['buyer_qube_test_report_28days']) && $final_data['qube_test_report_28days_accept_reject'] == 2)
                                                                    <a href="{{ $final_data['buyer_qube_test_report_28days'] }}">Download My Report </a>
                                                                @endif
                                                            </li>
                                                        </ul>
                                                    <!-- </div> -->
                                                </span>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="ordlst-hlink-block">
                                            <div class="dropdown order_dtl_drop">
                                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
                                                    <li><a class="dropdown-item" href="javascript:void(0)" id="download">Download Invoice </a></li>
                                                    <li><a class="dropdown-item" href="#">Cancel Order</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                    <!-- <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="ordlst-id-date-block">
                                        <span class="badge bg-green green">Shipped</span>
                                    </div>


                                </div> -->




                                <div class="track-process-step1">
                                    <ol class="progress progress--medium">
                                        <?php
                                            $ordered_status_class = 'is-complete';
                                            $processed_status_class = '';
                                            $truck_assigned_status_class = '';
                                            $pickup_status_class = '';
                                            $delivered_status_class = '';

                                            $cancel_delivered_status = 'Delivered';

                                            if(isset($final_data["event_status"])){

                                                if($final_data["event_status"] == 'TM_ASSIGNED'){

                                                    $ordered_status_class = 'is-complete';
                                                    $processed_status_class = 'is-complete';
                                                    $truck_assigned_status_class = 'is-complete';
                                                    $pickup_status_class = 'is-active';
                                                    $delivered_status_class = 'progress__last';

                                                }else if($final_data["event_status"] == 'PICKUP'){

                                                    $ordered_status_class = 'is-complete';
                                                    $processed_status_class = 'is-complete';
                                                    $truck_assigned_status_class = 'is-complete';
                                                    $pickup_status_class = 'is-complete';
                                                    $delivered_status_class = 'is-active';

                                                }else if($final_data["event_status"] == 'DELAYED'){

                                                    $ordered_status_class = 'is-complete';
                                                    $processed_status_class = 'is-complete';
                                                    $truck_assigned_status_class = 'is-complete';
                                                    $pickup_status_class = 'is-complete';
                                                    $delivered_status_class = 'is-active';

                                                }else if($final_data["event_status"] == 'DELIVERED'){

                                                    $ordered_status_class = 'is-complete';
                                                    $processed_status_class = 'is-complete';
                                                    $truck_assigned_status_class = 'is-complete';
                                                    $pickup_status_class = 'is-complete';
                                                    $delivered_status_class = 'is-complete';

                                                }else if($final_data["event_status"] == 'REJECTED'){

                                                    $ordered_status_class = 'is-complete';
                                                    $processed_status_class = 'is-complete';
                                                    $truck_assigned_status_class = 'is-complete';
                                                    $pickup_status_class = 'is-complete';
                                                    $delivered_status_class = 'is-inactive';

                                                    $cancel_delivered_status = 'Rejected';

                                                }else{

                                                    $ordered_status_class = 'progress__last';
                                                    $processed_status_class = 'progress__last';
                                                    $truck_assigned_status_class = 'progress__last';
                                                    $pickup_status_class = 'progress__last';
                                                    $delivered_status_class = 'progress__last';

                                                }


                                            }else{

                                                if($final_data["order_status"] == 'PROCESSING'){

                                                    $ordered_status_class = 'is-complete';
                                                    $processed_status_class = 'is-active';
                                                    $truck_assigned_status_class = 'progress__last';
                                                    $pickup_status_class = 'progress__last';
                                                    $delivered_status_class = 'progress__last';

                                                }else if($final_data["order_status"] == 'CONFIRMED'){

                                                    $ordered_status_class = 'is-complete';
                                                    $processed_status_class = 'is-complete';
                                                    $truck_assigned_status_class = 'is-active';
                                                    $pickup_status_class = 'progress__last';
                                                    $delivered_status_class = 'progress__last';

                                                }else{

                                                    $ordered_status_class = 'progress__last';
                                                    $processed_status_class = 'progress__last';
                                                    $truck_assigned_status_class = 'progress__last';
                                                    $pickup_status_class = 'progress__last';
                                                    $delivered_status_class = 'progress__last';

                                                }


                                            }

                                        ?>

                                        <li class="{{ $ordered_status_class }}" data-step="1">Order Placed</li>
                                        <li class="{{ $processed_status_class }}" data-step="2">Processed</li>
                                        <li class="{{ $truck_assigned_status_class }}" data-step="3">TM Assigned</li>
                                        <li class="{{ $pickup_status_class }}" data-step="4">Picked Up</li>
                                        <li class="{{ $delivered_status_class }}" data-step="5">{{ $cancel_delivered_status }}
                                            @if(isset($cancel_delivered_status))
                                                @if($cancel_delivered_status == 'Rejected')
                                                    <div class="ordlst-truck-no">
                                                        @if(isset($final_data["refund_id"]))
                                                            Refund Id: <span>{{$final_data["refund_id"]}}</span>
                                                        @endif
                                                        
                                                    </div>
                                                    <div class="ordlst-truck-no">
                                                        @if(isset($final_data["refund_amount"]))
                                                            Refund Amount (Incl. GST): <span>₹ {{$final_data["refund_amount"]}}</span>
                                                        @endif
                                                        
                                                    </div>
                                                @endif
                                            @endif
                                        </li>
                                    </ol>
                                </div>

                                <div class="track-details-block">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Date/Time (As Par Status)</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($final_data["created_at"]))
                                                    <tr>
                                                        <td>
                                                            <!--{{ date('d M Y h:i:s a', strtotime($final_data["created_at"])) }}-->
                                                            {{ AdminController::dateTimeFormat($final_data["created_at"]) }}
                                                        </td>
                                                        <td>
                                                            Product has been placed.
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if(isset($final_data["processed_at"]))
                                                    <tr>
                                                        <td>
                                                           <!-- {{ date('d M Y h:i:s a', strtotime($final_data["processed_at"])) }}-->
                                                            {{ AdminController::dateTimeFormat($final_data["processed_at"]) }}
                                                        </td>
                                                        <td>
                                                            Product has been proccessed.
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if(isset($final_data["assigned_at"]))
                                                    <tr>
                                                        <td>
                                                           <!-- {{ date('d M Y h:i:s a', strtotime($final_data["assigned_at"])) }}-->
                                                            {{ AdminController::dateTimeFormat($final_data["assigned_at"]) }}
                                                        </td>
                                                        <td>
                                                            Transit Mixer has been assigned to your product.
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if(isset($final_data["pickedup_at"]))
                                                    <tr>
                                                        <td>
                                                           <!-- {{ date('d M Y h:i:s a', strtotime($final_data["pickedup_at"])) }}-->
                                                            {{ AdminController::dateTimeFormat($final_data["pickedup_at"]) }}
                                                        </td>
                                                        <td>
                                                            Product has been picked up.
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if(isset($final_data["delayed_at"]))
                                                <tr>
                                                    <td>
                                                    <!--{{ date('d M Y h:i:s a', strtotime($final_data["delayed_at"])) }}-->
                                                    {{ AdminController::dateTimeFormat($final_data["delayed_at"]) }}
                                                    </td>
                                                    <td style="color: #f00;">
                                                        Product has been delayed {{ $final_data["delayTime"] }} due to {{ $final_data["reasonForDelay"] }}
                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($final_data["delivered_at"]))
                                                <tr>
                                                    <td>
                                                        <!--{{ date('d M Y h:i:s a', strtotime($final_data["delivered_at"])) }}-->
                                                        {{ AdminController::dateTimeFormat($final_data["delivered_at"]) }}
                                                    </td>
                                                    <td>
                                                        Product has been delivered.
                                                    </td>
                                                </tr>
                                                @endif
                                                @if(isset($final_data["rejected_at"]))
                                                <tr>
                                                    <td>
                                                        <!--{{ date('d M Y h:i:s a', strtotime($final_data["delivered_at"])) }}-->
                                                        {{ AdminController::dateTimeFormat($final_data["rejected_at"]) }}
                                                    </td>
                                                    <td>
                                                        Product has been rejected due to {{$final_data["reasonForReject"]}}.
                                                    </td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
            <!-- PRODUCT LISTING BLOCK END -->
            <!-- REQUEST A QUOTE SECTION START -->
           <!--  <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                            <p>Have any ideas in your mind?</p>
                            <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                        </div>
                        <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                            <div class="button_request">
                                <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- REQUEST A QUOTE SECTION END -->
    </div>
        <!-- CONTENT END -->


        <!-- Invoice stated -->
        <style>
            body {
                background-color: #FFF;
                font-family: 'Roboto';
                font-size: 14px;
                font-weight: normal;
                color: #767676;
                padding: 0;
                margin: 0;
                overflow-x: hidden;
                line-height: 24px;
            }
            .container {
                max-width: 1170px;
                margin-right: auto;
                margin-left: auto;
            }
            h1, h2, h3, h4, h5, h6 {
                margin-bottom: 0px;
                font-weight: bold;
            }
            h2 {
                font-family: 'Roboto';
                font-weight: bold;
                font-size: 24px;
                line-height: 24px;
                color: #1a1a1a;
            }
            h5 {
                font-size: 16px;
                line-height: 22px;
            }
            h6 {
                font-size: 14px;
                line-height: 18px;
            }
            table {
                border-spacing: 0;
                border-collapse: collapse;
                background-color: transparent;
                width: 100%;
            }
            table.cust_invoice, table.cust_invoice table {
                margin: 0;
                color: #000;
            }
            p, h6, h5 {
                margin: 0;
            }
            table.cust_invoice td {
                border-collapse: collapse;
                vertical-align: top;
                padding: 0;
            }
            .cust_invoice_order_tbl tr th, .cust_invoice_order_tbl tr td {
                border-top: 1px solid #000;
                 border-bottom: : 1px solid #000;
                  border-right: 1px solid #000;
                   border-left: 0px ;
                padding: 10px
            }
           /* .cust_invoice_order_tbl tr th {
                border-bottom: 1px solid #000000;
            }*/
            .cust_invoice_total_tbl tr td {
                padding: 10px;
               /* border-bottom: 1px solid #000000;*/
                text-align: right;
                color: #000;
            }
            .vTitle {
                font-weight: bold;
            }
        </style>
        <div class="container" id="invoice" style="display:none;">

            <!-- <h2>Invoice</h2> -->
            <table class="cust_invoice" style="border: 1px solid #000000; width: 100%; margin-bottom: 50px;">
                <tbody>
                    <tr>
                        <td align="center">
                            <table cellspacing="0" cellpadding="0"  align="center" style="border-bottom: 1px solid #000000;">
                                <tr>
                                    <td width="50%" style="padding: 10px 20px; vertical-align: middle;">
                                    <img src="{{asset('assets/buyer/images/logo.webp')}}" width="200" alt="Conmate"></td>
                                    <td width="50%" style="padding: 10px 20px;">
                                        <div class="inv-nodate-block" style="float: right">
                                            <p><b>Tax Invoice</b> </p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                           <td align="center">
                            <table cellspacing="0" cellpadding="0"  align="center" style="border-bottom: 1px solid #000000;">
                                <tr>
                                    <td width="25%" style="padding: 10px; border-right: 1px solid #000000; text-align: center;" > <p><b>Invoice No :</b> <span>#007612</span></p></td>
                                    <td width="25%" style="padding: 10px; border-right: 1px solid #000000; text-align: center;" ><p><b>Date of Invoice :</b> <span>08 Aug 2021</span></td>
                                 <td width="25%" style="padding: 10px; border-right: 1px solid #000000; text-align: center;" ><p><b>Order Id :</b> <span>#123456</span></td>
                                    <td width="25%" style="padding: 10px;  text-align: center;" ><p><b>Order Date :</b> <span>6 Aug 2021</span></td>

                                </tr>
                            </table>
                        </td>


                    </tr>
                    <tr>
                        <td align="center">
                            <table cellspacing="0" cellpadding="0"  align="center" >
                                <tbody>
                                    <tr>
                                        <td width="50%" style="padding: 10px; border-right: 1px solid #000000;"><h6>Consigner :</h6></td>
                                        <td width="50%" style="padding: 10px; border-left: 1px solid #000000;"><h6>Consignee :</h6></td>
                                    </tr>
                                    <tr>
                                        <td style="border-right: 1px solid #000000;">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td style="padding: 0 10px 10px 10px;">
                                                        <div class="inv-info-block">
                                                            <h5>Conmix - Ready Mix Concrete</h5>
                                                            <p>

                                                                ‘ SCC House ‘ , Opp. Nirma University,
                                                                Nr. Balaji Temple,</br> S. G. Highway, Chharodi,
                                                                Ahmedabad-382481, Gujarat.</br>

                                                                Phone: +91 99096 88845, Email: info@conmix.cin
                                                            </p>
                                                           <p>
                                                                <span class="vTitle">State : </span>
                                                                <span class="vName">Gujarat , Code (24)</span>
                                                            </p>
                                                            <p>
                                                                <span class="vTitle">GST No : </span>
                                                                <span class="vName">GS123456789</span>
                                                            </p>
                                                            <p>
                                                                <span class="vTitle">PAN No :  </span>
                                                                <span class="vName">123456BG</span>
                                                            </p>
                                                        </div>
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                        <td style="border-left: 1px solid #ddd;">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                  <td style="padding: 0 10px 10px 10px;">
                                                        <div class="inv-info-block">
                                                            <h5>Buyer </h5>
                                                            <p>
                                                                102, Sumel II, Near Gurudwara,
                                                                SG Highway Ahmedabad 380054, </br>
                                                                Phone: 079 - 1234 5678, Email: info@test.com
                                                            </p>
                                                           <p>
                                                                <span class="vTitle">State : </span>
                                                                <span class="vName">Gujarat , Code (24)</span>
                                                            </p>
                                                            <p>
                                                                <span class="vTitle">GST No : </span>
                                                                <span class="vName">GS123456789</span>
                                                            </p>
                                                            <p>
                                                                <span class="vTitle">PAN No : </span>
                                                                <span class="vName">123456BG</span>
                                                            </p>
                                                        </div>
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table class="cust_invoice_order_tbl" cellspacing="0" cellpadding="0" align="center" style="border-bottom: 1px solid #000000;">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">Sr.No</th>
                                        <th>Description of Product</th>
                                        <th style="text-align: center;">HSN Code</th>
                                        <th style="text-align: center;">Qty.</th>
                                        <th style="text-align: center;">Unit</th>
                                         <th style="text-align: right;">Price</th>
                                        <th style="text-align: center;">CGST Rate</th>
                                        <th style="text-align: right;">CGST Amount</th>
                                        <th style="text-align: center;">SGST Rate</th>
                                        <th style="text-align: right;">SGST Amount</th>
                                        <th style="text-align: right; border-right:none;">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: center;">1.</td>
                                        <td>Custome Design M10</td>
                                        <td style="text-align: center;">1234</td>
                                        <td style="text-align: center;">47.720</td>
                                        <td style="text-align: center;">Cu.Mtr</td>
                                        <td style="text-align: right;">750.00</td>
                                        <td style="text-align: center;">2.50 %</td>
                                        <td style="text-align: right;">894.75</td>
                                        <td style="text-align: center;">2.50 %</td>
                                        <td style="text-align: right;">897.75</td>
                                        <td style="text-align: right; border-right:none;">37,579.00</td>
                                    </tr>
                                    <tr>
                                        <td colspan="10" style="text-align: center;">Add : Rounded Off (+) </td>
                                        <td style="text-align: right;  border-right:none;">37,579.00 </br>0.50</td>
                                    </tr>
                                    <tr>
                                        <td colspan="10" style="bold;text-align: center;"><span style=" font-weight: bold;">Discount :</span>  Five Hundred  Only</td>
                                        <td style="text-align: right; border-right:none;">- ₹ 500</td>
                                    </tr>
                                    <tr>
                                        <td colspan="10" style="bold;text-align: center;"><span style=" font-weight: bold;">Amount In Words :</span> Ruppees Thirty Seven Thousand Eighty Only</td>
                                        <td style="font-weight: bold;text-align: right; border-right:none;">₹ 37,080</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="60%" cellspacing="0" cellpadding="0"  align="center" style=" width: 60%">
                                <tr>
                                    <td>
                                        <table class="cust_invoice_total_tbl" cellspacing="0" cellpadding="0"  align="center" >
                                            <td style="text-align: left;">
                                                <p>
                                                    <span  style="font-weight: bold; text-align: left;">Grand Total : </span>
                                                    <span>47.720 Cu.Mtr</span>
                                                </p>
                                            </td>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="60%" class="cust_invoice_order_tbl" cellspacing="0" cellpadding="0"  align="center" style="border-bottom: 1px solid #000000; width: 60%;font-size: 12px; border-left: 1px solid #000;margin-left:10px;">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">Sr.No</th>
                                        <th style="text-align: center;">HSN Code</th>
                                        <th style="text-align: center;">Tax Rate</th>
                                        <th style="text-align: center;">Main Qty.</th>
                                        <th style="text-align: right;">Taxable Amt.</th>
                                        <th style="text-align: right;">CGST Amt.</th>
                                        <th style="text-align: right;">SGST Amt.</th>
                                        <th style="text-align: right;">Total Tax</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: center;">1.</td>
                                        <td style="text-align: center;">1234</td>
                                        <td style="text-align: center;">5 %</td>
                                        <td style="text-align: center;">47.720</td>
                                        <td style="text-align: right;">35,790.00</td>
                                        <td style="text-align: right;">897.75</td>
                                        <td style="text-align: right;">894.75</td>
                                        <td style="text-align: right;">1,789.50</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table width="100%" cellspacing="0" cellpadding="0"  align="center" style="width: 100%;font-size: 12px;">
                               <tr>
                                    <td>
                                        <table class="cust_invoice_total_tbl" cellspacing="0" cellpadding="0"  align="center" style="border-bottom: 1px solid #000000;">
                                            <td style="text-align: left;">
                                            </td>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" cellpadding="0"  align="center" style="width: 100%;font-size: 12px;">
                                <tr>
                                    <td align="center">
                                        <table cellspacing="0" cellpadding="0"  align="center" style="border-bottom: 1px solid #000000;padding: 10px;">
                                            <tbody>
                                                <tr>
                                                    <td style="border-right: 1px solid #000000;">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="50%" style="padding: 10px 10px 10px 10px;">
                                                                    <h5>Bank Detail <small>(Conmix bank detail)</small></h5>
                                                                    <p>
                                                                        <span class="vName">Bank Name : </span>
                                                                        <span class="vTitle">ICICI Bank</span>
                                                                    </p>
                                                                    <p>
                                                                        <span class="vName">A/c No  : </span>
                                                                        <span class="vTitle">12345678911112</span>
                                                                    </p>
                                                                    <p>
                                                                        <span class="vName">IFSC Code :  </span>
                                                                        <span class="vTitle">ICIC0002200</span>
                                                                    </p>
                                                                    <p>
                                                                        <span class="vName">Branch Name :  </span>
                                                                        <span class="vTitle">Bodakdev Ahmedabad</span>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="50%" style="border-left: 1px solid #ddd;">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr >
                                                                <td style="padding: 10px 10px 10px 10px; text-align: right; height: 90px;">
                                                                    <span class="vTitle">For Conmix - Ready Mix Concrete</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 10px 10px 10px 10px;  text-align: right;">
                                                                    <span class="vTitle">Authorised Signatory</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table cellspacing="0" cellpadding="0"  align="center">
                                            <tbody>
                                                <tr>
                                                    <td >
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="50%" style="padding: 10px 10px 10px 10px;">
                                                                    <div class="inv-info-block">
                                                                        <h5>Terms & Conditions</h5>
                                                                        <p>
                                                                            <span class="vName">1.Measurement of Material Qty <b>www.conmix.in/privercypolicy</b></br>
                                                                            2.Interest @ 18% p.a will be charged if the payment is not made with the stioulated time.</br>
                                                                            3.Subject to “ Rawatsar ” Jurisdiction Only. </span>
                                                                        </p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table cellspacing="0" cellpadding="0"  align="center" style="border-top: 1px solid #000000; ">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="50%" style="padding: 10px 10px 10px 10px; text-align: center;">
                                                                    <span class="vName"><b>Custome Care:</b> +91 9979016486 , <b>Write Us On :</b>  support@conmix.in</br></span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <script>

window.onload = function () {
    document.getElementById("download")
        .addEventListener("click", () => {
            const invoice = this.document.getElementById("invoice");
            console.log(invoice);
            console.log(window);
            var opt = {
                margin: 5,
                filename: 'myfile.pdf',
                image: { type: 'jpeg', quality: 0.98 },
                html2canvas: { scale: 2 },
                jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
            };
            html2pdf().from(invoice).set(opt).save();
        })
}

        </script>

@endsection