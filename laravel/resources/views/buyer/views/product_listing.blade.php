@section('title', 'Product Listing')
@extends('buyer.layouts.buyer_layout')

@section('content')

<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>
            <?php //dd($data["grade_id"]); ?>
                @if(isset($all_concrete_data[$data["grade_id"]]["name"]))
                    {{$all_concrete_data[$data["grade_id"]]["name"]}}
                @endif
            </span>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="product_listing mb-4">
            <div class="row">
                <?php //dd($data["data"]); ?>
                @if(isset($data["data"]) && count($data["data"]) > 0)
                    @foreach($data["data"] as $value)
                        <div class="col-lg-3 col-md-4 col-sm-4 col-6">
                            <div class="product_list_bx mb-4">
                                <div class="product_list_bx_img">
                                    <!-- <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category"> -->
                                    <?php 
                                        $primary_img = "";

                                        if(isset($value["vendor_media"])){
                                            foreach($value["vendor_media"] as $media_value){
                                                if($media_value["type"] == "PRIMARY"){
                                                    $primary_img = $media_value["site_url"].$media_value["media_url"];
                                                }
                                                
                                            }
                                        }

                                    ?>
                                    @if($primary_img == "")
                                    <a href="{{ route('buyer_product_detail',['id' => $value['_id']]) }}"><img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category"></a>
                                    @else
                                    <a href="{{ route('buyer_product_detail',['id' => $value['_id']]) }}"><img src="{{$primary_img}}" alt="category"></a>
                                    @endif
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="fas fa-info-circle"></i>
                                        </a>
                                        <div class="dropdown-menu product_list_info dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                            <h6 class="py-2">{{ $value["concrete_grade"]["name"] }}</h6>
                                            <div class="info_item px-3">
                                                <p><span>Cement (kg)</span>: {{ $value["cement_quantity"] }}</p>

                                                <p><span>{{ $value["aggregate1_sand_category"]["sub_category_name"] }} (kg)</span>: {{ $value["aggregate1_quantity"] }}</p>
                                                <p><span>{{ $value["aggregate2_sand_category"]["sub_category_name"] }} (kg)</span>: {{ $value["aggregate2_quantity"] }}</p>

                                                <p><span>Coarse Sand (kg)</span>: {{ $value["sand_quantity"] }}</p>
                                                
                                                <p><span>Admixture (kg)</span>: {{ isset($value["ad_mixture_quantity"]) ? $value["ad_mixture_quantity"] : '-' }}</p>
                                                @if(isset($value["fly_ash_quantity"]))
                                                    <p><span>Fly Ash (kg)</span>: {{ $value["fly_ash_quantity"] }}</p>
                                                @endif
                                                
                                                <p><span>Water (ltr)</span>: {{ $value["water_quantity"] }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product_distance"> <img src="{{asset('assets/buyer/images/ic_arrow.svg')}}" alt="direction arrow"> {{ isset($value["distance"]) ? number_format($value["distance"],2) : 0 }} km</div>
                                </div>
                                <div class="product_list_bx_dtl">
                                    <h6 class="mb-1">
                                        <a href="{{ route('buyer_product_detail',['id' => $value['_id']]) }}">{{ $value["concrete_grade"]["name"] }}</a>
                                        @if(isset($value["vendor"]["rating"]) && ($value["vendor"]["rating"] > 0))
                                            <span><i class="far fa-star"></i> {{ number_format($value["vendor"]["rating"],1) }} <sub>/5</sub></span>
                                        @endif
                                    </h6>
                                    <p>Mix Type : <span>{{$value["product_name"]}}</span></p>
                                    <p>Seller Name : <span>{{ $value["vendor"]["company_name"]}}</span></p>
                                    <div class="sell_price mb-1"> Price : <b>₹{{ isset($value["selling_price_with_margin"]) ? number_format($value["selling_price_with_margin"],2) : 0 }}/</b> Cu.Mtr</div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else

                    <!-- <p>No Data Found</p> -->

                    <div class="empty_cart_sidebar wht_bx">
                        <div class="empty_cart_sidebar_img">
                            <img src="{{asset('assets/buyer/images/norecord.jpg')}}" alt="Empty screen">
                        </div>
                        <!-- <div class="empty_cart_sidebar_text">No RMC Supplier Found In Your Location </div> -->
                        <div class="empty_cart_sidebar_text">Error: No RMC Supplier found in your chosen location for the required grade of Concrete. Kindly change either delivery location or your RMC Grade requirement. </div>
                        <div class="button_request">
                            <a href="{{ route('buyer_home') }}" class="btn btn-primary">Continue Shopping</a>
                        </div>
                    </div>

                @endif
               
            </div>
        </div>
    </div>
</section>

<script>

var current_selected_product_id = '{{ $data['grade_id'] }}';

</script>

@endsection