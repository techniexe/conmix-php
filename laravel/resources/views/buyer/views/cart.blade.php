@section('title', 'My Cart')
@extends('buyer.layouts.buyer_layout')

@section('content')
@php
    $profile = session('buyer_profile_details', null);
@endphp
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<?php $applied_code_id = "" ?>
<section class="breadcrumbs-fs">
    <div class="container">
        <div class="breadcrumbs my-4">
            <a href="{{ route('buyer_home') }}">Home</a>
            <span>Cart</span>
        </div>
    </div>
</section>
<section class="">
    <div class="container">
        <div class="row" id="cart_checkout_row_div">
        @if(isset($cart_details["data"]) && count($cart_details["data"]) > 0)
        <?php //dd($cart_details["data"]); ?>
            <div class="col-md-8">
                <div class="cart_item wht_bx p-3 mb-4">
                    <div class="table-responsive cart_table">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-start">Product Name</th>
                                    <th>Qty</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <!-- <th>With TM & Pump <br/>Delivery Date & Time</th> -->
                                    <th>Amount</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <input type="hidden" value="3" id="product_page_minimum_order" />
                            <input type="hidden" value="" id="design_mix_product_details" />
                            @if(isset($cart_details["data"]) && count($cart_details["data"]) > 0)
                                @if(count($cart_details["data"]["items"]) > 0)
                                    @foreach($cart_details["data"]["items"] as $value)
                                    <?php //dd($value); ?>
                                        @if(isset($value["design_mix_id"]))
                                        <!-- Design Mix -->

                                            <tr>
                                                <td class="text-start">
                                                    <div class="tbl_pro_name">
                                                        <b>{{ $value["concrete_grade_name"] }}</b><br/>{{ $value["company_name"] }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="qty-block">
                                                        <button type="button" class="sub qty_sub" data-design-mix-detail="{{ json_encode($value) }}"  data-product= "{{ $value['design_mix_id'] }}" data-product-id= "{{ $value['_id'] }}" data-token-id="{{ @csrf_token() }}" data-is-custom-mix="0"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                        <input type="number" id="qty_input" readOnly data-design-mix-detail="{{ json_encode($value) }}" data-product= "{{ $value['design_mix_id'] }}" data-product-id= "{{ $value['_id'] }}" data-token-id="{{ @csrf_token() }}" data-is-custom-mix="0" class="form-control" value="{{ $value['quantity'] }}" min="3" max="100">
                                                        <button type="button" class="add qty_add" data-design-mix-detail="{{ json_encode($value) }}" data-product= "{{ $value['design_mix_id'] }}" data-product-id= "{{ $value['_id'] }}" data-token-id="{{ @csrf_token() }}" data-is-custom-mix="0"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                    </div>

                                                    <div class="text-center">  <i>Minimum Order Qty 3 Cu.Mtr</i></div>
                                                    <p class="error tbl_degtonly_error" id="qty_digit_error_{{ $value['_id'] }}"></p>
                                                </td>
                                                <td>Cu.Mtr</td>
                                                <td>₹ {{ number_format($value["unit_price_With_Margin"],2) }}</td>
                                                <!-- <td>
                                                    @if(isset($value["with_TM"]) && ($value["with_TM"] == true))
                                                        <div>TM and CP Available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</div>
                                                        <div class="date_time"><br>10:50 AM  <br><a href="javascript:void(0)" onclick="cartChangeDate('{{ csrf_token() }}','{{ json_encode($value) }}','{{ $value['quantity'] }}','cart_checkout',0,'{{ $value['_id'] }}')" class="color_orange">Change</a></div>
                                                    @else
                                                        <div>No TM and CP Available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</div>
                                                        <div class="date_time"><,<br>10:50 AM   <br>
                                                            <a href="javascript:void(0)" onclick="cartChangeDate('{{ csrf_token() }}','{{ json_encode($value) }}','{{ $value['quantity'] }}','cart_checkout',0,'{{ $value['_id'] }}')" class="color_orange">Change</a>
                                                            @if(!isset($value["TM_no"]))
                                                                | <a href="javascript:void(0)" onclick="addTMDetails('{{ csrf_token() }}','{{ json_encode($value) }}','{{ $value['quantity'] }}','cart_checkout',0,'{{ $value['_id'] }}')" class="color_orange">Add TM Details</a>
                                                                <p class="error">Please add TM details as TM is not available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</p>
                                                                <input type="hidden" id="is_TM_details_not_available" value="1"/>
                                                            @endif
                                                        </div>
                                                    @endif
                                                </td> -->
                                                <td>₹ {{ number_format($value["selling_price_With_Margin"],2) }}</td>
                                                <td><a href="javascript:void(0)" onclick="deleteCartItem('{{ csrf_token() }}','{{ $value["_id"] }}','cartslide_{{ $value["_id"] }}','{{ count($cart_details["data"]["items"]) }}','cart_checkout','{{ $value["cart_design_mix_detail_id"] }}')" class="color_orange p-1 remove_cart_item"><i class="far fa-trash-alt"></i></a></td>

                                                <input type="hidden" id="with_TM_{{ $value["_id"] }}" value="{{ $value['with_TM'] }}"/>
                                                <input type="hidden" id="with_CP_{{ $value["_id"] }}" value="{{ $value['with_CP'] }}"/>

                                                <input type="hidden" id="delivery_date_{{ $value["_id"] }}" value="{{ $value['delivery_date'] }}"/>
                                            </tr>

                                            <tr class="tm_pump_change_tr">
                                                <td colspan="6">
                                                    <div class="tm_pump_change">
                                                        <div class="alert alert-primary" role="alert">
                                                            <?php
                                                                $modify_details = $value;
                                                                if(isset($modify_details['description'])){
                                                                    unset($modify_details['description']);
                                                                }

                                                            ?>
                                                            @if(isset($value["with_TM"]) && ($value["with_TM"] == true))
                                                            <div class="d-flex justify-content-between">
                                                                <!-- <div>TM and CP Available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</div> -->
                                                                <div>Your RMC order with Transit Mixer {{ isset($value["with_CP"]) ? ($value["with_CP"] == 'true' ? '& Concrete Pump' : '') : '' }} is available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</div>
                                                                <a href="javascript:void(0)" onclick="cartChangeDate('{{ csrf_token() }}','{{ json_encode($modify_details) }}','{{ $value['quantity'] }}','cart_checkout',0,'{{ $value['_id'] }}','{{ $value['cart_design_mix_detail_id'] }}')" class="color_orange">Modify Date</a>
                                                            </div>
                                                            @else
                                                                <div class="d-flex justify-content-between">
                                                                    <div>Sorry, Your RMC order with Transit Mixer & Concrete Pump is not available on {{ date('d M Y', strtotime($value["delivery_date"])) }}<br/></div>
                                                                    <a href="javascript:void(0)" onclick="cartChangeDate('{{ csrf_token() }}','{{ json_encode($modify_details) }}','{{ $value['quantity'] }}','cart_checkout',0,'{{ $value['_id'] }}','{{ $value['cart_design_mix_detail_id'] }}')" class="color_orange">Modify Date</a>
                                                                </div>
                                                                @if(!isset($value["TM_no"]))
                                                                <div class="d-flex justify-content-between mt-2">
                                                                    <div><p class="error mb-0">Sorry, Transit Mixer is not available on {{ date('d M Y', strtotime($value["delivery_date"])) }}. Please add your Transit Mixer details</p></div>
                                                                    <a href="javascript:void(0)" onclick="addTMDetails('{{ csrf_token() }}','{{ json_encode($modify_details) }}','{{ $value['quantity'] }}','cart_checkout',0,'{{ $value['_id'] }}')" class="color_orange">+ Add TM Details</a>

                                                                    <input type="hidden" id="is_TM_details_not_available" value="1"/>
                                                                </div>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @else
                                        <!-- Custom Mix -->
                                            <?php $custom_mix = $value["custom_mix"]; //dd($custom_mix); ?>
                                            <tr>
                                                <td class="text-start">
                                                    <div class="tbl_pro_name">
                                                        <b>{{ isset($custom_mix["concrete_grade_name"]) ? $custom_mix["concrete_grade_name"] : '' }}</b>
                                                        <br/>{{ isset($custom_mix["company_name"]) ? $custom_mix["company_name"] : '' }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="qty-block">
                                                        <button type="button" id="sub" class="sub qty_sub" data-design-mix-detail="" data-product= "{{ json_encode($value['custom_mix']) }}" data-product-id= "{{ $value['_id'] }}" data-token-id="{{ @csrf_token() }}" data-is-custom-mix="1"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                        <input type="number" class="form-control" readOnly data-design-mix-detail=""  data-product= "{{ json_encode($value['custom_mix']) }}" data-product-id= "{{ $value['_id'] }}" data-token-id="{{ @csrf_token() }}" data-is-custom-mix="1" value="{{ $value['quantity'] }}" min="3" max="100">
                                                        <button type="button" id="add" class="add qty_add" data-design-mix-detail="" data-product= "{{ json_encode($value['custom_mix']) }}" data-product-id= "{{ $value['_id'] }}" data-token-id="{{ @csrf_token() }}" data-is-custom-mix="1"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                    </div>
                                                    <div class="text-center">  <i>Minimum Order Qty 3 Cu.Mtr</i></div>
                                                </td>
                                                <td>Cu.Mtr</td>
                                                <td>₹ {{ isset($value["unit_price_With_Margin"]) ? number_format($value["unit_price_With_Margin"],2) : 0.00 }}</td>
                                                <!-- <td>

                                                    @if(isset($value["with_TM"]) && ($value["with_TM"] == true))
                                                        <div>TM and CP Available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</div>
                                                        <div class="date_time"><,<br>10:50 AM   <br><a href="javascript:void(0)" onclick="cartChangeDate('{{ csrf_token() }}','{{ json_encode($value['custom_mix']) }}','{{ $value['quantity'] }}','cart_checkout',1,'{{ $value['_id'] }}')" class="color_orange">Change</a></div>
                                                    @else
                                                        <div>No TM and CP Available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</div>
                                                        <div class="date_time">,<br>10:50 AM   <br>
                                                            <a href="javascript:void(0)" onclick="cartChangeDate('{{ csrf_token() }}','{{ json_encode($value['custom_mix']) }}','{{ $value['quantity'] }}','cart_checkout',1,'{{ $value['_id'] }}')" class="color_orange">Change</a>
                                                            @if(!isset($value["TM_no"]))
                                                                | <a href="javascript:void(0)" onclick="addTMDetails('{{ csrf_token() }}','{{ json_encode($value['custom_mix']) }}','{{ $value['quantity'] }}','cart_checkout',1,'{{ $value['_id'] }}')" class="color_orange">Add TM Details</a>
                                                                <p class="error">Please add TM detai    ls as TM is not available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</p>
                                                                <input type="hidden" id="is_TM_details_not_available" value="1"/>
                                                            @endif
                                                        </div>
                                                    @endif
                                                </td> -->
                                                <td>₹ {{ number_format($value["selling_price_With_Margin"],2) }}</td>
                                                <td><a href="javascript:void(0)" onclick="deleteCartItem('{{ csrf_token() }}','{{ $value["_id"] }}','cartslide_{{ $value["_id"] }}','{{ count($cart_details["data"]["items"]) }}','cart_checkout')" class="color_orange p-1 remove_cart_item"><i class="far fa-trash-alt"></i></a></td>
                                                <input type="hidden" id="with_TM_{{ $value["_id"] }}" value="{{ $value['with_TM'] }}"/>
                                                <input type="hidden" id="with_CP_{{ $value["_id"] }}" value="{{ $value['with_CP'] }}"/>
                                                <input type="hidden" id="delivery_date_{{ $value["_id"] }}" value="{{ $value['delivery_date'] }}"/>
                                            </tr>

                                            <tr class="tm_pump_change_tr">
                                                <td colspan="6">
                                                    <div class="tm_pump_change">
                                                        <div class="alert alert-primary" role="alert">
                                                            <?php
                                                                $modify_details = $value;
                                                                if(isset($modify_details['description'])){
                                                                    unset($modify_details['description']);
                                                                }

                                                            ?>
                                                            @if(isset($value["with_TM"]) && ($value["with_TM"] == true))
                                                            <div class="d-flex justify-content-between">
                                                                <!-- <div>TM and CP Available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</div> -->
                                                                <div>Your RMC order with Transit Mixer {{ isset($value["with_CP"]) ? ($value["with_CP"] == 'true' ? '& Concrete Pump' : '') : '' }} is available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</div>
                                                                <a href="javascript:void(0)" onclick="cartChangeDate('{{ csrf_token() }}','{{ json_encode($modify_details) }}','{{ $value['quantity'] }}','cart_checkout',1,'{{ $value['_id'] }}')" class="color_orange">Modify Date</a>
                                                            </div>
                                                            @else
                                                                <div class="d-flex justify-content-between">
                                                                    <div>No TM and CP Available on {{ date('d M Y', strtotime($value["delivery_date"])) }}<br/></div>
                                                                    <a href="javascript:void(0)" onclick="cartChangeDate('{{ csrf_token() }}','{{ json_encode($modify_details) }}','{{ $value['quantity'] }}','cart_checkout',1,'{{ $value['_id'] }}')" class="color_orange">Modify Date</a>
                                                                </div>
                                                                @if(!isset($value["TM_no"]))
                                                                <div class="d-flex justify-content-between mt-2">
                                                                    <div><p class="error mb-0">Please add your TM details as TM is not available on {{ date('d M Y', strtotime($value["delivery_date"])) }}</p></div>
                                                                    <a href="javascript:void(0)" onclick="addTMDetails('{{ csrf_token() }}','{{ json_encode($modify_details) }}','{{ $value['quantity'] }}','cart_checkout',1,'{{ $value['_id'] }}')" class="color_orange">+ Add TM Details</a>

                                                                    <input type="hidden" id="is_TM_details_not_available" value="1"/>
                                                                </div>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endif

                                    @endforeach

                                @else

                                    <tr>
                                        <td colspan="6">No Item in cart</td>
                                    </tr>

                                @endif
                            @else

                                <tr>
                                    <td colspan="6">No Item in cart</td>
                                </tr>

                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cart_right p-3 mb-4 wht_bx">
                    <h6 class="mb-3 pb-3">Cart</h6>
                    <div class="select_add_bx">
                    @if(isset($cart_details["data"]["site_id"]))
                        <?php
                            $site_id = $cart_details["data"]["site_id"];
                        ?>

                        <input type="hidden" id="cart_site_id" value="{{ $site_id }}" />
                    @endif
                        <label for="" class="form-label">Select Delivery Address</label>
                        <select id="site_addresses_select" class="form-select">
                            <option value="">Select Delivery Address</option>
                            @if(isset($data["site_details"]))
                                <?php
                                $site_id = "";

                                if(isset($cart_details["data"]["site_id"])){
                                    $site_id = $cart_details["data"]["site_id"];
                                }

                                ?>

                                @foreach($data["site_details"] as $value)
                                    <option value="{{ json_encode($value) }}" {{ $site_id == $value["_id"] ? 'selected' : '' }}>{{ $value["site_name"] }} - {{ $value["address_line1"] }}, {{ $value["address_line2"] }}, {{ $value["city_name"] }} {{ $value["pincode"] }}, {{ $value["state_name"] }}, ({{ $value["country_name"] }})</option>
                                @endforeach
                            @endif
                        </select>
                        @if(isset($data["site_details"]))
                            @foreach($data["site_details"] as $value)
                                @if($site_id == $value["_id"])
                                    <input type="hidden" value="{{ json_encode($value) }}" id="selected_site_detail_{{ $site_id }}"/>
                                @endif
                            @endforeach
                        @endif
                        <p class="text-end py-1 mb-1"><a href="javascript:void(0)" onclick="resetForm('add_site_form','add_site_address_popup')" data-bs-toggle="modal" data-bs-target="#add_site_address_popup"><span class="color_orange">+ Add Delivery Address</span></a></p>

                        <p class="error" id="site_select_error"></p>
                    </div>
                    <!-- Billing Address -->

                    <div class="select_add_bx">
                    @if(isset($cart_details["data"]["billing_address_id"]))
                        <?php
                            $billing_id = $cart_details["data"]["billing_address_id"];
                        ?>

                        <input type="hidden" id="cart_billing_id" value="{{ $billing_id }}" />
                    @endif

                    </div>

                    <!-- Billing Address -->
                    <span class="d-block" style="font-style: italic;">Note : Price may vary as per the selected delivery location, Qty and date</span>
                    <p class="selected_address pb-3" id="site_details">
                        @if(isset($cart_details["data"]["site"]))
                            <?php $site_info = $cart_details["data"]["site"]; //dd($site_info);?>
                            <b class="d-block"><i class="fas fa-map-marker-alt" aria-hidden="true"></i> {{ $site_info["site_name"] }}</b>
                            {{ $site_info["address_line1"] }}, {{ $site_info["address_line2"] }}, <br>  {{ $site_info["city_details"]["city_name"] }} {{ $site_info["pincode"] }}, {{ $site_info["state_details"]["state_name"] }}, (India)
                            <span class="d-block">Site Contact Person: {{ $site_info["person_name"] }}</span>
                            <span class="d-block">Site Mobile No.: {{ $site_info["mobile_number"] }}</span>
                        @else
                            <b>Delivery address not selected. Please select from above list</b>

                                <!-- <p>104, Sumel II, Near GuruDwara, SG Highway <br> Ahmedabad 380054, (INDIA)</p> -->

                            @endif
                    </p>
                    <div class="select_add_bx">
                        <label for="" class="form-label">Select Billing Address</label>
                        <select id="billing_addresses_select" class="form-select">
                            <option value="">Select Billing Address</option>
                            @if(isset($billing_address["data"]))
                                <?php
                                $billing_id = "";

                                if(isset($cart_details["data"]["billing_address_id"])){
                                    $billing_id = $cart_details["data"]["billing_address_id"];
                                }

                                ?>

                                @foreach($billing_address["data"] as $value)
                                    <option value="{{ json_encode($value) }}" {{ $billing_id == $value["_id"] ? 'selected' : '' }}>{{ $value["line1"] }}, {{ isset($value["line2"]) ? $value["line2"] : '' }}, {{ $value["cityDetails"]["city_name"] }} {{ $value["pincode"] }}, {{ $value["stateDetails"]["state_name"] }} </option>
                                @endforeach
                            @endif
                        </select>
                        @if(isset($billing_address["data"]))
                            @foreach($billing_address["data"] as $value)
                                @if($site_id == $value["_id"])
                                    <input type="hidden" value="{{ json_encode($value) }}" id="selected_billing_detail_{{ $billing_id }}"/>
                                @endif
                            @endforeach
                        @endif
                        <p class="text-end py-1 mb-1"><a href="javascript:void(0)" onclick="resetForm('add_billing_address_form','add_billing_address_popup')" data-bs-toggle="modal" data-bs-target="#add_billing_address_popup"><span class="color_orange">+ Add Billing Address</span></a></p>
                    </div>

                    <span class="error" id="billing_select_error"></span>
                    <p class="selected_address pb-3" id="billing_details">
                        @if(isset($cart_details["data"]["biiling_address"]))
                            <?php $billing_info = $cart_details["data"]["biiling_address"]; //dd($billing_info);?>
                            <b class="d-block"><i class="fas fa-map-marker-alt" aria-hidden="true"></i> {{ $billing_info["company_name"] }}</b>
                            {{ $billing_info["line1"] }}, {{ isset($billing_info["line2"]) ? $billing_info["line2"] : '' }}, <br>  {{ $billing_info["city_details"]["city_name"] }} {{ $billing_info["pincode"] }}, {{ $billing_info["state_details"]["state_name"] }}, (India)
                            <span class="d-block">GST No.: {{ $billing_info["gst_number"] }}</span>
                        @else
                            <b>Billing address not selected. Please select from above list</b>

                                <!-- <p>104, Sumel II, Near GuruDwara, SG Highway <br> Ahmedabad 380054, (INDIA)</p> -->

                            @endif
                    </p>
                    @if(false)
                        @if(isset($cart_details["data"]["couponInfo"]))
                            <?php $applied_code_id = isset($cart_details["data"]["couponInfo"]["_id"]) ? $cart_details["data"]["couponInfo"]["_id"] : ''; ?>
                            <div class="promocode promocode-apply">
                                <img src="{{asset('assets/buyer/images/code-img.png')}}">
                                <p>{{ isset($cart_details["data"]["couponInfo"]["code"]) ? $cart_details["data"]["couponInfo"]["code"] : '' }}</p>
                                <a href="javascript:void(0)" onclick="applyOfferCode('', 'remove_coupon')" class="btn-close"></a>
                            </div>

                        @else
                            <div class="promocode ofrSldm-toggle">
                                <img src="{{asset('assets/buyer/images/code-img.png')}}">
                                <p>Apply Promocode</p>
                            </div>
                        @endif
                    @endif

                    <div class="cart_price">
                        <p>Cart Amount : <span>₹{{ isset($cart_details["data"]["selling_price_With_Margin"]) ? number_format($cart_details["data"]["selling_price_With_Margin"],2) : '0' }}</span></p>
                        @if(isset($cart_details["data"]["coupon_amount"]) && $cart_details["data"]["coupon_amount"] > 0)
                             <p>Discount Amount : <span>₹{{ isset($cart_details["data"]["coupon_amount"]) ? number_format($cart_details["data"]["coupon_amount"],2) : '0' }}</span></p>
                        @endif
                        <!-- <p>Transit Mixer Amount : <span>₹5,000.00</span></p>
                        <p>Concrete Pump Amount : <span>₹3,000.00</span></p> -->
                        <!-- <p>GST<small>(18%)</small> : <span>₹{{ isset($cart_details["data"]["gst_price"]) ? number_format($cart_details["data"]["gst_price"],2) : '0' }}</span></p> -->
                        @if(isset($cart_details["data"]["cgst_price"]) && $cart_details["data"]["cgst_price"] > 0)
                            <p>CGST<small>({{ $cart_details["data"]["cgst_rate"] }}%)</small> : <span>₹{{ isset($cart_details["data"]["cgst_price"]) ? number_format($cart_details["data"]["cgst_price"],2) : '0' }}</span></p>
                        @endif
                        @if(isset($cart_details["data"]["sgst_price"]) && $cart_details["data"]["sgst_price"] > 0)
                            <p>SGST<small>({{ $cart_details["data"]["sgst_rate"] }}%)</small> : <span>₹{{ isset($cart_details["data"]["sgst_price"]) ? number_format($cart_details["data"]["sgst_price"],2) : '0' }}</span></p>
                        @endif
                        @if(isset($cart_details["data"]["igst_price"]) && $cart_details["data"]["igst_price"] > 0)
                            <p>IGST<small>({{ $cart_details["data"]["igst_rate"] }}%)</small> : <span>₹{{ isset($cart_details["data"]["igst_price"]) ? number_format($cart_details["data"]["igst_price"],2) : '0' }}</span></p>
                        @endif
                        <?php $total_amount = $cart_details["data"]["total_amount"];
                            $total_amount = $total_amount * 100;
                            // dd($total_amount);
                        ?>
                        <p class="cart_price_total pt-3">Total Amount <span>₹{{ isset($cart_details["data"]["total_amount"]) ? number_format($cart_details["data"]["total_amount"],2) : '0' }}</span></p>
                    </div>
                    <div class="cart_right_btn d-flex justify-content-evenly flex-wrap">
                        <a href="{{ route('buyer_home') }}#home_product_section" class="btn btn-primary my-1">ADD MORE</a>
                        @if(isset($profile))
                                @if(isset($cart_details["data"]))
                                    <?php $cart_item_count = count($cart_details["data"]["items"]) ?>
                                    @if($cart_item_count > 0)
                                        <!-- <a href="javascript:void(0)" onclick="placeOrder()" class="btn btn-primary my-1 btn-secondary">PLACE ORDER</a> -->
                                        <a href="javascript:void(0)" id="paybtn" class="btn btn-primary my-1 btn-secondary">PLACE ORDER</a>

                                        <form name='razorpayform' action="{!!route('payment')!!}" method="POST">
                                            <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id">
                                            <input type="hidden" name="razorpay_signature"  id="razorpay_signature" >
                                        </form>


                                    @endif
                                @endif
                        @else
                            <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#Login-form" class="btn btn-primary my-1 btn-secondary">PLACE ORDER</a>
                        @endif
                    </div>
                </div>
            </div>

        @else

            <div class="empty_cart_sidebar wht_bx">
                <div class="empty_cart_sidebar_img">
                    <img src="{{asset('assets/buyer/images/empty_cart.jpg')}}" alt="Empty screen">
                </div>
                <div class="empty_cart_sidebar_text">No Item in cart</div>
                <div class="button_request">
                    <a href="{{ route('buyer_home') }}" class="btn btn-primary">Continue Shopping</a>
                </div>
            </div>

        @endif


            <!-- Offcanvas Coupon start-->
<div class="ofcnvs_coupn">
    <button type="button" class="ofrSldm-toggle ofrSldm-hamburger">
        <span class="sr-only">toggle navigation</span>
        <span class="ofrSldm-hamburger-icon"></span>
    </button>

    <nav class="ofrSldm-nav" role="navigation">
        <div class="ofrSldm-menu">
            <div class="ofr_list_blk">
                <h3>Apply Coupon</h3>
                <div class="ofr_inpt_blk">
                    <input name="promocode" id="promocode" class="ofr_inptbx form-control" placeholder="Have a couponcode? Enter here" />
                    <button class="ofr_btn" onclick="applyOfferCode('', 'added_coupon')">Apply</button>
                </div>
                <span class="error" id="coupon_errors"></span>
                <div class="ofr_or"><span>OR</span></div>
                <div class="ofr_cod_lst">
                    <h4>Choose from the offers below</h4>
                    <ul>
                        @if(isset($data["coupon_data"]) && (count($data["coupon_data"]) > 0))
                            @foreach($data["coupon_data"] as $value)
                                <li>
                                    <div class="ofrcod_dtl">
                                        <div class="ofrcod_slct">
                                            <input type="radio" id="{{ $value["_id"] }}_code" name="radio-group" onclick="applyOfferCode('{{ $value["code"] }}', 'selected_coupon')" {{ $applied_code_id == $value["_id"] ? 'checked' : '' }}>
                                            <label for="{{ $value["_id"] }}_code">{{ $value["code"] }}</label>
                                        </div>
                                        <div class="ofrcod_dtls">
                                            <p>{{ $value["info"] }} Conditions Apply *</p>
                                            <a href="#" role="button" id="{{ $value["_id"] }}_tnc" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="TndCppbtn dropdown-toggle">*Trems & Conditions</a>
                                            <div class="dropdown-menu TndCppWrp" aria-labelledby="{{ $value["_id"] }}_tnc">
                                                <div class="TndCppCnt">
                                                    {{ $value["tnc"] }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @else

                                <span>No Coupon Available</span>

                        @endif

                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>
<!-- Offcanvas Coupon end-->

        </div>

    </div>




</section>
@php
    $profile = session('buyer_profile_details', null);
    //dd($profile);
@endphp
<script>
// Checkout details as a json

var prefil = {
    "name"              : "{{ isset($profile['full_name']) ? $profile['full_name'] : '' }}",
    "email"             : "{{ isset($profile['email']) ? $profile['email'] : '' }}",
    "contact"           : "{{ isset($profile['mobile_number']) ? $profile['mobile_number'] : '' }}",
}

var notes = {};

@if(isset($site_info))

notes = {
    "address"           : "{{ $site_info["address_line1"] }}, {{ $site_info["address_line2"] }}, {{ $site_info["city_details"]["city_name"] }} {{ $site_info["pincode"] }}, {{ $site_info["state_details"]["state_name"] }}, (India)",
    "merchant_order_id" : "{{ isset($cart_details["data"]["unique_id"]) ? $cart_details["data"]["unique_id"] : '' }}",
}

@endif

var options = {
        key: "{{ Config::get('razor_key.razor_key') }}",
        amount: parseInt('{{ isset($total_amount) ? $total_amount : 0 }}'),
        name: 'Conmix',
        description: '',
        image: '{{asset('assets/buyer/images/logo.webp')}}',
        prefill: prefil,
        notes: notes,
        handler: demoSuccessHandler
    }

/**
 * The entire list of Checkout fields is available at
 * https://docs.razorpay.com/docs/checkout-form#checkout-fields
 */
// options.handler = function (response){
//     document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id;
//     document.getElementById('razorpay_signature').value = response.razorpay_signature;
//     document.razorpayform.submit();
// };

// Boolean whether to show image inside a white frame. (default: true)
// options.theme.image_padding = false;

// options.modal = {
//     ondismiss: function() {
//         console.log("This code runs when the popup is closed");
//     },
//     // Boolean indicating whether pressing escape key
//     // should close the checkout form. (default: true)
//     escape: true,
//     // Boolean indicating whether clicking translucent blank
//     // space outside checkout form should close the form. (default: false)
//     backdropclose: false
// };

var rzp = new Razorpay(options);

document.getElementById('paybtn').onclick = function(e){
    $is_site_selected = $("#site_addresses_select"). children("option:selected"). val();
    $is_billing_addresses_select = $("#billing_addresses_select"). children("option:selected"). val();

    var is_TM_details_not_available = $("#is_TM_details_not_available").val() == undefined ? 0 : $("#is_TM_details_not_available").val();

    if(is_TM_details_not_available == 0){

        if($is_site_selected.length > 0){
            if($is_billing_addresses_select.length > 0){
                rzp.open();
            }else{
                $('#billing_select_error').html("Please select or add billing address from above");    
            }
        }else{
            $('#site_select_error').html("Please select or add delivery address from above");
        }
    }else{
        $('#site_select_error').html("One of your item do not have the TM details. Please add to proceed");
    }
}

function padStart(str) {
        return ('0' + str).slice(-2)
    }

function demoSuccessHandler(transaction) {
        // You can write success code here. If you want to store some data in database.
        $("#paymentDetail").removeAttr('style');
        $('#paymentID').text(transaction.razorpay_payment_id);
        var paymentDate = new Date();
        $('#paymentDate').text(
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes())
                );
        $("#overlay").fadeIn(300);
        $.ajax({
            method: 'post',
            url: "{!!route('payment')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "razorpay_payment_id": transaction.razorpay_payment_id
            },
            complete: function (r) {
                console.log('complete');
                console.log(r);
            },
            success: function (data){
                console.log('success');
                console.log(data);
                $('#payment_fail_popup').hide();
                $('#payment_success_popup').hide();

                if(data["status"] == 200){
                    $('#payment_success_popup').show();

                    setTimeout( function(){
                        // console.log("place order");
                        placeOrder(transaction.razorpay_payment_id);

                    }  , 2000 );

                }else if(data["status"] == 300){
                    $("$pay_error_msg").html(data["error"]["msg"]);
                    $('#payment_fail_popup').show();
                }

                $("#overlay").fadeOut(300);
            }
        })
    }

</script>


@endsection