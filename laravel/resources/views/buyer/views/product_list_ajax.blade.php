@php
    //$value = Cookie::get('name');
    //dd($value);
    $profile = session('buyer_profile_details', null);
@endphp
    @if(isset($data["data"]))
        @if(count($data["data"]) > 0)
        <?php //dd($data["data"]); ?>
        @foreach($data["data"] as $value)
            <?php $product_name = $value['productCategory']["category_name"].' : '.$value['productSubCategory']["sub_category_name"] ?>
            <?php $source_name = $value["address"]['source_name'] ?>
            <div class="product-lst-sllr-block">
                @if(!isset($profile))
                    <a href="javascript:;" data-toggle="modal" data-target="#Login-form" class="addtowishlist" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                @else
                    
                        <a href="javascript:;" id="delete_wishlidt_item_listing" style="{{ $value['is_wishlisted'] ? '' : 'display:none;' }}" onclick="deleteWishListItem('{{ csrf_token() }}','{{ $value['_id'] }}','item_{{ $value['_id'] }}','{{ count($wish_list["data"]) }}','listing_page')" class="addtowishlist active" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                    

                        <a href="javascript:;" id="add_wishlidt_item_listing" style="{{ $value['is_wishlisted'] ? 'display:none;' : '' }}" onclick="addToWishList('{{ csrf_token() }}','{{ $value["_id"] }}','1','{{ $value["productSubCategory"]["_id"] }}','listing_page')" class="addtowishlist" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                    
                    
                @endif
                <form action="" method="post" id="product_detail_form_{{ $value['_id'] }}">
                        @csrf
                        <input type="hidden" name="product_detail" id="product_detail_{{ $value['_id'] }}" /> 
                </form>
                <!-- <a href="javascript:void(0)" onclick="productClick('{{ $value['_id'] }}','{{ json_encode($value) }}')" class="plst-sllr-blk"> -->
                <a href="{{ route('buyer_product_detail',['id' => $value['_id']]) }}" class="plst-sllr-blk">
                    <div class="plst-img-block">
                        <img src="{{ $value['productCategory']['image_url'] }}" alt="" class="img-responsive" />
                    </div>
                    <div class="plst-detls-block">
                        <div class="ptabs-details-block">
                            <h3>Seller : {{ $value["supplier"]["full_name"] }}</h3>
                            <h6>{{ $product_name  }} </span> <span class="st-rating verygood"><i class="fa fa-star"></i> {{ $value["rating_count"] > 0 ? number_format(($value["rating_sum"] / $value["rating_count"]),1) : 0 }} </span></h6>
                            <h6><span>Source : </span> <span>{{ $source_name }}</span></h6>
                            <div class="prd-price-block price">
                                <span class="prd-amunt"><i class="fa fa-rupee"></i> {{ number_format($value["price_with_margin_and_logistics"],2) }}</span>
                                <span style="font-size: 14px;">Per {{ $value["productSubCategory"]["selling_unit"][0] }}</span>
                                <span style="font-size: 12px;color: #f00;">(Excl. GST)</span>
                            </div>
                            @if($value["quantity"] > 0)
                            <div class="availability_stock">Availability: <span class="in_stock">In Stock</span> ({{ $value["quantity"] }} {{ $value["productSubCategory"]["selling_unit"][0] }})</div>
                            @else
                                <div class="availability_stock">Availability: <span class="out_stock">Out Of Stock</span> (0 {{ $value["productSubCategory"]["selling_unit"][0] }})</div>
                            @endif
                            <!--<div class="product-lst-info-block m-a0">
                                <ul>
                                    <?php $vehicle = $value["vehicles"]; ?>
                                    @foreach($vehicle as $vahicle_value)
                                        <li>
                                            <p><i class="fa fa-circle"></i> {{ $vahicle_value["vehicleSubCategory"]["sub_category_name"] }}: {{ $vahicle_value["vehicleSubCategory"]["min_load_capacity"] }} {{ $vahicle_value["vehicleSubCategory"]["weight_unit_code"] }} to {{ $vahicle_value["vehicleSubCategory"]["max_load_capacity"] }} {{ $vahicle_value["vehicleSubCategory"]["weight_unit_code"] }} <span class="p-price"><i class="fa fa-rupee"></i> {{ $vahicle_value["min_trip_price"] }}</span></p>
                                        </li>
                                    @endforeach-->
                                    <!-- <li>
                                        <p><i class="fa fa-circle"></i> 10 Tyre: 18 MT to 21 MT <span class="p-price"><i class="fa fa-rupee"></i> 159.00</span></p>
                                    </li>
                                    <li>
                                        <p><i class="fa fa-circle"></i> 12 Tyre: 28 MT to 31 MT <span class="p-price"><i class="fa fa-rupee"></i> 269.00</span></p>
                                    </li>
                                    <li>
                                        <p><i class="fa fa-circle"></i> 14 Tyre: 31 MT to 41 MT <span class="p-price"><i class="fa fa-rupee"></i> 379.00</span></p>
                                    </li>
                                    <li>
                                        <p><i class="fa fa-circle"></i> 22 Tyre: 45 MT to 55 MT <span class="p-price"><i class="fa fa-rupee"></i> 489.00</span></p>
                                    </li> -->
                                <!--</ul>
                            </div>-->
                                <div class="view-km-block">
                                <span class="vkm-text-block"><i class="fa fa-map-marker"></i> {{ number_format($value["distance"],2) }} km</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
        @else

        <p class="alert alert-secondary">No data found!</p>
        @endif
    @else

        <p class="alert alert-secondary">No data found!</p>
    @endif