@extends('buyer.layouts.buyer_layout')

@section('content')

<!-- CONTENT START -->
<div class="page-content">
    <!-- BREADCRUMB ROW -->
    <div class="bg-gray-light p-tb20">
        <div class="container">
            <ul class="wt-breadcrumb breadcrumb-style-2">
                <li><a href="{{ route('buyer_home') }}">Home</a></li>
                <li>FAQ</li>
            </ul>
        </div>
    </div>
    <!-- BREADCRUMB ROW END -->
    <!-- ABOUT COMPANY SECTION START -->
    <div class="section-full p-tb50 about-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="qa-block">
                        <div class="faq-block wow fadeInUp" data-wow-delay="0.1s">
                            <h2 class="faq-que">Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.</h2>
                            <div class="faq-ans">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                        </div>
                        <div class="faq-block wow fadeInUp" data-wow-delay="0.3s">
                            <h2 class="faq-que">What is Lorem Ipsum?</h2>
                            <div class="faq-ans">
                                Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.
                            </div>
                        </div>
                        <div class="faq-block wow fadeInUp" data-wow-delay="0.5s">
                            <h2 class="faq-que">Lorem ipsum began as scrambled, nonsensical Latin derived from Cicero's 1st-century BC text De Finibus Bonorum et Malorum.</h2>
                            <div class="faq-ans">
                                Until recently, the prevailing view assumed lorem ipsum was born as a nonsense text. “It's not Latin, though it looks like it, and it actually says nothing,” Before & After magazine answered a curious reader, “Its ‘words’ loosely approximate the frequency with which letters occur in English, which is why at a glance it looks pretty real.”
                            </div>
                        </div>
                        <div class="faq-block wow fadeInUp" data-wow-delay="0.7s">
                            <h2 class="faq-que">Creation timelines for the standard lorem ipsum passage vary, with some citing the 15th century and others the 20th.</h2>
                            <div class="faq-ans">
                                So how did the classical Latin become so incoherent? According to McClintock, a 15th century typesetter likely scrambled part of Cicero's De Finibus in order to provide placeholder text to mockup various fonts for a type specimen book. 
                            </div>
                        </div>
                        <div class="faq-block wow fadeInUp" data-wow-delay="0.9s">
                            <h2 class="faq-que">Lorem ipsum was purposefully designed to have no meaning, but appear like real text, making it the perfect placeholder.</h2>
                            <div class="faq-ans">
                                Don't bother typing “lorem ipsum” into Google translate. If you already tried, you may have gotten anything from "NATO" to "China", depending on how you capitalized the letters. The bizarre translation was fodder for conspiracy theories, but Google has since updated its “lorem ipsum” translation to, boringly enough, “lorem ipsum”. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
    <!-- ABOUT COMPANY SECTION END -->
    
    <!-- REQUEST A QUOTE SECTION START -->
    <!-- <div class="request_A_quote" style="background-image: url(./images/call-back-bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 wow fadeInLeft" data-wow-delay="0.3s">
                    <p>Have any ideas in your mind?</p>
                    <h4>CREATE YOUR NEXT PROJECT WITH US</h4>
                </div>
                <div class="col-md-3 col-sm-4 wow fadeInRight" data-wow-delay="0.3s">
                    <div class="button_request">
                        <a href="javascript:;" data-toggle="modal" data-target="#request-quote-form" class="site-button black"><span>REQUEST A QUOTE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- REQUEST A QUOTE SECTION END -->
</div>
<!-- CONTENT END -->

@endsection