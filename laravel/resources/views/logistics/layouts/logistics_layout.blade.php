@include('logistics.common.master')

<body class="nw-theme">
    <div class="wrapper">
        @include('logistics.common.sidebar')

        <div id="content">
            @include('logistics.common.header')
            <div class="bg_dark"></div>
                @include('flash-message')
            <!-- <div class="middle-container-wrap"> -->
                @yield('content')
            <!-- </div> -->

            <footer class="w-100 bg-gray py-3 px-4 position-absolute">
                <div class="customer-sidebar-support-left-block">
                    <h3>Customer Care : <span>+91 9979016486</span> &nbsp;|&nbsp; Write Us On : <span>support@conmix.in</span></h3>
                </div>
                <aside class="left float-lg-left text-center">
                    <p class="m-0">© Copyright {{date("Y")}}. All Rights Reserved. Conmix Pvt. Ltd.</p>
                </aside>
            </footer>

        </div>
        <!-- CONTENT BLOCK END  -->
        @include('logistics.common.right_sidebar')
    </div>

    @include('logistics.common.popups')


@include('logistics.common.footer')
@include('logistics.common.chart')