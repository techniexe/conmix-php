@extends('logistics.layouts.logistics_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Payment History</h1>
    <div class="clearfix"></div>
    <!--Payment History start -->
    <div class="payment-history-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="download-search-block">
                    <div class="download-btn-block">
                        <a href="#" class="site-button outline gray"><i class="fa fa-file-pdf-o"></i> PDF</a>
                        <a href="#" class="site-button outline gray"><i class="fa fa-file-excel-o"></i> Excel</a>
                    </div>
                    <div class="report-search-block" >
                        <input type="text" name="search" placeholder="Search" style="height: 36px;">
                        <button style="height: 36px;""><i class="fa fa-search"></i></button>
                    </div>
                    <button id="filterCollapse" class="site-button outline gray" ><i class="fa fa-filter"></i></button>
                </div>

                <div class="comn-table1 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Transaction Id</th>
                                <th>Transaction Date</th>
                                <th>By Bank Name</th>
                                <th>Base Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>GST Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>YES10362316</td>
                                <td>04 Aug 2020, 11:40:23 am</td>
                                <td>SBI</td>
                                <td>1,50,000</td>
                                <td>2,000</td>
                                <td>1,52,000</td>
                                <td>
                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                            <li>
                                                <h3>Payment Failed :</h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                                <tr>
                                <td>YES10362316</td>
                                <td>04 Aug 2020, 11:40:23 am</td>
                                <td>SBI</td>
                                <td>1,50,000</td>
                                <td>2,000</td>
                                <td>1,52,000</td>
                                <td>
                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                            <li>
                                                <h3>Payment Failed :</h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                                <tr>
                                <td>YES10362316</td>
                                <td>04 Aug 2020, 11:40:23 am</td>
                                <td>SBI</td>
                                <td>1,50,000</td>
                                <td>2,000</td>
                                <td>1,52,000</td>
                                <td>
                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                            <li>
                                                <h3>Payment Failed :</h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                          
                           
                          
                        </tbody>
                    </table>
                </div>

                <div class="pagination-block m-t20">
                    <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p>
                </div>

            </div>
        </div>
    </div>
    <!--Payment History end -->
</div>

@endsection