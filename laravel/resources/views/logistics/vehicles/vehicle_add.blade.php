@extends('logistics.layouts.logistics_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Vehicle</h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="vehicle-block vehicle-category-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="data-box-header m-b20">
                    <h2 class="box-title">Add Vehicle</h2>
                </div>
                <div class="vehicle-list-block">
                    @if(isset($data["error"]))
                        <div class="error">{{ $data["error"]["message"] }}</div>
                    @endif
                    <div class="vehicle-block vehicle-category-block">
                        <form action="" method="post" name="add_vehicles_form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group" id="vehicleCategoryId_div">
                                            <label>Vehicle Category <span class="str">*</span></label>
                                            <div class="cstm-select-box">
                                                <select name="vehicleCategoryId" id="vehicleCategoryId">
                                                    <option disabled="disabled" selected="selected">Select Vehicle Category</option>
                                                    @if(isset($data["vehicle_categoies"]))
                                                        @foreach($data["vehicle_categoies"] as $cat_value)
                                                        <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == Request::get("vehicle_category_id") ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                                                        @endforeach
                                                    @endif

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" id="vehicleSubCategoryId_div">
                                            <label>Vehicle Sub Category <span class="str">*</span></label>
                                            <div class="cstm-select-box">
                                                <select name="vehicleSubCategoryId" id="vehicleSubCategoryId">
                                                    <option disabled="disabled" selected="selected">Select Vehicle Sub Category</option>
                                                    @if(isset($data["subcategories"]))
                                                        @foreach($data["subcategories"] as $cat_value)
                                                            <option value="{{ $cat_value['_id'] }}" data-min-load="{{ $cat_value['min_load_capacity'] }}" data-max-load="{{ $cat_value['max_load_capacity'] }}"  {{ $cat_value['_id'] == Request::get("vehicle_sub_category_id") ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                       <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Min Load Capacity (MT)</label>
                                                  <input disabled type="text" id="vehicle_min_load" class="form-control" placeholder="e.g. 10">
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Max Load Capacity (MT) </label>
                                                  <input disabled type="text" id="vehicle_max_load" class="form-control" placeholder="e.g. 30">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Vehicle Registration No <span class="str">*</span></label>
                                            <div class="row">
                                                <div class="col-md-2 pd-r-5">
                                                    <input type="text" name="vehicle_rc_number_1" class="form-control" placeholder="GJ">
                                                </div>
                                                <div class="col-md-2 pd-lr-5">
                                                    <input type="text" name="vehicle_rc_number_2" class="form-control" placeholder="01">
                                                </div>
                                                <div class="col-md-2 pd-lr-5">
                                                    <input type="text" name="vehicle_rc_number_3" class="form-control" placeholder="AB">
                                                </div>
                                                <div class="col-md-6 pd-l-5">
                                                    <input type="text" name="vehicle_rc_number_4" class="form-control" placeholder="1234">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Per MT/KM Rate <span class="notinfo" data-tooltip="&#8377; 1.00 to 9.99 Allowed"><i class="fa fa-info-circle"></i></span></label>
                                                    <div class="input-group-prepend m--l1"><span class="input-group-text"><i class="fa fa-rupee"></i></span></div>
                                                    <input type="text" name="per_metric_ton_per_km_rate" id="per_metric_ton_per_km_rate"  class="form-control" placeholder="e.g 9.99"/>
                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Min Trip Price <span class="str">*</span> <span class="notinfo" id="min_trip_price_tooltip" data-tooltip=""><i class="fa fa-info-circle"></i></label>
                                                    <div class="input-group-prepend m--l1"><span class="input-group-text"><i class="fa fa-rupee"></i></span></div>
                                                    <input type="text" name="min_trip_price" class="form-control" placeholder="e.g. 500">
                                                    <input type="hidden" id="server_min_trip_price" class="form-control" placeholder="e.g. 500">

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Vehicle Make <span class="str">*</span></label>
                                                    <input type="text" name="manufacturer_name" class="form-control" placeholder="e.g. TATA Hyva">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Vehicle Type Modal <span class="str">*</span></label>
                                                    <input type="text" name="vehicle_model" class="form-control" placeholder="e.g. 2523">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Vehicle Manufacture Year <span class="str">*</span></label>
                                                    <input type="text" name="manufacture_year" class="form-control" placeholder="e.g. 2019">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group" id="delivery_range_div">
                                                    <label>Delivery Range <span class="str">*</span></label>
                                                    <input type="text" name="delivery_range" class="form-control brd-rds-rit" placeholder="e.g. 100">
                                                    <div class="input-group-prepend m--l1"><span class="input-group-text">KM</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" id="rc_book_image_div">
                                            <label>Upload RC Book Image <span class="str">*</span></label>
                                            <div class="file-browse">
                                                <span class="button-browse"> Browse <input type="file" name="rc_book_image"></span>
                                                <input type="text" id="rc_book_image_text"  class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" id="insurance_image_div">
                                            <label>Upload Insurance Image <span class="str">*</span></label>
                                            <div class="file-browse">
                                                <span class="button-browse"> Browse <input type="file" name="insurance_image"></span>
                                                <input type="text" id="insurance_image_text"  class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" id="vehicle_image_div">
                                            <label>Upload Vehicle Image <span class="str">*</span></label>
                                            <div class="file-browse">
                                                <span class="button-browse"> Browse <input type="file" name="vehicle_image"></span>
                                                <input type="text" id="vehicle_image_text"  class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <div class="input-group">
                                            <input type="checkbox" name="is_registered_address" id="is_use_registered_address" style="margin-top: 3px;margin-right: 6px;" /> Use Same Registered Address
                                            <input type="hidden" id="user_profile_data" value="{{ json_encode(session()->get('profile_details',null)) }}"/>
                                        </div>
                                    </div> -->
                                    
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group" id="driver1_id_div">
                                                    <label>Driver 1 <span class="str">*</span></label>
                                                    <div class="cstm-select-box">
                                                        <select name="driver1_id">
                                                            <option disabled="disabled" selected="selected">Select Driver</option>
                                                            @if(isset($data["driver_data"]))
                                                                @foreach($data["driver_data"] as $value)
                                                                    <option value="{{ $value['_id'] }}">{{ $value["driver_name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group" id="driver2_id_div">
                                                    <label>Driver 2 <span class="str">*</span></label>
                                                    <div class="cstm-select-box">
                                                        <select name="driver2_id">
                                                            <option disabled="disabled" selected="selected">Select Driver</option>
                                                            @if(isset($data["driver_data"]))
                                                                @foreach($data["driver_data"] as $value)
                                                                    <option value="{{ $value['_id'] }}">{{ $value["driver_name"] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Latitude</label>
                                                    <input type="text" required name="us2_lat" id="us2_lat" class="form-control" readonly/>
                                                    <p class="error" id="us2_lat_error"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Longitude</label>
                                                    <input type="text" required name="us2_lon" id="us2_lon" class="form-control" readonly/>
                                                    <p class="error" id="us2_lon_error"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mapnote">Please Set Accurate Pickup Address</div>
                                    <div class="map-content-block add-vehicle-map-block m-t20" style="height: 410px; margin-top: 0px;">
                                        <div id="us2" style="width: 100%; height: 400px;"></div>
                                    </div>

                                    

                                    <div class="row">

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Address Line 1 <span class="str">*</span></label>
                                                    <!-- <input type="text" name="line1" id="vehicle_address_line_1" class="form-control" placeholder="e.g. Address line 1"> -->
                                                    <input type="text" name="line1" id="address_line1" class="form-control" placeholder="e.g. Address line 1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Address Line 2 <span class="str">*</span></label>
                                                    <!-- <input type="text" name="line2" id="vehicle_address_line_2" class="form-control" placeholder="e.g. Address line 2"> -->
                                                    <input type="text" name="line2" id="address_line2" class="form-control" placeholder="e.g. Address line 2">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group single-search-select2" id="state_name_city_div">
                                                    <label>State Name <span class="str">*</span></label>
                                                    <!-- <select id="state_name_city_address" name="state_id" class="form-control-chosen" data-placeholder="Select State Name">
                                                        <option></option>
                                                        @if(isset($data["state_data"]))
                                                            @foreach($data["state_data"] as $value)
                                                                <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select> -->

                                                    <select id="add_site_state" readOnly name="state_id" class="form-control" data-placeholder="Select State Name">
                                                            <option disabled="disabled" selected="selected">State</option>
                                                            
                                                        </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group single-search-select2" id="state_name_city_filter_div">
                                                    <label>Select City Name <span class="str">*</span></label>
                                                    <!-- <select id="state_name_city_filter" name="city_id" class="form-control-chosen" data-placeholder="Select City Name">
                                                        <option></option>
                                                        @if(isset($data["city_data"]))
                                                            @foreach($data["city_data"] as $value)
                                                                <option value="{{ $value['_id'] }}">{{ $value["city_name"] }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select> -->

                                                    <select id="add_site_city" readOnly name="city_id" class="form-control" data-placeholder="Select City Name">
                                                        <option disabled="disabled" selected="selected">City</option>
                                                        
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Pincode <span class="str">*</span></label>
                                                    <!-- <input type="text" name="pincode" id="vehicle_pincode" class="form-control" placeholder="e.g. 123456"> -->
                                                    <input type="text" name="pincode" id="pincode" class="form-control" placeholder="e.g. 123456">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mp_note_block"><strong>Note:</strong> Choose permenant location of your vehicle.</div>
                                            </div> -->
                                            <!-- <div class="col-md-6">
                                                <div class="location_field">
                                                    <input type="text" class="form-control" placeholder="Enter city name">
                                                    <button type="button" class="site-button umcl-btn">Use my current location</button>
                                                </div>
                                            </div> -->
                                        <!-- </div>
                                          <div class="form-group">
                                                <div class="input-group">

                                                    <div class="location_field">
                                                        <input type="text" class="form-control" placeholder="Enter city name">
                                                        <button type="button" class="site-button umcl-btn">Use my current location</button>
                                                    </div>
                                                   <input type="text" placeholder="Enter Your Location" required name="us2_address" id="us2_address" class="form-control" onkeydown="return event.key != 'Enter';"/ style=" margin-top: 15px;">
                                                </div>
                                            </div>
                                        <div class="map-content-block add-vehicle-map-block m-t20" style="height: 410px; margin-top: 0px;">
                                            <div id="us2" style="width: 100%; height: 400px;"></div>
                                        </div>
                                    </div>
                                </div>-->



                                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group m-b0">
                                        <div class="input-group avlbl-sswich m-t15">
                                            <label class="wid-120">Available:</label>
                                            <div class="verified-switch-btn cstm-css-checkbox">
                                                <label class="new-switch1 switch-green">
                                                    <input type="checkbox" class="switch-input" checked="">
                                                    <span class="switch-label" data-on="Yes" data-off="No"></span>
                                                    <span class="switch-handle"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group m-b0">
                                        <div class="input-group avlbl-sswich m-t15">
                                            <label>Is Insurance Active :</label>
                                            <div class="verified-switch-btn cstm-css-checkbox">
                                                <label class="new-switch1 switch-green">
                                                    <input type="checkbox" name="is_insurance_active" class="switch-input" checked="">
                                                    <span class="switch-label" data-on="Yes" data-off="No"></span>
                                                    <span class="switch-handle"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group m-b0">
                                        <div class="input-group avlbl-sswich m-t15">
                                            <label>Is GPS Active    :</label>
                                            <div class="verified-switch-btn cstm-css-checkbox">
                                                <label class="new-switch1 switch-green">
                                                    <input type="checkbox" name="is_gps_enabled" class="switch-input" checked="">
                                                    <span class="switch-label" data-on="Yes" data-off="No"></span>
                                                    <span class="switch-handle"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="add_vehicle_form_type" id="add_vehicle_form_type" value="add"/>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group m-t20">
                                        <div class="input-group">
                                            <button type="submit" class="site-button m-r10">Save</button>
                                            <a href="{{ route('logistic_vehicles') }}" class="site-button gray">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

<script>

$('#us2').locationpicker({
    enableAutocomplete: true,
        enableReverseGeocode: true,
    radius: 0,
    inputBinding: {
        latitudeInput: $('#us2_lat'),
        longitudeInput: $('#us2_lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2_address')
    },
    onchanged: function (currentLocation, radius, isMarkerDropped) {

            // var addressComponents = $(this).locationpicker('map').location.addressComponents;
        console.log(currentLocation);  //latlon
        // updateControls(addressComponents); //Data
        }
    });

    function updateControls(addressComponents) {
        console.log(addressComponents);
    }

</script>


@endsection