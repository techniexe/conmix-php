@extends('logistics.layouts.logistics_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block register_addres_stap">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Logistics</h1>
                <div id="signupregister" style="display: block;">
                @if(isset($data["error"]))
                    <div class="error alert alert-danger">{{ $data["error"]["message"] }}</div>
                @endif
                        <h2>Register</h2>
                    <form action="{{route('logistics_register')}}" name="logistics_register_form" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="signup_type" value="{{ $data['data']['signup_type'] }}" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group ">
                                        <label>Company Type <span class="str">*</span></label>
                                        <div class="cstm-select-box">
                                            <select class="form-control" name="company_type">
                                                <option disabled="disabled" selected="selected">Select Type</option>
                                                <option value="Partnership">Partnership</option>
                                                <option value="Proprietor">Proprietor</option>
                                                <option value="LLP">LLP.</option>
                                                <option value="LTD">LTD.</option>
                                                <option value="PVT. LTD.">PVT.LTD.</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Person Name <span class="str">*</span></label>
                                        <input class="form-control" name="full_name" placeholder="e.g. Johndoe" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Company Name <span class="str">*</span></label>
                                        <input class="form-control" name="company_name" placeholder="e.g. Ambika Logistics" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Mobile No. <span class="str">*</span></label>
                                        <input id="" class="form-control" name="mobile_number" type="text" placeholder="e.g. 9898989898" readonly value="{{ $data['data']['email_mobile'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Landline No. </label>
                                        <input type="text" class="form-control" name="landline_number" placeholder="e.g. 0791234567">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Email <span class="str">*</span></label>
                                        <input type="email" class="form-control" name="email" placeholder="e.g. johndoe@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Company Registration No <span class="str">*</span></label>
                                        <input class="form-control" name="company_certification_number" placeholder="e.g. CMP123456789" type="text">
                                    </div>
                                </div>
                            </div>
                                 <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group" id="company_certification_image_div">
                                        <label>Company Register Certification <span class="str">*</span></label>
                                        <div class="file-browse">
                                            <span class="button-browse" >
                                                Browse <input type="file" name="company_certification_image" />
                                            </span>
                                            <input type="text" id="company_certification_image_text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Pan No <span class="str">*</span></label>
                                        <input class="form-control" name="pan_number" placeholder="e.g. AAABB0000C" type="text">
                                    </div>
                                </div>
                            </div>
                               <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group" id="pancard_image_div">
                                        <label>Upload Pancard <span class="str">*</span></label>
                                        <div class="file-browse">
                                            <span class="button-browse">
                                                Browse <input type="file" name="pancard_image" />
                                            </span>
                                            <input type="text" id="pancard_image_text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>GST No <span class="str">*</span> </label>
                                        <input class="form-control" name="gst_number" placeholder="e.g. 00AABBC0000A1BB" type="text">
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group" id="gst_certification_image_div">
                                                <label>Upload GST Certificate <span class="str">*</span></label>
                                                <div class="file-browse">
                                                    <span class="button-browse">
                                                        Browse <input type="file" name="gst_certification_image"/>
                                                    </span>
                                                    <input type="text" id="gst_certification_image_text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Password <span class="str">*</span></label>
                                        <input class="form-control" name="password" placeholder="e.g. Enter Password" type="password">
                                    </div>
                                </div>
                            </div>
                          <!--   <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Fax No </label>
                                        <input class="form-control" name="fax_number" placeholder="e.g. cmp123456789" type="text">
                                    </div>
                                </div>
                            </div> -->

                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Company Certification Document <span class="str">*</span></label>
                                        <div class="file-browse">
                                            <span class="button-browse">
                                                Browse <input type="file" name="company_certification_image" />
                                            </span>
                                            <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                         <!--    <div class="col-md-12">
                                <div class="form-group m-t10">
                                    <div class="input-group">
                                        <span class="display-inline-block text-gray-dark m-r40">
                                            Account type
                                        </span>
                                        <div class="display-inline-block m-r20 cstm-rdio-btn">
                                            <input type="radio" name="account_type" value="personal" id="personal" class="css-checkbox" checked="checked" /><label for="personal" class="css-label radGroup1 radGroup2">Personal</label>
                                        </div>
                                        <div class="display-inline-block cstm-rdio-btn">
                                            <input type="radio" name="account_type" value="company" id="company" class="css-checkbox" /><label for="company" class="css-label radGroup1 radGroup2">Company</label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-md-12">
                                <div class="form-group m-t20">
                                    <div class="input-group text-left">
                                        <button type="submit" name="register_btn" value="register_btn" class="site-button">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection