@extends('logistics.layouts.logistics_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block register_addres_stap">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Logistics</h1>
                <div id="signupregister" style="display: block;">
                    @if(isset($data["error"]))
                        <div class="error alert alert-danger">{{ $data["error"]["message"] }}</div>
                    @endif
                    <h2>Add Your Office Address</h2>
                    <form action="{{route('logistics_register_2_submit')}}" name="logistics_address_register_form" method="post">
                        @csrf
                        <input type="hidden" name="signup_type" value="" />
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group single-search-select2" id="state_name_city_div">
                                        <label>State Name <span class="str">*</span></label>
                                        <select id="state_name_city" name="state_id" class="form-control-chosen" data-placeholder="Select State Name">
                                            <option></option>
                                            @if(isset($data["state_data"]))
                                                @foreach($data["state_data"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group single-search-select2" id="state_name_city_filter_div">
                                        <label>City Name <span class="str">*</span></label>
                                        <select id="state_name_city_filter" name="city_id" class="form-control-chosen" data-placeholder="Select City Name">
                                            <option></option>
                                            @if(isset($data["city_data"]))
                                                @foreach($data["city_data"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["city_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Address Line 1 <span class="str">*</span></label>
                                        <input class="form-control" name="address_line_1" placeholder="e.g. Address Line 1" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Address Line 2 <span class="str">*</span></label>
                                        <input class="form-control" name="address_line_2" placeholder="e.g. Address Line 2" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>Pincode <span class="str">*</span></label>
                                        <input type="text" name="pincode" class="form-control" placeholder="e.g. 123456">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="mp_note_block"><strong>Note:</strong> Choose permenant location of your vehicle.</div>
                                            <input type="text" placeholder="Enter Your Location" required="" name="us2_address" id="us2_address" class="form-control pac-target-input" onkeydown="return event.key != 'Enter';" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Latitude</label>
                                                <input type="text" required="" name="us2_lat" id="us2_lat" class="form-control" readonly="">
                                                <p class="error" id="us2_lat_error"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <label>Longitude</label>
                                                <input type="text" required="" name="us2_lon" id="us2_lon" class="form-control" readonly="">
                                                <p class="error" id="us2_lon_error"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="mapnote">Please Set Accurate Pickup Address</div>
                                        <div class="map-content-block add-vehicle-map-block m-t20" style="height: 300px; margin-top: 0px;">
                                            <div id="us2" style="width: 100%; height: 290px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-t20">
                            <div class="input-group text-left">
                                <button type="submit" name="register_btn" value="register_btn" class="site-button">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

$('#us2').locationpicker({
    enableAutocomplete: true,
    enableReverseGeocode: true,
    radius: 0,
    inputBinding: {
        latitudeInput: $('#us2_lat'),
        longitudeInput: $('#us2_lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2_address')
    },
    onchanged: function (currentLocation, radius, isMarkerDropped) {

            // var addressComponents = $(this).locationpicker('map').location.addressComponents;
        console.log(currentLocation);  //latlon
        // updateControls(addressComponents); //Data
        }
    });

    function updateControls(addressComponents) {
        console.log(addressComponents);
    }

</script>


@endsection