@extends('logistics.layouts.logistics_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Logistics</h1>
                <div id="login" style="display: block;">
                @if(isset($data["error"]))
                    <div class="error alert alert-danger">{{ $data["error"]["message"] }}</div>
                @endif
                    <form action="{{route('logistics_login')}}" name="logistics_login_form" method="post" onsubmit="return validateForm()">
                    @csrf
                        <div class="form-group">
                            <div class="input-group">
                                <label>Mobile No. / Email Id <span class="str">*</span></label>
                                <input class="form-control" name="email_mobile" id="mobile_or_email" value="" placeholder="e.g. 9898989898 / johndoe@example.com" type="text">
                                <!-- <p id="email_mobile_error"></p> -->
                                <div class="invalid-feedback" id="email_mobile_error"></div>
                                @error('email_mobile')
                                    <div class="">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label>Password <span class="str">*</span></label>
                                <input class="form-control" id="pass" name="password" placeholder="e.g. Enter Password" type="password">
                                <div class="invalid-feedback" id="password_error"></div>
                                @error('password')
                                    <div class="">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group pull-left w-100">
                            <div class="input-group display-block">
                                <a href="{{ route('logistics_forgot_pass_email_verifier') }}" class="pull-right">Forgot Password</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group text-left">
                                <div class="lsf-btn-block">
                                    <button type="submit" name="login_btn" value="login_btn" class="site-button m-b10">Login</button>
                                    <p class="m-b10 text-uppercase text-gray-dark fw-medium m-b0">Or</p>
                                    <button type="button" class="site-button google m-b0 m-r20"><i class="fa fa-google-plus"></i> Google</button>
                                    <button type="button" class="site-button linkedin m-b0"><i class="fa fa-facebook"></i> Facebook</button>
                                </div>
                            </div>
                        </div>
        <span class="text-gray-dark">Don't have an account? <a href="{{ route('logistic_signup_email_verifier') }}" class="text-primary">Sign Up</a></span>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function validateForm() {
    
    $(".error").html("");
    $(".error").removeClass("alert-danger");
    
    $("#email_mobile_error").html("");
    $("#password_error").html("");
    
    var email_mobile = document.forms["logistics_login_form"]["email_mobile"].value.trim();
    var pass = document.forms["logistics_login_form"]["password"].value.trim();
    
    var is_valid = validateLogin(email_mobile,pass);

    return is_valid;

  
//   var email_mobile = document.forms["logistics_login_form"]["email_mobile"].value;
  
//   if(isNaN(email_mobile)){
//     //validate email address
//     var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//     if(!email_mobile.match(mailformat)){
//         $("#email_mobile_error").html("Please enter valid email address");
//         return false;
//     }
//   }else{
//     //validate Mobile number
//     email_mobile = email_mobile.trim().replace("+","");
//     var phoneno = /^\d{12}$/;
//     if (!email_mobile.match(phoneno)) {
//         // alert("Phone number should be 10 digits");
//         $("#email_mobile_error").html("Phone number should be 13 digits. including + sign");
//         return false;
//     }


//   }
  
} 

</script>

@endsection