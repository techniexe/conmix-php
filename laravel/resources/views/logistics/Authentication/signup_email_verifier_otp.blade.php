@extends('logistics.layouts.logistics_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Logistics</h1>
                <div id="signupemailverifyotp" style="display: block;">
                @if(isset($data["error"]))
                    <div class="error alert alert-danger">{{ $data["error"]["message"] }}</div>
                @endif
                    
                        <div class="form-group">
                            <div class="input-group">
                                <h3>Enter Confirmation Code</h3>
                                <span class="text-gray-dark">
                                @if(isset($data["data"]))
                                    <div class="">{{ isset($data["data"]["message"]) ? $data["data"]["message"] : '' }}</div>
                                @endif <a href="{{ route('logistic_signup_email_verifier') }}" class="text-primary">Change Mobile No.</a></span>
                            </div>
                        </div>
                        <form action="{{route('logistic_signup_otp_verify')}}" name="logistics_otp_verify_form" method="post" name="login_form" onsubmit="return validateForm()">
                            @csrf
                            <div class="form-group">
                                <div class="input-group">
                                    <label>OTP <span class="str">*</span></label>
                                    <input class="form-control" id="otp_code" name="otp_code" placeholder="Enter OTP" type="text">
                                    <div class="invalid-feedback" id="otp_error"></div>
                                    <input class="form-control" name="email_mobile" value="{{ $data['data']['email_mobile'] }}" placeholder="Enter OTP" type="hidden">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group text-left">
                                    <button type="submit" name="otp_btn" value="otp_btn" class="site-button">Continue</a>
                                </div>
                            </div>
                        </form>

                        <div class="text-gray-dark resend_otp_div">Don't Get OTP?
                            <button type="submit" name="otp_send_btn" value="otp_send_btn" class="text-primary">Resend</button>
                        </div>

                        <div class="form-group pull-right w-100">
                            <div class="input-group display-block">

                                <form action="{{route('logistic_signup_otp_send')}}"  name="logistics_email_verify_form" method="post">
                                    @csrf
                                    
                                    <input class="form-control" name="email_mobile" value="{{ $data['data']['email_mobile'] }}" required placeholder="e.g. 9898989898 / johndoe@example.com" type="hidden">
                                        
                                    <button type="submit" name="register_btn" value="register_btn" class="pull-right">Resend</button>
                                </form>
                            </div>
                        </div>                        
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function validateForm() {
    
    $(".error").html("");
    $("#otp_error").html("");
    
    $(".error").removeClass("alert-danger");

  var otp = document.forms["logistics_otp_verify_form"]["otp_code"].value.trim();
  
  var is_valid = validateOTP(otp);

    return is_valid;


  
} 

</script>

@endsection