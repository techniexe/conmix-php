@extends('logistics.layouts.logistics_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Dashboard</h1>
    <div class="clearfix"></div>
    <!--Order Trip-->
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
                <div class="inner">
                    <h3><i class="fa fa-rupee"></i> {{ isset($data["data"]["totalLogisticsOrderAmount"][0]["total"]) ? round($data["data"]["totalLogisticsOrderAmount"][0]["total"]) : '   0' }}</h3>
                    <p>Logistics Total Sales</p>
                </div>
                <div class="icon bg-yellow">
                    <i class="fa fa-calculator"></i>
                </div>
            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
                <div class="inner">
                    <h3><i class="fa fa-rupee"></i> {{ isset($data["data"]["todayLogisticsOrderAmount"][0]["total"]) ? round($data["data"]["todayLogisticsOrderAmount"][0]["total"]) : '   0' }}</h3>
                    <p>Logistics Today Sales</p>
                </div>
                <div class="icon bg-dblue">
                    <i class="fa fa-calculator"></i>
                </div>
            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
                <div class="inner">
                    <h3><i class="fa fa-rupee"></i> {{ isset($data["data"]["thisWeekLogisticsOrderAmount"][0]["total"]) ? round($data["data"]["thisWeekLogisticsOrderAmount"][0]["total"]) : '   0' }}</h3>
                    <p>Logistics This Week Sales</p>
                </div>
                <div class="icon bg-mpinch">
                    <i class="fa fa-calculator"></i>
                </div>

            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
                <div class="inner">
                    <h3><i class="fa fa-rupee"></i> {{ isset($data["data"]["thisMonthLogisticsOrderAmount"][0]["total"]) ? round($data["data"]["thisMonthLogisticsOrderAmount"][0]["total"]) : '   0' }}</h3>
                    <p>Logistics This Month Sales</p>
                </div>
                <div class="icon bg-dgray">
                   <i class="fa fa-calculator"></i>
               </div>
           </div>
       </div>
   </div>
   <!--Order Trip End-->
   <!--Order Trip-->
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
                <div class="inner">
                    <h3>{{ isset($data["data"]["totalLogisticsOrder"]) ? $data["data"]["totalLogisticsOrder"] : '   0' }}</h3>
                    <p>Logistics Total Order</p>
                </div>
               <div class="icon bg-yellow">
                        <i class="fa fa-truck"></i>
                        </div>
            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
                <div class="inner">
                    <h3>{{ isset($data["data"]["todayLogisticsOrder"]) ? $data["data"]["todayLogisticsOrder"] : '   0' }}</h3>
                    <p>Logistics Today Order</p>
                </div>
                <div class="icon bg-dblue">
                        <i class="fa fa-truck"></i>
                        </div>
            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
                <div class="inner">
                    <h3>{{ isset($data["data"]["logisticsOrderOfThisWeek"]) ? $data["data"]["logisticsOrderOfThisWeek"] : '   0' }}</h3>
                    <p>Logistics This Week Order</p>
                </div>
                <div class="icon bg-mpinch">
                        <i class="fa fa-truck"></i>
                        </div>
                
            </div>
        </div>
        
        <div class="col-lg-3 col-sm-6">
            <div class="small-box bg-white">
                <div class="inner">
                    <h3>{{ isset($data["data"]["logisticsOrderOfThisMonth"]) ? $data["data"]["logisticsOrderOfThisMonth"] : '   0' }}</h3>
                    <p>Logistics This Month Orders</p>
                </div>
                 <div class="icon bg-dgray">
                        <i class="fa fa-truck"></i>
                </div>
               
           </div>
       </div>
   </div>
   <!--Order Trip End-->
   <div class="clearfix"></div>

   <!--Send Quote start-->
   <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="data-box">
            <div class="data-box-header with-border">
                <h2 class="box-title">Send Quote</h2>
            </div>
            <div class="data-box-body">
                <div class="comn-table latest-order-table send-quote-table table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-nowrap">Quote Id</th>
                                <th class="text-nowrap">Product</th>
                                <!-- <th class="text-nowrap">Sub Category</th> -->
                                <th class="text-nowrap">Quantity</th>
                                <th class="text-nowrap">Pickup Location</th>
                                <th class="text-nowrap">Delivery Location</th>
                                <th class="text-nowrap">Distance</th>
                                <th class="text-nowrap">Order Date</th>
                                <th class="text-nowrap">Max Bid Price ( <i class="fa fa-rupee"></i> )</th>
                                <th class="text-nowrap">My Bidding Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th class="text-nowrap">Status</th>
                                <th class="text-nowrap">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php //dd($data); ?>
                        @if(isset($data['quotes']) && count($data['quotes']) > 0)
                            
                            @foreach($data["quotes"] as $value)  
                            <tr>
                                    <td>#{{ isset($value["quote_display_id"]) ? $value["quote_display_id"] : '' }}</td>
                                    <td>{{ isset($value["productCategory"]["category_name"]) ? $value["productCategory"]["category_name"] : '' }} - {{ isset($value["productSubcategory"]["sub_category_name"]) ? $value["productSubcategory"]["sub_category_name"] : '' }}</td>
                                    <!-- <td>Sub Category</td> -->
                                    <td class="text-nowrap">{{ isset($value["quantity"]) ? $value["quantity"] : '' }} MT</td>
                                    <td>
                                        <div class="cstm-tooltip" data-direction="bottom">
                                            @if(isset($value["pickup_address"]["line1"]))
                                            <div class="pickup-address pickup_address_new  cstm-tooltip__initiator">
                                                <!-- {{ $value["pickup_address"]["line1"] }} -->
                                                {{ isset($value["pickup_address"]['line1']) ? $value["pickup_address"]['line1'] : '' }},
                                                {{ isset($value["pickup_address"]['line2']) ? $value["pickup_address"]['line2'] : '' }},

                                                {{ isset($value["pickup_address_city"]['city_name']) ? $value["pickup_address_city"]['city_name'] : '' }},
                                                {{ isset($value["pickup_address_state"]['state_name']) ? $value["pickup_address_state"]['state_name'] : '' }}
                                            </div>
                                            <!-- <div class="cstm-tooltip__item"> -->
                                                <!-- Techniexe Infolabs LLP 104, Sumel II, Near GuruDwara, SG Highway Ahmedabad 380054, (INDIA) -->
                                                <!-- {{ isset($value["pickup_address"]['line1']) ? $value["pickup_address"]['line1'] : '' }},
                                                {{ isset($value["pickup_address"]['line2']) ? $value["pickup_address"]['line2'] : '' }},

                                                {{ isset($value["pickup_address_city"]['city_name']) ? $value["pickup_address_city"]['city_name'] : '' }},
                                                {{ isset($value["pickup_address_state"]['state_name']) ? $value["pickup_address_state"]['state_name'] : '' }}
                                            </div> -->
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                      <div class="cstm-tooltip" data-direction="bottom">
                                        @if(isset($value["delivery_address"]["address_line1"]))
                                        <div class="pickup-address pickup_address_new  cstm-tooltip__initiator">
                                            <!-- {{ $value["delivery_address"]["address_line1"] }} -->
                                            {{ isset($value["delivery_address"]['address_line1']) ? $value["delivery_address"]['address_line1'] : '' }},
                                            {{ isset($value["delivery_address"]['address_line2']) ? $value["delivery_address"]['address_line2'] : '' }},
                                            {{ isset($value["delivery_address_city"]['city_name']) ? $value["delivery_address_city"]['city_name'] : '' }},
                                            {{ isset($value["delivery_address_state"]['state_name']) ? $value["delivery_address_state"]['state_name'] : '' }}
                                        </div>
                                        <!--<div class="cstm-tooltip__item">
                                             Techniexe Infolabs LLP 104, Sumel II, Near GuruDwara, SG Highway Ahmedabad 380054, (INDIA) 
                                            {{ isset($value["delivery_address"]['address_line1']) ? $value["delivery_address"]['address_line1'] : '' }},
                                            {{ isset($value["delivery_address"]['address_line2']) ? $value["delivery_address"]['address_line2'] : '' }},
                                            {{ isset($value["delivery_address_city"]['city_name']) ? $value["delivery_address_city"]['city_name'] : '' }},
                                            {{ isset($value["delivery_address_state"]['state_name']) ? $value["delivery_address_state"]['state_name'] : '' }}
                                        </div>-->
                                        @endif
                                    </div>
                                </td>
                                    <td>{{ isset($value["distance"]) ? round($value["distance"]) : '' }} KM</td>
                                    <td class="text-nowrap">{{ isset($value["created_at"]) ? date('d M Y h:i:s a',strtotime($value["created_at"])) : '' }}</td>
                                    <td>{{ isset($value["max_quote_amount"]) ? round($value["max_quote_amount"]) : '' }}</td>
                                    <td>{{ isset($value["quoted_amount"]) ? round($value["quoted_amount"]) : 'Not Bidded yet' }}</td>
                                    <td><span class="badge bg-red">{{ isset($value["status"]) ? $value["status"] : '' }}</span></td>
                                    <td>
                                    @if(isset($value["quoted_amount"]))
                                        <p>You have bidded on this quote</p>
                                    @else
                                        <div class="dropdown cstm-dropdown-select bid-rate-dropdown">
                                            <a href="javascript:;" class="site-button gray dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Bid Rate</a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                <li>
                                                    <form action="" name="quote_update_form_{{ $value['quote_display_id'] }}" method="post">
                                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        <!-- <div class="form-group">
                                                            <div class="input-group single-search-select2">
                                                                <select id="multiple" class="form-control-chosen" data-placeholder="Select Truck">
                                                                    <option></option>
                                                                    <option>GJ-01-AB-1234 (10 Tyre)</option>
                                                                    <option>GJ-01-CD-4567 (12 Tyre)</option>
                                                                    <option>GJ-01-EF-8910 (14 Tyre)</option>
                                                                    <option>GJ-01-HI-1112 (22 Tyre)</option>
                                                                </select>
                                                            </div>
                                                        </div> -->
                                                        
                                                        <input type="hidden" name="quoteId" value="{{ isset($value['_id']) ? $value['_id'] : '' }}" />
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                    <label>Bidding Amount ( <i class="fa fa-rupee"></i> )</label>
                                                                <input type="text" name="quoted_amount" class="form-control" placeholder="e.g. 500" />
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="site-button blue">Send</button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                    </td>
                                </tr>
                                <script>

                                    $(document).ready(function(){

                                        $("form[name='quote_update_form_{{ $value['quote_display_id'] }}']").validate({
                                            rules: {
                                                quoted_amount: {
                                                    required: true,
                                                    number: true
                                                }
                                            },
                                            messages: {
                                                
                                            },
                                            submitHandler: function(form) {
                                                console.log("submited");
                                            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
                                            var form_values = $("form[name='quote_update_form_{{ $value['quote_display_id'] }}']").serialize();
                                            // var form_values = new FormData(form);
                                            console.log("form_type ..."+form_values );

                                            // $("form[name='logistics_register_form']").submit();
                                            updateQuote(form_values);
                                            

                                            
                                        },
                                        invalidHandler: function(event, validator) {
                                            console.log("error");
                                        }
                                    });

                                    });

                                    

                                </script>
                            @endforeach
                            @else
                                <tr>
                                    <td colspan="11">No Record Found</td>
                                </tr>
                            @endif
                            
                          
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="data-box-footer clearfix">
                <div class="pull-right">
                    <a href="{{ route('logistic_send_quote') }}" class="site-button dark">View All</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Send Quote end-->

<!--Graph block start-->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="data-box">
            <div class="data-box-header with-border data-box-header-dropdown">
                <h2 class="box-title">Monthly Report</h2>
                <div class="cust_datepicker_outer">
                    <div class="cstm-select-box">
                        <select id="monthly_chart_data_status_filter" class="form-control">
                            <option disabled="disabled" selected="selected">Select</option>
                            <option value="PROCESSING" title="PROCESSING">PROCESSING</option>
                            <option value="PROCESSED" title="PROCESSED">PROCESSED</option>                            
                            <option value="SHIPPED" title="SHIPPED">SHIPPED</option>                            
                            <option value="DELIVERED" title="DELIVERED">DELIVERED</option>                            
                            <option value="CANCELLED" title="CANCELLED">CANCELLED</option>                            
                            <option value="FAILED" title="FAILED">FAILED</option>                            
                            <option value="REFUNDED" title="REFUNDED">REFUNDED</option>                            
                        </select>
                    </div>
                    <div class="cstm-select-box m-l10">
                        <select id="monthly_chart_data_year_filter" class="form-control">
                            <option disabled="disabled" selected="selected">Select Year</option>
                            <option value="2020">2020</option>
                            <option value="2019">2019</option>
                            <option value="2018">2018</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="monthly_chart_loader" class="overlay" style="display:none">
                    <span class="spinner"></span>
                </div>
            <div class="data-box-body">
                <div id="monthly-report-chart"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="data-box">
                <div class="data-box-header with-border data-box-header-dropdown">
                    <h2 class="box-title">Daily Report</h2>

                    <div class="cust_datepicker_outer">
                            <div class="cust_datepicker">
                                <input type="text" id="daily_chart_year_filter" class="form-control from" readonly="" placeholder="Select Year Month">
                            </div>
                        <div class="cstm-select-box m-l10">
                            <select id="daily_chart_status_filter" class="form-control">
                            <option disabled="disabled" selected="selected">Select</option>
                                <option value="PROCESSING" title="PROCESSING">PROCESSING</option>
                                <option value="PROCESSED" title="PROCESSED">PROCESSED</option>                            
                                <option value="SHIPPED" title="SHIPPED">SHIPPED</option>                            
                                <option value="DELIVERED" title="DELIVERED">DELIVERED</option>                            
                                <option value="CANCELLED" title="CANCELLED">CANCELLED</option>                            
                                <option value="FAILED" title="FAILED">FAILED</option>                            
                                <option value="REFUNDED" title="REFUNDED">REFUNDED</option>               
                            </select>
                        </div>
                    </div>

                    
                           
                           
                            
                </div>
                <div id="daily_chart_loader" class="overlay" style="display:none">
                    <span class="spinner"></span>
                </div>
                <div class="data-box-body">
                    <div id="daily-report-chart"></div>
                </div>

                
            </div>
        </div>
</div>
<!--Graph block End-->
<div class="clearfix"></div>

<!--Latest trips start-->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="data-box">
            <div class="data-box-header with-border">
                <h2 class="box-title">Latest Orders</h2>
            </div>
            <div class="data-box-body">
                <div class="comn-table latest-order-table nw-latest-trip-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Order Id</th>
                                <th>Category</th>
                                <!-- <th>Sub Category</th>
                                <th>Vehicle No</th> -->
                                <th>Pickup Location</th>
                                <th>Delivery Location</th>
                                <th>Quantity</th>
                                <th>Order Date</th>
                                <!-- <th>Bill Status</th> -->
                                <th>Status</th>
                               <!--  <th>Action</th>
                                <th>Remark</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        @if(isset($data['latest_orders']) && count($data['latest_orders']) > 0)
                            
                            @foreach($data["latest_orders"] as $value) 
                            <tr>
                                <td><a href="{{ route('logistic_latest_trips_details', $value['_id']) }}" class="tc-orange">#{{ $value["_id"] }}</a></td>
                                <td>{{ $value["product_category"]["category_name"] }} - {{ $value["product_sub_category"]["sub_category_name"] }}</td>
                                <td>{{ $value["pickup_address_city"]["city_name"] }}, {{ $value["pickup_address_state"]["state_name"] }}</td>
                                <td>{{ $value["delivery_address_city"]["city_name"] }}, {{ $value["delivery_address_state"]["state_name"] }}</td>
                                <td>{{ $value["quantity"] }} MT</td>
                                <td>{{ date('d M Y h:i:s a', strtotime($value["created_at"])) }}</td>
                                @if(isset($value["delivery_status"]))
                                        @if($value["delivery_status"] == 'DELIVERED')
                                            <td><div class="badge bg-green">{{ $value["delivery_status"] }}</div></td>
                                        @else @if($value["delivery_status"] == 'CANCELLED')
                                            <td><div class="badge bg-red">{{ $value["delivery_status"] }}</div></td>

                                            @else
                                                <td><div class="badge bg-yellow">{{ $value["delivery_status"] }}</div></td>
                                            @endif
                                        @endif
                                    @else
                                        <td>-</td>
                                    @endif
                                
                          
                            </tr>
                            @endforeach
                        @endif
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="data-box-footer clearfix">
                <div class="pull-right">
                    <a href="{{ route('logistic_latest_trips') }}" class="site-button dark">View All</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Latest trips end-->

<!--Latest Vehicles start-->
<!-- <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="data-box">
            <div class="data-box-header with-border">
                <h2 class="box-title">Latest added vehicle</h2>
            </div>
            <div class="data-box-body">
                <div class="comn-table latest-order-table nw-latest-vahicals-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Vehicle Type/Modal</th>
                                <th>Sub Category</th>
                                <th>Manufacture Year</th>
                                <th>Registration No</th>
                                <th>Created On</th>
                                <th>Per MT/km Rate</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>TATA Hyva - 2523</td>
                                <td>10 Tyre - (18 MT to 21 MT)</td>
                                <td>2016</td>
                                <td>GJ/01/AB/1234</td>
                                <td>18 Sep 2019</td>
                                <td><i class="fa fa-rupee"></i> 4/MT/KM</td>
                            </tr>
                            <tr>
                                <td>TATA Hyva - 2523</td>
                                <td>12 Tyre - (28 MT to 31 MT)</td>
                                <td>2017</td>
                                <td>GJ/18/LB/4561</td>
                                <td>17 Sep 2019</td>
                                <td><i class="fa fa-rupee"></i> 6/MT/KM</td>
                            </tr>
                            <tr>
                                <td>TATA Hyva - 2523</td>
                                <td>14 Tyre - (31 MT to 41 MT)</td>
                                <td>2017</td>
                                <td>GJ/27/XS/8521</td>
                                <td>16 Sep 2019</td>
                                <td><i class="fa fa-rupee"></i> 8/MT/KM</td>
                            </tr>
                            <tr>
                                <td>TATA Hyva - 2523</td>
                                <td>22 Tyre - (45 MT to 55 MT)</td>
                                <td>2018</td>
                                <td>GJ/02/AB/4567</td>
                                <td>15 Sep 2019</td>
                                <td><i class="fa fa-rupee"></i> 10/MT/KM</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="data-box-footer clearfix">
                <div class="pull-right">
                    <a href="vehicle-list.html" class="site-button dark">View All</a>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!--Latest Vehicles end-->

</div>

@endsection