@extends('logistics.layouts.logistics_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Logistics</h1>
                <div id="forgotpassword" style="display: block;">
                    <h2>Forgot Password</h2>
                    @if(isset($data["error"]))
                        <div class="error alert alert-danger">{{ $data["error"]["message"] }}</div>
                    @endif
                    <!-- <form id="newpasswordotp" style="display: block;"> -->
                    <form action="{{ route('logistics_forgot_pass_otp_send') }}" name="forgot_pass_email_verify_form" method="post" onsubmit="return validateForm()">
                        @csrf
                        <!-- <div class="form-group">
                            <div class="input-group">
                                <span class="text-gray-dark">We will send a link on your registered email or One Time OTP on your mobile no to reset your password.</span>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <div class="input-group">
                                <label>Mobile No. / Email Id <span class="str">*</span></label>
                                <input class="form-control" id="mobile_or_email" name="email_mobile" placeholder="e.g. 9898989898 / johndoe@example.com" type="text">
                                <div class="invalid-feedback" id="email_mobile_error"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group text-left">
                                <!-- <a href="forgot-password-otp.html" class="site-button yellow m-t15">Send OTP</a> -->
                                <button type="submit" name="otp_send_btn" value="otp_send_btn" class="site-button m-t15">Send OTP</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function validateForm() {

    $(".error").html("");
    $("#email_mobile_error").html("");

    $(".error").removeClass("alert-danger");

  var email_mobile = document.forms["forgot_pass_email_verify_form"]["email_mobile"].value.trim();

  var is_valid = validateMobileEmail(email_mobile);

    return is_valid;



}

</script>

@endsection