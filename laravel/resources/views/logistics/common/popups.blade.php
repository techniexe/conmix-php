<!-- Dashboard -->

<!-- ALERT MESSAGE START -->
<div id="accept-alert-popup" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15">
        Are you sure you want to accept bill?
      </div>
      <div class="modal-footer text-center">
          <button type="button" data-dismiss="modal" class="site-button green m-r15">Yes</button>
          <button type="button" data-dismiss="modal" class="site-button red">No</button>
      </div>
    </div>
  </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- ALERT MESSAGE START -->
<div id="approve-alert-popup" class="alert-message modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body p-tb20 p-lr15">
            Are you sure you want to approve?
        </div>
        <div class="modal-footer text-center">
            <button type="button" data-dismiss="modal" class="site-button green m-r15">Yes</button>
            <button type="button" data-dismiss="modal" class="site-button red">No</button>
        </div>
        </div>
    </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- Dashboard Over -->

<!-- Confirmation Popup MESSAGE START -->
<div id="confirmModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="confirmMessage">
        Are you sure you want to accept bill?
      </div>
      <div class="modal-footer text-center">
          <button type="button" data-dismiss="modal" class="site-button green m-r15" id="confirmOk">Yes</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="confirmCancel">No</button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation Popup MESSAGE END -->

<!-- Confirmation Popup MESSAGE START -->
<div id="otpModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="otpMessage">
        <label>Enter sent OTP on your registered mobile no <span class="str">*</span></label>
        <input type="text" id="otp" required class="form-control" placeholder="Enter OTP">
        <p id="otp_error" style="color:red;"></p>
      </div>
      <div class="modal-footer text-center">
          <button type="button" class="site-button green m-r15" id="otpOk">Submit</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="otpCancel">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation Popup MESSAGE END -->

<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>





<!-- Bank Details Module Start  Request::segment(2)-->

@if(Route::current()->getName() == 'logistic_bank_details')

<!-- ALERT MESSAGE START -->
<div id="make-default-alert-popup" class="alert-message modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body p-tb20 p-lr15">
            Are you sure to make this account as default?
        </div>
        <div class="modal-footer text-center">
            <input type="hidden" id="make_default_bank_detail_id"/>
            <input type="hidden" id="make_default_bank_detail_csrf"/>
            <button type="button" onclick="setBankAccountDefault()" class="site-button m-r15">Yes</button>
            <button type="button" data-dismiss="modal" class="site-button gray">No</button>
        </div>
        </div>
    </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-bank-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_bank_details_title">Add Bank Details</h4>
        </div>
        <form action="" name="add_bank_detail_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Account Holder Name <span class="str">*</span></label>
                                    <input type="text" name="account_holder_name" id="account_holder_name" class="form-control" placeholder="e.g. Johndoe">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Account Number <span class="str">*</span></label>
                                    <input type="text" name="account_number" id="account_number" class="form-control" placeholder="e.g. 0000123456789">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>IFSC Code <span class="str">*</span></label>
                                    <input type="text" name="ifsc" id="ifsc" class="form-control" placeholder="e.g. SBI000123">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="account_type_div">
                                    <label>Account Type <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="account_type" id="account_type" class="form-control">
                                            <option disabled="disabled" selected="selected">Select Account Type</option>
                                            <option value="current">Current</option>
                                            <option value="saving">Saving</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group m-b0">
                                <div class="input-group">
                                    <label>Cancelled Cheque Image <span class="str">*</span></label>
                                    <input type="file" name="cancelled_cheque_image" id="cancelled_cheque_image">
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="cancelled_cheque_image_div">
                                    <label>Cancelled Cheque Image <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="cancelled_cheque_image" id="cancelled_cheque_image" />
                                        </span>
                                        <input type="text" id="cancelled_cheque_image_text" class="form-control browse-input"  placeholder="e.g. pdf, jpg" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        
                        </div>
                </div>
            </div>
            <input type="hidden" name="add_bank_detail_form_type" id="add_bank_detail_form_type" value="add"/>
              <input type="hidden" name="bank_detail_id" id="bank_detail_id" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif

<!-- Bank Details Module Over -->

<!-- Product Module Start -->

@if(Route::current()->getName() == 'product')

<!-- MODAL ADD PRODUCT START-->
<div id="add-product-list" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title">Add Product</h4>
        </div>
        <form action="" name="add_product_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Product Name <span class="str">*</span></label>
                                    <input type="text" name="product_name" id="product_name" class="form-control" placeholder="e.g. superflow jointagg">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Category <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="category_id" id="category_id">
                                            <option disabled="disabled" selected="selected">Select Category</option>
                                            @if(isset($data["category"]))
                                                @foreach($data["category"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["category_name"] }}</option>
                                                @endforeach
                                            @endif
                                            <option>Sand</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Sub Category <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="sub_category_id" id="sub_category_id">
                                            
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Unit Price<span class="str">*</span></label>
                                    <input type="text" name="unit_price" id="unit_price" class="form-control" placeholder="e.g. 1000 rs">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Quantity <span class="str">*</span></label>
                                    <input type="text" name="quantity" id="product_qty" class="form-control" placeholder="e.g. 100 mt">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Min. Order <span class="str">*</span></label>
                                    <input type="text" name="minimum_order" id="minimum_order" class="form-control" placeholder="e.g. 1 mt">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Contact Person <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="contact_person_id" id="contact_person">
                                            <option disabled="disabled" selected="selected">Select contact person</option>
                                            @if(isset($data["contact_data"]))
                                                @foreach($data["contact_data"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["person_name"] }} - ({{ $value["mobile_number"] }})</option>
                                                @endforeach
                                            @endif
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Pickup Address <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select name="pickup_address_id" id="contact_address">
                                            <option disabled="disabled" selected="selected">Select Address</option>
                                            @if(isset($data["address_data"]))
                                                @foreach($data["address_data"] as $value)
                                                    <option value="{{ $value['_id'] }}">{{ $value["line1"] }}, {{ isset($value["line1"]) ? $value["line1"] : '' }}, {{ $value["cityDetails"]["city_name"] }}, {{ $value["stateDetails"]["state_name"] }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Upload Image <span class="str">*</span></label>
                                    <div class="file-browse">
                                        <span class="button-browse"> Browse <input type="file"></span>
                                        <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group avlbl-sswich m-t15">
                                    <label class="wid-60">Available:</label>
                                    <div class="verified-switch-btn cstm-css-checkbox">
                                        <label class="new-switch1 switch-green">
                                            <input type="checkbox" name="is_available" id="is_product_avail" class="switch-input" checked="">
                                            <span class="switch-label" data-on="Yes" data-off="No"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                              <div class="input-group avlbl-sswich m-t15">
                                  <label class="wid-60">Self Logistics:</label>
                                  <div class="verified-switch-btn cstm-css-checkbox">
                                      <label class="new-switch1 switch-green">
                                          <input type="checkbox" name="self_logistics" id="is_self_logistics" class="switch-input">
                                          <span class="switch-label" data-on="Yes" data-off="No"></span>
                                          <span class="switch-handle"></span>
                                      </label>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <input type="hidden" name="add_product_form_type" id="add_product_form_type" value="add"/>
                        <input type="hidden" name="add_product_id" id="add_product_id" value=""/>

                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD PRODUCT END-->


@endif

<!-- Product Module Over -->

<!-- Support Ticket Module Start -->

@if(Route::current()->getName() == 'support_ticket')

<!-- MODAL CREATE TICKET START-->
<div id="create-ticket-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Create Ticket</h4>
            </div>
            <form action="" name="add_support_ticket_form" method="post" enctype="multipart/form-data">
            @csrf
                <div class="modal-body p-tb20 p-lr15">
                    <div class="compose-message-block">
                        
                            <div class="row">
                              
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Question Type <span class="str">*</span></label>
                                            <div class="cstm-select-box">
                                                <select name="question_type">
                                                    <option disabled="disabled" selected="selected">Select question</option>
                                                    <option value="Problem on payment">Problem on payment</option>
                                                    <option value="Problem on product">Problem on product</option>
                                                    <option value="Enable to order">Enable to order</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Severity </label>
                                            <div class="cstm-select-box">
                                                <select name="severity">
                                                    <option disabled="disabled" selected="selected">Select severity</option>
                                                    <option value="Urgent">Urgent</option>
                                                    <option value="High">High</option>
                                                    <option value="Low">Normal</option>
                                                    <option value="Normal">Low</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Subject <span class="str">*</span></label>
                                            <div class="cstm-input">
                                                <textarea name="subject" class="form-control" id="ctsubject" placeholder="Brief summary of the question or issue" data-maxchar="250"></textarea>
                                                <span class="character-text">Maximum 250 characters (<span class="character-counter"></span> remains)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Description <span class="str">*</span></label>
                                            <div class="cstm-input">
                                                <textarea name="description" class="form-control" id="ctdescription" placeholder="Detailed of the question or issue" data-maxchar="5000"></textarea>
                                                <span class="character-text">Maximum 5000 characters (<span class="character-counter"></span> remaining)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Attachment <span class="str">*</span></label>
                                            <div class="upload-file-block mt-0">
                                                <input type="file" name="attachments">
                                            </div>
                                            <span class="file-note-text">Up to 3 attachment, each less than 5MB.</span>
                                            <span class="file-error-text" style="display: none;"><i class="ion-alert-circled"></i> Maximum amount of files exceeded!</span>
                                        </div>
                                    </div>

                                    <input type="hidden" name="order_id" />
                                </div>
                                
                            </div>
                        
                    </div>
                </div>
                <div class="modal-footer text-left">
                    <button type="submit" class="site-button pull-left">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL CREATE TICKET END-->



<!-- MODAL RESOLVE ALERT START-->
<div id="resolve-alertmsg" class="alert-message modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body p-tb40 p-lr15">
            Thank you for contacting customer support
        </div>
        <div class="modal-footer text-center">
            <button type="button" data-dismiss="modal" class="site-button">Ok</button>
        </div>
        </div>
    </div>
</div>
<!-- MODAL RESOLVE ALERT END-->


@endif

<!-- Support Ticket Module Over -->

<!-- MODAL VIEW MAP START-->
<div id="view-map-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header p-a0 border-0">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body p-tb20 p-lr15">
            <div class="popup-map-content-block" id="map_iframe_div">
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5864.520301024851!2d72.51392826831795!3d23.048447656590255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e9cb1d6441583%3A0x99c9230212282810!2sSCC+Infrastructure+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1551245667065" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                  
            </div>
        </div>
        </div>
    </div>
</div>
<!-- MODAL VIEW MAP END-->

<!-- Profile Module Start -->

@if(Route::current()->getName() == 'logistics_profile_edit_view')

<!-- MODAL EDIT PROFILE START-->
<div id="edit-profile-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Edit Profile</h4>
        </div>
        <form action="" name="update_profile_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                
                <div class="form-group">
                    <div class="input-group">
                        <label>Full Name</label>
                        <input type="text" name="full_name" id="full_name" class="form-control" value="" placeholder="e.g. jhon">
                    </div>
                </div>
                <!-- <div class="form-group">
                    <div class="input-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" value="Doe" placeholder="e.g. doe">
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="input-group">
                        <label>Email</label>
                        <input type="email" id="email" class="form-control" value="" disabled placeholder="e.g. johndoe@example.com">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <label>Mobile No.</label>
                        <input type="text" class="form-control" disabled value="" id="mobile_number">
                    </div>
                </div>
            
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button pull-left">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- MODAL EDIT PROFILE END-->

<!-- MODAL EDIT PROFILE START-->
<div id="change-password-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Change Password</h4>
        </div>
        <form action="" name="update_password_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
            
                    <!-- <div class="form-group">
                        <div class="input-group">
                            <label>Old Password</label>
                            <input type="password" class="form-control" value="" placeholder="e.g. old password">
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="input-group">
                            <label>New Password</label>
                            <input type="password" name="password" id="new_password" class="form-control" value="" placeholder="e.g. new password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Confirm Password</label>
                            <input type="password" name="confirm_password" class="form-control" value="" placeholder="e.g. confirm password">
                        </div>
                    </div>
            
            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button pull-left">Update</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- MODAL EDIT PROFILE END-->

@endif

<!-- Profile Module Over -->

<!-- Driver Module Start -->

@if(Route::current()->getName() == 'logistic_driver_details')

<!-- MODAL ADD BANK DETAIL START-->
<div id="add-bank-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_driver_form_title">Add Driver Details</h4>
        </div>
        <form action="" name="add_driver_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Driver Name <span class="str">*</span></label>
                                    <input type="text" name="driver_name" id="driver_name" class="form-control" placeholder="e.g. Johndoe">
                                </div>
                            </div>
                        </div>                 

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Mobile No. <span class="str">*</span></label>
                                    <input name="driver_mobile_number"  id="driver_mobile_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Alternate No </label>
                                    <input name="driver_alt_mobile_number"  id="driver_alt_mobile_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Whatsapp No </label>
                                    <input name="driver_whatsapp_number"  id="driver_whatsapp_number" class="form-control mobile_validate" value="" placeholder="e.g. 9898989898" >
                                </div>
                            </div>
                        </div>

                    

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="driver_pic_div">
                                    <label>Driver Image </label>
                                    <div class="file-browse">
                                        <span class="button-browse">
                                        Browse <input type="file" name="driver_pic" />
                                        </span>
                                        <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly>
                                    </div>
                                </div>
                            </div>                    
                        </div>
                        <input type="hidden" name="add_driver_form_type" id="add_driver_form_type" value="add"/>
                         <input type="hidden" name="driver_id" id="driver_id" value=""/> 
                        <div class="clearfix"></div>
                        
                        </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif


<!-- Driver Module Over -->


<!-- Latest Order Moduel start -->

@if(Route::current()->getName() == 'logistic_latest_trips_details')

<!-- MODAL ADD BANK DETAIL START-->
<div id="assign-truck-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="add_driver_form_title">Truck Assign</h4>
        </div>
        <form action="" name="truck_assigned_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="vehicle-block vehicle-category-block">
                    <div class="form-group">
                        <div class="input-group">
                            <label>Vehicle No<span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="vehicle_id">
                                    <option disabled="disabled" selected="selected">Select Vehicle</option>
                                    @if(isset($final_data["vehicles"]))
                                        @foreach($final_data["vehicles"] as $value)
                                            <option value="{{ $value['_id'] }}">{{ $value["vehicle_rc_number"] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Deliver Quntity (MT)<span class="str">*</span></label>
                            <input type="text" name="delivered_quantity" class="form-control" placeholder="Quntity (MT)">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Select Vehicle Assign Date<span class="str">*</span></label>
                            <input type="text" name="assigned_at" class="form-control truck_assign_date" readonly="" placeholder="e.g. mm/dd/yyyy">
                            
                        </div>
                    </div>
                    <input type="hidden" name="order_id" value="{{ isset($final_data['data']['_id']) ? $final_data['data']['_id'] : '' }}" id="logistic_order_id"/>
                    <input type="hidden" name="order_item_id" value="{{ isset($final_data['data']['orderItem'][0]['_id']) ? $final_data['data']['orderItem'][0]['_id'] : '' }}" id="logistic_order_item_id"/> 
                    <input type="hidden" name="order_item_temp_qty" value="{{ isset($final_data['data']['orderItem'][0]['temp_quantity']) ? $final_data['data']['orderItem'][0]['temp_quantity'] : '0' }}" id="logistic_order_item_qty"/> 
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

<script>
	$('.truck_assign_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'mm/dd/yyyy'
	});
</script>




<!-- MODAL Order tracking START-->
<div id="order-track" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Order Tracking</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15" id="order_trackking_div">
                
            </div>
        </div>
    </div>
</div>
<!-- MODAL Order tracking END-->




@endif

<!-- Latest Order Moduel over -->



