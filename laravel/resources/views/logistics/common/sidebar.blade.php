<!-- SIDBAR NAV MENU START  -->
<nav id="sidebar" class="NW-sidebar-wrapper">
    <div class="sidebar-header">
        <h3 class="text-center"><a href="{{ route('logistics_dashboard') }}"><img src="{{asset('assets/logistics/images/logo3.png') }}"></a></h3>
        <strong><a href="{{ route('logistics_dashboard') }}"><img src="{{asset('assets/logistics/images/small_logo.png') }}"></a></strong>
    </div>
    <div class="NW-sidebar-menu">
            <ul>
              <li class="{{ Route::current()->getName() == 'logistics_dashboard' ? 'active' : '' }}">
                <a href="{{ route('logistics_dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span>
                </a>
              </li>
              <li class="{{ Route::current()->getName() == 'logistic_send_quote' ? 'active' : '' }}">
                <a href="{{ route('logistic_send_quote') }}"><i class="fa fa-file-text-o"></i><span>Quote</span>
                </a>
              </li>
              <li class="{{ Route::current()->getName() == 'logistic_latest_trips' ? 'active' : '' }}">
                <a href="{{ route('logistic_latest_trips') }}"><i class="fa fa-shopping-bag"></i><span>Latest Order</span>
                </a>
              </li>
              <li class="{{ Route::current()->getName() == 'logistic_vehicles' ? 'active' : '' }}">
                <a href="{{ route('logistic_vehicles') }}"><i class="fa fa-truck"></i><span>Vehicle</span>
                </a>
              </li>
              <li class="{{ Route::current()->getName() == 'logistic_driver_details' ? 'active' : '' }}">
                <a href="{{ route('logistic_driver_details') }}"><i class="fa fa-users"></i><span>Driver</span>
                </a>
              </li>
              <li class="{{ Route::current()->getName() == 'logistic_bank_details' ? 'active' : '' }}">
                <a href="{{ route('logistic_bank_details') }}"><i class="fa fa-bank"></i><span>Bank Detail</span>
                </a>
              </li>
              <li class="{{ Route::current()->getName() == 'logistics_payment_history' ? 'active' : '' }}">
                <a href="{{ route('logistics_payment_history') }}"><i class="fa fa-money"></i><span>Payment History</span>
                </a>
              </li>
              
              <!-- <li class="{{ Route::current()->getName() == 'supplier_dashboard' ? 'active' : '' }}">
                <a href="{{ route('supplier_dashboard') }}"><i class="fa fa-id-card-o"></i><span>Customer Support</span>
                </a>
              </li> -->
              <li class="{{ Route::current()->getName() == 'logistics_report' ? 'active' : '' }}">
                <a href="{{ route('logistics_report') }}"><i class="fa fa-line-chart"></i><span>Report</span>
                </a>
              </li>
              
              <li class="{{ Route::current()->getName() == 'logistics_faq' ? 'active' : '' }}">
                <a href="{{ route('logistics_faq') }}"><i class="fa fa-question-circle"></i><span>FAQs</span>
                </a>
              </li>
            </ul>
          </div>
</nav>
<!-- SIDBAR NAV MENU END  -->