<!doctype html>
<html lang="en">
<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />
    <meta name="description" content="" />
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
    
    <!-- FAVICONS ICON -->
    <link rel="icon" type="image/png" href="{{asset('assets/logistics/images/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{asset('assets/logistics/images/favicon-16x16.png') }}" sizes="16x16" />
    
    <title>Logistics</title>
    
    <!-- BOOTSTRAP STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/logistics/css/bootstrap.min.css') }}">
    <!-- FONTAWESOME STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/logistics/css/font-awesome.min.css') }}"/>
    <!-- CUSTOM SCROLLBAR STYLE SHEET -->
    <link rel="stylesheet" href="{{asset('assets/logistics/css/jquery.mCustomScrollbar.css') }}">
    <!-- MAIN STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/logistics/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/custom.css')}}">
    
    <!-- NEW THEME STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/logistics/css/new-theme-style.css') }}">
   
    <link rel="stylesheet" type="text/css" href="{{asset('assets/common/css/jquery.toast.css')}}">

    <!-- SELECT2 CHOSEN STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/select2-chosen.css')}}">

    <script type="text/javascript" src="{{asset('assets/logistics/js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyDgc0AfDO-QPVxfx03eXhLBphZ3CKwYp5o&sensor=false&libraries=places'></script>
    <script type="text/javascript" src="{{asset('assets/common/js/locationpicker.jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/common/js/utils.js') }}"></script>
    <script type="text/javascript" src="{{asset('assets/logistics/js/logistics.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/logistics/js/jquery.lightbox.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>

</head>
<script>

var BASE_URL = '{{ url('/') }}';
// console.log("BASE_URL..."+BASE_URL);

</script>