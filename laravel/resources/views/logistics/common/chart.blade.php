
@if((Route::current()->getName() == 'logistics_dashboard') )

<script>

$('.from').datepicker({
	    autoclose: true,
	    minViewMode: 1,
	    format: 'M yyyy'
		}).on('changeDate', function(selected){
	        startDate = new Date(selected.date.valueOf());
	        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});

/*montly chart script start*/



function setDataInChart(order_data, sales_amount_data, quantity_data, div_id, title, categories){

    var options = {
        chart: {
          fontFamily: 'Roboto',
          height: 350,
          type: 'bar',
          zoom: {
            enabled: false
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: [0.1, 0.1, 0.1],
          curve: 'straight',
          dashArray: [0, 0, 0]
        },
        series: [{
            name: "Orders",
            data: order_data
          },
          {
            name: "Quantity",
            data: quantity_data
          },
          {
            name: 'Sales Amount ( <i class="fa fa-rupee"></i> )',
            data: sales_amount_data
          }
        ],
        title: {
          text: ''+title,
          align: 'center'
        },
        markers: {
          size: 0,
          hover: {
            sizeOffset: 5
          }
        },
        xaxis: {
          categories: categories,
        },
        tooltip: {
          y: [{
            title: {
              formatter: function (val) {
                return val + ""
              }
            }
          }, {
            title: {
              formatter: function (val) {
                return val + ""
              }
            }
          }, {
            title: {
              formatter: function (val) {
                return val;
              }
            }
          }]
        },
        grid: {
          borderColor: '#d3e0e9',
        },
        colors: ['#008ffb', '#00e396', '#feb019'],
      }
      
      // var chart = new ApexCharts(
      //   document.querySelector("#"+div_id),
      //   options

      // );
      // chart.render();
  
    return options;
}

var monthly_cat = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
var daily_cat = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];


var monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];

var product_monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var product_monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var product_monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];

var daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];


var product_daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var product_daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var product_daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];


// setDataInChart(daily_order_data, daily_sales_data, daily_quantity_data, 'daily-report-chart', "Daily Average");


var month_array = {

    "01" : 0,
    "02" : 1,
    "03" : 2,
    "04" : 3,
    "05" : 4,
    "06" : 5,
    "07" : 6,
    "08" : 7,
    "09" : 8,
    "10" : 9,
    "11" : 10,
    "12" : 11,

};

var all_month_array = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

var monthly_chart_option = setDataInChart(monthly_order_data, monthly_sales_data, monthly_quantity_data, 'monthly-report-chart', "Monthly Average", monthly_cat);

var monthly_chart = new ApexCharts(
    document.querySelector("#monthly-report-chart"),
    monthly_chart_option

  );
  monthly_chart.render();
  
  
var daily_chart_option = setDataInChart(daily_order_data, daily_sales_data, daily_quantity_data, 'daily-report-chart', "Daily Average",daily_cat);

var daily_chart = new ApexCharts(
    document.querySelector("#daily-report-chart"),
    daily_chart_option

  );
  daily_chart.render();


var product_monthly_chart_option = setDataInChart(product_monthly_order_data, product_monthly_sales_data, daily_quantity_data, 'product-monthly-report-chart', "Product Monthly Average",monthly_cat);

var product_monthly_chart = new ApexCharts(
    document.querySelector("#product-monthly-report-chart"),
    product_monthly_chart_option

  );
  product_monthly_chart.render();


  var product_daily_chart_option = setDataInChart(product_daily_order_data, product_daily_sales_data, product_daily_quantity_data, 'product-daily-report-chart', "Product Daily Average",monthly_cat);

var product_daily_chart = new ApexCharts(
    document.querySelector("#product-daily-report-chart"),
    product_daily_chart_option

  );
  product_daily_chart.render();

var product_daily_chart;

function getMonthlyChartDetails(year,status){

    $("#monthly_chart_loader").fadeIn(300);

    var request_data = {
      'year': year,
      'status': status
    };
    

    customResponseHandler(
        "logistics/getMonthlyChartData", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess

            // data = JSON.parse(data);
            console.log(data["data"]);
            
            $.each(data["data"], function(key,value){

              console.log(value);
              var month = value["_id"].split("-")[1];

              month = month_array[month];
              monthly_order_data[month] = value["count"];
              monthly_sales_data[month] = Math.round(value["total_amount"]);
              monthly_quantity_data[month] = Math.round(value["quantity"]);

            });

            if(data["data"].length == 0){
                monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                monthly_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];
            }

            
            monthly_chart_option = setDataInChart(monthly_order_data, monthly_sales_data, monthly_quantity_data, 'monthly-report-chart', "Monthly Average", monthly_cat);
          
            monthly_chart.updateOptions(monthly_chart_option,false,true);
            
            $("#monthly_chart_loader").fadeOut(300);
            
            

        }
    );

}


getMonthlyChartDetails();

$("#monthly_chart_data_year_filter").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  var monthly_chart_status = $("#monthly_chart_data_status_filter").val();

  console.log("valueSelected..."+valueSelected+"..."+monthly_chart_status);

  getMonthlyChartDetails(valueSelected,monthly_chart_status);

});



$("#monthly_chart_data_status_filter").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  var monthly_chart_year = $("#monthly_chart_data_year_filter").val();

  console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year);

  getMonthlyChartDetails(monthly_chart_year,valueSelected);

});

function getDailyChartDetails(year,month,status){

  $("#daily_chart_loader").fadeIn(300);

  var request_data = {
      'year': year,
      'month': month,
      'status': status
    };

    customResponseHandler(
        "logistics/getDailyChartData", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess

            // data = JSON.parse(data);
            console.log(data["data"]);
            $.each(data["data"], function(key,value){

              console.log(value);
              var day = value["_id"]["day"];

              if(day > 0){
                  day = day -1;
              }
              daily_order_data[day] = value["count"];
              daily_sales_data[day] = Math.round(value["total_amount"]);
              daily_quantity_data[day] = Math.round(value["quantity"]);

            });

            if(data["data"].length == 0){
              daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
              daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
              daily_quantity_data = [0,0,0,0,0,0,0,0,0,0,0,0];
            }

            daily_chart_option = setDataInChart(daily_order_data, daily_sales_data, daily_quantity_data, 'daily-report-chart', "Daily Average",daily_cat);
            
            daily_chart.updateOptions(daily_chart_option,false,true);
            
            $("#daily_chart_loader").fadeOut(300);

        }
    );

}


getDailyChartDetails();


// $("#daily_chart_year_filter").change(function() {
//   var optionSelected = $("option:selected", this);
//   var valueSelected = this.value;
  
//   var monthly_chart_month = $("#daily_chart_month_filter").val();
//   var monthly_chart_status = $("#daily_chart_status_filter").val();

//   console.log("valueSelected..."+valueSelected+"..."+monthly_chart_month+"..."+monthly_chart_status);

//   getDailyChartDetails(valueSelected,monthly_chart_month, monthly_chart_status);

// });

var daily_month;
var daily_year;
$("#daily_chart_year_filter").datepicker().on('changeMonth', function(e){ 
   daily_month = new Date(e.date).getMonth() + 1;
   daily_month = all_month_array[daily_month-1];
   daily_year = String(e.date).split(" ")[3];
   console.log("daily_year..."+daily_year+"..."+daily_month);

   var monthly_chart_status = $("#daily_chart_status_filter").val();

   getDailyChartDetails(daily_year,daily_month, monthly_chart_status);
 });

// $("#daily_chart_month_filter").change(function() {
//   var optionSelected = $("option:selected", this);
//   var valueSelected = this.value;
  
//   var monthly_chart_year = $("#daily_chart_year_filter").val();
//   var monthly_chart_status = $("#daily_chart_status_filter").val();

//   console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year+"..."+monthly_chart_status);

//   getDailyChartDetails(monthly_chart_year,valueSelected, monthly_chart_status);

// });

$("#daily_chart_status_filter").change(function() {
  var optionSelected = $("option:selected", this);
  var valueSelected = this.value;
  
  // var monthly_chart_year = $("#daily_chart_year_filter").val();
  // var monthly_chart_month = $("#daily_chart_month_filter").val();

  console.log("valueSelected..."+valueSelected+"..."+daily_month+"..."+daily_year);

  getDailyChartDetails(daily_year,daily_month, valueSelected);

});





</script>

@endif