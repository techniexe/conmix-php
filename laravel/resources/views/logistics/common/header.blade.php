<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-dark toggle_btn">
            <i class="fa fa-bars"></i>
        </button>
        <div class="user_block">
            <!-- <div class="notification float-left position-relative">
                <a href="javascript:;" class="notificationClick"><i class="fa fa-bell"></i><span class="note_badge position-absolute">4</span></a>
                <div class="more_notification position-absolute pullDown">
                    <div class="more_header p-3 float-left w-100">
                        <span class="float-left">You have 4 new notifications</span>
                    </div>
                    <ul class="mCustomScrollbar list-unstyled float-left w-100 m-0">
                        <li class="float-left w-100">
                            <a href="#" class="float-left w-100">                                        
                                <i class="fa fa-info-circle"></i> You have new Aggregate Order <span>SCC Online Pvt. Ltd.</span> 10:30 AM Today                                        
                            </a>
                        </li>
                        <li class="float-left w-100">
                            <a href="#" class="float-left w-100">                                        
                                <i class="fa fa-info-circle"></i> You have new Aggregate Order <span>SCC Online Pvt. Ltd.</span> 10:30 AM Today                                        
                            </a>
                        </li>
                        <li class="float-left w-100">
                            <a href="#" class="float-left w-100">                                        
                                <i class="fa fa-info-circle"></i> You have new Aggregate Order <span>SCC Online Pvt. Ltd.</span> 10:30 AM Today                                        
                            </a>
                        </li>
                        <li class="float-left w-100">
                            <a href="#" class="float-left w-100">                                        
                                <i class="fa fa-info-circle"></i> You have new Aggregate Order <span>SCC Online Pvt. Ltd.</span> 10:30 AM Today                                        
                            </a>
                        </li>
                    </ul>
                    <div class="notification_footer text-center float-left w-100">
                        <a href="#" class="view_all position-relative float-left w-100">See all Notifications</a>
                    </div>
                </div>
            </div> -->
            
            <div class="user_icon float-left">
                <a href="javascript:;" class="userdropdownClick">
                    @php
                        $profile = session('profile_details', null);
                    @endphp
                    <img src="{{asset('assets/supplier/images/user-img.png')}}">
                    <span class="float-left">{{ $profile["full_name"] }}</span>
                </a>
                <ul class="user_menu m-0 pullDown">
                    <!--<li><a href="#"><i class="fa fa-cog"></i>Setting</a></li>-->
                    <li><a href="{{ route('logistics_profile') }}"><i class="fa fa-user"></i>My Profile</a></li>
                    
                    <li><a href="{{ route('logistic_logout') }}"><i class="fa fa-power-off"></i>Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>