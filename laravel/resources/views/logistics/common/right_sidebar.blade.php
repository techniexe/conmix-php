<!-- Access Logs Module Over -->
@if(Route::current()->getName() == 'logistic_vehicles')

<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <form action="{{ route('logistic_vehicles') }}" name="" method="get">
          @csrf
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                      <select name="vehicle_category_id">
                          <option disabled="disabled" selected="selected">Select vehicle category</option>
                          @if(isset($data["vehicle_categoies"]))
                            @foreach($data["vehicle_categoies"] as $cat_value)
                              <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == Request::get("vehicle_category_id") ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                            @endforeach
                          @endif
                      </select>
                    </div>
                </div>


            </div>

            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="vehicle_sub_category_id">
                        <option disabled="disabled" selected="selected">Select vehicle sub category</option>
                        @if(isset($data["subcategories"]))
                          @foreach($data["subcategories"] as $cat_value)
                            <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == Request::get("vehicle_sub_category_id") ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                          @endforeach
                        @endif
                    </select>
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="is_gps_enabled">
                        <option disabled="disabled" selected="selected">Select GPS enabled</option>
                        <option value="true" {{ Request::get("is_gps_enabled") == 'true' ? 'selected' : '' }}>Yes</option>
                        <option value="false" {{ Request::get("is_gps_enabled") == 'false' ? 'selected' : '' }}>No</option>
                    </select>
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="is_insurance_active">
                        <option disabled="disabled" selected="selected">Select insurance active</option>
                        <option value="true" {{ Request::get("is_insurance_active") == 'true' ? 'selected' : '' }}>Yes</option>
                        <option value="false" {{ Request::get("is_insurance_active") == 'false' ? 'selected' : '' }}>No</option>
                    </select>
                  </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

@endif
<!-- Access Logs Module Over -->