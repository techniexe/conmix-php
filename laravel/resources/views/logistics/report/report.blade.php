@extends('logistics.layouts.logistics_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Reports</h1>
    <div class="clearfix"></div>
    <div class="review-complaints-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Reports List</h2>
                </div>
                <div class="reports_block">
                    <!-- <div class="reportsList_box">
                        <h3>Finances</h3>
                        <ul>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                        </ul>
                    </div> -->
                    <!-- <div class="reportsList_box">
                        <h3>Inventory</h3>
                        <ul>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                        </ul>
                    </div> -->
                    <div class="reportsList_box">
                        <h3>Sales</h3>
                        <ul>
                            <li><a href="{{ route('logistics_order_listing_report') }}">Orders</a></li>
                        </ul>
                    </div>
                    <!-- <div class="reportsList_box">
                        <h3>Clients</h3>
                        <ul>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy</a></li>
                            <li><a href="report-details.html">Lorem ipuem dummy text</a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection