@extends('logistics.layouts.logistics_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Quote</h1>
    <div class="clearfix"></div>
    <!--Latest Orders start-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="data-box wht-tble-bg">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Bid Quote</h2>
                </div>
                <div class="data-box-body">
                    <div class="comn-table latest-order-table table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-nowrap">Bid Timer</th>
                                <th class="text-nowrap">Quote Id</th>
                                <th class="text-nowrap">Product</th>
                                <!-- <th class="text-nowrap">Sub Category</th> -->
                                <th class="text-nowrap">Quantity</th>
                                <th class="text-nowrap">Pickup Location</th>
                                <th class="text-nowrap">Delivery Location</th>
                                <th class="text-nowrap">Distance</th>
                                <th class="text-nowrap">Order Date</th>
                                <th class="text-nowrap">Max Bid Price ( <i class="fa fa-rupee"></i> )</th>
                                <th class="text-nowrap">My Bidding Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th class="text-nowrap">Status</th>
                                <th class="text-nowrap">Action</th>
                            </tr>


                        </thead>
                        <tbody>
                        @if(isset($data['data']) && count($data['data']) > 0)
                        @php
                        //dd($data['data']);
                            $first_created_date = '';
                            $last_created_date = '';
                            $count = 0;
                        @endphp
                        <?php //dd($data["data"]); 
                            $second = 1000;
                            $minute = $second * 60;
                            $hour = $minute * 60;
                        ?>
                        <script>    

                            const second = 1000,
                            minute = second * 60,
                            hour = minute * 60;

                        </script>
                        @foreach($data["data"] as $value)  
                            @if($count == 0)
                                @php 
                                $first_created_date = $value["created_at"];
                                $count++; 
                                @endphp
                                @endif
                                @php
                                $last_created_date = $value["created_at"];
                                @endphp
                                <tr>
                                    <td>
                                        @if(isset($value['max_quoted_at']))
                                        <?php 
                                            $current_time = time();
                                            $max_quote_time = strtotime($value['max_quoted_at']);
                                            // dd($max_quote_time.'...'.$current_time);
                                            $final_diff = $max_quote_time - $current_time;
                                            
                                            $final_hour = round($final_diff / ($hour));
                                            $final_min = round(($final_diff % ($hour)) / ($minute));
                                            $final_sec = round(($final_diff % ($minute)) / $second);

                                            // echo $final_sec;

                                            $final_hour = $final_hour > 0 ? $final_hour : 0;
                                            $final_min = $final_min > 0 ? $final_min : 0;
                                            $final_sec = $final_sec > 0 ? $final_sec : 0;
                                            

                                        ?>
                                            
                                            <div class="custom_timer" id="timer_div_{{$value['_id']}}">
                                                
                                                <input type="hidden" id="timer_hid_{{$value['_id']}}" value="{{ date('d M, Y H:i:s', strtotime($value['max_quoted_at'])) }}" />
                                                <ul>
                                                    <li><span id="hours_{{$value['_id']}}"></span></li>
                                                    <li><span id="minutes__{{$value['_id']}}"></span></li>
                                                    <li><span id="seconds__{{$value['_id']}}"></span></li>
                                                </ul>
                                            </div>
                                            <script>
                                                
                                                <?php //$time =   strtotime($value['max_quoted_at']) ?>
                                                
                                                x = setInterval(function() {
                                                    var time = $("#timer_hid_{{$value['_id']}}").val();
                                                    let countDown = new Date(time).getTime();
                                                    // console.log("countDown..."+countDown);
                                                    let now = new Date().getTime(),
                                                    distance = countDown - now;

                                                    var final_hour = Math.round(distance  / (hour));
                                                    var final_min = Math.round((distance % (hour)) / (minute));
                                                    var final_sec = Math.round((distance % (minute)) / second);
                                                    
                                                    final_hour = final_hour > 0 ? final_hour : 0;
                                                    final_min = final_min > 0 ? final_min : 0;
                                                    final_sec = final_sec > 0 ? final_sec : 0;

                                                    if(final_hour > 0 || final_min > 0 || final_sec > 0){
                                                        document.getElementById("hours_{{$value['_id']}}").innerText = final_hour,
                                                        document.getElementById("minutes__{{$value['_id']}}").innerText = final_min,
                                                        document.getElementById("seconds__{{$value['_id']}}").innerText = final_sec;
                                                    }else{
                                                        $("#timer_div_{{$value['_id']}}").html("TimeOut");
                                                    }
                                                
                                                }, second)
                                            </script>
                                            
                                        @endif
                                    </td>
                                    <td>#{{ isset($value["quote_display_id"]) ? $value["quote_display_id"] : '' }}</td>
                                    <td>{{ isset($value["productCategory"]["category_name"]) ? $value["productCategory"]["category_name"] : '' }} - {{ isset($value["productSubcategory"]["sub_category_name"]) ? $value["productSubcategory"]["sub_category_name"] : '' }}</td>
                                    <!-- <td>Sub Category</td> -->
                                    <td class="text-nowrap">{{ isset($value["quantity"]) ? $value["quantity"] : '' }} MT</td>
                                    <td>
                                        <div class="cstm-tooltip" data-direction="bottom">
                                            @if(isset($value["pickup_address"]["line1"]))
                                            <div class="pickup-address pickup_address_new  cstm-tooltip__initiator">
                                                <!-- {{ $value["pickup_address"]["line1"] }} -->
                                                {{ isset($value["pickup_address"]['line1']) ? $value["pickup_address"]['line1'] : '' }},
                                                {{ isset($value["pickup_address"]['line2']) ? $value["pickup_address"]['line2'] : '' }},

                                                {{ isset($value["pickup_address_city"]['city_name']) ? $value["pickup_address_city"]['city_name'] : '' }},
                                                {{ isset($value["pickup_address_state"]['state_name']) ? $value["pickup_address_state"]['state_name'] : '' }}
                                            </div>
                                            <!-- <div class="cstm-tooltip__item"> -->
                                                <!-- Techniexe Infolabs LLP 104, Sumel II, Near GuruDwara, SG Highway Ahmedabad 380054, (INDIA) -->
                                                <!-- {{ isset($value["pickup_address"]['line1']) ? $value["pickup_address"]['line1'] : '' }},
                                                {{ isset($value["pickup_address"]['line2']) ? $value["pickup_address"]['line2'] : '' }},

                                                {{ isset($value["pickup_address_city"]['city_name']) ? $value["pickup_address_city"]['city_name'] : '' }},
                                                {{ isset($value["pickup_address_state"]['state_name']) ? $value["pickup_address_state"]['state_name'] : '' }}
                                            </div> -->
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        <div class="cstm-tooltip" data-direction="bottom">
                                            @if(isset($value["delivery_address"]["address_line1"]))
                                            <div class="pickup-address pickup_address_new  cstm-tooltip__initiator">
                                                <!-- {{ $value["delivery_address"]["address_line1"] }} -->
                                                {{ isset($value["delivery_address"]['address_line1']) ? $value["delivery_address"]['address_line1'] : '' }},
                                                {{ isset($value["delivery_address"]['address_line2']) ? $value["delivery_address"]['address_line2'] : '' }},
                                                {{ isset($value["delivery_address_city"]['city_name']) ? $value["delivery_address_city"]['city_name'] : '' }},
                                                {{ isset($value["delivery_address_state"]['state_name']) ? $value["delivery_address_state"]['state_name'] : '' }}
                                            </div>
                                            <!--<div class="cstm-tooltip__item">
                                                Techniexe Infolabs LLP 104, Sumel II, Near GuruDwara, SG Highway Ahmedabad 380054, (INDIA) 
                                                {{ isset($value["delivery_address"]['address_line1']) ? $value["delivery_address"]['address_line1'] : '' }},
                                                {{ isset($value["delivery_address"]['address_line2']) ? $value["delivery_address"]['address_line2'] : '' }},
                                                {{ isset($value["delivery_address_city"]['city_name']) ? $value["delivery_address_city"]['city_name'] : '' }},
                                                {{ isset($value["delivery_address_state"]['state_name']) ? $value["delivery_address_state"]['state_name'] : '' }}
                                            </div>-->
                                            @endif
                                        </div>
                                    </td>
                                    <td>{{ isset($value["distance"]) ? round($value["distance"]) : '' }} KM</td>
                                    <td class="text-nowrap">{{ isset($value["created_at"]) ? date('d M Y h:i:s a',strtotime($value["created_at"])) : '' }}</td>
                                    <td>{{ isset($value["max_quote_amount"]) ? round($value["max_quote_amount"]) : '' }}</td>
                                    <td>{{ isset($value["quoted_amount"]) ? round($value["quoted_amount"]) : 'Not Bidded yet' }}</td>
                                    <td><span class="badge bg-red">{{ isset($value["status"]) ? $value["status"] : '' }}</span></td>
                                    <td>
                                    @if(isset($value["quoted_amount"]))
                                        <p>You have bidded on this quote</p>
                                    @else
                                        <div class="dropdown cstm-dropdown-select bid-rate-dropdown">
                                            <a href="javascript:;" class="site-button gray dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Bid Rate</a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                <li>
                                                    <form action="" name="quote_update_form_{{ $value['quote_display_id'] }}" method="post">
                                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        <!-- <div class="form-group">
                                                            <div class="input-group single-search-select2">
                                                                <select id="multiple" class="form-control-chosen" data-placeholder="Select Truck">
                                                                    <option></option>
                                                                    <option>GJ-01-AB-1234 (10 Tyre)</option>
                                                                    <option>GJ-01-CD-4567 (12 Tyre)</option>
                                                                    <option>GJ-01-EF-8910 (14 Tyre)</option>
                                                                    <option>GJ-01-HI-1112 (22 Tyre)</option>
                                                                </select>
                                                            </div>
                                                        </div> -->
                                                        
                                                        <input type="hidden" name="quoteId" value="{{ isset($value['_id']) ? $value['_id'] : '' }}" />
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                    <label>Bidding Amount ( <i class="fa fa-rupee"></i> )</label>
                                                                <input type="text" name="quoted_amount" class="form-control" placeholder="e.g. 500" />
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="site-button blue">Send</button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                    </td>
                                </tr>
                                <script>

                                    $(document).ready(function(){

                                        $("form[name='quote_update_form_{{ $value['quote_display_id'] }}']").validate({
                                            rules: {
                                                quoted_amount: {
                                                    required: true,
                                                    number: true
                                                }
                                            },
                                            messages: {
                                                
                                            },
                                            submitHandler: function(form) {
                                                console.log("submited");
                                                // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
                                                var form_values = $("form[name='quote_update_form_{{ $value['quote_display_id'] }}']").serialize();
                                                // var form_values = new FormData(form);
                                                console.log("form_type ..."+form_values );

                                                // $("form[name='logistics_register_form']").submit();
                                                updateQuote(form_values);
                                                

                                                
                                            },
                                            invalidHandler: function(event, validator) {
                                                console.log("error");
                                            }
                                        });

                                    });

                                    

                                </script>

                                @endforeach

                                @else
                                <tr>
                                    <td colspan="12">No Record Found</td>
                                </tr>
                                @endif
                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="data-box-footer clearfix">
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        @if(isset($data['data']) && (count($data['data']) >=  ApiConfig::PAGINATION_LIMIT))
                        <div class="pagination justify-content-end m-0">
                            @if(isset($first_created_date))
                            <form action="{{ route('logistic_send_quote') }}" method="get">                                
                             
                                
                                <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @if(isset($last_created_date))
                            <form action="{{ route('logistic_send_quote') }}" method="get">                              
                                
                                
                                <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Latest Orders end-->
    
    <div class="clearfix"></div>
</div>



@endsection