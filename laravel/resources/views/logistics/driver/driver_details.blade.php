@extends('logistics.layouts.logistics_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Driver Contact Detail</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="contact-detail-main-block wht-tble-bg">
        <div class="row">

            <div class="col-md-12">
                <ul>
                    <li>
                        <div class="contact-detail-block add-contact-detail-block">
                            <a href="javascript:;" onclick="resetForm('add_driver_form','add-bank-details')" data-toggle="modal" data-target="#add-bank-details"><i>+</i></a>
                        </div>
                    </li>

                    @if(isset($data["data"]))
                        @foreach($data["data"] as $value)
                            <li>
                                
                                <div class="mCustomScrollbar contact-detail-block p-a15">
                                    <p> <span class="driver-img-block"><img src="{{ isset($value['driver_pic']) ? $value['driver_pic'] : '' }}" height="50px" width="50px" alt="" /></span></p>
                                    <p><i class="fa fa-user"></i> <span class="vTitle">Driver Name : </span> <span class="vName">{{ isset($value['driver_name']) ? $value['driver_name'] : '' }}</span></p>
                                    <p><i class="fa fa-phone-square"></i> <span class="vTitle">Mobile No. : </span> <span class="vName">{{ isset($value['driver_mobile_number']) ? $value['driver_mobile_number'] : '' }}</span></p>
                                    <p><i class="fa fa-phone-square"></i>   <span class="vTitle">Alternate No. : </span> <span class="vName">{{ isset($value['driver_alt_mobile_number']) ? $value['driver_alt_mobile_number'] : '' }}</span></p>
                                    <p><i class="fa fa-whatsapp"></i> <span class="vTitle">Whatsapp No. : </span> <span class="vName">{{ isset($value['driver_whatsapp_number']) ? $value['driver_whatsapp_number'] : '' }}</span></p>
                                    <p><i class="fa fa-calendar"></i> <span class="vTitle">Created at : </span> <span class="vName">{{ isset($value['created_at']) ? date('d M Y, h:i:s a', strtotime($value['created_at'])) : '' }}</span></p>
                                    <div class="driver_opration_btn top-right-edit-link">
                                        <a href="javascript:;" onclick="showDriverDetailEditDialog('{{ json_encode($value) }}')" class="site-button green adrs-edt-icon"  title="Edit"><i class="fa fa-edit"></i></a>
                                       <!--  <a href="javascript:;" class="site-button red adrs-dlt-icon" data-toggle="modal" data-target="" title="Delete"><i class="fa fa-trash"></i></a> -->
                                    </div>
                                </div>
                                
                            </li>
                        @endforeach
                    @endif
                    
                    
                    
                    
                    
                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection