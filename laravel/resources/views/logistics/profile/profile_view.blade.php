@extends('logistics.layouts.logistics_layout')
@section('content')
@php
    $profile_data = $data["data"];
    //dd($profile_data);
@endphp
<div class="middle-container-wrap">
    <h1 class="main-title">My Profile</h1>
    <div class="clearfix"></div>
    <!-- my profile start -->
    <div class="my-profile-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="my-profile-content-block">
                    <div class="sub-title-block">
                        <h2 class="sub-title pull-left">Account Details </h2>
                        <a href="{{ route('logistics_profile_edit_view') }}" class="edit-profile-btn site-button outline"><i class="fa fa-edit"></i> Edit</a>
                    </div>
                    <div class="my-profile-details-block">
                    <p><strong>Company Type :</strong> {{ isset($profile_data['company_type']) ? $profile_data['company_type'] : 'Not Available' }}</p>
                        <p><strong>Person Name :</strong> {{ isset($profile_data['full_name']) ? $profile_data['full_name'] : 'Not Available' }}</p>
                           <p><strong>Company Name :</strong> {{ isset($profile_data['company_name']) ? $profile_data['company_name'] : 'Not Available' }}</p>
                       
                     <!--    <p><strong>Fax Number:</strong> {{ isset($profile_data['fax_number']) ? $profile_data['fax_number'] : 'Not Available' }}</p> -->
                        <p><strong>Mobile No. :</strong> {{ isset($profile_data['mobile_number']) ? $profile_data['mobile_number'] : 'Not Available' }}</p>
                        <p><strong>Landline No. :</strong> {{ isset($profile_data['landline_number']) ? $profile_data['landline_number'] : 'Not Available' }}</p>
                         <p><strong>Email :</strong> {{ isset($profile_data['email']) ? $profile_data['email'] : 'Not Available' }}</p>
                        <p><strong>Website Url :</strong> {{ isset($profile_data['website_URL']) ? $profile_data['website_URL'] : 'Not Available' }}</p>
                        <p><strong>Company Reg No :</strong>{{ isset($profile_data['company_certification_number']) ? $profile_data['company_certification_number'] : '' }} |  
                        
                        <p>
                            <strong>Certification Image :</strong> 
                            @if(isset($profile_data["company_certification_image_url"]))

                                @if(strpos($profile_data["company_certification_image_url"], '.doc') || strpos($profile_data["company_certification_image_url"], '.docx'))
                                    <a href="{{ $profile_data['company_certification_image_url'] }}" class="document_box docfile">
                                        <i class="fa fa-file-word-o"></i>
                                        <span>Company Certificate</span>
                                    </a>
                                @endif

                                @if(strpos($profile_data["company_certification_image_url"], '.jpg') || strpos($profile_data["company_certification_image_url"], '.png') || strpos($profile_data["company_certification_image_url"], '.jpeg'))
                                    <a href="$profile_data['company_certification_image_url']" class="document_box gal_link">
                                        <i class="fa fa-image"></i>
                                        <span>Company Certificate</span>
                                    </a>

                                @endif

                                @if(strpos($profile_data["company_certification_image_url"], '.pdf'))
                                    <a href="{{ $profile_data['company_certification_image_url'] }}" class="document_box pdffile">
                                        <i class="fa fa-file-pdf-o"></i>
                                        <span>Company Certificate</span>
                                    </a>
                                @endif

                                <!-- <i class="fa fa-image" style="color: #388E3C;"></i>
                                <a href="{{ isset($profile_data['company_certification_image_url']) ? $profile_data['company_certification_image_url'] : 'Not Available' }}">Comapny registration Cerificate</a>
                                
                                <a href="#" class="document_box docfile">
                                    <i class="fa fa-file-word-o"></i>
                                    <span>Company Doc file</span>
                                </a> -->
                            @endif
                        </p>
                        
                        <p>
                            <strong>Pan Image :</strong> 
                                @if(isset($profile_data["pancard_image_url"]))
                                    @if(strpos($profile_data["pancard_image_url"], '.doc') || strpos($profile_data["pancard_image_url"], '.docx'))
                                        <a href="{{ $profile_data['pancard_image_url'] }}" class="document_box docfile">
                                            <i class="fa fa-file-word-o"></i>
                                            <span>Pancard</span>
                                        </a>
                                    @endif

                                    @if(strpos($profile_data["pancard_image_url"], '.jpg') || strpos($profile_data["pancard_image_url"], '.png') || strpos($profile_data["pancard_image_url"], '.jpeg'))
                                        <a href="$profile_data['pancard_image_url']" class="document_box gal_link">
                                            <i class="fa fa-image"></i>
                                            <span>Pancard</span>
                                        </a>

                                    @endif

                                    @if(strpos($profile_data["pancard_image_url"], '.pdf'))
                                        <a href="{{ $profile_data['pancard_image_url'] }}" class="document_box pdffile">
                                            <i class="fa fa-file-pdf-o"></i>
                                            <span>Pancard</span>
                                        </a>
                                    @endif
                                @endif

                                <!-- <i class="fa fa-image" style="color: #388E3C;"></i>
                                <a href="{{ isset($profile_data['company_certification_image_url']) ? $profile_data['company_certification_image_url'] : 'Not Available' }}">Comapny registration Cerificate</a>
                                
                                <a href="#" class="document_box docfile">
                                    <i class="fa fa-file-word-o"></i>
                                    <span>Company Doc file</span>
                                </a> -->
                           
                        </p>
                        
                         <p>
                            <strong>GST Image :</strong> 
                                @if(isset($profile_data["gst_certification_image_url"]))
                                    @if(strpos($profile_data["gst_certification_image_url"], '.doc') || strpos($profile_data["gst_certification_image_url"], '.docx'))
                                        <a href="{{ $profile_data['gst_certification_image_url'] }}" class="document_box docfile">
                                            <i class="fa fa-file-word-o"></i>
                                            <span>GST Certificate</span>
                                        </a>
                                    @endif

                                    @if(strpos($profile_data["gst_certification_image_url"], '.jpg') || strpos($profile_data["gst_certification_image_url"], '.png') || strpos($profile_data["gst_certification_image_url"], '.jpeg'))
                                        <a href="$profile_data['gst_certification_image_url']" class="document_box gal_link">
                                            <i class="fa fa-image"></i>
                                            <span>GST Certificate</span>
                                        </a>

                                    @endif

                                    @if(strpos($profile_data["gst_certification_image_url"], '.pdf'))
                                        <a href="{{ $profile_data['gst_certification_image_url'] }}" class="document_box pdffile">
                                            <i class="fa fa-file-pdf-o"></i>
                                            <span>GST Certificate</span>
                                        </a>
                                    @endif
                                @endif

                                <!-- <i class="fa fa-image" style="color: #388E3C;"></i>
                                <a href="{{ isset($profile_data['company_certification_image_url']) ? $profile_data['company_certification_image_url'] : 'Not Available' }}">Comapny registration Cerificate</a>
                                
                                <a href="#" class="document_box docfile">
                                    <i class="fa fa-file-word-o"></i>
                                    <span>Company Doc file</span>
                                </a> -->
                           
                        </p>

                        <!-- <i class="fa fa-image" style="color: #388E3C
;"></i> <a href="{{ isset($profile_data['company_certification_image_url']) ? $profile_data['company_certification_image_url'] : 'Not Available' }}" >Company Registration Certificate</a></p> -->
                        <!-- <p><strong>Pan No:</strong> {{ isset($profile_data['pan_number']) ? $profile_data['pan_number'] : 'Not Available' }}   | 
                        <i class="fa fa-image" style="color: #388E3C
;"></i> <a href="{{ isset($profile_data['pancard_image_url']) ? $profile_data['pancard_image_url'] : 'Not Available' }}">Pancard</a></p> -->
                        <!-- <p><strong>GST No:</strong> {{ isset($profile_data['gst_number']) ? $profile_data['gst_number'] : 'Not Available' }}    |  <i class="fa fa-image" style="color: #388E3C
;"></i> <a href="{{ isset($profile_data['gst_certification_image_url']) ? $profile_data['gst_certification_image_url'] : 'Not Available' }}"> GST Certificate </a></p> -->
                        <p><strong>Password :</strong> ********</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="my-profile-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="my-profile-content-block">
                    <div class="sub-title-block">
                        <h2 class="sub-title pull-left">Office Address </h2>
                        <!-- <a href="{{ route('logistics_profile_edit_view') }}" class="edit-profile-btn site-button outline"><i class="fa fa-edit"></i> Edit</a> -->
                    </div>
                    <div class="my-profile-details-block">
                        <p><strong>Address : </strong> 
                            {{ isset($profile_data['line1']) ? $profile_data['line1'] : 'Not Available' }}, 
                            {{ isset($profile_data['line2']) ? $profile_data['line2'] : 'Not Available' }}
                        </p>
                        <p><strong>Pincode : </strong> {{ isset($profile_data['pincode']) ? $profile_data['pincode'] : 'Not Available' }}</p>
                        <p><strong>City : </strong> {{ isset($profile_data['city_name']) ? $profile_data['city_name'] : 'Not Available' }}</p>
                        <p><strong>State : </strong> {{ isset($profile_data['state_name']) ? $profile_data['state_name'] : 'Not Available' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- my profile end -->
</div>

@endsection

 