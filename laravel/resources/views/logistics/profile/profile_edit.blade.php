@extends('logistics.layouts.logistics_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Edit My Profile</h1>
    <div class="clearfix"></div>
    <!-- my profile start -->
    <div class="row">
        <div class="col-md-12">
            <form action="" name="edit_myprofile_details_form" method="post">
            @csrf
                @php
                    $profile_data = $data["data"];
                @endphp
                <div class="my-profile-block edit-my-profile-block wht-tble-bg">
                    <div class="my-profile-content-block">
                        <h3>Account Details</h3>
                        <div class="my-profile-details-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="input-group ">
                                                <label>Company Type <span class="str">*</span></label>
                                                <div class="cstm-select-box">
                                                    <select class="form-control" name="company_type">
                                                        <option disabled="disabled">Select Type</option>
                                                        <option value="Partnership" {{ $profile_data['full_name'] == 'Partnership' ? 'selected' : '' }}>Partnership</option>
                                                        <option value="Proprietor" {{ $profile_data['full_name'] == 'Proprietor' ? 'selected' : '' }}>Proprietor</option>
                                                        <option value="LLP" {{ $profile_data['full_name'] == 'LLP' ? 'selected' : '' }}>LLP.</option>
                                                        <option value="LTD" {{ $profile_data['full_name'] == 'LTD' ? 'selected' : '' }}>LTD.</option>
                                                        <option value="PVT. LTD." {{ $profile_data['full_name'] == 'PVT. LTD.' ? 'selected' : '' }}>PVT.LTD.</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Person Name <span class="str">*</span></label>
                                            <input type="text" class="form-control" name="full_name" value="{{ isset($profile_data['full_name']) ? $profile_data['full_name'] : '' }}" placeholder="e.g. Johndoe">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Mobile No. <span class="str">*</span></label>
                                            <input type="tel" class="form-control mobile_validate" name="mobile_number" placeholder="e.g. 9898989898" value="{{ isset($profile_data['mobile_number']) ? $profile_data['mobile_number'] : '' }}" id="mobileno">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Landline No. </label>
                                            <input type="text" class="form-control" name="landline_number" value="{{ isset($profile_data['landline_number']) ? $profile_data['landline_number'] : '' }}" placeholder="e.g. 0791234567">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Email <span class="str">*</span></label>
                                            <input type="email" class="form-control" name="email" value="{{ isset($profile_data['email']) ? $profile_data['email'] : '' }}" placeholder="e.g. johndoe@example.com">
                                        </div>
                                    </div>
                                </div>
                           <!--      <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Fax Number </label>
                                            <input type="text" class="form-control" name="fax_number" value="{{ isset($profile_data['fax_number']) ? $profile_data['fax_number'] : '' }}" placeholder="e.g. 1234">
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Website Url </label>
                                            <input type="text" class="form-control" name="website_URL" value="{{ isset($profile_data['website_URL']) ? $profile_data['website_URL'] : '' }}" placeholder="e.g. example.com">
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Company Registration No <span class="str">*</span></label>
                                            <input type="text" class="form-control" name="company_certification_number" value="{{ isset($profile_data['company_certification_number']) ? $profile_data['company_certification_number'] : '' }}" placeholder="e.g. CSC123456789">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group" id="company_certification_image_div">
                                            <label>Company Registration Certificate <span class="str">*</span></label>
                                            <div class="file-browse">
                                                <span class="button-browse">
                                                    Browse <input type="file" name="company_certification_image">
                                                </span>
                                                <input type="text" value="{{ isset($profile_data['company_certification_image_url']) ? $profile_data['company_certification_image_url'] : '' }}" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Pan No <span class="str">*</span></label>
                                            <input type="text" class="form-control" name="pan_number" value="{{ isset($profile_data['pan_number']) ? $profile_data['pan_number'] : '' }}" placeholder="e.g. CSC123456789">
                                        </div>
                                    </div>
                                </div>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group" id="pancard_image_div">
                                            <label>Upload Pancard <span class="str">*</span> </label>
                                            <div class="file-browse">
                                                <span class="button-browse">
                                                    Browse <input type="file" name="pancard_image">
                                                </span>
                                                <input type="text" value="{{ isset($profile_data['pancard_image_url']) ? $profile_data['pancard_image_url'] : '' }}" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>GST No <span class="str">*</span></label>
                                            <input type="text" class="form-control" name="gst_number" value="{{ isset($profile_data['gst_number']) ? $profile_data['gst_number'] : '' }}" placeholder="e.g. CSC123456789">
                                        </div>
                                    </div>
                                </div>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group" id="gst_certification_image_div">
                                                    <label>GST Certificate <span class="str">*</span></label>
                                                    <div class="file-browse">
                                                        <span class="button-browse">
                                                            Browse <input type="file">
                                                        </span>
                                                        <input type="text" value="{{ isset($profile_data['gst_certification_image_url']) ? $profile_data['gst_certification_image_url'] : '' }}" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>New Password </label>
                                            <input type="password" class="form-control" name="password" value="" id="new_password" placeholder="e.g. New Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Confirm Password </label>
                                            <input type="password" class="form-control" name="confirm_password" value="" placeholder="e.g. Confirm Password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="is_my_profile" value="yes"/>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <button type="submit" class="site-button m-r10">Save</button>
                               <a href="{{ route('logistics_profile') }}" class="site-button gray">Cancel</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="clearfix m-t20"></div>

            <form action="{{route('logistics_register_2_submit')}}" name="logistics_address_register_form" method="post">
            @csrf
                <div class="my-profile-block edit-my-profile-block wht-tble-bg">
                    <div class="my-profile-content-block">
                        <h3>Edit Office Address</h3>
                        <div class="my-profile-details-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group single-search-select2" id="state_name_city_div">
                                            <label>State Name <span class="str">*</span></label>
                                            <select id="state_name_city" name="state_id" class="form-control-chosen" data-placeholder="Select State Name">
                                                <?php //dd($profile_data); ?>
                                                <option></option>
                                                @if(isset($data["state_data"]))                                                    
                                                    @foreach($data["state_data"] as $value)
                                                        <option value="{{ $value['_id'] }}" {{ isset($profile_data['state_id']) ? ($profile_data['state_id'] == $value['_id'] ? 'selected' : '') : '' }}>{{ $value['state_name'] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group single-search-select2" id="state_name_city_filter_div">
                                            <label>City Name <span class="str">*</span></label>
                                            <select id="state_name_city_filter" name="city_id" class="form-control-chosen" data-placeholder="Select City Name">
                                                <option></option>
                                                @if(isset($data["city_data"]))                                                    
                                                    @foreach($data["city_data"] as $value)
                                                        <option value="{{ $value['_id'] }}" {{ isset($profile_data['city_id']) ? ($profile_data['city_id'] == $value['_id'] ? 'selected' : '') : '' }}>{{ $value['city_name'] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Address Line 1 <span class="str">*</span></label>
                                            <input class="form-control" name="address_line_1" value="{{ isset($profile_data['line1']) ? $profile_data['line1'] : '' }}" placeholder="e.g. Address Line 1" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Address Line 2 <span class="str">*</span></label>
                                            <input class="form-control" name="address_line_2" value="{{ isset($profile_data['line2']) ? $profile_data['line2'] : '' }}" placeholder="e.g. Address Line 2" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Pincode <span class="str">*</span></label>
                                            <input type="text" name="pincode" value="{{ isset($profile_data['pincode']) ? $profile_data['pincode'] : '' }}" class="form-control" placeholder="e.g. 123456">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="mp_note_block"><b>Note:</b> Choose permenant location of your vehicle.</div>
                                                <input type="text" placeholder="Enter Your Location" required="" name="us2_address" id="us2_address" class="form-control pac-target-input" onkeydown="return event.key != 'Enter';" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Latitude</label>
                                                    <input type="text" required="" name="us2_lat" id="us2_lat" class="form-control" readonly="">
                                                    <p class="error" id="us2_lat_error"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <label>Longitude</label>
                                                    <input type="text" required="" name="us2_lon" id="us2_lon" class="form-control" readonly="">
                                                    <p class="error" id="us2_lon_error"></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="mapnote">Please Set Accurate Pickup Address</div>
                                            <div class="map-content-block add-vehicle-map-block m-t20" style="height: 300px; margin-top: 0px;">
                                                <div id="us2" style="width: 100%; height: 290px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="is_my_profile" value="yes"/>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <button type="submit" name="register_btn" value="register_btn" class="site-button m-r10">Save</button>
                               <a href="{{ route('logistics_profile') }}" class="site-button gray">Cancel</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </form>
           
        </div>
    </div>
    <!-- my profile end -->
</div>

<script>
@php
    // $final_data = $data["data"];
    // dd($profile_data);
    $latitude = $profile_data["pickup_location"]["coordinates"][1];
    $longitude = $profile_data["pickup_location"]["coordinates"][0];
@endphp
$('#us2').locationpicker({
    enableAutocomplete: true,
        enableReverseGeocode: true,
    radius: 0,
    inputBinding: {
        latitudeInput: $('#us2_lat'),
        longitudeInput: $('#us2_lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2_address')
    },
    onchanged: function (currentLocation, radius, isMarkerDropped) {
            // var addressComponents = $(this).locationpicker('map').location.addressComponents;
        console.log(currentLocation);  //latlon  
        // updateControls(addressComponents); //Data
        }
    });

    function updateControls(addressComponents) {
        console.log(addressComponents);
    }

    $('#us2').locationpicker("location", {latitude: '{{ isset($latitude) ? $latitude : 0 }}', longitude: '{{ isset($longitude) ? $longitude : 0 }}'});
</script>


@endsection