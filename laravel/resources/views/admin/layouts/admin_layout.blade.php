@include('admin.common.master')

<body class="nw-theme">
<!-- <div id="loadingDiv" style="position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 99999999999;
    background-color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;"><img src="{{asset('assets/buyer/images/gif_logo.gif')}}" alt="conmix"></div>
 -->
 <div id="loadingDiv" style="position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 99999999999;
    background-color: #fff;
    display: block;
    align-items: center;
    justify-content: center;">
        @include('supplier.layouts.shimmer.after_login_panel_text')
    </div>
    <div class="wrapper">
        @include('admin.common.sidebar')

        <div id="content">
            @include('admin.common.header')
            <div class="bg_dark"></div>

            <!-- <div class="middle-container-wrap"> -->
                @yield('content')
            <!-- </div> -->

            <!-- <footer class="w-100 bg-gray py-3 px-4 position-absolute">
                <aside class="left float-lg-left text-center">
                    <p class="m-0">© Copyright {{date("Y")}}. All Rights Reserved. Conmix Pvt. Ltd.</p>
                </aside>
            
            </footer> -->
            <script>
                $(".clear_filter_button").hide();
            </script>
            <footer class="w-100 bg-gray py-3 px-4 position-absolute">
                <div class="customer-sidebar-support-left-block">
                     <h3>Customer Care : <span>+91 9979016486</span> &nbsp;|&nbsp; Write Us On : <span>support@conmix.in</span></h3>
                </div>
                <aside class="left float-lg-left text-center">
                    <p class="m-0">© Copyright {{date("Y")}}. All Rights Reserved. Conmix Pvt. Ltd.</p>
                </aside>
            </footer>
            
        </div>
        <!-- CONTENT BLOCK END  -->
        @include('admin.common.right_sidebar')
    </div>

    @include('admin.common.popups')


@include('admin.common.footer')
@include('admin.common.charts')