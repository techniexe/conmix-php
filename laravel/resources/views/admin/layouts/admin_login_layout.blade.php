@include('admin.common.master')

<body>
<!-- <div id="loadingDiv" style="position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 99999999999;
    background-color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;"><img src="{{asset('assets/buyer/images/gif_logo.gif')}}" alt="conmix"></div> -->
    <div id="loadingDiv" style="position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 99999999999;
    background-color: #fff;
    display: block;
    align-items: center;
    justify-content: center;">
        @include('supplier.layouts.shimmer.simple_text')
    </div>
    <div class="wrapper lsf-main-block">
        <!-- SIDBAR NAV MENU START  -->
        <nav id="sidebar" class="NW-sidebar-wrapper">
            <a href="#" class="login_logo"><img src="{{asset('assets/admin/images/logo3.webp') }}"><sup style="color: #fff;top: 0px;vertical-align: text-top;">BETA</sup></a>
        </nav>
        <!-- SIDBAR NAV MENU END  -->
        <!-- CONTENT BLOCK START  -->
        <div id="content">

            <!-- <div class="middle-container-wrap"> -->
                @yield('content')
            <!-- </div> -->
        </div>
        <!-- CONTENT BLOCK END  -->
        <footer class="w-100 bg-gray py-3 px-4 position-absolute">
            <div class="customer-sidebar-support-left-block">
                 <h3>Customer Care: <span>+91 9979016486</span> &nbsp;|&nbsp; Write Us On: <span>support@conmix.in</span></h3>
            </div>
            <aside class="left float-lg-left text-center">
                <p class="m-0">© Copyright {{date("Y")}}. All Rights Reserved. Conmix Pvt. Ltd.</p>
            </aside>
        </footer>
    </div>

 

@include('admin.common.footer')
<script type="text/javascript" src="{{asset('assets/admin/js/intlTelInput.js')}}"></script>
<script>
$(document).ready(function() {
    /*input mobile no with flag*/
    var input = document.querySelector("#mobileno");
    window.intlTelInput(input, {
      utilsScript: "js/utils.js" // just for formatting/placeholders etc
    });
    /*input mobile no with flag end*/
});

</script>