@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums">Logs / <span>Action Logs</span></h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="logs-block action-logs-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Action Logs <span> - (For developer only)</span></h2>
                    <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                    <a href="{{ route('admin_show_action_logs') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                </div>
                <div class="comn-table1 table-responsive m-b20 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>User Id</th>
                                <th>User Type</th>
                                <th>User Details</th>
                                <th>Action</th>
                                <!-- <th>Access Type</th> -->
                                <th>Old Value</th>
                                <th>New Value</th>
                                <th>Changed Value</th>
                                <th>Log Description</th>
                                <th>Date & Time</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data["data"]) && count($data['data']) > 0)
                                
                                @foreach($data["data"] as $value)  
                                    @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                   
                                        <tr>
                                            <td>#{{ isset($value["user_id"]) ? $value["user_id"] : '' }}</td>
                                            <!-- <td>{{ isset($value["user_details"]["user_type"]) ? ($value["user_details"]["user_type"] == 1 ? $value["user_details"]["admin_details"]["admin_type"] : ($value["user_details"]["user_type"] == 2 ? 'Supplier' : 'Buyer')) : '' }}</td> -->
                                            <td>

                                            <?php 
                                                if(isset($value["user_details"]["user_type"])){

                                                    if($value["user_details"]["user_type"] == 1){

                                                        echo $value["user_details"]["admin_details"]["admin_type"];

                                                    }else if($value["user_details"]["user_type"] == 2){

                                                        echo 'Supplier '.'('.($value["user_details"]["vendor_details"]["master_vendor_id"] == null ? 'Master Vendor' : 'Sub Vendor').')';

                                                    }else if($value["user_details"]["user_type"] == 3){

                                                        echo 'Buyer';

                                                    }


                                                }

                                            ?>

                                            </td>
                                            <td>

                                                <?php 

                                                    if(isset($value["user_details"]["user_type"])){

                                                        if($value["user_details"]["user_type"] == 1){

                                                            echo "<span style='color:#000;font-style: italic;font-weight: bolder;'>Name:</span> ".$value["user_details"]["full_name"];

                                                        }else if($value["user_details"]["user_type"] == 2){

                                                            echo "<span style='color:#000;font-style: italic;font-weight: bolder;'>Name:</span> ".$value["user_details"]["full_name"]."<br/>";
                                                            echo "<span style='color:#000;font-style: italic;font-weight: bolder;'>Company Name:</span> ".$value["user_details"]["vendor_details"]["company_name"]."<br/>";

                                                        }else if($value["user_details"]["user_type"] == 3){

                                                            echo "<span style='color:#000;font-style: italic;font-weight: bolder;'>Name: ".$value["user_details"]["full_name"]."<br/>";
                                                            echo "<span style='color:#000;font-style: italic;font-weight: bolder;'>Company Name:</span> ".$value["user_details"]["buyer_details"]["company_name"]."<br/>";

                                                        }


                                                    }


                                                ?>

                                            </td>
                                            <td>{{ isset($value["action"]) ? ucfirst($value["action"]) : '' }}</td>
                                            @if(isset($value["old_value"]))
                                                <td>
                                                <?php
                                                    $old_value = json_decode($value["old_value"]);
                                                    foreach($old_value as $key => $val) {
                                                       echo "<span style='color:#000;font-style: italic;font-weight: bolder;'>$key:</span> <span>$val</span> <br/>";
                                                        // $display_old_text = $display_old_text.$final_val;
                                                    }

                                                    // if(is_array($new_value) && is_array($new_value)){

                                                    // $all_new_value_key = array_keys($new_value);
                                                        
                                                    // $display_old_text = "";
                                                    // $display_new_text = "";

                                                    // foreach($all_new_value_key as $key_value){

                                                    //     if(($key_value != 'updated_at') && ($key_value != 'updated_by_id')){

                                                    //         if(isset($old_value[$key_value]) && isset($new_value[$key_value])){
                                                    //             if(is_array($new_value[$key_value])){
                                                    //                 $display_old_text = $display_old_text.$key_value.': '.json_encode($old_value[$key_value]).', ';
                                                    //                 $display_new_text = $display_new_text.$key_value.': '.json_encode($new_value[$key_value]).', ';
                                                    //             }else{
                                                    //                 $display_old_text = $display_old_text.$key_value.': '.$old_value[$key_value].', ';
                                                    //                 $display_new_text = $display_new_text.$key_value.': '.$new_value[$key_value].', ';
                                                    //             }
                                                                
                                                    //         }
                                                            

                                                    //     }

                                                    // }

                                                    // $display_old_text = rtrim($display_old_text, ', ');
                                                    // $display_new_text = rtrim($display_new_text, ', ');

                                                    // }else{
                                                    //     $display_old_text = $value["old_value"];
                                                    //     $display_new_text = $value["new_value"];
                                                    // }

                                                ?>
                                            </td>
                                                
                                            @else
                                                <td style="color:#ff0000"> - </td>
                                                <td style="color:#0000ff"> - </td>
                                            @endif
                                            @if(isset($value["new_value"]))
                                            <td>
                                                <?php
                                                    $new_value = json_decode($value["new_value"]);
                                                    
                                                    foreach($new_value as $key => $val) {
                                                       echo "<span style='color:#000;font-style: italic;font-weight: bolder;'>$key:</span> <span>$val</span> <br/>";
                                                        // $display_old_text = $display_old_text.$final_val;
                                                    }
                                                ?>
                                            </td>
                                            @endif
                                            @if(isset($value["changed_value"]))
                                            <td>
                                                <?php
                                                    $new_value = json_decode($value["changed_value"]);
                                                    
                                                    foreach($new_value as $key => $val) {
                                                       echo "<span style='color:#000;font-style: italic;font-weight: bolder;'>$key:</span> <span>$val</span> <br/>";
                                                        // $display_old_text = $display_old_text.$final_val;
                                                    }
                                                ?>
                                            </td>
                                            @endif
                                            <td>{{ isset($value["description"]) ? $value["description"] : '' }}</td>
                                            <td class="text-nowrap">{{ isset($value["created_at"]) ?  AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>
                                        </tr>
                                @endforeach
                            @else
                                <tr>
                                   <td colspan="7">No Record Found</td>
                                </tr>
                            @endif
                           
                        </tbody>
                    </table>
                </div>

                <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="pagination-block">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                    <div class="pagination justify-content-end m-0">
                    @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_type'))
                                    <input type="hidden" value="{{ Request::get('user_type') ? Request::get('user_type') : '' }}" name="user_type"/>
                                @endif
                                @if(Request::get('access_type'))
                                    <input type="hidden" value="{{ Request::get('access_type') ? Request::get('access_type') : '' }}" name="access_type"/>
                                @endif
                                @if(Request::get('search'))
                                    <input type="hidden" value="{{ Request::get('search') ? Request::get('search') : '' }}" name="search"/>
                                @endif
                                @if(Request::get('start_date'))
                                    <input type="hidden" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}" name="start_date"/>
                                @endif
                                @if(Request::get('end_date'))
                                    <input type="hidden" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}" name="end_date"/>
                                @endif
                                
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_type'))
                                    <input type="hidden" value="{{ Request::get('user_type') ? Request::get('user_type') : '' }}" name="user_type"/>
                                @endif
                                @if(Request::get('access_type'))
                                    <input type="hidden" value="{{ Request::get('access_type') ? Request::get('access_type') : '' }}" name="access_type"/>
                                @endif
                                @if(Request::get('search'))
                                    <input type="hidden" value="{{ Request::get('search') ? Request::get('search') : '' }}" name="search"/>
                                @endif
                                @if(Request::get('start_date'))
                                    <input type="hidden" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}" name="start_date"/>
                                @endif
                                @if(Request::get('end_date'))
                                    <input type="hidden" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}" name="end_date"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button ml-2">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>


@endsection