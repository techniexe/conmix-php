@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums">Logs / <span>Email Logs</span></h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="logs-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Email Logs</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_email_logs') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                </div>
                <div class="logs-email-block">
                @php
                            $first_created_date = '';
                            $last_created_date = '';
                            $count = 0;
                        @endphp
                    @if(isset($data["data"]) && count($data['data']) > 0)
                        
                        @foreach($data["data"] as $value)  
                            @if($count == 0)
                                @php 
                                    $first_created_date = $value["sent_at"];
                                    $count++; 
                                @endphp
                            @endif
                            @php
                                $last_created_date = $value["sent_at"];
                            @endphp
                            @php 
                                $header_name = '';
                                $header_value = '';
                            @endphp
                            @if(isset($value['headers']))
                                @foreach($value['headers'] as $header_values)
                                    @php 
                                        
                                        $header_name = $header_name . ''.$header_values["header_name"].',';
                                        $header_value = $header_value . ''.$header_values["header_value"].',';
                                    @endphp
                                
                                @endforeach
                            @endif

                                <div class="logs-email-contet-block m-b20">
                                    <div class="row">
                                        <div class="logs-links-block">
                                            <a href="javascript:;" class="site-button green pull-right" onclick="showResendEmailTemplateDialog('{{ json_encode($value) }}')" data-tooltip="Resend Mail"><i class="fa fa-send"></i></a>
                                            <a href="javascript:;" class="site-button blue pull-right m-r5" onclick="logEmailPreview('{{ isset($value['html']) ? $value['html'] : '' }}')" data-tooltip="Preview"><i class="fa fa-eye"></i></a>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <h3 class="logs-email-title">{{ isset($value['user']['user_type']) ? $value['user']['user_type'] : ''  }}</h3>
                                            <ul>
                                                <li>
                                                    <p><span class="vTitle">To Email: </span> <span class="vName">{{ isset($value['to_email']) ? $value['to_email'] : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">To Name: </span> <span class="vName">{{ isset($value['to_name']) ? $value['to_name'] : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">From Email: </span> <span class="vName">{{ isset($value['from_email']) ? $value['from_email'] : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">From Name: </span> <span class="vName">{{ isset($value['from_name']) ? $value['from_name'] : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Subject: </span> <span class="vName">{{ isset($value['subject']) ? $value['subject'] : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Cc: </span> <span class="vName">{{ isset($value['cc']) ? $value['cc'] : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Bcc: </span> <span class="vName">{{ isset($value['bcc']) ? $value['bcc'] : ''  }}</span></p>
                                                </li>
                                                <li>                                                    
                                                    <p><span class="vTitle">Header Name: </span> <span class="vName">{{ isset($header_name) ? $header_name : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Header Value: </span> <span class="vName">{{ isset($header_value) ? $header_value : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Send At: </span> <span class="vName">{{ isset($value['sent_at']) ? date('d M Y h:i:s a',strtotime($value['sent_at'])) : ''  }}</span></p>
                                                </li>
                                                <li>
                                                    <p><span class="vTitle">Send Status: </span> <span class="text-red">{{ isset($value['send_status']) ? $value['send_status'] : ''  }}</span></p>
                                                </li>
                                            </ul>
                                        </div>                                    
                                    </div>
                                </div>
                        @endforeach
                    @else
                       <div style="text-align: center;">No Record Found</div>
                    @endif
                    

                    <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif
                                      
                    
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        <div class="pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('from_email'))
                                    <input type="hidden" value="{{ Request::get('from_email') ? Request::get('from_email') : '' }}" name="from_email"/>
                                @endif
                                @if(Request::get('to_email'))
                                    <input type="hidden" value="{{ Request::get('to_email') ? Request::get('to_email') : '' }}" name="to_email"/>
                                @endif
                                @if(Request::get('subject'))
                                    <input type="hidden" value="{{ Request::get('subject') ? Request::get('subject') : '' }}" name="subject"/>
                                @endif
                                
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                               
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('from_email'))
                                    <input type="hidden" value="{{ Request::get('from_email') ? Request::get('from_email') : '' }}" name="from_email"/>
                                @endif
                                @if(Request::get('to_email'))
                                    <input type="hidden" value="{{ Request::get('to_email') ? Request::get('to_email') : '' }}" name="to_email"/>
                                @endif
                                @if(Request::get('subject'))
                                    <input type="hidden" value="{{ Request::get('subject') ? Request::get('subject') : '' }}" name="subject"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

@endsection