@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Email Template</span></h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="et-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <button class="site-button pull-right" onclick="resetForm('add_email_template_form','create-email-template')" data-toggle="modal" data-target="#create-email-template">Add</button>
                </div>
                <div class="et-block">
                    @if(isset($data["data"]) && !empty($data["data"]))
                        @php
                            $first_created_date = '';
                            $last_created_date = '';
                            $count = 0;
                        @endphp
                        @foreach($data["data"] as $value)  
                            @if($count == 0)
                                @php 
                                    $first_created_date = $value["created_at"];
                                    $count++; 
                                @endphp
                            @endif
                            @php
                                $last_created_date = $value["created_at"];
                            @endphp

                                <div class="et-contet-block m-b20">
                                    <div class="et-field-data-block">
                                        <div class="et-link-block">
                                            <a href="javascript:;" class="site-button green pull-right" onclick="getEmailTemplateDetails('{{ $value['template_type'] }}','{{ csrf_token() }}')" data-tooltip="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:;" class="site-button blue pull-right m-r5" onclick="showEmailPreview('{{ url("/email_template/iframe?templateType=") }} {{ $value['template_type'] }}','{{ $value['template_type'] }}')" data-toggle="modal" data-target="#tempPrev-mode" data-tooltip="Preview"><i class="fa fa-eye"></i></a>
                                        </div>
                                        <div class="et-preview-block">
                                            <iframe src="{{ url('/email_template/iframe?templateType=') }} {{ isset($value['template_type']) ? $value['template_type'] : '' }}" frameborder="0"></iframe>
                                        </div>
                                        <ul>
                                            <li>
                                                <p><span class="vTitle">Title: </span> <span class="vName">{{ isset($value["template_type"]) ? $value["template_type"] : '' }}</span></p>
                                            </li>
                                            <li>
                                                <p><span class="vTitle">Subject: </span> <span class="vName">{{ isset($value["subject"]) ? $value["subject"] : '' }}</span></p>
                                            </li>
                                            <li>
                                                <p><span class="vTitle">Cc: </span> <span class="vName">{{ isset($value["cc"]) ? str_replace(",",", ",$value["cc"]) : '' }}</span></p>
                                            </li>
                                            <li>
                                                <p><span class="vTitle">Bcc: </span> <span class="vName">{{ isset($value["bcc"]) ? str_replace(",",", ",$value["bcc"]) : '' }}</span></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="et-attachment-block">
                                        <h3><i class="fa fa-paperclip"></i> Attachments <span>({{ isset($value["attachments"]) ? count($value["attachments"]) : '0' }})</span></h3>
                                        <ul>
                                            @if(isset($value["attachments"]))

                                                @if(!empty($value["attachments"]))
                                                    @foreach($value["attachments"] as $attch_value)
                                                        @php
                                                            $attch_value["template_type"] = $value["template_type"];
                                                        @endphp

                                                        <li>
                                                            <div class="mailbox-attachment-info">
                                                            <a href="{{ isset($attch_value['attachment_url']) ? $attch_value['attachment_url'] : '' }}" class="mailbox-attachment-name"><i class="fa fa-file-pdf-o"></i> {{ isset($attch_value['attachment_name']) ? $attch_value['attachment_name'] : '' }}</a>
                                                            <span class="mailbox-attachment-size">{{ isset($attch_value['size']) ? ($attch_value['size'] / 1000) : '' }} KB</span>
                                                            <span class="inputremove1" onclick="confirmPopup(15,'{{ json_encode($attch_value) }}','{{ csrf_token() }}','',0)"><i class="fa fa-close"></i></span>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                @endif

                                            @endif
                                            
                                        </ul>
                                    </div>
                                </div>
                        @endforeach

                    @else
                        <p style="text-align: center;">No Record Found</p>
                    @endif
                    
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

@endsection