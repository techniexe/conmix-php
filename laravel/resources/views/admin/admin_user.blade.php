@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap">
    <h1 class="breadcrums">Users / <span>Add User</span></h1>
    <div class="clearfix"></div>
    <!--Add User start -->
    <div class="user-block add-user-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                @if(isset($data["error"]))
                    <div class="">{{ $data["error"]["message"] }}</div>
                @endif
                    <h2 class="sub-title pull-left">Add User</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_user') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    @if($profile["rights"]["admin_user_add"])
                        <button class="site-button pull-right m-r10" data-toggle="modal" data-target="#add-user-popup" onclick="resetForm('add_user_form','add-user-popup')">Add User</button>
                    @endif
                </div>
                <div class="comn-table1 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>Mobile No</th>
                                <th>Email</th>
                                <th>Admin Type</th>
                                <th>Date & Time</th>
                                <th>Status</th>
                                <th>Is Deleted</th>
                                @if($profile["rights"]["admin_user_action"])
                                    <th>Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                        @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data["data"]) && count($data['data']) > 0)

                                @foreach($data["data"] as $value)
                                
                                    @if($count == 0)
                                        @php
                                            $first_created_date = $value["created_at"];
                                            $count++;
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                    <tr>
                                        <td>{{ $value["full_name"] }}</td>
                                        <td>{{ isset($value["mobile_number"]) ? $value["mobile_number"] : "" }}</td>
                                        <td>{{ $value["email"] }}</td>
                                        <!-- <td>{{ str_replace("_"," ",$value["admin_type"]) }}</td> -->
                                        <td><?php echo AdminController::ADMIN_ROLES[$value["admin_type"]] ?></td>

                                        <td><?php echo AdminController::dateTimeFormat($value["created_at"]); ?>
                                            </td>
                                        <td>

                                            <div id="active_inactive_{{ $value['_id'] }}" class="badge {{ $value['is_active'] ? 'bg-green' : 'bg-grey'}} ">
                                                {{ $value['is_active'] ? 'Active' : 'InActive'}}
                                            </div>

                                        </td>
                                        <td>
                                            <div id="" class="badge {{ $value['is_deleted'] ? 'bg-grey' : 'bg-green'}} ">
                                                {{ $value['is_deleted'] ? 'Deleted' : 'Active'}}
                                            </div>
                                        </td>
                                            @if($profile["rights"]["admin_user_action"])
                                                <td>
                                                @if($value["admin_type"] != 'superAdmin')
                                                    @if($value['is_deleted'] == 0)
                                                    <div class="dropdown cstm-dropdown-select">
                                                        <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                            <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            @if($profile["rights"]["admin_user_active_inactive"])    
                                                            <li><a class="dropdown-item" href="javascript:void(0);" onclick="confirmPopup(1,'{{ json_encode($value) }}','{{ csrf_token() }}','{{ $value["_id"] }}')">{{ $value["is_active"] ? "InActive" : "Active" }}</a></li>
                                                            @endif
                                                            @if($profile["rights"]["admin_user_edit"])
                                                            <li><a class="dropdown-item" href="javascript:void(0);" onclick="showAdminDialog('{{ json_encode($value) }}')">Edit</a></li>
                                                            @endif
                                                            @if($profile["rights"]["admin_user_delete"])
                                                            <li><a class="dropdown-item" href="javascript:void(0);" onclick="confirmPopup(3,'{{ json_encode($value) }}','{{ csrf_token() }}','{{ $value["_id"] }}')">Delete</a></li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                    @endif
                                                @endif
                                                </td>
                                            @endif
                                        
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

                <div class="clearfix"></div>

                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="data-box-footer clearfix">
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                            <div class="pagination justify-content-end m-0">
                            @if($is_previous_avail == 1)
                                @if($first_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    @if(Request::get('name'))
                                        <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                    @endif
                                    @if(Request::get('mobile_no'))
                                        <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                                    @endif
                                    @if(Request::get('email'))
                                        <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('admin_type'))
                                        <input type="hidden" name="admin_type" value="{{ Request::get('admin_type') ? Request::get('admin_type') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('is_active'))
                                        <input type="hidden" name="is_active" value="{{ Request::get('is_active') ? Request::get('is_active') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                    <button type="submit" class="site-button">Previous</button>
                                </form>
                                @endif
                                @endif
                                @if($is_next_avail == 1)
                                @if($last_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    @if(Request::get('name'))
                                        <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                    @endif
                                    @if(Request::get('mobile_no'))
                                        <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                                    @endif
                                    @if(Request::get('email'))
                                        <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('admin_type'))
                                        <input type="hidden" name="admin_type" value="{{ Request::get('admin_type') ? Request::get('admin_type') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('is_active'))
                                        <input type="hidden" name="is_active" value="{{ Request::get('is_active') ? Request::get('is_active') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                    <button type="submit" class="site-button">Next</button>
                                </form>
                                @endif
                                @endif
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
            <!--Add User end -->
</div>






@endsection
