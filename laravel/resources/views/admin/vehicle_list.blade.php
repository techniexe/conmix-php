@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
            <h1 class="breadcrums">Vehicle / <span>Vehicle List</span></h1>
            <div class="clearfix"></div>
            <!--vehicle category list start -->
            <div class="vehicle-block vehicle-category-block wht-tble-bg">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="sub-title-block">
                        @if(isset($data["error"]))
                            <div class="">{{ $data["error"]["message"] }}</div>
                        @endif
                            <h2 class="sub-title pull-left">Vehicle List</h2>
                            <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                            <a href="{{ route('admin_show_vehicle_list') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                        </div>
                        <div class="vehical-list-block">
                        @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data["data"]) && count($data['data']) > 0)
                            
                                @foreach($data["data"] as $value) 
                                    @if($count == 0)
                                        @if(isset($value["created_at"]))
                                            @php                                             
                                                $first_created_date = $value["created_at"];
                                            @endphp
                                        @endif
                                        @php 
                                            $count++; 
                                        @endphp
                                    @endif
                                    
                                    @if(isset($value["created_at"]))
                                        @php
                                            $last_created_date = $value["created_at"];
                                        @endphp
                                    @endif
                                    
                                    <div class="vehical-list-contet-block m-b20">
                                        <a href="javascript:;" style="display:none;" class="site-button green pull-right vehical-edit-icon" data-toggle="modal" data-target="#add-vehical-list" data-tooltip="Edit"><i class="fa fa-edit"></i></a>
                                        <ul>
                                            <li>
                                                <p>
                                                    <span class="vTitle">Category : </span> 
                                                    <span class="vName">{{isset($value["vehicle_category"]["category_name"]) ? $value["vehicle_category"]["category_name"] : '' }}</span>
                                                </p>
                                                <p>
                                                    <span class="vTitle">Sub Category : </span> 
                                                    <span class="vName">{{isset($value["vehicle_sub_category"]["sub_category_name"]) ? $value["vehicle_sub_category"]["sub_category_name"] : '' }}</span>
                                                </p>
                                                <p>
                                                    <span class="vTitle">Vehicle No : </span> 
                                                    <span class="vName">{{isset($value["vehicle_rc_number"]) ? $value["vehicle_rc_number"] : '' }}</span>
                                                </p>
                                                <p>
                                                    <span class="vTitle">Per MT/KM Rate : </span> 
                                                    <span class="vName"><i class="fa fa-rupee"></i>{{isset($value["per_metric_ton_per_km_rate"]) ? $value["per_metric_ton_per_km_rate"] : '' }}</span>
                                                </p>
                                                <p>
                                                    <span class="vTitle">Min Trip Price : </span> 
                                                    <span class="vName"><i class="fa fa-rupee"></i>{{isset($value["min_trip_price"]) ? $value["min_trip_price"] : '' }}</span>
                                                </p>
                                            </li>
                                            <li>
                                                <p>
                                                    <span class="vTitle">Make : </span> 
                                                    <span class="vName">{{isset($value["manufacturer_name"]) ? $value["manufacturer_name"] : '' }}</span>
                                                </p>
                                                <p>
                                                    <span class="vTitle">Type Model : </span> 
                                                    <span class="vName">{{isset($value["vehicle_model"]) ? $value["vehicle_model"] : '' }}</span>
                                                </p>
                                                <p>
                                                    <span class="vTitle">Manufacture Year : </span> 
                                                    <span class="vName">{{isset($value["manufacture_year"]) ? $value["manufacture_year"] : '' }}</span>
                                                </p>
                                                 <p>
                                                    <span class="vTitle">Min Load Capacity (MT) :</span> 
                                                    <span class="vName">10 MT</span>
                                                </p>
                                                 <p>
                                                    <span class="vTitle">Max Load Capacity (MT) :</span> 
                                                    <span class="vName">30 MT</span>
                                                </p>
                                                
                                               
                                            </li>
                                            <li>
                                                 <p>
                                                    <span class="vTitle">Delivery Range : </span> 
                                                    <span class="vName">{{isset($value["delivery_range"]) ? $value["delivery_range"] : '' }}</span>
                                                </p>
                                                <p><span class="vTitle">Driver 1 : </span> <span class="vName">John</span></p>
                                                <p><span class="vTitle">Driver 2 : </span> <span class="vName">John John</span></p>
                                                <p>
                                                    <span class="vTitle">Available : </span> 
                                                    <span class="vName">{{isset($value["is_active"]) ? ($value["is_active"] ? 'Yes' : 'No') : '' }}</span>
                                                </p>
                                                <p>
                                                    <span class="vTitle">Is Insurance Active : </span> 
                                                    <span class="vName">{{isset($value["is_insurance_active"]) ? ($value["is_insurance_active"] ? 'Yes' : 'No') : '' }}</span>
                                                </p>
                                               
                                            </li>
                                              <li>
                                <p><span class="vTitle">Address : </span> 
                                     <p>
                                                  
                                                  <span class="vName">
                                    {{ isset($value["line1"]) ? $value["line1"] : '' }},
                                    {{ isset($value["line2"]) ? $value["line2"] : '' }},
                                    {{ isset($value["city_name"]) ? $value["city_name"] : '' }} - {{ isset($value["pincode"]) ? $value["pincode"] : '' }},
                                    {{ isset($value["state_name"]) ? $value["state_name"] : '' }},
                                    </span>
                                                </p>
                                </p>



                            </li>
                                  
                                        </ul>
                                           <ul class="mt-3 vehicle_gallary">
                            <li>
                                <div id="gallery">
                                    @if(isset($value["rc_book_image_url"]))
                                        <a href="{{ $value['rc_book_image_url'] }}" class="vehicle-img-block gal_link"><img src="{{ $value['rc_book_image_url'] }}"></a>
                                    @endif

                                    @if(isset($value["insurance_image_url"]))
                                        <a href="{{ $value['insurance_image_url'] }}" class="vehicle-img-block gal_link"><img src="{{ $value['insurance_image_url'] }}"></a>
                                    @endif
                                    
                                    @if(isset($value["vehicle_image_url"]))
                                        <a href="{{ $value['vehicle_image_url'] }}" class="vehicle-img-block gal_link"><img src="{{ $value['vehicle_image_url'] }}"></a>
                                    @endif
                                </div>
                            </li>
                        </ul>
                                    </div>
                                @endforeach
                            @else
                               <p style="text-align: center;">No Record Found</p>
                            @endif
                        
                            <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif
                            
                            <div class="pagination-block">
                                <!-- <ul class="pagination justify-content-end m-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                    </li>
                                </ul>
                                <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                            
                                <div class="pagination justify-content-end m-0">
                                @if($is_previous_avail == 1)
                                    @if(isset($first_created_date))
                                    <form action="{{ route(Route::current()->getName()) }}" method="get">
                                        @if(Request::get('vehicle_category_id'))
                                            <input type="hidden" name="vehicle_category_id" value="{{ Request::get('vehicle_category_id') ? Request::get('vehicle_category_id') : ''  }}" class="form-control" placeholder="Name">
                                        @endif
                                        @if(Request::get('vehicle_sub_category_id'))
                                            <input type="hidden" name="vehicle_sub_category_id" value="{{ Request::get('vehicle_sub_category_id') ? Request::get('vehicle_sub_category_id') : ''  }}" class="form-control" placeholder="Mobile No">
                                        @endif
                                        @if(Request::get('is_gps_enabled'))
                                            <input type="hidden" name="is_gps_enabled" value="{{ Request::get('is_gps_enabled') ? Request::get('is_gps_enabled') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        @if(Request::get('is_insurance_active'))
                                            <input type="hidden" name="is_insurance_active" value="{{ Request::get('is_insurance_active') ? Request::get('is_insurance_active') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                        <button type="submit" class="site-button">Previous</button>                                    
                                    </form>
                                    @endif
                                    @endif
                                    @if($is_next_avail == 1)
                                    @if(isset($last_created_date))
                                    <form action="{{ route(Route::current()->getName()) }}" method="get">
                                        @if(Request::get('vehicle_category_id'))
                                            <input type="hidden" name="vehicle_category_id" value="{{ Request::get('vehicle_category_id') ? Request::get('vehicle_category_id') : ''  }}" class="form-control" placeholder="Name">
                                        @endif
                                        @if(Request::get('vehicle_sub_category_id'))
                                            <input type="hidden" name="vehicle_sub_category_id" value="{{ Request::get('vehicle_sub_category_id') ? Request::get('vehicle_sub_category_id') : ''  }}" class="form-control" placeholder="Mobile No">
                                        @endif
                                        @if(Request::get('is_gps_enabled'))
                                            <input type="hidden" name="is_gps_enabled" value="{{ Request::get('is_gps_enabled') ? Request::get('is_gps_enabled') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        @if(Request::get('is_insurance_active'))
                                            <input type="hidden" name="is_insurance_active" value="{{ Request::get('is_insurance_active') ? Request::get('is_insurance_active') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                        <button type="submit" class="site-button ml-2">Next</button>
                                    </form>
                                    @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--vehicle category list end -->
        </div>

@endsection