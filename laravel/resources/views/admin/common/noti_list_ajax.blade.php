<?php use App\Http\Controllers\Admin\AdminController;?>
<?php 

    $notification_types = array(

        // 1 => "Dear, {{NAME}} Truck has been assigned to your order of id #{{ORDER_ID}}", 
        1 => "Hi Admin, an order has been placed with an order Id #{{ORDER_ID}} by {{CLIENT_COMPANY_NAME}}.", //orderCreate
        //2 => "Order with id #{{ORDER_ID}} has been accepted.", //orderAccept
        2 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has accepted & confirmed the product id #{{ORDER_ITEM}} having Order Id #{{ORDER_ID}} placed by {{CLIENT_COMPANY_NAME}}.", //orderRejected
        //3 => "Dear, {{NAME}} Order with #{{ORDER_ID}} has been confirmed.", //orderCreate
        3 => "Hi Admin, an order request with the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by Client {{CLIENT_COMPANY_NAME}} has been rejected by RMC Supplier {{SUPPLIER_NAME}}.", //truckAssigned
        4 => "Dear, {{NAME}} Your Order with #{{ORDER_ID}} has been picked up.", //orderPickup
        5 => "Hi Admin, Transit mixer has been assigned by RMC Supplier {{SUPPLIER_NAME}} for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by client {{CLIENT_COMPANY_NAME}}. ", //orderPickup
        6 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has picked up the product id #{{ORDER_ITEM}}, for client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}.", //orderReject
        7 => "Hi Admin,  your client {{CLIENT_COMPANY_NAME}} has rejected the product id #{{ORDER_ITEM}} supplied by {{SUPPLIER_NAME}} having an order id #{{ORDER_ID}}", //orderDelay
        8 => "Order with #{{ORDER_ID}} has been delivered partially.", //orderDelay
        9 => "Hi Admin, the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}} has been delayed by 'nos. of hours' owing to {{REASON_FOR_DELAY}}.", //orderDelivered
        10 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has partially delivered  the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}", //orderDelivered
        11 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has fully delivered  the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}", //orderDelivered
        12 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has fully delivered  the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}", //orderDelivered
        13 => "Hi Admin, your client {{CLIENT_COMPANY_NAME}} has cancelled the product id #{{ORDER_ITEM}}, which was to be supplied by the RMC Supplier {{SUPPLIER_NAME}}, having an order id #{{ORDER_ID}} Client has thus partially cancelled the order.", //orderDelivered
        14 => "Hi Admin, your client {{CLIENT_COMPANY_NAME}} has fully cancelled the order with an order id #{{ORDER_ID}}", //orderDelivered
        15 => "Hi Admin, supply of product id #{{ORDER_ITEM}}, for your client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}} has been shortclosed by RMC Supplier {{SUPPLIER_NAME}} as the balance quantity is less than 3 cum.", //orderDelivered
        16 => "Hi Admin, CP has been assigned by RMC Supplier {{SUPPLIER_NAME}} for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by client {{CLIENT_COMPANY_NAME}}.", //orderDelivered
        17 => "Hi Admin, CP has been delayed for the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}} by 'nos. of hours' owing to {{REASON_FOR_DELAY}}.", //orderDelivered
        18 => "Hi Admin, {{CLIENT_COMPANY_NAME}} has created a support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        19 => "Hi Admin, {{SUPPLIER_NAME}} has created a support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        20 => "Hi Admin, {{CLIENT_COMPANY_NAME}} has replied to your support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        21 => "Hi Admin, {{SUPPLIER_NAME}} has replied to your support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        22 => "Hi Admin, {{SUPPLIER_NAME}} has applied for registration and its application is due for verification by Admin. Kindly verify the same within 5 working days.", //orderDelivered
        //23 => "Hi Admin, {{SUPPLIER_NAME}} has applied for registration and its application is due for verification by Admin. Kindly verify the same within 5 working days.", //orderDelivered
        24 => "Hi Admin, Your client {{CLIENT_COMPANY_NAME}}'s order having Product Id #{{ORDER_ITEM}} for an Order ID #{{ORDER_ID}} has received a requested for acceptance to reassign his order by the RMC Supplier {{SUPPLIER_NAME}}. Your client shall accept or may reject the reassign request within 24 hours, failing to which the affected part of such Product of that Order will get cancelled.", //orderDelivered
        25 => "Hi Admin, Your client {{CLIENT_COMPANY_NAME}} has accepted the request of reassigning his order for product having Product Id #{{ORDER_ITEM}} for an Order having an Order Id #{{ORDER_ID}}. This request was sent by the RMC Supplier {{SUPPLIER_NAME}}", //orderDelivered
        26 => "Hi Admin, Your client {{CLIENT_COMPANY_NAME}} has rejected the request of reassigning his order for product having Product Id #{{ORDER_ITEM}} for an Order having an Order Id #{{ORDER_ID}}. This request was sent by the RMC Supplier {{SUPPLIER_NAME}}", //orderDelivered
        27 => "Hi Admin, Your client {{CLIENT_COMPANY_NAME}} has accepted the 7 days cube test report, for its Product Id #{{ORDER_ITEM}} having an Order ID #{{ORDER_ID}},  uploaded by the RMC Supplier {{SUPPLIER_NAME}}", //orderDelivered
        28 => "Hi Admin, Your client {{CLIENT_COMPANY_NAME}}'s order with Product Id #{{ORDER_ITEM}} having an Order ID #{{ORDER_ID}} has rejected the 7 days cube test report uploaded by the RMC Supplier {{SUPPLIER_NAME}} and have uploaded his own 7 days cube test report or put his remarks. Conmix shall not make the due payment of the RMC Supplier if his 28 days Cube Test Report is also rejected by the Buyer.", //orderDelivered
        29 => "Hi Admin, Your client {{CLIENT_COMPANY_NAME}} has accepted the 28 days cube test report, for its Product Id #{{ORDER_ITEM}} having an Order ID #{{ORDER_ID}},  uploaded by the RMC Supplier {{SUPPLIER_NAME}}", //orderDelivered
        30 => "Hi Admin, Your client {{CLIENT_COMPANY_NAME}}'s order with Product Id #{{ORDER_ITEM}} having an Order ID #{{ORDER_ID}} has rejected the 28 days cube test report uploaded by the RMC Supplier {{SUPPLIER_NAME}} and have uploaded his own 28 days cube test report or put his remarks. Conmix shall not make the due payment of the RMC Supplier for the said Product/Order", //orderDelivered

    );

    $notification_types_name = array(

        1 => "Order Placed",
        //2 => "Order Process" ,
        2 => "Order Accept" ,
        3 => "Order Rejected" ,
        4 => "Order Confirm",
        5 => "TM Assigned",
        6 => "Order Pickup",
        7 => "Order Product Reject",
        8 => "Order Reject",
        9 => "Order Delay",
        10 => "Partially Product Delivered",
        11 => "Fully Product Delivered",
        12 => "Order Delivered",
        13 => "Partially Order Cancelled",
        14 => "Order Cancelled",
        15 => "Order Short Close",
        16 => "CP Assigned",
        17 => "CP Delay",
        18 => "Create Support Ticket By Buyer",
        19 => "Create Support Ticket By Vendor",
        20 => "Reply Of Support Ticket By Buyer",
        21 => "Reply Of Support Ticket By Vendor",
        22 => "Vendor Register",
        23 => "Reminder For Vendor Verification",
        24 => "Order Reassigning Request Sent By Supplier",
        25 => "Order Reassigning Request Accepted By Buyer",
        26 => "Order Reassigning Request Rejected By Buyer",
        27 => "7 Days Cube Test Report Accepted By Buyer",
        28 => "7 Days Cube Test Report Rejected By Buyer",
        29 => "28 Days Cube Test Report Accepted By Buyer",
        30 => "28 Days Cube Test Report Rejected By Buyer",

    );

?>
<div class="more_header p-3 float-left w-100">
    <span class="float-left" id="new_unseen_msg">
        @if($admin_noti_count_data["data"]["unseen_message_count"] > 0)
            You have {{ isset($admin_noti_count_data["data"]["unseen_message_count"]) ? $admin_noti_count_data["data"]["unseen_message_count"] : 0 }} new notifications
        @else
            Notifications
        @endif
        </span>
</div>
@if(isset($data["data"]["notifications"]) && count($data["data"]["notifications"]) > 0)
<div id="noti_list_div">
    <!-- <div class="overlay">
        <span class="spinner"></span>
    </div> -->


    <ul class="mCustomScrollbar list-unstyled float-left w-100 m-0">
        <?php //dd($data["data"]["notifications"]); ?>
        
            @foreach($data["data"]["notifications"] as $value)
            
                <?php 
                    $msg = "";
                    $is_order = 1;
                    $msg = $notification_types[$value["notification_type"]];

                    if($value["notification_type"] == 22){
                        $is_order = 2;
                    }

                    if(isset($value["to_user"]["full_name"])){
                        $msg = str_replace("{{NAME}}",$value["to_user"]["full_name"],$msg);
                    }
                    
                    if(isset($value["order"]["display_id"])){
                        $msg = str_replace("{{ORDER_ID}}",$value["order"]['display_id'],$msg);
                    }

                    if(isset($value["vendor"]["full_name"])){
                        $msg = str_replace("{{SUPPLIER_NAME}}",$value["vendor"]["full_name"],$msg);
                    }
                    
                    if(isset($value["order_item"]["display_item_id"])){
                        $msg = str_replace("{{ORDER_ITEM}}",$value["order_item"]["display_item_id"],$msg);
                    }
                    
                    if(isset($value["support_ticket"]["ticket_id"])){
                        $is_order = 0;
                        $msg = str_replace("{{TICKET_NO}}",$value["support_ticket"]["ticket_id"],$msg);
                    }
                    
                    if(isset($value["client"])){

                        // $account_type = $value["client"]["account_type"];

                        // $client_name = "";

                        // if($account_type == "Individual"){

                            $client_name = $value["client"]["full_name"];

                        // }else{

                        //     $client_name = $value["client"]["company_name"];

                        // }

                        $msg = str_replace("{{CLIENT_COMPANY_NAME}}",$client_name,$msg);
                        
                    }

                    // $created = date('d M Y', strtotime($value["created_at"]));

                    $created = AdminController::dateTimeFormat($value["created_at"]);
                    
                ?>

                <li class="float-left w-100" style="<?php echo $value['is_seen'] == 0 ? 'background: #e8f1fb;' : '' ?>">
                    <a href="{{($is_order == 1) ? route('admin_show_order_details',$value['order']['_id']) : ($is_order == 1 ? route('admin_show_support_ticket_details',$value['support_ticket']['ticket_id']) : '') }}" onclick="updateNotiStatus('{{ $value['_id'] }}')" class="float-left w-100">                                        
                        <i class="fa fa-info-circle"></i> {{$msg}} {{$created}}                                        
                    </a>
                </li>
            @endforeach
        
        
    </ul>

    <div class="notification_footer text-center float-left w-100">
        <a href="{{ route('admin_notifications') }}" class="view_all position-relative float-left w-100 text-center">See all Notifications</a>
    </div>
</div>
    

@endif