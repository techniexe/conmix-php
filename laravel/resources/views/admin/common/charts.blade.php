@if((Route::current()->getName() == 'admin_dashboard') )

<script>

$('.from').datepicker({
	    autoclose: true,
	    minViewMode: 1,
	    format: 'M yyyy'
		}).on('changeDate', function(selected){
	        startDate = new Date(selected.date.valueOf());
	        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});

var month_array = {

    "01" : 0,
    "02" : 1,
    "03" : 2,
    "04" : 3,
    "05" : 4,
    "06" : 5,
    "07" : 6,
    "08" : 7,
    "09" : 8,
    "10" : 9,
    "11" : 10,
    "12" : 11,

};

var all_month_array = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


var current_date = new Date();
var monthly_chart_selected_year = current_date.getFullYear();

var monthly_chart_user = "Buyer";

var daily_chart_selected_year = all_month_array[current_date.getMonth()] + ", "+ current_date.getFullYear();

var daily_chart_user = "Buyer";


function setDataInChart(order_data, sales_amount_data, cancel_order_data, div_id, title, categories,selected_year,order_status){

    var options = {
        chart: {
        fontFamily: 'Roboto',
        height: 350,
        type: 'bar',
        zoom: {
            enabled: false
        },
        },
        dataLabels: {
        enabled: false
        },
        stroke: {
        width: [0.1, 0.1, 0.1],
        curve: 'straight',
        dashArray: [0, 0, 0]
        },
        series: [{
            name: "Orders",
            data: order_data
        },
        {
            name: "Cancel Orders",
            data: cancel_order_data
        },
        {
            name: 'Sales Amount',
            data: sales_amount_data
        }
        ],
        title: {
        text: ''+title,
        align: 'center'
        },
        markers: {
        size: 0,
        hover: {
            sizeOffset: 5
        }
        },
        xaxis: {
        categories: categories,
        title: {
            text: ''
        }
        },
        yaxis: {
        title: {
            text: ''
        }
        },
        tooltip: {
        x: {
            show: true,            
            formatter: function (val) {
            // console.log(title);
            // console.log("val..."+val+"..."+selected_year+"..."+order_status);
            var mystatus = "";
            if(order_status.length > 0){
                mystatus = " | "+order_status;
            }
            return val + " " + selected_year + " " + mystatus;
            }
        

        },
        y: [{
            title: {
            formatter: function (val) {
                // console.log("val..."+val);
                return val + ""
            }
            }
        }, {
            title: {
            formatter: function (val) {
                // console.log("val..."+val);
                return val + ""
            }
            }
        }, {
            title: {
            formatter: function (val) {
                // console.log("val..."+val);
                return val;
            }
            }
        }]
        },
        grid: {
        borderColor: '#d3e0e9',
        },
        colors: ['#008ffb', '#00e396', '#feb019'],
        legend: {
        show: false
        }
    }
    
    // var chart = new ApexCharts(
    //   document.querySelector("#"+div_id),
    //   options

    // );
    // chart.render();

    return options;
}


var monthly_cat = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
var daily_cat = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];


var monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
var monthly_cancel_data = [0,0,0,0,0,0,0,0,0,0,0,0];

var daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var daily_cancel_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];


var monthly_chart_option = setDataInChart(monthly_order_data, monthly_sales_data, monthly_cancel_data, 'monthly-report-chart', "Monthly Average", monthly_cat,monthly_chart_selected_year,monthly_chart_user);

var monthly_chart = new ApexCharts(
    document.querySelector("#monthly-report-chart"),
    monthly_chart_option

  );
  monthly_chart.render();

  checkMonthlyChartLegends();

  function checkMonthlyChartLegends() {
    var allLegends = document.querySelectorAll("#monthly_chart input[type='checkbox']")

    for(var i = 0; i < allLegends.length; i++) {
      if(!allLegends[i].checked) {
        console.log(allLegends[i].value);
        monthly_chart.toggleSeries(allLegends[i].value);
      }
    }
  }

  // toggleSeries accepts a single argument which should match the series name you're trying to toggle
  function toggleMonthlyChartSeries(checkbox) {
    monthly_chart.toggleSeries(checkbox.value);
  }
  
  
var daily_chart_option = setDataInChart(daily_order_data, daily_sales_data, daily_cancel_data, 'daily-report-chart', "Daily Average",daily_cat,daily_chart_selected_year,daily_chart_user);

var daily_chart = new ApexCharts(
    document.querySelector("#daily-report-chart"),
    daily_chart_option

  );
  daily_chart.render();

  checkDailyChartLegends();

  function checkDailyChartLegends() {
    var allLegends = document.querySelectorAll("#daily_chart_legend input[type='checkbox']")

    for(var i = 0; i < allLegends.length; i++) {
      if(!allLegends[i].checked) {
        console.log(allLegends[i].value);
        daily_chart.toggleSeries(allLegends[i].value);
      }
    }
  }

  // toggleSeries accepts a single argument which should match the series name you're trying to toggle
  function toggleDailyChartSeries(checkbox) {
    daily_chart.toggleSeries(checkbox.value);
  }


  function getMonthlyChartDetails(year,user_type){

        if(year != undefined){
            monthly_chart_selected_year = year;
        }

        if(user_type != undefined){
            monthly_chart_user = user_type.charAt(0).toUpperCase() + user_type.slice(1);
        }else{
            monthly_chart_user = 'Buyer';
        }


        $("#monthly_chart_loader").fadeIn(300);

        var request_data = {
            'year': year,
            'user_type': user_type
        };
        

        customResponseHandler(
            "admin/getMonthlyChartData", // Ajax URl
            'GET', // Method call
            request_data, // Request data
            function success(data){ // onSuccess

                // data = JSON.parse(data);
                console.log(data["data"]);
                
                $.each(data["data"]["TotalorderData"], function(key,value){

                    console.log(value);
                    if(value["_id"] != undefined){
                      var month = value["_id"].split("-")[1];
                    }
                    
                    if(value["month"] != undefined){
                      var month = value["month"];

                      if(month != 10 && month != 11 && month != 12){
                          month = '0'+month;
                      }
                    }

                    month = month_array[month];
                    monthly_order_data[month] = value["count"];
                    monthly_sales_data[month] = Math.round(value["sum"]);

                });
                
                $.each(data["data"]["CancelorderData"], function(key,value){

                    console.log(value);
                    if(value["_id"] != undefined){
                      var month = value["_id"].split("-")[1];
                    }
                    
                    if(value["month"] != undefined){
                      var month = value["month"];
                    }

                    month = month_array[month];
                    monthly_cancel_data[month] = value["count"];

                });

                if(data["data"]["TotalorderData"].length == 0){
                    monthly_order_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                    monthly_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                    monthly_cancel_data = [0,0,0,0,0,0,0,0,0,0,0,0];
                }

                $('#monthly_chart_order').prop('checked',true);
                $('#monthly_chart_quntity').prop('checked',true);
                $('#monthly_chart_salse').prop('checked',true);
                
                monthly_chart_option = setDataInChart(monthly_order_data, monthly_sales_data, monthly_cancel_data, 'monthly-report-chart', "Monthly Average", monthly_cat,monthly_chart_selected_year,monthly_chart_user);
                
                monthly_chart.updateOptions(monthly_chart_option,false,true);
                
                $("#monthly_chart_loader").fadeOut(300);
                
                

            }
        );

}


getMonthlyChartDetails();

$("#monthly_chart_data_year_filter").change(function() {
var optionSelected = $("option:selected", this);
var valueSelected = this.value;

var monthly_chart_user_filter = $("#monthly_order_data_user_filter").val();

// console.log("valueSelected..."+valueSelected+"..."+monthly_chart_status);

getMonthlyChartDetails(valueSelected,monthly_chart_user_filter);

});



$("#monthly_order_data_user_filter").change(function() {
var optionSelected = $("option:selected", this);
var valueSelected = this.value;

var monthly_chart_year = $("#monthly_chart_data_year_filter").val();

console.log("valueSelected..."+valueSelected+"..."+monthly_chart_year);

getMonthlyChartDetails(monthly_chart_year,valueSelected);

});


function getDailyChartDetails(year,month,user_type){

  if(year != undefined){
    daily_chart_selected_year = month+ ", "+year;
  }

  if(user_type != undefined){
    daily_chart_user = user_type.charAt(0).toUpperCase() + user_type.slice(1);;
  }

  $("#daily_chart_loader").fadeIn(300);

  var request_data = {
      'year': year,
      'month': month,
      'user_type': user_type
    };
    
  customResponseHandler(
      "admin/getDailyChartData", // Ajax URl
      'GET', // Method call
      request_data, // Request data
      function success(data){ // onSuccess

          // data = JSON.parse(data);
          console.log(data["data"]);
          $.each(data["data"]["TotalorderData"], function(key,value){

            console.log(value);
            if(value["_id"] != undefined){
              var day = value["_id"]["day"];
            }
            
            if(value["day"] != undefined){
              var day = value["day"];
            }

            if(day > 0){
                day = day -1;
            }
            console.log("day1..."+day);
            daily_order_data[day] = value["count"];
            daily_sales_data[day] = Math.round(value["total_amount"]);

          });
          
          $.each(data["data"]["CancelorderData"], function(key,value){

            console.log(value);
            if(value["_id"] != undefined){
              var day = value["_id"]["day"];
            }
            
            if(value["day"] != undefined){
              var day = value["day"];
            }

            if(day > 0){
                day = day -1;
            }
            // console.log("day2..."+day);
            daily_cancel_data[day] = Math.round(value["count"]);

          });

          if(data["data"]["TotalorderData"].length == 0){
            daily_order_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            daily_sales_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            daily_cancel_data = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
          }

          $('#daily_chart_legend_order').prop('checked',true);
            $('#daily_chart_legend_quantity').prop('checked',true);
            $('#daily_chart_legend_sales').prop('checked',true);
            // console.log(daily_sales_data);
          daily_chart_option = setDataInChart(daily_order_data, daily_sales_data, daily_cancel_data, 'daily-report-chart', "Daily Average",daily_cat,daily_chart_selected_year,daily_chart_user);
          
          daily_chart.updateOptions(daily_chart_option,false,true);
          
          $("#daily_chart_loader").fadeOut(300);

      }
  );

}


getDailyChartDetails();



var daily_month;
var daily_year;
$("#daily_chart_year_filter").datepicker().on('changeMonth', function(e){ 
 daily_month = new Date(e.date).getMonth() + 1;
 daily_month = all_month_array[daily_month-1];
 daily_year = String(e.date).split(" ")[3];
 console.log("daily_year..."+daily_year+"..."+daily_month);

 var monthly_chart_user_type = $("#daily_order_data_user_filter").val();

 getDailyChartDetails(daily_year,daily_month, monthly_chart_user_type);
});



$("#daily_order_data_user_filter").change(function() {
var optionSelected = $("option:selected", this);
var valueSelected = this.value;

// var monthly_chart_year = $("#daily_chart_year_filter").val();
// var monthly_chart_month = $("#daily_chart_month_filter").val();

console.log("valueSelected..."+valueSelected+"..."+daily_month+"..."+daily_year);

getDailyChartDetails(daily_year,daily_month, valueSelected);

});




</script>





@endif