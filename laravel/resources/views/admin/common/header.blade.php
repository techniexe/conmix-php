<?php use App\Http\Controllers\Admin\AdminController;?>
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn btn-dark toggle_btn">
                <i class="fa fa-bars"></i>
            </button>
            <div class="btn btn-dark toggle_btn" style="margin-left:10px;">
                <?php echo AdminController::ADMIN_ROLES[$profile["admin_type"]] ?>
            </div>
            <div class="user_block">
            <div class="notification float-left position-relative">
                    <a href="javascript:;" onclick="notificationClick()" class="notificationClick">
                        <i class="fa fa-bell"></i>
                        @if(isset($admin_noti_count_data["data"]["unseen_message_count"]) && $admin_noti_count_data["data"]["unseen_message_count"] > 0)
                            <span class="note_badge position-absolute" id="noti_count">
                                {{ isset($admin_noti_count_data["data"]["unseen_message_count"]) ? ($admin_noti_count_data["data"]["unseen_message_count"] > 9 ? '9+' : $admin_noti_count_data["data"]["unseen_message_count"]) : 0 }}
                            </span>
                        @endif
                    </a>

                    <div class="more_notification position-absolute pullDown" id="noti_main_div">
                        <div class="more_header p-3 float-left w-100">
                            <span class="float-left">
                                @if(isset($admin_noti_count_data["data"]))
                                @if(count($admin_noti_count_data["data"]) > 0)
                                    You have {{ isset($admin_noti_count_data["data"]["unseen_message_count"]) ? $admin_noti_count_data["data"]["unseen_message_count"] : 0 }} new notifications
                                @else
                                    No Notification Found
                                @endif
                                @endif
                                </span>
                        </div>
                        @if(isset($admin_noti_count_data["data"]) && count($admin_noti_count_data["data"]) > 0)
                            <div id="noti_list_div">
                                <div class="overlay">
                                    <span class="spinner"></span>
                                </div>

                            </div>
                        
                            <div class="notification_footer text-center float-left w-100">
                                <a href="{{ route('admin_notifications') }}" class="view_all position-relative float-left w-100 text-center">See all Notifications</a>
                            </div>
                        @endif
                        
                    </div>
                </div>

                <div class="user_icon float-left">
                    <a href="javascript:;" class="userdropdownClick">
                        @php
                            $profile = session('profile_details', null);
                            //dd($profile);
                        @endphp
                        <input type="hidden" id="OTP_mob_number" value="{{ isset($profile["mobile_number"]) ? $profile["mobile_number"] : '' }}"/>
                        <img src="{{asset('assets/admin/images/user-img.webp')}}">
                        <span class="float-left">
                            {{ isset($profile["full_name"]) ? $profile["full_name"] : 'Admin' }}
                        </span>
                    </a>
                    <ul class="user_menu m-0 pullDown">
                        <!--<li><a href="#"><i class="fa fa-cog"></i>Setting</a></li>-->
                        <li><a href="{{ route('admin_show_my_profile') }}"><i class="fa fa-user"></i>My Profile</a></li>

                        <li><a href="{{ url('logout') }}"><i class="fa fa-power-off"></i>Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>