@if (Route::current()->getName() == 'admin_show_user')
<!-- Administrator Listing -->

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{route(Route::current()->getName())}}" method="get" name="add_user_filter_form">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="mobile_no" id="mobile_or_email" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="cstm-select-box">
                            <select name="admin_type">
                                <!-- <option value="" {{ Request::get('admin_type') === '' ? 'selected' : ''  }}>Select Admin Type</option>
                                <option value="admin" {{ Request::get('admin_type') === 'admin' ? 'selected' : ''  }}>Admin</option>
                                <option value="supplier_admin" {{ Request::get('admin_type') === 'supplier_admin' ? 'selected' : ''  }}>Supplier Admin</option>
                                <option value="buyer_admin" {{ Request::get('admin_type') === 'buyer_admin' ? 'selected' : ''  }}>Buyer Admin</option>
                                <option value="logistics_admin" {{ Request::get('admin_type') === 'sales' ? 'selected' : ''  }}>Logistics Admin</option>
                                <option value="sales" {{ Request::get('admin_type') === 'sales' ? 'selected' : ''  }}>Sales</option>
                                <option value="support" {{ Request::get('admin_type') === 'support' ? 'selected' : ''  }}>Support</option>
                                <option value="account_manager" {{ Request::get('admin_type') === 'account_manager' ? 'selected' : ''  }}>Account Manager</option> -->

                                <option value="admin_manager" {{ Request::get('admin_type') === 'admin_manager' ? 'selected' : ''  }}>Admin manager</option>
                                <option value="admin_marketing" {{ Request::get('admin_type') === 'admin_marketing' ? 'selected' : ''  }}>Marketing Admin</option>
                                <option value="admin_sales" {{ Request::get('admin_type') === 'admin_sales' ? 'selected' : ''  }}>Sales Admin</option>
                                
                                <option value="admin_accounts" {{ Request::get('admin_type') === 'admin_accounts' ? 'selected' : ''  }}>Account Admin</option>
                                <option value="admin_customer_care" {{ Request::get('admin_type') === 'admin_customer_care' ? 'selected' : ''  }}>Customer Care Admin</option>
                                <option value="admin_grievances" {{ Request::get('admin_type') === 'admin_grievances' ? 'selected' : ''  }}>Grievances Admin</option>



                                <!-- <option value="sales" {{ Request::get('admin_type') === 'sales' ? 'selected' : ''  }} >Sales</option>
                                <option value="support" {{ Request::get('admin_type') === 'support' ? 'selected' : ''  }} >Support</option>
                                <option value="account_manager" {{ Request::get('admin_type') === 'account_manager' ? 'selected' : ''  }}>Admin</option> -->
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="cstm-select-box">
                            <select name="is_active">
                                <option value="" {{ Request::get('is_active') === '' ? 'selected' : ''  }}>Select Status</option>
                                <option value="true" {{ Request::get('is_active') === 'true' ? 'selected' : ''  }}>Active</option>
                                <option value="false" {{ Request::get('is_active') === 'false' ? 'selected' : ''  }}>In Active</option>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                    <input type="submit" name="admin_filter_submit" value="Search" class="site-button"/>
                        <!-- <button class="site-button">Search</button> -->
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

<!-- Admin User Listing Over -->
@endif

<!-- Logistics User Listing -->

@if (
    (Route::current()->getName() == 'admin_show_logistics_user')
        || (Route::current()->getName() == 'admin_show_buyers_user')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" method="get">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" placeholder="Company Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Full Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="User Id">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="mobile_no" id="mobile_or_email" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="email" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="account_type">
                            <option disabled="disabled" selected="selected">Select Account Type</option>
                            <!-- <option value="">Select Account Type</option> -->
                            <option value="Builder">Builder</option>
                            <option value="Contractor">Contractor</option>
                            <option value="Individual">Individual</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                <input type="submit" name="logistic_filter_submit" value="Search" class="site-button"/>
                    <!-- <button class="site-button">Search</button> -->
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if (
    (Route::current()->getName() == 'admin_show_suppliers_user')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" method="get">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Comapny Name">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="User Id">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="mobile_no" id="mobile_or_email" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="email" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="pan_number" value="{{ Request::get('pan_number') ? Request::get('pan_number') : ''  }}" class="form-control" placeholder="Pan Number">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select class="form-control" name="company_type">
                            <option disabled="disabled" selected="selected">Select Company Type</option>
                            <option value="Partnership">Partnership</option>
                            <option value="Proprietor">Proprietor</option>
                            <option value="LLP">LLP</option>
                            <option value="LTD">LTD.</option>
                            <option value="PVT LTD">PVT LTD</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="verified_by_admin">
                            <option value="" {{ Request::get('verified_by_admin') === '' ? 'selected' : ''  }}>Select verified or Not</option>
                            <option value="true" {{ Request::get('verified_by_admin') === 'true' ? 'selected' : ''  }}>Yes</option>
                            <option value="false" {{ Request::get('verified_by_admin') === 'false' ? 'selected' : ''  }}>No</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                <input type="submit" name="logistic_filter_submit" value="Search" class="site-button"/>
                    <!-- <button class="site-button">Search</button> -->
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif


@if (
    (Route::current()->getName() == 'admin_report_supplier_listing')
    )

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Comapny Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="User Id">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="mobile_no" id="mobile_or_email" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="email" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="pan_number" value="{{ Request::get('pan_number') ? Request::get('pan_number') : ''  }}" class="form-control" placeholder="Pan Number">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select class="form-control" name="company_type">
                            <option disabled="disabled" selected="selected">Select Company Type</option>
                            <option value="Partnership">Partnership</option>
                            <option value="Proprietor">Proprietor</option>
                            <option value="LLP">LLP</option>
                            <option value="LTD">LTD.</option>
                            <option value="PVT LTD">PVT LTD</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="verified_by_admin">
                            <option value="" {{ Request::get('verified_by_admin') === '' ? 'selected' : ''  }}>Select verified or Not</option>
                            <option value="true" {{ Request::get('verified_by_admin') === 'true' ? 'selected' : ''  }}>Yes</option>
                            <option value="false" {{ Request::get('verified_by_admin') === 'false' ? 'selected' : ''  }}>No</option>

                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                <input type="submit" name="logistic_filter_submit" value="Search" class="site-button"/>
                    <!-- <button class="site-button">Search</button> -->
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

<!-- Logistics User Listing Over -->

<!-- Vehicle Module Start -->
@if(Route::current()->getName() == 'admin_show_vehicle_list_ddd')
<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
          @csrf
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                      <select name="vehicle_category_id">
                          <option disabled="disabled" selected="selected">Select Vehicle Category</option>
                          @if(isset($data["vehicle_categoies"]["data"]))
                            @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                              <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == Request::get("vehicle_category_id") ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                            @endforeach
                          @endif
                      </select>
                    </div>
                </div>


            </div>

            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="vehicle_sub_category_id">
                        <option disabled="disabled" selected="selected">Select Vehicle Sub Category</option>
                        @if(isset($data["subcategories"]["data"]))
                          @foreach($data["subcategories"]["data"] as $cat_value)
                            <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == Request::get("vehicle_sub_category_id") ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                          @endforeach
                        @endif
                    </select>
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="is_gps_enabled">
                        <option disabled="disabled" selected="selected">Select GPS Enabled</option>
                        <option value="true" {{ Request::get("is_gps_enabled") == 'true' ? 'selected' : '' }}>Yes</option>
                        <option value="false" {{ Request::get("is_gps_enabled") == 'false' ? 'selected' : '' }}>No</option>
                    </select>
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="is_insurance_active">
                        <option disabled="disabled" selected="selected">Select Insurance Active</option>
                        <option value="true" {{ Request::get("is_insurance_active") == 'true' ? 'selected' : '' }}>Yes</option>
                        <option value="false" {{ Request::get("is_insurance_active") == 'false' ? 'selected' : '' }}>No</option>
                    </select>
                  </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

    @endif


@if(Route::current()->getName() == 'admin_show_coupon')
<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
          @csrf
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="code" value="{{ Request::get('code') ? Request::get('code') : ''  }}" class="form-control" placeholder="Code">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="discount_value" value="{{ Request::get('discount_value') ? Request::get('discount_value') : ''  }}" class="form-control" placeholder="Discount Value">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="daterange" value="01/01/2018 - 01/15/2018" />
                    <input type="hidden" id="access_log_start_date"  name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : ''  }}" />
                    <input type="hidden" id="access_log_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : ''  }}" />
                    
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="coupon_start_date_filter" name="start_date" class="form-control"  placeholder="Select Start Date">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="coupon_end_date_filter" name="end_date" class="form-control"  placeholder="Select End Date">
                    </div>
                </div>
            </div> -->
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="min_order_range_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="text" name="min_order" value="{{ Request::get('min_order') ? Request::get('min_order') : ''  }}" class="form-control" placeholder="Min Order">
                    </div>
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="min_order" value="{{ Request::get('min_order') ? Request::get('min_order') : ''  }}" class="form-control" placeholder="Min Order">
                </div>
            </div> -->
            <div class="form-group">
                <div class="two_fillter_box">
                <div class="cstm-select-box">
                        <select name="max_discount_range_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="text" name="max_discount" value="{{ Request::get('max_discount') ? Request::get('max_discount') : ''  }}" class="form-control" placeholder="Max Order">
                    </div>
                    
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="is_active">
                            <option value="" {{ Request::get('is_active') === '' ? 'selected' : ''  }}>Select Status</option>
                            <option value="true" {{ Request::get('is_active') === 'true' ? 'selected' : ''  }}>Active</option>
                            <option value="false" {{ Request::get('is_active') === 'false' ? 'selected' : ''  }}>In Active</option>

                        </select>
                    </div>
                </div>
            </div>
            

            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

<script>
$(function() {

    var today = new Date();
    var end = new Date();
    // today.setDate(today.getDate() + 2);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    end = dd + '/' + mm + '/' + yyyy;
    console.log(today);

    @if(Request::get('start_date'))

        var date = new Date('{{Request::get('start_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        today = dd + '/' + mm + '/' + yyyy;
        // today = yyyy + '-' + mm + '-' + dd;
        console.log("start_date..."+today);
    @endif
    
    @if(Request::get('end_date'))

        var date = new Date('{{Request::get('end_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        end = dd + '/' + mm + '/' + yyyy;
        // end = yyyy + '-' + mm + '-' + dd;
        console.log("end_date..."+end);
    @endif

  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
    startDate: today,
    endDate: end,
    locale: {
            format: 'DD/MM/YYYY'
        }
  }, function(start, end, label) {
      $("#access_log_start_date").val(start.format('YYYY-MM-DD'));
      $("#access_log_end_date").val(end.format('YYYY-MM-DD'));
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>

<!-- <script>
    $('#coupon_start_date_filter').datepicker({
	    autoclose: true,
	    format: 'dd M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('start_date'))

    var date = new Date('{{Request::get('start_date')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#coupon_start_date_filter").datepicker("setDate",new Date(yyyy,mm, dd, 1)).trigger('change');

@endif

$('#coupon_end_date_filter').datepicker({
	    autoclose: true,
	    format: 'dd M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('end_date'))

    var date = new Date('{{Request::get('end_date')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#coupon_end_date_filter").datepicker("setDate",new Date(yyyy,mm, dd, 1)).trigger('change');

@endif

</script> -->

    @endif
    
@if(Route::current()->getName() == 'admin_coupon_report')
<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
          @csrf
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="code" value="{{ Request::get('code') ? Request::get('code') : ''  }}" class="form-control" placeholder="Code">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="discount_value" value="{{ Request::get('discount_value') ? Request::get('discount_value') : ''  }}" class="form-control" placeholder="Discount Value">
                </div>
            </div>
            
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="min_order_range_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="text" name="min_order" value="{{ Request::get('min_order') ? Request::get('min_order') : ''  }}" class="form-control" placeholder="Min Order">
                    </div>
                </div>
            </div>
           
            <div class="form-group">
                <div class="two_fillter_box">
                <div class="cstm-select-box">
                        <select name="max_discount_range_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="text" name="max_discount" value="{{ Request::get('max_discount') ? Request::get('max_discount') : ''  }}" class="form-control" placeholder="Max Order">
                    </div>
                    
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="is_active">
                            <option value="" {{ Request::get('is_active') === '' ? 'selected' : ''  }}>Select Status</option>
                            <option value="true" {{ Request::get('is_active') === 'true' ? 'selected' : ''  }}>Active</option>
                            <option value="false" {{ Request::get('is_active') === 'false' ? 'selected' : ''  }}>In Active</option>

                        </select>
                    </div>
                </div>
            </div>
            
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->



@endif

    <!-- Vehicle Module over -->
    <!-- Region Module Start -->

@if(Route::current()->getName() == 'admin_show_region_country')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
                @csrf
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="country_name" value="{{ Request::get('country_name') ? Request::get('country_name') : '' }}" class="form-control" placeholder="Country Name">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <button class="site-button btn_bg_color">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName()== 'admin_show_region_states')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
                @csrf
                <!-- <div class="form-group">
                    <div class="input-group">
                        <input type="text" value="{{ Request::get('country_code') ? Request::get('country_code') : ''  }}" name="country_code" class="form-control" placeholder="Country Code">
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" value="{{ Request::get('state_name') ? Request::get('state_name') : ''  }}" name="state_name" class="form-control" placeholder="State Name">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_show_region_cities')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
        @csrf
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('country_code') ? Request::get('country_code') : ''  }}" name="country_code" class="form-control" placeholder="Country Code">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="city_name" value="{{ Request::get('city_name') ? Request::get('city_name') : ''  }}" class="form-control" placeholder="City Name">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group single-search-select2">
                    <label>State Name</label>
                    <select id="state_name_city_filter" name="state_id" class="form-control-chosen" data-placeholder="State Name">
                        <option></option>
                        @if(isset($data["states"]))
                            @foreach($data["states"] as $value)
                                <option value="{{ $value['_id'] }}" {{ Request::get("state_id") == $value['_id'] ? 'selected' : '' }}>{{ $value["state_name"] }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
              </div>

            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

    <!-- Region Module Over -->


<!-- Porduct Module Start -->

@if(Route::current()->getName() == 'admin_show_product_category')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
            @csrf

                <div class="form-group">
                    <div class="input-group">
                        <input type="text" value="{{ Request::get('search') ? Request::get('search') : ''  }}" name="search" class="form-control" placeholder="Product Category">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

@endif



@if(Route::current()->getName() == 'admin_show_product_sub_category')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
        @csrf
            @if(Request::get('categoryId') != null)
                <input type="hidden" value="{{ Request::get('categoryId') ? Request::get('categoryId') : '' }}" name="categoryId"/>
            @endif
            <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('search') ? Request::get('search') : ''  }}" name="search" class="form-control" placeholder="Product Sub Category">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group" id="">
                    <label>Margin Rate</label>
                    <div class="cstm-select-box">
                        <select name="margin_rate_id">
                            <option disabled="disabled" selected="selected">Select Margin Rate</option>
                            @if(isset($data["margin_rate_data"]))
                                @foreach($data["margin_rate_data"] as $key => $value)
                                <option value="{{ $value['_id'] }}" {{ Request::get('margin_rate_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["title"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_show_sand_source')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
        @csrf
            @if(Request::get('categoryId') != null)
                <input type="hidden" value="{{ Request::get('categoryId') ? Request::get('categoryId') : '' }}" name="categoryId"/>
            @endif

            <div class="form-group">
                <div class="input-group" id="">
                    <label>Select Region Serv</label>
                    <div class="cstm-select-box">
                        <select name="region_id">
                            <option disabled="disabled" selected="selected">Select Region</option>
                            @if(isset($state_data["data"]))
                                @foreach($state_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('region_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_sand_source_report')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
        @csrf

            <div class="form-group">
                <div class="input-group" id="">
                    <label>Select Region Serv</label>
                    <div class="cstm-select-box">
                        <select name="region_id">
                            <option disabled="disabled" selected="selected">Select Region</option>
                            @if(isset($state_data["data"]))
                                @foreach($state_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('region_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_show_flyash_source')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
        @csrf

            <div class="form-group">
                <div class="input-group" id="">
                    <label>Select Region Serv</label>
                    <div class="cstm-select-box">
                        <select name="region_id">
                            <option disabled="disabled" selected="selected">Select Region</option>
                            @if(isset($state_data["data"]))
                                @foreach($state_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('region_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_flyAsh_source_report')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
        @csrf

            <div class="form-group">
                <div class="input-group" id="">
                    <label>Select Region Serv</label>
                    <div class="cstm-select-box">
                        <select name="region_id">
                            <option disabled="disabled" selected="selected">Select Region</option>
                            @if(isset($state_data["data"]))
                                @foreach($state_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('region_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_show_aggregate_source')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
        @csrf

            <div class="form-group">
                <div class="input-group" id="">
                    <label>Select Region Serv</label>
                    <div class="cstm-select-box">
                        <select name="region_id">
                            <option disabled="disabled" selected="selected">Select Region</option>
                            @if(isset($state_data["data"]))
                                @foreach($state_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('region_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_agg_source_report')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
        @csrf

            <div class="form-group">
                <div class="input-group" id="">
                    <label>Select Region Serv</label>
                    <div class="cstm-select-box">
                        <select name="region_id">
                            <option disabled="disabled" selected="selected">Select Region</option>
                            @if(isset($state_data["data"]))
                                @foreach($state_data["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('region_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["state_name"] }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif


@if(Route::current()->getName() == 'admin_show_TM_sub_category')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
        @csrf
            <div class="form-group">
                <div class="input-group" id="">
                    <label>TM Category</label>
                    <div class="cstm-select-box">
                        <select name="TM_category_id">
                            <option disabled="disabled" selected="selected">Select TM Category</option>
                            @if(isset($data["vehicle_categoies"]["data"]))
                                @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="number" value="{{ Request::get('load_capacity') ? Request::get('load_capacity') : ''  }}" name="load_capacity" class="form-control" placeholder="TM Load Capacity">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_report_TM_sub_category_report')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
        @csrf
            <div class="form-group">
                <div class="input-group" id="">
                    <!-- <label>TM Category</label> -->
                    <div class="cstm-select-box">
                        <select name="TM_category_id">
                            <option disabled="disabled" selected="selected">Select TM Category</option>
                            @if(isset($data["vehicle_categoies"]["data"]))
                                @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="number" value="{{ Request::get('load_capacity') ? Request::get('load_capacity') : ''  }}" name="load_capacity" class="form-control" placeholder="TM Load Capacity">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_show_vehicle_list')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
        @csrf
            <div class="form-group">
                <div class="input-group" id="">
                    <!-- <label>TM Category</label> -->
                    <div class="cstm-select-box">
                        <select name="TM_category_id">
                            <option disabled="disabled" selected="selected">Select TM Category</option>
                            @if(isset($data["vehicle_categoies"]["data"]))
                                @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" id="">
                    <!-- <label>TM Sub Category</label> -->
                    <div class="cstm-select-box">
                        <select name="TM_sub_category_id">
                            <option disabled="disabled" selected="selected">Select TM Sub Category</option>
                            @if(isset($data["subcategories"]["data"]))
                                @foreach($data["subcategories"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_sub_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="min_delivery_range_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" value="{{ Request::get('min_delivery_range') ? Request::get('min_delivery_range') : ''  }}" name="min_delivery_range" class="form-control" placeholder="Delivery Range">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="min_trip_price_range_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" value="{{ Request::get('min_trip_price') ? Request::get('min_trip_price') : ''  }}" name="min_trip_price" class="form-control" placeholder="Min Trip Price">
                    </div>
                </div>
            </div>
            
            
            <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('TM_rc_number') ? Request::get('TM_rc_number') : ''  }}" name="TM_rc_number" class="form-control" placeholder="Vehicle Number">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" name="company_name" class="form-control" placeholder="Company Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="is_active">
                            <option value="" {{ Request::get('is_active') === '' ? 'selected' : ''  }}>Select Is Available</option>
                            <option value="true" {{ Request::get('is_active') === 'true' ? 'selected' : ''  }}>Yes</option>
                            <option value="false" {{ Request::get('is_active') === 'false' ? 'selected' : ''  }}>No</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="is_insurance_active">
                            <option value="" {{ Request::get('is_insurance_active') === '' ? 'selected' : ''  }}>Select Is Insurance Active</option>
                            <option value="true" {{ Request::get('is_insurance_active') === 'true' ? 'selected' : ''  }}>Yes</option>
                            <option value="false" {{ Request::get('is_insurance_active') === 'false' ? 'selected' : ''  }}>No</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_show_product_list')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
            @csrf
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" name="company_name" class="form-control" placeholder="Company Name">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <select name="grade_id" class="form-control" data-placeholder="Select Concrete Grade">
                            <option value="">Select Concrete Grade</option>
                            @if(isset($concrete_grade["data"]))
                                @foreach($concrete_grade["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('grade_id') == $value['_id'] ? 'selected' : '' }} >{{ $value["name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
                

                <div class="form-group">
                    <div class="input-group">
                        <button class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif
<!-- Porduct Module Over -->


@if(Route::current()->getName() == 'admin_report_designmix_list')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
            @csrf
                <!-- <div class="form-group">
                    <div class="input-group">
                        <input type="text" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" name="company_name" class="form-control" placeholder="Company Name">
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="input-group">
                        <select name="grade_id" class="form-control" data-placeholder="Select Concrete Grade">
                            <option value="">Select Concrete Grade</option>
                            @if(isset($concrete_grade["data"]))
                                @foreach($concrete_grade["data"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('grade_id') == $value['_id'] ? 'selected' : '' }} >{{ $value["name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
                
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif
<!-- Porduct Module Over -->

<!-- Product Review Over -->

@if(Route::current()->getName() == 'admin_show_product_review')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
            @csrf
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" name="buyer_name" class="form-control" placeholder="Buyer Name">
                </div>
            </div> -->

            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" name="company_name" class="form-control" placeholder="Supplier Company Name">
                </div>
            </div> -->

            <!-- <div class="form-group">
                <div class="input-group single-search-select2">
                    <select id="product__sub_category_list" name="subcategoryId" class="form-control-chosen" data-placeholder="Select Product Sub Category">
                        <option></option>
                        @if(isset($data["sub_category"]))
                            @foreach($data["sub_category"] as $value)
                                <option value="{{ $value['_id'] }}" {{ Request::get('subcategoryId') == $value['_id'] ? 'selected' : '' }} >{{ $value["sub_category_name"] }}</option>
                            @endforeach

                        @endif
                    </select>
                </div>
            </div> -->
            <!-- <div class="form-group">
                <div class="input-group single-search-select2">
                    <select id="review_product_list" name="product_id" class="form-control-chosen" data-placeholder="Select Product">
                        <option></option>
                        @if(isset($data["product_data"]))
                            @foreach($data["product_data"] as $value)
                                <option value="{{ $value['_id'] }}" {{ Request::get('product_id') == $value['_id'] ? 'selected' : '' }} >{{ $value["concrete_grade"]["name"] }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div> -->
            <!-- <div class="form-group">
                <div class="input-group single-search-select2">
                    <select id="review_user_list" name="review_user_id" class="form-control-chosen" data-placeholder="Select User">
                        <option></option>
                        @if(isset($data["user_data"]))
                            @foreach($data["user_data"] as $value)
                                <option value="{{ $value['_id'] }}" {{ Request::get('review_user_id') == $value['_id'] ? 'selected' : '' }} >{{ $value["full_name"] }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div> -->
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="review_user_id" value="{{ Request::get('review_user_id') ? Request::get('review_user_id') : '' }}" class="form-control" placeholder="User Id">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="search_term" value="{{ Request::get('search_term') ? Request::get('search_term') : '' }}" class="form-control" placeholder="Search Review">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="status">
                            <option disabled="disabled" selected="selected">Status</option>
                            <option value="published" {{ Request::get('status') == 'published' ? 'selected' : '' }}>Publish</option>
                            <option value="unpublished" {{ Request::get('status') == 'unpublished' ? 'selected' : '' }}>Unpublish</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_show_feedback')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
            @csrf
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" name="buyer_name" class="form-control" placeholder="Buyer Name">
                </div>
            </div> -->

            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" name="company_name" class="form-control" placeholder="Supplier Company Name">
                </div>
            </div> -->

            <!-- <div class="form-group">
                <div class="input-group single-search-select2">
                    <select id="product__sub_category_list" name="subcategoryId" class="form-control-chosen" data-placeholder="Select Product Sub Category">
                        <option></option>
                        @if(isset($data["sub_category"]))
                            @foreach($data["sub_category"] as $value)
                                <option value="{{ $value['_id'] }}" {{ Request::get('subcategoryId') == $value['_id'] ? 'selected' : '' }} >{{ $value["sub_category_name"] }}</option>
                            @endforeach

                        @endif
                    </select>
                </div>
            </div> -->
            <!-- <div class="form-group">
                <div class="input-group single-search-select2">
                    <select id="review_product_list" name="product_id" class="form-control-chosen" data-placeholder="Select Product">
                        <option></option>
                        @if(isset($data["product_data"]))
                            @foreach($data["product_data"] as $value)
                                <option value="{{ $value['_id'] }}" {{ Request::get('product_id') == $value['_id'] ? 'selected' : '' }} >{{ $value["concrete_grade"]["name"] }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div> -->
            <!-- <div class="form-group">
                <div class="input-group single-search-select2">
                    <select id="review_user_list" name="review_user_id" class="form-control-chosen" data-placeholder="Select User">
                        <option></option>
                        @if(isset($data["user_data"]))
                            @foreach($data["user_data"] as $value)
                                <option value="{{ $value['_id'] }}" {{ Request::get('review_user_id') == $value['_id'] ? 'selected' : '' }} >{{ $value["full_name"] }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div> -->
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="review_user_id" value="{{ Request::get('review_user_id') ? Request::get('review_user_id') : '' }}" class="form-control" placeholder="User Id">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="search_term" value="{{ Request::get('search_term') ? Request::get('search_term') : '' }}" class="form-control" placeholder="Search Review">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="feedback_type">
                            <option disabled="disabled" selected="selected">Status</option>
                            <option value="Good" {{ Request::get('feedback_type') == 'Good' ? 'selected' : '' }}>Good</option>
                            <option value="Better" {{ Request::get('feedback_type') == 'Better' ? 'selected' : '' }}>Better</option>
                            <option value="Best" {{ Request::get('feedback_type') == 'Best' ? 'selected' : '' }}>Best</option>
                            <option value="Bad" {{ Request::get('feedback_type') == 'Bad' ? 'selected' : '' }}>Bad</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

<!-- Product Review Over -->

<!-- Access Logs Module Start -->

@if(Route::current()->getName() == 'admin_show_email_logs')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
            @csrf
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="User Id">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="to_email" value="{{ Request::get('to_email') ? Request::get('to_email') : ''  }}" class="form-control" placeholder="To Email">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="from_email" value="{{ Request::get('from_email') ? Request::get('from_email') : ''  }}" class="form-control" placeholder="From Email">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="subject" value="{{ Request::get('subject') ? Request::get('subject') : ''  }}" class="form-control" placeholder="Subject">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif


@if(Route::current()->getName() == 'admin_show_action_logs')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
                @csrf
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="User Id">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="user_type" value="{{ Request::get('user_type') ? Request::get('user_type') : ''  }}" class="form-control" placeholder="User Type">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="access_type" value="{{ Request::get('access_type') ? Request::get('access_type') : ''  }}" class="form-control" placeholder="Access Type">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="daterange" value="01/01/2018 - 01/15/2018" />
                        <input type="hidden" id="access_log_start_date"  name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : ''  }}" />
                        <input type="hidden" id="access_log_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : ''  }}" />
                        
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

<script>
$(function() {

    var today = new Date();
    var end = new Date();
    // today.setDate(today.getDate() + 2);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    end = dd + '/' + mm + '/' + yyyy;
    console.log(today);

    @if(Request::get('start_date'))

        var date = new Date('{{Request::get('start_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        today = dd + '/' + mm + '/' + yyyy;
        // today = yyyy + '-' + mm + '-' + dd;
        console.log("start_date..."+today);
    @endif

    @if(Request::get('end_date'))

        var date = new Date('{{Request::get('end_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        end = dd + '/' + mm + '/' + yyyy;
        // end = yyyy + '-' + mm + '-' + dd;
        console.log("end_date..."+end);
    @endif

  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
    startDate: today,
    endDate: end,
    locale: {
            format: 'DD/MM/YYYY'
        }
  }, function(start, end, label) {
      $("#access_log_start_date").val(start.format('YYYY-MM-DD'));
      $("#access_log_end_date").val(end.format('YYYY-MM-DD'));
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>

@endif
<!-- Access Logs Module Over -->
<!-- Leder Module Over -->

@if(Route::current()->getName() == 'admin_show_orders')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="order_status">
                            <option disabled="disabled" selected="selected">Select Order Status</option>
                            <option {{ Request::get('order_status') == 'RECEIVED' ? 'selected' : '' }} value="RECEIVED">RECEIVED</option>
                            <option {{ Request::get('order_status') == 'ACCEPTED' ? 'selected' : '' }} value="ACCEPTED">ACCEPTED</option>
                            <option {{ Request::get('order_status') == 'PROCESSING' ? 'selected' : '' }} value="PROCESSING">PROCESSING</option>
                            <option {{ Request::get('order_status') == 'REJECTED' ? 'selected' : '' }} value="REJECTED">REJECTED</option>
                            <option {{ Request::get('order_status') == 'TM_ASSIGNED' ? 'selected' : '' }} value="TM_ASSIGNED">TM ASSIGNED</option>
                            <option {{ Request::get('order_status') == 'PICKUP' ? 'selected' : '' }} value="PICKUP">PICKUP</option>
                            <option {{ Request::get('order_status') == 'DELAYED' ? 'selected' : '' }} value="DELAYED">DELAYED</option>
                            <option {{ Request::get('order_status') == 'DELIVERED' ? 'selected' : '' }} value="DELIVERED">DELIVERED</option>
                            <option {{ Request::get('order_status') == 'CANCELLED' ? 'selected' : '' }} value="CANCELLED">CANCELLED</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_status">
                            <option disabled="disabled" selected="selected">Select Payment Status</option>
                            <option value="PAID" {{ Request::get('payment_status') == 'PAID' ? 'selected' : '' }}>PAID</option>
                            <option value="UNPAID" {{ Request::get('payment_status') == 'UNPAID' ? 'selected' : '' }}>UNPAID</option>

                        </select>
                    </div>
                </div>
            </div>

            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="order_id" value="{{ Request::get('order_id') ? Request::get('order_id') : ''  }}" class="form-control" placeholder="My Order Id">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="buyer_id" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : ''  }}" class="form-control" placeholder="Buyer Order Id">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" class="form-control" placeholder="Buyer Name">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_mode">
                            <option disabled="disabled" selected="selected">Select Payment Mode</option>
                            <option value="PAID" {{ Request::get('payment_mode') == 'PAID' ? 'selected' : '' }}>PAID</option>


                        </select>
                    </div>
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_year_filter" name="year" class="form-control"  placeholder="Select Year">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_month_filter" name="month" class="form-control"  placeholder="Select Month">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_date_filter" name="date" class="form-control"  placeholder="Select Date">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->
<script>

$('#order_year_filter').datepicker({
	    autoclose: true,
        minViewMode: "years",
	    format: 'yyyy'
		}).on('changeDate', function(selected){
	        // startDate = new Date(selected.date.valueOf());
	        // startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});

@if(Request::get('year'))

    $("#order_year_filter").datepicker("setDate",new Date({{Request::get('year')}}, 1)).trigger('change');

@endif

    $('#order_month_filter').datepicker({
	    autoclose: true,
        minViewMode: "months",
	    format: 'M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('month'))

    var date = new Date('{{Request::get('month')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_month_filter").datepicker("setDate",new Date(yyyy,mm, 1)).trigger('change');

@endif

    $('#order_date_filter').datepicker({
	    autoclose: true,
	    format: 'dd M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('date'))

    var date = new Date('{{Request::get('date')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_date_filter").datepicker("setDate",new Date(yyyy,mm, dd, 1)).trigger('change');

@endif

</script>

@endif


@if(Route::current()->getName() == 'admin_show_transaction')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
                @csrf
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" placeholder="User Id">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="order_id" value="{{ Request::get('order_id') ? Request::get('order_id') : ''  }}" placeholder="Order Id">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="gateway_transaction_id" value="{{ Request::get('gateway_transaction_id') ? Request::get('gateway_transaction_id') : ''  }}" placeholder="Gateway Transaction Id">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="transaction_status" value="{{ Request::get('transaction_status') ? Request::get('transaction_status') : ''  }}" placeholder="Transaction Status">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="payment_gateway" value="{{ Request::get('payment_gateway') ? Request::get('payment_gateway') : ''  }}" placeholder="Payment Gateway">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif
<!-- Leder Module Over -->


@if(Route::current()->getName() == 'admin_admix_brand_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif
<!-- Leder Module Over -->

@if(Route::current()->getName() == 'admin_admix_types_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif
<!-- Leder Module Over -->

@if(Route::current()->getName() == 'admin_agg_sand_cat_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" value="{{ Request::get('search') ? Request::get('search') : ''  }}" name="search" class="form-control" placeholder="Product Category">
                    </div>
                </div>

                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif
<!-- Leder Module Over -->

@if(Route::current()->getName() == 'admin_gst_slab_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif

@if(Route::current()->getName() == 'admin_margin_rate_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif

@if(Route::current()->getName() == 'admin_agg_sand_subcategory_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                @if(Request::get('categoryId') != null)
                    <input type="hidden" value="{{ Request::get('categoryId') ? Request::get('categoryId') : '' }}" name="categoryId"/>
                @endif
                <div class="form-group">
                    <div class="input-group">
                        <div class="cstm-select-box">
                            <select id="" name="categoryId" class="" data-placeholder="Select Product Category">
                                <option disabled="disabled" selected="selected">Select Category</option>
                                @if(isset($data["product_cat_data"]))
                                    @foreach($data["product_cat_data"] as $value)
                                        <option value="{{ $value['_id'] }}" {{ Request::get('categoryId') != null ? (Request::get('categoryId') == $value['_id'] ? 'selected' : '') : ($value["category_name"] == 'Aggregate' ? 'selected' : '') }} >{{ $value["category_name"] }}</option>
                                    @endforeach
                                @endif
                                
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" value="{{ Request::get('search') ? Request::get('search') : ''  }}" name="search" class="form-control" placeholder="Product Sub Category">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group" id="">
                        <!-- <label>Margin Rate</label> -->
                        <div class="cstm-select-box">
                            <select name="margin_rate_id">
                                <option disabled="disabled" selected="selected">Select Margin Rate</option>
                                @if(isset($data["margin_rate_data"]))
                                    @foreach($data["margin_rate_data"] as $key => $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('margin_rate_id') == $value['_id'] ? 'selected' : '' }}>{{ $value["title"] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif


@if(Route::current()->getName() == 'admin_concrete_grade_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif

@if(Route::current()->getName() == 'admin_concrete_pump_category_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif
<!-- Leder Module Over -->

<!-- Support Ticket Module Start -->

@if(Route::current()->getName() == 'admin_show_support_ticket')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="severity">
                            <option disabled="disabled" name="severity" selected="selected">Select Priority</option>
                            <option value="Urgent" {{ Request::get('severity') == 'Urgent' ? 'selected' : ''  }}>Urgent</option>
                            <option value="High" {{ Request::get('severity') == 'High' ? 'selected' : ''  }}>High</option>
                            <option value="Low" {{ Request::get('severity') == 'Low' ? 'selected' : ''  }}>Normal</option>
                            <option value="Normal" {{ Request::get('severity') == 'Normal' ? 'selected' : ''  }}>Low</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="subject" value="{{ Request::get('subject') ? Request::get('subject') : ''  }}" placeholder="Subject">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="ticket_id" value="{{ Request::get('ticket_id') ? Request::get('ticket_id') : ''  }}" placeholder="Ticket Id">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" placeholder="Buyer Name">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" placeholder="Supplier Company Name">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="client_type">
                            <option disabled="disabled" selected="selected">Select User type</option>
                            <option value="Buyer">Buyer</option>
                            <option value="Vendor">Vendor</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="support_ticket_status">
                            <option disabled="disabled" selected="selected">Select Ticket Status</option>
                            <option value="OPEN">OPEN</option>
                            <option value="CLOSED">CLOSED</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button class="site-button btn_bg_color">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->





@endif


@if(Route::current()->getName() == 'admin_support_ticket_report')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="severity">
                            <option disabled="disabled" name="severity" selected="selected">Select Priority</option>
                            <option value="Urgent" {{ Request::get('severity') == 'Urgent' ? 'selected' : ''  }}>Urgent</option>
                            <option value="High" {{ Request::get('severity') == 'High' ? 'selected' : ''  }}>High</option>
                            <option value="Low" {{ Request::get('severity') == 'Low' ? 'selected' : ''  }}>Normal</option>
                            <option value="Normal" {{ Request::get('severity') == 'Normal' ? 'selected' : ''  }}>Low</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="subject" value="{{ Request::get('subject') ? Request::get('subject') : ''  }}" placeholder="Subject">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="ticket_id" value="{{ Request::get('ticket_id') ? Request::get('ticket_id') : ''  }}" placeholder="Ticket Id">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" placeholder="Buyer Name">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" placeholder="Supplier Company Name">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="client_type">
                            <option disabled="disabled" selected="selected">Select User type</option>
                            <option value="Buyer">Buyer</option>
                            <option value="Vendor">Vendor</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="support_ticket_status">
                            <option disabled="disabled" selected="selected">Select Ticket Status</option>
                            <option value="OPEN">OPEN</option>
                            <option value="CLOSED">CLOSED</option>
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button btn_bg_color">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->





@endif

<!-- Support Ticket Module Over -->

@if(Route::current()->getName() == 'admin_cement_brand_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif

@if(Route::current()->getName() == 'admin_cement_grade_report')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif

@if(Route::current()->getName() == 'admin_concrete_grade_report_export')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif

@if(Route::current()->getName() == 'admin_concrete_pump_category_report_export')


<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
                @csrf
                <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
                <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
                <div class="form-group">
                    <div class="input-group">
                        <button type="submit" class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif
<!-- Leder Module Over -->

<!-- Proposal Module Over -->

@if(Route::current()->getName() == 'admin_show_proposal')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
                @csrf
        <div class="form-group">
            <div class="input-group single-search-select2">

                <select id="product_category_subcategory_list" name="categoryId" onchange="getSubcatByCategoryId()" class="form-control-chosen" data-placeholder="Select Product Category">
                    <option></option>
                    @if(isset($data["product_cat_data"]))
                        @foreach($data["product_cat_data"] as $value)
                            <option value="{{ $value['_id'] }}" {{ Request::get('categoryId') == $value['_id'] ? 'selected' : '' }} >{{ $value["category_name"] }}</option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group single-search-select2">
                <select id="product__sub_category_list" name="subcategoryId" class="form-control-chosen" data-placeholder="Select Product Sub Category">
                    <option></option>
                    @if(isset($data["sub_category"]))
                        @foreach($data["sub_category"] as $value)
                            <option value="{{ $value['_id'] }}" {{ Request::get('subcategoryId') == $value['_id'] ? 'selected' : '' }} >{{ $value["sub_category_name"] }}</option>
                        @endforeach

                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group single-search-select2">
                <select id="city_list" name="city_id" class="form-control-chosen" data-placeholder="Select City">
                    <option></option>
                    @if(isset($data["city_data"]))
                        @foreach($data["city_data"] as $value)
                            <option value="{{ $value['_id'] }}" {{ Request::get('city_id') == $value['_id'] ? 'selected' : '' }} >{{ $value["city_name"] }}</option>
                        @endforeach

                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <button class="site-button">Search</button>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_report_buyer_listing')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" placeholder="Company Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Full Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="User Id">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="mobile_no" id="mobile_or_email" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="email" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="account_type">
                            <option disabled="disabled" selected="selected">Select Account Type</option>
                            <option value="">Select Account Type</option>
                            <option value="Builder">Builder</option>
                            <option value="Contractor">Contractor</option>
                            <option value="Individual">Individual</option>
                        </select>
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                <input type="submit" name="logistic_filter_submit" value="Search" class="site-button"/>
                    <!-- <button class="site-button">Search</button> -->
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->


@endif

<!-- Proposal Module Over -->

<!-- Report Module Start -->

@if(Route::current()->getName() == 'admin_report_buyer_orders')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="order_status">
                            <option disabled="disabled" selected="selected">Select Order Status</option>
                            <option {{ Request::get('order_status') == 'RECEIVED' ? 'selected' : '' }} value="RECEIVED">RECEIVED</option>
                            <option {{ Request::get('order_status') == 'ACCEPTED' ? 'selected' : '' }} value="ACCEPTED">ACCEPTED</option>
                            <option {{ Request::get('order_status') == 'PROCESSING' ? 'selected' : '' }} value="PROCESSING">PROCESSING</option>
                            <option {{ Request::get('order_status') == 'REJECTED' ? 'selected' : '' }} value="REJECTED">REJECTED</option>
                            <option {{ Request::get('order_status') == 'TM_ASSIGNED' ? 'selected' : '' }} value="TM_ASSIGNED">TM ASSIGNED</option>
                            <option {{ Request::get('order_status') == 'PICKUP' ? 'selected' : '' }} value="PICKUP">PICKUP</option>
                            <option {{ Request::get('order_status') == 'DELAYED' ? 'selected' : '' }} value="DELAYED">DELAYED</option>
                            <option {{ Request::get('order_status') == 'DELIVERED' ? 'selected' : '' }} value="DELIVERED">DELIVERED</option>
                            <option {{ Request::get('order_status') == 'CANCELLED' ? 'selected' : '' }} value="CANCELLED">CANCELLED</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_status">
                            <option disabled="disabled" selected="selected">Select Payment Status</option>
                            <option value="PAID" {{ Request::get('payment_status') == 'PAID' ? 'selected' : '' }}>PAID</option>
                            <option value="UNPAID" {{ Request::get('payment_status') == 'UNPAID' ? 'selected' : '' }}>UNPAID</option>

                        </select>
                    </div>
                </div>
            </div>

            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="order_id" value="{{ Request::get('order_id') ? Request::get('order_id') : ''  }}" class="form-control" placeholder="My Order Id">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="buyer_id" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : ''  }}" class="form-control" placeholder="Buyer Order Id">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" class="form-control" placeholder="Buyer Name">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_mode">
                            <option disabled="disabled" selected="selected">Select Payment Mode</option>
                            <option value="PAID" {{ Request::get('payment_mode') == 'PAID' ? 'selected' : '' }}>PAID</option>


                        </select>
                    </div>
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_year_filter" name="year" class="form-control"  placeholder="Select Year">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_month_filter" name="month" class="form-control"  placeholder="Select Month">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_date_filter" name="date" class="form-control"  placeholder="Select Date">
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->
<script>

$('#order_year_filter').datepicker({
	    autoclose: true,
        minViewMode: "years",
	    format: 'yyyy'
		}).on('changeDate', function(selected){
	        // startDate = new Date(selected.date.valueOf());
	        // startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});

@if(Request::get('year'))

    $("#order_year_filter").datepicker("setDate",new Date({{Request::get('year')}}, 1)).trigger('change');

@endif

    $('#order_month_filter').datepicker({
	    autoclose: true,
        minViewMode: "months",
	    format: 'M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('month'))

    var date = new Date('{{Request::get('month')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_month_filter").datepicker("setDate",new Date(yyyy,mm, 1)).trigger('change');

@endif

    $('#order_date_filter').datepicker({
	    autoclose: true,
	    format: 'dd M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('date'))

    var date = new Date('{{Request::get('date')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_date_filter").datepicker("setDate",new Date(yyyy,mm, dd, 1)).trigger('change');

@endif

</script>
@endif

@if(Route::current()->getName() == 'admin_report_supplier_orders')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="supplier_report_form" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="order_status">
                            <option disabled="disabled" selected="selected">Select Order status</option>
                            <option {{ Request::get('order_status') == 'RECEIVED' ? 'selected' : '' }} value="RECEIVED">RECEIVED</option>
                            <option {{ Request::get('order_status') == 'ACCEPTED' ? 'selected' : '' }} value="ACCEPTED">ACCEPTED</option>
                            <option {{ Request::get('order_status') == 'REJECTED' ? 'selected' : '' }} value="REJECTED">REJECTED</option>
                            <option {{ Request::get('order_status') == 'TM_ASSIGNED' ? 'selected' : '' }} value="TM_ASSIGNED">TM_ASSIGNED</option>
                            <option {{ Request::get('order_status') == 'PICKUP' ? 'selected' : '' }} value="PICKUP">PICKUP</option>
                            <option {{ Request::get('order_status') == 'DELAYED' ? 'selected' : '' }} value="DELAYED">DELAYED</option>
                            <option {{ Request::get('order_status') == 'DELIVERED' ? 'selected' : '' }} value="DELIVERED">DELIVERED</option>
                            <option {{ Request::get('order_status') == 'CANCELLED' ? 'selected' : '' }} value="CANCELLED">CANCELLED</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_status">
                            <option disabled="disabled" selected="selected">Select Payment Status</option>
                            <option value="PAID" {{ Request::get('payment_status') == 'PAID' ? 'selected' : '' }}>PAID</option>
                            <option value="UNPAID" {{ Request::get('payment_status') == 'UNPAID' ? 'selected' : '' }}>UNPAID</option>

                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="order_id" value="{{ Request::get('order_id') ? Request::get('order_id') : ''  }}" class="form-control" placeholder="Supplier Order Id">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="buyer_id" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : ''  }}" class="form-control" placeholder="Buyer Id">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" placeholder="Company Name">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_year_filter" name="year" class="form-control"  placeholder="Select Year">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_month_filter" name="month" class="form-control"  placeholder="Select Month">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_date_filter" name="date" class="form-control"  placeholder="Select Date">
                    </div>
                </div>
            </div>
            <input type="hidden" id="report_start_date" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : '' }}">
            <input type="hidden" id="report_end_date" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : '' }}">
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->
<script>

$('#order_year_filter').datepicker({
	    autoclose: true,
        minViewMode: "years",
	    format: 'yyyy'
		}).on('changeDate', function(selected){
	        // startDate = new Date(selected.date.valueOf());
	        // startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});

@if(Request::get('year'))

    $("#order_year_filter").datepicker("setDate",new Date({{Request::get('year')}}, 1)).trigger('change');

@endif

    $('#order_month_filter').datepicker({
	    autoclose: true,
        minViewMode: "months",
	    format: 'M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('month'))

    var date = new Date('{{Request::get('month')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_month_filter").datepicker("setDate",new Date(yyyy,mm, 1)).trigger('change');

@endif

    $('#order_date_filter').datepicker({
	    autoclose: true,
	    format: 'dd M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('date'))

    var date = new Date('{{Request::get('date')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_date_filter").datepicker("setDate",new Date(yyyy,mm, dd, 1)).trigger('change');

@endif

</script>
@endif


@if(Route::current()->getName() == 'admin_report_logistics_orders')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="order_status">
                            <option disabled="disabled" selected="selected">Select Order Status</option>
                            <option {{ Request::get('order_status') == 'ORDER_RECEIVED' ? 'selected' : '' }} value="ORDER_RECEIVED">ORDER RECEIVED</option>
                            <option {{ Request::get('order_status') == 'TRUCK_ASSIGNED_FOR_PICKUP' ? 'selected' : '' }} value="TRUCK_ASSIGNED_FOR_PICKUP">TRUCK ASSIGNED FOR PICKUP</option>
                            <option {{ Request::get('order_status') == 'PICKUP' ? 'selected' : '' }} value="PICKUP">PICKUP</option>
                            <option {{ Request::get('order_status') == 'DELIVERED' ? 'selected' : '' }} value="DELIVERED">DELIVERED</option>
                            <option {{ Request::get('order_status') == 'CANCELLED' ? 'selected' : '' }} value="CANCELLED">CANCELLED</option>
                            <option {{ Request::get('order_status') == 'DELAYED' ? 'selected' : '' }} value="DELAYED">DELAYED</option>

                        </select>
                    </div>
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_status">
                            <option disabled="disabled" selected="selected">Select Payment Status</option>
                            <option value="PAID" {{ Request::get('payment_status') == 'PAID' ? 'selected' : '' }}>PAID</option>
                            <option value="UNPAID" {{ Request::get('payment_status') == 'UNPAID' ? 'selected' : '' }}>UNPAID</option>

                        </select>
                    </div>
                </div>
            </div> -->

            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" value="{{Request::get('selected_month_year')}}" name="selected_month_year" id="daily_chart_year_filter" class="form-control from" readonly="" placeholder="Select Year Month">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="logistic_id" value="{{ Request::get('logistic_id') ? Request::get('logistic_id') : ''  }}" class="form-control" placeholder="Supplier Id">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="transaction_id" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : ''  }}" class="form-control" placeholder="Transaction Id">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
@endif


@if(Route::current()->getName() == 'admin_report_product_list')

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
            @csrf
                <!-- <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search Product Name">
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="input-group single-search-select2">
                        <select id="product_category_subcategory_list" name="categoryId" onchange="this.form.submit()" class="form-control-chosen" data-placeholder="Select Product Category">
                            <option></option>
                            @if(isset($data["category"]))
                                @foreach($data["category"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('categoryId') == $value['_id'] ? 'selected' : '' }} >{{ $value["category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group single-search-select2">
                        <select id="product__sub_category_list" name="subcategoryId" class="form-control-chosen" data-placeholder="Select Product Sub Category">
                            <option></option>
                            @if(isset($data["sub_category"]))
                                @foreach($data["sub_category"] as $value)
                                    <option value="{{ $value['_id'] }}" {{ Request::get('subcategoryId') == $value['_id'] ? 'selected' : '' }} >{{ $value["sub_category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <button class="site-button">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->

@endif

@if(Route::current()->getName() == 'admin_report_vehicle_list_ddd')
<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
        <div id="filterdismiss"><i class="fa fa-close"></i></div>
        <form action="{{ route(Route::current()->getName()) }}" name="" method="get">
          @csrf
        <div class="filter-sidebar-content">
            <h2>Filter</h2>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                      <select name="vehicle_category_id">
                          <option disabled="disabled" selected="selected">Select Vehicle Category</option>
                          @if(isset($data["vehicle_categoies"]["data"]))
                            @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                              <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == Request::get("vehicle_category_id") ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                            @endforeach
                          @endif
                      </select>
                    </div>
                </div>


            </div>

            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="vehicle_sub_category_id">
                        <option disabled="disabled" selected="selected">Select Vehicle Sub Category</option>
                        @if(isset($data["subcategories"]["data"]))
                          @foreach($data["subcategories"]["data"] as $cat_value)
                            <option value="{{ $cat_value['_id'] }}" {{ $cat_value['_id'] == Request::get("vehicle_sub_category_id") ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                          @endforeach
                        @endif
                    </select>
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="is_gps_enabled">
                        <option disabled="disabled" selected="selected">Select GPS Enabled</option>
                        <option value="true" {{ Request::get("is_gps_enabled") == 'true' ? 'selected' : '' }}>Yes</option>
                        <option value="false" {{ Request::get("is_gps_enabled") == 'false' ? 'selected' : '' }}>No</option>
                    </select>
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                  <div class="cstm-select-box">
                    <select name="is_insurance_active">
                        <option disabled="disabled" selected="selected">Select Insurance Active</option>
                        <option value="true" {{ Request::get("is_insurance_active") == 'true' ? 'selected' : '' }}>Yes</option>
                        <option value="false" {{ Request::get("is_insurance_active") == 'false' ? 'selected' : '' }}>No</option>
                    </select>
                  </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <button type="submit" class="site-button">Search</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

    @endif
    
@if(Route::current()->getName() == 'admin_report_vehicle_list')
<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{ route(Route::current()->getName()) }}" name="supplier_report_form" method="get">
        @csrf
            <div class="form-group">
                <div class="input-group" id="">
                    <!-- <label>TM Category</label> -->
                    <div class="cstm-select-box">
                        <select name="TM_category_id">
                            <option disabled="disabled" selected="selected">Select TM Category</option>
                            @if(isset($data["vehicle_categoies"]["data"]))
                                @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" id="">
                    <!-- <label>TM Sub Category</label> -->
                    <div class="cstm-select-box">
                        <select name="TM_sub_category_id">
                            <option disabled="disabled" selected="selected">Select TM Sub Category</option>
                            @if(isset($data["subcategories"]["data"]))
                                @foreach($data["subcategories"]["data"] as $cat_value)
                                    <option value="{{ $cat_value['_id'] }}" {{ Request::get('TM_sub_category_id') == $cat_value['_id'] ? 'selected' : '' }}>{{ $cat_value["sub_category_name"] }}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="min_delivery_range_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" value="{{ Request::get('min_delivery_range') ? Request::get('min_delivery_range') : ''  }}" name="min_delivery_range" class="form-control" placeholder="Delivery Range">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="two_fillter_box">
                    <div class="cstm-select-box">
                        <select name="min_trip_price_range_value">
                            <option value="less" selected>Lessthan</option>
                            <option value="more">Greaterthan </option>
                        </select>
                    </div>
                    <div class="two_fillter_box_right">
                        <input type="number" value="{{ Request::get('min_trip_price') ? Request::get('min_trip_price') : ''  }}" name="min_trip_price" class="form-control" placeholder="Min Trip Price">
                    </div>
                </div>
            </div>
            
            
            <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('TM_rc_number') ? Request::get('TM_rc_number') : ''  }}" name="TM_rc_number" class="form-control" placeholder="Vehicle Number">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" name="company_name" class="form-control" placeholder="Company Name">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="is_active">
                            <option value="" {{ Request::get('is_active') === '' ? 'selected' : ''  }}>Select Is Available</option>
                            <option value="true" {{ Request::get('is_active') === 'true' ? 'selected' : ''  }}>Yes</option>
                            <option value="false" {{ Request::get('is_active') === 'false' ? 'selected' : ''  }}>No</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="is_insurance_active">
                            <option value="" {{ Request::get('is_insurance_active') === '' ? 'selected' : ''  }}>Select Is Insurance Active</option>
                            <option value="true" {{ Request::get('is_insurance_active') === 'true' ? 'selected' : ''  }}>Yes</option>
                            <option value="false" {{ Request::get('is_insurance_active') === 'false' ? 'selected' : ''  }}>No</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
    <!-- FILTER SLIDE POPUP END  -->

    @endif

<!-- Report Module Over -->
@if((Route::current()->getName() == 'admin_show_supplier_orders'))

<!-- FILTER SLIDE POPUP START  -->
<div id="filterSidebar">
    <div id="filterdismiss"><i class="fa fa-close"></i></div>
    <div class="filter-sidebar-content">
        <h2>Filter</h2>
        <form action="{{route(Route::current()->getName())}}" name="" method="get">
                @csrf
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="order_status">
                            <option disabled="disabled" selected="selected">Select Order status</option>
                            <option {{ Request::get('order_status') == 'RECEIVED' ? 'selected' : '' }} value="RECEIVED">RECEIVED</option>
                            <option {{ Request::get('order_status') == 'ACCEPTED' ? 'selected' : '' }} value="ACCEPTED">ACCEPTED</option>
                            <option {{ Request::get('order_status') == 'REJECTED' ? 'selected' : '' }} value="REJECTED">REJECTED</option>
                            <option {{ Request::get('order_status') == 'TM_ASSIGNED' ? 'selected' : '' }} value="TM_ASSIGNED">TM_ASSIGNED</option>
                            <option {{ Request::get('order_status') == 'PICKUP' ? 'selected' : '' }} value="PICKUP">PICKUP</option>
                            <option {{ Request::get('order_status') == 'DELAYED' ? 'selected' : '' }} value="DELAYED">DELAYED</option>
                            <option {{ Request::get('order_status') == 'DELIVERED' ? 'selected' : '' }} value="DELIVERED">DELIVERED</option>
                            <option {{ Request::get('order_status') == 'CANCELLED' ? 'selected' : '' }} value="CANCELLED">CANCELLED</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cstm-select-box">
                        <select name="payment_status">
                            <option disabled="disabled" selected="selected">Select Payment Status</option>
                            <option value="PAID" {{ Request::get('payment_status') == 'PAID' ? 'selected' : '' }}>PAID</option>
                            <option value="UNPAID" {{ Request::get('payment_status') == 'UNPAID' ? 'selected' : '' }}>UNPAID</option>

                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="order_id" value="{{ Request::get('order_id') ? Request::get('order_id') : ''  }}" class="form-control" placeholder="Supplier Order Id">
                </div>
            </div>
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="buyer_id" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : ''  }}" class="form-control" placeholder="Buyer Id">
                </div>
            </div> -->
            <!-- <div class="form-group">
                <div class="input-group">
                    <input type="text" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" placeholder="Company Name">
                </div>
            </div> -->
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_year_filter" name="year" class="form-control"  placeholder="Select Year">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_month_filter" name="month" class="form-control"  placeholder="Select Month">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="cust_datepicker cust_datepicker_100">
                        <input type="text" id="order_date_filter" name="date" class="form-control"  placeholder="Select Date">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <button class="site-button">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="filteroverlay"></div>
<!-- FILTER SLIDE POPUP END  -->
<script>

$('#order_year_filter').datepicker({
	    autoclose: true,
        minViewMode: "years",
	    format: 'yyyy'
		}).on('changeDate', function(selected){
	        // startDate = new Date(selected.date.valueOf());
	        // startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
	});

@if(Request::get('year'))

    $("#order_year_filter").datepicker("setDate",new Date({{Request::get('year')}}, 1)).trigger('change');

@endif

    $('#order_month_filter').datepicker({
	    autoclose: true,
        minViewMode: "months",
	    format: 'M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('month'))

    var date = new Date('{{Request::get('month')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_month_filter").datepicker("setDate",new Date(yyyy,mm, 1)).trigger('change');

@endif

    $('#order_date_filter').datepicker({
	    autoclose: true,
	    format: 'dd M yyyy'
		}).on('changeDate', function(selected){

	});

@if(Request::get('date'))

    var date = new Date('{{Request::get('date')}}');

    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth()).padStart(2, '0'); //January is 0!
    var yyyy = date.getFullYear();
    console.log(mm+"..."+yyyy);

    $("#order_date_filter").datepicker("setDate",new Date(yyyy,mm, dd, 1)).trigger('change');

@endif

</script>


@endif

