@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<!-- SIDBAR NAV MENU START  -->
<nav id="sidebar" class="NW-sidebar-wrapper">
        <div class="sidebar-header">
            <h3 class="text-center">
                <a href="{{ route('admin_dashboard') }}">
                    <img src="{{asset('assets/admin/images/logo3.webp')}}">
                    <sup style="color: #fff;left: -40px;top: 0px;">BETA</sup>
                </a>
            </h3>
            <strong>
                <a href="{{ route('admin_dashboard') }}">
                    <img src="{{asset('assets/admin/images/small_logo.webp')}}">.
                    <sup style="color: #fff;">BETA</sup>
                </a>
            </strong>
        </div>
        <div class="NW-sidebar-menu">
            <ul>
              @if($profile["rights"]["dashboard_view"])
                <li class="{{ Route::current()->getName() == 'admin_dashboard' ? 'active' : '' }}">
                  <a href="{{ route('admin_dashboard') }}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
                </li>
              @endif

              @if($profile["rights"]["users_view"])

                  @php $is_user_active = ''; @endphp
                  @if ((Route::current()->getName() == 'admin_show_logistics_user') 
                      || (Route::current()->getName() == 'admin_show_suppliers_user') 
                      || (Route::current()->getName() == 'admin_show_buyers_user')
                      || (Route::current()->getName() == 'admin_show_user')
                  )
                      @php $is_user_active = 'display:block'; @endphp
                  @endif
                <li class="NW-sidebar-dropdown {{ $is_user_active != '' ? 'Active' : '' }}">
                  <a href="javascript:;"><i class="fa fa-users"></i> <span>Users</span></a>
                  <!-- Submenu /-->
                  

                  <div class="NW-sidebar-submenu" style="{{ $is_user_active }}">
                    <ul>
                      @if($profile["rights"]["admin_user_view"])
                        <li class="{{ Route::current()->getName() == 'admin_show_user' ? 'subActive' : '' }}"><a href="{{ route('admin_show_user') }}"><i class="fa fa-circle"></i> Administrator <!-- <span class="count-noti bg-aqua">4</span>--></a> </li>
                      @endif
                      @if($profile["rights"]["buyers_view"])
                        <li class="{{ Route::current()->getName() == 'admin_show_buyers_user' ? 'subActive' : '' }}"><a href="{{ route('admin_show_buyers_user') }}"><i class="fa fa-circle"></i> Buyers</a></li>
                      @endif
                      @if($profile["rights"]["suppliers_view"])
                        <li class="{{ Route::current()->getName() == 'admin_show_suppliers_user' ? 'subActive' : '' }}"><a href="{{ route('admin_show_suppliers_user') }}"><i class="fa fa-circle"></i>RMC Suppliers</a></li>
                      @endif

                      <!-- <li class="{{ Route::current()->getName() == 'admin_show_logistics_user' ? 'subActive' : '' }}"><a href="{{ route('admin_show_logistics_user') }}"><i class="fa fa-circle"></i> Logistics</a></li> -->
                    
                    </ul>
                  </div>
                  <!-- Submenu end /-->
                </li>
              @endif

              @if($profile["rights"]["region_view"])

                @php $is_region_active = ''; @endphp
                  @if ((Route::current()->getName() == 'admin_show_region_country') 
                      || (Route::current()->getName() == 'admin_show_region_states') 
                      || (Route::current()->getName() == 'admin_show_region_cities')
                  )
                      @php $is_region_active = 'display:block'; @endphp
                  @endif
                <li class="NW-sidebar-dropdown {{ $is_region_active != '' ? 'Active' : '' }}">
                  <a href="javascript:;"><i class="fa fa-flag"></i> <span>Region</span></a>
                    <!-- Submenu /-->
                    <div class="NW-sidebar-submenu" style="{{ $is_region_active }}">
                        <ul>
                          @if($profile["rights"]["country_view"])
                            <li class="{{ Route::current()->getName() == 'admin_show_region_country' ? 'subActive' : '' }}"><a href="{{ route('admin_show_region_country') }}"><i class="fa fa-circle"></i> Country</a></li>
                          @endif
                          @if($profile["rights"]["state_view"])
                            <li class="{{ Route::current()->getName() == 'admin_show_region_states' ? 'subActive' : '' }}"><a href="{{ route('admin_show_region_states') }}"><i class="fa fa-circle"></i> States</a></li>
                          @endif
                          @if($profile["rights"]["city_view"])
                            <li class="{{ Route::current()->getName() == 'admin_show_region_cities' ? 'subActive' : '' }}"><a href="{{ route('admin_show_region_cities') }}"><i class="fa fa-circle"></i> City</a></li>
                          @endif
                          
                        </ul>
                    </div>
                    <!-- Submenu end /-->
                </li>

              @endif

              @if($profile["rights"]["grade_cat_brands_view"])
                   @php $is_product_active = ''; @endphp
                @if ((Route::current()->getName() == 'admin_show_product_category') 
                    || (Route::current()->getName() == 'admin_show_product_sub_category') 
                    || (Route::current()->getName() == 'admin_show_product_list')
                    || (Route::current()->getName() == 'admin_show_gst_info')
                    || (Route::current()->getName() == 'admin_show_margin_slab')
                    || (Route::current()->getName() == 'admin_show_aggregate_source')
                    || (Route::current()->getName() == 'admin_show_concrete_grade')
                    || (Route::current()->getName() == 'admin_show_cement_brand')
                    || (Route::current()->getName() == 'admin_show_cement_grade')
                    || (Route::current()->getName() == 'admin_show_admixture_brand')
                    || (Route::current()->getName() == 'admin_show_admixture_types')
                    || (Route::current()->getName() == 'admin_show_sand_source')
                    || (Route::current()->getName() == 'admin_show_flyash_source')
                    || (Route::current()->getName() == 'admin_show_sand_zone')
                )
                    @php $is_product_active = 'display:block'; @endphp
                @endif

              <li class="NW-sidebar-dropdown {{ $is_product_active != '' ? 'Active' : '' }}">
                <a href="javascript:;"><i class="fa fa-cubes"></i> <span>Grade / Category / Brands</span></a>
                  <!-- Submenu /-->
                  <div class="NW-sidebar-submenu" style="{{ $is_product_active }}">
                      <ul>
                        @if($profile["rights"]["concrete_grade_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_concrete_grade' ? 'subActive' : '' }}"><a href="{{ route('admin_show_concrete_grade') }}"><i class="fa fa-circle"></i> Concrete Grade</a></li>
                        @endif
                        @if($profile["rights"]["cement_brand_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_cement_brand' ? 'subActive' : '' }}"><a href="{{ route('admin_show_cement_brand') }}"><i class="fa fa-circle"></i> Cement Brand</a></li>
                        @endif
                        @if($profile["rights"]["cement_grade_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_cement_grade' ? 'subActive' : '' }}"><a href="{{ route('admin_show_cement_grade') }}"><i class="fa fa-circle"></i> Cement Grade</a></li>
                        @endif
                        @if($profile["rights"]["admixture_brand_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_admixture_brand' ? 'subActive' : '' }}"><a href="{{ route('admin_show_admixture_brand') }}"><i class="fa fa-circle"></i> Admixture Brand</a></li>
                        @endif
                        @if($profile["rights"]["admixture_types_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_admixture_types' ? 'subActive' : '' }}"><a href="{{ route('admin_show_admixture_types') }}"><i class="fa fa-circle"></i> Admixture Types</a></li>
                        @endif
                        @if($profile["rights"]["aggregate_sand_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_product_category' ? 'subActive' : '' }}"><a href="{{ route('admin_show_product_category') }}"><i class="fa fa-circle"></i> Aggregate (VSI) & Sand Category</a></li>
                        @endif
                        @if($profile["rights"]["aggregate_sand_sub_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_product_sub_category' ? 'subActive' : '' }}"><a href="{{ route('admin_show_product_sub_category') }}"><i class="fa fa-circle"></i> Aggregate (VSI), Sand Sub Category </a></li>
                        @endif
                        <!-- <li class="{{ Route::current()->getName() == 'admin_show_product_list' ? 'subActive' : '' }}"><a href="{{ route('admin_show_product_list') }}"><i class="fa fa-circle"></i> Suppliers Design Mix List</a></li> -->
                        @if($profile["rights"]["aggregate_source_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_aggregate_source' ? 'subActive' : '' }}"><a href="{{ route('admin_show_aggregate_source') }}"><i class="fa fa-circle"></i> Aggregate Source (VSI)</a></li>
                        @endif
                        @if($profile["rights"]["sand_source_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_sand_source' ? 'subActive' : '' }}"><a href="{{ route('admin_show_sand_source') }}"><i class="fa fa-circle"></i> Coarse Sand Source</a></li>
                        @endif
                        @if($profile["rights"]["sand_source_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_sand_zone' ? 'subActive' : '' }}"><a href="{{ route('admin_show_sand_zone') }}"><i class="fa fa-circle"></i> Coarse Sand Zone</a></li>
                        @endif
                        @if($profile["rights"]["flyash_source_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_flyash_source' ? 'subActive' : '' }}"><a href="{{ route('admin_show_flyash_source') }}"><i class="fa fa-circle"></i> Flyash Source</a></li>
                        @endif
                        @if($profile["rights"]["margin_slab_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_margin_slab' ? 'subActive' : '' }}"><a href="{{ route('admin_show_margin_slab') }}"><i class="fa fa-circle"></i> Margin Slab</a></li>
                        @endif
                        @if($profile["rights"]["gst_slab_view"])
                        
                        <li class="{{ Route::current()->getName() == 'admin_show_gst_info' ? 'subActive' : '' }}"><a href="{{ route('admin_show_gst_info') }}"><i class="fa fa-circle"></i> GST Info</a></li>
                        @endif
                      </ul>
                  </div>
                  <!-- Submenu end /-->
              </li>

              @endif

              @if($profile["rights"]["TM_CP_view"])

                @php $is_vehicle_active = ''; @endphp
                  @if ((Route::current()->getName() == 'admin_show_TM_category') 
                      || (Route::current()->getName() == 'admin_show_TM_sub_category') 
                      || (Route::current()->getName() == 'admin_show_pump_category') 
                      || (Route::current()->getName() == 'admin_show_vehicle_list')
                      || (Route::current()->getName() == 'admin_show_vehicle_pm_km_rate')
                  )
                      @php $is_vehicle_active = 'display:block'; @endphp
                  @endif
                <li class="NW-sidebar-dropdown {{ $is_vehicle_active != '' ? 'Active' : '' }}">
                  <a href="javascript:;"><i class="fa fa-truck"></i> <span>Transit Mixer & Pump</span></a>
                    <!-- Submenu /-->
                    <div class="NW-sidebar-submenu" style="{{ $is_vehicle_active }}">
                        <ul>
                          @if($profile["rights"]["TM_category_view"])  
                          <li class="{{ Route::current()->getName() == 'admin_show_TM_category' ? 'subActive' : '' }}"><a href="{{ route('admin_show_TM_category') }}"><i class="fa fa-circle"></i> TM Category </a></li>
                          @endif
                          @if($profile["rights"]["TM_sub_category_view"])
                          <li class="{{ Route::current()->getName() == 'admin_show_TM_sub_category' ? 'subActive' : '' }}"><a href="{{ route('admin_show_TM_sub_category') }}"><i class="fa fa-circle"></i> TM Sub Category </a></li>
                          @endif
                          @if($profile["rights"]["CP_category_view"])
                          <li class="{{ Route::current()->getName() == 'admin_show_pump_category' ? 'subActive' : '' }}"><a href="{{ route('admin_show_pump_category') }}"><i class="fa fa-circle"></i> Concrete Pump Category </a></li>
                          @endif
                          <!-- <li class="{{ Route::current()->getName() == 'admin_show_vehicle_list' ? 'subActive' : '' }}"><a href="{{ route('admin_show_vehicle_list') }}"><i class="fa fa-circle"></i> Suppliers TM List</a></li> -->
                          <!-- <li class="{{ Route::current()->getName() == 'admin_show_vehicle_pm_km_rate' ? 'subActive' : '' }}"><a href="{{ route('admin_show_vehicle_pm_km_rate') }}"><i class="fa fa-circle"></i> Per MT/KM Rate</a></li> -->
                        </ul>
                    </div>
                    <!-- Submenu end /-->
                </li>
              @endif
              <!-- <li class="{{ Route::current()->getName() == 'admin_show_coupon' ? 'active' : '' }}">
                <a href="{{ route('admin_show_coupon') }}"><i class="fa fa-tags"></i><span>Coupon</span></a>
              </li> -->
              @if($profile["rights"]["ledger_view"])

                @php $is_ledger_active = ''; @endphp
                  @if ((Route::current()->getName() == 'admin_show_orders') 
                      || (Route::current()->getName() == 'admin_show_transaction')
                      || (Route::current()->getName() == 'admin_show_supplier_orders')
                      || (Route::current()->getName() == 'admin_show_logistics_orders')
                  )
                      @php $is_ledger_active = 'display:block'; @endphp
                  @endif

                <li class="NW-sidebar-dropdown {{ $is_ledger_active != '' ? 'Active' : '' }}">
                  <a href="javascript:;"><i class="fa fa-shopping-bag"></i> <span>Ledger</span></a>
                    <!-- Submenu /-->
                    <div class="NW-sidebar-submenu" style="{{ $is_ledger_active }}">
                        <ul>
                          @if($profile["rights"]["buyer_order_view"])  
                          <li class="{{ Route::current()->getName() == 'admin_show_orders' ? 'subActive' : '' }}"><a href="{{ route('admin_show_orders') }}"><i class="fa fa-circle"></i> Buyer Orders</a></li>
                          @endif
                          @if($profile["rights"]["supplier_order_view"])
                          <li class="{{ Route::current()->getName() == 'admin_show_supplier_orders' ? 'subActive' : '' }}"><a href="{{ route('admin_show_supplier_orders') }}"><i class="fa fa-circle"></i> Suppliers Orders</a></li>
                          @endif
                          
                          <!-- <li class="{{ Route::current()->getName() == 'admin_show_transaction' ? 'subActive' : '' }}"><a href="{{ route('admin_show_transaction') }}"><i class="fa fa-circle"></i> Transaction</a></li> -->
                          
                        </ul>
                    </div>
                    <!-- Submenu end /-->
                </li>
              @endif
                <!-- <li class="{{ Route::current()->getName() == 'admin_show_proposal' ? 'active' : '' }}">
                <a href="{{ route('admin_show_proposal') }}"><i class="fa fa-edit"></i><span>Suggestion For New Product</span>
                </a>
              </li> -->
              @if($profile["rights"]["customer_support_view"])
                <li class="{{ Route::current()->getName() == 'admin_show_support_ticket' ? 'active' : '' }}">
                  <a href="{{ route('admin_show_support_ticket') }}"><i class="fa fa-life-bouy"></i><span>Customer Support</span>
                  </a>
                </li>
              @endif
                
         
              @if($profile["rights"]["review_feedback_view"])

                @php $is_review_active = ''; @endphp
                  @if (
                    (Route::current()->getName() == 'admin_show_product_review')
                    || (Route::current()->getName() == 'admin_show_feedback')
                  )
                      @php $is_review_active = 'display:block'; @endphp
                  @endif

                <li class="NW-sidebar-dropdown {{ $is_review_active != '' ? 'Active' : '' }}">
                  <a href="javascript:;"><i class="fa fa-balance-scale"></i> <span>Review & Feedback</span></a>
                    <!-- Submenu /-->
                    <div class="NW-sidebar-submenu" style="{{ $is_review_active }}">
                        <ul>
                          @if($profile["rights"]["review_view"])  
                            <li class="{{ Route::current()->getName() == 'admin_show_product_review' ? 'subActive' : '' }}"><a href="{{ route('admin_show_product_review') }}"><i class="fa fa-circle"></i> List Review</a></li>
                          @endif
                          @if($profile["rights"]["feedback_view"])
                            <li class="{{ Route::current()->getName() == 'admin_show_feedback' ? 'subActive' : '' }}"><a href="{{ route('admin_show_feedback') }}"><i class="fa fa-circle"></i> List Feedback</a></li>
                          @endif
                          
                        </ul>
                    </div>
                    <!-- Submenu end /-->
                </li>
             @endif
            

            
             @if($profile["rights"]["report_view"])
                <li class="{{ Route::current()->getName() == 'admin_show_report' ? 'active' : '' }}">
                  <a href="{{ route('admin_show_report') }}"><i class="fa fa-file-text-o "></i><span>Report</span>
                  </a>
                </li>
              @endif
                  
              @php $is_email_template_active = ''; @endphp
                @if ((Route::current()->getName() == 'admin_show_email_template'))
                
                    @php $is_email_template_active = 'display:block'; @endphp
                @endif

              <!-- <li class="{{ Route::current()->getName() == 'admin_show_email_template' ? 'active' : '' }}">
                  <a href="{{ route('admin_show_email_template') }}"><i class="fa fa-at"></i><span>Email Template</span></a>
              </li> -->
               @php $is_logs_active = ''; @endphp
                @if ((Route::current()->getName() == 'admin_show_email_logs') 
                    || (Route::current()->getName() == 'admin_show_action_logs')
                )
                    @php $is_logs_active = 'display:block'; @endphp
                @endif
              <li class="NW-sidebar-dropdown {{ $is_logs_active != '' ? 'Active' : '' }}" style="display:block;">
                <a href="javascript:;"><i class="fa fa-key"></i> <span>Logs</span></a>
                  <!-- Submenu /-->
                  <div class="NW-sidebar-submenu" style="{{ $is_logs_active }}">
                      <ul>
                        <!-- <li class="{{ Route::current()->getName() == 'admin_show_email_logs' ? 'subActive' : '' }}"><a href="{{ route('admin_show_email_logs') }}"><i class="fa fa-circle"></i> Email Logs</a></li> -->
                        <li class="{{ Route::current()->getName() == 'admin_show_action_logs' ? 'subActive' : '' }}"><a href="{{ route('admin_show_action_logs') }}"><i class="fa fa-circle"></i> Action Logs</a></li>
                      </ul>
                  </div>
                  <!-- Submenu end /-->
              </li>
            
                
              <!-- <li class="{{ Route::current()->getName() == 'admin_show_faq' ? 'active' : '' }}">
                <a href="{{ route('admin_show_faq') }}"><i class="fa fa-question-circle"></i><span>FAQs</span>
                </a>
              </li> -->
                
            </ul>
          </div>
    </nav>
    <!-- SIDBAR NAV MENU END  -->