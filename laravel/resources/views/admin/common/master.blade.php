<!doctype html>
<html lang="en">
<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />
    <meta name="description" content="" />
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui" />
    
    <!-- FAVICONS ICON -->
    <link rel="icon" type="image/webp" href="{{asset('assets/admin/images/favicon-32x32.webp') }}" sizes="32x32" />
    <link rel="icon" type="image/webp" href="{{asset('assets/admin/images/favicon-16x16.webp') }}" sizes="16x16" />
    
    <title>ConMix - Admin</title>
    
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/admin/css/css_cache.php')}}" />
    @if(false)
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" href="{{asset('assets/common/css/shimmer.css')}}" async>
        <!-- BOOTSTRAP STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/bootstrap.min.css')}}">
        <!-- FONTAWESOME STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/font-awesome.min.css')}}"/>
        <!-- CUSTOM SCROLLBAR STYLE SHEET -->
        <link rel="preload" href="{{asset('assets/admin/css/jquery.mCustomScrollbar.css')}}">

        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/daterangepicker.css')}}" />

        <!-- MAIN STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/style.css')}}">
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/custom.css')}}">
        <!-- NEW THEME STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/new-theme-style.css')}}">

        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/common/css/jquery.toast.css')}}">

        <!-- SELECT2 CHOSEN STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/select2-chosen.css')}}">
    
        <!-- STAR RATING STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/css/star-rating-svg.css')}}">
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">

        <!-- SELECT2 MULTIPLE STYLE SHEET -->
        <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" async href="{{asset('assets/admin/plugins/select2/select2.min.css')}}">
    @endif

    <script type="text/javascript" src="{{asset('assets/admin/js/js_cache.php')}}" ></script>

    @if(false)
        <script type="text/javascript" src="{{asset('assets/admin/js/jquery-3.2.1.min.js')}}"></script>    


        <script type="text/javascript" src="{{asset('assets/admin/js/moment.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/daterangepicker.min.js')}}"></script> 


        <script type="text/javascript" src="{{asset('assets/admin/js/jquery.star-rating-svg.js')}}"></script>

        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    @endif
    
    

</head>
<script>

var BASE_URL = '{{ url('/') }}';
// console.log("BASE_URL..."+BASE_URL);

</script>