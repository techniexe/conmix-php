</body>

@if(false)
<!-- JavaScript -->

<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/jquery.mCustomScrollbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/characterlimit.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/common/js/utils.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/admin.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/intlTelInput.js')}}"></script>

 <!-- Vehicle list page script -->
<script type="text/javascript" src="{{asset('assets/admin/js/jquery.lightbox.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/jquery.fileuploader.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/select2.jquery.min.js') }}"></script>
<!-- Vehicle list page script -->

<!-- chart script -->
<script type="text/javascript" src="{{asset('assets/admin/js/charts3.2.1.min.js')}}"></script>
<!-- Toast -->
<script type="text/javascript" src="{{asset('assets/common/js/jquery.toast.js')}}"></script>
<!-- Validation JS -->
<script type="text/javascript" src="{{asset('assets/admin/js/form.validate.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/common/js/jquery.validate.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/common/js/additional-methods.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/common/js/locationpicker.jquery.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/admin/js/editor.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/admin/plugins/select2/select2.min.js')}}"></script>

@endif
<script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDgc0AfDO-QPVxfx03eXhLBphZ3CKwYp5o&sensor=false&libraries=places'></script>

<script>
$( function() {
    setTimeout(removeLoader); //wait for page load PLUS two seconds.
});
function removeLoader(){
    $( "#loadingDiv" ).fadeOut(500, function() {
        // fadeOut complete. Remove the loading div
        $( "#loadingDiv" ).remove(); //makes page more lightweight
    });
}
 $(document).ready(function() {
    /*input mobile no with flag*/
    // var input = document.querySelector("#mobileno");
    // window.intlTelInput(input, {
    //   utilsScript: "{{asset('assets/admin/js/utils.js')}}" // just for formatting/placeholders etc
    // });
    /*input mobile no with flag end*/


    /*text-editor script start*/
    $("#txtEditor").Editor();

    
    /*text-editor script end*/
});


// Vehicle list page script

$('.form-control-chosen').chosen({
        width: '100%'
    });
    
/*drag N Drop file upload script start*/
    $(document).ready(function() {
      var drop_img_path = "{{asset('assets/admin/images/fileuploader-dragdrop-icon.png')}}";
        $('input[name="files"]').fileuploader({changeInput:'<div class="fileuploader-input"><div class="fileuploader-input-inner"><img src="'+drop_img_path+'"><h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3><p>or</p><div class="site-button"><span>Browse Files</span></div></div></div>',theme:"dragdrop",upload:{url:"",data:null,type:"POST",enctype:"multipart/form-data",start:!0,synchron:!0,beforeSend:null,onSuccess:function(e,a){var n=JSON.parse(e);n.isSuccess&&n.files[0]&&(a.name=n.files[0].name),a.html.find(".column-actions").append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>'),setTimeout(function(){a.html.find(".progress-bar2").fadeOut(400)},400)},onError:function(e){var a=e.html.find(".progress-bar2");a.length>0&&(a.find("span").html("0%"),a.find(".fileuploader-progressbar .bar").width("0%"),e.html.find(".progress-bar2").fadeOut(400)),"cancelled"!=e.upload.status&&0==e.html.find(".fileuploader-action-retry").length&&e.html.find(".column-actions").prepend('')},onProgress:function(e,a){var n=a.html.find(".progress-bar2");n.length>0&&(n.show(),n.find("span").html(e.percentage+"%"),n.find(".fileuploader-progressbar .bar").width(e.percentage+"%"))},onComplete:null},onRemove:function(e){$.post("",{file:e.name})},captions:{feedback:"Drag and drop files here",feedback2:"Drag and drop files here",drop:"Drag and drop files here"}});
    
        $('input[name="attachments"]').fileuploader({
          limit: 20,
          maxSize: 50,
        });
      });
/*drag N Drop file upload script end -- //<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>*/

// Vehicle list page script Over


function showToast(message,type){

  $.toast({
    heading: ''+type,
    text: ''+message,
    showHideTransition: 'fade',
    icon: ''+type.toLowerCase(),
    position: 'top-right',
  });

}

function reload(){
  location.reload();
  // setTimeout(function() { location.reload(); }, 1000);

}

function confirmDialog(message, onConfirm){

  var fClose = function(){
    modal.modal("hide");
  };
  var modal = $("#confirmModal");
  modal.modal("show");
  $("#confirmMessage").empty().append(message);
  $("#confirmOk").unbind().one('click', onConfirm).one('click', fClose);
  $("#confirmCancel").unbind().one("click", fClose);
  
}

function otpDialog(onConfirm){

  var fClose = function(){
    modal.modal("hide");
  };
  var modal = $("#otpModal");
  modal.modal("show");
  
  $("#otpOk").unbind().one('click', onConfirm);
  $("#otpCancel").unbind().one("click", fClose);

}

function plantOTPDialog(onConfirm){

  var fClose = function(){
    modal.modal("hide");
  };
  var modal = $("#Plant_otpModal");
  modal.modal("show");

  $("#plant_otpOk").unbind().one('click', onConfirm);
  $("#plant_otpCancel").unbind().one("click", fClose);

}


$('#us2').locationpicker({
enableAutocomplete: true,
    enableReverseGeocode: true,
  radius: 0,
  inputBinding: {
    latitudeInput: $('#us2_lat'),
    longitudeInput: $('#us2_lon'),
    radiusInput: $('#us2-radius'),
    locationNameInput: $('#us2_address')
  },
  onchanged: function (currentLocation, radius, isMarkerDropped) {
        // var addressComponents = $(this).locationpicker('map').location.addressComponents;
    console.log(currentLocation);  //latlon  
    // updateControls(addressComponents); //Data
    }
});

function updateControls(addressComponents) {
  console.log(addressComponents);
}


$(document).ready(function(){
  
var url_param = getParamsForFilter(window.location.href);

// console.log("url_param..."+url_param);

if(url_param == '=undefined'){
  console.log("url_param..."+url_param);
  $(".clear_filter_button").hide();
}else{
  $(".clear_filter_button").show();
}

});

jQuery( document ).ready(function( $ ) {

//Use this inside your document ready jQuery 
$(window).on('popstate', function() {
   location.reload(true);
});

});
</script>


</html>