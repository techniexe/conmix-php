  <!-- Dashboard -->

<!-- ALERT MESSAGE START -->
<div id="accept-alert-popup" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15">
        Are you sure you want to accept bill?
      </div>
      <div class="modal-footer text-center">
          <button type="button" data-dismiss="modal" class="site-button green m-r15">Yes</button>
          <button type="button" data-dismiss="modal" class="site-button red">No</button>
      </div>
    </div>
  </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- ALERT MESSAGE START -->
<div id="approve-alert-popup" class="alert-message modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body p-tb20 p-lr15">
            Are you sure you want to approve?
        </div>
        <div class="modal-footer text-center">
            <button type="button" data-dismiss="modal" class="site-button green m-r15">Yes</button>
            <button type="button" data-dismiss="modal" class="site-button red">No</button>
        </div>
        </div>
    </div>
</div>
<!-- ALERT MESSAGE END -->

<!-- Dashboard Over -->

<!-- Confirmation Popup MESSAGE START -->
<div id="confirmModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="confirmMessage">
        Are you sure you want to accept bill?
      </div>
      <div class="modal-footer text-center">
          <button type="button" data-dismiss="modal" class="site-button green m-r15" id="confirmOk">Yes</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="confirmCancel">No</button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation Popup MESSAGE END -->

<!-- Confirmation Popup MESSAGE START -->
<div id="otpModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr10" id="otpMessage">
        <label>Enter the 6 digits OTP sent on superadmin's mobile no. <span id="OTP_mob_no"></span> <span class="str">*</span></label>
        <input type="text" id="otp" required class="form-control" placeholder="One Time Password">
        <p id="otp_error" style="color:red;"></p>
        <div class="text-gray-dark resend_otp_div text-left">
            <!-- <form action=""  name="" method="">
                @csrf -->
                <!-- <input class="form-control" name="" value="" required  type="hidden"> -->

                <div class="timer_otp mb-3">
                    <span id="timer_forgot_pass">
                        <span id="time_forgot_pass">02:00</span>
                    </span>
                </div>
            <!-- </form> -->
        </div>
      </div>
      <div class="modal-footer text-center">
          <button type="button" class="site-button green m-r15" id="otpOk">Submit</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="otpCancel">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Confirmation Popup MESSAGE END -->

<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>

<!-- Admin User Listing -->

<!-- MODAL ADD USER START-->
<div id="add-user-popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="admin_dialog_popup_title">Add User</h4>
          </div>
          <form action="" method="post" name="add_user_form" id="add_admin_user">
          <div class="modal-body p-tb20 p-lr15">

              @csrf
                <input type="hidden" name="form_type" id="add_user_form_type" value="add"/>
                <input type="hidden" name="authentication_code" id="admin_edit_form_authentication_code" value="add"/>
                <input type="hidden" name="_id" id="user_id" value=""/>
                <div class="form-group">
                    <div class="input-group">
                        <label>Full Name <span class="str">*</span></label>
                        <input type="text" name="name" id="admin_user_name" class="form-control text-capitalize" placeholder="e.g. Jhondoe">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <label>Email <span class="str">*</span></label>
                        <input type="email" name="email" id="admin_email" class="form-control" placeholder="e.g. jhon@example.com">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <label>Mobile No. <span class="str">*</span></label>
                        <input name="mobile" class="form-control mobile_validate" value="" id="mobileno" placeholder="e.g. 9898989898">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group" id="admin_type_div">
                        <label>Admin Type <span class="str">*</span></label>
                        <div class="cstm-select-box">
                            <select name="admin_type" id="admin_user_role">
                                <option disabled="disabled" selected="selected">Select Role</option>
                                <option value="admin_manager">Admin Manager</option>
                                <option value="admin_sales_marketing">Sales & Marketing Admin</option>
                                <!-- <option value="admin_sales">Sales Admin</option> -->
                                <!-- <option value="logistics_admin">Logistics Admin</option> -->
                                <option value="admin_accounts">Accounts Admin</option>
                                <option value="admin_customer_care_grievances">Customer Care & Grievances Admin</option>
                                <!-- <option value="admin_grievances">Grievances Admin</option> -->

                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <label>Password <span class="str">*</span></label>
                        <input type="password" name="password" id="admin_user_password" class="form-control" placeholder="e.g. Enter Password">
                    </div>
                </div>

          </div>
          <div class="modal-footer text-left">
            <!-- <button class="site-button m-r20">Save</button> -->
            <input type="submit" name="add_admin_user" class="site-button m-r20" value="Save"/>
            <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
          </form>
        </div>
      </div>
    </div>
<!-- MODAL ADD USER END-->

<!-- Admin User listing Over -->

<!-- Vehicle Module Start -->

@if(Route::current()->getName() == 'admin_show_TM_category')
<!-- MODAL ADD VEHICAL CATEGORY START-->
<div id="add-TM-category" class="modal fade" role="dialog">
      <div class="modal-dialog">

          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
              <h4 class="modal-title" id="add_TM_category_title">Edit TM Category</h4>
            </div>
            <form action="" name="TM_add_cat_form" id="TM_add_cat_form" method="post">
          @csrf
            <div class="modal-body p-tb20 p-lr15">

                  <div class="form-group">
                      <div class="input-group">
                          <label>TM Category <span class="str">*</span></label>
                          <!-- <div class="cstm-select-box"> -->
                              <!-- <select>
                                  <option disabled="disabled" selected="selected">Select vehicle category</option>
                                  <option>Tipper</option>
                                  <option>Trailer</option>
                              </select> -->
                              <input type ="hidden" name="form_type" id="TM_add_cat_form_type" value="add"/>
                              <input type ="hidden" name="cat_id" id="cat_id" value=""/>
                              <!-- <input type ="hidden" name="authentication_code" id="add_category_authentication_code" value=""/> -->
                              <input type="text" name="category_name" id="TM_category_name" required class="form-control text-capitalize" placeholder="e.g. 10 Tyre">
                              <div class="invalid-feedback" id="category_name_error"></div>
                          <!-- </div> -->
                      </div>
                  </div>

            </div>
            <div class="modal-footer text-left">
              <input type="submit" value="Save" name="vehicle_cat_save" class="site-button m-r20"/>
              <!-- <button class="site-button m-r20">Save</button> -->
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
            </form>
          </div>

      </div>
    </div>
<!-- MODAL ADD VEHICAL CATEGORY END-->
@endif

@if(Route::current()->getName() == 'admin_show_TM_sub_category')

  <!-- MODAL SUB VEHICAL CATEGORY START-->
  <div id="TM-sub-category" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="TM_sub_category_title">Add TM Sub Category</h4>
          </div>
          <form action="" name="TM_add_sub_cat_form" method="post">
          @csrf
            <div class="modal-body p-tb20 p-lr15">
                  <div class="form-group">
                      <div class="input-group" id="TM_cat_div">
                          <label>TM Category <span class="str">*</span></label>
                          <div class="cstm-select-box">
                              <select name="TM_cat_id" id="TM_cat_id">
                                  <option disabled="disabled" selected="selected">Select TM Category</option>
                                  @if(isset($data["vehicle_categoies"]["data"]))
                                    @foreach($data["vehicle_categoies"]["data"] as $cat_value)
                                      <option value="{{ $cat_value['_id'] }}">{{ $cat_value["category_name"] }}</option>
                                    @endforeach
                                  @endif

                              </select>
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="input-group">
                          <label>Sub Category <span class="str">*</span></label>
                          <input type="text" name="sub_category_name" id="sub_category_name" class="form-control text-capitalize" placeholder="e.g. 4 cu.Mtr, 6 cu.Mtr" />
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="input-group">
                          <label>TM Capacity (Cu.Mtr) <span class="str">*</span></label>
                          <input type="text" name="TM_capacity" id="TM_capacity" class="form-control" placeholder="e.g. 4,6,8" />
                      </div>
                  </div>
                  <!-- <div class="form-group">
                      <div class="input-group">
                          <label>Max Load Capacity (MT) <span class="str">*</span></label>
                          <input type="text" name="max_load_capacity" id="max_load_capacity" class="form-control" placeholder="e.g. 30" />
                      </div>
                  </div> -->
                  <div class="form-group">
                      <div class="input-group">
                          <label>Weight Unit Abbreviations <span class="str">*</span></label>
                          <input type="text" name="weight_unit_code" value="Cu.Mtr" readOnly id="weight_unit_code" class="form-control" placeholder="e.g. Cu.Mtr" />
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="input-group">
                          <label>Weight Unit Full Form <span class="str">*</span></label>
                          <input type="text" name="weight_unit" value="Cubic Meter" readOnly id="weight_unit" class="form-control" placeholder="e.g. Cubic Meter" />
                      </div>
                  </div>
                  <input type="hidden" name="add_sub_category_form_type" id="TM_add_sub_cat_form_type" value="add"/>
                  <input type="hidden" name="TM_sub_cat_id" id="TM_sub_cat_id" value=""/>

            </div>
            <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div>
<!-- MODAL SUB VEHICAL CATEGORY END-->

@endif


@if(Route::current()->getName() == 'admin_show_pump_category')
<!-- MODAL ADD VEHICAL CATEGORY START-->
<div id="add-pump-category" class="modal fade" role="dialog">
      <div class="modal-dialog">

          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
              <h4 class="modal-title" id="add_pump_category_title">Add Concrete Pump Category</h4>
            </div>
            <form action="" name="pump_add_cat_form" id="pump_add_cat_form" method="post">
          @csrf
            <div class="modal-body p-tb20 p-lr15">

                  <div class="form-group">
                      <div class="input-group">
                          <label>Concrete Pump Category <span class="str">*</span></label>
                          <!-- <div class="cspump-select-box"> -->
                              <!-- <select>
                                  <option disabled="disabled" selected="selected">Select vehicle category</option>
                                  <option>Tipper</option>
                                  <option>Trailer</option>
                              </select> -->
                              <input type ="hidden" name="form_type" id="pump_add_cat_form_type" value="add"/>
                              <input type ="hidden" name="cat_id" id="cat_id" value=""/>
                              <!-- <input type ="hidden" name="authentication_code" id="add_category_authentication_code" value=""/> -->
                              <input type="text" name="category_name" id="pump_category_name" required class="form-control text-capitalize" placeholder="e.g. Truck Mounted">
                              <div class="invalid-feedback" id="category_name_error"></div>
                          <!-- </div> -->
                      </div>
                  </div>

            </div>
            <div class="modal-footer text-left">
              <input type="submit" value="Save" name="vehicle_cat_save" class="site-button m-r20"/>
              <!-- <button class="site-button m-r20">Save</button> -->
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
            </form>
          </div>

      </div>
    </div>
<!-- MODAL ADD VEHICAL CATEGORY END-->
@endif


@if(Route::current()->getName() == 'admin_show_vehicle_list')

<!-- MODAL VIEW MAP START-->
<div id="view-map-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header p-a0 border-0">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body p-tb20 p-lr15">
            <div class="popup-map-content-block" id="map_iframe_div">
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5864.520301024851!2d72.51392826831795!3d23.048447656590255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e9cb1d6441583%3A0x99c9230212282810!2sSCC+Infrastructure+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1551245667065" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                  <iframe src="https://maps.google.com/maps?q=35.856737, 10.606619&z=15&output=embed" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- MODAL VIEW MAP END-->



@endif

@if(Route::current()->getName() == 'admin_show_vehicle_pm_km_rate')
<!-- MODAL ADD VEHICAL CATEGORY START-->
<div id="add-vehical-pm-km" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
              <h4 class="modal-title" id="vehical_pm_km_title">Add Per PM/KM Rate</h4>
            </div>
            <form action="" name="vehical_pm_km_form" id="vehical_pm_km_form" method="post">
          @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="form-group">
                    <div class="input-group">
                        <label>Select State <span class="str">*</span></label>
                        <div class="cstm-select-box">
                            <select name="state_id" id="vehicle_pmkm_state_id">
                                <option disabled="disabled" selected="selected">Select State Name</option>
                                @if(isset($state_data["data"]))
                                    @foreach($state_data["data"] as $value)
                                        <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <label>Select Region <span class="str">*</span></label>
                        <div class="cstm-select-box">
                            <select name="region_type" id="pmkm_region_type">
                                <option disabled="disabled" selected="selected">Select Region Type</option>
                                <option value="PLAIN">Plain</option>
                                <option value="HILLY">Hilly</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Per MT/KM Rate (<i class="fa fa-rupee"></i>) <span class="str">*</span></label>
                    <input type="text" name="per_metric_ton_per_km_rate" id="per_metric_ton_per_km_rate"  class="form-control" placeholder="e.g. 5">
                    <input type="hidden" name="vehical_pm_km_form_type" id="vehical_pm_km_form_type" value="add"  class="form-control">
                    <input type="hidden" name="vehical_pm_km_id" id="vehical_pm_km_id" value="add"  class="form-control">

                </div>
            </div>
            <div class="modal-footer text-left">
              <input type="submit" value="Save" name="vehicle_pm_km_save" class="site-button m-r20"/>
              <!-- <button class="site-button m-r20">Save</button> -->
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
            </form>
          </div>

      </div>
    </div>
<!-- MODAL ADD VEHICAL CATEGORY END-->
@endif


@if(Route::current()->getName() == 'admin_show_detail')

<!-- MODAL VIEW MAP START-->
<div id="view-map-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header p-a0 border-0">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body p-tb20 p-lr15">
            <div class="popup-map-content-block" id="map_iframe_div">
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5864.520301024851!2d72.51392826831795!3d23.048447656590255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e9cb1d6441583%3A0x99c9230212282810!2sSCC+Infrastructure+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1551245667065" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                  <iframe src="https://maps.google.com/maps?q=35.856737, 10.606619&z=15&output=embed" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- MODAL VIEW MAP END-->



@endif



<!-- Vehicle Module Over -->

<!-- Region Module Start -->

@if(Route::current()->getName() == 'admin_show_region_country')
<!-- MODAL COUNTRY START-->
<div id="country-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="country_popup_title">Add Country</h4>
          </div>
          <form action="" method="post" name="add_country_form">
            @csrf
          <div class="modal-body p-tb20 p-lr15">
              <div class="form-group">
                  <div class="input-group">
                      <label>Country Id <span class="str">*</span></label>
                      <input type="text" required name="country_id" id="country_id" class="form-control" placeholder="e.g. 103">
                  </div>
              </div>
              <div class="form-group" >
                  <div class="input-group single-search-select2" id="country_name_div">
                      <label>Country Name <span class="str">*</span></label>
                      <select name="country_name" id="country_name" required class="form-control-chosen" data-placeholder="Select Country Name">

                      <option value="Afghanistan">Afghanistan</option>
                      <option value="Albania">Albania</option>
                      <option value="Algeria">Algeria</option>
                      <option value="American Samoa">American Samoa</option>
                      <option value="Andorra">Andorra</option>
                      <option value="Angola">Angola</option>
                      <option value="Anguilla">Anguilla</option>
                      <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                      <option value="Argentina">Argentina</option>
                      <option value="Armenia">Armenia</option>
                      <option value="Aruba">Aruba</option>
                      <option value="Australia">Australia</option>
                      <option value="Austria">Austria</option>
                      <option value="Azerbaijan">Azerbaijan</option>
                      <option value="Bahamas">Bahamas</option>
                      <option value="Bahrain">Bahrain</option>
                      <option value="Bangladesh">Bangladesh</option>
                      <option value="Barbados">Barbados</option>
                      <option value="Belarus">Belarus</option>
                      <option value="Belgium">Belgium</option>
                      <option value="Belize">Belize</option>
                      <option value="Benin">Benin</option>
                      <option value="Bermuda">Bermuda</option>
                      <option value="Bhutan">Bhutan</option>
                      <option value="Bolivia">Bolivia</option>
                      <option value="Bonaire">Bonaire</option>
                      <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                      <option value="Botswana">Botswana</option>
                      <option value="Brazil">Brazil</option>
                      <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                      <option value="Brunei">Brunei</option>
                      <option value="Bulgaria">Bulgaria</option>
                      <option value="Burkina Faso">Burkina Faso</option>
                      <option value="Burundi">Burundi</option>
                      <option value="Cambodia">Cambodia</option>
                      <option value="Cameroon">Cameroon</option>
                      <option value="Canada">Canada</option>
                      <option value="Canary Islands">Canary Islands</option>
                      <option value="Cape Verde">Cape Verde</option>
                      <option value="Cayman Islands">Cayman Islands</option>
                      <option value="Central African Republic">Central African Republic</option>
                      <option value="Chad">Chad</option>
                      <option value="Channel Islands">Channel Islands</option>
                      <option value="Chile">Chile</option>
                      <option value="China">China</option>
                      <option value="Christmas Island">Christmas Island</option>
                      <option value="Cocos Island">Cocos Island</option>
                      <option value="Colombia">Colombia</option>
                      <option value="Comoros">Comoros</option>
                      <option value="Congo">Congo</option>
                      <option value="Cook Islands">Cook Islands</option>
                      <option value="Costa Rica">Costa Rica</option>
                      <option value="Cote DIvoire">Cote DIvoire</option>
                      <option value="Croatia">Croatia</option>
                      <option value="Cuba">Cuba</option>
                      <option value="Curaco">Curacao</option>
                      <option value="Cyprus">Cyprus</option>
                      <option value="Czech Republic">Czech Republic</option>
                      <option value="Denmark">Denmark</option>
                      <option value="Djibouti">Djibouti</option>
                      <option value="Dominica">Dominica</option>
                      <option value="Dominican Republic">Dominican Republic</option>
                      <option value="East Timor">East Timor</option>
                      <option value="Ecuador">Ecuador</option>
                      <option value="Egypt">Egypt</option>
                      <option value="El Salvador">El Salvador</option>
                      <option value="Equatorial Guinea">Equatorial Guinea</option>
                      <option value="Eritrea">Eritrea</option>
                      <option value="Estonia">Estonia</option>
                      <option value="Ethiopia">Ethiopia</option>
                      <option value="Falkland Islands">Falkland Islands</option>
                      <option value="Faroe Islands">Faroe Islands</option>
                      <option value="Fiji">Fiji</option>
                      <option value="Finland">Finland</option>
                      <option value="France">France</option>
                      <option value="French Guiana">French Guiana</option>
                      <option value="French Polynesia">French Polynesia</option>
                      <option value="French Southern Ter">French Southern Ter</option>
                      <option value="Gabon">Gabon</option>
                      <option value="Gambia">Gambia</option>
                      <option value="Georgia">Georgia</option>
                      <option value="Germany">Germany</option>
                      <option value="Ghana">Ghana</option>
                      <option value="Gibraltar">Gibraltar</option>
                      <option value="Great Britain">Great Britain</option>
                      <option value="Greece">Greece</option>
                      <option value="Greenland">Greenland</option>
                      <option value="Grenada">Grenada</option>
                      <option value="Guadeloupe">Guadeloupe</option>
                      <option value="Guam">Guam</option>
                      <option value="Guatemala">Guatemala</option>
                      <option value="Guinea">Guinea</option>
                      <option value="Guyana">Guyana</option>
                      <option value="Haiti">Haiti</option>
                      <option value="Hawaii">Hawaii</option>
                      <option value="Honduras">Honduras</option>
                      <option value="Hong Kong">Hong Kong</option>
                      <option value="Hungary">Hungary</option>
                      <option value="Iceland">Iceland</option>
                      <option value="Indonesia">Indonesia</option>
                      <option value="India">India</option>
                      <option value="Iran">Iran</option>
                      <option value="Iraq">Iraq</option>
                      <option value="Ireland">Ireland</option>
                      <option value="Isle of Man">Isle of Man</option>
                      <option value="Israel">Israel</option>
                      <option value="Italy">Italy</option>
                      <option value="Jamaica">Jamaica</option>
                      <option value="Japan">Japan</option>
                      <option value="Jordan">Jordan</option>
                      <option value="Kazakhstan">Kazakhstan</option>
                      <option value="Kenya">Kenya</option>
                      <option value="Kiribati">Kiribati</option>
                      <option value="Korea North">Korea North</option>
                      <option value="Korea Sout">Korea South</option>
                      <option value="Kuwait">Kuwait</option>
                      <option value="Kyrgyzstan">Kyrgyzstan</option>
                      <option value="Laos">Laos</option>
                      <option value="Latvia">Latvia</option>
                      <option value="Lebanon">Lebanon</option>
                      <option value="Lesotho">Lesotho</option>
                      <option value="Liberia">Liberia</option>
                      <option value="Libya">Libya</option>
                      <option value="Liechtenstein">Liechtenstein</option>
                      <option value="Lithuania">Lithuania</option>
                      <option value="Luxembourg">Luxembourg</option>
                      <option value="Macau">Macau</option>
                      <option value="Macedonia">Macedonia</option>
                      <option value="Madagascar">Madagascar</option>
                      <option value="Malaysia">Malaysia</option>
                      <option value="Malawi">Malawi</option>
                      <option value="Maldives">Maldives</option>
                      <option value="Mali">Mali</option>
                      <option value="Malta">Malta</option>
                      <option value="Marshall Islands">Marshall Islands</option>
                      <option value="Martinique">Martinique</option>
                      <option value="Mauritania">Mauritania</option>
                      <option value="Mauritius">Mauritius</option>
                      <option value="Mayotte">Mayotte</option>
                      <option value="Mexico">Mexico</option>
                      <option value="Midway Islands">Midway Islands</option>
                      <option value="Moldova">Moldova</option>
                      <option value="Monaco">Monaco</option>
                      <option value="Mongolia">Mongolia</option>
                      <option value="Montserrat">Montserrat</option>
                      <option value="Morocco">Morocco</option>
                      <option value="Mozambique">Mozambique</option>
                      <option value="Myanmar">Myanmar</option>
                      <option value="Nambia">Nambia</option>
                      <option value="Nauru">Nauru</option>
                      <option value="Nepal">Nepal</option>
                      <option value="Netherland Antilles">Netherland Antilles</option>
                      <option value="Netherlands">Netherlands (Holland, Europe)</option>
                      <option value="Nevis">Nevis</option>
                      <option value="New Caledonia">New Caledonia</option>
                      <option value="New Zealand">New Zealand</option>
                      <option value="Nicaragua">Nicaragua</option>
                      <option value="Niger">Niger</option>
                      <option value="Nigeria">Nigeria</option>
                      <option value="Niue">Niue</option>
                      <option value="Norfolk Island">Norfolk Island</option>
                      <option value="Norway">Norway</option>
                      <option value="Oman">Oman</option>
                      <option value="Pakistan">Pakistan</option>
                      <option value="Palau Island">Palau Island</option>
                      <option value="Palestine">Palestine</option>
                      <option value="Panama">Panama</option>
                      <option value="Papua New Guinea">Papua New Guinea</option>
                      <option value="Paraguay">Paraguay</option>
                      <option value="Peru">Peru</option>
                      <option value="Phillipines">Philippines</option>
                      <option value="Pitcairn Island">Pitcairn Island</option>
                      <option value="Poland">Poland</option>
                      <option value="Portugal">Portugal</option>
                      <option value="Puerto Rico">Puerto Rico</option>
                      <option value="Qatar">Qatar</option>
                      <option value="Republic of Montenegro">Republic of Montenegro</option>
                      <option value="Republic of Serbia">Republic of Serbia</option>
                      <option value="Reunion">Reunion</option>
                      <option value="Romania">Romania</option>
                      <option value="Russia">Russia</option>
                      <option value="Rwanda">Rwanda</option>
                      <option value="St Barthelemy">St Barthelemy</option>
                      <option value="St Eustatius">St Eustatius</option>
                      <option value="St Helena">St Helena</option>
                      <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                      <option value="St Lucia">St Lucia</option>
                      <option value="St Maarten">St Maarten</option>
                      <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                      <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                      <option value="Saipan">Saipan</option>
                      <option value="Samoa">Samoa</option>
                      <option value="Samoa American">Samoa American</option>
                      <option value="San Marino">San Marino</option>
                      <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                      <option value="Saudi Arabia">Saudi Arabia</option>
                      <option value="Senegal">Senegal</option>
                      <option value="Seychelles">Seychelles</option>
                      <option value="Sierra Leone">Sierra Leone</option>
                      <option value="Singapore">Singapore</option>
                      <option value="Slovakia">Slovakia</option>
                      <option value="Slovenia">Slovenia</option>
                      <option value="Solomon Islands">Solomon Islands</option>
                      <option value="Somalia">Somalia</option>
                      <option value="South Africa">South Africa</option>
                      <option value="Spain">Spain</option>
                      <option value="Sri Lanka">Sri Lanka</option>
                      <option value="Sudan">Sudan</option>
                      <option value="Suriname">Suriname</option>
                      <option value="Swaziland">Swaziland</option>
                      <option value="Sweden">Sweden</option>
                      <option value="Switzerland">Switzerland</option>
                      <option value="Syria">Syria</option>
                      <option value="Tahiti">Tahiti</option>
                      <option value="Taiwan">Taiwan</option>
                      <option value="Tajikistan">Tajikistan</option>
                      <option value="Tanzania">Tanzania</option>
                      <option value="Thailand">Thailand</option>
                      <option value="Togo">Togo</option>
                      <option value="Tokelau">Tokelau</option>
                      <option value="Tonga">Tonga</option>
                      <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                      <option value="Tunisia">Tunisia</option>
                      <option value="Turkey">Turkey</option>
                      <option value="Turkmenistan">Turkmenistan</option>
                      <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                      <option value="Tuvalu">Tuvalu</option>
                      <option value="Uganda">Uganda</option>
                      <option value="United Kingdom">United Kingdom</option>
                      <option value="Ukraine">Ukraine</option>
                      <option value="United Arab Erimates">United Arab Emirates</option>
                      <option value="United States of America">United States of America</option>
                      <option value="Uraguay">Uruguay</option>
                      <option value="Uzbekistan">Uzbekistan</option>
                      <option value="Vanuatu">Vanuatu</option>
                      <option value="Vatican City State">Vatican City State</option>
                      <option value="Venezuela">Venezuela</option>
                      <option value="Vietnam">Vietnam</option>
                      <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                      <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                      <option value="Wake Island">Wake Island</option>
                      <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                      <option value="Yemen">Yemen</option>
                      <option value="Zaire">Zaire</option>
                      <option value="Zambia">Zambia</option>
                      <option value="Zimbabwe">Zimbabwe</option>
                      <option value="new">New Country</option>
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <div class="input-group">
                      <label>Country Code <span class="str">*</span></label>
                      <input type="text" required name="country_code" id="country_code" class="form-control" placeholder="e.g. IN">
                  </div>
              </div>

          </div>
          <input type="hidden" name="add_country_form_type" id="add_country_form_type" value="add"/>
          <input type="hidden" name="country_original_id" id="country_original_id" value=""/>
          <div class="modal-footer text-left">
              <button type="submit" name="add_country_submit_form" id="add_country_submit_form" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
          </form>
      </div>

    </div>
</div>
<script>
    $('.form-control-chosen').chosen({
        width: '100%'
    });
</script>
<!-- MODAL COUNTRY END-->
@endif

@if(Route::current()->getName() == 'admin_show_region_states')

<!-- MODAL STATE START-->
<div id="state-popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="state_popup_title">Add State</h4>
          </div>
          <form action="" name="add_states_form" method="post">
          @csrf
            <div class="modal-body p-tb20 p-lr15">

              <div class="form-group">
                  <div class="input-group" id="country_name_div">
                      <label>Country Name <span class="str">*</span></label>
                      <select id="country_name_states" name="country_name_select" class="form-control" data-placeholder="Select Country Name">
                          <option value="">Select country</option>
                          @if(isset($data["countries"]))
                            @foreach($data["countries"] as $value)
                              <option value="{{ $value['country_name'].'_'.$value['country_id'].'_'.$value['country_code'] }}" data-id="{{ $value['country_id'] }}" data-code="{{ $value['country_code'] }}">{{ $value["country_name"] }}</option>
                            @endforeach
                          @endif
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <div class="input-group">
                      <label>State Name <span class="str">*</span></label>
                      <input type="text" name="state_name" id="state_name" class="form-control text-capitalize" placeholder="e.g. Gujarat">
                  </div>
              </div>

            </div>
            <input type="hidden" name="country_name" id="state_country_name"/>
            <input type="hidden" name="country_id" id="state_country_id"/>
            <input type="hidden" name="country_code" id="state_country_code"/>
            <input type="hidden" name="add_state_form_type" id="add_states_form_type" value="add"/>
            <input type="hidden" name="state_original_id" id="state_original_id" value=""/>
            <div class="modal-footer text-left">
                  <button type="submit" class="site-button m-r20">Save</button>
                  <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div>
<!-- MODAL STATE END-->

@endif


@if(Route::current()->getName() == 'admin_show_region_cities')

<!-- MODAL CITY START-->
<div id="city-popup" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title" id="city_popup_title">Add City</h4>
            </div>
            <form action="" name="add_city_form" method="post">
            @csrf
                <div class="modal-body p-tb20 p-lr15">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="state_name_div">
                                    <label>State Name <span class="str">*</span></label>
                                    <select id="state_name_city_1" name="state_id" class="form-control" data-placeholder="Select State Name">
                                        <option value="">Select State</option>
                                        @if(isset($data["states"]))
                                            @foreach($data["states"] as $value)
                                                <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label>City Name <span class="str">*</span></label>
                                    <input type="text" name="city_name" id="city_name" class="form-control text-capitalize" placeholder="e.g. Ahmedabad">
                                </div>
                            </div>

                            <input type="hidden" name="add_city_form_type" id="add_city_form_type" value="add"/>
                            <input type="hidden" name="city_original_id" id="city_original_id" value=""/>
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Location <span class="str">*</span></label>
                                    <input type="text" required name="us2_address" id="us2_address" class="form-control" onkeydown="return event.key != 'Enter';"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Latitude</label>
                                            <input type="text" name="us2_lat" id="us2_lat" class="form-control" readonly />
                                            <p class="error" id="us2_lat_error"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Longitude</label>
                                            <input type="text" name="us2_lon" id="us2_lon" class="form-control" readonly />
                                            <p class="error" id="us2_lon_error"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="mb-3" id="us2" style="width: 100%; height: 350px;"></div>
                        </div>
                    </div>
                    <div class="modal-footer text-left">
                        <button type="submit" class="site-button m-r20">Save</button>
                        <button class="site-button gray" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL CITY END-->


@endif

<!-- Region Module Over -->


<!-- Grade Brand Module Over -->

@if(Route::current()->getName() == 'admin_show_concrete_grade')

<!-- MODAL PRODUCT CATEGORY START-->
<div id="concrete-grade-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="concrete_grade_popup_title">Add Concrete Grade</h4>
        </div>
        <form action="" name="add_grade_form" method="post" enctype="multipart/form-data">
        @csrf
          <div class="modal-body p-tb20 p-lr15">

              <div class="form-group">
                  <div class="input-group">
                      <label>Concrete Grade Name <span class="str">*</span></label>
                      <input type="text" id="grade_name" name="grade_name" class="form-control text-capitalize" placeholder="e.g. M10, M20">
                  </div>
              </div>
              <!-- <div class="form-group">
                  <div class="input-group">
                      <label>Product Image <span class="str">*</span></label>
                      <div class="file-browse">
                          <span class="button-browse">
                              Browse <input type="file" name="product_img"/>
                          </span>
                          <input type="text" name="image_name" id="product_img_text" class="form-control browse-input" placeholder="e.g. png, jpg" readonly>
                      </div>
                  </div>
              </div> -->
              <input type="hidden" name="add_grade_form_type" id="add_grade_form_type" value="add"/>
              <input type="hidden" name="grade_id" id="grade_id" value=""/>
              <!-- <input type="hidden" name="authentication_code" id="product_cat_authentication_code" value=""/> -->
          </div>
          <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->



@endif

@if(Route::current()->getName() == 'admin_show_cement_brand')

<!-- MODAL PRODUCT CATEGORY START-->
<div id="cement-brand-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="cement_brand_popup_title">Add Cement Brand</h4>
        </div>
        <form action="" name="cement_brand_form" id="cement_brand_form" method="post" enctype="multipart/form-data">
        @csrf
          <div class="modal-body p-tb20 p-lr15">

              <div class="form-group">
                  <div class="input-group">
                      <label>Cement Brand Name <span class="str">*</span></label>
                      <input type="text" id="brand_name" name="brand_name" class="form-control text-capitalize" placeholder="e.g. JK, UltraTech">
                  </div>
              </div>
              <div class="form-group">
                  <div class="input-group">
                      <label>Brand Image <span class="str">*</span></label>
                      <div class="file-browse">
                          <span class="button-browse">
                              Browse <input type="file" name="brand_img"/>
                          </span>
                          <input type="text" name="image_name" id="brand_img_text" class="form-control browse-input" placeholder="e.g. jpg" readonly>
                      </div>
                  </div>
              </div>
              <input type="hidden" name="cement_brand_form_type" id="cement_brand_form_type" value="add"/>
              <input type="hidden" name="brand_id" id="brand_id" value=""/>
              <!-- <input type="hidden" name="authentication_code" id="product_cat_authentication_code" value=""/> -->
          </div>
          <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->



@endif

@if(Route::current()->getName() == 'admin_show_admixture_brand')

<!-- MODAL PRODUCT CATEGORY START-->
<div id="admixture-brand-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="admixture_brand_popup_title">Add Admixture Brand</h4>
        </div>
        <form action="" name="admixture_brand_form" id="admixture_brand_form" method="post" enctype="multipart/form-data">
        @csrf
          <div class="modal-body p-tb20 p-lr15">

              <div class="form-group">
                  <div class="input-group">
                      <label>Admixture Brand Name <span class="str">*</span></label>
                      <input type="text" id="brand_name" name="brand_name" class="form-control text-capitalize" placeholder="e.g. Fosroc, Sika">
                  </div>
              </div>
              <div class="form-group">
                  <div class="input-group">
                      <label>Brand Image <span class="str">*</span></label>
                      <div class="file-browse">
                          <span class="button-browse">
                              Browse <input type="file" name="brand_img"/>
                          </span>
                          <input type="text" name="image_name" id="brand_img_text" class="form-control browse-input" placeholder="e.g. jpg" readonly>
                      </div>
                  </div>
              </div>
              <input type="hidden" name="admixture_brand_form_type" id="admixture_brand_form_type" value="add"/>
              <input type="hidden" name="brand_id" id="brand_id" value=""/>
              <!-- <input type="hidden" name="authentication_code" id="product_cat_authentication_code" value=""/> -->
          </div>
          <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->



@endif

<!-- Grade Brand Module Over -->
<!-- Product Module Over -->

@if(Route::current()->getName() == 'admin_show_product_category')

<!-- MODAL PRODUCT CATEGORY START-->
<div id="product-category-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="product_category_popup_title">Add Product Category List</h4>
        </div>
        <form action="" name="add_product_cat_form" id="add_product_cat_form" method="post" enctype="multipart/form-data">
        @csrf
          <div class="modal-body p-tb20 p-lr15">

              <div class="form-group">
                  <div class="input-group">
                      <label>Category Name <span class="str">*</span></label>
                      <input type="text" id="product_cat_name" name="category_name" class="form-control text-capitalize" placeholder="e.g. Aggregate, Sand">
                  </div>
              </div>
              <div class="form-group">
                  <div class="input-group">
                      <label>Product Image <span class="str">*</span></label>
                      <div class="file-browse">
                          <span class="button-browse">
                              Browse <input type="file" name="product_img"/>
                          </span>
                          <input type="text" name="image_name" id="product_img_text" class="form-control browse-input" placeholder="e.g. jpg" readonly>
                      </div>
                  </div>
              </div>
              <input type="hidden" name="add_product_cat_form_type" id="add_product_cat_form_type" value="add"/>
              <input type="hidden" name="product_cat_id" id="product_cat_id" value=""/>
              <input type="hidden" name="authentication_code" id="product_cat_authentication_code" value=""/>
          </div>
          <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->



@endif

@if(Route::current()->getName()== 'admin_show_product_sub_category')

<!-- MODAL PRODUCT SUB CATEGORY START-->
<div id="product-sub-category-popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="product_sub_category_popup_title">Add Product Sub Category List</h4>
          </div>
          <form action="" name="add_product_sub_cat_form" id="add_product_sub_cat_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="modal-body p-tb20 p-lr15">
                  <!-- <form action="" method="get"> -->

                    <div class="form-group">
                      <div class="input-group single-search-select2" id="category_id_div">
                          <label>Category Name <span class="str">*</span></label>
                          <select id="product_category_subcategory_list_add_form" name="categoryId" class="form-control-chosen" data-placeholder="Select Product Category">
                              <option></option>
                              @if(isset($data["product_cat_data"]))
                                  @foreach($data["product_cat_data"] as $value)
                                      <option value="{{ $value['_id'] }}">{{ $value["category_name"] }}</option>
                                  @endforeach
                              @endif

                          </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Sub Category <span class="str">*</span></label>
                            <input type="text" name="sub_category_name" id="sub_category_name" class="form-control" placeholder="e.g. 10 mm">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group" id="gst_slab_id_div">
                            <label>GST Slab <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="gst_slab_id" id="gst_slab_id">
                                    <option disabled="disabled" selected="selected">Select GST Slab</option>
                                    @if(isset($data["gst_slab_data"]))
                                      @foreach($data["gst_slab_data"] as $key => $value)
                                        <option value="{{ $value['_id'] }}">{{ $value["title"] }}</option>
                                      @endforeach
                                    @endif

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group" id="margin_rate_id_div">
                            <label>Margin Rate <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="margin_rate_id" id="margin_rate_id">
                                    <option disabled="disabled" selected="selected">Select Margin Rate</option>
                                    @if(isset($data["margin_rate_data"]))
                                      @foreach($data["margin_rate_data"] as $key => $value)
                                        <option value="{{ $value['_id'] }}">{{ $value["title"] }}</option>
                                      @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Qty Unit </label>
                            <div class="input-group">
                                <select name="selling_unit[]" id="selling_unit" class="form-control" data-placeholder="Select Qty Unit">
                                    <option value="" disabled>Select Unit</option>
                                    <!-- <option value="pcs">PCS</option>
                                    <option value="nos">NOS</option>
                                    <option value="kg">KG</option> -->
                                    <option value="MT">MT</option>
                                    <!-- <option value="sqmeter">Sq. Meter</option>
                                    <option value="qtl">QTL</option> -->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Min. Qty Acceptance </label>
                            <input name="min_quantity" id="min_quantity" type="text" class="form-control" placeholder="e.g. 100">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Product Image <span class="str">*</span></label>
                            <div class="file-browse">
                                <span class="button-browse">
                                  Browse <input type="file" name="product_sub_cat_image" />
                                </span>
                                <input type="text"  id="product_sub_cat_image_text" class="form-control browse-input" placeholder="e.g.jpg, jpeg" readonly>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="add_product_sub_cat_form_type" id="add_product_sub_cat_form_type" value="add"/>
                    <input type="hidden" name="product_sub_cat_id" id="product_sub_cat_id" value=""/>
                    <input type="hidden" name="authentication_code" id="product_sub_cat_authentication_code" value=""/>
              </div>
              <div class="modal-footer text-left">
                    <button type="submit" class="site-button m-r20">Save</button>
                    <button class="site-button gray" data-dismiss="modal">Cancel</button>
              </div>
          </form>
        </div>
      </div>
    </div>
<!-- MODAL PRODUCT SUB CATEGORY END-->




@endif


<!-- Design Mix Module Start -->

@if(Route::current()->getName() == 'admin_show_product_list')



<!-- MODAL ADD Concrete Pump START-->
<div id="show-design-mix-details" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title" id="design_mix_popup_title">Design Mix</h4>
        </div>
        <div id="design_mix_body">

        </div>

      </div>
    </div>
  </div>
<!-- MODAL ADD BANK DETAIL END-->

@endif


@if(Route::current()->getName() == 'admin_show_product_list')
<!-- MODAL VIEW MAP START-->
<div id="view-map-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header p-a0 border-0">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body p-tb20 p-lr15">
            <div class="popup-map-content-block" id="map_iframe_div">

            </div>
        </div>
        </div>
    </div>
</div>
<!-- MODAL VIEW MAP END-->


<!-- MODAL ADD PRODUCT START-->
<div id="add-product-list" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
          <h4 class="modal-title">Edit Product</h4>
        </div>
        <form action="" name="add_product_form" method="post">
        @csrf
          <div class="modal-body p-tb20 p-lr15">
              <div class="vehicle-block vehicle-category-block">
                  <div class="row">
                      <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Product Name <span class="str">*</span></label>
                                  <input type="text" name="product_name" id="product_name" class="form-control" placeholder="e.g. superflow jointagg">
                              </div>
                          </div>
                      </div> -->

                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Category <span class="str">*</span></label>
                                  <div class="cstm-select-box">
                                      <select id="add_prodcut_category">
                                          <option disabled="disabled" selected="selected">Select Category</option>

                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Sub Category <span class="str">*</span></label>
                                  <div class="cstm-select-box">
                                      <select id="add_product_sub_cat">
                                          <option disabled="disabled" selected="selected">Select Sub category</option>

                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Unit Price</label>
                                  <input type="text" name="unit_price" id="unit_price" class="form-control" placeholder="e.g. 1000 Rs">
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Qty In Stock <span class="str">*</span></label>
                                  <input type="text" id="product_qty" class="form-control" placeholder="e.g. 100 MT">
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Min. Order Acceptance</label>
                                  <input type="text" name="minimum_order" id="minimum_order" class="form-control" placeholder="e.g. 1 MT">
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Contact Person <span class="str">*</span></label>
                                  <div class="cstm-select-box">
                                      <select id="contact_person">
                                          <option disabled="disabled" selected="selected">Select contact person</option>

                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Pickup Address <span class="str">*</span></label>
                                  <div class="cstm-select-box">
                                      <select id="contact_address">
                                          <option disabled="disabled" selected="selected">Select Address</option>
                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                              <div class="input-group">
                                  <label>Upload Image <span class="str">*</span></label>
                                  <div class="file-browse">
                                      <span class="button-browse"> Browse <input type="file"></span>
                                      <input type="text" class="form-control browse-input" placeholder="e.g. pdf, jpg" readonly="">
                                  </div>
                              </div>
                          </div>
                      </div> -->

                      <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                              <div class="input-group avlbl-sswich m-t15">
                                  <label class="wid-60">Available :</label>
                                  <div class="verified-switch-btn cstm-css-checkbox">
                                      <label class="new-switch1 switch-green">
                                          <input type="checkbox" name="is_available" id="is_product_avail" class="switch-input">
                                          <span class="switch-label" data-on="Yes" data-off="No"></span>
                                          <span class="switch-handle"></span>
                                      </label>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                              <div class="input-group avlbl-sswich m-t15">
                                  <label class="wid-60">Self Logistics :</label>
                                  <div class="verified-switch-btn cstm-css-checkbox">
                                      <label class="new-switch1 switch-green">
                                          <input type="checkbox" name="self_logistics" id="is_self_logistics" class="switch-input">
                                          <span class="switch-label" data-on="Yes" data-off="No"></span>
                                          <span class="switch-handle"></span>
                                      </label>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="clearfix"></div>

                    </div>
              </div>
          </div>
          <input type="hidden" name="add_product_form_type" id="add_product_form_type" value="add"/>
          <input type="hidden" name="add_product_id" id="add_product_id" value=""/>
          <input type="hidden" name="authentication_code" id="product_authentication_code" value=""/>
          <input type="hidden" name="verified_by_admin" id="verified_by_admin_input" value=""/>
          <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<!-- MODAL ADD PRODUCT END-->


@endif

@if(Route::current()->getName() == 'admin_show_margin_slab')

<!-- MODAL ADD USER START-->
<div id="add-margin-slab-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="add_margin_slab_popup_title">Add Margin Slab</h4>
        </div>
        <div class="modal-body p-tb20 p-lr15">
            <form action="" name="add_margin_slab_form" method="post">
                @csrf

                        <div class="form-group">
                            <div class="input-group">
                                <label>Slab Title <span class="str">*</span></label>
                                <input type="text" name="slab_title" id="slab_title" class="form-control text-capitalize" placeholder="e.g. Slab 1">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="et-dynamic-add-btn-block clearfix">
                                <h2 class='clearfix'><span class="m-r10">Add Slab <span class="str">*</span></span>
                                <button id="AddMorebtn1" type="button" value="Add" class="site-button gray pull-right">Add More Slab</button></h2>
                            </div>
                            <div class="et-dynamic-textbox-wrap daynemic_textbox_scrpoll mCustomScrollbar">
                                <div id="et-dynamic-textboxcontainer" class="row mt-3">
                                    <div class="col-md-12 et-dynamic-input-block">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Upto Price ( <i class="fa fa-rupee"></i> ) <span class="str">*</span></label>
                                                <input type="text" class="form-control" name="margin_upto[]" value="" placeholder="e.g. 5000" >
                                            </div>
                                            <div class="col-md-6">
                                                <label>Margin Rate <span class="str">*</span></label>
                                                <input type="text" class="form-control" name="margin_rate[]" value="" placeholder="e.g. 5" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                <input type="hidden" name="add_margin_slab_form_type" id="add_margin_slab_form_type" value="add"/>
                <input type="hidden" name="margin_slab_id" id="margin_slab_id" value=""/>
                <input type="hidden" name="authentication_code" id="margin_slab_authentication_code" value=""/>
                <div class="modal-footer text-left">
                    <button type="submit" class="site-button m-r20">Save</button>
                    <button class="site-button gray" data-dismiss="modal">Cancel</button>
                </div>
            </form>

        </div>
    </div>
</div>
<!-- MODAL ADD USER END-->


@endif

@if(Route::current()->getName() == 'admin_show_gst_info')

<!-- MODAL ADD USER START-->
<div id="add-gst-info-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="add_gst_info_popup_title">Add GST Info</h4>
        </div>
        <form action="" name="add_gst_slab_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                    <div class="form-group">
                        <div class="input-group">
                            <label>Title <span class="str">*</span></label>
                            <input type="text" name="title" id="gst_title" class="form-control text-capitalize" placeholder="e.g. 5% Slab">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>IGST Rate (In Percentage) <span class="str">*</span></label>
                            <input type="text" name="igst_rate" id="igst_rate" class="form-control" placeholder="e.g. 5">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>SGST Rate (In Percentage) <span class="str">*</span></label>
                            <input type="text" name="sgst_rate" id="sgst_rate" class="form-control" placeholder="e.g. 10">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>CGST Rate (In Percentage) <span class="str">*</span></label>
                            <input type="text" name="cgst_rate" id="cgst_rate" class="form-control" placeholder="e.g. 12">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>HSN Code</label>
                            <input type="text" name="hsn_code" id="hsn_code" class="form-control" placeholder="e.g. HSN123456">
                        </div>
                    </div>

            </div>
            <input type="hidden" name="add_gst_slab_form_type" id="add_gst_slab_form_type" value="add"/>
            <input type="hidden" name="gst_slab_id" id="gst_slab_id" value=""/>
            <input type="hidden" name="authentication_code" id="gst_slab_authentication_code" value=""/>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- MODAL ADD USER END-->

@endif

<!-- Product Module Over -->

<!-- Email Template Start -->

@if(Route::current()->getName() == 'admin_show_email_template')

<!-- MODAL CREATE EMAIL TEMPLATE START-->
<div id="create-email-template" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="create_email_template_title">Add Email Template</h4>
        </div>
        <form action="" name="add_email_template_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="compose-message-block">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group" id="template_type_div">
                                    <div class="cstm-select-box">
                                        <select name="templateType" id="templateType">
                                            <option disabled="disabled" selected="selected">Select Template Type</option>
                                            <option value="welcome_user">Welcome User</option>
                                            <option value="new_order">New Order</option>
                                            <option value="new_invoice">New Invoice</option>
                                            <option value="payment_received">Payment Received</option>
                                            <option value=item_dispatched>Item Dispatched</option>
                                            <option value="item_delivered">Item Delivered</option>
                                            <option value="email_verification">Email Verification</option>
                                            <option value="product_complaint">Product Complaint</option>
                                            <option value="accept_quote">Accept Quote</option>
                                            <option value="supplier_new_order">Supplier New Order</option>
                                            <!-- <option>Promotion Offer</option>
                                            <option>Refund</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="subject" id="subject" class="form-control text-capitalize" placeholder="Subject:" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="form-control tags" id="tags">
                                        <input type="text" class="taginput" id="taginput" placeholder="Cc:">
                                        <input type="hidden" value="" id="email_cc" name="cc">
                                        <label class="error" id="email_cc_error"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="form-control tags" id="tags1">
                                        <input type="text"  class="taginput" placeholder="Bcc:" />
                                        <input type="hidden" value="" id="email_bcc" name="bcc">
                                        <label class="error" id="email_bcc_error"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="from_email" id="from_email" class="form-control" placeholder="From Email:" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="from_name" id="from_name" class="form-control" placeholder="From Name:" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="et-dynamic-add-btn-block">
                                        <h2><span class="m-r10">Header</span> <button id="AddMorebtn" type="button" value="Add" class="site-button">Add More</button></h2>
                                    </div>
                                    <div class="et-dynamic-textbox-wrap">
                                        <div id="et-dynamic-textboxcontainer" class="row mt-3">
                                            <div class="col-md-12 et-dynamic-input-block">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="header_name[]" value="" placeholder="Name" >
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="header_value[]" value="" placeholder="Value" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="compose-message-editor-block">
                                <textarea id="txtEditor" id="html" name="html"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="upload-file-block">
                                <input type="file" name="attachments">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <input type="hidden" name="add_email_template_form_type" id="add_email_template_form_type" value="add"/>
             <input type="hidden" name="email_template_type" id="email_template_type" value=""/>
            <!--<input type="hidden" name="authentication_code" id="gst_slab_authentication_code" value=""/> -->
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL CREATE EMAIL TEMPLATE END-->

<!-- MODAL TEMPLATE PREVIEW START-->
<div id="tempPrev-mode" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Template Preview Mode / <span id="preview_template_type"></span></h4>
          </div>
          <div class="modal-body p-tb20 p-lr15">
              <div id="devicesSwitcher">
                <div class="devicesMenu">
                    <a href="#" class="desktop active" title="Desktop View"><i class="fa fa-desktop"></i></a>
                    <a href="#" class="mobileportrait" title="Mobile View"><i class="fa fa-mobile"></i></a>
                </div>
              </div>
              <div class="iframe-content-block" id="template_iframe">

              </div>
          </div>
        </div>
      </div>
    </div>
<!-- MODAL TEMPLATE PREVIEW END-->

@endif

<!-- Email Template Over -->

<!-- Product Review Start -->

@if(Route::current()->getName() == 'admin_show_product_review')

<!-- MODAL MODIRAT REVIEW START-->
<div id="write-RC-popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Moderate Review</h4>
          </div>
            <form action="" name="update_product_review_form" method="post" class="write-a-review-form">
                @csrf
                <div class="modal-body p-tb20 p-lr15">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control text-capitalize" name="review_text" id="review_text" placeholder="Write a review"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="cstm-select-box">
                                <select name="status" id="status">
                                    <option disabled="disabled" selected="selected">Select Review/Status</option>
                                    <option value="published">Publish</option>
                                    <option value="unpublished">Unpublish</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="update_product_review_form_type" id="update_product_review_form_type" value="add"/>
                <input type="hidden" name="peoduct_review_id" id="peoduct_review_id" value=""/>
                <div class="modal-footer text-left">
                    <button type="submit" class="site-button ">Update</button>
                </div>
            </form>
        </div>
      </div>
    </div>
<!-- MODAL MODIRAT REVIEW END-->

@endif

<!-- Product Review Over -->

<!-- Access Logs Start -->

@if(Route::current()->getName() == 'admin_show_email_logs')


<!-- MODAL CREATE EMAIL TEMPLATE START-->
<div id="create-email-template" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title">Create an Email Template</h4>
        </div>
        <form action="" name="resend_email_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="compose-message-block">
                    <div class="row">

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="subject" id="subject" class="form-control text-capitalize" placeholder="Subject:" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="form-control tags" id="tags">
                                        <input type="text" class="taginput" id="taginput" placeholder="Cc:">
                                        <input type="hidden" value="" id="email_cc" name="cc">
                                        <p class="error" id="email_cc_error"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="form-control tags" id="tags1">
                                        <input type="text"  class="taginput" placeholder="Bcc:" />
                                        <input type="hidden" value="" id="email_bcc" name="bcc">
                                        <p class="error" id="email_bcc_error"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="from_email" id="from_email" class="form-control" placeholder="From Email:" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="from_name" id="from_name" class="form-control" placeholder="From Name:" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="et-dynamic-add-btn-block">
                                        <h2><span class="m-r10">Header</span> <button id="AddMorebtn" type="button" value="Add" class="site-button">Add More</button></h2>
                                    </div>
                                    <div class="et-dynamic-textbox-wrap">
                                        <div id="et-dynamic-textboxcontainer" class="row mt-3">
                                            <div class="col-md-12 et-dynamic-input-block">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="header_name[]" value="" placeholder="Name" >
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="header_value[]" value="" placeholder="value" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="compose-message-editor-block">
                                <textarea id="txtEditor" id="html" name="html"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="upload-file-block">
                                <input type="file" name="attachments">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <input type="hidden" name="add_email_template_form_type" id="add_email_template_form_type" value="add"/>
             <input type="hidden" name="email_template_type" id="email_template_type" value=""/>
            <!--<input type="hidden" name="authentication_code" id="gst_slab_authentication_code" value=""/> -->
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Send Mail</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL CREATE EMAIL TEMPLATE END-->



<!-- MODAL TEMPLATE PREVIEW START-->
<div id="tempPrev-mode" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title">Template Preview Mode / <span>Untitled Template Name</span></h4>
        </div>
        <div class="modal-body p-tb20 p-lr15">
            <div id="devicesSwitcher">
            <div class="devicesMenu">
                <a href="#" class="desktop active" title="Desktop View"><i class="fa fa-desktop"></i></a>
                <a href="#" class="mobileportrait" title="Mobile View"><i class="fa fa-mobile"></i></a>
            </div>
            </div>
            <div class="iframe-content-block" id="log_email_preview">
                <iframe id="viewDevicesiframe" src="e-template.html" frameborder="0" width="100%" height="100%"></iframe>
            </div>
        </div>
    </div>
    </div>
</div>

@endif
<!-- MODAL TEMPLATE PREVIEW END-->

<!-- Access Logs Over -->

<!-- Ledger Module Start -->

@if((Route::current()->getName() == 'admin_show_orders')
|| (Route::current()->getName() == 'admin_show_order_details')
|| (Route::current()->getName() == 'admin_show_supplier_order_details')
)

<!-- MODAL CHANGES STATUS START-->
<div id="changes-ds-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title" id="status_dialog_title">Changes Delivery Status</h4>
            </div>
            <form action="" name="update_order_status_form" method="post" class="write-a-review-form">
                @csrf
            <div class="modal-body p-tb20 p-lr15">

                    <div class="form-group">
                        <div class="input-group" id="order_status_div">
                            <label>Select Status <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="order_status" id="order_status">
                                    <option disabled="disabled" selected="selected">Select Status</option>
                                    <option value="RECEIVED">RECEIVED</option>
                                    <option value="ACCEPTED" title="ACCEPTED">ACCEPTED</option>
                                    <option value="REJECTED" title="REJECTED">REJECTED</option>
                                    <option value="TM_ASSIGNED">TM_ASSIGNED</option>
                                    <option value="PICKUP">PICKUP</option>
                                    <option value="DELAYED">DELAYED</option>
                                    <option value="CANCELLED">CANCELLED</option>
                                    <option value="DELIVERED">DELIVERED</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group" id="order_item_status_div">
                            <label>Select Status <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="order_item_status" id="order_item_status">
                                    <option disabled="disabled" selected="selected">Select Status</option>
                                    <option value="RECEIVED">RECEIVED</option>
                                    <option value="ACCEPTED" title="ACCEPTED">ACCEPTED</option>
                                    <option value="REJECTED" title="REJECTED">REJECTED</option>
                                    <option value="TM_ASSIGNED">TM_ASSIGNED</option>
                                    <option value="PICKUP">PICKUP</option>
                                    <option value="DELAYED">DELAYED</option>
                                    <option value="CANCELLED">CANCELLED</option>
                                    <option value="DELIVERED">DELIVERED</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group" id="order_vendor_item_status_div">
                            <label>Select Status <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="vendor_order_status" id="order_vendor_item_status">
                                    <option disabled="disabled" selected="selected">Select order Status</option>
                                    <option value="RECEIVED">RECEIVED</option>
                                    <option value="ACCEPTED" title="ACCEPTED">ACCEPTED</option>
                                    <option value="REJECTED" title="REJECTED">REJECTED</option>
                                    <option value="TM_ASSIGNED">TM_ASSIGNED</option>
                                    <option value="PICKUP">PICKUP</option>
                                    <option value="DELAYED">DELAYED</option>
                                    <option value="CANCELLED">CANCELLED</option>
                                    <option value="DELIVERED">DELIVERED</option>
                                    <!-- <option value="DELIVERED">DELIVERED</option>
                                    <option value="CANCELLED">CANCELLED</option> -->

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group" id="payment_status_div">
                            <label>Select Payment Status <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="payment_status" id="payment_status">
                                    <option disabled="disabled" selected="selected">Select Payment Status</option>
                                    <option value="PAID">PAID</option>
                                    <option value="UNPAID">UNPAID</option>
                                    <option value="PARTIALLY_PAID">PARTIALLY PAID</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group" id="cancel_status_div">
                            <label>Select Cancel Status <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select name="cancel_status" id="cancel_status">
                                    <!-- <option disabled="disabled" selected="selected">Select status</option> -->
                                    <option value="CANCELLED" selected="selected">CANCELLED</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group m-b0">
                        <div class="input-group">
                            <label>Remark <span class="str">*</span></label>
                            <textarea name="admin_remarks" id="admin_remarks" class="form-control" placeholder="Write a reason"></textarea>
                        </div>
                    </div> -->

            </div>
            <input type="hidden" name="order_id" id="order_id" />
            <input type="hidden" name="item_id" id="item_id" />
            <input type="hidden" name="is_item_status" id="is_item_status" />
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL CHANGES STATUS END-->


<!-- MODAL CHANGES STATUS START-->
<div id="changes-sts-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Changes Status</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15">
                <form action="" method="get">
                    <div class="form-group">
                        <div class="input-group">
                            <label>Select status <span class="str">*</span></label>
                            <div class="cstm-select-box">
                                <select>
                                    <option disabled="disabled" selected="selected">Select status</option>
                                    <option>Delivered</option>
                                    <option>Processing</option>
                                    <option>Cancel</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-b0">
                        <div class="input-group">
                            <label>Remark <span class="str">*</span></label>
                            <textarea class="form-control" placeholder="Write a reason"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
                <button class="site-button gray">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL CHANGES STATUS END-->

<!-- MODAL SEND INVOICE TO EMAIL START-->
<div id="sendemil-sts-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Send Invoice to Email</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15">
                <form action="" method="get">
                    <div class="form-group">
                        <div class="input-group">
                            <label>Email <span class="str">*</span></label>
                            <input type="text" class="form-control" placeholder="e.g. johndoe@example.com">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-left">
                <button class="site-button m-r20">Save</button>
                <button class="site-button gray">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL SEND INVOICE TO EMAIL END-->

<!-- MODAL CONTACT SUPPLIER START-->
<div id="changes-splr-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Contact Supplier</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15">
                <div class="track-dtl-block">
                    <table class="table border">
                        <tbody>
                            <tr>
                                <td><b>Supplier Name</b></td>
                                <td><a href="javascript:;" id="supplier_order_name"></a></td>
                            </tr>
                            <tr>
                                <td><b>Mobile No.</b></td>
                                <td><a href="javascript:;" id="supplier_mobile_no"></a></td>
                            </tr>
                            <!-- <tr>
                                <td><b>Alternate Number</b></td>
                                <td><a href="javascript:;">+91 7898798548</a></td>
                            </tr> -->
                            <!-- <tr>
                                <td><b>Landline No.</b></td>
                                <td><a href="javascript:;" id="supplier_landline_no"></a></td>
                            </tr> -->
                            <tr>
                                <td><b>Email</b></td>
                                <td><a href="javascript:;" id="supplier_email"></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL CONTACT SUPPLIER END -->


<!-- MODAL CONTACT LOGISTICS START-->
<div id="changes-lgstc-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Contact Logistics</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15">
                <div class="track-dtl-block">
                    <table class="table border">
                        <tbody>
                            <tr>
                                <td><b>Mobile No.</b></td>
                                <td><a href="javascript:;">+91 7393949897</a></td>
                            </tr>
                            <tr>
                                <td><b>Alternate No.</b></td>
                                <td><a href="javascript:;">+91 7898798548</a></td>
                            </tr>
                            <tr>
                                <td><b>Landline No.</b></td>
                                <td><a href="javascript:;">079 12345678</a></td>
                            </tr>
                            <tr>
                                <td><b>Email</b></td>
                                <td><a href="javascript:;">demo@companyname.com</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL CONTACT LOGISTICS END -->

<!-- MODAL VIEW MAP START-->
<div id="view-map-details" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header p-a0 border-0">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body p-tb20 p-lr15">
            <div class="popup-map-content-block" id="map_iframe_div">
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5864.520301024851!2d72.51392826831795!3d23.048447656590255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e9cb1d6441583%3A0x99c9230212282810!2sSCC+Infrastructure+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1551245667065" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                  <iframe src="https://maps.google.com/maps?q=35.856737, 10.606619&z=15&output=embed" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- MODAL VIEW MAP END-->

<!-- MODAL Order tracking START-->
<div id="order-CP-track" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Concrete Pump (CP) Tracking</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15" id="order_CP_trackking_div">

            </div>
        </div>
    </div>
</div>
<!-- MODAL Order tracking END-->

@endif



<!-- Ledger Module Over -->

<!-- Support Ticket Module Start -->

@if((Route::current()->getName() == 'admin_show_support_ticket')
        || (Route::current()->getName() == 'admin_show_support_ticket_details')
)

<!-- MODAL CREATE TICKET START-->
<div id="create-ticket-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Create Ticket</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15">
            <form action="" name="add_support_ticket_form" method="post" enctype="multipart/form-data">
            @csrf

                    <div class="compose-message-block">

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group" id="user_type_div">
                                            <label>User Type <span class="str">*</span></label>
                                            <div class="cstm-select-box">
                                                <select name="user_type" id="user_type">
                                                    <option disabled="disabled" selected="selected">Select User</option>
                                                    <option value="Vendor">Supplier</option>
                                                    <option value="Buyer">Buyer</option>
                                                    <!-- <option value="Logistics">Logistics</option> -->
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group single-search-select2" id="user_type_list_div">
                                            <select id="supplier_user_list"  name="supplier_user_id" style="display:none" class="form-control-chosen" data-placeholder="Select Supplier">
                                                <option></option>
                                                @if(isset($data["supplier_data"]))
                                                    @foreach($data["supplier_data"] as $value)
                                                        <option value="{{ $value['_id'] }}" >{{ $value["full_name"] }}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            <select id="buyer_user_list"  name="buyer_user_id" style="display:none" class="form-control-chosen" data-placeholder="Select Buyer">
                                                <option></option>
                                                @if(isset($data["buyer_data"]))
                                                    @foreach($data["buyer_data"] as $value)
                                                        <option value="{{ $value['_id'] }}"  >{{ $value["full_name"] }}</option>
                                                    @endforeach
                                                @endif

                                            </select>

                                            <p class="error" id="user_type_listing_error"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group" id="question_type_div">
                                            <label>Type Of Issue <span class="str">*</span></label>
                                            <div class="cstm-select-box">
                                                <select name="question_type" id="question_type_dropdown">
                                                    <option disabled="disabled" selected="selected">Select Question</option>
                                                </select>
                                                
                                                <select id="question_type_dropdown_buyer" style="display:none">
                                                    <option disabled="disabled" selected="selected">Select Question</option>
                                                    <!-- <option value="Problem on payment">Problem On Payment</option>
                                                    <option value="Problem on product">Problem On Product</option>
                                                    <option value="Enable to order">Enable To Order</option> -->
                                                    <option value="Rejection Of Order">Rejection Of Order</option>
                                                    <option value="Quality of RMC">Quality of RMC</option>
                                                    <option value="Dispute with RMC Supplier">Dispute with RMC Supplier</option>
                                                    <option value="Delay in RMC Supply">Delay in RMC Supply</option>
                                                    <option value="Non-Delivery of Concrete Pump">Non-Delivery of Concrete Pump</option>
                                                    <option value="Damage / Theft of RMC Supplier's Concrete Pump">Damage / Theft of RMC Supplier's Concrete Pump</option>
                                                    <option value="Accident of TM at Customer's Site">Accident of TM at Customer's Site</option>
                                                    <option value="Payment Related issue : Payment Transaction Failed">Payment Related issue : Payment Transaction Failed</option>
                                                    <option value="Payment Related issue : Payment duplication for the Order">Payment Related issue : Payment duplication for the Order</option>
                                                    <option value="Payment Related issue : Payment  refund for cancelled/rejected/short close">Payment Related issue : Payment  refund for cancelled/rejected/short closed/Lapsed order</option>
                                                    <option value="Order Confirmation Pending">Order Confirmation Pending</option>
                                                    <option value="Payment processed but order not placed">Payment processed but order not placed</option>
                                                    <option value="RMC supply not as per Design Mix requirement">RMC supply not as per Design Mix requirement</option>
                                                    <option value="Suggestion for Concrete Grade">Suggestion for Concrete Grade</option>
                                                    <option value="Querry related to Concrete Cube test Report">Querry related to Concrete Cube test Report</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                                
                                                <select id="question_type_dropdown_supplier" style="display:none">
                                                    <option disabled="disabled" selected="selected">Select Question</option>
                                                    <!-- <option value="Problem on payment">Problem on payment</option>
                                                    <option value="Problem on product">Problem on product</option>
                                                    <option value="Enable to order">Enable to order</option> -->
                                                    <option value="RMC Product listing display query">RMC Product listing display query</option>
                                                    <option value="Query related to Cement Brand, Admixture Brand, Type of Cement">Query related to Cement Brand, Admixture Brand, Type of Cement</option>
                                                    <option value="Query related to Source of Aggregates,Sand, Fly Ash">Query related to Source of Aggregates,Sand, Fly Ash</option>
                                                    <option value="Understnding of Conmix RMC Solution ">Understnding of Conmix RMC Solution </option>
                                                    <option value="Query related to type & Capacity of Transit Mixer">Query related to type & Capacity of Transit Mixer</option>
                                                    <option value="Query related to Category of Concrete Pump">Query related to Category of Concrete Pump</option>
                                                    <option value="Query related to Conmix Tracking App for Driver">Query related to Conmix Tracking App for Driver</option>
                                                    <option value="Dispute with Customer">Dispute with Customer</option>
                                                    <option value="Query related to Tax Invoice or Debit Note">Query related to Tax Invoice or Debit Note</option>
                                                    <option value="Query related to Damage/Accident/Theft of TM or Concrete Pump">Query related to Damage/Accident/Theft of TM or Concrete Pump</option>
                                                    <option value="Query related to incorrect delivery address or delivery address not found">Query related to incorrect delivery address or delivery address not found</option>
                                                    <option value="Query related to Payment">Query related to Payment</option>
                                                    <option value="Query related to Conmix E-commerce Software System">Query related to Conmix E-commerce Software System</option>
                                                    <option value="Query related to RMC Supplier Price vs Conmix RMC display price">Query related to RMC Supplier Price vs Conmix RMC display price</option>
                                                    <option value="Suggestions related to Conmix E-Commerce Service Terms & Conditions">Suggestions related to Conmix E-Commerce Service Terms & Conditions</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group" id="severity_div">
                                            <label>Priority </label>
                                            <div class="cstm-select-box">
                                                <select name="severity">
                                                    <option disabled="disabled" selected="selected">Select Priority</option>
                                                    <option value="Urgent">Urgent</option>
                                                    <option value="High">High</option>
                                                    <option value="Low">Normal</option>
                                                    <option value="Normal">Low</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Subject <span class="str">*</span></label>
                                            <div class="cstm-input">
                                                <textarea name="subject" class="form-control text-capitalize" id="ctsubject" placeholder="Brief summary of the question or issue" data-maxchar="250"></textarea>
                                                <span class="character-text">Maximum 250 Characters (<span class="character-counter"></span> remains)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label>Description <span class="str">*</span></label>
                                            <div class="cstm-input">
                                                <textarea name="description" class="form-control text-capitalize" id="ctdescription" placeholder="Detailed of the question or issue" data-maxchar="5000"></textarea>
                                                <span class="character-text">Maximum 5000 Characters (<span class="character-counter"></span> remaining)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group" id="support_ticket_attach_div">
                                            <label>Attachment </label>
                                            <div class="upload-file-block mt-0">
                                                <input type="file" name="attachments">
                                            </div>
                                            <!-- <span class="file-note-text">Up to 3 attachment, each less than 5MB.</span> -->
                                            <span class="file-note-text">Use one of the jpg, jpeg, mp4, doc, docx, pdf file format</span>
                                            <span class="file-error-text" style="display: none;"><i class="ion-alert-circled"></i> Maximum amount of files exceeded!</span>
                                        </div>
                                    </div>

                                    <input type="hidden" name="order_id" />
                                </div>

                            </div>

                    </div>
                </div>
                <div class="modal-footer text-left">
                    <button type="submit" class="site-button pull-left">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- MODAL CREATE TICKET END-->



<!-- MODAL RESOLVE ALERT START-->
<div id="resolve-alertmsg" class="alert-message modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-body p-tb40 p-lr15">
            Thank you for contacting customer support
        </div>
        <div class="modal-footer text-center">
            <button type="button" data-dismiss="modal" class="site-button">Ok</button>
        </div>
        </div>
    </div>
</div>
<!-- MODAL RESOLVE ALERT END-->


@endif

<!-- Support Ticket Module Over -->

<!-- New Proposal Module Start -->

@if(Route::current()->getName() == 'admin_show_proposal')

<!-- MODAL COMPOSE NEW MESSAGE START-->
<div id="compose-new-message" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title">Compose New Message</h4>
        </div>
        <div class="modal-body p-tb20 p-lr15">
        <form action="" name="compose_new_message_form" method="post" enctype="multipart/form-data">
            @csrf

            <div class="compose-message-block">

                    <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="form-control tags" id="tags">
                                    <input type="text" class="taginput" placeholder="To:">
                                    <input type="hidden" value="" name="to_email" id="to_email">
                                    <p class="error" id="email_to_error"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="form-control tags" id="tags1">
                                    <input type="text" class="taginput" placeholder="Cc:" />
                                    <input type="hidden" value="" name="cc" id="email_cc">
                                    <p class="error" id="email_cc_error"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="subject" class="form-control text-capitalize" placeholder="Subject:" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="et-dynamic-add-btn-block">
                                    <h2><span class="m-r10">Header</span> <button id="AddMorebtn" type="button" value="Add" class="site-button">Add More</button></h2>
                                </div>
                                <div class="et-dynamic-textbox-wrap">
                                    <div id="et-dynamic-textboxcontainer" class="row mt-3">
                                        <div class="col-md-12 et-dynamic-input-block">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="header_name[]" value="" placeholder="Name" >
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="header_value[]" value="" placeholder="value" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="compose-message-editor-block">
                            <textarea id="txtEditor" name="html"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="upload-file-block">
                            <input type="file" name="attachments">
                        </div>
                    </div>
                    </div>

            </div>
        </div>
        <div class="modal-footer text-left">
            <button type="submit" type="button" class="site-button pull-left">Send Mail</button>
        </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL COMPOSE NEW MESSAGE END-->

@endif

<!-- New Proposal Module Over -->

<!-- Profile Module Start -->

@if(Route::current()->getName() == 'admin_show_my_profile')

<!-- MODAL EDIT PROFILE START-->
<div id="edit-profile-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Edit Profile</h4>
        </div>
        <form action="" name="update_profile_form" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                <div class="form-group">
                    <div class="input-group">
                        <label>Full Name</label>
                        <input type="text" name="full_name" id="full_name" class="form-control text-capitalize" value="" placeholder="e.g. Jhondoe">
                    </div>
                </div>
                <!-- <div class="form-group">
                    <div class="input-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" value="Doe" placeholder="e.g. doe">
                    </div>
                </div> -->
                <div class="form-group">

                    <div class="d-flex justify-content-between align-items-center">
                        <label>Email</label>
                        <a href="javascript:void(0)" onclick="OTPonUpdateEmail(1, 0, 0)">Update</a>
                    </div>
                    <input type="hidden" id="edit_address_email" value="{{ isset($profile_data['email']) ? $profile_data['email'] : '' }}" />
                    <input type="email" name="email" id="email" readOnly class="form-control" value="" placeholder="e.g. johndoe@example.com">

                </div>
                <div class="form-group">

                    <div class="d-flex justify-content-between align-items-center">
                        <label>Mobile No.</label>
                        <a href="javascript:void(0)" onclick="OTPonUpdateMobile(1, 0, 0)" >Update</a>
                    </div>
                    <input type="hidden" id="edit_address_mobile" value="{{ isset($profile_data['mobile_number']) ? $profile_data['mobile_number'] : '' }}" />
                    <input type="text" name="mobile_number" readOnly class="form-control" value="" id="mobile_or_email">

                </div>

            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button pull-left">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- MODAL EDIT PROFILE END-->

<!-- MODAL EDIT PROFILE START-->
<div id="change-password-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title">Change Password</h4>
        </div>
        <div class="modal-body p-tb20 p-lr15">
        <form action="" name="update_password_form" method="post">
            @csrf


                    <div class="form-group">
                        <div class="input-group">
                            <label>Old Password</label>
                            <input type="password" name="old_password" class="form-control" value="" placeholder="e.g. Old password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>New Password</label>
                            <input type="password" name="password" id="new_password" class="form-control" value="" placeholder="e.g. New Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label>Confirm Password</label>
                            <input type="password" name="confirm_password" class="form-control" value="" placeholder="e.g. Confirm Password">
                        </div>
                    </div>

            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button pull-left">Update</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- MODAL EDIT PROFILE END-->

@endif

<!-- Profile Module Over -->



<!-- MODAL PRODUCT CATEGORY START-->
<div id="aggregate-source-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title">Add Aggregate Souce</h4>
        </div>
        <form name="add_aggregate_source_form" action="" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                    <div class="form-group">
                        <div class="input-group">
                            <label>Select Region Serv <span class="str">*</span></label>
                            <div class="cstm-select-box" id="region_id_div">
                                <select id="aggregate_region_id" name="region_id">
                                    <option disabled="disabled" selected="selected">Select Region</option>
                                    @if(isset($state_data["data"]))
                                        @foreach($state_data["data"] as $value)
                                            <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                    <div class="input-group">
                        <label>Aggregate Source Name <span class="str">*</span></label>
                        <input type="text" id="aggregate_source_name" name="aggregate_source_name" class="form-control" placeholder="e.g. Rajpadi, Sabermati , Vadagam">
                    </div>
                </div>
                <input type="hidden" id="add_aggregate_source_form_type" name="add_aggregate_source_form_type" value="add"/>
                <input type="hidden" id="aggregate_source_id" name="aggregate_source_id" />


            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->


@if(Route::current()->getName() == 'admin_show_sand_source')
<!-- MODAL PRODUCT CATEGORY START-->
<div id="sand-source-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title">Add Coarse Sand Souce</h4>
        </div>
        <form name="add_sand_source_form" action="" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                    <div class="form-group">
                        <div class="input-group">
                            <label>Select Region Serv <span class="str">*</span></label>
                            <div class="cstm-select-box" id="region_id_div">
                                <select id="sand_region_id" name="region_id">
                                    <option disabled="disabled" selected="selected">Select Region</option>
                                    @if(isset($state_data["data"]))
                                        @foreach($state_data["data"] as $value)
                                            <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                    <div class="input-group">
                        <label>Coarse Sand Source Name <span class="str">*</span></label>
                        <input type="text" id="sand_source_name" name="sand_source_name" class="form-control" placeholder="e.g. Rajpadi, Sabermati , Vadagam">
                    </div>
                </div>
                <input type="hidden" id="add_sand_source_form_type" name="add_sand_source_form_type" value="add"/>
                <input type="hidden" id="sand_source_id" name="sand_source_id" />


            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->
@endif

@if(Route::current()->getName() == 'admin_show_sand_zone')
<!-- MODAL PRODUCT CATEGORY START-->
<div id="sand-zone-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title">Add Coarse Sand Zone</h4>
        </div>
        <form name="add_sand_zone_form" action="" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                    <div class="form-group">
                        <div class="input-group">
                            <label>Coarse Sand Zone Name <span class="str">*</span></label>
                            <input type="text" id="zone_name" name="zone_name" class="form-control" placeholder="e.g. Zone 1">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <label>Finess Module Range (mm) <span class="str">*</span></label>
                            <input type="text" id="finess_module_range" name="finess_module_range" class="form-control" placeholder="e.g. 4 - 2.71">
                        </div>
                    </div>
                <input type="hidden" id="add_sand_zone_form_type" name="add_sand_zone_form_type" value="add"/>
                <input type="hidden" id="sand_zone_id" name="sand_zone_id" />


            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->

@endif


@if(Route::current()->getName() == 'admin_show_flyash_source')
<!-- MODAL PRODUCT CATEGORY START-->
<div id="flyash-source-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title">Add Fly Ash Souce</h4>
        </div>
        <form name="add_flyash_source_form" action="" method="post">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                    <div class="form-group">
                        <div class="input-group">
                            <label>Select Region Serv <span class="str">*</span></label>
                            <div class="cstm-select-box" id="region_id_div">
                                <select id="flyash_region_id" name="region_id">
                                    <option disabled="disabled" selected="selected">Select Region</option>
                                    @if(isset($state_data["data"]))
                                        @foreach($state_data["data"] as $value)
                                            <option value="{{ $value['_id'] }}">{{ $value["state_name"] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                    <div class="input-group">
                        <label>Flyash Source Name <span class="str">*</span></label>
                        <input type="text" id="flyash_source_name" name="flyash_source_name" class="form-control" placeholder="e.g. Rajpadi, Sabermati , Vadagam">
                    </div>
                </div>
                <input type="hidden" id="add_flyash_source_form_type" name="add_flyash_source_form_type" value="add"/>
                <input type="hidden" id="flyash_source_id" name="flyash_source_id" />


            </div>
            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->

@endif




<!-- MULTI TRUCK MODAL START -->
    <div id="multi-truck-popup" class="modal fade multi-truck-modal-block" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                    <h4 class="modal-title">Track Vehicle Details</h4>
                </div>
                <div class="modal-body" id="multi_truck_details_div">

                </div>
            </div>
        </div>
    </div>
    <!-- MULTI TRUCK MODAL END -->





<!-- MODAL Order tracking START-->
<div id="order-track" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Order Tracking</h4>
            </div>
            <div class="modal-body p-tb20 p-lr15" id="buyers_item_tracking_div">

            </div>
        </div>
    </div>
</div>
<!-- MODAL Order tracking END-->

<!-- Amixture Types module start-->


@if(Route::current()->getName() == 'admin_show_admixture_types')

<!-- MODAL PRODUCT CATEGORY START-->
<div id="admixture-type-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="admixture_type_popup_title">Add Admixture Type</h4>
        </div>
        <form action="" name="admixture_type_form" id="admixture_type_form" method="post" enctype="multipart/form-data">
        @csrf
          <div class="modal-body p-tb20 p-lr15">


                <div class="form-group">
                    <div class="input-group single-search-select2" id="brand_id_div">
                        <label>Admixture Brand <span class="str">*</span></label>
                        <div class="cstm-select-box">
                            <select id="brand_id" name="brand_id" class="form-control" data-placeholder="Select Admixture Brand">
                                <option disabled selected="selected"> Select Admixture Brand</option>
                                @if(isset($data["admixture_brand_data"]))
                                    @foreach($data["admixture_brand_data"] as $value)
                                        <option value="{{ $value['_id'] }}">{{ $value["name"] }}</option>
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group single-search-select2" id="admixture_type_div">
                        <label>Admixture Type <span class="str">*</span></label>
                        <div class="cstm-select-box">
                            <select id="admixture_type" name="admixture_type" class="form-control" data-placeholder="Select Admixture Type">
                            <option disabled selected="selected"> Select Admixture Type</option>
                                @if(isset($data["admixture_types"]))
                                    @foreach($data["admixture_types"] as $value)
                                        <option value="{{ $value['admixture_type'] }}">{{ $value["admixture_type"] }}</option>
                                    @endforeach
                                @endif
                                <!-- <option value="Plasticizer">Plasticizer</option>
                                <option value="Superplasticizers">Superplasticizers</option>
                                <option value="Retarding admixtures">Retarding Admixtures</option>
                                <option value="Accelerating admixtures">Accelerating Admixtures</option>
                                <option value="Corrosion-inhibiting admixtures">Corrosion Inhibiting Admixtures</option>
                                <option value="Water-reducing admixtures">Water Reducing Admixtures</option> -->

                            </select>
                        </div>
                    </div>
                </div>

              <div class="form-group">
                  <div class="input-group">
                      <label>Admixture Code <span class="str">*</span></label>
                      <input type="text" id="category_name" name="category_name" class="form-control" placeholder="e.g. 3070, 3071">
                  </div>
              </div>


              <input type="hidden" name="admixture_type_form_type" id="admixture_type_form_type" value="add"/>
              <input type="hidden" name="admixture_type_id" id="admixture_type_id" value=""/>
              <!-- <input type="hidden" name="authentication_code" id="product_cat_authentication_code" value=""/> -->
          </div>
          <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->



@endif

<!-- Amixture Types module Over -->

<!-- Cement grade module Start -->


@if(Route::current()->getName() == 'admin_show_cement_grade')

<!-- MODAL PRODUCT CATEGORY START-->
<div id="cement-grade-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="cement_grade_popup_title">Add Cement Grade</h4>
        </div>
        <form action="" name="cement_grade_form" id="cement_grade_form" method="post" enctype="multipart/form-data">
        @csrf
          <div class="modal-body p-tb20 p-lr15">

              <div class="form-group">
                  <div class="input-group">
                      <label>Cement Grade Name <span class="str">*</span></label>
                      <input type="text" id="cement_grade_name" name="cement_grade_name" class="form-control" placeholder="e.g. PPC, OPC, PSC">
                  </div>
              </div>

              <input type="hidden" name="cement_grade_form_type" id="cement_grade_form_type" value="add"/>
              <input type="hidden" name="grade_id" id="grade_id" value=""/>
              <!-- <input type="hidden" name="authentication_code" id="product_cat_authentication_code" value=""/> -->
          </div>
          <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->



@endif

@if(Route::current()->getName() == 'admin_show_payement_method')

<!-- MODAL PRODUCT CATEGORY START-->
<div id="payment-method-popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="payment_method_popup_title">Add Payment Method</h4>
        </div>
        <form action="" name="payment_method_form" method="post" enctype="multipart/form-data">
        @csrf
          <div class="modal-body p-tb20 p-lr15">

              <div class="form-group">
                  <div class="input-group">
                      <label>Payment Method Name <span class="str">*</span></label>
                      <input type="text" id="payment_method_name" name="payment_method_name" class="form-control" placeholder="e.g. NBFC, Razorpay">
                  </div>
              </div>

              <input type="hidden" name="payment_method_form_type" id="payment_method_form_type" value="add"/>
              <input type="hidden" name="pay_id" id="pay_id" value=""/>
              <!-- <input type="hidden" name="authentication_code" id="product_cat_authentication_code" value=""/> -->
          </div>
          <div class="modal-footer text-left">
              <button type="submit" class="site-button m-r20">Save</button>
              <button class="site-button gray" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
    </div>
</div>
<!-- MODAL PRODUCT CATEGORY END-->



@endif

<!-- Cement grade module Over -->

<!-- MODAL ADD COUPON START-->
<div id="add-coupon-popup" class="modal fade" role="dialog" >
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="add_coupon_popup_title">Add Coupon</h4>
        </div>
        <form name="add_coupon" id="addedit_coupon" action="" method="post" enctype="multipart/form-data">
            <div class="modal-body p-tb20 p-lr15">

                @CSRF
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Code <span class="str">*</span></label>
                            <input type="text" id="code" name="code" value="{{ old('code', isset($coupondetails['code']) ? $coupondetails['code'] : '') }}" class="form-control" placeholder="e.g. Flat15">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label>Discount Type <span class="str">*</span></label>
                                    <div class="cstm-select-box">
                                        <select id="discount_type" name="discount_type" class="form-control" data-placeholder="Select Discount Type">
                                            <option disabled="disabled" selected="selected">Select Discount Type</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="fix">Fixed</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Discount Value <span class="str">*</span></label>
                            <input type="text" id="discount_value" name="discount_value" value="{{ old('discount_value', isset($coupondetails['discount_value']) ? $coupondetails['discount_value'] : '') }}" class="form-control" placeholder="e.g. 15%">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Min. Order <span class="str">*</span></label>
                            <input type="text" id="min_order" name="min_order" value="{{ old('min_order', isset($coupondetails['min_order']) ? $coupondetails['min_order'] : '') }}" class="form-control" placeholder="e.g. 100 pcs">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Max. Discount <span class="str">*</span></label>
                            <input type="text" id="max_discount" name="max_discount" value="{{ old('max_discount', isset($coupondetails['max_discount']) ? $coupondetails['max_discount'] : '') }}" class="form-control" placeholder="e.g. 150">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Max. Usage <span class="str">*</span></label>
                            <input type="text" id="max_usage" name="max_usage" value="{{ old('max_usage', isset($coupondetails['max_usage']) ? $coupondetails['max_usage'] : '') }}" class="form-control" placeholder="e.g. 1">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Start Date <span class="str">*</span></label>
                            <div class="input-group">
                                <input type="text" id="start_date" name="start_date" value="" class="form-control datepicker-autoclose coupon-date" placeholder="yyyy-mm-dd">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>End Date <span class="str">*</span></label>
                            <div class="input-group">
                                <input type="text" id="end_date" name="end_date" value="" class="form-control datepicker-autoclose coupon-date" placeholder="yyyy-mm-dd">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <div class="input-group single-search-select2">
                                <label>Select Buyer</label>
                                <div class="cstm-select-box">
                                    <select id="buyer_id" name="buyer_id[]" class="select2 select2-multiple" data-placeholder="Select buyer" multiple="multiple">
                                        @if(empty($data["buyerlist"]))
                                        @else
                                            @foreach($data["buyerlist"] as $buyer)

                                                <option value="{{ $buyer['_id'] }}" >{{ $buyer['full_name'] }}</option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <div class="input-group avlbl-sswich" id="single_use_div">
                                <label>Single Use</label>
                                <div class="verified-switch-btn cstm-css-checkbox">
                                    <label class="new-switch1 switch-green">
                                        <input class="switch-input" type="checkbox" id="unique_use" name="unique_use">
                                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label>Info <span class="str">*</span></label>
                            <div class="cstm-input">
                                <textarea class="form-control" id="ctsubject" name="info" placeholder="" data-maxchar="250" maxlength="250">{{ old('info', isset($coupondetails['info']) ? $coupondetails['info'] : '') }}</textarea>
                                <span class="character-text">Maximum 250 characters (<span class="character-counter">250</span> remains)</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label>Terms And Conditions <span class="str">*</span></label>
                            <!--div id="summernotecoupon" class="summernote"></div-->
                            <textarea class="form-control" id="summernotecoupon" name="tnc" class="summernote" rows="3">{{ old('tnc', isset($coupondetails['tnc']) ? $coupondetails['tnc'] : '') }}</textarea>
                        </div>
                    </div>

                    <input type="hidden" id="coupon_id" name="coupon_id" value="" />
                    <input type="hidden" id="add_coupon_type" name="add_coupon_type" value="add" />


                </div>



        </div>
        <div class="modal-footer text-left">
        <!-- <button class="site-button m-r20">Save</button> -->
        <input type="submit" name="add_admin_user" class="site-button m-r20" value="Save"/>
        <button class="site-button gray" data-dismiss="modal">Cancel</button>
        </div>
        </form>
    </div>
    </div>
</div>

<script>
	$('.coupon-date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'yyyy-mm-dd'
	});


    // $('.TM_CP_change_date').datepicker().val('').trigger('change')

    $(".coupon-date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        // $("#selected_delivery_date").val(this.value);
        // $("#delivery_date_error").html("");
        /* Act on the event */
    });
</script>

<!-- MODAL ADD COUPON END-->


    <div id="supplier-block-popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="suplier_block_popup_title">Reject</h4>
          </div>
          <form action="" method="post" name="supplier_block_form" >
            <div class="modal-body p-tb20 p-lr15">
                @csrf
                <div class="form-group">
                    <div class="input-group">
                        <label>Reason <span class="str">*</span></label>
                        <div class="cstm-select-box">
                            <select name="reject_reason" id="">
                                <option disabled="disabled" selected="selected">Select Reason</option>
                                <option value="Wrong Information">Wrong Information</option>
                                <option value="Not Registered">Not Registered</option>

                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer text-left">
                <!-- <button class="site-button m-r20">Save</button> -->
               <input type="hidden" name="supplier_id" id="reject_supplier_id" />
                <input type="submit" name="" class="site-button m-r20" value="Save"/>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="sub_vender_popup" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                    <h4 class="modal-title" id="">RMC Suppliers Plant list</h4>
                </div>
                <div class="modal-body p-tb20 p-lr15" id="supplier_plant_details">


                </div>
                <div class="modal-footer text-left">
                    <button class="site-button gray" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div id="supplier-plant-block-popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="suplier_block_popup_title">Reject</h4>
          </div>
          <form action="" method="post" name="supplier_plant_block_form">
            <div class="modal-body p-tb20 p-lr15">
                @csrf
                <div class="form-group">
                    <div class="input-group">
                        <label>Reason <span class="str">*</span></label>
                        <div class="cstm-select-box">
                            <select name="reject_reason" id="">
                                <option disabled="disabled" selected="selected">Select Reason</option>
                                <option value="Wrong Information">Wrong Information</option>
                                <option value="Not Registered">Not Registered</option>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-left">
                <!-- <button class="site-button m-r20">Save</button> -->
               <input type="hidden" name="address_id" id="reject_supplier_plant_id" />
               <input type="hidden" name="vendor_id" id="reject_vendor_id" />
                <input type="submit" name="" class="site-button m-r20" value="Save"/>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div>

<!-- Coupon -->
    <div id="coupon-info-popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="">Coupon Info Details</h4>
          </div>

            <div class="modal-body p-tb20 p-lr15">
                <p id="coupon_info_details">

                </p>
            </div>


        </div>
      </div>
    </div>

    <div id="tnc-info-popup" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="modal-title" id="">T&C</h4>
          </div>

            <div class="modal-body p-tb20 p-lr15">
                <p id="tnc_info_details">

                </p>
            </div>

        </div>
      </div>
    </div>


<div id="update_mobile_profile" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <form name="update-mobile-otp-form" style="display: block;" autocomplete="off">
        @csrf
            <div class="modal-body p-tb20 p-lr15">
                <div class="form-group">
                    <label id="update_mobile_otp_confirm_message">Enter the 6 digits OTP sent on your given mobile no. 2222222222222</label>
                    <input type="text" name="otp_code_1" id="plant_mobile_otp" class="form-control" placeholder="Enter OTP sent on mobile">
                </div>

                <div class="text-gray-dark resend_otp_div text-left">

                    <div class="timer_otp mb-3">
                        <span id="time_update_mobile">
                            <span id="times">01:59</span>
                        </span>
                    </div>
                </div>

                <input type="hidden" name="mobile_number" id="verify_update_mobile" />
                <input type="hidden" name="mobile_or_email_type" id="mobile_type" />
            </div>
            <div class="modal-footer text-center">
                <button type="submit" class="site-button green m-r15">Submit</button>
                <button type="button" data-dismiss="modal" class="site-button red">Cancel</button>
            </div>

        </form>

    </div>
  </div>
</div>

<div id="new_mobile_no_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="">New Mobile Number</h4>
        </div>
        <form action="" name="new_mobile_form" method="post">
        @csrf
        <div class="modal-body p-tb20 p-lr15">

            <div class="form-group">
                <div class="input-group">
                    <label>New Mobile No. <span class="str">*</span></label>
                    <input type="text" name="mobile" id="update_new_mobile_no" class="form-control mobile_validate" placeholder="e.g. 9898989898">
                </div>
            </div>

        </div>

        <div class="modal-footer text-left">
            <button type="submit" class="site-button m-r20">Save</button>
            <button class="site-button gray" data-dismiss="modal">Cancel</button>
        </div>
        </form>
    </div>
    </div>
</div>

<div id="update_email_profile" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    <form name="update-email-otp-form" style="display: block;" autocomplete="off">
    @csrf
        <div class="modal-body p-tb20 p-lr15" id="otpMessage">
                <div class="form-group">
                    <label id="update_email_otp_confirm_message"></label>
                    <input type="text" name="otp_code_1" class="form-control" placeholder="Enter OTP sent on Email">
                </div>
                <div class="text-gray-dark resend_otp_div text-left">
                    <div class="timer_otp mb-3">
                        <span id="time_update_email">
                            <span id="times">01:59</span>
                        </span>
                    </div>
                </div>
        </div>
        <input type="hidden" name="email" id="verify_update_email" />
                        <input type="hidden" name="mobile_or_email_type" id="email_type" />
        <div class="modal-footer text-center">
            <button type="submit" class="site-button green m-r15" ">Submit</button>
            <button type="button" data-dismiss="modal" class="site-button red" ">Cancel</button>
        </div>
    </form>
    </div>
  </div>
</div>

<div id="new_email_no_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        <h4 class="modal-title" id="">New Email</h4>
        </div>
        <form action="" name="new_email_form">
            @csrf
            <div class="modal-body p-tb20 p-lr15">

                <div class="form-group">
                    <div class="input-group">
                        <label>New Email <span class="str">*</span></label>
                        <input type="text" name="email" id="update_new_email" class="form-control mobile_validate" placeholder="e.g. johndoe@example.com">
                    </div>
                </div>

            </div>

            <div class="modal-footer text-left">
                <button type="submit" class="site-button m-r20">Save</button>
                <button class="site-button gray" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    </div>
</div>