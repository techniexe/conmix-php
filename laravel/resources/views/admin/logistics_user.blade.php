@extends('admin.layouts.admin_layout')
@section('content')

<div class="middle-container-wrap">
            <h1 class="breadcrums">Users / <span>Logistics</span></h1>
            <div class="clearfix"></div>
            <!--vehicle category list start -->
            <div class="users-main-block wht-tble-bg">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="sub-title-block">
                        @if(isset($data["error"]))
                            <div class="">{{ $data["error"]["message"] }}</div>
                        @endif
                            <h2 class="sub-title pull-left">Logistics</h2>
                            <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                        </div>
                        <div class="bls-users-block">
                            @if(isset($data["data"]) && count($data['data']) > 0)
                                @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp

                                    <div class="bls-users-contet-block m-b20">
                                        <a href="javascript:;" class="site-button outline gray pull-right top-right-link-icon" data-tooltip="Dashboard"><i class="fa fa-dashboard"></i></a>
                                        <div class="row">
                                            <form action="" method="post" name="logistics_verify_form_{{ $value['_id'] }}" id="logistics_verify_form_{{ $value['_id'] }}">
                                                    @csrf
                                                    <div class="verified-switch-btn cstm-css-checkbox" style="right: 50px;">
                                                        <label class="new-switch switch-green">
                                                            
                                                                <input type="hidden" name="logistics_id" value="{{ $value['_id'] }}" />
                                                                <input type="checkbox" name="verified_by_admin" data-logistics-id="{{ $value['_id'] }}" id="logistics_verify_by_admin_{{ $value['_id'] }}" class="switch-input" {{ $value['verified_by_admin'] == true ? 'checked' : '' }}>
                                                                <span class="switch-label" data-on="Verified" data-off="Unverified"></span>
                                                                <span class="switch-handle"></span>
                                                            
                                                        </label>
                                                    </div>
                                                </form>
                                                <script>
                                                    $('#logistics_verify_by_admin_{{ $value['_id'] }}').change(function() {
                                                        $is_checked = '';
                                                        if(this.checked) {
                                                            $is_checked = 'true';
                                                        }else{
                                                            $is_checked = 'false';
                                                        }

                                                        var logistics_id = $(this).attr("data-logistics-id");

                                                        request_data = $('#logistics_verify_form_'+logistics_id).serialize();
                                                        console.log(request_data);
                                                        logisticsVerifyByAdmin(request_data);
                                                        
                                                    });
                                                </script>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <h3 class="bls-users-title">{{ isset($value["full_name"]) ? $value["full_name"] : '' }}</h3>
                                                <ul>
                                                    <li>
                                                        <p><span class="vTitle">User Id : </span> <span class="vName">#{{ isset($value["user_id"]) ? $value["user_id"] : '' }}</span></p>
                                                    </li>
                                                        <li>
                                                <p><span class="vTitle">Company Type : </span> <span class="vName">{{ isset($value["company_type"]) ? $value["company_type"] : '' }}</span></p>
                                                         </li>
                                                  <li>
                                                <p><span class="vTitle">Person Name : </span> <span class="vName">{{ isset($value["full_name"]) ? $value["full_name"] : '' }}</span></p>
                                            </li>
                                                    <li>
                                                        <p><span class="vTitle">Mobile No. : </span> <span class="vName">{{ isset($value["mobile_number"]) ? $value["mobile_number"] : '' }}</span></p>
                                                    </li>
                                                    @if(isset($value["landline_number"]))
                                                     <li>
                                                        <p><span class="vTitle">Landline No. : </span> <span class="vName">{{ isset($value["landline_number"]) ? $value["landline_number"] : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                    <li>
                                                        <p><span class="vTitle">Email : </span> <span class="vName">{{ isset($value["email"]) ? $value["email"] : '' }}</span></p>
                                                    </li>
                                                    @if(isset($value['website_URL']))
                                                        <li>
                                                        <p><span class="vTitle">Website URL : </span> <span class="vName"><a href="{{ isset($value['website_URL']) ? $value['website_URL'] : '#' }}">{{ isset($value["website_URL"]) ? $value["website_URL"] : '' }}</a></span></p>
                                                    </li>
                                                    @endif
                                                    <!-- <li>
                                                        <p><span class="vTitle">Account Type: </span> <span class="vName">{{ isset($value["account_type"]) ? $value["account_type"] : '' }}</span></p>
                                                    </li> -->
                                                     <li>
                                                     @if(isset($value["company_certification_number"]))
                                                <p><span class="vTitle">Company Registration No : </span> <span class="vName">{{ isset($value["company_certification_number"]) ? $value["company_certification_number"] : '' }}</span></p>
                                                         </li>
                                                         @endif
                                                         @if(isset($value["pancard_number"]))
                                                           <li>
                                                        <p><span class="vTitle">Pancard No : </span> <span class="vName">{{ isset($value["pancard_number"]) ? $value["pancard_number"] : '' }}</span></p>
                                                    </li>
                                                    @endif

                                                    @if(isset($value["gst_number"]))
                                                      <li>
                                                        <p><span class="vTitle">GST No : </span> <span class="vName">{{ isset($value["gst_number"]) ? $value["gst_number"] : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                    @if(isset($value["signup_type"]))
                                                    <li>
                                                        <p><span class="vTitle">Signup Type : </span> <span class="vName">{{ isset($value["signup_type"]) ? $value["signup_type"] : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                   
                                                   
                                                   <!--  <li>
                                                        <p><span class="vTitle">FAX Number: </span> <span class="vName">{{ isset($value["fax_number"]) ? $value["fax_number"] : '' }}</span></p>
                                                    </li> -->
                                                   
                                                   <li>
                                                        <p><span class="vTitle">Verified by Admin : </span> <span class="vName"> <i class='{{ isset($value["verified_by_admin"]) ? ($value["verified_by_admin"] ? "fa fa-check text-green" : "fa fa-close text-red") : "" }}'></i></span></p>
                                                    </li>
                                               
                                                               
                                                    <li>
                                                        <p><span class="vTitle">Date & Time : </span> <span class="vName">{{ isset($value["created_at"]) ? date('d M Y h:i:s a',strtotime($value["created_at"])) : '' }}</span></p>
                                                    </li> 
                                                    <!-- <li>
                                                        <div id="gallery">
                                                        @if(isset($value["company_certification_image_url"]))
                                                                <a href="{{ $value["company_certification_image_url"] }}" class="vehicle-img-block gal_link"><img src="{{ $value["company_certification_image_url"] }}"></a>
                                                            @endif
                                                            @if(isset($value["pancard_image_url"]))
                                                                <a href="{{ $value["pancard_image_url"] }}" class="vehicle-img-block gal_link"><img src="{{ $value["pancard_image_url"] }}"></a>
                                                            @endif
                                                            @if(isset($value["gst_certification_image_url"]))
                                                                <a href="{{ $value["gst_certification_image_url"] }}" class="vehicle-img-block gal_link"><img src="{{ $value["gst_certification_image_url"] }}"></a>
                                                            @endif
                                                        
                                                        </div>
                                                    </li> -->
                                                </ul>
                                                <div class="document_list">
                                                @if(isset($value["company_certification_image_url"]))
                                                        @if(strpos($value["company_certification_image_url"], '.doc') || strpos($value["company_certification_image_url"], '.docx'))
                                                            <a href="{{ $value['company_certification_image_url'] }}" class="document_box docfile">
                                                                <i class="fa fa-file-word-o"></i>
                                                                <span>Company Certificate</span>
                                                            </a>
                                                        @endif

                                                        @if(strpos($value["company_certification_image_url"], '.jpg') || strpos($value["company_certification_image_url"], '.png') || strpos($value["company_certification_image_url"], '.jpeg'))
                                                            <a href="$value['company_certification_image_url']" class="document_box gal_link">
                                                                <i class="fa fa-image"></i>
                                                                <span>Company Certificate</span>
                                                            </a>

                                                        @endif

                                                        @if(strpos($value["company_certification_image_url"], '.pdf'))
                                                            <a href="{{ $value['company_certification_image_url'] }}" class="document_box pdffile">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                                <span>Company Certificate</span>
                                                            </a>
                                                        @endif
                                                    @endif
                                                    
                                                    @if(isset($value["pancard_image_url"]))
                                                        @if(strpos($value["pancard_image_url"], '.doc') || strpos($value["pancard_image_url"], '.docx'))
                                                            <a href="{{ $value['pancard_image_url'] }}" class="document_box docfile">
                                                                <i class="fa fa-file-word-o"></i>
                                                                <span>Pancard</span>
                                                            </a>
                                                        @endif

                                                        @if(strpos($value["pancard_image_url"], '.jpg') || strpos($value["pancard_image_url"], '.png') || strpos($value["pancard_image_url"], '.jpeg'))
                                                            <a href="$value['pancard_image_url']" class="document_box gal_link">
                                                                <i class="fa fa-image"></i>
                                                                <span>Pancard</span>
                                                            </a>

                                                        @endif

                                                        @if(strpos($value["pancard_image_url"], '.pdf'))
                                                            <a href="{{ $value['pancard_image_url'] }}" class="document_box pdffile">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                                <span>Pancard</span>
                                                            </a>
                                                        @endif
                                                    @endif
                                                    
                                                    @if(isset($value["gst_certification_image_url"]))
                                                        @if(strpos($value["gst_certification_image_url"], '.doc') || strpos($value["gst_certification_image_url"], '.docx'))
                                                            <a href="{{ $value['gst_certification_image_url'] }}" class="document_box docfile">
                                                                <i class="fa fa-file-word-o"></i>
                                                                <span>GST Certificate</span>
                                                            </a>
                                                        @endif

                                                        @if(strpos($value["gst_certification_image_url"], '.jpg') || strpos($value["gst_certification_image_url"], '.png') || strpos($value["gst_certification_image_url"], '.jpeg'))
                                                            <a href="$value['gst_certification_image_url']" class="document_box gal_link">
                                                                <i class="fa fa-image"></i>
                                                                <span>GST Certificate</span>
                                                            </a>

                                                        @endif

                                                        @if(strpos($value["gst_certification_image_url"], '.pdf'))
                                                            <a href="{{ $value['gst_certification_image_url'] }}" class="document_box pdffile">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                                <span>GST Certificate</span>
                                                            </a>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>                                    
                                        </div>
                                    </div>
                                @endforeach
                            @else
                               <p style="text-align: center;">No Record Found</p>
                            @endif
                            
                            
                            <div class="pagination-block">
                                <!-- <ul class="pagination justify-content-end m-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                    </li>
                                </ul>
                                <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> 
                                 
                                -->
                                @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)
                                <div class="pagination justify-content-end m-0">
                                    @if(isset($first_created_date))
                                    <form action="{{ route(Route::current()->getName()) }}" method="get">
                                        @if(Request::get('name'))
                                            <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                        @endif
                                        @if(Request::get('mobile_no'))
                                            <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                                        @endif
                                        @if(Request::get('email'))
                                            <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        @if(Request::get('user_id'))
                                            <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                        <button type="submit" class="site-button">Previous</button>                                    
                                    </form>
                                    @endif
                                    @if(isset($last_created_date))
                                    <form action="{{ route(Route::current()->getName()) }}" method="get">
                                        @if(Request::get('name'))
                                            <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                        @endif
                                        @if(Request::get('mobile_no'))
                                            <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                                        @endif
                                        @if(Request::get('email'))
                                            <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        @if(Request::get('user_id'))
                                            <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                        <button type="submit" class="site-button">Next</button>
                                    </form>
                                    @endif
                                    
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--vehicle category list end -->
        </div>

@endsection