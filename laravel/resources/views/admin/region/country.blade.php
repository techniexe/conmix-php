@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap mini-middle-container-wrap">
            <h1 class="breadcrums">Region / <span>Country</span></h1>
            <div class="clearfix"></div>
            <!--Region start -->
            <div class="region-block region-country-block wht-tble-bg">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="sub-title-block">
                        @if(isset($data["error"]))
                            <div class="">{{ $data["error"]["message"] }}</div>
                        @endif
                            <h2 class="sub-title pull-left">Country</h2>
                            <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                            <button class="site-button pull-right m-r10" data-toggle="modal" data-target="#country-popup" onclick="resetForm('add_country_form','country-popup')">Add</button> -->
                        </div>
                        <div class="comn-table1 m-b20 pull-left w-100">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Country Id</th>
                                        <th>Country Name</th>
                                        <th>Country Code</th>
                                        <th>Date & Time</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($data["data"]) && count($data['data']) > 0)
                                    @php
                                        $first_created_date = '';
                                        $last_created_date = '';
                                        $count = 0;
                                    @endphp
                                    @foreach($data["data"] as $value)  
                                        @if($count == 0)
                                            @php 
                                                $first_created_date = $value["created_at"];
                                                $count++; 
                                            @endphp
                                        @endif
                                        @php
                                            $last_created_date = $value["created_at"];
                                        @endphp
                                    <tr>
                                        <td>{{ $value["country_id"] }}</td>
                                        <td>{{ $value["country_name"] }}</td>
                                        <td>{{ $value["country_code"] }}</td>
                                        <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                        <!-- <td>
                                            <a href="javascript:;" onclick="showEditCountryDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-edit"></i></a> 
                                            <a href="javascript:;" onclick="confirmPopup(6,'{{ json_encode($value) }}','{{ csrf_token() }}','{{ $value["country_id"] }}',0)" data-toggle="modal" data-target="#delete-alert-popup" class="site-button red button-sm" data-tooltip="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td> -->
                                    </tr>
                                   @endforeach
                                @else
                                    <tr>
                                         <td colspan="5">No Record Found</td>
                                    </tr>
                                @endif  
                                </tbody>
                            </table>
                        </div>
                        <div class="pagination-block">
                            <!-- <ul class="pagination justify-content-end m-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            </ul>
                            <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                            @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)
                            <div class="pagination justify-content-end m-0">
                                @if($first_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    @if(Request::get('country_name'))
                                        <input type="hidden" name="country_name" value="{{ Request::get('country_name') ? Request::get('country_name') : ''  }}" class="form-control" >
                                    @endif
                                    
                                    <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                    <button type="submit" class="site-button">Previous</button>                                    
                                </form>
                                @endif
                                @if($last_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    
                                    @if(Request::get('country_name'))
                                        <input type="hidden" name="country_name" value="{{ Request::get('country_name') ? Request::get('country_name') : ''  }}" class="form-control" >
                                    @endif
                                    <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                    <button type="submit" class="site-button">Next</button>
                                </form>
                                @endif
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!--Region end -->
        </div>

@endsection