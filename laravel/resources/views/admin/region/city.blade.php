@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap mini-middle-container-wrap">
    <h1 class="breadcrums">Region / <span>City</span></h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="region-block region-city-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">City</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_region_cities') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    @if($profile["rights"]["city_add"])
                        <button class="site-button pull-right m-r10" onclick="resetForm('add_city_form','city-popup')" data-toggle="modal" data-target="#city-popup">Add</button>
                    @endif
                </div>
                <div class="comn-table1 m-b20 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Country Name</th>
                                <th>Country Code</th>
                                <th>State Name</th>
                                <th>City Name</th>
                                <th>Date & Time</th>
                                @if(($profile["rights"]["city_edit"]) || ($profile["rights"]["city_delete"]))
                                    <th>Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                        @php
                                $first_created_date = '';
                                $last_created_date = '';
                                $count = 0;
                            @endphp
                        @if(isset($data["data"]) && count($data['data']) > 0)
                            
                            @foreach($data["data"] as $value)  
                                @if($count == 0)
                                    @php 
                                        $first_created_date = $value["created_at"];
                                        $count++; 
                                    @endphp
                                @endif
                                @php
                                    $last_created_date = $value["created_at"];
                                @endphp
                            <tr>
                                <td>{{ $value["country_name"] }}</td>
                                <td>{{ $value["country_code"] }}</td>
                                <td>{{ $value["state_name"] }}</td>
                                <td>{{ $value["city_name"] }}</td>
                                <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                
                                @if(($profile["rights"]["city_edit"]) || ($profile["rights"]["city_delete"]))
                                    <td class="text-nowrap">
                                        @if(($profile["rights"]["city_edit"]))    
                                        <a href="javascript:;" onclick="showEditCityDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-edit"></i></a> 
                                        @endif
                                        @if(($profile["rights"]["city_delete"]))
                                        <a href="javascript:;" onclick="confirmPopup(45,'{{ json_encode($value) }}','{{ csrf_token() }}','{{ $value["_id"] }}',1)" class="site-button red button-sm" data-tooltip="Delete"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">No Record Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>


                <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="pagination-block">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                   
                        <div class="pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if($first_created_date)
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                @if(Request::get('state_id'))
                                    <input type="hidden" name="state_id" value="{{ Request::get('state_id') ? Request::get('state_id') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('city_name'))
                                    <input type="hidden" name="city_name" value="{{ Request::get('city_name') ? Request::get('city_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('country_code'))
                                    <input type="hidden" name="country_code" value="{{ Request::get('country_code') ? Request::get('country_code') : ''  }}" class="form-control" >
                                @endif
                                
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if($last_created_date)
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('state_id'))
                                    <input type="hidden" name="state_id" value="{{ Request::get('state_id') ? Request::get('state_id') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('city_name'))
                                    <input type="hidden" name="city_name" value="{{ Request::get('city_name') ? Request::get('city_name') : ''  }}" class="form-control" >
                                @endif
                                
                                @if(Request::get('country_code'))
                                    <input type="hidden" name="country_code" value="{{ Request::get('country_code') ? Request::get('country_code') : ''  }}" class="form-control" >
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button ml-2">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>

@endsection