@extends('admin.layouts.admin_layout')

@section('content')
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap mini-middle-container-wrap">
            <h1 class="breadcrums">Product / <span>GST Info</span></h1>
            <div class="clearfix"></div>
            <!--Region start -->
            <div class="product-block product-category-block wht-tble-bg">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="sub-title-block">
                            <h2 class="sub-title pull-left">GST Info</h2>
                            <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                            @if($profile["rights"]["gst_slab_add"])
                                <button class="site-button pull-right m-r10" onclick="resetForm('add_gst_slab_form','add-gst-info-popup')" data-toggle="modal" data-target="#add-gst-info-popup">Add GST Info</button>
                            @endif
                        </div>
                        <div class="comn-table1 m-b20 pull-left w-100">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <!-- <th>HSN Code</th> -->
                                        <th>IGST Rate</th>
                                        <th>SGST Rate</th>
                                        <th>CGST Rate</th>
                                        @if($profile["rights"]["gst_slab_edit"])
                                            <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($data["data"]) && !empty($data["data"]))
                                    @foreach($data["data"] as $value)
                                        <tr>
                                            <td>{{ $value["title"] }}</td>
                                            <!-- <td>{{ isset($value["hsn_code"]) ? $value["hsn_code"] : '' }}</td> -->
                                            <td>{{ $value["igst_rate"] }}%</td>
                                            <td>{{ $value["sgst_rate"] }}%</td>
                                            <td>{{ $value["cgst_rate"] }}%</td>
                                            @if($profile["rights"]["gst_slab_edit"])
                                            <td>
                                                <div class="bil-action-block">
                                                    @if($profile["rights"]["gst_slab_edit"])
                                                        <div class="dis-inline-block">
                                                            <a href="javascript:;" onclick="showEditGSTDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td colspan="6">No Data Found</td>
                                </tr>
                            @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="pagination-block">
                            <!-- <ul class="pagination justify-content-end m-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            </ul>
                            <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        </div>
                    </div>
                </div>
            </div>
            <!--Region end -->
        </div>

@endsection