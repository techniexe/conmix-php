@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums">Product / <span>Product Detail</span></h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="product-list-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Product Detail</h2>
                    <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                </div>
                <div class="product-list-block">
                    @if(isset($data["data"]))
                        <?php $product = $data["data"]; ?>
                        @php
                            $selling_unit = '';
                            $lattitude = '';
                            $longitude = '';
                            
                        @endphp
                        @php
                            $lattitude = isset($product['address']['location']['coordinates'][1]) ? $product['address']['location']['coordinates'][1] : '';
                            $longitude = isset($product['address']['location']['coordinates'][0]) ? $product['address']['location']['coordinates'][0] : '';
                        @endphp
                        <div class="product-list-contet-block m-b20">
                            <div class="row">
                                <form action="" method="post" name="product_verify_form_{{ $product['_id'] }}" id="product_verify_form_{{ $product['_id'] }}">
                                    @csrf
                                    <div class="verified-switch-btn cstm-css-checkbox">
                                        <label class="new-switch switch-green">
                                            
                                                <input type="hidden" name="product_id" product="{{ $product['_id'] }}" />
                                                <input type="checkbox" name="verified_by_admin" data-product-id="{{ $product['_id'] }}" id="product_verify_by_admin_{{ $product['_id'] }}" class="switch-input" {{ $product['verified_by_admin'] == true ? 'checked' : '' }}>
                                                <span class="switch-label" data-on="Verified" data-off="Unverified"></span>
                                                <span class="switch-handle"></span>
                                            
                                        </label>
                                    </div>
                                </form>
                                <script>
                                    $('#product_verify_by_admin_{{ $product['_id'] }}').change(function() {
                                        $is_checked = '';
                                        if(this.checked) {
                                            $is_checked = 'true';
                                        }else{
                                            $is_checked = 'false';
                                        }

                                        var product_id = $(this).attr("data-product-id");

                                        request_data = $('#product_verify_form_'+product_id).serialize();
                                        console.log(request_data);
                                        requestOTP(11,request_data,"");
                                        
                                    });
                                </script>
                                <!-- <div class="eedit-product">
                                    <a href="javascript:;" onclick="showEditProductDialog('{{ json_encode($product) }}')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                </div> -->
                                <div class="product-img-block">
                                    <img src="{{ isset($product['productCategory']['image_url']) ? $product['productCategory']['image_url'] : '' }}" alt="" />
                                </div>
                                <div class="product-list-details-block row">
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <h3 class="product-list-title">{{ isset($product['productCategory']['category_name']) ? $product['productCategory']['category_name'] : '' }} - {{ isset($product['productSubCategory']['sub_category_name']) ? $product['productSubCategory']['sub_category_name'] : '' }}</h3>
                                        <ul>
                                            <li>
                                                <p><span class="vTitle">Category: </span> <span class="vName">{{ isset($product['productCategory']['category_name']) ? $product['productCategory']['category_name'] : '' }} - {{ isset($product['productSubCategory']['sub_category_name']) ? $product['productSubCategory']['sub_category_name'] : '' }}</span></p>
                                            </li>
                                            <li>
                                                <p><span class="vTitle">Unit Price - (For 1MT): </span> <span class="vName"><i class="fa fa-rupee"></i> {{ isset($product['unit_price']) ? $product['unit_price'] : '' }}</span></p>
                                            </li>
                                            <!-- <li>
                                                <p><span class="vTitle">Qty: </span> <span class="vName">{{ isset($product['quantity']) ? $product['quantity'] : '' }} MT</span></p>
                                            </li> -->
                                            <li>
                                                <p><span class="vTitle">Min. Order: </span> <span class="vName">{{ isset($product['minimum_order']) ? $product['minimum_order'] : '' }} MT</span></p>
                                            </li>
                                            <!-- <li>
                                                <p><span class="vTitle">Created On: </span> <span class="vName">18 Jul 2019</span></p>
                                            </li> -->
                                            <li>
                                                <p><span class="vTitle">Stock as on Today: </span> <span class="vName">{{ isset($product['quantity']) ? $product['quantity'] : '' }} MT</span></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <h3 class="product-list-title">Pickup Address <a href="javascript:;" onclick="viewMap({{ $lattitude }}, {{ $longitude }})" data-tooltip="View Map" class="pl-vw-mp"><i class="fa fa-map-marker"></i></a></h3>
                                        <div class="product-address-details">
                                            <!-- <h4>krishnamurthy Subramanian</h4>
                                            <p>+91 9863258741</p> -->
                                            <p>{{ isset($product['address']['line1']) ? $product['address']['line1'] : '' }} <br> {{ isset($product['address']['line2']) ? $product['address']['line2'] : '' }}, {{ isset($product['address']['city_name']) ? $product['address']['city_name'] : '' }} {{ isset($product['address']['pincode']) ? $product['address']['pincode'] : '' }}, {{ isset($product['address']['state_name']) ? $product['address']['state_name'] : '' }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

@endsection