@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap">
            <h1 class="breadcrums">Category / <span> Aggregate (VSI) & Sand Sub Category </span></h1>
            <div class="clearfix"></div>
            <!--Region start -->
            <div class="product-block product-sub-category-block wht-tble-bg">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="sub-title-block">
                            <h2 class="sub-title pull-left m-b10">Select Category</h2>
                            <div class="hed_from_btn_main">
                                <form action="{{ route(Route::current()->getName())  }}" name="product_sub_cat_form" method="get"> 
                                    @csrf
                                    @if(Request::get('search'))
                                            <input type="hidden" name="search" value="{{ Request::get('search') ? Request::get('search') : ''  }}" class="form-control" >
                                        @endif
                                    <div class="form-group">
                                        <div class="cstm-select-box">
                                          <!--   <label>Product Category <span class="str">*</span></label> -->
                                            <select id="product_category_subcategory_list" name="categoryId" onchange="this.form.submit()" class="" data-placeholder="Select Product Category">
                                                <option>Select Category</option>
                                                @if(isset($data["product_cat_data"]))
                                                    @foreach($data["product_cat_data"] as $value)
                                                        <option value="{{ $value['_id'] }}" {{ Request::get('categoryId') != null ? (Request::get('categoryId') == $value['_id'] ? 'selected' : '') : ($value["category_name"] == 'Aggregate' ? 'selected' : '') }} >{{ $value["category_name"] }}</option>
                                                    @endforeach
                                                @endif
                                                
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <div class='hed_from_btn'>
                                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                                    <a href="{{ route('admin_show_product_sub_category') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                                    @if($profile["rights"]["aggregate_sand_sub_add"])
                                        <button class="site-button pull-right m-r10" onclick="resetForm('add_product_sub_cat_form','product-sub-category-popup')" data-toggle="modal" data-target="#product-sub-category-popup">Add</button>
                                    @endif
                                </div>
                            </div>  
                        </div>
                        
                        <div class="comn-table1 m-b20 pull-left w-100">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product Img</th>
                                        <th>Category</th>
                                        <th>Margin Slab</th>
                                        <th>GST</th>
                                        <th>Min. Qty</th>
                                        <th>Date & Time</th>
                                        @if($profile["rights"]["aggregate_sand_sub_edit"])
                                            <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                        $first_created_date = '';
                                        $last_created_date = '';
                                        $count = 0;
                                    @endphp
                                @if(isset($data["data"]))
                                   
                                @endif
                                @if(isset($data["data"]) && !empty($data['data']))
                                    
                                    @foreach($data['data'] as $key => $value)
                                        @if($count == 0)
                                                @php 
                                                    $first_created_date = $value["created_at"];
                                                    $count++; 
                                                @endphp
                                            @endif
                                            @php
                                                $json_value = json_encode($value);
                                                //dump($json_value);
                                                //dump($value['min_quantity']["MT"]);
                                                $last_created_date = $value["created_at"];
                                            @endphp
                                        <tr>
                                            <td>
                                                <div class="product-img">
                                                    <img src="{{ isset($value['image_url']) ? $value['image_url'] : '' }}" alt="" />
                                                </div>    
                                            </td>
                                            <td>{{ $value["sub_category_name"] }}</td>
                                            <td>
                                            @if(isset($value["margin_rate"]))
                                                {{ $value["margin_rate"]["title"] }}
                                            @endif
                                            </td>
                                            <td>
                                                <div class="slab_txt">
                                                    <div class="slab_txt_contnt">GST: <span> {{ $value["gst_slab"]["title"] }}</span></div>
                                                </div>
                                                <div class="slab_txt">
                                                    <div class="slab_txt_contnt">IGST: <span> {{ $value["gst_slab"]["igst_rate"] }}</span></div>
                                                </div>
                                                <div class="slab_txt">
                                                    <div class="slab_txt_contnt">SGST: <span> {{ $value["gst_slab"]["sgst_rate"] }}</span></div>
                                                </div>
                                                <div class="slab_txt">
                                                    <div class="slab_txt_contnt">CGST: <span> {{ $value["gst_slab"]["cgst_rate"] }}</span></div>
                                                </div>
                                            </td>
                                            <td>
                                            @if(isset($value["min_quantity"]))
                                                @foreach($value["min_quantity"] as $key_min => $value_min)

                                                    {{ $value_min }} {{ $key_min }}

                                                @endforeach
                                            @endif

                                            </td>
                                            <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                            @if($profile["rights"]["aggregate_sand_sub_edit"])
                                                <td>
                                                    <div class="bil-action-block">
                                                        @if($profile["rights"]["aggregate_sand_sub_edit"])
                                                            <div class="dis-inline-block">
                                                                <a href="javascript:;" onclick="showEditProductSubCatDialog('<?php echo $value['sub_category_name']; ?>','<?php echo $value['category_id']; ?>','<?php echo $value['gst_slab_id']; ?>','<?php echo $value['margin_rate_id']; ?>','<?php echo $value['selling_unit'][0]; ?>','<?php echo $value['min_quantity']['MT']; ?>','<?php echo $value['_id']; ?>','<?php echo $value['image_url']; ?>')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                @else
                                    <tr class="common" align="center" style="height: 100px;">
                                        @if(isset($_GET['categoryId']))
                                            <td colspan="8">Records are not available for this category.</td>
                                        @else
                                            <td colspan="8">Please select product category from above list</td>
                                        @endif

                                    </tr>   
                                @endif
                                   
                                </tbody>
                            </table>
                        </div>


                        <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                        <div class="pagination-block">
                            <!-- <ul class="pagination justify-content-end m-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            </ul>
                            <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                            <div class="pagination justify-content-end m-0">
                            @if($is_previous_avail == 1)
                                @if($first_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    
                                    @if(Request::get('search'))
                                        <input type="hidden" name="search" value="{{ Request::get('search') ? Request::get('search') : ''  }}" class="form-control" >
                                    @endif
                                    @if(Request::get('margin_rate_id'))
                                        <input type="hidden" name="margin_rate_id" value="{{ Request::get('margin_rate_id') ? Request::get('margin_rate_id') : ''  }}" class="form-control" >
                                    @endif
                                    @if(Request::get('categoryId'))
                                        <input type="hidden" value="{{ Request::get('categoryId') ? Request::get('categoryId') : '' }}" name="categoryId"/>
                                    @endif
                                    
                                    <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                    <button type="submit" class="site-button">Previous</button>                                    
                                </form>
                                @endif
                                @endif
                                @if($is_next_avail == 1)
                                @if($last_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    
                                    
                                    @if(Request::get('search'))
                                        <input type="hidden" name="search" value="{{ Request::get('search') ? Request::get('search') : ''  }}" class="form-control" >
                                    @endif
                                    @if(Request::get('margin_rate_id'))
                                        <input type="hidden" name="margin_rate_id" value="{{ Request::get('margin_rate_id') ? Request::get('margin_rate_id') : ''  }}" class="form-control" >
                                    @endif
                                    @if(Request::get('categoryId'))
                                        <input type="hidden" value="{{ Request::get('categoryId') ? Request::get('categoryId') : '' }}" name="categoryId"/>
                                    @endif
                                    <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                    <button type="submit" class="site-button">Next</button>
                                </form>
                                @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Region end -->
        </div>

@endsection
