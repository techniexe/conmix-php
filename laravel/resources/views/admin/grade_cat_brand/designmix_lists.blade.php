@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<!-- <div class="middle-container-wrap">
    <h1 class="main-title">Design Mix</h1>
    <div class="clearfix"></div>
   
    <div class="bank-detail-main-block design_mix_bx wht-tble-bg">
        <div class="row">
            <div class="col-md-12">
                <ul>
                   
                    @if(isset($data["data"]))
                    @php
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                        <li>
                            <div class="bank-details-select">
                                
                               
                                <div class="bank-detail-block p-a15" onclick="viewProductDesignMixOfGrade('{{ $value['_id'] }}')">
                                    <span>{{ isset($value["concrete_grade"]["name"]) ? $value["concrete_grade"]["name"] : '' }}</span>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    @endif
                    

                    
                </ul>
            </div>
        </div>
    </div>
    
</div> -->

<div class="middle-container-wrap mini-middle-container-wrap">
    <h1 class="breadcrums">Grade / <span>Design Mix</span></h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="product-block product-category-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Design Mix</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_product_list') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    <!-- <button class="site-button pull-right m-r10" onclick="resetForm('add_grade_form','concrete-grade-popup')" data-toggle="modal" data-target="#concrete-grade-popup">Add</button> -->
                </div>
                <div class="comn-table1 m-b20 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <!-- <th>Product Img</th> -->
                                <th>Supplier Name</th>
                                <th>Concrete Grade</th>
                                <th>Date & Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                                $first_created_date = '';
                                $last_created_date = '';
                                $count = 0;
                            @endphp
                        @if(isset($data["data"]) && count($data['data']) > 0)
                            
                            @foreach($data["data"] as $value)  
                                @if($count == 0)
                                    @php 
                                        //dd($data["data"]);
                                        $first_created_date = $value["created_at"];
                                        $count++; 
                                    @endphp
                                @endif
                                @php
                                    $last_created_date = $value["created_at"];
                                @endphp
                                <tr>
                                    
                                    <td>{{ isset($value["vendor"]["company_name"]) ? $value["vendor"]["company_name"] : '' }}</td>
                                    <td>{{ isset($value["concrete_grade"]["name"]) ? $value["concrete_grade"]["name"] : '' }}</td>
                                    <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                    <td>
                                        <div class="bil-action-block">
                                            <div class="dis-inline-block">
                                                <a href="javascript:;" onclick="viewProductDesignMixOfGrade('{{ $value['_id'] }}')" class="site-button green button-sm" data-tooltip="View"><i class="fa fa-eye"></i></a>
                                            </div>
                                            <!-- <div class="dis-inline-block">
                                                <a href="javascript:;" data-toggle="modal" data-target="#delete-alert-popup" class="site-button red button-sm" data-tooltip="Delete"><i class="fa fa-trash-o"></i></a>
                                            </div> -->
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No Record Found</td>
                            </tr>
                        @endif
                            
                        </tbody>
                    </table>
                </div>

                <?php 
            
            $is_previous_avail = 0;
            $is_next_avail = 0;

            ?>
            @if(isset($data["data"]) && count($data['data']) > 0)

                @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                    @if(!Request::get('before') && !Request::get('after'))

                        <?php $is_next_avail = 1; ?>
                        <?php $is_previous_avail = 0; ?>

                    @else

                        <?php $is_next_avail = 1; ?>
                        <?php $is_previous_avail = 1; ?>

                    @endif
                    

                @else

                    @if(Request::get('after'))
                        <?php $is_next_avail = 1; ?>
                        
                        
                    @elseif(Request::get('before'))
                        <?php $is_previous_avail = 1; ?>

                    @endif

                @endif
            @else

                @if(Request::get('before'))

                    <?php $is_previous_avail = 1; ?>

                    @if($first_created_date == '')

                        <?php $first_created_date = Request::get('before'); ?>

                    @endif

                    

                @endif
                
                @if(Request::get('after'))

                    <?php $is_next_avail = 1; ?>

                    @if($last_created_date == '')

                        <?php $last_created_date = Request::get('after'); ?>

                    @endif

                @endif


            @endif

                <div class="pagination-block">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                    
                        <div class="pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if($first_created_date)
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('search'))
                                    <input type="hidden" name="search" value="{{ Request::get('search') ? Request::get('search') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('grade_id'))
                                    <input type="hidden" name="grade_id" value="{{ Request::get('grade_id') ? Request::get('grade_id') : ''  }}" class="form-control" >
                                @endif
                                
                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if($last_created_date)
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                
                                @if(Request::get('search'))
                                    <input type="hidden" name="search" value="{{ Request::get('search') ? Request::get('search') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('grade_id'))
                                    <input type="hidden" name="grade_id" value="{{ Request::get('grade_id') ? Request::get('grade_id') : ''  }}" class="form-control" >
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>


@endsection