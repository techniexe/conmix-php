@extends('admin.layouts.admin_layout')
@section('content')
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap mini-middle-container-wrap">
            <h1 class="breadcrums">Product / <span>Margin Slab</span></h1>
            <div class="clearfix"></div>
            <!--Region start -->
            <div class="product-block product-category-block wht-tble-bg">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="sub-title-block">
                            <h2 class="sub-title pull-left">Margin Slab</h2>
                            <!-- <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                            @if($profile["rights"]["margin_slab_add"])
                                <button class="site-button pull-right m-r10" data-toggle="modal" data-target="#add-margin-slab-popup" onclick="resetForm('add_margin_slab_form','add-margin-slab-popup')">Add Margin Slab</button>
                            @endif
                        </div>
                        <div class="comn-table1 m-b20 pull-left w-100">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Slab Title</th>
                                        <th>Slab</th>
                                        @if($profile["rights"]["margin_slab_edit"])
                                            <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($data["data"]) && !empty($data["data"]))
                                    @php

                                        $count = 0;
                                    @endphp
                                    @foreach($data["data"] as $value)
                                        @php $count++ @endphp
                                        <tr>
                                            <td>{{ $count }}</td>
                                            <td>{{ $value["title"] }}</td>
                                            <td>
                                                @if(isset($value["slab"]))
                                                    @foreach($value["slab"] as $slab_value)
                                                        <div class="mrg_slab">
                                                            <div class="mrg_slb_contnt">Upto Price : <span><i class="fa fa-rupee"></i> {{ $slab_value["upto"] }}</span></div> 
                                                            <div class="mrg_slb_contnt">Margin Rate : <span>{{ $slab_value["rate"] }}%</span></div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                                
                                            </td>
                                            @if($profile["rights"]["margin_slab_edit"])
                                            <td>
                                                <div class="bil-action-block">
                                                    @if($profile["rights"]["margin_slab_edit"])
                                                        <div class="dis-inline-block">
                                                            <a href="javascript:;" onclick="showEditMarginDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                               @else
                                <tr>
                                    <td colspan="4">No Data Found</td>
                                </tr>
                            @endif
                                   
                                </tbody>
                            </table>
                            <p>Note: For more than 100000 price 5.5 margin will be applicable</p>
                        </div>
                        <!-- <div class="pagination-block">
                            <ul class="pagination justify-content-end m-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                </li>
                            </ul>
                            <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p>
                        </div> -->
                    </div>
                </div>
            </div>
            <!--Region end -->
        </div>

@endsection