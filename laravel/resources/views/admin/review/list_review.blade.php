@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap">
    <h1 class="breadcrums">Review & Feedback / <span>List Review</span></h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="review-complaints-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">List Review</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_product_review') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                </div>
                <div class="review-complaints-details-block">
                @php
                
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                @if(isset($data["data"]) && count($data['data']) > 0)
                    
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                            //dd($data["data"]);
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp

                        <div class="rc-contet-block">
                            <div class="lr-link-block">
                                @if($profile["rights"]["review_edit"])
                                    <a href="javascript:;" onclick="showEditProductReviewDialog('{{ json_encode($value) }}')" class="site-button green button-sm pull-right" data-tooltip="Edit"><i class="fa fa-edit"></i></a>
                                @endif
                            </div>
                            <div class="rc-list-RC-block">
                                <!-- <div class="rc-list-RC-img-block">
                                    <img src="" alt="" />
                                </div> -->
                                <div class="rc-list-RC-content-block">
                                    <div class="rc-list-RC-title-product-block">
                                        <h2>{{ isset($value["buyer_details"]["full_name"]) ? $value["buyer_details"]["full_name"] : '' }} : [ <span class="product-text">Vendor: {{ isset($value["vendor_details"]["full_name"]) ? $value["vendor_details"]["full_name"] : ''  }}</span> ]</h2> 
                                        <span class="badge {{ ($value['status'] == 'published') ? 'bg-green' : 'bg-red'  }}">{{ isset($value["status"]) ? $value["status"] : ''  }}</span>
                                    </div>
                                    <div class="rc-list-RC-rating-block">
                                        <fieldset class="rating">
                                            
                                        <div class="my-rating" id="{{ $value['_id'] }}_rating"></div>
                                        <script>

                                            $("#{{ $value['_id'] }}_rating").starRating({
                                                starSize: 22,
                                                readOnly:true,
                                                callback: function(currentRating, $el){
                                                    // make a server call here
                                                }
                                            });

                                            @if(isset($value["rating"]))
                                                
                                                $('#{{ $value['_id'] }}_rating').starRating('setRating', {{ $value['rating'] }});

                                            @endif

                                        </script>

                                        </fieldset>
                                    </div>
                                    <div class="rc-list-RC-description-block">
                                        <p>{{ isset($value["review_text"]) ? $value["review_text"] : '' }}</p>
                                    </div>
                                    <div class="rc-list-RC-date-block">
                                        <span>Post On : {{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span>
                                        @if(isset($value["modirated_at"]))
                                            <span>Modify On : {{ isset($value["modirated_at"]) ? AdminController::dateTimeFormat($value["modirated_at"]) : '' }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                         <div style="text-align: center;">No Record Found</div>
                @endif



                <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif
                    
                    
                    
                    <div class="pagination-block m-t20">
                        
                        <div class="pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('buyer_name'))
                                    <input type="hidden" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('company_name'))
                                    <input type="hidden" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('review_user_id'))
                                    <input type="hidden" value="{{ Request::get('review_user_id') ? Request::get('review_user_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('search_term'))
                                    <input type="hidden" value="{{ Request::get('search_term') ? Request::get('search_term') : '' }}" name="search_term"/>
                                @endif
                                @if(Request::get('status'))
                                    <input type="hidden" value="{{ Request::get('status') ? Request::get('status') : '' }}" name="status"/>
                                @endif
                                
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('buyer_name'))
                                    <input type="hidden" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('company_name'))
                                    <input type="hidden" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('review_user_id'))
                                    <input type="hidden" value="{{ Request::get('review_user_id') ? Request::get('review_user_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('search_term'))
                                    <input type="hidden" value="{{ Request::get('search_term') ? Request::get('search_term') : '' }}" name="search_term"/>
                                @endif
                                @if(Request::get('status'))
                                    <input type="hidden" value="{{ Request::get('status') ? Request::get('status') : '' }}" name="status"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>



@endsection