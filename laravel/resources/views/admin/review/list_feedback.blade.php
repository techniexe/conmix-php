@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums">Review & Feedback / <span>List Feedback</span></h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="review-complaints-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">List Feedback</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_feedback') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                </div>
                <div class="review-complaints-details-block">
                @php
                
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                @if(isset($data["data"]) && count($data['data']) > 0)
                    
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                            //dd($data["data"]);
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp

                        <div class="rc-contet-block">
                            <!-- <div class="lr-link-block">
                                <a href="javascript:;" onclick="showEditProductReviewDialog('{{ json_encode($value) }}')" class="site-button green button-sm pull-right" data-tooltip="Edit"><i class="fa fa-edit"></i></a>
                            </div> -->
                            <div class="rc-list-RC-block">
                                <!-- <div class="rc-list-RC-img-block">
                                    <img src="" alt="" />
                                </div> -->
                                <div class="rc-list-RC-content-block">
                                    <div class="rc-list-RC-title-product-block">
                                        <h2>{{ isset($value["buyer_details"]["full_name"]) ? $value["buyer_details"]["full_name"] : '' }} </h2> 
                                        <span class="badge {{ ($value['feedback_type'] == 'bad') ? 'bg-red' : 'bg-green'  }}">{{ isset($value["feedback_type"]) ? $value["feedback_type"] : ''  }}</span>
                                    </div>
                                    <div class="rc-list-RC-rating-block">
                                        
                                    </div>
                                    <div class="rc-list-RC-description-block">
                                        <p>{{ isset($value["feedback"]) ? $value["feedback"] : '' }}</p>
                                    </div>
                                    <div class="rc-list-RC-date-block">
                                        <span>Order Id : <a href="{{ url('admin/orders/details/'.$value['order_id']) }}">{{ isset($value["order_details"]["display_id"]) ? '#'.$value["order_details"]["display_id"] : '' }}</a></span>
                                        
                                    </div><div class="rc-list-RC-date-block">
                                        <span>Post On : {{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                         <div style="text-align: center;">No Record Found</div>
                @endif



                <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif
                    
                    
                    
                    <div class="pagination-block m-t20">
                        
                        <div class="pagination justify-content-end m-0">
                        @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('buyer_name'))
                                    <input type="hidden" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('company_name'))
                                    <input type="hidden" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('review_user_id'))
                                    <input type="hidden" value="{{ Request::get('review_user_id') ? Request::get('review_user_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('search_term'))
                                    <input type="hidden" value="{{ Request::get('search_term') ? Request::get('search_term') : '' }}" name="search_term"/>
                                @endif
                                @if(Request::get('status'))
                                    <input type="hidden" value="{{ Request::get('status') ? Request::get('status') : '' }}" name="status"/>
                                @endif
                                
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('buyer_name'))
                                    <input type="hidden" name="buyer_name" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('company_name'))
                                    <input type="hidden" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('review_user_id'))
                                    <input type="hidden" value="{{ Request::get('review_user_id') ? Request::get('review_user_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('search_term'))
                                    <input type="hidden" value="{{ Request::get('search_term') ? Request::get('search_term') : '' }}" name="search_term"/>
                                @endif
                                @if(Request::get('status'))
                                    <input type="hidden" value="{{ Request::get('status') ? Request::get('status') : '' }}" name="status"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>



@endsection