@extends('admin.layouts.admin_layout')
@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
            <h1 class="main-title">Dashboard</h1>
            <div class="clearfix"></div>
            <!--Order Trip-->
            <div class="row">

            
                <!-- Sales Details -->

                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                    <?php //dd($data["data"]); ?>
                      <h3><i class="fa fa-rupee"></i> 
                      @if(isset($data["data"]["totalOrderAmount"][0]["total"]))
                        {{ isset($data["data"]["totalOrderAmount"][0]["total"]) ? number_format($data["data"]["totalOrderAmount"][0]["total"],2) : '   0' }}
                      @elseif(isset($data["data"]["totalOrderAmount"]))
                        {{ isset($data["data"]["totalOrderAmount"]) ? number_format($data["data"]["totalOrderAmount"],2) : '   0' }}
                      @endif
                      </h3>
                      <p>Total Sales</p>
                    </div>
                    <div class="icon bg-aqua">
                      <i class="fa fa-calculator"></i>
                    </div>

                  </div>
                </div>

                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3><i class="fa fa-rupee"></i> 
                      @if(isset($data["data"]["todayOrderAmount"][0]["total"]))
                        {{ isset($data["data"]["todayOrderAmount"][0]["total"]) ? number_format($data["data"]["todayOrderAmount"][0]["total"],2) : '   0' }}
                      @elseif(isset($data["data"]["todayOrderAmount"]))
                        {{ isset($data["data"]["todayOrderAmount"]) ? number_format($data["data"]["todayOrderAmount"],2) : '   0' }}
                      @endif
                    </h3>
                      <p>Today's Sales</p>
                    </div>
                    <div class="icon bg-red">
                      <i class="fa fa-calculator"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3><i class="fa fa-rupee"></i> 
                      @if(isset($data["data"]["thisWeekOrderAmount"][0]["total"]))
                        {{ isset($data["data"]["thisWeekOrderAmount"][0]["total"]) ? number_format($data["data"]["thisWeekOrderAmount"][0]["total"],2) : '   0' }}
                      @elseif(isset($data["data"]["thisWeekOrderAmount"]))
                        {{ isset($data["data"]["thisWeekOrderAmount"]) ? number_format($data["data"]["thisWeekOrderAmount"],2) : '   0' }}
                      @endif
                    </h3>
                      <p>This Week Sales</p>
                    </div>
                    <div class="icon bg-purple">
                      <i class="fa fa-calculator"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3><i class="fa fa-rupee"></i> 
                      @if(isset($data["data"]["thisMonthAmount"][0]["total"]))
                        {{ isset($data["data"]["thisMonthAmount"][0]["total"]) ? number_format($data["data"]["thisMonthAmount"][0]["total"],2) : '   0' }}
                      @elseif(isset($data["data"]["thisMonthAmount"]))
                        {{ isset($data["data"]["thisMonthAmount"]) ? number_format($data["data"]["thisMonthAmount"],2) : '   0' }}
                      @endif
                    </h3>
                      <p>This Month Sales</p>
                    </div>
                    <div class="icon bg-green">
                    <i class="fa fa-calculator"></i>
                    </div>

                  </div>
                </div>
                <!-- Admin Order Details Start.... -->
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                    <?php //dd($data["data"]); ?>
                      <h3>
                        {{ isset($data["data"]["totalOrder"]) ? $data["data"]["totalOrder"] : '   0' }}
                      </h3>
                      <p>Total Order</p>
                    </div>
                    <div class="icon bg-aqua">
                      <i class="fa fa-shopping-bag"></i>
                    </div>

                  </div>
                </div>

                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3>{{ isset($data["data"]["todayOrder"]) ? $data["data"]["todayOrder"] : '   0' }}</h3>
                      <p>Today's Order</p>
                    </div>
                    <div class="icon bg-red">
                      <i class="fa fa-shopping-bag"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3>{{ isset($data["data"]["OrderOfThisWeek"]) ? $data["data"]["OrderOfThisWeek"] : '   0' }}</h3>
                      <p>This Week Order</p>
                    </div>
                    <div class="icon bg-purple">
                      <i class="fa fa-shopping-bag"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3>{{ isset($data["data"]["OrderOfThisMonth"]) ? $data["data"]["OrderOfThisMonth"] : '   0' }}</h3>
                      <p>This Month Order</p>
                    </div>
                    <div class="icon bg-green">
                      <i class="fa fa-shopping-bag"></i>
                    </div>

                  </div>
                </div>

                <!-- Admin Order Details Over.... -->
                <!-- Suppliers Details................... -->

                    <div class="col-lg-12 col-sm-12 col-xs-12"> <h3 class="small-box-Tittle">Suppliers</h3></div>
                  <!-- Sales Details... -->

                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3><i class="fa fa-rupee"></i> 
                      @if(isset($data["data"]["totalVendorOrderAmount"][0]["total"]))
                        {{ isset($data["data"]["totalVendorOrderAmount"][0]["total"]) ? number_format($data["data"]["totalVendorOrderAmount"][0]["total"],2) : '   0' }}
                      @elseif(isset($data["data"]["totalVendorOrderAmount"]))
                        {{ isset($data["data"]["totalVendorOrderAmount"]) ? number_format($data["data"]["totalVendorOrderAmount"],2) : '   0' }}
                      @endif
                    </h3>
                      <p>Total Sales</p>
                    </div>
                    <div class="icon bg-yellow">
                      <i class="fa fa-calculator"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3><i class="fa fa-rupee"></i> 
                      @if(isset($data["data"]["todayVendorOrderAmount"][0]["total"]))
                        {{ isset($data["data"]["todayVendorOrderAmount"][0]["total"]) ? number_format($data["data"]["todayVendorOrderAmount"][0]["total"],2) : '   0' }}
                      @elseif(isset($data["data"]["todayVendorOrderAmount"]))
                        {{ isset($data["data"]["todayVendorOrderAmount"]) ? number_format($data["data"]["todayVendorOrderAmount"],2) : '   0' }}
                      @endif
                    </h3>
                      <p>Today's Sales</p>
                    </div>
                    <div class="icon bg-dblue">
                      <i class="fa fa-calculator"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3><i class="fa fa-rupee"></i> 
                      @if(isset($data["data"]["thisWeekVendorOrderAmount"][0]["total"]))
                        {{ isset($data["data"]["thisWeekVendorOrderAmount"][0]["total"]) ? number_format($data["data"]["thisWeekVendorOrderAmount"][0]["total"],2) : '   0' }}
                      @elseif(isset($data["data"]["thisWeekVendorOrderAmount"]))
                        {{ isset($data["data"]["thisWeekVendorOrderAmount"]) ? number_format($data["data"]["thisWeekVendorOrderAmount"],2) : '   0' }}
                      @endif

                      
                    </h3>
                      <p>This Week Sales</p>
                    </div>
                    <div class="icon bg-mpinch">
                      <i class="fa fa-calculator"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3><i class="fa fa-rupee"></i> 
                      @if(isset($data["data"]["thisMonthVendorOrderAmount"][0]["total"]))
                        {{ isset($data["data"]["thisMonthVendorOrderAmount"][0]["total"]) ? number_format($data["data"]["thisMonthVendorOrderAmount"][0]["total"],2) : '   0' }}
                      @elseif(isset($data["data"]["thisMonthVendorOrderAmount"]))
                        {{ isset($data["data"]["thisMonthVendorOrderAmount"]) ? number_format($data["data"]["thisMonthVendorOrderAmount"],2) : '   0' }}
                      @endif
                    </h3>
                      <p>This Month Sales</p>
                    </div>
                    <div class="icon bg-dgray">
                      <i class="fa fa-calculator"></i>
                    </div>

                  </div>
                </div>

                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3>{{ isset($data["data"]["totalVendorOrder"]) ? $data["data"]["totalVendorOrder"] : '   0' }}</h3>
                      <p>Total Order</p>
                    </div>
                    <div class="icon bg-yellow">
                      <i class="fa fa-truck"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3>{{ isset($data["data"]["todayVendorOrder"]) ? $data["data"]["todayVendorOrder"] : '   0' }}</h3>
                      <p>Today's Order</p>
                    </div>
                    <div class="icon bg-dblue">
                      <i class="fa fa-truck"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3>{{ isset($data["data"]["vendorOrderOfThisWeek"]) ? $data["data"]["vendorOrderOfThisWeek"] : '   0' }}</h3>
                      <p>This Week Order</p>
                    </div>
                    <div class="icon bg-mpinch">
                      <i class="fa fa-truck"></i>
                    </div>

                  </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-xl-3">
                  <div class="small-box bg-white">
                    <div class="inner">
                      <h3>{{ isset($data["data"]["vendorOrderOfThisMonth"]) ? $data["data"]["vendorOrderOfThisMonth"] : '   0' }}</h3>
                      <p>This Month Orders</p>
                    </div>
                    <div class="icon bg-dgray">
                      <i class="fa fa-truck"></i>
                    </div>

                  </div>
                </div>



                <!-- Suppliers Details Over................... -->


            </div>
            <!--Order Trip End-->
            <div class="clearfix"></div>
            <!--Monthly Report-->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="data-box">
                      <!--Chart Loader Start-->
                        <!-- <div class="overlay">
                          <span class="spinner"></span>
                        </div> -->
                         <!--Chart Loader End-->
        <?php
            $current_year = date("Y");
            $total_years = $current_year - 2019;
            

        ?>
                        <div class="data-box-header with-border data-box-header-dropdown" >
                          <h2 class="box-title" >Monthly Report</h2>
                            <div class="cust_datepicker_outer">
                                <div class="cstm-select-box monthly-report-action-block">
                                  <select id="monthly_order_data_user_filter">
                                      <option disabled="disabled" selected="selected">Select</option>
                                      <option value="buyer" selected>Buyer</option>
                                      <option value="vendor">Vendor</option>
                                  </select>
                                </div>

                                <div class="cstm-select-box m-l10">
                                    <select id="monthly_chart_data_year_filter" class="form-control">
                                        <option value="" selected="selected">Select Year</option>
                                          <?php $my_year = $current_year; ?>
                                        @for($i=0;$i<$total_years;$i++)
                                          <option value="{{ $my_year }}">{{ $my_year }}</option>

                                          <?php $my_year = $my_year - 1; ?>
                                        @endfor
                                            

                                    </select>
                                </div>

                            </div>


                        </div>
                        <div id="monthly_chart_loader" class="overlay" style="display:none">
                            <span class="spinner"></span>
                        </div>
                        <div class="data-box-body">




                          <div id="monthly-report-chart"></div>

                          <div class="chart_checkbox d-flex" id="monthly_chart">
                              <div class="pretty_checkbox check_blue">
                                  <input type="checkbox" id="monthly_chart_order" checked onclick="toggleMonthlyChartSeries(this)" value="Orders">
                                  <label for="monthly_chart_order">Orders</label>
                              </div>
                              <div class="pretty_checkbox check_green">
                                  <input type="checkbox" id="monthly_chart_quntity" checked onclick="toggleMonthlyChartSeries(this)" value="Cancel Orders">
                                  <label for="monthly_chart_quntity">Cancel Orders</label>
                              </div>
                              <div class="pretty_checkbox check_yellow">
                                  <input type="checkbox" id="monthly_chart_salse" checked onclick="toggleMonthlyChartSeries(this)" value="Sales Amount">
                                  <label for="monthly_chart_salse">Sales Amount</label>
                              </div>
                          </div>

                        </div>

                    </div>
                </div>
            </div>
            <!--Monthly Report End-->
            <div class="clearfix"></div>

        <!--Dayly Report-->
        <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="data-box">
                        <div class="data-box-header with-border data-box-header-dropdown">
                          <h2 class="box-title">Daily Report</h2>
                           <div class="cust_datepicker_outer">

                              <div class="cust_datepicker">
                                    <input type="text" id="daily_chart_year_filter" class="form-control from" readonly="" placeholder="Select Year Month">
                                </div>

                                <div class="cstm-select-box monthly-report-action-block m-l10">
                                  <select id="daily_order_data_user_filter">
                                      <option disabled="disabled" selected="selected">Select</option>
                                      <option value="buyer" selected>Buyer</option>
                                      <option value="vendor">Vendor</option>
                                  </select>
                                </div>

                            </div>

                        </div>
                        <div id="daily_chart_loader" class="overlay" style="display:none">
                            <span class="spinner"></span>
                        </div>
                        <div class="data-box-body">
                          <div id="daily-report-chart"></div>

                          <div class="chart_checkbox d-flex" id="daily_chart_legend">
                              <div class="pretty_checkbox check_blue">
                                  <input id="daily_chart_legend_order" type="checkbox" checked onclick="toggleDailyChartSeries(this)" value="Orders">
                                  <label for="daily_chart_legend_order">Orders</label>
                              </div>

                              <div class="pretty_checkbox check_green">
                                  <input id="daily_chart_legend_quantity" type="checkbox" checked onclick="toggleDailyChartSeries(this)" value="Cancel Orders">
                                  <label for="daily_chart_legend_quantity">Cancel Orders</label>
                              </div>

                              <div class="pretty_checkbox check_yellow">
                                  <input id="daily_chart_legend_sales" type="checkbox" checked onclick="toggleDailyChartSeries(this)" value="Sales Amount">
                                  <label for="daily_chart_legend_sales">Sales Amount</label>
                              </div>

                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Dayly Report End-->
            <div class="clearfix"></div>

           <!--Latest Orders start-->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="data-box">
                        <div class="data-box-header with-border">
                            <h2 class="box-title">Latest Orders Buyers</h2>
                        </div>
                        <div class="data-box-body">
                            <div class="comn-table latest-order-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Order Id</th>
                                            <th>Buyer Name</th>
                                            <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <th>Order Date & Time</th>
                                            <th>Order Status</th>
                                            <th>Payment Status</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($data["buyer_latest_order_data"]) && count($data["buyer_latest_order_data"]) > 0)
                                            @foreach($data["buyer_latest_order_data"] as $value)
                                                <tr>
                                                    <td><a href="{{ route('admin_show_order_details', $value['_id']) }}">#{{ $value["display_id"] }}</a></td>
                                                    <td>{{ $value["buyer"]["full_name"] }}</td>

                                                    <td> {{ isset($value["total_amount"]) ? number_format($value["total_amount"],2) : 0 }}</td>
                                                     <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                                    @if($value["order_status"] == 'DELIVERED')
                                                        <td><div class="badge bg-green">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                    @else @if($value["order_status"] == 'CANCELLED' || $value["order_status"] == 'REJECTED' || $value["order_status"] == 'LAPSED')
                                                        <td><div class="badge bg-red">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                    @else @if($value["order_status"] == 'PICKUP')
                                                        <td><div class="badge bg-purple">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>

                                                        @else
                                                            <td><div class="badge bg-yellow">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                        @endif
                                                        @endif
                                                    @endif
                                                    @if($value["payment_status"] == 'PAID')
                                                        <td><div class="badge bg-green">{{ $value["payment_status"] }}</div></td>
                                                    @else
                                                        <td><div class="badge bg-red">{{ $value["payment_status"] }}</div></td>
                                                    @endif
                                                    <!-- <td>
                                                        <div class="dropdown cstm-dropdown-select">
                                                            <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <li><a class="dropdown-item" href="{{ route('admin_show_order_details', $value['_id']) }}" >Change Delivery Status</a></li>
                                                                <li><a class="dropdown-item" href="{{ route('admin_show_order_details', $value['_id']) }}" >Change Payment Status</a></li>

                                                                <li><a class="dropdown-item" href="{{ route('admin_show_order_details', $value['_id']) }}" >Cancel</a></li>
                                                            </ul>
                                                        </div>
                                                    </td> -->
                                                </tr>
                                            @endforeach
                                        @else
                                            <td colspan="7">No Record Found</td>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="data-box-footer clearfix">
                            <div class="pull-right">
                                @if(isset($data["buyer_latest_order_data"]) && count($data["buyer_latest_order_data"]) > 0)
                                    <a href="{{ route('admin_show_orders') }}" class="site-button dark">View All</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Latest Orders End-->
             <!--Latest Cancel Order start-->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="data-box">
                        <div class="data-box-header with-border">
                            <h2 class="box-title">Cancelled Orders Buyers</h2>
                        </div>
                        <div class="data-box-body">
                            <div class="comn-table latest-order-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Order Id</th>
                                            <th>Buyer Name</th>

                                            <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <th>Order Date & Time</th>
                                            <th>Order Status</th>
                                            <th>Payment Status</th>
                                           <!-- <th>Remark</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($data["data"]["cancelOrderData"]) && count($data["data"]["cancelOrderData"]) > 0)
                                        @foreach($data["data"]["cancelOrderData"] as $value)
                                            <tr>
                                                <td><a href="{{ route('admin_show_order_details', $value['_id']) }}">#{{ $value["display_id"] }}</a></td>
                                                <td>{{ $value["buyer"]["full_name"] }}</td>

                                                <td>{{ isset($value["total_amount"]) ? number_format($value["total_amount"],2) : 0 }}</td>
                                                 <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                                @if($value["order_status"] == 'DELIVERED')
                                                <td><div class="badge bg-green">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'CANCELLED')
                                                    <td><div class="badge bg-red">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>

                                                    @else
                                                        <td><div class="badge bg-yellow">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                    @endif
                                                @endif
                                                @if($value["payment_status"] == 'PAID')
                                                    <td><div class="badge bg-green">{{ $value["payment_status"] }}</div></td>
                                                @else
                                                    <td><div class="badge bg-red">{{ $value["payment_status"] }}</div></td>
                                                @endif
                                                <!-- <td>
                                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <li>
                                                                <h3>Admin By :</h3>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                </p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td> -->
                                            </tr>
                                            @endforeach
                                        @else
                                            <td colspan="6">No Record Found</td>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="data-box-footer clearfix">
                            <div class="pull-right">
                            @if(isset($data["data"]["cancelOrderData"]) && count($data["data"]["cancelOrderData"]) > 0)
                                <a href="{{ route('admin_show_orders',['order_status'=>'CANCELLED']) }}" class="site-button dark">View All</a>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Latest Cancel Order end-->
 <!--Latest Processing Order Order start-->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="data-box">
                        <div class="data-box-header with-border">
                            <h2 class="box-title">Processing Order Buyer</h2>
                        </div>
                        <div class="data-box-body">
                            <div class="comn-table latest-order-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Order Id</th>
                                            <th>Buyer Name</th>

                                            <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <th>Order Date & Time</th>
                                            <th>Order Status</th>
                                            <th>Payment Status</th>
                                           <!-- <th>Remark</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($data["data"]["processingOrderData"]) && count($data["data"]["processingOrderData"]) > 0)
                                        @foreach($data["data"]["processingOrderData"] as $value)
                                        <tr>
                                            <td><a href="{{ route('admin_show_order_details', $value['_id']) }}">#{{ $value["display_id"] }}</a></td>
                                            <td>{{ $value["buyer"]["full_name"] }}</td>

                                            <td>{{ isset($value["total_amount"]) ? number_format($value["total_amount"],2) : 0 }}</td>
                                            <td>{{ AdminController::dateTimeFormat($value["created_at"])   }}</td>
                                            @if($value["order_status"] == 'DELIVERED')
                                                <td><div class="badge bg-green">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                            @else @if($value["order_status"] == 'CANCELLED')
                                                <td><div class="badge bg-red">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>

                                                @else
                                                    <td><div class="badge bg-yellow">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @endif
                                            @endif
                                            @if($value["payment_status"] == 'PAID')
                                                <td><div class="badge bg-green">{{ $value["payment_status"] }}</div></td>
                                            @else
                                                <td><div class="badge bg-red">{{ $value["payment_status"] }}</div></td>
                                            @endif
                                            <!-- <td>
                                                <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <h3>Admin By :</h3>
                                                            <p>
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td> -->
                                        </tr>
                                        @endforeach
                                    @else
                                        <td colspan="6">No Record Found</td>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="data-box-footer clearfix">
                            <div class="pull-right">
                            @if(isset($data["data"]["processingOrderData"]) && count($data["data"]["processingOrderData"]) > 0)
                                <a href="{{ route('admin_show_orders',['order_status'=>'PROCESSING']) }}" class="site-button dark">View All</a>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Latest Processing Order end-->
            <!--Latest Orders supplier start-->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="data-box">
                        <div class="data-box-header with-border">
                          <h2 class="box-title">Latest Delivery Orders Suppliers </h2>
                        </div>
                        <div class="data-box-body">
                          <div class="comn-table latest-order-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Order Id</th>
                                        <!-- <th>Product Name</th> -->
                                        <th>Supplier name</th>
                                        <th>Qty</th>
                                        <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                        <th>Order Date & Time</th>
                                        <th>Payment Status</th>
                                        <th>Action</th>
                                        <!-- <th>Remark</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($data["supplier_latest_order_data"]) && count($data["supplier_latest_order_data"]) > 0)
                                        @foreach($data["supplier_latest_order_data"] as $value)
                                            <tr>
                                            <!-- <span class="nworrd puls">New</span> -->
                                                <td><a href="{{ route('admin_show_supplier_order_details', $value['_id']) }}" class="tc-nwblue"> #{{ $value["_id"] }}</a></td>
                                                <!-- <td>SuperFlow JointAgg</td> -->
                                                <td>{{ isset($value["vendor"]["full_name"]) ? $value["vendor"]["full_name"] : '' }}</td>

                                                <td>{{ $value["quantity"] }} Cu. Mtr</td>
                                                <td>{{ isset($value["total_amount"]) ? number_format($value["total_amount"],2) : 0 }}</td>
                                                <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                                @if($value["payment_status"] == 'PAID')
                                                    <td><div class="badge bg-green">{{ $value["payment_status"] }}</div></td>
                                                @else
                                                    <td><div class="badge bg-red">{{ $value["payment_status"] }}</div></td>
                                                @endif
                                                <td>
                                                    <div class="bil-action-block">
                                                        @if(isset($value["bill_path"]))
                                                        <div class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                            <a href="{{ route('admin_show_supplier_order_details', $value['_id']) }}" class="site-button blue button-sm" data-tooltip="Accept Bill"><i class="fa fa-file-text-o"></i></a>
                                                        </div>
                                                            <!-- <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button green button-sm" data-toggle="dropdown" aria-expanded="true" data-tooltip="Edit Bill"><i class="fa fa-edit"></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <form action="" method="get">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="file-browse">
                                                                            <span class="button-browse">
                                                                                Browse <input type="file">
                                                                            </span>
                                                                            <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button class="site-button blue">Upload</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div> -->



                                                        <div id="gallery" class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                            <a href="{{ $value['bill_path'] }}" class="gal_link site-button blue button-sm" data-tooltip="Preview Bill"><i class="fa fa-eye"></i></a>
                                                        </div>

                                                        <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block">
                                                            <a href="{{ route('admin_show_supplier_order_details', $value['_id']) }}" class="site-button red button-sm" aria-expanded="true" data-tooltip="Reject Bill"><i class="fa fa-ban"></i></a>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <li>
                                                                    <form action="" method="get">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <textarea class="form-control" placeholder="Write a Reason"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <button class="site-button blue">Submit</button>
                                                                    </form>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        @else
                                                            <div>Bill Not Uploaded Yet</div>

                                                        @endif


                                                    </div>
                                                </td>
                                                <!-- <td>
                                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <li>
                                                                <h3>Admin By :</h3>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                </p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td> -->
                                            </tr>
                                        @endforeach
                                    @else
                                        <td colspan="8">No Record Found</td>
                                    @endif

                                </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="data-box-footer clearfix">
                          <div class="pull-right">
                            @if(isset($data["supplier_latest_order_data"]) && count($data["supplier_latest_order_data"]) > 0)
                                <a href="{{ route('admin_show_supplier_orders') }}" class="site-button dark">View All</a>
                            @endif
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Latest Orders end-->

            <div class="clearfix"></div>



            <div class="clearfix"></div>
            <!--Unverified Product start-->
            <div class="row" style="display:none">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="data-box">
                        <div class="data-box-header with-border">
                          <h2 class="box-title">Request For Product Approval (Unverified)</h2>
                        </div>
                        <div class="data-box-body">
                          <div class="comn-table unverified-product-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product Img</th>
                                        <th>Category Name</th>
                                        <th>Seller Name</th>
                                        <th>Pickup Address</th>
                                        <th>Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                        <th>Date & Time</th>
                                        <!-- <th>Created By</th> -->
                                        <th>Status</th>
                                       <!--  <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($data["unverified_product_data"]) && count($data["unverified_product_data"]) > 0)
                                        @foreach($data["unverified_product_data"] as $value)
                                    <tr>
                                        <td>
                                            <div class="product-img">
                                                <img src="{{ $value['productCategory']['image_url'] }}" alt="" />
                                            </div>
                                        </td>
                                        <td><a href="{{ route('admin_show_detail',$value['_id']) }}" class="tc-orange">{{ $value['productCategory']['category_name'] }} - {{ $value['productSubCategory']['sub_category_name'] }}</a></td>
                                        <td>{{ $value["supplier"]["full_name"] }}</td>
                                        <td>
                                            <div class="cstm-tooltip" data-direction="bottom">
                                                @if(isset($value["address"]["line1"]))
                                                    <div class="pickup-address cstm-tooltip__initiator">{{ $value["address"]["line1"] }}</div>
                                                    <div class="cstm-tooltip__item">
                                                        <!-- Techniexe Infolabs LLP 104, Sumel II, Near GuruDwara, SG Highway Ahmedabad 380054, (INDIA) -->
                                                        {{ $value["address"]["line1"] }}, {{ $value["address"]["line2"] }}, {{ $value["address"]["city_name"] }} - {{ $value["address"]["pincode"] }}, {{ $value["address"]["state_name"] }}
                                                    </div>
                                                @endif
                                            </div>
                                        </td>
                                        <td> {{ $value["unit_price"] }}/Cu. Mtr</td>
                                        <td>{{ date('d M Y h:i:s a', strtotime($value["created_at"])) }}</td>
                                        <!-- <td>Admin</td> -->
                                        <td><span class="badge bg-red">Unverified</span></td>
                                       <!--  <td>
                                            <div class="bil-action-block">
                                                <div class="dis-inline-block">
                                                    <a href="javascript:;" class="site-button blue button-sm" data-toggle="modal" data-target="#approve-alert-popup" data-tooltip="Approve"><i class="fa fa-thumbs-o-up"></i></a>
                                                </div>
                                                <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm" data-toggle="dropdown" aria-expanded="true" data-tooltip="Reject"><i class="fa fa-ban"></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <form action="" method="get">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <textarea class="form-control" placeholder="Write a Reason"></textarea>
                                                                    </div>
                                                                </div>
                                                                <button class="site-button blue">Submit</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td> -->
                                    </tr>
                                        @endforeach

                                    @else
                                        <td colspan="7">No Record Found</td>
                                    @endif

                                </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="data-box-footer clearfix">
                          <div class="pull-right">
                            <!-- <a href="#" class="site-button dark">View All</a> -->
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Unverified Product end-->

@endsection