@extends('admin.layouts.admin_login_layout')
@section('content')    

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                @if(Session::get('data') != null)
                    <?php $session_data = Session::get('data'); ?>
                @endif
                <h1 class="login-title">Admin Panel - <span style="color:#687ae8;"> Conmix </span></h1>
                <div id="login" style="display: block;">
                @if(isset($data["error"]))
                    <div class="error alert alert-danger">{{ str_replace("_"," ",$data["error"]["message"]) }}</div>
                @endif
                @if(isset($data["message"]))
                    <div class="alert alert-success">{{ str_replace("_"," ",$data["message"]) }}</div>
                @endif
                @if(isset($session_data["message"]))
                    <div class="alert alert-success">{{ str_replace("_"," ",$session_data["message"]) }}</div>
                @endif
                    <form action="{{url('admin/login')}}" method="post" name="login_form" onsubmit="return validateForm()" autocomplete="off">
                    @csrf
                        <div class="form-group">
                            <div class="input-group">
                                <label>Mobile No. / Email Id <span class="str">*</span></label>
                                <input class="form-control" value="" id="mobile_or_email" name="email_mobile" placeholder="e.g. 9898989898 / johndoe@example.com" type="text">
                                <!-- <p id="email_mobile_error"></p> -->
                                <div class="invalid-feedback" id="email_mobile_error"></div>
                                @error('email_mobile')`
                                    <div class="">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label>Password <span class="str">*</span></label>
                                <input id="pass" class="form-control" name="password" placeholder="e.g. Enter Password" type="password">
                                <div class="invalid-feedback" id="password_error"></div>
                                @error('password')
                                    <div class="">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group pull-left w-100">
                            <div class="input-group display-block">
                                <a href="{{ route('admin_forgot_pass_email_verifier') }}" class="pull-right">Forgot Password</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group text-left">
                                <div class="lsf-btn-block">
                                    <input type="submit" name="login_btn" value="Log In" class="site-button m-b10">
                                    <!-- <a href="dashboard.html" class="site-button m-b10">Login</a> -->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
// function validateForm() {
    
  
//   var email_mobile = document.forms["login_form"]["email_mobile"].value;
  
//   if(isNaN(email_mobile)){
//     //validate email address
//     var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//     if(!email_mobile.match(mailformat)){
//         $("#email_mobile_error").html("Please enter valid email address");
//         return false;
//     }
//   }else{
//     //validate Mobile number
//     // var phoneno = /^\d{10}$/;
//     // if (!email_mobile.match(phoneno)) {
//     //     // alert("Phone number should be 10 digits");
//     //     $("#email_mobile_error").html("Phone number should be 10 digits");
//     //     return false;
//     // }
//   }
  
// } 

function validateForm() {
    
    $(".error").html("");
    $(".error").removeClass("alert-danger");
    // $(".error").toggle();
    $("#email_mobile_error").html("");
    $("#password_error").html("");
  
    var email_mobile = document.forms["login_form"]["email_mobile"].value.trim();
    var pass = document.forms["login_form"]["password"].value.trim();
    
    var is_valid = validateLogin(email_mobile,pass);
  
    return is_valid;
    
  } 

</script>

@endsection