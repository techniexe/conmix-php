@extends('admin.layouts.admin_layout')

@section('content')
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap mini-middle-container-wrap">
    <h1 class="main-title">My Profile</h1>
    <div class="clearfix"></div>
    <!-- my profile start -->
    <div class="my-profile-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="my-profile-content-block">
                    <div class="sub-title-block">
                        <h2 class="sub-title pull-left">Personal Details</h2>
                        @if($profile["rights"]["profile_edit"])
                            <a href="javascript:;" onclick="showEditProfileDialog('{{ json_encode($data['data']) }}')" class="edit-profile-btn site-button outline"><i class="fa fa-edit"></i> Edit</a>
                        @endif
                    </div>
                    <div class="my-profile-details-block">
                        @if(isset($data["data"]))
                            @php
                                $profile_data = $data["data"];
                            @endphp
                            <p><strong>First Name :</strong> {{ isset($profile_data["full_name"]) ? $profile_data["full_name"] : '' }}</p>
                            <!-- <p><strong>Last Name:</strong> Doe</p> -->
                            <p><strong>Email :</strong> {{ isset($profile_data["email"]) ? $profile_data["email"] : '' }}</p>
                            <p><strong>Mobile No.:</strong> {{ isset($profile_data["mobile_number"]) ? $profile_data["mobile_number"] : '' }}</p>
                            <p><strong>Password :</strong> ******** 
                            @if($profile["rights"]["profile_change_password"])
                                <a href="javascript:;" onclick="resetForm('update_password_form','change-password-popup')" data-toggle="modal" data-target="#change-password-popup" class="pull-right site-button-link blue">Change Password</a></p>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- my profile end -->
</div>

@endsection

<!-- <div id="Plant_otpModal" class="alert-message modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body p-tb20 p-lr15" id="otpMessage">
        <label id="plant_OTP_label">Enter the 6 digits OTP sent on your given mobile no., Email id and current registered number <span id="plant_OTP_mob_no"></span><span class="str">*</span></label> 
        <input type="text" id="plant_mobile_otp" class="form-control" placeholder="Enter OTP sent on mobile">
        <p id="plant_mobile_otp_error" style="color:red;"></p>
        
        <input type="text" id="plant_email_otp" class="form-control" placeholder="Enter OTP sent on Email">
        <p id="plant_email_otp_error" style="color:red;"></p>
        
        <input type="text" id="old_mobile_otp" class="form-control" placeholder="Enter OTP sent on your registered mobile number">
        <p id="old_mobile_otp_error" style="color:red;"></p>
        <div class="text-gray-dark resend_plant_otp_div text-left">
            
                <input class="form-control" name="" value="" required  type="hidden">
                
        </div>
      </div>
      <div class="modal-footer text-center">
          <button type="button" class="site-button green m-r15" id="plant_otpOk">Submit</button>
          <button type="button" data-dismiss="modal" class="site-button red" id="plant_otpCancel">Cancel</button>
      </div>
    </div>
  </div>
</div> -->