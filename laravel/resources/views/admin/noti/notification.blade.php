@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<?php 

    $notification_types = array(

        // 1 => "Dear, {{NAME}} Truck has been assigned to your order of id #{{ORDER_ID}}", 
        1 => "Hi Admin, an order has been placed with an order Id #{{ORDER_ID}} by {{CLIENT_COMPANY_NAME}}.", //orderCreate
        //2 => "Order with id #{{ORDER_ID}} has been accepted.", //orderAccept
        2 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has accepted & confirmed the product id #{{ORDER_ITEM}} having Order Id #{{ORDER_ID}} placed by {{CLIENT_COMPANY_NAME}}.", //orderRejected
        //3 => "Dear, {{NAME}} Order with #{{ORDER_ID}} has been confirmed.", //orderCreate
        3 => "Hi Admin, an order request with the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by Client {{CLIENT_COMPANY_NAME}} has been rejected by RMC Supplier {{SUPPLIER_NAME}}.", //truckAssigned
        4 => "Dear, {{NAME}} Your Order with #{{ORDER_ID}} has been picked up.", //orderPickup
        5 => "Hi Admin, Transit mixer has been assigned by RMC Supplier {{SUPPLIER_NAME}} for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by client {{CLIENT_COMPANY_NAME}}. ", //orderPickup
        6 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has picked up the product id #{{ORDER_ITEM}}, for client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}.", //orderReject
        7 => "Hi Admin,  your client {{CLIENT_COMPANY_NAME}} has rejected the product id #{{ORDER_ITEM}} supplied by {{SUPPLIER_NAME}} having an order id #{{ORDER_ID}}", //orderDelay
        8 => "Order with #{{ORDER_ID}} has been delivered partially.", //orderDelay
        9 => "Hi Admin, the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}} has been delayed by 'nos. of hours' owing to {{REASON_FOR_DELAY}}.", //orderDelivered
        10 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has partially delivered  the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}", //orderDelivered
        11 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has fully delivered  the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}", //orderDelivered
        12 => "Hi Admin, RMC Supplier {{SUPPLIER_NAME}} has fully delivered  the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}}", //orderDelivered
        13 => "Hi Admin, your client {{CLIENT_COMPANY_NAME}} has cancelled the product id #{{ORDER_ITEM}}, which was to be supplied by the RMC Supplier {{SUPPLIER_NAME}}, having an order id #{{ORDER_ID}} Client has thus partially cancelled the order.", //orderDelivered
        14 => "Hi Admin, your client {{CLIENT_COMPANY_NAME}} has fully cancelled the order with an order id #{{ORDER_ID}}", //orderDelivered
        15 => "Hi Admin, supply of product id #{{ORDER_ITEM}}, for your client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}} has been shortclosed by RMC Supplier {{SUPPLIER_NAME}} as the balance quantity is less than 3 cum.", //orderDelivered
        16 => "Hi Admin, CP has been assigned by RMC Supplier {{SUPPLIER_NAME}} for the product id #{{ORDER_ITEM}} having an order id #{{ORDER_ID}} placed by client {{CLIENT_COMPANY_NAME}}.", //orderDelivered
        17 => "Hi Admin, CP has been delayed for the product id #{{ORDER_ITEM}}, for the client {{CLIENT_COMPANY_NAME}}, having an order id #{{ORDER_ID}} by 'nos. of hours' owing to {{REASON_FOR_DELAY}}.", //orderDelivered
        18 => "Hi Admin, {{CLIENT_COMPANY_NAME}} has created a support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        19 => "Hi Admin, {{SUPPLIER_NAME}} has created a support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        20 => "Hi Admin, {{CLIENT_COMPANY_NAME}} has replied to your support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        21 => "Hi Admin, {{SUPPLIER_NAME}} has replied to your support ticket with ticket no. #{{TICKET_NO}}", //orderDelivered
        22 => "Hi Admin, {{SUPPLIER_NAME}} has applied for registration and its application is due for verification by Admin. Kindly verify the same within 5 working days.", //orderDelivered

    );

    $notification_types_name = array(

        1 => "Order Placed",
        //2 => "Order Process" ,
        2 => "Order Accept" ,
        3 => "Order Rejected" ,
        4 => "Order Confirm",
        5 => "TM Assigned",
        6 => "Order Pickup",
        7 => "Order Product Reject",
        8 => "Order Reject",
        9 => "Order Delay",
        10 => "Partially Product Delivered",
        11 => "Fully Product Delivered",
        12 => "Order Delivered",
        13 => "Partially Order Cancelled",
        14 => "Order Cancelled",
        15 => "Order Short Close",
        16 => "CP Assigned",
        17 => "CP Delay",
        18 => "Create Support Ticket By Buyer",
        19 => "Create Support Ticket By Vendor",
        20 => "Reply Of Support Ticket By Buyer",
        21 => "Reply Of Support Ticket By Vendor",
        22 => "Vendor Register",

    );

?>
<div class="middle-container-wrap">
    <h1 class="breadcrums">Dashboard / <span>Notifications</span></h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Notifications</h2>
                </div>
                <div class="orders-block">

                    <div class="orders-content-middle-block">
                        <div class="comn-table1 p-a0 table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Notifications</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                                    
                                    @if(isset($data["data"]["notifications"]) && count($data["data"]["notifications"]) > 0)
                                        @foreach($data["data"]["notifications"] as $value)
                                            @if($count == 0)
                                                @php
                                                    $first_created_date = $value["created_at"];
                                                    $count++;
                                                @endphp
                                            @endif
                                            @php
                                                $last_created_date = $value["created_at"];
                                            @endphp
                                        <tr style="<?php echo $value['is_seen'] == 0 ? 'background: #e8f1fb;' : '' ?>">
                                            <?php 
                                                $msg = "";
                                                $is_order = 1;
                                                $msg = $notification_types[$value["notification_type"]];

                                                if($value["notification_type"] == 22){
                                                    $is_order = 2;
                                                }

                                                if(isset($value["to_user"]["full_name"])){
                                                    $msg = str_replace("{{NAME}}",$value["to_user"]["full_name"],$msg);
                                                }
                                                
                                                if(isset($value["order"]["display_id"])){
                                                    $msg = str_replace("{{ORDER_ID}}",$value["order"]['display_id'],$msg);
                                                }

                                                if(isset($value["vendor"]["full_name"])){
                                                    $msg = str_replace("{{SUPPLIER_NAME}}",$value["vendor"]["full_name"],$msg);
                                                }
                                                
                                                if(isset($value["order_item"]["display_item_id"])){
                                                    $msg = str_replace("{{ORDER_ITEM}}",$value["order_item"]["display_item_id"],$msg);
                                                }
                                                
                                                if(isset($value["support_ticket"]["ticket_id"])){
                                                    $is_order = 0;
                                                    $msg = str_replace("{{TICKET_NO}}",$value["support_ticket"]["ticket_id"],$msg);
                                                }
                                                
                                                if(isset($value["client"])){

                                                    // $account_type = $value["client"]["account_type"];

                                                    // $client_name = "";

                                                    // if($account_type == "Individual"){

                                                        $client_name = $value["client"]["full_name"];

                                                    // }else{

                                                    //     $client_name = $value["client"]["company_name"];

                                                    // }

                                                    $msg = str_replace("{{CLIENT_COMPANY_NAME}}",$client_name,$msg);
                                                    
                                                }

                                                // $created = date('d M Y', strtotime($value["created_at"]));

                                                $created = AdminController::dateTimeFormat($value["created_at"]);
                                                
                                            ?>

                                            <td>{{ isset($notification_types_name[$value["notification_type"]]) ? $notification_types_name[$value["notification_type"]] : "No Type Defined" }}</td>
                                            
                                            <td>
                                                <a href="{{ ($is_order == 1) ? route('admin_show_order_details',$value['order']['_id']) : ($is_order == 1 ? route('admin_show_support_ticket_details',$value['support_ticket']['ticket_id']) : '') }}" onclick="updateNotiStatus('{{ $value['_id'] }}')" class="text-dark">
                                                {{$msg}}
                                                </a>
                                            </td>
                                            <td class="text-nowrap">{{$created}} </td>
                                        </tr>
                                        @endforeach
                                    @else
                                    

                                        <tr>
                                            <td colspan="3" style="text-align: center;">No Record Found</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]["notifications"]) && count($data['data']["notifications"]) > 0)

                        @if(isset($data["data"]["notifications"]) && count($data['data']["notifications"]) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="pagination-block m-t20">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                    <div class="pagination justify-content-end m-0">
                    @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">

                                @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif

                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button m-r10">Previous</button>
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">


                                @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif

                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>

@endsection