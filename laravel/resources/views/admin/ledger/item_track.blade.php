@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums">Ledger / <a href="order-details.html">Order Details</a> / <span>Track</span></h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Track Details</h2>
                </div>
                
                <div class="track-block">
                    <div class="truck-hed-block">
                        <div class="truck-dtls">
                            <div class="truck-dtls-lst">
                                <span><b>Tipper R.C. Number:</b></span> <span>GJ/01/FV/1234</span>
                            </div>
                            <div class="truck-dtls-lst">
                                <span><b>Tipper Type:</b></span> <span>10 Tyre: 18 MT to 21 MT</span>
                            </div>
                        </div>
                        <div class="truck-logstic-info-block">
                            <a href="javascript:;" class="site-button gray button-sm" data-toggle="modal" data-target="#changes-lgstc-popup">Contact Logistics</a>
                        </div>
                    </div>

                    <div class="track-product-block">
                        <div class="track-product-img-block">
                            <img src="images/cart-pic-1.jpg" alt="" />
                        </div>
                        <div class="track-product-details-block">
                            <h2>10 mm Aggregate <span>- Sevalia</span></h2>
                            <p>Order Id: <span>AG/1009004205982</span></p>
                            <p>Order On: <span>15 Jul 2019</span></p>
                            <span class="badge bg-green green">Shipped</span>

                        </div>
                    </div>
                    
                    <div class="track-process-step1">
                        <ol class="progress progress--medium">
                            <li class="is-complete" data-step="1">In Process</li>
                            <li class="is-complete" data-step="2">Pickup</li>
                            <li class="is-active" data-step="3">In Transit</li>
                            <li data-step="4" class="progress__last">Delivered</li>
                        </ol>
                    </div>
                    
                    <div class="track-details-block">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date/Time</th>
                                    <th>Process</th>
                                    <th>Location</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        15 Jul 2019 10:10 PM
                                    </td>
                                    <td>
                                        Your Order Has Been Processed
                                    </td>
                                    <td>
                                        Ahmedabad
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        15 Jul 2019 10:10 PM
                                    </td>
                                    <td>
                                        Your Order Has Been Processed
                                    </td>
                                    <td>
                                        Ahmedabad
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        15 Jul 2019 10:10 PM
                                    </td>
                                    <td>
                                        Your Order Has Been Processed
                                    </td>
                                    <td>
                                        Ahmedabad
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        15 Jul 2019 10:10 PM
                                    </td>
                                    <td>
                                        Your Order Has Been Processed
                                    </td>
                                    <td>
                                        Ahmedabad
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>

@endsection