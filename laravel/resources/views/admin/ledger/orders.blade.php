@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums">Ledger / <span>Buyer Orders</span></h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Buyer Orders</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_orders') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                </div>
                <div class="orders-block">

                    <div class="orders-content-middle-block">
                        <div class="comn-table1 p-a0 table-responsive">
                            <table class="table">
                            <thead>
                                <tr>
                                    <th>Order Id</th>
                                    <th>Buyer Name</th>

                                    <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                    <!-- <th>Payment Mode</th> -->
                                    <th>Transaction ID</th>
                                    <th>Payment Gateway Fee ( <i class="fa fa-rupee"></i> )</th>
                                    <th>Payment Gateway Tax ( <i class="fa fa-rupee"></i> )</th>
                                    <th>Order Date & Time</th>
                                    <th>Order Status</th>
                                    <th>Payment Status</th>
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            @php

                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp

                            @if(isset($data["data"]) && count($data['data']) > 0)

                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php
                                            $first_created_date = $value["created_at"];
                                            $count++;
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp

                                        <tr>
                                            <td><a href="{{ url('admin/orders/details/'.$value['_id']) }}">#{{ isset($value["display_id"]) ? $value["display_id"] : '' }}</a></td>
                                            <td>{{ isset($value["buyer"]["full_name"]) ? $value["buyer"]["full_name"] : '' }}</td>

                                            <td><!-- <i class="fa fa-rupee"></i> --> {{ isset($value["total_amount"]) ? number_format($value["total_amount"],2) : '' }}</td>
                                            <!-- <td>{{ isset($value["payment_mode"]) ? $value["payment_mode"] : '-' }}</td> -->
                                            <td>{{ isset($value["gateway_transaction_id"]) ? $value["gateway_transaction_id"] : '' }}</td>
                                            <td> {{ isset($value["fee"]) ? number_format($value["fee"],2) : '' }}</td>
                                            <td> {{ isset($value["tax"]) ? number_format($value["tax"],2) : '' }}</td>
                                            <td>{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</td>

                                            <!-- <td>
                                                <div class="badge bg-yellow">
                                                    {{ isset($value["order_status"]) ? $value["order_status"] : '' }}
                                                </div>
                                            </td> -->

                                            @if(isset($value["order_status"]))
                                                @if($value["order_status"] == 'DELIVERED' || $value["order_status"] == 'ACCEPTED')
                                                    <td><div class="badge bg-green">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'CANCELLED' || $value["order_status"] == 'REJECTED' || $value["order_status"] == 'LAPSED')
                                                    <td><div class="badge bg-red">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'RECEIVED')
                                                    <td><div class="badge bg-blue">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'PICKUP')
                                                    <td><div class="badge bg-purple">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>

                                                    @else
                                                        <td><div class="badge bg-yellow">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                    @endif
                                                @endif
                                                @endif
                                                @endif
                                            @else
                                                <td>-</td>
                                            @endif

                                            @if(isset($value["payment_status"]))
                                                @if($value["payment_status"] == 'PAID')
                                                    <td><div class="badge bg-green">{{ isset($value["payment_status"]) ? $value["payment_status"] : '' }}</div></td>
                                                @else
                                                    <td><div class="badge bg-red">{{ isset($value["payment_status"]) ? $value["payment_status"] : '' }}</div></td>
                                                @endif
                                            @endif
                                            <!-- <td>
                                                <div class="dropdown cstm-dropdown-select">
                                                    <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('order_status','{{ json_encode($value) }}')">Change Delivery Status</a></li>
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('payment_status','{{ json_encode($value) }}')">Change Payment Status</a></li>
                                                        <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('cancel_status','{{ json_encode($value) }}')">Cancel</a></li>
                                                    </ul>
                                                </div>
                                            </td> -->
                                        </tr>
                                @endforeach
                            @else

                                  <td colspan="11" style="text-align: center;">No Record Found</td>

                            @endif

                            </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="pagination-block m-t20">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                    <div class="pagination justify-content-end m-0">
                    @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">

                                @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                @if(Request::get('buyer_id'))
                                    <input type="hidden" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : '' }}" name="buyer_id"/>
                                @endif
                                @if(Request::get('buyer_name'))
                                    <input type="hidden" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : '' }}" name="buyer_name"/>
                                @endif
                                @if(Request::get('order_year_filter'))
                                    <input type="hidden" value="{{ Request::get('order_year_filter') ? Request::get('order_year_filter') : '' }}" name="order_year_filter"/>
                                @endif
                                @if(Request::get('order_month_filter'))
                                    <input type="hidden" value="{{ Request::get('order_month_filter') ? Request::get('order_month_filter') : '' }}" name="order_month_filter"/>
                                @endif
                                @if(Request::get('order_date_filter'))
                                    <input type="hidden" value="{{ Request::get('order_date_filter') ? Request::get('order_date_filter') : '' }}" name="order_date_filter"/>
                                @endif

                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button m-r10">Previous</button>
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">


                            @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                @if(Request::get('buyer_id'))
                                    <input type="hidden" value="{{ Request::get('buyer_id') ? Request::get('buyer_id') : '' }}" name="buyer_id"/>
                                @endif
                                @if(Request::get('buyer_name'))
                                    <input type="hidden" value="{{ Request::get('buyer_name') ? Request::get('buyer_name') : '' }}" name="buyer_name"/>
                                @endif
                                @if(Request::get('order_year_filter'))
                                    <input type="hidden" value="{{ Request::get('order_year_filter') ? Request::get('order_year_filter') : '' }}" name="order_year_filter"/>
                                @endif
                                @if(Request::get('order_month_filter'))
                                    <input type="hidden" value="{{ Request::get('order_month_filter') ? Request::get('order_month_filter') : '' }}" name="order_month_filter"/>
                                @endif
                                @if(Request::get('order_date_filter'))
                                    <input type="hidden" value="{{ Request::get('order_date_filter') ? Request::get('order_date_filter') : '' }}" name="order_date_filter"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>

@endsection