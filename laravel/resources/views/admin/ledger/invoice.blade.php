@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums">Ledger / <a href="#">Order Details</a> / <span>Invoice</span></h1>
    <div class="clearfix"></div>
    <!--invoice start -->
    <div class="invoice-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="invoice-content-block">
                        @php
                            $item_data = array();
                        @endphp
                     @if(isset($data["data"]))
                        @php
                            $invoice_data = $data["data"][0];
                            //dd($invoice_data);
                            $item_data = isset($invoice_data["items"]) ? $invoice_data["items"] : array();
                        @endphp

                        <div class="invoice-head-block">
                            <div class="inv-logo-block">
                            <img src="images/invoice-logo.jpg" alt="" />
                            </div>
                            <div class="inv-nodate-block">
                            <p>Invoice: <span class="text-primary">#{{ isset($invoice_data["_id"]) ? $invoice_data["_id"] : '' }}</span></p>
                            <p>Date: <span>{{ isset($invoice_data["created_at"]) ? date('d M Y h:i:s a',strtotime($invoice_data["created_at"])) : '' }}</span></p>
                            </div>
                        </div>

                        <div class="invoice-middle-top-block">
                            <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="inv-info-block">
                                <h6>From</h6>
                                <h5>Admin, Inc.</h5>
                                <p>
                                    102, Sumel II, Near Gurudwara, <br />
                                    SG Highway Ahmedabad 380054, <br />
                                    Phone: 079 - 1234 5678 <br />
                                    Email: info@test.com
                                </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="inv-info-block">
                                <h6>To</h6>
                                <h5>{{ isset($invoice_data['buyer']["full_name"]) ? $invoice_data['buyer']["full_name"] : '' }}</h5>
                                <p>
                                {{ isset($invoice_data["address_line1"]) ? $invoice_data["address_line1"] : '' }}, <br />
                                {{ isset($invoice_data["address_line2"]) ? $invoice_data["address_line2"] : '' }}, <br />
                                {{ isset($invoice_data["city_name"]) ? $invoice_data["city_name"] : '' }}, <br />
                                {{ isset($invoice_data["state_name"]) ? $invoice_data["state_name"] : '' }}, <br />
                                {{ isset($invoice_data["pincode"]) ? $invoice_data["pincode"] : '' }}, <br />
                                    Phone:{{ isset($invoice_data['buyer']["mobile_number"]) ? $invoice_data['buyer']["mobile_number"] : '' }} <br />
                                    Email: {{ isset($invoice_data['buyer']["email"]) ? $invoice_data['buyer']["email"] : '' }}
                                </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="inv-info-block">
                                <p>
                                    <span class="vTitle">Order ID: </span>
                                    <span class="vName">#{{ isset($invoice_data["order_id"]) ? $invoice_data["order_id"] : '' }}</span>
                                </p>
                                <p>
                                    <span class="vTitle">GST No: </span>
                                    <span class="vName">{{ isset($invoice_data['buyer']["gst_number"]) ? $invoice_data['buyer']["gst_number"] : '' }}</span>
                                </p>
                                <!-- <p>
                                    <span class="vTitle">Pan No: </span>
                                    <span class="vName">{{ isset($invoice_data["_id"]) ? $invoice_data["_id"] : '' }}</span>
                                </p> -->
                                <p>
                                    <span class="vTitle">Order Date: </span>
                                    <span class="vName">{{ isset($invoice_data["created_at"]) ? date('d M Y h:i:s a',strtotime($invoice_data["created_at"])) : '' }}</span>
                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                    @endif

                    <div class="invoice-bottom-block">
                        <table class="table">
                        <thead>
                            <tr>
                            <th>Quote ID</th>
                            <th>Product Name</th>
                            <!-- <th>Description</th> -->
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $subtotal = 0;
                                $gst_amount = 0;
                                $final_amount = 0;
                            @endphp
                            
                            @if(isset($item_data))
                                @foreach($item_data as $value)
                                    <tr>
                                        <td><span class="text-primary">{{ isset($value["_id"]) ? $value["_id"] : ''  }}</span></td>
                                        <td>{{ isset($value["category_name"]) ? $value["category_name"] : '' }}</td>
                                        <!-- <td>
                                            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                        </td> -->
                                        <td>{{ isset($value["quantity"]) ? $value["quantity"] : '' }} {{ isset($value["quantity_unit"]) ? $value["quantity_unit"] : '' }}</td>
                                        <td><i class="fa fa-rupee"></i> {{ isset($value["unit_price"]) ? round($value["unit_price"]) : '' }}</td>
                                        <td><i class="fa fa-rupee"></i> {{ isset($value["total_price"]) ? round($value["total_price"]) : '' }}</td>
                                    </tr>
                                    @php
                                        $subtotal = $subtotal + $value["total_price"];
                                        $gst_amount = $value["cgst_amount"];
                                    @endphp

                                @endforeach
                            @endif
                            
                        </tbody>
                        <tfoot>
                            <tr>
                            <td colspan="3">
                                <div class="sign-block">
                                <img src="images/signature-img.jpg" alt="" />
                                <h5>Authorised Signatory</h5>
                                </div>
                            </td>
                            <td colspan="3">
                                <div class="inv-sub-total-block">
                                <p>
                                    <span class="title">Sub Total:</span>
                                    <span class="price"><i class="fa fa-rupee"></i> {{ round($subtotal) }}</span>
                                </p>
                                <p>
                                    <span class="title">GST:</span>
                                    <span class="price"><i class="fa fa-rupee"></i> {{ round($gst_amount) }}</span>
                                </p>
                                <hr />
                                @php $final_amount = $subtotal + $gst_amount; @endphp
                                <p>
                                    <span class="title">Total:</span>
                                    <span class="price"><i class="fa fa-rupee"></i> {{ round($final_amount) }}</span>
                                </p>
                                </div>
                                <div class="inv-btn-block d-print-none">
                                <a href="javascript:window.print()" class="site-button m-r5"><i class="fa fa-file-pdf-o"></i> Generate
                                    PDF</a>
                                <a href="javascript:window.print()" class="site-button gray"><i class="fa fa-print"></i>
                                    Print</a>
                                </div>
                            </td>
                            </tr>
                        </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!--invoice end -->
</div>

@endsection