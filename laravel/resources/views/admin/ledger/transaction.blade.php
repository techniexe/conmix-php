@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums">Ledger / <span>Transaction</span></h1>
    <div class="clearfix"></div>
    <!--Region start -->
    <div class="ledger-block transaction-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Transaction</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                </div>
                <div class="comn-table1 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Gateway Transaction Id</th>
                                <th>Transaction Id</th>
                                <th>User Name</th>
                                <th>Order Id</th>
                                <th>Payment Gateway</th>
                                <th>Status</th>
                                <th>Order Date & Time</th>
                                <th>Transaction Date</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                    
                            @if(isset($data["data"]) && count($data['data']) > 0)
                                @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                                @foreach( $data['data'] as $value)  
                                     @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                    <tr>
                                        <td>{{ isset($value['gateway_transaction_id']) ? $value['gateway_transaction_id'] : ''  }}</td>
                                        <td>{{ isset($value['_id']) ? $value['_id'] : ''  }}</td>
                                        <td>{{ isset($value['buyer']['full_name']) ? $value['buyer']['full_name'] : ''  }}</td>
                                        <td><span class="text-primary"><a href="{{ url('admin/orders/details/'.$value['order_id']) }}"> {{ isset($value['order_id']) ? $value['order_id'] : ''  }} </a></span></td>
                                        <td>{{ isset($value['payment_gateway']) ? $value['payment_gateway'] : ''  }}</td>
                                        <td><span class="badge bg-green">{{ isset($value['transaction_status']) ? $value['transaction_status'] : ''  }}</span></td>
                                        <td>{{ isset($value['created_at']) ? date('d M Y h:i:s a',strtotime($value['created_at'])) : ''  }}</td>
                                        <td>{{ isset($value['created_at']) ? date('d M Y h:i:s a',strtotime($value['created_at'])) : ''  }}</td>
                                        <!-- <td>
                                            <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                    <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#paid-alert-popup">Mark As Paid</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#unpaid-alert-popup">Mark As Unpaid</a></li>
                                                </ul>
                                            </div>
                                        </td> -->
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">No Record Found</td>
                                </tr>
                            @endif
                            
                        </tbody>
                    </table>
                </div>
                
                <div class="pagination-block m-t20">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                    @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)
                    <div class="pagination justify-content-end m-0">
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('user_id'))
                                    <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('order_id'))
                                    <input type="hidden" value="{{ Request::get('order_id') ? Request::get('order_id') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('gateway_transaction_id'))
                                    <input type="hidden" value="{{ Request::get('gateway_transaction_id') ? Request::get('gateway_transaction_id') : '' }}" name="gateway_transaction_id"/>
                                @endif 
                                @if(Request::get('transaction_status'))
                                    <input type="hidden" value="{{ Request::get('transaction_status') ? Request::get('transaction_status') : '' }}" name="transaction_status"/>
                                @endif 
                                @if(Request::get('payment_gateway'))
                                    <input type="hidden" value="{{ Request::get('payment_gateway') ? Request::get('payment_gateway') : '' }}" name="payment_gateway"/>
                                @endif
                                
                                <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                
                                @if(Request::get('user_id'))
                                    <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('order_id'))
                                    <input type="hidden" value="{{ Request::get('order_id') ? Request::get('order_id') : '' }}" name="categoryId"/>
                                @endif
                               
                                @if(Request::get('gateway_transaction_id'))
                                    <input type="hidden" value="{{ Request::get('gateway_transaction_id') ? Request::get('gateway_transaction_id') : '' }}" name="gateway_transaction_id"/>
                                @endif
                                @if(Request::get('transaction_status'))
                                    <input type="hidden" value="{{ Request::get('transaction_status') ? Request::get('transaction_status') : '' }}" name="transaction_status"/>
                                @endif 
                                @if(Request::get('payment_gateway'))
                                    <input type="hidden" value="{{ Request::get('payment_gateway') ? Request::get('payment_gateway') : '' }}" name="payment_gateway"/>
                                @endif 
                                <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                        </div>
                        @endif
                </div>
            </div>
        </div>
    </div>
    <!--Region end -->
</div>

@endsection