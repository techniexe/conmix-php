@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Cancel Orders</h1>
    <div class="clearfix"></div>
    <!--orders list start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Cancel Orders List</h2>
                    <!--  <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                </div>
                <div class="orders-block">
                    
                    <div class="orders-content-middle-block">
                        <div class="comn-table1 p-a0">
                            <table class="table">
                            <thead>
                                <tr>
                                    <th>Order Id</th>
                                    <th>Buyer Name</th>
                                    <th>Created On</th>
                                    <th>Total Price(Rs.)</th>
                                    <th>Delivery Status</th>
                                    <th>Payment Status</th>
                                    <th>Remark</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="order-details.html">#AG123456987</a></td>
                                    <td>M M Patel Hardware</td>
                                    <td>15 Jul 2019</td>
                                    <td><i class="fa fa-rupee"></i> 1,05,000</td>
                                    <td><div class="badge bg-red">Cancel</div></td>
                                    <td><div class="badge bg-red">Unpaid</div></td>
                                        <td>
                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                            <li>
                                                <h3>Admin By :</h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><a href="order-details.html">#AG123456988</a></td>
                                    <td>Ambica Hardware & Sanitary</td>
                                    <td>14 Jul 2019</td>
                                    <td><i class="fa fa-rupee"></i> 95,000</td>
                                    <td><div class="badge bg-red">Cancel</div></td>
                                    <td><div class="badge bg-green">Paid</div></td>
                                    <td>
                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                            <li>
                                                <h3>Admin By :</h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><a href="order-details.html">#AG123456989</a></td>
                                    <td>Aaron Hank</td>
                                    <td>13 Jul 2019</td>
                                    <td><i class="fa fa-rupee"></i> 50,000</td>
                                    <td><div class="badge bg-red">Cancel</div></td>
                                    <td><div class="badge bg-green">Paid</div></td>
                                        <td>
                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                            <li>
                                                <h3>Admin By :</h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><a href="order-details.html">#AG123456990</a></td>
                                    <td>Sharma Sirgun</td>
                                    <td>12 Jul 2019</td>
                                    <td><i class="fa fa-rupee"></i> 98,000</td>
                                    <td><div class="badge bg-red">Cancel</div></td>
                                    <td><div class="badge bg-green">Paid</div></td>
                                    <td>
                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                            <li>
                                                <h3>Admin By :</h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td><a href="order-details.html">#AG123456990</a></td>
                                    <td>Shridhar Pandey</td>
                                    <td>11 Jul 2019</td>
                                    <td><i class="fa fa-rupee"></i> 92,000</td>
                                    <td><div class="badge bg-red">Cancel</div></td>
                                    <td><div class="badge bg-green">Paid</div></td>
                                    <td>
                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                        <a href="javascript:;" class="site-button bg-grey button-sm" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i> Info</a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                            <li>
                                                <h3>Admin By :</h3>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    
                </div>
                
                <div class="pagination-block m-t20">
                    <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p>
                </div>
            </div>
        </div>
    </div>
    <!--orders list end -->
</div>

@endsection