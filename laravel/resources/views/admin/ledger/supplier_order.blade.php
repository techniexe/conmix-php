@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums">Ledger / <span>Suppliers Orders</span></h1>
    <!-- <h1 class="main-title">Suppliers Orders</h1> -->
    <div class="clearfix"></div>
    <!--orders list start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Suppliers Orders List</h2>
                     <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                     <a href="{{ route('admin_show_supplier_orders') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                </div>
                <div class="orders-block">

                    <div class="orders-content-middle-block">
                        <div class="comn-table1 p-a0">
                            <table class="table">
                            <thead>
                                <tr>
                                <th>Order Id</th>
                                <th>Company Name</th>
                                <th>Qty (Cu. Mtr)</th>
                                <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                <th>Margin Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>Base Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>Order Date & Time</th>
                                <th>Status</th>
                                <!-- <th>Bill Status</th> -->
                                <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                    //dd($data['data']);
                                        $first_created_date = '';
                                        $last_created_date = '';
                                        $count = 0;
                                    @endphp
                                @if(isset($data["data"]) && count($data['data']) > 0)

                                    @foreach($data["data"] as $value)
                                        @if($count == 0)
                                            @php
                                                $first_created_date = $value["created_at"];
                                                $count++;
                                            @endphp
                                        @endif
                                        @php
                                            $last_created_date = $value["created_at"];
                                        @endphp


                                        <tr>
                                            <td><a href="{{ route('admin_show_supplier_order_details', $value['_id']) }}">#{{ $value["_id"] }}</a></td>

                                            <td>{{ $value["vendor"]["company_name"] }}</td>
                                            <td>{{ $value["quantity"] }}</td>
                                            <td>{{ isset($value["unit_price"]) ? number_format($value["unit_price"],2) : 0 }}</td>
                                            <td>{{ isset($value["margin_price"]) ? number_format($value["margin_price"],2) : 0 }}</td>
                                            <td>{{ isset($value["selling_price"]) ? number_format($value["selling_price"],2) : 0 }}</td>
                                            <td>{{ isset($value["total_amount_without_margin"]) ? number_format($value["total_amount_without_margin"],2) : 0 }}</td>
                                            <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                            <!-- <td><div class="badge {{ $value['payment_status'] == 'UNPAID' ? 'bg-red' : 'bg-green' }}">{{ $value["payment_status"] }}</div></td> -->
                                           @if(isset($value["order_status"]))
                                                @if($value["order_status"] == 'DELIVERED' || $value["order_status"] == 'ACCEPTED')
                                                    <td><div class="badge bg-green">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'CANCELLED' || $value["order_status"] == 'REJECTED' || $value["order_status"] == 'LAPSED')
                                                    <td><div class="badge bg-red">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'RECEIVED')
                                                    <td><div class="badge bg-blue">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                @else @if($value["order_status"] == 'PICKUP')
                                                    <td><div class="badge bg-purple">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>

                                                    @else
                                                        <td><div class="badge bg-yellow">{{ str_replace("_"," ",$value["order_status"]) }}</div></td>
                                                    @endif
                                                @endif
                                                @endif
                                                @endif
                                            @else
                                                <td>-</td>
                                            @endif
                                            <!-- <td>
                                                @if(isset($value['bill_status']))
                                                    <div class="badge {{ $value['bill_status'] == 'REJECTED' ? 'bg-red' : 'bg-green' }}">{{ $value["bill_status"] }}</div>
                                                @else
                                                    -
                                                @endif
                                            </td> -->
                                            <!-- <td>
                                                <div class="bil-action-block">
                                                    @if(isset($value["bill_path"]))
                                                        <div class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                            <a href="javascript:void(0)" onclick="updateBillStatus('1','{{ $value['_id']}}','{{ csrf_token() }}','')" class="site-button blue button-sm" data-tooltip="Accept Bill"><i class="fa fa-file-text-o"></i></a>
                                                        </div>

                                                        <div id="gallery" class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                            <a href="{{ $value['bill_path'] }}" class="gal_link site-button blue button-sm" data-tooltip="Preview Bill"><i class="fa fa-eye"></i></a>
                                                        </div>

                                                        <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block" >
                                                            <div id="reject_dropdown_{{ $value['_id'] }}">
                                                            <a href="javascript:void(0)" class="site-button red button-sm" aria-expanded="false" data-tooltip="Reject Bill"><i class="fa fa-ban"></i></a>
                                                            </div>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <li>
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <textarea class="form-control" id="bill_reject_reason_{{ $value['_id'] }}" placeholder="Write a Reason"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <button data-id="{{ $value['_id'] }}" onclick="updateBillStatus('2','{{ $value['_id']}}','{{ csrf_token() }}','bill_reject_reason_{{ $value['_id'] }}')" class="site-button blue">Submit</button>

                                                                </li>
                                                            </ul>

                                                            <script>

                                                                $('#reject_dropdown_{{ $value['_id'] }}').on('click',function(){
                                                                    console.log("drop click");
                                                                    if($(this).parent().hasClass('open')){
                                                                        $(this).parent().removeClass('open');
                                                                    }else{
                                                                        $(this).parent().addClass('open');
                                                                    }


                                                                });

                                                            </script>

                                                        </div>
                                                    @else
                                                        <span>Biil Not Uploaded Yet.</span>

                                                    @endif
                                                </div>
                                            </td> -->

                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11" style="text-align: center;">No Record Found</td>
                                    </tr>

                                @endif


                            </tbody>
                        </table>
                        </div>
                    </div>

                </div>

                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="pagination-block m-t20">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p>
                     -->

                    <div class="pagination justify-content-end m-0">
                    @if($is_previous_avail == 1)
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">

                                @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                @if(Request::get('order_id'))
                                    <input type="hidden" value="{{ Request::get('order_id') ? Request::get('order_id') : '' }}" name="order_id"/>
                                @endif
                                @if(Request::get('company_name'))
                                    <input type="hidden" value="{{ Request::get('company_name') ? Request::get('company_name') : '' }}" name="company_name"/>
                                @endif
                                @if(Request::get('order_year_filter'))
                                    <input type="hidden" value="{{ Request::get('order_year_filter') ? Request::get('order_year_filter') : '' }}" name="order_year_filter"/>
                                @endif
                                @if(Request::get('order_month_filter'))
                                    <input type="hidden" value="{{ Request::get('order_month_filter') ? Request::get('order_month_filter') : '' }}" name="order_month_filter"/>
                                @endif
                                @if(Request::get('order_date_filter'))
                                    <input type="hidden" value="{{ Request::get('order_date_filter') ? Request::get('order_date_filter') : '' }}" name="order_date_filter"/>
                                @endif

                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>
                            </form>
                            @endif
                            @endif
                            @if($is_next_avail == 1)
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">


                                @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                @if(Request::get('order_id'))
                                    <input type="hidden" value="{{ Request::get('order_id') ? Request::get('order_id') : '' }}" name="order_id"/>
                                @endif
                                @if(Request::get('company_name'))
                                    <input type="hidden" value="{{ Request::get('company_name') ? Request::get('company_name') : '' }}" name="company_name"/>
                                @endif
                                @if(Request::get('order_year_filter'))
                                    <input type="hidden" value="{{ Request::get('order_year_filter') ? Request::get('order_year_filter') : '' }}" name="order_year_filter"/>
                                @endif
                                @if(Request::get('order_month_filter'))
                                    <input type="hidden" value="{{ Request::get('order_month_filter') ? Request::get('order_month_filter') : '' }}" name="order_month_filter"/>
                                @endif
                                @if(Request::get('order_date_filter'))
                                    <input type="hidden" value="{{ Request::get('order_date_filter') ? Request::get('order_date_filter') : '' }}" name="order_date_filter"/>
                                @endif
                                <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                            @endif
                        </div>

                </div>
            </div>
        </div>
    </div>
    <!--orders list end -->
</div>

@endsection