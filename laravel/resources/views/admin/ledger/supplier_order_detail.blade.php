@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap">
    <h1 class="breadcrums">Ledger / <a href="{{ route('admin_show_supplier_orders') }}">Suppliers Orders</a> / <span>Order Details</span></h1>
    <div class="clearfix"></div>
    <!--order Details start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Order Details</h2>
                </div>

                <div class="odrs-details-block p-a0">
                <?php //dd($data['data']); ?>
                    @if(isset($data['data']))
                        <?php $order_item_data = $data["data"]; ?>
                        <?php $data = $data["data"]; ?>
                        <?php //$order_data = $data["order"]; ?>
                        <?php $buyer_data = $data["buyer"]; ?>
                        <?php //$product_data = $data["product"]; ?>

                        <?php $orderTrackDetails = $data["orderTrackData"]; ?>
                        <?php $orderItemPartData = $data["orderItemPartData"]; ?>
                    <div class="orders-content-block">
                        <div class="orders-content-head-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-id-date-block">
                                        <div class="ordlst-sd-text-block spa">
                                            Order Id : <span>#{{ $data["_id"] }}</span>
                                        </div>
                                        <!-- <div class="ordlst-id-block">
                                            Invoice Id: <a href="#">10090042</a>
                                        </div> -->
                                        <div class="ordlst-date-block">
                                            Order Date & Time : <span>{{ AdminController::dateTimeFormat($data["created_at"]) }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Buyer Name : <span>{{ $buyer_data["full_name"] }}</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-ship-dlvry-date-block">
                                        <div class="ordlst-sd-text-block">
                                            Payment Status : <span class="badge {{ $data['payment_status'] == 'PAID' ? 'bg-green' : 'bg-red' }} ">{{ $data["payment_status"] }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Total Amount : <span class="rupee-text"><i class="fa fa-rupee"></i> {{ isset($data["total_amount_without_margin"]) ? number_format($data["total_amount_without_margin"],2) : 0 }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Pickup Location :
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                @if(isset($data["pickup_address"]))
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['pickup_address']['location']['coordinates'][1]) ? $data['pickup_address']['location']['coordinates'][1] : '' }}','{{ isset($data['pickup_address']['location']['coordinates'][0]) ? $data['pickup_address']['location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                {{ isset($data["pickup_address"]['line1']) ? $data["pickup_address"]['line1'] : '' }},
                                                    {{ isset($data["pickup_address"]['line2']) ? $data["pickup_address"]['line2'] : '' }},
                                                    {{ isset($data["pickup_address"]['state_name']) ? $data["pickup_address"]['state_name'] : '' }},
                                                    {{ isset($data["pickup_address"]['city_name']) ? $data["pickup_address"]['city_name'] : '' }} - {{ isset($data["pickup_address"]['pincode']) ? $data["pickup_address"]['pincode'] : '' }},
                                                @endif

                                                @if(isset($data['line1']))
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['latitude']) ? $data['latitude'] : '' }}','{{ isset($data['longitude']) ? $data['longitude'] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                    {{ isset($data['line1']) ? $data['line1'] : '' }},
                                                    {{ isset($data['line2']) ? $data['line2'] : '' }},
                                                    {{ isset($data['state_name']) ? $data['state_name'] : '' }},
                                                    {{ isset($data['city_name']) ? $data['city_name'] : '' }} - {{ isset($data['pincode']) ? $data['pincode'] : '' }},
                                                @endif
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Delivery Location :
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                @if(isset($data["delivery_address"]))
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['delivery_address']['location']['coordinates'][1]) ? $data['delivery_address']['location']['coordinates'][1] : '' }}','{{ isset($data['delivery_address']['location']['coordinates'][0]) ? $data['delivery_address']['location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                {{ isset($data["delivery_address"]['address_line1']) ? $data["delivery_address"]['address_line1'] : '' }},
                                                    {{ isset($data["delivery_address"]['address_line2']) ? $data["delivery_address"]['address_line2'] : '' }},
                                                    {{ isset($data["delivery_address"]['state_name']) ? $data["delivery_address"]['state_name'] : '' }},
                                                    {{ isset($data["delivery_address"]['city_name']) ? $data["delivery_address"]['city_name'] : '' }} - {{ isset($data["delivery_address"]['pincode']) ? $data["delivery_address"]['pincode'] : '' }},
                                                @endif

                                                @if(isset($data["order"]))
                                                    <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['order']['latitude']) ? $data['order']['latitude'] : '' }}','{{ isset($data['order']['longitude']) ? $data['order']['longitude'] : '' }}')">View Map</a>
                                                    <span class="cstm-tooltip__item">
                                                    {{ isset($data["order"]['address_line1']) ? $data["order"]['address_line1'] : '' }},
                                                    {{ isset($data["order"]['address_line2']) ? $data["order"]['address_line2'] : '' }},
                                                    {{ isset($data["order"]['state_name']) ? $data["order"]['state_name'] : '' }},
                                                    {{ isset($data["order"]['city_name']) ? $data["order"]['city_name'] : '' }} - {{ isset($data["order"]['pincode']) ? $data["order"]['pincode'] : '' }},
                                                @endif
                                                </span>
                                            </span>
                                        </div>



                                    </div>
                                    <!--     <div class="odr-select-optin-block">                                  <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="dropdown-label">Select Invoice <i class="fa fa-sort-down"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li><a class="dropdown-item" href="invoice.html">View invoice</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Download invoice</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#sendemil-sts-popup">Send invoice to email</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Download purchase order</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Send purchase order to email</a></li>
                                                </ul>
                                            </div>
                                        </div> -->
                                        @if(isset($data["bill_path"]))
                                            <!-- <div class="dropdown cstm-dropdown-select bid-rate-dropdown" style="display: block;display: block;position: absolute; right: 10px; top: 0;">

                                                <a href="javascript:;" onclick="updateBillStatus('1','{{ $data['_id']}}','{{ csrf_token() }}','')" class="site-button green button-sm" >Accept Bill</a>
                                                <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm" data-toggle="dropdown" aria-expanded="false" data-tooltip="Reject Bill"><i class="fa fa-ban"></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <form action="" method="get">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <textarea class="form-control" id="bill_status_reason" placeholder="Write a Reason"></textarea>
                                                                    </div>
                                                                </div>
                                                                <button onclick="updateBillStatus('2','{{ $data['_id']}}','{{ csrf_token() }}','bill_status_reason')" class="site-button blue">Submit</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div id="gallery" class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                <a href="{{ $data['bill_path'] }}" class="gal_link site-button blue button-sm" data-tooltip="View Bill"><i class="fa fa-eye"></i></a>
                                                    <div class="dropdown cstm-dropdown-select order_dtl_action dis-inline-block">
                                                        <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                            <i class="fa fa-eye"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                            <li><a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 <i class="fa fa-times-circle"></i></a></li>
                                                            <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                        </ul>
                                                    </div>

                                                </div> -->
                                            @endif
                                        <?php //dd($data); ?>
                                        @if(!empty($data["bills"]) || !empty($data["creditNotes"]) || !empty($data["debitNotes"]))
                                            <div class="dropdown cstm-dropdown-select bid-rate-dropdown" style="display: block;display: block;position: absolute; right: 10px; top: 0;">
                                                <div class="dropdown cstm-dropdown-select order_dtl_action dis-inline-block">
                                                    <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <i class="fa fa-file-text-o"></i> View Bill
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
<!--................................ Invoice Bill........................................................................  -->
                                                    @if(isset($data["bills"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["bills"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Invoice {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:;" onclick="updateBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','','invoice')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason_{{ $bill_value['_id'] }}" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <a href="javascript:;" onclick="updateBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id'] }}','{{ csrf_token() }}','bill_status_reason_{{ $bill_value['_id'] }}','invoice')" class="site-button blue">Submit</a>
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Invoice {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Invoice {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ Invoice Bill........................................................................  -->


<!--................................ credit Notes Bill........................................................................  -->
                                                    @if(isset($data["creditNotes"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["creditNotes"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Credit Note {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:;" onclick="updateBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','','credit_notes')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason_{{ $bill_value['_id'] }}" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <a href="javascript:;" onclick="updateBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','bill_status_reason_{{ $bill_value['_id'] }}','credit_notes')" class="site-button blue">Submit</a>
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Credit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Credit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ credit Notes Bill........................................................................  -->

<!--................................ debit Notes Bill........................................................................  -->
                                                    @if(isset($data["debitNotes"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["debitNotes"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Debit note {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:void(0);" onclick="updateBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','','debit_notes')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason_{{ $bill_value['_id'] }}" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <a href="javascript:void(0);" onclick="updateBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','bill_status_reason_{{ $bill_value['_id'] }}','debit_notes')" class="site-button blue">Submit</a>
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Debit note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Debit note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ debit Notes Bill........................................................................  -->
                                                        <!-- <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 </a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-green">Accept</span>
                                                                <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <li class="p-a15">
                                                                        <form action="" method="get">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <textarea class="form-control" placeholder="Write a Reason"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <button class="site-button blue">Submit</button>
                                                                        </form>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3</i></a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-green">Accepted</span>
                                                            </div>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3</i></a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-red">Rejected</span>
                                                                <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                        <li class="p-a15">
                                                                            <h3>Rejected By Admin :</h3>
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                            </p>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li> -->
                                                    </ul>
                                                </div>


                                            <!-- <a href="javascript:;" class="site-button blue dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Upload Bill</a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                <li>
                                                    <form action="" method="get">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="file-browse">
                                                                    <span class="button-browse">
                                                                        Browse <input type="file">
                                                                    </span>
                                                                    <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button class="site-button blue">Upload</button>
                                                    </form>
                                                </li>
                                            </ul> -->

                                                <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <h3>Admin By :</h3>
                                                            <p>
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div> -->
                                        </div>
                                        @endif


                                                <!-- <a href="javascript:;" class="site-button blue dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Upload Bill</a>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li>
                                                        <form action="" method="get">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="file-browse">
                                                                        <span class="button-browse">
                                                                            Browse <input type="file">
                                                                        </span>
                                                                        <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button class="site-button blue">Upload</button>
                                                        </form>
                                                    </li>
                                                </ul> -->

                                                <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <h3>Admin By :</h3>
                                                            <p>
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div> -->
                                            </div>


                                </div>
                            </div>
                            <div class="orders-content-middle-block">
                                <div class="orders-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Concrete Grade</th>
                                                <th>Design Mix Name</th>
                                                <th>Qty (Cu. Mtr)</th>


                                                <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                                <th>With TM & CP </th>
                                                <!-- <th>TM Price ( <i class="fa fa-rupee"></i> )</th> -->
                                                <!-- <th>CP Price ( <i class="fa fa-rupee"></i> )</th> -->
                                                <th>Margin Amount ( <i class="fa fa-rupee"></i> )</th>
                                                <th>Base Amount ( <i class="fa fa-rupee"></i> )</th>
                                                <th>GST Amount ( <i class="fa fa-rupee"></i> )</th>
                                                <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                                <th>Order Status</th>
                                                @if($profile["rights"]["supplier_order_action"])
                                                    <th>Action</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($order_item_data))

                                            <tr>
                                                <td>
                                                    <div class="cate_img">

                                                        <?php
                                                            $primary_img = "";

                                                            if(isset($order_item_data["vendor_media"])){
                                                                foreach($order_item_data["vendor_media"] as $media_value){
                                                                    if($media_value["type"] == "PRIMARY"){
                                                                        $primary_img = $media_value["media_url"];
                                                                    }

                                                                }
                                                            }

                                                        ?>
                                                        @if($primary_img == "")
                                                            <img src="{{asset('assets/buyer/images/category_truck.webp')}}" alt="category">
                                                        @else
                                                            <img src="{{ $primary_img }}" alt="" />
                                                        @endif

                                                    </div>
                                                </td>
                                                <td>
                                                    {{ isset($order_item_data['concrete_grade']) ? $order_item_data['concrete_grade']['name'] : (isset($order_item_data['concrete_grade_name']) ? $order_item_data['concrete_grade_name'] : '') }}

                                                </td>
                                                <td>
                                                    <div class="dropdown tm_info_dropdown custom_rmc_dropdown">
                                                        <a href="#" class="dropdown-toggle text-nowrap" data-toggle="dropdown" aria-expanded="true"><span class="dropdown-label">
                                                        {{ isset($order_item_data['design_mix']["product_name"]) ? $order_item_data['design_mix']["product_name"] : (isset($order_item_data["orderItem"]["product_name"]) ? $order_item_data["orderItem"]["product_name"] : 'Custom Mix') }}
                                                        <i class="fa fa-sort-down"></i></span></a>
                                                        <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <div class="custom_rmc_bx">
                                                                <h4>Design Mix</h4>
                                                                <div class="custom_rmc_info">
                                                                    <h5>{{ isset($order_item_data['concrete_grade']) ? $order_item_data['concrete_grade']['name'] : (isset($order_item_data['concrete_grade_name']) ? $order_item_data['concrete_grade_name'] : '') }} - {{ isset($order_item_data['design_mix']["product_name"]) ? $order_item_data['design_mix']["product_name"] : (isset($order_item_data["orderItem"]["product_name"]) ? $order_item_data["orderItem"]["product_name"] : 'Custom Mix') }}</h5>
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <p>Cement ( Kg ) : <span>{{ isset($order_item_data['cement_quantity']) ? $order_item_data['cement_quantity'] : '' }}</span></p>
                                                                            <p>Coarse Sand ( Kg ) : <span>{{ isset($order_item_data['sand_quantity']) ? $order_item_data['sand_quantity'] : '' }}</span></p>
                                                                            <p>Fly Ash ( Kg ) : <span>{{ isset($order_item_data['fly_ash_quantity']) ? $order_item_data['fly_ash_quantity'] : '' }}</span></p>
                                                                            <p>Admixture ( Kg ) : <span>{{ isset($order_item_data['admix_quantity']) ? $order_item_data['admix_quantity'] : '' }}</span></p>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <p>{{ isset($order_item_data["aggregate1_sub_cat"]["sub_category_name"]) ? $order_item_data["aggregate1_sub_cat"]["sub_category_name"] : (isset($value["aggregate1_sub_cat_name"]) ? $value["aggregate1_sub_cat_name"] : '') }} ( Kg ) : 
                                                                                <span>{{ isset($order_item_data['aggregate1_quantity']) ? $order_item_data['aggregate1_quantity'] : '' }}</span></p>
                                                                            <p>{{ isset($order_item_data["aggregate2_sub_cat"]["sub_category_name"]) ? $order_item_data["aggregate2_sub_cat"]["sub_category_name"] : (isset($value["aggregate2_sub_cat_name"]) ? $value["aggregate2_sub_cat_name"] : '') }} ( Kg ) : 
                                                                                <span>{{ isset($order_item_data['aggregate2_quantity']) ? $order_item_data['aggregate2_quantity'] : '' }}</span></p>
                                                                            <p>Water ( Ltr ) : <span>{{ isset($order_item_data['water_quantity']) ? $order_item_data['water_quantity'] : '' }}</span></p>
                                                                            <p>Grade : <span>{{ isset($order_item_data['concrete_grade']) ? $order_item_data['concrete_grade']['name'] : (isset($order_item_data['concrete_grade_name']) ? $order_item_data['concrete_grade_name'] : '') }}</span></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="custom_rmc_info custom_rmc_dec">
                                                                    <b>Design Mix Description</b>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                        
                                                                            <p>Cement Brand : <span>{{ isset($order_item_data["cement_brand"]["name"]) ? $order_item_data["cement_brand"]["name"] : (isset($order_item_data["cement_brand_name"]) ? $order_item_data["cement_brand_name"] : '') }}</span></p>
                                                                            <p>Coarse Sand Source : <span>{{ isset($order_item_data["sand_source"]["sand_source_name"]) ? $order_item_data["sand_source"]["sand_source_name"] : (isset($order_item_data["sand_source_name"]) ? $order_item_data["sand_source_name"] : '') }}</span></p>
                                                                            <p>Coarse Sand Zone : <span>{{ (isset($order_item_data["sand_zone_name"]) ? $order_item_data["sand_zone_name"] : '') }}</span></p>
                                                                            <p>Aggregate Source : <span>{{ isset($order_item_data["aggregate_source"]["aggregate_source_name"]) ? $order_item_data["aggregate_source"]["aggregate_source_name"] : (isset($order_item_data["agg_source_name"]) ? $order_item_data["agg_source_name"] : '') }}</span></p>
                                                                            <p>Admixture Brand : <span>{{ isset($order_item_data["admix_brand"]["name"]) ? $order_item_data["admix_brand"]["name"] : (isset($order_item_data["ad_mixture1_brand_name"]) ? $order_item_data["ad_mixture1_brand_name"] : '') }} - {{ isset($order_item_data["admix_cat"]["category_name"]) ? $order_item_data["admix_cat"]["category_name"] : (isset($order_item_data["ad_mixture1_category_name"]) ? $order_item_data["ad_mixture1_category_name"] : '') }}</span></p>
                                                                            <p>Fly Ash Source Name : <span>{{ (isset($order_item_data["fly_ash_source_name"]) ? $order_item_data["fly_ash_source_name"] : '') }}</span></p>
                                                                            <p>Water : <span>Regular</span></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td><span class="qty-text">{{ $order_item_data['quantity'] }}</span></td>


                                                <td>{{ isset($order_item_data["unit_price"]) ? number_format($order_item_data["unit_price"],2) : 0 }}</td>
                                                <td>{{ ($order_item_data["with_TM"] == 'true') ? (($order_item_data["with_CP"] == 'true') ? 'TM & CP' : 'TM Only' ): "Buyer's TM" }}
                                                    @if(!$order_item_data["with_TM"])
                                                        <div class="dropdown tm_info_dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Buyer TM Info</a>
                                                            <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <div class="tm_info_bx">
                                                                    <h4>Buyer TM Detail</h4>
                                                                    <div class="tm_info_list"> <span>Delivery Date & Time :</span> 04 Jan 2021, 10:50 AM </div>
                                                                    <div class="tm_info_list"> <span>Transit Mixer No :</span> GJ-01-AA-0000 </div>
                                                                    <div class="tm_info_list"> <span>Driver Name :</span> Jhon </div>
                                                                    <div class="tm_info_list"> <span>Mobile No. :</span> +91 9979016486 </div>
                                                                    <div class="tm_info_list"> <span>TM Operator Name :</span> Jhondoe </div>
                                                                    <div class="tm_info_list"> <span>TM Operator Mobile No. :</span> +91 9979016486 </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </td>
                                                <!-- <td> {{ isset($order_item_data['TM_price']) ? number_format($order_item_data['TM_price'],2) : 0 }}</td> -->
                                                <!-- <td> {{ isset($order_item_data['CP_price']) ? number_format($order_item_data['CP_price'],2) : 0 }}</td> -->
                                                <td> {{ isset($order_item_data['margin_price']) ? number_format($order_item_data['margin_price'],2) : 0 }}</td>
                                                <td> {{ isset($order_item_data['selling_price']) ? number_format($order_item_data['selling_price'],2) : 0 }}</td>
                                                <td> {{ isset($order_item_data['gst_price']) ? number_format($order_item_data['gst_price'],2) : 0 }}</td>
                                                <td>{{ isset($order_item_data['total_amount_without_margin']) ? number_format($order_item_data['total_amount_without_margin'],2) : 0 }}</td>
                                                <td class="noowrp">

                                                @if($order_item_data["order_status"] == 'DELIVERED' || $order_item_data["order_status"] == 'ACCEPTED')
                                                    <div class="badge bg-green">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>
                                                @else @if($order_item_data["order_status"] == 'CANCELLED' || $order_item_data["order_status"] == 'REJECTED' || $order_item_data["order_status"] == 'LAPSED')
                                                    <div class="badge bg-red">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>
                                                @else @if($order_item_data["order_status"] == 'RECEIVED')
                                                    <div class="badge bg-blue">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>
                                                @else @if($order_item_data["order_status"] == 'PICKUP')
                                                    <div class="badge bg-purple">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>

                                                @else
                                                    <div class="badge bg-yellow">{{ str_replace("_"," ",$order_item_data["order_status"]) }}</div>
                                                @endif
                                                @endif
                                                @endif
                                                @endif


                                                </td>
                                                @if($profile["rights"]["supplier_order_action"])
                                                    <td>
                                                        @if($order_item_data["order_status"] == 'DELIVERED')
                                                            <div class="dropdown cstm-dropdown-select">
                                                                <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                    <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <li><a class="dropdown-item" onclick="showOrderStatusDialog('vendor_item_status','{{ json_encode($order_item_data) }}')"  >Change Payment Status</a></li>
                                                                    <!-- <li><a class="dropdown-item" onclick="showSupplierDialog(' json_encode($data['vendor']) ')">Contact Supplier</a></li> -->
                                                                    <!-- <li><a class="dropdown-item" href="{{ url('admin/orders/track/'.$order_item_data['_id']) }}">Track</a></li> -->
                                                                </ul>
                                                            </div>
                                                        @else
                                                                Action will visible after order delivered.
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>

                                            @endif
                                        </tbody>
                                    </table>

                                    <div class="trip_driver_tbl">
                                        <div class="comn-table latest-order-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Assigned Date</th>
                                                        <th>Assigned Start Time</th>
                                                        <th>Assigned End Time</th>
                                                        <th>Deliver Qty</th>
                                                        <th>CP Tracking</th>

                                                        <!-- <th>Assign TM</th>
                                                        <th>Assign CP</th> -->
                                                        <!-- <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($orderItemPartData) && count($orderItemPartData) > 0)
                                                <?php //dd($orderItemPartData); ?>
                                                    @foreach($orderItemPartData as $value)
                                                    <tr>
                                                        <td>{{ date('d M Y', strtotime($value["start_time"])) }}</td>
                                                        <td>{{ date('d M Y, h:i', strtotime($value["start_time"])) }}</td>
                                                        <td>{{ date('d M Y, h:i', strtotime($value["end_time"])) }}</td>
                                                        <td>{{ $value["assigned_quantity"] }} Cu. Mtr</td>
                                                        <td>

                                                            @if(!isset($value['CPtrack_details'][0]))
                                                                <a href="javascript:;"  class="site-button dark button-sm text-nowrap">CP not assigned </a>

                                                            @else
                                                                <span class="text-success">CP Assigned</span>
                                                                <a href="javascript:void(0)" onclick="supplierCPTrack('{{ $value['_id'] }}')">Track</a>
                                                            @endif

                                                        </td>


                                                    </tr>
                                                    @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="11">No Record Found</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                    <div class="trip_driver_tbl">
                                        <div class="comn-table latest-order-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Category</th>
                                                        <th>Vehical No</th>
                                                        <th>Driver Detail</th>
                                                        <th>Assign Date</th>
                                                        <th>Delivery Start Time </th>
                                                        <th>Delivery End Time </th>
                                                        <th>Deliver Qty</th>
                                                        <th>Delivery Status</th>
                                                        <th>Track</th>
                                                        <!-- <th>View Bill</th> -->
                                                        <!-- <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($orderTrackDetails) && count($orderTrackDetails) > 0)
                                                <?php //dd($orderTrackDetails); ?>
                                                    @foreach($orderTrackDetails as $value)
                                                    <tr>
                                                        <td>{{ isset($value["category_name"]) ? $value["category_name"] : $value["TM_category_name"] }}</td>
                                                        <td class="text-uppercase">{{ $value["TM_rc_number"] }}</td>
                                                        @if(isset($value["driver_name"]))
                                                            <td>{{ $value["driver_name"] }} - {{ $value["driver_mobile_number"] }}</td>
                                                        @endif
                                                        @if(isset($value["TM_driver1_name"]))
                                                            <td>{{ $value["TM_driver1_name"] }} - {{ $value["TM_driver1_mobile_number"] }}</td>
                                                        @endif
                                                        <td>{{ date('d M Y', strtotime($value["assigned_at"])) }}</td>
                                                        <td>{{ date('d M Y, h:i a', strtotime($value["start_time"])) }}</td>
                                                        <td>{{ date('d M Y, h:i a', strtotime($value["end_time"])) }}</td>
                                                        <td>{{ $value["pickup_quantity"] }} Cu.Mtr</td>
                                                        <td>
                                                        @if($value["event_status"] == 'DELIVERED')
                                                            <div class="badge bg-green">{{ str_replace("_"," ",$value["event_status"]) }}</div>
                                                        @else @if($value["event_status"] == 'CANCELLED' || $value["event_status"] == 'REJECTED')
                                                            <div class="badge bg-red">{{ str_replace("_"," ",$value["event_status"]) }}</div>

                                                        @else @if($value["event_status"] == 'PICKUP')
                                                            <div class="badge bg-purple">{{ str_replace("_"," ",$value["event_status"]) }}</div>

                                                        @else
                                                            <div class="badge bg-yellow">{{ str_replace("_"," ",$value["event_status"]) }}</div>
                                                        @endif
                                                        @endif
                                                        @endif
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)" onclick="supplierItemTrack('{{ $value['order_item_id'] }}','{{ $value['_id'] }}')">Track</a>
                                                        </td>
                                                        <!-- <td>  <a href="" class="gal_link site-button blue button-sm" data-tooltip="View Bill"><i class="fa fa-eye"></i></a></td> -->
                                                        <!-- <td>
                                                            <div class="dropdown cstm-dropdown-select order_dtl_action">
                                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                    <i class="fa fa-eye"></i>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                                    <li><a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 <i class="fa fa-times-circle"></i></a></li>
                                                                    <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </td> -->
                                                    </tr>
                                                    @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="11">No Record Found</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--order Details end -->


<script>
    $(document).ready(function(){
        $('.dropdown-submenu a.dropdown-submenu-link').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
</script>

@endsection