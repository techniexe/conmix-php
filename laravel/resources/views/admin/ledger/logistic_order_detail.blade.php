@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums"><a href="orders.html">Order List</a> / <span>Order Details</span></h1>
    <div class="clearfix"></div>
    <!--order Details start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Order Details</h2>
                </div>

                <div class="odrs-details-block p-a0">
                @if(isset($data['data']))
                    <?php $final_data = $data; //dd($final_data); ?>
                    <?php $data = $data["data"]; //dd($data)?>

                    <?php $buyer_data = $data["buyer"]; ?>
                    <?php $product_data = $data["product"]; ?>
                    <?php $order_item_data = $data["orderItem"]; ?>
                    <?php $supplier_data = $data["supplier"]; ?>
                    <?php $logistic_data = $data["logistics_user"]; ?>
                    <?php $quoteDetails = $data["quoteDetails"]; ?>
                    <?php $orderTrackDetails = $data["orderTrackData"]; ?>
                    <div class="orders-content-block">
                        <div class="orders-content-head-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="ordlst-id-date-block">
                                        <div class="ordlst-sd-text-block spa">
                                            Order Id : <span>#{{ $data["_id"] }}</span>
                                        </div>
                                        <!-- <div class="ordlst-id-block">
                                            Invoice Id: <a href="#">10090042</a>
                                        </div> -->
                                        <div class="ordlst-date-block">
                                            Order Date : <span>{{ date('d M Y, h:i:s a', strtotime($data["created_at"])) }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Buyer Name : <span>{{ $buyer_data["full_name"] }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Supplier Name : <span>{{ $supplier_data["full_name"] }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Logistic Name : <span>{{ $logistic_data["full_name"] }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Order Status :
                                            @if($data["delivery_status"] == 'DELIVERED')
                                                <div class="badge bg-green">{{ $data["delivery_status"] }}</div>
                                            @else @if($data["delivery_status"] == 'CANCELLED')
                                                <div class="badge bg-red">{{ $data["delivery_status"] }}</div>

                                                @else
                                                    <div class="badge bg-yellow">{{ $data["delivery_status"] }}</div>
                                                @endif
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-ship-dlvry-date-block">
                                        <div class="ordlst-sd-text-block">
                                            Payment Status : <span class="badge {{ $data['logistics_payment_status'] == 'PAID' ? 'bg-green' : 'bg-red' }}">{{ $data["logistics_payment_status"] }}</span>
                                        </div>

                                        <div class="ordlst-sd-text-block">
                                            Total Amount : <span class="rupee-text"><i class="fa fa-rupee"></i> {{ round($quoteDetails["quoted_amount"]) }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Pickup Location :
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['pickup_address']['location']['coordinates'][1]) ? $data['pickup_address']['location']['coordinates'][1] : '' }}','{{ isset($data['pickup_address']['location']['coordinates'][0]) ? $data['pickup_address']['location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                {{ isset($data["pickup_address"]['line1']) ? $data["pickup_address"]['line1'] : '' }},
                                                    {{ isset($data["pickup_address"]['line2']) ? $data["pickup_address"]['line2'] : '' }},
                                                    {{ isset($data["pickup_address_state"]['state_name']) ? $data["pickup_address_state"]['state_name'] : '' }},
                                                    {{ isset($data["pickup_address_city"]['city_name']) ? $data["pickup_address_city"]['city_name'] : '' }} - {{ isset($data["pickup_address"]['pincode']) ? $data["pickup_address"]['pincode'] : '' }},

                                                </span>
                                            </span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Delivery Location :
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($data['delivery_address']['location']['coordinates'][1]) ? $data['delivery_address']['location']['coordinates'][1] : '' }}','{{ isset($data['delivery_address']['location']['coordinates'][0]) ? $data['delivery_address']['location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                {{ isset($data['delivery_address']['address_line1']) ? $data['delivery_address']['address_line1'] : '' }},
                                                    {{ isset($data['delivery_address']['address_line2']) ? $data['delivery_address']['address_line2'] : '' }},
                                                    {{ isset($data['delivery_address_state']['state_name']) ? $data['delivery_address_state']['state_name'] : '' }},
                                                    {{ isset($data['delivery_address_city']['city_name']) ? $data['delivery_address_city']['city_name'] : '' }} - {{ isset($data['delivery_address']['pincode']) ? $data['delivery_address']['pincode'] : '' }},
                                                </span>
                                            </span>
                                        </div>

                                    </div>
                                    <!--     <div class="odr-select-optin-block">                                  <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="dropdown-label">Select Invoice <i class="fa fa-sort-down"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li><a class="dropdown-item" href="invoice.html">View invoice</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Download invoice</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#sendemil-sts-popup">Send invoice to email</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Download purchase order</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Send purchase order to email</a></li>
                                                </ul>
                                            </div>
                                        </div> -->
                                        @if(isset($data["bill_url"]))
                                            <!--  <div class="dropdown cstm-dropdown-select bid-rate-dropdown" style="display: block;display: block;position: absolute; right: 10px; top: 0;">

                                               <a href="javascript:;" onclick="updateLogisticsBillStatus('1','{{ $data['_id']}}','{{ csrf_token() }}','')" class="site-button green button-sm" >Accept Bill</a>

                                                <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm" data-toggle="dropdown" aria-expanded="false" data-tooltip="Reject Bill"><i class="fa fa-ban"></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <form action="" method="get">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <textarea class="form-control" id="bill_status_reason" placeholder="Write a Reason"></textarea>
                                                                    </div>
                                                                </div>
                                                                <a href="javascript:;" onclick="updateLogisticsBillStatus('2','{{ $data['_id']}}','{{ csrf_token() }}','bill_status_reason')" class="site-button blue">Submit</a>
                                                            </form>
                                                        </li>
                                                    </ul>

                                                </div>

                                            <div id="gallery" class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                <a href="{{ $data['bill_url'] }}" class="gal_link site-button blue button-sm" data-tooltip="View Bill"><i class="fa fa-eye"></i></a>
                                                <div class="dropdown cstm-dropdown-select order_dtl_action dis-inline-block">
                                                    <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <i class="fa fa-eye"></i>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                        <li><a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 <i class="fa fa-times-circle"></i></a></li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div> -->
                                        @endif

                                        @if(!empty($data["bills"]) || !empty($data["creditNotes"]) || !empty($data["debitNotes"]))
                                        <div class="dropdown cstm-dropdown-select bid-rate-dropdown" style="display: block;display: block;position: absolute; right: 10px; top: 0;">
                                        <div class="dropdown cstm-dropdown-select order_dtl_action dis-inline-block">
                                                    <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <i class="fa fa-file-text-o"></i> View Bill
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
<!--................................ Invoice Bill........................................................................  -->
                                                    @if(isset($data["bills"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["bills"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Invoice {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:;" onclick="updateLogisticsBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id'] }}','{{ csrf_token() }}','','invoice')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <a href="javascript:;" onclick="updateLogisticsBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id'] }}','{{ csrf_token() }}','bill_status_reason','invoice')" class="site-button blue">Submit</a>
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Invoice {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Invoice {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ Invoice Bill........................................................................  -->


<!--................................ credit Notes Bill........................................................................  -->
                                                    @if(isset($data["creditNotes"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["creditNotes"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Credit Note {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:;" onclick="updateLogisticsBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id'] }}','{{ csrf_token() }}','','credit_notes')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <a href="javascript:;" onclick="updateLogisticsBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id'] }}','{{ csrf_token() }}','bill_status_reason','credit_notes')" class="site-button blue">Submit</a>
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Credit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Credit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ credit Notes Bill........................................................................  -->

<!--................................ debit Notes Bill........................................................................  -->
                                                    @if(isset($data["debitNotes"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["debitNotes"] as $bill_value)

                                                                <?php
                                                                    $file_type_class = '';
                                                                    $file_type_fa_class = '';
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>

                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Debit note {{ $biils_count++ }}
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:;" onclick="updateLogisticsBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id'] }}','{{ csrf_token() }}','','debit_notes')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="javascript:void(0)" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <button onclick="updateLogisticsBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id'] }}','{{ csrf_token() }}','bill_status_reason','debit_notes')" class="site-button blue">Submit</button>
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Debit note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')

                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Debit note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach


                                                        @endif

<!--................................ debit Notes Bill........................................................................  -->
                                                        <!-- <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 </a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-green">Accept</span>
                                                                <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <li class="p-a15">
                                                                        <form action="" method="get">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <textarea class="form-control" placeholder="Write a Reason"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <button class="site-button blue">Submit</button>
                                                                        </form>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3</i></a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-green">Accepted</span>
                                                            </div>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3</i></a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-red">Rejected</span>
                                                                <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                        <li class="p-a15">
                                                                            <h3>Rejected By Admin :</h3>
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                            </p>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li> -->
                                                    </ul>
                                                </div>


                                            <!-- <a href="javascript:;" class="site-button blue dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Upload Bill</a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                <li>
                                                    <form action="" method="get">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="file-browse">
                                                                    <span class="button-browse">
                                                                        Browse <input type="file">
                                                                    </span>
                                                                    <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button class="site-button blue">Upload</button>
                                                    </form>
                                                </li>
                                            </ul> -->

                                                <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <h3>Admin By :</h3>
                                                            <p>
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div> -->



                                            <!-- <a href="javascript:;" class="site-button blue dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Upload Bill</a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                <li>
                                                    <form action="" method="get">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="file-browse">
                                                                    <span class="button-browse">
                                                                        Browse <input type="file">
                                                                    </span>
                                                                    <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button class="site-button blue">Upload</button>
                                                    </form>
                                                </li>
                                            </ul> -->

                                                <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <h3>Admin By :</h3>
                                                            <p>
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div> -->
                                        </div>
                                        @endif
                                        </div>

                                </div>
                            </div>
                        </div>
                        <div class="orders-content-middle-block">
                            <div class="orders-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Image</th>
                                            <th>Category Name</th>
                                            <th>Qty</th>
                                            <!-- <th>Per Unit Price( <i class="fa fa-rupee"></i> )</th>
                                            <th>Base Amount( <i class="fa fa-rupee"></i> )</th>
                                            <th>GST Amount( <i class="fa fa-rupee"></i> )</th> -->
                                            <!-- <th>Total Amount( <i class="fa fa-rupee"></i> )</th> -->
                                            <th class="tble_heding_color_red">Remaining Qty</th>
                                            <th class="tble_heding_color_green">Delivered Qty</th>
                                            <th>Supplier Detail</th>
                                            <th>Pending Qty To Assign</th>
                                            <!-- <th>Status</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($order_item_data))
                                            @foreach($order_item_data as $value)
                                        <tr>
                                        <td>
                                                <div class="cate_img"><img width="60" src="{{ isset($data['product_category']['image_url']) ? $data['product_category']['image_url'] : '' }}"></div>
                                            </td>
                                            <td>{{ $data['product_category']['category_name'] }} - {{ $data['product_sub_category']['sub_category_name'] }}</td>
                                            <td><span class="qty-text">{{ $value['quantity'] }} {{ $value['quantity_unit'] }}</span></td>
                                             <!-- <td>100</td>
                                             <td>5000</td>


                                            <td> 3,000</td> -->
                                            <!-- <td>1,300</td> -->
                                            <td class="tble_heding_color_red">{{ $value['remaining_quantity'] }} {{ $value['quantity_unit'] }}</td>
                                            <td class="tble_heding_color_green">{{ isset($value['delivered_quantity']) ? $value['delivered_quantity'] : '0' }} {{ $value['quantity_unit'] }}</td>
                                            <td>
                                                {{ $supplier_data["full_name"] }},
                                                {{ $supplier_data["mobile_number"] }}
                                            </td>
                                            <td>{{ isset($value["temp_quantity"]) ? $value["temp_quantity"] : 0 }} {{ $value["quantity_unit"] }}</td>
                                            <!-- <td class="noowrp">
                                                @if($value["item_status"] == 'DELIVERED')
                                                        <div class="badge bg-green">{{ $value["item_status"] }}</div>
                                                    @else @if($value["item_status"] == 'CANCELLED')
                                                        <div class="badge bg-red">{{ $value["item_status"] }}</div>

                                                        @else
                                                            <div class="badge bg-yellow">{{ $value["item_status"] }}</div>
                                                        @endif
                                                    @endif
                                                <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button bg-grey button-sm button-sm-ctm1" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <h3>Admin By :</h3>
                                                            <p>
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td> -->
                                            <td>
                                                <!-- <div class="dropdown cstm-dropdown-select">
                                                    <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#changes-sts-popup">Change Status</a></li>
                                                        <li ><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#assign-truck-popup">Truck Assign</a></li>

                                                        <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#changes-splr-popup">Contact Supplier</a></li>
                                                        <li><a class="dropdown-item" href="track.html">Track</a></li>
                                                    </ul>
                                                </div> -->
                                                @if(isset($value["temp_quantity"]) && $value["temp_quantity"] != 0)
                                                    <a href="javascript:;" data-toggle="modal" data-target="#assign-truck-popup" class="site-button blue button-sm">Truck Assign</a>
                                                @else
                                                    Truck assigned to all Qty
                                                @endif
                                            </td>
                                        </tr>
                                            @endforeach

                                        @endif

                                    </tbody>
                                </table>

                                <div class="trip_driver_tbl">
                                    <div class="comn-table latest-order-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Category</th>
                                                    <th>Vehical No</th>
                                                    <th>Driver Detail</th>
                                                    <th>Assign Date</th>
                                                    <th>Deliver Qty</th>
                                                    <th>Track</th>
                                                    <th>Delivery Status</th>
                                                    <th>View Biil</th>
                                                    <!-- <th>Action</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($orderTrackDetails) && count($orderTrackDetails) > 0)
                                                @foreach($orderTrackDetails as $value)
                                                <tr>
                                                    <td>{{ $value["category_name"] }}</td>
                                                    <td>{{ $value["vehicle_rc_number"] }}</td>
                                                    <td>{{ $value["driver_name"] }} - {{ $value["driver_mobile_number"] }}</td>
                                                    <td>{{ date('d M Y', strtotime($value["assigned_at"])) }}</td>
                                                    <td>{{ isset($value["pickup_quantity"]) ? $value["pickup_quantity"] : 0 }} {{ $order_item_data[0]["quantity_unit"] }}</td>
                                                    <td>
                                                        <a href="javascript:void(0)" onclick="logisticsItemTrack('{{ $value['order_item_id'] }}','{{ $value['_id'] }}')">Track</a>
                                                    </td>
                                                    <td>
                                                    @if($value["event_status"] == 'DELIVERED')
                                                        <div class="badge bg-green">{{ $value["event_status"] }}</div>
                                                    @else @if($value["event_status"] == 'CANCELLED')
                                                        <div class="badge bg-red">{{ $value["event_status"] }}</div>

                                                        @else
                                                            <div class="badge bg-yellow">{{ $value["event_status"] }}</div>
                                                        @endif
                                                    @endif
                                                    </td>
                                                    <td>
                                                        <div class="dropdown cstm-dropdown-select order_dtl_action">
                                                            <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                @if(isset($value["challan_image_url"]))
                                                                <?php
                                                                    $challen_file_class = '';
                                                                    $challen_fa_class = '';

                                                                ?>
                                                                @if(str_contains($value["challan_image_url"], '.jpg'))
                                                                    <?php
                                                                     $challen_file_class = '';
                                                                     $challen_fa_class = 'fa-image';
                                                                    ?>
                                                                @elseif(str_contains($value["challan_image_url"], '.doc') || str_contains($value["challan_image_url"], '.docx'))
                                                                    <?php
                                                                    $challen_file_class = 'docfile';
                                                                     $challen_fa_class = 'fa-times-circle';
                                                                ?>
                                                                @elseif(str_contains($value["challan_image_url"], '.jpg'))
                                                                <?php
                                                                    $challen_file_class = 'pdffile';
                                                                     $challen_fa_class = 'fa-file-pdf-o';
                                                                     ?>
                                                                @endif

                                                                    <li>
                                                                        <a class="dropdown-item {{ $challen_file_class }}" href="{{ $value['challan_image_url'] }}">
                                                                            <i class="fa {{ $challen_fa_class }}"></i> Delivery Challan
                                                                        </a>
                                                                    </li>
                                                                @endif

                                                                @if(isset($value["royalty_pass_image_url"]))
                                                                <?php
                                                                    $royalty_file_class = '';
                                                                    $royalty_fa_class = '';

                                                                ?>
                                                                @if(str_contains($value["royalty_pass_image_url"], '.jpg'))
                                                                <?php
                                                                     $royalty_file_class = '';
                                                                     $royalty_fa_class = 'fa-image';
                                                                    ?>
                                                                @elseif(str_contains($value["royalty_pass_image_url"], '.doc') || str_contains($value["royalty_pass_image_url"], '.docx'))
                                                                <?php
                                                                    $royalty_file_class = 'docfile';
                                                                     $royalty_fa_class = 'fa-times-circle';
                                                                ?>
                                                                @elseif(str_contains($value["royalty_pass_image_url"], '.jpg'))
                                                                <?php
                                                                    $royalty_file_class = 'pdffile';
                                                                     $royalty_fa_class = 'fa-file-pdf-o';
                                                                     ?>
                                                                @endif

                                                                    <li>
                                                                        <a class="dropdown-item {{ $royalty_file_class }}" href="{{ $value['royalty_pass_image_url'] }}">
                                                                            <i class="fa {{ $royalty_fa_class }}"></i> Royalty Pass
                                                                        </a>
                                                                    </li>
                                                                @endif


                                                                @if(isset($value["way_slip_image_url"]))
                                                                <?php
                                                                    $way_slip_file_class = '';
                                                                    $way_slip_fa_class = '';

                                                                ?>
                                                                @if(str_contains($value["way_slip_image_url"], '.jpg'))
                                                                <?php
                                                                     $way_slip_file_class = '';
                                                                     $way_slip_fa_class = 'fa-image';
                                                                ?>
                                                                @elseif(str_contains($value["way_slip_image_url"], '.doc') || str_contains($value["way_slip_image_url"], '.docx'))
                                                                <?php
                                                                    $way_slip_file_class = 'docfile';
                                                                     $way_slip_fa_class = 'fa-times-circle';
                                                                ?>
                                                                @elseif(str_contains($value["way_slip_image_url"], '.jpg'))
                                                                <?php
                                                                    $way_slip_file_class = 'pdffile';
                                                                     $way_slip_fa_class = 'fa-file-pdf-o';
                                                                     ?>
                                                                @endif

                                                                    <li>
                                                                        <a class="dropdown-item {{ $way_slip_file_class }}" href="{{ $value['way_slip_image_url'] }}">
                                                                            <i class="fa {{ $way_slip_fa_class }}"></i> Way Slip
                                                                        </a>
                                                                    </li>
                                                                @endif

                                                                @if(isset($value["driver_pickup_signature"]))
                                                                <?php
                                                                    $way_slip_file_class = '';
                                                                    $way_slip_fa_class = '';

                                                                ?>
                                                                @if(str_contains($value["driver_pickup_signature"], '.jpg'))
                                                                <?php
                                                                     $way_slip_file_class = '';
                                                                     $way_slip_fa_class = 'fa-image';
                                                                ?>
                                                                @elseif(str_contains($value["driver_pickup_signature"], '.doc') || str_contains($value["driver_pickup_signature"], '.docx'))
                                                                <?php
                                                                    $way_slip_file_class = 'docfile';
                                                                     $way_slip_fa_class = 'fa-times-circle';
                                                                ?>
                                                                @elseif(str_contains($value["driver_pickup_signature"], '.jpg'))
                                                                <?php
                                                                    $way_slip_file_class = 'pdffile';
                                                                     $way_slip_fa_class = 'fa-file-pdf-o';
                                                                     ?>
                                                                @endif

                                                                    <li>
                                                                        <a class="dropdown-item {{ $way_slip_file_class }}" href="{{ $value['driver_pickup_signature'] }}">
                                                                            <i class="fa {{ $way_slip_fa_class }}"></i> Deliver Pickup Signature
                                                                        </a>
                                                                    </li>
                                                                @endif

                                                                @if(isset($value["supplier_pickup_signature"]))
                                                                <?php
                                                                    $way_slip_file_class = '';
                                                                    $way_slip_fa_class = '';

                                                                ?>
                                                                @if(str_contains($value["supplier_pickup_signature"], '.jpg'))
                                                                <?php
                                                                     $way_slip_file_class = '';
                                                                     $way_slip_fa_class = 'fa-image';
                                                                    ?>
                                                                @elseif(str_contains($value["supplier_pickup_signature"], '.doc') || str_contains($value["supplier_pickup_signature"], '.docx'))
                                                                <?php
                                                                    $way_slip_file_class = 'docfile';
                                                                     $way_slip_fa_class = 'fa-times-circle';
                                                                ?>
                                                                @elseif(str_contains($value["supplier_pickup_signature"], '.jpg'))
                                                                <?php
                                                                    $way_slip_file_class = 'pdffile';
                                                                     $way_slip_fa_class = 'fa-file-pdf-o';
                                                                     ?>
                                                                @endif

                                                                    <li>
                                                                        <a class="dropdown-item {{ $way_slip_file_class }}" href="{{ $value['supplier_pickup_signature'] }}">
                                                                            <i class="fa {{ $way_slip_fa_class }}"></i> Supplier Pickup Signature
                                                                        </a>
                                                                    </li>
                                                                @endif

                                                                @if(isset($value["buyer_drop_signature"]))
                                                                <?php
                                                                    $way_slip_file_class = '';
                                                                    $way_slip_fa_class = '';

                                                                ?>
                                                                @if(str_contains($value["buyer_drop_signature"], '.jpg'))
                                                                <?php
                                                                     $way_slip_file_class = '';
                                                                     $way_slip_fa_class = 'fa-image';
                                                                ?>
                                                                @elseif(str_contains($value["buyer_drop_signature"], '.doc') || str_contains($value["buyer_drop_signature"], '.docx'))
                                                                <?php
                                                                    $way_slip_file_class = 'docfile';
                                                                     $way_slip_fa_class = 'fa-times-circle';
                                                                ?>
                                                                @elseif(str_contains($value["buyer_drop_signature"], '.jpg'))
                                                                <?php
                                                                    $way_slip_file_class = 'pdffile';
                                                                     $way_slip_fa_class = 'fa-file-pdf-o';
                                                                     ?>
                                                                @endif

                                                                    <li>
                                                                        <a class="dropdown-item {{ $way_slip_file_class }}" href="{{ $value['buyer_drop_signature'] }}">
                                                                            <i class="fa {{ $way_slip_fa_class }}"></i> Buyer Pickup Signature
                                                                        </a>
                                                                    </li>
                                                                @endif

                                                                @if(isset($value["driver_drop_signature"]))
                                                                <?php
                                                                    $way_slip_file_class = '';
                                                                    $way_slip_fa_class = '';

                                                                ?>
                                                                @if(str_contains($value["driver_drop_signature"], '.jpg'))
                                                                <?php
                                                                     $way_slip_file_class = '';
                                                                     $way_slip_fa_class = 'fa-image';
                                                                ?>
                                                                @elseif(str_contains($value["driver_drop_signature"], '.doc') || str_contains($value["driver_drop_signature"], '.docx'))
                                                                <?php
                                                                    $way_slip_file_class = 'docfile';
                                                                     $way_slip_fa_class = 'fa-times-circle';
                                                                ?>
                                                                @elseif(str_contains($value["driver_drop_signature"], '.jpg'))
                                                                <?php
                                                                    $way_slip_file_class = 'pdffile';
                                                                     $way_slip_fa_class = 'fa-file-pdf-o';
                                                                     ?>
                                                                @endif

                                                                    <li>
                                                                        <a class="dropdown-item {{ $way_slip_file_class }}" href="{{ $value['driver_drop_signature'] }}">
                                                                            <i class="fa {{ $way_slip_fa_class }}"></i> Driver Pickup Signature
                                                                        </a>
                                                                    </li>
                                                                @endif
                                                                <!-- <li><a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 <i class="fa fa-times-circle"></i></a></li>
                                                                <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li> -->
                                                            </ul>
                                                        </div>
                                                    </td>
                                                    <!-- <td>
                                                        <div class="dropdown cstm-dropdown-select">
                                                            <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <li><a class="dropdown-item" href="#">Action1</a></li>
                                                                <li><a class="dropdown-item" href="#">Action2</a></li>
                                                                <li><a class="dropdown-item" href="#">Action3</a></li>
                                                            </ul>
                                                        </div>
                                                    </td> -->
                                                </tr>
                                                @endforeach
                                            @else
                                            <tr>
                                                <td colspan="11">No Record Found</td>
                                            </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                @endif
                </div>
            </div>
        </div>
    </div>
    <!--order Details end -->
</div>

<script>
    $(document).ready(function(){
        $('.dropdown-submenu a.dropdown-submenu-link').on("click", function(e){
            console.log("testing...");
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
</script>

@endsection