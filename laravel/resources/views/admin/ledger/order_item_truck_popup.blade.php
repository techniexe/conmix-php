<?php //dd($data); ?>
<?php use App\Http\Controllers\Admin\AdminController;?>
<ul>
    @if(isset($data["data"]) && count($data["data"]) > 0)
        @foreach($data["data"] as $value)
            <li>
                <div class="ordlst-delivery-details-block">
                    <i class="fa fa-truck"></i>
                    @if(isset($value["delivered_at"]))
                        <div class="ordlst-delivery-date">Delivered on {{ AdminController::dateTimeFormat($value["delivered_at"]) }}</div>
                    @endif
                    <div class="ordlst-truck-no">
                        @if(isset($value["TM_category"]))    
                            {{ $value["TM_category"] }} ({{ $value["TM_sub_category"] }}) 
                        @endif
                        
                        @if(isset($value["TM_category_name"]))    
                            {{ $value["TM_category_name"] }} ({{ $value["TM_sub_category_name"] }}) 
                        @endif
                    </div>
                    <div class="ordlst-truck-no">TM No: <span class="text-uppercase">{{ $value["TM_rc_number"] }} </span></div>
                    <div class="ordlst-truck-no">Contact No: <span>
                        @if(isset($value["driver_mobile_number"]))
                            {{ $value["driver_mobile_number"] }}
                        @endif
                        
                        @if(isset($value["TM_driver1_mobile_number"]))
                            {{ $value["TM_driver1_mobile_number"] }}
                        @endif
                    </span></div>
                    <div class="ordlst-truck-no">

                        @if($value["event_status"] == 'DELIVERED')
                            <span class="badge bg-success">{{ $value["event_status"] }}</span>
                            
                        @else @if($value["event_status"] == 'CANCELLED' || $value["event_status"] == 'REJECTED' || $value["event_status"] == 'LAPSED')
                            <span class="badge bg-danger">{{ $value["event_status"] }}</span>
                            @if($value["event_status"] == 'LAPSED')
                                (Qty not assigned)
                            @endif
                            @if($value["event_status"] == 'REJECTED')
                                <div class="ordlst-truck-no">
                                    @if(isset($value["refund_id"]))
                                        Refund Id: <span>{{$value["refund_id"]}}</span>
                                    @endif
                                    
                                </div>
                                <div class="ordlst-truck-no">
                                    @if(isset($value["refund_amount"]))
                                        Refund Amount (Incl. GST): <span>₹ {{$value["refund_amount"]}}</span>
                                    @endif
                                    
                                </div>
                            @endif
                            
                        @else @if($value["event_status"] == 'PICKUP')
                            <span class="badge bg-purple">{{ $value["event_status"] }}</span>
                            
                        @else
                            <span class="badge bg-warning text-dark">{{ str_replace('_',' ',$value["event_status"]) }}</span>
                            @if($value["event_status"] == 'RECEIVED')
                                (Confirmation pending from RMC supplier)
                            @endif
                            
                        @endif
                        @endif
                        @endif

                    </div>
                    <div class="track-link"><a href="javascript:void(0)" onclick="buyerItemTracking('{{ $value['order_item_id'] }}','{{ $value['_id'] }}')" >Track</a></div>
                </div>
            </li>
        @endforeach
    @else
        <p>No Record Found</p>
    @endif
    
</ul>