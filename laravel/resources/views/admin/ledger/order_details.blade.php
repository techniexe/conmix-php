@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums">Ledger / <a href="{{ route('admin_show_orders') }}">Buyer Orders</a> / <span>Order Details</span></h1>
    <div class="clearfix"></div>

    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Order Details</h2>
                </div>

                <div class="odrs-details-block p-a0">

                    <div class="orders-content-block">
                    <?php //dd($data["data"]); ?>
                    @if(isset($data["data"]["orderData"]))
                        @php
                            $order_data = $data["data"]["orderData"];
                        @endphp
                        <div class="orders-content-head-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-id-date-block">
                                        <div class="ordlst-sd-text-block spa">
                                            Order Id : <span>#{{ isset($order_data['display_id']) ? $order_data['display_id'] : '' }}</span>
                                        </div>

                                        @if(isset($order_data['display_id']) && isset($order_data['invoice']['_id']))
                                        <div class="ordlst-id-block">

                                                Invoice Id : <a href="{{ url('admin/orders/invoice/'.$order_data['display_id']) }}">{{ isset($order_data['invoice']['_id']) ? $order_data['invoice']['_id'] : '' }}</a>

                                        </div>
                                        @endif
                                        <div class="ordlst-date-block">
                                            Order Date & Time : <span>{{ isset($order_data['created_at']) ? AdminController::dateTimeFormat($order_data["created_at"]) : '' }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Buyer Company Name : <span>{{ isset($order_data['buyer']['company_name']) ? $order_data['buyer']['company_name'] : '' }}  {{ isset($order_data['buyer']['full_name']) ? ' - '.$order_data['buyer']['full_name'] : '' }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-ship-dlvry-date-block">
                                        <div class="ordlst-sd-text-block">
                                            Payment Status : <span class="badge {{ isset($order_data['payment_status']) ? (($order_data['payment_status'] == 'PAID') ? 'bg-green' : 'bg-red') : '' }} ">{{ isset($order_data['payment_status']) ? $order_data['payment_status'] : '' }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            GST Amount : <span class="rupee-text"><i class="fa fa-rupee"></i> {{ isset($order_data['gst_price']) ? number_format($order_data['gst_price'],2) : '' }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Total Amount : <span class="rupee-text"><i class="fa fa-rupee"></i> {{ isset($order_data['total_amount']) ? number_format($order_data['total_amount'],2) : '' }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Delivery Location :
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                <!-- <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon" onclick="viewMap('{{ isset($order_data['delivery_location']['coordinates'][1]) ? $order_data['delivery_location']['coordinates'][1] : '' }}','{{ isset($order_data['delivery_location']['coordinates'][0]) ? $order_data['delivery_location']['coordinates'][0] : '' }}')">View Map</a> -->
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon" onclick="viewMap('{{ isset($order_data['latitude']) ? $order_data['latitude'] : '' }}','{{ isset($order_data['longitude']) ? $order_data['longitude'] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                    {{ isset($order_data['address_line1']) ? $order_data['address_line1'] : '' }},
                                                    {{ isset($order_data['address_line2']) ? $order_data['address_line2'] : '' }},
                                                    {{ isset($order_data['state_name']) ? $order_data['state_name'] : '' }},
                                                    {{ isset($order_data['city_name']) ? $order_data['city_name'] : '' }},

                                                </span>
                                            </span>
                                        </div>
                                        @if(isset($order_data['display_id']) && isset($order_data['invoice']['_id']))
                                        <div class="odr-select-optin-block">
                                            <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="dropdown-label">Select Invoice <i class="fa fa-sort-down"></i></span>
                                                </button>

                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">

                                                            <li><a class="dropdown-item" href="{{ url('admin/orders/invoice/'.$order_data['display_id']) }}">View invoice</a></li>

                                                        <!-- <li><a class="dropdown-item" href="javascript:;">Download invoice</a></li> -->
                                                        <!-- <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#sendemil-sts-popup">Send invoice to email</a></li> -->
                                                        <!-- <li><a class="dropdown-item" href="javascript:;">Download purchase order</a></li>
                                                        <li><a class="dropdown-item" href="javascript:;">Send purchase order to email</a></li> -->
                                                    </ul>

                                            </div>
                                        </div>
                                        @endif
                                        <!-- <div class="odr-select-optin-block">
                                            <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                </button>

                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">

                                                    <li><a class="dropdown-item" href="javascript:;" onclick="showOrderStatusDialog('payment_status','{{ json_encode($order_data) }}')">Change Payment Status</a></li>

                                                        
                                                    </ul>

                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                        <div class="orders-content-middle-block">
                            <div class="orders-table comn-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Concrete Grade</th>
                                            <th>Supplier</th>
                                            <th>RMC Design Mix</th>
                                            <th>Qty ( Cu.Mtr )</th>
                                            <!-- <th>Margin Rate</th> -->
                                            <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                            <th>TM & CP</th>
                                            <!-- <th>TM Price ( <i class="fa fa-rupee"></i> )</th> -->
                                            <!-- <th>CP Price ( <i class="fa fa-rupee"></i> )</th> -->
                                            <th>Basic Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <th>Margin Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <!-- <th>GST Amount ( <i class="fa fa-rupee"></i> )</th> -->

                                            <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                            <th>Order Status</th>
                                            <!-- <th>Track</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($data['data']["orderItemData"]))
                                            @php
                                                $item_data = $data['data']["orderItemData"];
                                            @endphp
                                        @foreach( $item_data as $value)
                                        <?php //dd($value); ?>
                                            <tr>
                                                <td>
                                                    <div class="cate_img">
                                                        <?php
                                                            $primary_img = "";

                                                            if(isset($value["vendor_media"])){
                                                                foreach($value["vendor_media"] as $media_value){
                                                                    if($media_value["type"] == "PRIMARY"){
                                                                        $primary_img = $media_value["media_url"];
                                                                    }

                                                                }
                                                            }

                                                        ?>
                                                        @if($primary_img == "")
                                                            <img src="{{asset('assets/buyer/images/category_truck.webp')}}">
                                                        @else
                                                            <img src="{{ $primary_img }}" alt="" />
                                                        @endif

                                                    </div>
                                                </td>
                                                <td>{{ isset($value['concrete_grade']) ? $value['concrete_grade']['name'] : (isset($value['concrete_grade_name']) ? $value['concrete_grade_name'] : '') }}</td>
                                                <td>{{ isset($value['vendor']['company_name']) ? $value['vendor']['company_name'] : '' }}</td>
                                                <td>

                                                    <div class="dropdown tm_info_dropdown custom_rmc_dropdown">
                                                        <a href="#" class="dropdown-toggle text-nowrap" data-toggle="dropdown" aria-expanded="true">
                                                        <span class="dropdown-label">
                                                        {{ isset($value['design_mix']["product_name"]) ? $value['design_mix']["product_name"] : (isset($value["product_name"]) ? $value["product_name"] : 'Custom Mix') }}
                                                        <i class="fa fa-sort-down"></i></span></a>
                                                        <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <div class="custom_rmc_bx">
                                                                <h4>Design Mix</h4>
                                                                <div class="custom_rmc_info">
                                                                    <h5>{{ isset($value['concrete_grade']) ? $value['concrete_grade']['name'] : (isset($value['concrete_grade_name']) ? $value['concrete_grade_name'] : '') }} - {{ isset($value['design_mix']["product_name"]) ? $value['design_mix']["product_name"] : (isset($value["product_name"]) ? $value["product_name"] : 'Custom Mix') }}</h5>
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <p>Cement ( Kg ) : <span>{{ isset($value['cement_quantity']) ? $value['cement_quantity'] : '' }}</span></p>
                                                                            <p>Coarse Sand ( Kg ) : <span>{{ isset($value['sand_quantity']) ? $value['sand_quantity'] : '' }}</span></p>
                                                                            <p>Fly Ash ( Kg ) : <span>{{ isset($value['fly_ash_quantity']) ? $value['fly_ash_quantity'] : '' }}</span></p>
                                                                            <p>Admixture ( Kg ) : <span>{{ isset($value['admix_quantity']) ? $value['admix_quantity'] : '' }}</span></p>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <p>10mm ( Kg ) : <span>{{ isset($value['aggregate1_quantity']) ? $value['aggregate1_quantity'] : '' }}</span></p>
                                                                            <p>20mm ( Kg ) : <span>{{ isset($value['aggregate2_quantity']) ? $value['aggregate2_quantity'] : '' }}</span></p>
                                                                            <p>Water ( Ltr ) : <span>{{ isset($value['water_quantity']) ? $value['water_quantity'] : '' }}</span></p>
                                                                            <p>Grade : <span>{{ isset($value['concrete_grade']) ? $value['concrete_grade']['name'] : (isset($value["concrete_grade_name"]) ? $value["concrete_grade_name"] : '') }}</span></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="custom_rmc_info custom_rmc_dec">
                                                                    <b>Design Mix Description</b>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                        
                                                                            <p>Cement Brand : <span>{{ isset($value["cement_brand"]["name"]) ? $value["cement_brand"]["name"] : (isset($value["cement_brand_name"]) ? $value["cement_brand_name"] : '') }}</span></p>
                                                                            <p>Coarse Sand Source : <span>{{ isset($value["sand_source"]["sand_source_name"]) ? $value["sand_source"]["sand_source_name"] : (isset($value["sand_source_name"]) ? $value["sand_source_name"] : '') }}</span></p>
                                                                            <p>Coarse Sand Zone : <span>{{ (isset($value["sand_zone_name"]) ? $value["sand_zone_name"] : '') }}</span></p>
                                                                            <p>Aggregate Source : <span>{{ isset($value["aggregate_source"]["aggregate_source_name"]) ? $value["aggregate_source"]["aggregate_source_name"] : (isset($value["agg_source_name"]) ? $value["agg_source_name"] : '') }}</span></p>
                                                                            <p>Admixture Brand : <span>{{ isset($value["admix_brand"]["name"]) ? $value["admix_brand"]["name"] : (isset($value["ad_mixture1_brand_name"]) ? $value["ad_mixture1_brand_name"] : '') }} - {{ isset($value["admix_cat"]["category_name"]) ? $value["admix_cat"]["category_name"] : (isset($value["ad_mixture1_category_name"]) ? $value["ad_mixture1_category_name"] : '') }}</span></p>
                                                                            <p>Fly Ash Source Name : <span>{{ (isset($value["fly_ash_source_name"]) ? $value["fly_ash_source_name"] : '') }}</span></p>
                                                                            <p>Water : <span>Regular</span></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td><span class="qty-text">{{ isset($value['quantity']) ? $value['quantity'] : '' }}</span></td>
                                                <!-- <td></td> -->
                                                <td> {{ isset($value['unit_price']) ? number_format($value['unit_price'],2) : '' }}</td>
                                                <td>
                                                    {{ ($value["with_TM"] == 'true') ? (($value["with_CP"] == 'true') ? 'TM & CP' : 'TM Only' ): "Buyer's TM" }}
                                                    @if(!$value["with_TM"])
                                                        <div class="dropdown tm_info_dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Buyer TM Info</a>
                                                            <div class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <div class="tm_info_bx">
                                                                    <h4>Buyer TM Detail</h4>
                                                                    <div class="tm_info_list"> <span>Delivery Date & Time :</span> 04 Jan 2021, 10:50 AM </div>
                                                                    <div class="tm_info_list"> <span>Transit Mixer No :</span> GJ-01-AA-0000 </div>
                                                                    <div class="tm_info_list"> <span>Driver Name :</span> Jhon </div>
                                                                    <div class="tm_info_list"> <span>Mobile No. :</span> +91 9979016486 </div>
                                                                    <div class="tm_info_list"> <span>TM Operator Name :</span> Jhondoe </div>
                                                                    <div class="tm_info_list"> <span>TM Operator Mobile No. :</span> +91 9979016486 </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </td>
                                                <!-- <td> {{ isset($value['TM_price']) ? number_format($value['TM_price'],2) : '' }}</td> -->
                                                <!-- <td> {{ isset($value['CP_price']) ? number_format($value['CP_price'],2) : '' }}</td> -->
                                                <td> {{ isset($value['selling_price']) ? number_format($value['selling_price'],2) : '' }}</td>
                                                <td> {{ isset($value['margin_price']) ? number_format($value['margin_price'],2) : '' }}</td>
                                                <!-- <td> {{ isset($value['gst_price']) ? number_format($value['gst_price'],2) : '' }}</td> -->
                                                <td> {{ isset($value['selling_price_With_Margin']) ? number_format($value['selling_price_With_Margin'],2) : '' }}</td>
                                                <td class="noowrp">
                                                    @if($value["item_status"] == 'DELIVERED')
                                                        <div class="badge bg-green">{{ str_replace("_"," ",$value["item_status"]) }}</div>
                                                    @else @if($value["item_status"] == 'CANCELLED' || $value["item_status"] == 'LAPSED' || $value["item_status"] == 'REJECTED')
                                                        <div class="badge bg-red">{{ str_replace("_"," ",$value["item_status"]) }}</div>
                                                        @if($value["item_status"] == 'LAPSED')
                                                            (Qty not assigned)
                                                        @endif
                                                    @else @if($value["item_status"] == 'PICKUP')
                                                        <span class="badge bg-purple">{{ $value["item_status"] }}</span>
                                                    @else
                                                        <div class="badge bg-yellow">{{ str_replace("_"," ",$value["item_status"]) }}</div>
                                                    @endif
                                                    @endif
                                                    @endif


                                                    <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                        <a href="javascript:;" class="site-button bg-grey button-sm button-sm-ctm1" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i></a>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <li>
                                                                <h3>Cancel By Admin :</h3>

                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                </p>
                                                            </li>
                                                        </ul>
                                                    </div> -->
                                                </td>
                                                <!-- <td>
                                                    <a href="javascript:void(0)" onclick="buyerItemTrack('{{ $value['_id'] }}')">Track</a>
                                                </td> -->
                                                <td>
                                                    <div class="dropdown cstm-dropdown-select">
                                                        <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                            <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <!-- <li><a class="dropdown-item" onclick="showOrderStatusDialog('item_status','{{ json_encode($value) }}')"  >Change Status</a></li> -->
                                                            <!-- <li><a class="dropdown-item"   >Change Payment Status</a></li> -->
                                                            @if(isset($value['vendor']))
                                                            <li><a class="dropdown-item" onclick="showSupplierDialog('{{ json_encode($value['vendor']) }}')">Contact Supplier</a></li>
                                                            @endif
                                                            <li><a class="dropdown-item" href="javascript:void(0)" onclick="buyerItemTrack('{{ $value['_id'] }}')">Track</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="14" class="inner_tbl">
                                                <?php $orderItemPartData = $value["orderItemPartData"];  ?>
                                                <div class="trip_driver_tbl">
                                                    <div class="comn-table latest-order-table">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Assigned Date</th>
                                                                    <th>Assigned Start Time</th>
                                                                    <th>Assigned End Time</th>
                                                                    <th>Deliver Qty</th>
                                                                    <th>CP Track</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(isset($orderItemPartData) && count($orderItemPartData) > 0)
                                                                <?php //dd($orderItemPartData); ?>
                                                                    @foreach($orderItemPartData as $value)
                                                                    <tr>
                                                                        <td>{{ date('d M Y', strtotime($value["start_time"])) }}</td>
                                                                        <td>{{ date('d M Y, h:i', strtotime($value["start_time"])) }}</td>
                                                                        <td>{{ date('d M Y, h:i', strtotime($value["end_time"])) }}</td>
                                                                        <td>{{ $value["assigned_quantity"] }} Cu. Mtr</td>
                                                                        <td>

                                                                            @if(!isset($value['CPtrack_details'][0]))
                                                                                <a href="javascript:;"  class="site-button dark button-sm text-nowrap">CP not assigned </a>
                                                                            @else
                                                                                <span class="text-success">CP Assigned</span>
                                                                                <a href="javascript:void(0)" onclick="buyerCPTrack('{{ $value['_id'] }}')">Track</a>
                                                                            @endif

                                                                        </td>


                                                                    </tr>
                                                                    @endforeach
                                                                @else
                                                                <tr>
                                                                    <td colspan="11">No Record Found</td>
                                                                </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                </td>
                                            </tr>

                                        @endforeach
                                        @endif

                                    </tbody>
                                </table>




                            </div>
                        </div>

                        <!-- ........................ -->
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection