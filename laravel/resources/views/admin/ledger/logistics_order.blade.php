@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Logistics Orders</h1>
    <div class="clearfix"></div>
    <!--orders list start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Logistics Orders List</h2>
                    <!--  <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button> -->
                </div>
                <div class="orders-block">
                    
                    <div class="orders-content-middle-block">
                        <div class="comn-table1 p-a0">
                            <table class="table">
                            <thead>
                                <tr>
                                <th>Order Id</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Qty</th>
                                <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                <th>Order Date</th>
                                <th>Delivery Status</th>
                                <!-- <th>Bill Status</th> -->
                                <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($data["data"]) && count($data['data']) > 0)
                                    @php
                                        $first_created_date = '';
                                        $last_created_date = '';
                                        $count = 0;
                                    @endphp
                                    @foreach($data["data"] as $value)
                                        @if($count == 0)
                                            @php 
                                                $first_created_date = $value["created_at"];
                                                $count++; 
                                            @endphp
                                        @endif
                                        @php
                                            $last_created_date = $value["created_at"];
                                        @endphp


                                        <tr>
                                            <td><a href="{{ route('admin_show_logisctic_order_details', $value['_id']) }}">#{{ $value["_id"] }}</a></td>
                                            <td>{{ $value["product_category"]["category_name"] }}</td>
                                            <td>{{ $value["product_sub_category"]["sub_category_name"] }}</td>
                                            <td>{{ $value["quantity"] }} MT</td>
                                            <td>{{ isset($value["total_amount"]) ? round($value["total_amount"]) : '' }}</td>
                                                <td>{{ date('d M Y', strtotime($value["created_at"])) }}</td>
                                            <td><div class="badge {{ $value['delivery_status'] == 'CANCELLED' ? 'bg-red' : 'bg-green' }}">{{ $value["delivery_status"] }}</div></td>
                                            <!-- <td><div class="badge "></div></td> -->
                                            <!-- <td>
                                                <div class="bil-action-block">
                                                    @if(isset($value["bill_path"]))
                                                        <div class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                            <a href="" class="site-button blue button-sm" data-tooltip="Accept Bill"><i class="fa fa-file-text-o"></i></a>
                                                        </div>
                                                    
                                                        <div id="gallery" class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                            <a href="{{ $value['bill_path'] }}" class="gal_link site-button blue button-sm" data-tooltip="Preview Bill"><i class="fa fa-eye"></i></a>
                                                        </div>

                                                        <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block">
                                                            <a href="{{ route('admin_show_supplier_order_details', $value['_id']) }}" class="site-button red button-sm" aria-expanded="true" data-tooltip="Reject Bill"><i class="fa fa-ban"></i></a>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                <li>
                                                                    <form action="" method="get">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <textarea class="form-control" placeholder="Write a Reason"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <button class="site-button blue">Submit</button>
                                                                    </form>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    @else
                                                        <span>Biil Not Uploaded Yet.</span>

                                                    @endif
                                                </div>
                                            </td> -->
                                        
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9" style="text-align: center;">No Record Found</td>
                                    </tr>
                                
                                @endif
                            
                                
                            </tbody>
                        </table>
                        </div>
                    </div>
                    
                </div>
                
                <div class="pagination-block m-t20">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->


                    @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)
                    <div class="pagination justify-content-end m-0">
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif 
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif
                                
                                <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                
                                @if(Request::get('order_status'))
                                    <input type="hidden" name="order_status" value="{{ Request::get('order_status') ? Request::get('order_status') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('payment_status'))
                                    <input type="hidden" value="{{ Request::get('payment_status') ? Request::get('payment_status') : '' }}" name="categoryId"/>
                                @endif
                               
                                @if(Request::get('user_id'))
                                    <input type="hidden" value="{{ Request::get('user_id') ? Request::get('user_id') : '' }}" name="user_id"/>
                                @endif
                                @if(Request::get('transaction_id'))
                                    <input type="hidden" value="{{ Request::get('transaction_id') ? Request::get('transaction_id') : '' }}" name="transaction_id"/>
                                @endif 
                                <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                        </div>
                        @endif

                </div>
            </div>
        </div>
    </div>
    <!--orders list end -->
</div>

@endsection