@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap">
            <h1 class="breadcrums">Users / <span>RMC Suppliers</span></h1>
            <div class="clearfix"></div>
            <!--vehicle category list start -->
            <div class="users-main-block wht-tble-bg">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="sub-title-block">
                        @if(isset($data["error"]))
                            <div class="">{{ $data["error"]["message"] }}</div>
                        @endif
                            <h2 class="sub-title pull-left">RMC Suppliers</h2>
                            <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                            <a href="{{ route('admin_show_suppliers_user') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                        </div>
                        <div class="bls-users-block">
                        @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data["data"]) && count($data['data']) > 0)
                                <?php //dd($data["data"]); ?>
                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php
                                            $first_created_date = $value["created_at"];
                                            $count++;
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp



                                    <div class="bls-users-contet-block m-b20" >

                                        <div class="row">
                                            @if($profile["rights"]["suppliers_actions"])
                                                <div class="user_action_div">
                                                    @if($profile["rights"]["suppliers_verification"])
                                                        @if($value['verified_by_admin'] == false)
                                                            <form action="" method="" name="supplier_verify_form_{{ $value['_id'] }}" id="supplier_verify_form_{{ $value['_id'] }}">
                                                                @csrf
                                                                <div class="verified-switch-btn mb-2">
                                                                    <label class="new-switch switch-green">

                                                                            <input type="hidden" name="supplier_id" value="{{ $value['_id'] }}" />
                                                                            <input type="checkbox" name="verified_by_admin" data-supplier-id="{{ $value['_id'] }}" id="supplier_verify_by_admin_{{ $value['_id'] }}" class="switch-input" {{ $value['verified_by_admin'] == true ? 'checked' : '' }}>
                                                                            <span class="switch-label" data-on="Verified" data-off="Unverified"></span>
                                                                            <span class="switch-handle"></span>

                                                                    </label>
                                                                </div>
                                                            </form>
                                                        @endif
                                                    @endif
                                                    @if($profile["rights"]["suppliers_rejection"])
                                                        @if($value['verified_by_admin'] == false)
                                                            <button class="site-button red button-sm mb-1" onclick="rejectPopUp('{{ $value['_id'] }}')">Reject</button>
                                                        @endif
                                                    @endif
                                                    @if(($profile["rights"]["suppliers_unblock"]) || ($profile["rights"]["suppliers_block"]))
                                                        @if($value['verified_by_admin'] == true)
                                                            <form action="" method="" name="supplier_block_form_{{ $value['_id'] }}" id="supplier_block_form_{{ $value['_id'] }}">
                                                            @csrf
                                                                <div class="verified-switch-btn mb-2">
                                                                    <label class="new-switch switch-red">
                                                                        <input type="hidden" name="supplier_id" value="{{ $value['_id'] }}" />
                                                                        <input type="checkbox" name="blocked_by_admin" data-supplier-id="{{ $value['_id'] }}" id="supplier_blocked_by_admin_{{ $value['_id'] }}" class="switch-input" {{ $value['is_blocked'] == true ? 'checked' : '' }}>
                                                                        <span class="switch-label" data-on="Block" data-off="Unblock"></span>
                                                                        <span class="switch-handle"></span>
                                                                    </label>
                                                                </div>
                                                            </form>
                                                        @endif
                                                    @endif
                                                </div>
                                                <script>
                                                    $('#supplier_verify_by_admin_{{ $value['_id'] }}').change(function() {
                                                        $is_checked = '';
                                                        

                                                        var supplier_id = $(this).attr("data-supplier-id");

                                                        request_data = $('#supplier_verify_form_'+supplier_id).serialize();
                                                        console.log(request_data);
                                                        if(this.checked) {
                                                            $is_checked = 'true';
                                                            $('#supplier_verify_by_admin_{{ $value['_id'] }}').prop("checked",false);
                                                        }else{
                                                            $is_checked = 'false';
                                                            $('#supplier_verify_by_admin_{{ $value['_id'] }}').prop("checked",true);
                                                        }
                                                        // supplierVerifyByAdmin(request_data);
                                                        requestOTP(37,request_data,"","vendor_verification");

                                                    });

                                                    $('#supplier_blocked_by_admin_{{ $value['_id'] }}').change(function() {
                                                        $is_checked = '';
                                                        var event = "account_unblock";
                                                        

                                                        var supplier_id = $(this).attr("data-supplier-id");

                                                        request_data = $('#supplier_block_form_'+supplier_id).serialize();
                                                        console.log(request_data);

                                                        if(this.checked) {
                                                            $is_checked = 'true';
                                                            var event = "account_block";
                                                            $('#supplier_blocked_by_admin_'+supplier_id).prop("checked",false);
                                                        }else{
                                                            $is_checked = 'false';
                                                            $('#supplier_blocked_by_admin_'+supplier_id).prop("checked",true);
                                                        }

                                                        // supplierVerifyByAdmin(request_data);
                                                        requestOTP(38,request_data,"",event);

                                                    });
                                                </script>
                                            @endif
                                            <!-- <a href="javascript:;" class="site-button outline gray pull-right top-right-link-icon" data-tooltip="Dashboard"><i class="fa fa-dashboard"></i></a> -->
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <h3 class="bls-users-title">
                                                    @if($value["company_type"] == "Individual")
                                                        {{ isset($value["full_name"]) ? $value["full_name"] : '' }}
                                                    @else
                                                        {{ isset($value["company_name"]) ? $value["company_name"] : '' }}
                                                    @endif
                                                </h3>
                                                <ul>
                                                    <!-- <li>
                                                        <p><span class="vTitle">User Id : </span> <span class="vName">#{{ isset($value["user_id"]) ? $value["user_id"] : '' }}</span></p>
                                                    </li> -->
                                                    @if(isset($value["company_type"]))
                                                     <li>
                                                        <p><span class="vTitle">Company Type : </span> <span class="vName">{{ $value["company_type"] }}</span></p>
                                                    </li>
                                                    @endif
                                                    @if(isset($value["full_name"]))
                                                    <li>
                                                        <p><span class="vTitle">Person Name : </span> <span class="vName">{{ $value["full_name"] }}</span></p>
                                                    </li>
                                                    @endif

                                                    <li>
                                                        <p><span class="vTitle">Mobile No. : </span> <span class="vName">{{ isset($value["mobile_number"]) ? $value["mobile_number"] : '' }}</span></p>
                                                    </li>

                                                    @if(isset($value["landline_number"]))
                                                     <li>
                                                        <p><span class="vTitle">Landline No. : </span> <span class="vName">{{ isset($value["landline_number"]) ? $value["landline_number"] : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                    <li>
                                                        <p><span class="vTitle">Email : </span> <span class="vName">{{ isset($value["email"]) ? $value["email"] : '' }}</span></p>
                                                    </li>
                                                      <!-- <li>
                                                        <p><span class="vTitle">Region Served : </span> <span class="vName">Gujarat , Rajasthan</span></p>
                                                    </li> -->

                                                      <li>
                                                        <p><span class="vTitle">Company Registration No : </span> <span class="vName">{{ isset($value["company_certification_number"]) ? $value["company_certification_number"] : '' }}</span></p>
                                                    </li>
                                                    <li>
                                                        <p><span class="vTitle">PAN No : </span> <span class="vName">{{ isset($value["pan_number"]) ? $value["pan_number"] : '' }}</span></p>
                                                    </li>
                                                  <!--   <li>
                                                        <p><span class="vTitle">Account Type: </span> <span class="vName">{{ isset($value["account_type"]) ? $value["account_type"] : '' }}</span></p>
                                                    </li> -->
                                                    @if(isset($value["gst_number"]))
                                                    <li>
                                                        <p><span class="vTitle">GST No : </span> <span class="vName">{{ isset($value["gst_number"]) ? $value["gst_number"] : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                    @if(isset($value["signup_type"]))
                                                    <li>
                                                        <p><span class="vTitle">Signup Type : </span> <span class="vName text-capitalize">{{ isset($value["signup_type"]) ? $value["signup_type"] : '' }}</span></p>
                                                    </li>
                                                    @endif



                                                    <li>
                                                        <p><span class="vTitle">Verified by Admin : </span> <span class="vName"> 
                                                            @if(isset($value["verified_by_admin"]) && $value["verified_by_admin"] == true)
                                                                <i class='{{ isset($value["verified_by_admin"]) ? ($value["verified_by_admin"] ? "fa fa-check text-green" : "fa fa-close text-red") : "" }}'></i>
                                                            @else
                                                                Unverified
                                                            @endif
                                                        </span></p>
                                                    </li>
                                                    @if(isset($value["annual_turnover"]))
                                                    <li>
                                                        <p><span class="vTitle">Annual Turn Over : </span> <span class="vName">{{ isset($value["annual_turnover"]) ? $value["annual_turnover"] : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                    @if(isset($value["trade_capacity"]))
                                                    <li>
                                                        <p><span class="vTitle">Material Capacity : </span> <span class="vName">{{ isset($value["trade_capacity"]) ? $value["trade_capacity"] : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                    @if(isset($value["number_of_employee"]))
                                                    <li>
                                                        <p><span class="vTitle">Number of Employee : </span> <span class="vName">{{ isset($value["number_of_employee"]) ? $value["number_of_employee"] : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                    <!-- @if(isset($value["company_certification_number"]))
                                                    <li>
                                                        <p><span class="vTitle">ISO Certificate : </span> <span class="vName">{{ isset($value["company_certification_number"]) ? $value["company_certification_number"] : '' }}</span></p>
                                                    </li>
                                                    @endif -->
                                                    @if(isset($value["created_at"]))
                                                    <li>
                                                        <p><span class="vTitle">Register On : </span> <span class="vName">{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span></p>
                                                    </li>
                                                    @endif
                                                    <!-- <li>
                                                        <div id="gallery">
                                                            @if(isset($value["company_certification_image_url"]))
                                                                <a href="{{ $value["company_certification_image_url"] }}" class="vehicle-img-block gal_link"><img src="{{ $value["company_certification_image_url"] }}"></a>
                                                            @endif
                                                            @if(isset($value["pancard_image_url"]))
                                                                <a href="{{ $value["pancard_image_url"] }}" class="vehicle-img-block gal_link"><img src="{{ $value["pancard_image_url"] }}"></a>
                                                            @endif
                                                            @if(isset($value["gst_certification_image_url"]))
                                                                <a href="{{ $value["gst_certification_image_url"] }}" class="vehicle-img-block gal_link"><img src="{{ $value["gst_certification_image_url"] }}"></a>
                                                            @endif
                                                        </div>
                                                    </li> -->
                                                    @if(isset($value["working_with_other_ecommerce"]))
                                                    <li>
                                                        <p><span class="vTitle">Other Ecommerce Affiliation : </span> <span class="vName"><i class='{{ isset($value["working_with_other_ecommerce"]) ? ($value["working_with_other_ecommerce"] ? "fa fa-check text-green" : "fa fa-close text-red") : "" }}'></i></span></p>
                                                    </li>
                                                    @endif


                                                </ul>

                                                <div class="document_list">
                                                    @if(isset($value["company_certification_image_url"]))
                                                        @if(strpos($value["company_certification_image_url"], '.doc') || strpos($value["company_certification_image_url"], '.docx'))
                                                            <a href="{{ $value['company_certification_image_url'] }}" class="document_box docfile" target="_blank">
                                                                <i class="fa fa-file-word-o"></i>
                                                                <span>Company Certificate</span>
                                                            </a>
                                                        @endif

                                                        @if(strpos($value["company_certification_image_url"], '.jpg') || strpos($value["company_certification_image_url"], '.png') || strpos($value["company_certification_image_url"], '.jpeg'))
                                                            <a href="{{ $value['company_certification_image_url'] }}" class="document_box gal_link">
                                                                <i class="fa fa-image"></i>
                                                                <span>Company Certificate</span>
                                                            </a>

                                                        @endif

                                                        @if(strpos($value["company_certification_image_url"], '.pdf'))
                                                            <a href="{{ $value['company_certification_image_url'] }}" class="document_box pdffile" target="_blank">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                                <span>Company Certificate</span>
                                                            </a>
                                                        @endif
                                                    @endif

                                                    @if(isset($value["pancard_image_url"]))
                                                        @if(strpos($value["pancard_image_url"], '.doc') || strpos($value["pancard_image_url"], '.docx'))
                                                            <a href="{{ $value['pancard_image_url'] }}" class="document_box docfile" target="_blank">
                                                                <i class="fa fa-file-word-o"></i>
                                                                <span>Pancard</span>
                                                            </a>
                                                        @endif

                                                        @if(strpos($value["pancard_image_url"], '.jpg') || strpos($value["pancard_image_url"], '.png') || strpos($value["pancard_image_url"], '.jpeg'))
                                                            <a href="{{ $value['pancard_image_url'] }}" class="document_box gal_link">
                                                                <i class="fa fa-image"></i>
                                                                <span>Pancard</span>
                                                            </a>

                                                        @endif

                                                        @if(strpos($value["pancard_image_url"], '.pdf'))
                                                            <a href="{{ $value['pancard_image_url'] }}" class="document_box pdffile" target="_blank">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                                <span>Pancard</span>
                                                            </a>
                                                        @endif
                                                    @endif

                                                    @if(isset($value["gst_certification_image_url"]))
                                                        @if(strpos($value["gst_certification_image_url"], '.doc') || strpos($value["gst_certification_image_url"], '.docx'))
                                                            <a href="{{ $value['gst_certification_image_url'] }}" class="document_box docfile" target="_blank">
                                                                <i class="fa fa-file-word-o"></i>
                                                                <span>GST Certificate</span>
                                                            </a>
                                                        @endif

                                                        @if(strpos($value["gst_certification_image_url"], '.jpg') || strpos($value["gst_certification_image_url"], '.png') || strpos($value["gst_certification_image_url"], '.jpeg'))
                                                            <a href="{{ $value['gst_certification_image_url'] }}" class="document_box gal_link">
                                                                <i class="fa fa-image"></i>
                                                                <span>GST Certificate</span>
                                                            </a>

                                                        @endif

                                                        @if(strpos($value["gst_certification_image_url"], '.pdf'))
                                                            <a href="{{ $value['gst_certification_image_url'] }}" class="document_box pdffile" target="_blank">
                                                                <i class="fa fa-file-pdf-o"></i>
                                                                <span>GST Certificate</span>
                                                            </a>
                                                        @endif
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                        @if($value['verified_by_admin'] == true && $value['plant_cnt'] > 0 )
                                            <div class="user_btm_view_plant"><a href="javascript:void(0)" onclick="getSubVendorPlants('{{ $value["_id"] }}')">+ See Plants ({{ $value['plant_cnt'] }})</a></div>
                                        @endif
                                    </div>
                                @endforeach
                           @else
                               <p style="text-align: center;">No Record Found</p>
                            @endif


                            <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                            <div class="pagination-block">
                                <!-- <ul class="pagination justify-content-end m-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                    </li>
                                </ul>
                                <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->

                                    <div class="pagination justify-content-end m-0">
                                    @if($is_previous_avail == 1)
                                        @if(isset($first_created_date))
                                            <form action="{{ route(Route::current()->getName()) }}" method="get">

                                                @if(Request::get('name'))
                                                    <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('user_id'))
                                                    <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('mobile_no'))
                                                    <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('email'))
                                                    <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('pan_number'))
                                                    <input type="hidden" name="pan_number" value="{{ Request::get('pan_number') ? Request::get('pan_number') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('company_type'))
                                                    <input type="hidden" name="company_type" value="{{ Request::get('company_type') ? Request::get('company_type') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('verified_by_admin'))
                                                    <input type="hidden" name="verified_by_admin" value="{{ Request::get('verified_by_admin') ? Request::get('verified_by_admin') : ''  }}" class="form-control" placeholder="Name">
                                                @endif

                                                <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                                <button type="submit" class="site-button">Previous</button>
                                            </form>
                                        @endif
                                        @endif
                                        @if($is_next_avail == 1)
                                        @if(isset($last_created_date))
                                            <form action="{{ route(Route::current()->getName()) }}" method="get">

                                                @if(Request::get('name'))
                                                    <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('user_id'))
                                                    <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('mobile_no'))
                                                    <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('email'))
                                                    <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('pan_number'))
                                                    <input type="hidden" name="pan_number" value="{{ Request::get('pan_number') ? Request::get('pan_number') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('company_type'))
                                                    <input type="hidden" name="company_type" value="{{ Request::get('company_type') ? Request::get('company_type') : ''  }}" class="form-control" placeholder="Name">
                                                @endif
                                                @if(Request::get('verified_by_admin'))
                                                    <input type="hidden" name="verified_by_admin" value="{{ Request::get('verified_by_admin') ? Request::get('verified_by_admin') : ''  }}" class="form-control" placeholder="Name">
                                                @endif

                                            <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                                <button type="submit" class="site-button">Next</button>
                                            </form>
                                        @endif
                                        @endif
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--vehicle category list end -->
        </div>

@endsection