@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums"><a href="report.html">Reports</a> / <span>Reports Details</span></h1>
    <div class="clearfix"></div>
    <div class="review-complaints-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Product Detail Report</h2>
                    <button id="filterCollapse" class="site-button pull-right m-r10"><i class="fa fa-filter"></i></button>
                    <div class="dropdown cstm-dropdown-select exprt-drpdwn pull-right">
                        <button class="site-button m-r10 icn-btn" type="button" data-toggle="dropdown" aria-expanded="true"><span class="dropdown-label">Export <i class="fa fa-download"></i></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                            <li><a class="dropdown-item" href="javascript:;">PDF</a></li>
                            <li><a class="dropdown-item" href="javascript:;">Excel</a></li>
                        </ul>
                    </div>
                </div>
                <div class="reports_details_block">
                    <div class="comn-table1 pull-left w-100">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Registered On</th>
                                     <th>Product Name</th>
                                     <th>Source Name</th>
                                     <th>Min. Order Acceptance</th>
                                     <th>Unit Price - (For 1MT) (<i class="fa fa-rupee"></i>)</th>
                                     <th>Stock as on Today</th>
                                     <th>Contact Person</th>
                                     <th>Pickup Address </th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>20 Aug 2020 05:25:18 am</td>
                                    <td>Aggregate - 10 mm</td>
                                    <td>Sevalia</td>
                                    <td>50</td>
                                    <td>750</td>
                                    <td>1001</td>
                                    <td>Yogendra Joshi - +917016126770</td>
                                    <td>104, Sumel-­2
S.G.Highway Road, Thaltej, beside Gurudwara Temple, Ahmedabad 380054, Gujarat</td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection