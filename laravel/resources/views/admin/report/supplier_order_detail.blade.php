@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums"><a href="orders.html">Order</a> / <span>Order Details</span></h1>
    <div class="clearfix"></div>
    <!--order Details start -->
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Order Details</h2>
                </div>
                
                <div class="odrs-details-block p-a0">
                <?php //dd($data['data']); ?>
                    @if(isset($data['data']))
                        <?php $data = $data["data"]; ?>
                        <?php $order_data = $data["order"]; ?>
                        <?php $buyer_data = $data["buyer"]; ?>
                        <?php $product_data = $data["product"]; ?>
                        <?php $order_item_data = $data["orderItem"]; ?>
                        <?php $orderTrackDetails = $data["orderTrackData"]; ?>
                    <div class="orders-content-block">
                        <div class="orders-content-head-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-id-date-block">
                                        <div class="ordlst-sd-text-block spa">
                                            Order Id: <span>#{{ $data["_id"] }}</span>
                                        </div>
                                        <!-- <div class="ordlst-id-block">
                                            Invoice Id: <a href="#">10090042</a>
                                        </div> -->
                                        <div class="ordlst-date-block">
                                            Order Date: <span>{{ date('d M Y, h:i:s a', strtotime($data["created_at"])) }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Buyer Name: <span>{{ $buyer_data["full_name"] }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Logistic Name: <span>{{ isset($data["logistics_user"]["full_name"]) ? $data["logistics_user"]["full_name"] : 'Not Assigned' }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-ship-dlvry-date-block">
                                        <div class="ordlst-sd-text-block">
                                            Payment Status: <span class="badge {{ $data['payment_status'] == 'PAID' ? 'bg-green' : 'bg-red' }} ">{{ $data["payment_status"] }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Total Amount: <span class="rupee-text"><i class="fa fa-rupee"></i> {{ round($data["total_amount"]) }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Pickup Location: 
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($product_data['pickup_location']['coordinates'][1]) ? $product_data['pickup_location']['coordinates'][1] : '' }}','{{ isset($product_data['pickup_location']['coordinates'][0]) ? $product_data['pickup_location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                    {{ isset($product_data["pickup_address"]['address_line1']) ? $product_data["pickup_address"]['address_line1'] : '' }}, 
                                                    {{ isset($product_data["pickup_address"]['address_line2']) ? $product_data["pickup_address"]['address_line2'] : '' }}, 
                                                    {{ isset($product_data["pickup_address"]['state_name']) ? $product_data["pickup_address"]['state_name'] : '' }}, 
                                                    {{ isset($product_data["pickup_address"]['city_name']) ? $product_data["pickup_address"]['city_name'] : '' }} - {{ isset($product_data["pickup_address"]['pincode']) ? $product_data["pickup_address"]['pincode'] : '' }}, 
                                                    
                                                </span>
                                            </span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Delivery Location: 
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon1" onclick="viewMap('{{ isset($order_data['delivery_location']['coordinates'][1]) ? $order_data['delivery_location']['coordinates'][1] : '' }}','{{ isset($order_data['delivery_location']['coordinates'][0]) ? $order_data['delivery_location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                {{ isset($order_data['address_line1']) ? $order_data['address_line1'] : '' }}, 
                                                    {{ isset($order_data['address_line2']) ? $order_data['address_line2'] : '' }}, 
                                                    {{ isset($order_data['state_name']) ? $order_data['state_name'] : '' }}, 
                                                    {{ isset($order_data['city_name']) ? $order_data['city_name'] : '' }} - {{ isset($order_data['pincode']) ? $order_data['pincode'] : '' }}, 
                                                </span>
                                            </span>
                                        </div>
                                        

                                        
                                    </div> 
                                    <!--     <div class="odr-select-optin-block">                                  <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="dropdown-label">Select Invoice <i class="fa fa-sort-down"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li><a class="dropdown-item" href="invoice.html">View invoice</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Download invoice</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#sendemil-sts-popup">Send invoice to email</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Download purchase order</a></li>
                                                    <li><a class="dropdown-item" href="javascript:;">Send purchase order to email</a></li>
                                                </ul>  
                                            </div>
                                        </div> -->
                                        @if(isset($data["bill_path"]))
                                            <!-- <div class="dropdown cstm-dropdown-select bid-rate-dropdown" style="display: block;display: block;position: absolute; right: 10px; top: 0;">

                                                <a href="javascript:;" onclick="updateBillStatus('1','{{ $data['_id']}}','{{ csrf_token() }}','')" class="site-button green button-sm" >Accept Bill</a>
                                                <div class="dropdown cstm-dropdown-select edit-bill-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm" data-toggle="dropdown" aria-expanded="false" data-tooltip="Reject Bill"><i class="fa fa-ban"></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <form action="" method="get">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <textarea class="form-control" id="bill_status_reason" placeholder="Write a Reason"></textarea>
                                                                    </div>
                                                                </div>
                                                                <button onclick="updateBillStatus('2','{{ $data['_id']}}','{{ csrf_token() }}','bill_status_reason')" class="site-button blue">Submit</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div id="gallery" class="dropdown cstm-dropdown-select viw-bill-ddblk dis-inline-block">
                                                <a href="{{ $data['bill_path'] }}" class="gal_link site-button blue button-sm" data-tooltip="View Bill"><i class="fa fa-eye"></i></a>
                                                    <div class="dropdown cstm-dropdown-select order_dtl_action dis-inline-block">
                                                        <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                            <i class="fa fa-eye"></i>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                            <li><a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 <i class="fa fa-times-circle"></i></a></li>
                                                            <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                        </ul>
                                                    </div>

                                                </div> -->
                                            @endif
                                        <?php //dd($data); ?>
                                        @if(!empty($data["bills"]) || !empty($data["creditNotes"]) || !empty($data["debitNotes"]))
                                            <div class="dropdown cstm-dropdown-select bid-rate-dropdown" style="display: block;display: block;position: absolute; right: 10px; top: 0;">
                                        <div class="dropdown cstm-dropdown-select order_dtl_action dis-inline-block">
                                                    <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                        <i class="fa fa-file-text-o"></i> View Bill
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
<!--................................ Invoice Bill........................................................................  -->
                                                    @if(isset($data["bills"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["bills"] as $bill_value)

                                                                <?php 
                                                                    $file_type_class = ''; 
                                                                    $file_type_fa_class = ''; 
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php 
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php 
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>
                                                                    
                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php 
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>
                                                                    
                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php 
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')
                                                                    
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Invoice {{ $biils_count++ }} 
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:;" onclick="updateBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','','invoice')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason_{{ $bill_value['_id'] }}" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <a href="javascript:;" onclick="updateBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id'] }}','{{ csrf_token() }}','bill_status_reason_{{ $bill_value['_id'] }}','invoice')" class="site-button blue">Submit</a>           
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Invoice {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')
                                                                
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Invoice {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach

                                                        
                                                        @endif

<!--................................ Invoice Bill........................................................................  -->


<!--................................ credit Notes Bill........................................................................  -->
                                                    @if(isset($data["creditNotes"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["creditNotes"] as $bill_value)

                                                                <?php 
                                                                    $file_type_class = ''; 
                                                                    $file_type_fa_class = ''; 
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php 
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php 
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>
                                                                    
                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php 
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>
                                                                    
                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php 
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')
                                                                    
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Credit Note {{ $biils_count++ }} 
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:;" onclick="updateBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','','credit_notes')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason_{{ $bill_value['_id'] }}" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <a href="javascript:;" onclick="updateBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','bill_status_reason_{{ $bill_value['_id'] }}','credit_notes')" class="site-button blue">Submit</a>           
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Credit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')
                                                                
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Credit Note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach

                                                        
                                                        @endif

<!--................................ credit Notes Bill........................................................................  -->

<!--................................ debit Notes Bill........................................................................  -->
                                                    @if(isset($data["debitNotes"]))
                                                            <?php $biils_count = 1; ?>
                                                            @foreach($data["debitNotes"] as $bill_value)

                                                                <?php 
                                                                    $file_type_class = ''; 
                                                                    $file_type_fa_class = ''; 
                                                                ?>

                                                                @if(str_contains($bill_value["url"], '.jpg'))
                                                                    <?php 
                                                                        $file_type_class = 'jpg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>
                                                                @elseif(str_contains($bill_value["url"], '.doc') || str_contains($bill_value["url"], '.docx'))

                                                                    <?php 
                                                                        $file_type_class = 'docfile';
                                                                        $file_type_fa_class = 'fa-file-word-o'
                                                                    ?>
                                                                    
                                                                @elseif(str_contains($bill_value["url"], '.pdf'))

                                                                    <?php 
                                                                        $file_type_class = 'pdffile';
                                                                        $file_type_fa_class = 'fa-file-pdf-o'
                                                                    ?>
                                                                    
                                                                @elseif(str_contains($bill_value["url"], '.jpeg'))

                                                                    <?php 
                                                                        $file_type_class = 'jpeg';
                                                                        $file_type_fa_class = 'fa-image'
                                                                    ?>

                                                                @endif

                                                                @if($bill_value["status"] == 'PENDING')
                                                                    
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="{{ $bill_value['url'] }}"><i class="fa {{ $file_type_fa_class }}">
                                                                            </i> Debit note {{ $biils_count++ }} 
                                                                        </a>
                                                                        <div class="acept_rej_btn">
                                                                            <!-- <span class="badge bg-yellow">{{ $bill_value["status"] }}</span> -->

                                                                            <a href="javascript:void(0);" onclick="updateBillStatus('1','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','','debit_notes')" class="site-button green button-sm" >Accept</a>
                                                                            <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                <li class="p-a15">
                                                                                    <!-- <form action="" method="get"> -->
                                                                                        <div class="form-group">
                                                                                            <div class="input-group">
                                                                                                <textarea class="form-control" id="bill_status_reason_{{ $bill_value['_id'] }}" placeholder="Write a Reason"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                        <!-- <button class="site-button blue">Submit</button> -->
                                                                                        <a href="javascript:void(0);" onclick="updateBillStatus('2','{{ $data['_id'] }}','{{ $bill_value['_id']}}','{{ csrf_token() }}','bill_status_reason_{{ $bill_value['_id'] }}','debit_notes')" class="site-button blue">Submit</a>           
                                                                                    <!-- </form> -->
                                                                                </li>
                                                                            </ul>

                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'ACCEPTED')

                                                                    <li>
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Debit note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-green">Accepted</span>
                                                                        </div>
                                                                    </li>

                                                                @elseif($bill_value["status"] == 'REJECTED')
                                                                
                                                                    <li class="dropdown-submenu">
                                                                        <a class="dropdown-item {{ $file_type_class }}" href="#"><i class="fa {{ $file_type_fa_class }}"></i> Debit note {{ $biils_count++ }} </i></a>
                                                                        <div class="acept_rej_btn">
                                                                            <span class="badge bg-red">Rejected</span>
                                                                            <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                                <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                                    <li class="p-a15">
                                                                                        <h3>Rejected By Admin :</h3>
                                                                                        <p>
                                                                                            {{ isset($bill_value["reject_reason"]) ? $bill_value["reject_reason"] : '' }}
                                                                                        </p>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                @endif
                                                            @endforeach

                                                        
                                                        @endif

<!--................................ debit Notes Bill........................................................................  -->
                                                        <!-- <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 </a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-green">Accept</span>
                                                                <a href="#" class="badge bg-red dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true">Reject</a>
                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <li class="p-a15">
                                                                        <form action="" method="get">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <textarea class="form-control" placeholder="Write a Reason"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <button class="site-button blue">Submit</button>
                                                                        </form>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3</i></a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-green">Accepted</span>
                                                            </div>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3</i></a>
                                                            <div class="acept_rej_btn">
                                                                <span class="badge bg-red">Rejected</span>
                                                                <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm dropdown-submenu-link" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="option-post-cleanup">
                                                                        <li class="p-a15">
                                                                            <h3>Rejected By Admin :</h3>
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                            </p>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                        <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li> -->
                                                    </ul>
                                                </div>


                                            <!-- <a href="javascript:;" class="site-button blue dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Upload Bill</a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                <li>
                                                    <form action="" method="get">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="file-browse">
                                                                    <span class="button-browse">
                                                                        Browse <input type="file">
                                                                    </span>
                                                                    <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button class="site-button blue">Upload</button>
                                                    </form>
                                                </li>
                                            </ul> -->

                                                <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <h3>Admin By :</h3>
                                                            <p>
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div> -->
                                        </div>
                                        @endif


                                                <!-- <a href="javascript:;" class="site-button blue dropdown-toggle button-sm" data-toggle="dropdown" aria-expanded="true">Upload Bill</a>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li>
                                                        <form action="" method="get">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="file-browse">
                                                                        <span class="button-browse">
                                                                            Browse <input type="file">
                                                                        </span>
                                                                        <input type="text" class="form-control browse-input" placeholder="e. g. pdf, jpg" readonly="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button class="site-button blue">Upload</button>
                                                        </form>
                                                    </li>
                                                </ul> -->

                                                <!-- <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                    <a href="javascript:;" class="site-button red button-sm button-sm-ctm" data-toggle="dropdown" aria-expanded="true" title="Rejected Bill" ><i class="fa fa-info-circle" ></i></a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        <li>
                                                            <h3>Admin By :</h3>
                                                            <p>
                                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div> -->
                                            </div>
                                        
                                </div>
                            </div>
                            <div class="orders-content-middle-block">
                                <div class="orders-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Category Name</th>
                                                <th>Qty</th>
                                                
                                                
                                                <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                                <th>Base Amount ( <i class="fa fa-rupee"></i> )</th>
                                                <th>GST Amount ( <i class="fa fa-rupee"></i> )</th>
                                                <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                                <th>Order Status</th>
                                                <!-- <th>Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($order_item_data))
                                                @foreach($order_item_data as $value)
                                            <tr>
                                                <td>
                                                    <div class="cate_img"><img src="{{ isset($value['product_category_image']) ? $value['product_category_image'] : '' }}"></div>
                                                </td>
                                                <td>{{ $value['product_category_name'] }} - {{ $value['product_sub_category_name'] }}</td>
                                                <td><span class="qty-text">{{ $value['quantity'] }} MT</span></td>
                                                
                                                
                                                <td> {{ round($value['unit_price']) }}</td>
                                                <td> {{ round($value['base_amount']) }}</td>
                                                <td> {{ round($value['igst_amount']) }}</td>
                                                <td> {{ round($value['total_amount']) }}</td>
                                                <td class="noowrp">

                                                    @if($value["item_status"] == 'DELIVERED')
                                                        <div class="badge bg-green">{{ $value["item_status"] }}</div>
                                                    @else @if($value["item_status"] == 'CANCELLED')
                                                        <div class="badge bg-red">{{ $value["item_status"] }}</div>

                                                        @else
                                                            <div class="badge bg-yellow">{{ $value["item_status"] }}</div>
                                                        @endif
                                                    @endif

                                                    <!-- <div class="badge {{ $value['item_status'] == 'NONSCHDULED' ? 'bg-red' : 'bg-green' }}">{{ $value['item_status'] }}</div>
                                                    <div class="dropdown cstm-dropdown-select adm-info-ddblk dis-inline-block">
                                                        <a href="javascript:;" class="site-button bg-grey button-sm button-sm-ctm1" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-info-circle"></i></a>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <li>
                                                                <h3>Admin By :</h3>
                                                                <p>
                                                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus sunt debitis praesentium. Accusantium neque, labore adipisci, aspernatur laborum, sint ullam iste doloremque dolorum et tempore laboriosam soluta tenetur cupiditate sit earum excepturi?
                                                                </p>
                                                            </li>
                                                        </ul>
                                                    </div> -->
                                                </td>
                                                <!-- <td>
                                                    <div class="dropdown cstm-dropdown-select">
                                                        <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                            <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                            <li><a class="dropdown-item" onclick="showOrderStatusDialog('item_status','{{ json_encode($value) }}')"  >Change Status</a></li>
                                                            <li><a class="dropdown-item" onclick="showSupplierDialog('')">Contact Supplier</a></li>
                                                            <li><a class="dropdown-item" href="{{ url('admin/orders/track/'.$value['_id']) }}">Track</a></li> 
                                                        </ul>
                                                    </div>
                                                </td> -->
                                            </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>


                                    <div class="trip_driver_tbl">
                                        <div class="comn-table latest-order-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Category</th>
                                                        <th>Vehical No</th>
                                                        <th>Driver Detail</th>
                                                        <th>Assign Date</th>
                                                        <th>Deliver Qty</th>
                                                        <th>Delivery Status</th>
                                                        <th>Track</th>
                                                        <!-- <th>View Bill</th> -->
                                                        <!-- <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($orderTrackDetails) && count($orderTrackDetails) > 0)
                                                <?php //dd($orderTrackDetails); ?>
                                                    @foreach($orderTrackDetails as $value)
                                                    <tr>
                                                        <td>{{ $value["category_name"] }}</td>
                                                        <td>{{ $value["vehicle_rc_number"] }}</td>
                                                        <td>{{ $value["driver_name"] }} - {{ $value["driver_mobile_number"] }}</td>
                                                        <td>{{ date('d M Y', strtotime($value["assigned_at"])) }}</td>
                                                        <td>{{ $value["pickup_quantity"] }} {{ $order_item_data[0]["quantity_unit"] }}</td>
                                                        <td>
                                                        @if($value["event_status"] == 'DELIVERED')
                                                            <div class="badge bg-green">{{ $value["event_status"] }}</div>
                                                        @else @if($value["event_status"] == 'CANCELLED')
                                                            <div class="badge bg-red">{{ $value["event_status"] }}</div>

                                                            @else
                                                                <div class="badge bg-yellow">{{ $value["event_status"] }}</div>
                                                            @endif
                                                        @endif
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)" onclick="supplierItemTrack('{{ $value['order_item_id'] }}','{{ $value['_id'] }}')">Track</a>
                                                        </td>
                                                        <!-- <td>  <a href="" class="gal_link site-button blue button-sm" data-tooltip="View Bill"><i class="fa fa-eye"></i></a></td> -->
                                                        <!-- <td>
                                                            <div class="dropdown cstm-dropdown-select order_dtl_action">
                                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                                    <i class="fa fa-eye"></i>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                                    <li><a class="dropdown-item docfile" href="#"><i class="fa fa-file-word-o"></i> Action1 <i class="fa fa-times-circle"></i></a></li>
                                                                    <li><a class="dropdown-item" href="#"><i class="fa fa-image"></i> Action2 <i class="fa fa-times-circle"></i></a></li>
                                                                    <li><a class="dropdown-item pdffile" href="#"><i class="fa fa-file-pdf-o"></i> Action3 <i class="fa fa-times-circle"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </td> -->
                                                    </tr>
                                                    @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="11">No Record Found</td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--order Details end -->
</div>

<script>
    $(document).ready(function(){
        $('.dropdown-submenu a.dropdown-submenu-link').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
</script>

@endsection