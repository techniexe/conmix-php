@extends('admin.layouts.admin_layout')

@section('content')
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap">
    <h1 class="main-title">Reports</h1>
    <div class="clearfix"></div>
    <div class="review-complaints-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Reports List</h2>
                </div>
                <div class="reports_block">
                    @if($profile["rights"]["inventory_report_view"])
                        <div class="reportsList_box">
                        
                            <h3>Inventory & Users</h3>
                            <ul>
                                
                                <li><a href="{{ route('admin_admix_brand_report') }}">Admixture Brand</a></li>
                                <li><a href="{{ route('admin_admix_types_report') }}">Admixture Type</a></li>
                                <li><a href="{{ route('admin_agg_sand_cat_report') }}">Aggregate Sand Category</a></li>
                                <li><a href="{{ route('admin_agg_sand_subcategory_report') }}">Aggregate Sand Sub Category</a></li>
                                <li><a href="{{ route('admin_agg_source_report') }}">Aggregate Source</a></li>
                                <li><a href="{{ route('admin_cement_brand_report') }}">Cement Brand </a></li>
                                <li><a href="{{ route('admin_cement_grade_report') }}">Cement Grade </a></li>
                                <li><a href="{{ route('admin_concrete_grade_report') }}">Concrete Grade </a></li>
                                <li><a href="{{ route('admin_concrete_pump_category_report') }}">Concrete Pump Category </a></li>
                                <!-- <li><a href="{{ route('admin_coupon_report') }}">Coupon Report </a></li> -->
                                <!-- <li><a href="{{ route('admin_support_ticket_report') }}">Support Ticket Report </a></li> -->
                                <li><a href="{{ route('admin_flyAsh_source_report') }}">Flyash Report </a></li>
                                <!-- <li><a href="{{ route('admin_gst_slab_report') }}">GST Slab Report </a></li> -->
                                <!-- <li><a href="{{ route('admin_margin_rate_report') }}">Margin Slab Report </a></li> -->
                                <li><a href="{{ route('admin_sand_source_report') }}">Sand Source Report </a></li>
                                <!-- <li><a href="{{ route('admin_report_vehicle_list') }}">TM Listing Report </a></li> -->
                                <li><a href="{{ route('admin_report_TM_category_report') }}">TM Category Report </a></li>
                                <li><a href="{{ route('admin_report_TM_sub_category_report') }}">TM Sub Category Report </a></li>

                                <!-- <li><a href="{{ route('admin_report_designmix_list') }}">List of design mix.</a></li> -->
                                <!-- <li><a href="{{ route('admin_report_buyer_listing') }}">Buyer listing</a></li>
                                <li><a href="{{ route('admin_report_supplier_listing') }}">Supplier listing</a></li> -->
                                
                                
                            </ul>
                        </div>
                    @endif
                    @if($profile["rights"]["sales_report_view"])
                    <div class="reportsList_box">
                        <h3>Sales</h3>
                        <ul>
                            <li><a href="{{ route('admin_report_buyer_orders') }}">List buyer order</a></li>
                              <li><a href="{{ route('admin_report_supplier_orders') }}">List supplier order</a></li>
                              
                              
                              
                               <!-- <li><a href="{{ route('admin_report_vehicle_list') }}"> List of vehicles.</a></li> -->
                        </ul>
                    </div>
                    @endif
                   <!--  <div class="reportsList_box">
                        <h3>Sales</h3>
                        <ul>
                               <li><a href="#">Lorem ipuem dummy text</a></li>
                               <li><a href="#">Lorem ipuem dummy text</a></li>
                              <li><a href="#">Lorem ipuem dummy text</a></li>
                             <li><a href="#">Lorem ipuem dummy text</a></li>
                             <li><a href="#">Lorem ipuem dummy text</a></li>
                               <li><a href="#">Lorem ipuem dummy text</a></li>
                                <li><a href="#">Lorem ipuem dummy text</a></li>
                        </ul>
                    </div>
                    <div class="reportsList_box">
                        <h3>Clients</h3>
                        <ul>
                             <li><a href="#">Lorem ipuem dummy text</a></li>
                              <li><a href="#">Lorem ipuem dummy text</a></li>
                              <li><a href="#">Lorem ipuem dummy text</a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection