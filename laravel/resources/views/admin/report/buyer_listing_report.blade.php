@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums">Buyers Listing Report</h1>
    <div class="clearfix"></div>
    <!--Add User start -->
    <div class="user-block add-user-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                @if(isset($data["error"]))
                    <div class="">{{ $data["error"]["message"] }}</div>
                @endif
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route(Route::current()->getName()) }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <h2 class="sub-title pull-left">Buyers Listing Report</h2>
                        <div class="d-flex align-items-center">
                            <div class="range_picker m-r10">
                                <input type="text" class="form-control" name="daterange" value="01/01/2018 - 01/15/2018">
                                
                                <i class="fa fa-calendar"></i>
                            </div>

                            <div class="dropdown cstm-dropdown-select exprt-drpdwn pull-right">
                                <button class="site-button m-r10 icn-btn" type="button" data-toggle="dropdown" aria-expanded="true"><span class="dropdown-label">Export <i class="fa fa-download"></i></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                    <li><a class="dropdown-item" href="javascript:;" onclick="downloadBuyerList('pdf')">PDF</a></li>
                                    <li><a class="dropdown-item" href="javascript:;" onclick="downloadBuyerList('excel')">Excel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="comn-table1 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>User Id</th>
                                <th>Company Name</th>
                                <th>Owner Full Name</th>
                                <th>Mobile No</th>
                                <th>Email</th>
                                <th>Account Type</th>
                                <th>Date & Time</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data["data"]) && count($data['data']) > 0)
                            
                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                    <tr>
                                        <td>#{{ isset($value["user_id"]) ? $value["user_id"] : '' }}</td>                                        
                                        
                                        <td>{{ isset($value["company_name"]) ? $value["company_name"] : '' }}</td>
                                        <td>{{ isset($value["full_name"]) ? $value["full_name"] : '' }}</td>
                                        <td>{{ isset($value["mobile_number"]) ? $value["mobile_number"] : '' }}</td>
                                        <td>{{ isset($value["email"]) ? $value["email"] : '' }}</td>
                                        <td>{{ isset($value["account_type"]) ? $value["account_type"] : '' }}</td>

                                        <td>{{ date('d M Y h:i:s a', strtotime($value["created_at"])) }}</td>
                                                                         
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

                <div class="clearfix"></div>

                <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="data-box-footer clearfix">
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                       
                            <div class="pagination justify-content-end m-0">
                            @if($is_previous_avail == 1)
                                @if($first_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    @if(Request::get('company_name'))
                                                <input type="hidden" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('name'))
                                                <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('user_id'))
                                                <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('mobile_no'))
                                                <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('email'))
                                                <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('account_type'))
                                                <input type="hidden" name="account_type" value="{{ Request::get('account_type') ? Request::get('account_type') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('start_date'))
                                            <input type="hidden" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        @if(Request::get('end_date'))
                                            <input type="hidden" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                    <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                    <button type="submit" class="site-button">Previous</button>                                    
                                </form>
                                @endif
                                @endif
                                @if($is_next_avail == 1)
                                @if($last_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    @if(Request::get('company_name'))
                                                <input type="hidden" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('name'))
                                                <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('user_id'))
                                                <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('mobile_no'))
                                                <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('email'))
                                                <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('account_type'))
                                                <input type="hidden" name="account_type" value="{{ Request::get('account_type') ? Request::get('account_type') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('start_date'))
                                        <input type="hidden" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                        @if(Request::get('end_date'))
                                            <input type="hidden" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : ''  }}" class="form-control" placeholder="Email">
                                        @endif
                                    <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                    <button type="submit" class="site-button">Next</button>
                                </form>
                                @endif
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!--Add User end -->
</div>


<script>
$(function() {

    var today = new Date();
    var end = new Date();
    // today.setDate(today.getDate() + 2);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    end = dd + '/' + mm + '/' + yyyy;
    console.log(today);

    @if(Request::get('start_date'))

        var date = new Date('{{Request::get('start_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        today = dd + '/' + mm + '/' + yyyy;
        // today = yyyy + '-' + mm + '-' + dd;
        console.log("start_date..."+today);
    @endif

    @if(Request::get('end_date'))

        var date = new Date('{{Request::get('end_date')}}');

        var dd = String(date.getDate()).padStart(2, '0');
        var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = date.getFullYear();
        console.log(mm+"..."+yyyy);

        end = dd + '/' + mm + '/' + yyyy;
        // end = yyyy + '-' + mm + '-' + dd;
        console.log("end_date..."+end);
    @endif

  $('input[name="daterange"]').daterangepicker({
    opens: 'left',
    startDate: today,
    endDate: end,
    locale: {
            format: 'DD/MM/YYYY'
        }
  }, function(start, end, label) {
      $("#report_start_date").val(start.format('YYYY-MM-DD'));
      $("#report_end_date").val(end.format('YYYY-MM-DD'));

      $('form[name="supplier_report_form"]').submit();

        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>

@endsection