@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums">Logistics Listing Report</h1>
    <div class="clearfix"></div>
    <!--Add User start -->
    <div class="user-block add-user-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                @if(isset($data["error"]))
                    <div class="">{{ $data["error"]["message"] }}</div>
                @endif
                    <!-- <h2 class="sub-title pull-left">Add User</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <button class="site-button pull-right m-r10" data-toggle="modal" data-target="#add-user-popup" onclick="resetForm('add_user_form','add-user-popup')">Add User</button> -->
                </div>
                <div class="comn-table1 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Full Name</th>
                                <!-- <th>Mobile No</th>
                                <th>Email</th>
                                <th>Admin Type</th> -->
                                <th>Number Of Orders</th>
                                <th>Date & Time</th>
                                <!-- <th>Status</th> -->
                                <!-- <th>Is Deleted</th> -->
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($data["data"]) && count($data['data']) > 0)
                            @php
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                    <tr>
                                        <td>{{ $value["full_name"] }}</td>
                                        
                                        <td>{{ $value["order_count"] }}</td>
                                        
                                        <td>{{ date('d M Y h:i:s a', strtotime($value["created_at"])) }}</td>
                                                                         
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

                <div class="clearfix"></div>

                <div class="data-box-footer clearfix">
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)
                            <div class="pagination justify-content-end m-0">
                                @if($first_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    @if(Request::get('name'))
                                        <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                    @endif
                                    @if(Request::get('mobile_no'))
                                        <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                                    @endif
                                    @if(Request::get('email'))
                                        <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('admin_type'))
                                        <input type="hidden" name="admin_type" value="{{ Request::get('admin_type') ? Request::get('admin_type') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                    <button type="submit" class="site-button">Previous</button>                                    
                                </form>
                                @endif
                                @if($last_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    @if(Request::get('name'))
                                        <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                    @endif
                                    @if(Request::get('mobile_no'))
                                        <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Mobile No">
                                    @endif
                                    @if(Request::get('email'))
                                        <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('admin_type'))
                                        <input type="hidden" name="admin_type" value="{{ Request::get('admin_type') ? Request::get('admin_type') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                    <button type="submit" class="site-button">Next</button>
                                </form>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!--Add User end -->
</div>



    


@endsection