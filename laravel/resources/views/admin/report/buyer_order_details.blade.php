@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="breadcrums">Report / <a href="#">Buyer Orders</a> / <span>Order Details</span></h1>
    <div class="clearfix"></div>
    
    <div class="orders-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Order Details</h2>
                </div>
                
                <div class="odrs-details-block p-a0">
               
                    <div class="orders-content-block">
                    <?php //dd($data["data"]); ?>
                    @if(isset($data["data"]["orderData"]))
                        @php
                            $order_data = $data["data"]["orderData"];
                        @endphp
                        <div class="orders-content-head-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-id-date-block">
                                        <div class="ordlst-sd-text-block spa">
                                            Order Id: <span>#{{ isset($order_data['display_id']) ? $order_data['display_id'] : '' }}</span>
                                        </div>

                                        @if(isset($order_data['display_id']) && isset($order_data['invoice']['_id']))
                                        <div class="ordlst-id-block">
                                            
                                                Invoice Id: <a href="{{ url('admin/orders/invoice/'.$order_data['display_id']) }}">{{ isset($order_data['invoice']['_id']) ? $order_data['invoice']['_id'] : '' }}</a>
                                            
                                        </div>
                                        @endif
                                        <div class="ordlst-date-block">
                                            Order Date: <span>{{ isset($order_data['created_at']) ? date('d M Y h:i:s a', strtotime($order_data['created_at'])) : '' }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Buyer Name: <span>{{ isset($order_data['buyer']['full_name']) ? $order_data['buyer']['full_name'] : '' }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="ordlst-ship-dlvry-date-block">
                                        <div class="ordlst-sd-text-block">
                                            Payment Status: <span class="badge bg-green">{{ isset($order_data['payment_status']) ? $order_data['payment_status'] : '' }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block">
                                            Total Amount: <span class="rupee-text"><i class="fa fa-rupee"></i> {{ isset($order_data['total_amount']) ? round($order_data['total_amount']) : '' }}</span>
                                        </div>
                                        <div class="ordlst-sd-text-block dlivry-lctin">
                                            Delivery Location: 
                                            <span class="vName cstm-tooltip" data-direction="bottom">
                                                <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View Address</a> | <a href="javascript:;" class="view-map-icon" onclick="viewMap('{{ isset($order_data['delivery_location']['coordinates'][1]) ? $order_data['delivery_location']['coordinates'][1] : '' }}','{{ isset($order_data['delivery_location']['coordinates'][0]) ? $order_data['delivery_location']['coordinates'][0] : '' }}')">View Map</a>
                                                <span class="cstm-tooltip__item">
                                                    {{ isset($order_data['address_line1']) ? $order_data['address_line1'] : '' }}, 
                                                    {{ isset($order_data['address_line2']) ? $order_data['address_line2'] : '' }}, 
                                                    {{ isset($order_data['state_name']) ? $order_data['state_name'] : '' }}, 
                                                    {{ isset($order_data['city_name']) ? $order_data['city_name'] : '' }}, 
                                                    
                                                </span>
                                            </span>
                                        </div>
                                        @if(isset($order_data['display_id']) && isset($order_data['invoice']['_id']))
                                        <div class="odr-select-optin-block">
                                            <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button blue button-sm" type="button" data-toggle="dropdown" aria-expanded="false">
                                                    <span class="dropdown-label">Select Invoice <i class="fa fa-sort-down"></i></span>
                                                </button>
                                                
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                        
                                                            <li><a class="dropdown-item" href="{{ url('admin/orders/invoice/'.$order_data['display_id']) }}">View invoice</a></li>
                                                        
                                                        <!-- <li><a class="dropdown-item" href="javascript:;">Download invoice</a></li> -->
                                                        <!-- <li><a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#sendemil-sts-popup">Send invoice to email</a></li> -->
                                                        <!-- <li><a class="dropdown-item" href="javascript:;">Download purchase order</a></li>
                                                        <li><a class="dropdown-item" href="javascript:;">Send purchase order to email</a></li> -->
                                                    </ul>  
                                                
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                        <div class="orders-content-middle-block">
                            <div class="orders-table">
                                <table class="table">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Category Name</th>
                                        <th>Qty</th>
                                        <th>Margin Rate</th>
                                        <th>Per Unit Price ( <i class="fa fa-rupee"></i> )</th>
                                        <th>Basic Amount ( <i class="fa fa-rupee"></i> )</th>
                                        <th>Margin Amount ( <i class="fa fa-rupee"></i> )</th>
                                        <th>GST Amount ( <i class="fa fa-rupee"></i> )</th>
                                        <th>Logistic Amount ( <i class="fa fa-rupee"></i> )</th>
                                        <th>Total Amount ( <i class="fa fa-rupee"></i> )</th>
                                        <th>Order Status</th>
                                        <!-- <th>Track</th>
                                        <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($data['data']["orderItemData"]))
                                    @php
                                        $item_data = $data['data']["orderItemData"];
                                    @endphp
                                @foreach( $item_data as $value)  
                                    <tr>
                                        <td>
                                            <div class="cate_img"><img src="{{ isset($value['product_category']['image_url']) ? $value['product_category']['image_url'] : '' }}"></div>
                                        </td>
                                        <td>{{ isset($value['product_category']['category_name']) ? $value['product_category']['category_name'] : '' }} - {{ isset($value['product_sub_category']['sub_category_name']) ? $value['product_sub_category']['sub_category_name'] : '' }}</td>
                                        <td><span class="qty-text">{{ isset($value['quantity']) ? $value['quantity'] : '' }}</span></td>
                                        <td>{{ isset($value['margin_rate']) ? $value['margin_rate'] : '' }}%</td>
                                        <td> {{ isset($value['unit_price']) ? round($value['unit_price']) : '' }}</td>
                                        <td> {{ isset($value['base_amount']) ? round($value['base_amount']) : '' }}</td>
                                        <td> {{ isset($value['margin_amount']) ? round($value['margin_amount']) : '' }}</td>
                                        <td> {{ isset($value['cgst_amount']) ? round($value['cgst_amount']) : '' }}</td>
                                        <td> {{ isset($value['logistics_amount']) ? round($value['logistics_amount']) : '' }}</td>
                                        <td> {{ isset($value['total_amount']) ? round($value['total_amount']) : '' }}</td>
                                        <td class="noowrp">
                                            <div class="badge bg-red">{{ isset($value['item_status']) ? $value['item_status'] : '' }}</div>
                                           
                                        </td>
                                        
                                    </tr>
                                    
                                @endforeach
                            @endif
                                  
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

@endsection