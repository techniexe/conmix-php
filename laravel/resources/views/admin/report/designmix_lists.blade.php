@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Design Mix</h1>
    <div class="clearfix"></div>
    <!--Contact Detail start -->
    <div class="bank-detail-main-block design_mix_bx wht-tble-bg">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <!-- <li>
                        <div class="bank-detail-block add-bank-detail-block">
                            
                            <a href="{{ route('design_mix_add_view') }}"><i>+</i></a>
                        </div>
                    </li> -->
                    @if(isset($data["data"]) && !empty($data["data"]))
                    @php
                        $first_created_date = '';
                        $last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                                $first_created_date = $value["created_at"];
                                $count++; 
                            @endphp
                        @endif
                        @php
                            $last_created_date = $value["created_at"];
                        @endphp
                        <li>
                            <div class="bank-details-select">
                                <!-- <div class="make-default-btn site-button green" onclick="viewDesignMixOfGrade('{{ $value['_id'] }}')">View {{ isset($value["concrete_grade"]["name"]) ? $value["concrete_grade"]["name"] : '' }} Mix</div> -->
                               
                                <div class="bank-detail-block p-a15" onclick="viewDesignMixOfGrade('{{ $value['_id'] }}')">
                                    <span>{{ isset($value["concrete_grade"]["name"]) ? $value["concrete_grade"]["name"] : '' }}</span>
                                </div>
                            </div>
                        </li>
                        @endforeach

                    @else
                        <p style="text-align: center;">No Record Found</p>
                    @endif
                    

                    
                </ul>
            </div>
        </div>
    </div>
    <!--Contact Detail end -->
</div>

@endsection