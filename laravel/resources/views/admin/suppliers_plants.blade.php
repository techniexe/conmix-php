<?php use App\Http\Controllers\Admin\AdminController;?>
<?php //dd($plant_details); ?>
@if(isset($plant_details["data"]) && count($plant_details["data"]) > 0)
                @foreach($plant_details["data"] as $value)
 <div class="bls-users-contet-block m-b20">
        <div class="row">
            <div class="user_action_div">
                @if(isset($value['is_verified']) && $value['is_verified'] == false)
                    <form action="" method="post" name="supplier_verify_form_{{ $value['_id'] }}" id="supplier_plant_verify_form_{{ $value['_id'] }}">
                        @csrf    
                        <div class="verified-switch-btn mb-2">
                            <label class="new-switch switch-green">
                                <input type="hidden" name="supplier_id" value="{{ $value['vendorDetails']["_id"] }}">
                                <input type="hidden" name="address_id" value="{{ $value["_id"] }}">
                                <input type="checkbox" name="is_verified" data-address-id="{{ $value["_id"] }}" data-supplier-id="{{ $value['vendorDetails']["_id"] }}" id="supplier_plant_verify_by_admin_{{ $value["_id"] }}" class="switch-input" {{ $value['is_verified'] == true ? 'checked' : '' }}>
                                <span class="switch-label" data-on="Verified" data-off="Unverified"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </form>
                @endif
                @if(isset($value['is_verified']) && $value['is_verified'] == false)
                    <button class="site-button red button-sm mb-1" onclick="rejectPlantPopUp('{{ $value['_id'] }}','{{ $value['vendorDetails']['_id'] }}')">Reject</button>
                @endif
                @if(isset($value['is_verified']) && $value['is_verified'] == true)
                    <form action="" method="post" name="supplier_block_form_{{ $value['_id'] }}" id="supplier_block_form_{{ $value['_id'] }}">
                        @csrf
                        <div class="verified-switch-btn mb-2">
                            <label class="new-switch switch-red">
                                <input type="hidden" name="supplier_id" value="{{ $value['vendorDetails']["_id"] }}" />
                                <input type="hidden" name="address_id" value="{{ $value["_id"] }}">
                                <input type="checkbox" name="blocked_by_admin" data-address-id="{{ $value["_id"] }}" data-supplier-id="{{ $value['vendorDetails']['_id'] }}" id="supplier_blocked_by_admin_{{ $value['_id'] }}" class="switch-input" {{ isset($value['is_blocked']) ? ($value['is_blocked'] == true ? 'checked' : '') : '' }}>
                                <span class="switch-label" data-on="Block" data-off="Unblock"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </form>
                @endif
            </div>
            <script>
                $('#supplier_plant_verify_by_admin_{{ $value['_id'] }}').change(function() {
                    $is_checked = '';                    

                    var supplier_id = $(this).attr("data-supplier-id");
                    var address_id = $(this).attr("data-address-id");

                    request_data = $('#supplier_plant_verify_form_'+address_id).serialize();
                    console.log(request_data);
                    if(this.checked) {
                        $is_checked = 'true';
                        $('#supplier_plant_verify_by_admin_{{ $value['_id'] }}').prop("checked",false);
                    }else{
                        $is_checked = 'false';
                        $('#supplier_plant_verify_by_admin_{{ $value['_id'] }}').prop("checked",true);
                    }
                    // supplierVerifyByAdmin(request_data);
                    $("#sub_vender_popup").modal("hide");
                    requestOTP(41,request_data,"","plant_verification");

                });

                $('#supplier_blocked_by_admin_{{ $value['_id'] }}').change(function() {
                    $is_checked = '';
                    var event = "plant_unblock";                    

                    var supplier_id = $(this).attr("data-supplier-id");
                    var address_id = $(this).attr("data-address-id");

                    request_data = $('#supplier_block_form_'+address_id).serialize();
                    console.log(request_data);
                    if(this.checked) {
                        $is_checked = 'true';
                        event = "plant_block";
                        $('#supplier_blocked_by_admin_{{ $value['_id'] }}').prop("checked",false);
                    }else{
                        $is_checked = 'false';
                        $('#supplier_blocked_by_admin_{{ $value['_id'] }}').prop("checked",true);
                    }
                    // supplierVerifyByAdmin(request_data);
                    $("#sub_vender_popup").modal("hide");
                    requestOTP(42,request_data,"",event);

                });
            </script>
            <?php //dd($plant_details["data"]); ?>
            
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h3 class="bls-users-title">{{ $value["business_name"] }}</h3>
                        <ul>
                            <li>
                                <p><span class="vTitle">Person Name : </span> <span class="vName">{{ isset($value["vendorDetails"]["full_name"]) ? $value["vendorDetails"]["full_name"] : '' }}</span></p>
                            </li>
                            <li>
                                <p><span class="vTitle">Mobile No. : </span> <span class="vName">{{ isset($value["vendorDetails"]["mobile_number"]) ? $value["vendorDetails"]["mobile_number"] : '' }}</span></p>
                            </li>
                            <li>
                                <p><span class="vTitle">Email : </span> <span class="vName">{{ isset($value["vendorDetails"]["email"]) ? $value["vendorDetails"]["email"] : '' }}</span></p>
                            </li>
        
                            <li>
                                <p><span class="vTitle">GST No : </span> <span class="vName">{{ isset($value["billingAddressDetails"]["gst_number"]) ? $value["billingAddressDetails"]["gst_number"] : '' }}</span></p>
                            </li>
                            <li>
                                <p><span class="vTitle">Verified by Admin : </span> <span class="vName"> <i class="{{ isset($value["is_verified"]) ? ($value["is_verified"] ? "fa fa-check text-green" : "fa fa-close text-red") : "" }}"></i></span></p>
                            </li>
                            <li>
                                <p><span class="vTitle">Address : </span> <span class="vName"> 

                                    {{ $value["line1"] }},
                                    {{ $value["line2"] }},
                                    {{ $value["cityDetails"]["city_name"] }},
                                    {{ $value["pincode"] }},
                                    {{ $value["stateDetails"]["state_name"] }},

                                </span></p>
                            </li>
                            <li>
                                <p><span class="vTitle">Register On : </span> <span class="vName">{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span></p>
                            </li>
                        </ul>
                        <!-- <div class="document_list">
                            <a href="" class="document_box gal_link">
                                <i class="fa fa-image"></i>
                                <span>Company Certificate</span>
                            </a>
                        </div> -->
                    </div>
            
        </div>
    </div>
    @endforeach

@else

    <div>Plants not added Yet</div>

            @endif