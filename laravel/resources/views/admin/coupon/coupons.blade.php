@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
    <h1 class="breadcrums">Coupon / <span>Add Coupon</span></h1>
    <div class="clearfix"></div>
    <!--Add User start -->
    <div class="user-block add-user-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                @if(isset($data["error"]))
                    <div class="">{{ $data["error"]["message"] }}</div>
                @endif
                    <h2 class="sub-title pull-left">Add Coupon</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_coupon') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    <button class="site-button pull-right m-r10" data-toggle="modal" data-target="#add-coupon-popup" onclick="resetForm('add_coupon','add-coupon-popup')">Add Coupon</button>
                </div>
                <div class="comn-table1 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Discount Type</th>
                                <th>Discount Value</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Min Order</th>
                                <th>Max Discount</th>
                                <th>Info</th>
                                <th>T & C</th>
                                <th>Added On</th>
                                <th>Status</th>
                                <th>Is Deleted</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $first_created_date = '';
                                $last_created_date = '';
                                $count = 0;
                            @endphp
                            @if(isset($data["data"]) && count($data['data']) > 0)

                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php
                                            $first_created_date = $value["created_at"];
                                            $count++;
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp
                                    <tr>
                                        <td>{{ $value["code"] }}</td>
                                        <td>{{ isset($value["discount_type"]) ? ($value["discount_type"] == 'percentage' ? "Percentage" : 'Amount') : "" }}</td>
                                        <td>{{ $value["discount_value"] }}</td>
                                        <td>{{ date('d M Y', strtotime($value["start_date"])) }}</td>

                                        <td>{{ date('d M Y', strtotime($value["end_date"])) }}</td>
                                        <td>{{ $value["min_order"] }}</td>
                                        <td>{{ $value["max_discount"] }}</td>
                                        <td><a href="javascript:void(0)" onclick="couponInfo('{{ str_replace("'", "", $value["info"]) }}')"><i class="fa fa-info-circle"></i></a></td>
                                        <td><a href="javascript:void(0)" onclick="couponTnc('{{ str_replace("'", "", $value["tnc"]) }}')"><i class="fa fa-info-circle"></i></a></td>
                                        <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                        <td>

                                            <div id="active_inactive_{{ $value['_id'] }}" class="badge {{ $value['is_active'] ? 'bg-green' : 'bg-grey'}} ">
                                                {{ $value['is_active'] ? 'Active' : 'InActive'}}
                                            </div>

                                        </td>
                                        <td>
                                            <div id="" class="badge {{ $value['is_deleted'] ? 'bg-grey' : ''}} ">
                                                {{ $value['is_deleted'] ? 'Deleted' : ''}}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="dropdown cstm-dropdown-select">
                                                <button class="site-button gray button-sm" type="button" data-toggle="dropdown" aria-expanded="true">
                                                    <span class="dropdown-label">Action <i class="fa fa-sort-down"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="option-post-cleanup">
                                                    <li><a class="dropdown-item" href="javascript:void(0);" onclick="confirmPopup(28,'{{ str_replace("'", "", json_encode($value)) }}','{{ csrf_token() }}','{{ $value["_id"] }}')">{{ $value["is_active"] == 'true' ? "InActive" : "Active" }}</a></li>
                                                    <li><a class="dropdown-item" href="javascript:void(0);" onclick="showEditCoupon('{{ str_replace("'", "", json_encode($value)) }}')">Edit</a></li>
                                                    <li><a class="dropdown-item" href="javascript:void(0);" onclick="confirmPopup(29,'{{ str_replace("'", "", json_encode($value)) }}','{{ csrf_token() }}','{{ $value["_id"] }}')">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="13">No Record Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

                <div class="clearfix"></div>


                <?php

                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif


                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>


                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif



                        @endif

                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif

                <div class="data-box-footer clearfix">
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                            <div class="pagination justify-content-end m-0">
                            @if($is_previous_avail == 1)
                                @if($first_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                    @if(Request::get('code'))
                                        <input type="hidden" name="code" value="{{ Request::get('code') ? Request::get('code') : ''  }}" class="form-control" placeholder="Name">
                                    @endif
                                    @if(Request::get('discount_value'))
                                        <input type="hidden" name="discount_value" value="{{ Request::get('discount_value') ? Request::get('discount_value') : ''  }}" class="form-control" placeholder="Mobile No">
                                    @endif
                                    @if(Request::get('start_date'))
                                        <input type="hidden" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('end_date'))
                                        <input type="hidden" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('min_order'))
                                        <input type="hidden" name="min_order" value="{{ Request::get('min_order') ? Request::get('min_order') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('max_discount'))
                                        <input type="hidden" name="max_discount" value="{{ Request::get('max_discount') ? Request::get('max_discount') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('is_active'))
                                        <input type="hidden" name="is_active" value="{{ Request::get('is_active') ? Request::get('is_active') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                    <button type="submit" class="site-button">Previous</button>
                                </form>
                                @endif
                                @endif
                                @if($is_next_avail == 1)
                                @if($last_created_date)
                                <form action="{{ route(Route::current()->getName()) }}" method="get">
                                @if(Request::get('code'))
                                        <input type="hidden" name="code" value="{{ Request::get('code') ? Request::get('code') : ''  }}" class="form-control" placeholder="Name">
                                    @endif
                                    @if(Request::get('discount_value'))
                                        <input type="hidden" name="discount_value" value="{{ Request::get('discount_value') ? Request::get('discount_value') : ''  }}" class="form-control" placeholder="Mobile No">
                                    @endif
                                    @if(Request::get('start_date'))
                                        <input type="hidden" name="start_date" value="{{ Request::get('start_date') ? Request::get('start_date') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('end_date'))
                                        <input type="hidden" name="end_date" value="{{ Request::get('end_date') ? Request::get('end_date') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('min_order_range_value'))
                                        <input type="hidden" name="min_order_range_value" value="{{ Request::get('min_order_range_value') ? Request::get('min_order_range_value') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('min_order'))
                                        <input type="hidden" name="min_order" value="{{ Request::get('min_order') ? Request::get('min_order') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('max_discount_range_value'))
                                        <input type="hidden" name="max_discount_range_value" value="{{ Request::get('max_discount_range_value') ? Request::get('max_discount_range_value') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('max_discount'))
                                        <input type="hidden" name="max_discount" value="{{ Request::get('max_discount') ? Request::get('max_discount') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    @if(Request::get('is_active'))
                                        <input type="hidden" name="is_active" value="{{ Request::get('is_active') ? Request::get('is_active') : ''  }}" class="form-control" placeholder="Email">
                                    @endif
                                    <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                    <button type="submit" class="site-button">Next</button>
                                </form>
                                @endif
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!--Add User end -->
</div>






@endsection