@extends('admin.layouts.admin_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
                <h1 class="login-title">Admin</h1>
                <div id="forgotpassword" style="display: block;">
                    <h2>Forgot Password</h2>
                    @if(isset($data["error"]))
                        <div class="error alert alert-danger">{{ $data["error"]["message"] }}</div>
                    @endif
                    <div class="form-group">
                        <div class="input-group">
                            <span class="text-green">
                                <!-- OTP has been send to your mobile <span class="mobileno">+91 ******8989</span> -->
                                @if(isset($data["message"]))
                                    <div class="">{{ isset($data["message"]) ? $data["message"] : '' }}</div>
                                @endif 
                            </span>
                        </div>
                    </div>
                    
                    <!-- <form id="newpassword"> -->
                    <form action="{{ route('admin_forgot_pass') }}" name="forgot_pass_form" method="post">
                            @csrf
                        <div class="form-group">
                            <div class="input-group">
                                <span class="text-gray-dark">Choose a new password for your account. This password will replace the old one.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label>New Password <span class="str">*</span></label>
                                <input class="form-control" id="new_password" name="password" placeholder="e.g. Enter New Password" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label>Confirm Password <span class="str">*</span></label>
                                <input class="form-control" name="confirm_password" placeholder="e.g. Enter Confirm Password" type="password">
                            </div>
                        </div>
                        <div class="form-group m-t30">
                            <div class="input-group text-left">
                                <button type="submit" class="site-button">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection