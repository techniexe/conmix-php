@extends('admin.layouts.admin_login_layout')

@section('content')

<div class="middle-container-wrap">
    <div class="login-signup-modal-block">
        <div class="cstm-modal-block">
            <div class="ls-content-block">
            <h1 class="login-title">Admin Panel - <span style="color:#687ae8;"> Conmix </span></h1>
                <div id="forgotpassword" style="display: block;">
                    <h2>Forgot Password</h2>
                    @if(isset($data["error"]))
                        <div class="error alert alert-danger">{{ $data["error"]["message"] }}</div>
                    @endif
                    <form action="{{route('admin_forgot_pass_otp_verify')}}" name="forgot_pass_otp_verify_form" method="post" onsubmit="return validateForm()">
                            @csrf
                        <div class="form-group">
                            <div class="input-group">
                                <span class="text-green">
                                    <!-- OTP has been send to your mobile <span class="mobileno">+91 ******8989</span> -->
                                    @if(isset($data["data"]))
                                        <div class="">{{ isset($data["data"]["message"]) ? $data["data"]["message"] : '' }}</div>
                                    @endif 
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label>OTP <span class="str">*</span></label>
                                <input class="form-control" onkeypress="onlyNumbers(event)" maxlength="6" id="otp_code" name="otp_code" placeholder="One Time Password" type="text">
                                <div class="invalid-feedback" id="otp_error"></div>
                                <input class="form-control" name="email_mobile" value="{{ $data['data']['email_mobile'] }}" placeholder="One Time Password" type="hidden">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="input-group text-left">
                                <!-- <a href="forgot-password.html" class="site-button yellow">Verify OTP</a> -->
                                <button type="submit" name="otp_btn" value="otp_btn" class="site-button">Verify OTP</a>
                            </div>
                        </div>
                    </form>

                    <div class="text-gray-dark resend_otp_div">
                        <form action="{{route('admin_forgot_pass_otp_send')}}"  name="forgot_pass_email_verify_form" method="post">
                            @csrf
                            
                            <input class="form-control" name="email_mobile" value="{{ $data['data']['email_mobile'] }}" required  type="hidden">                          
                            

                            <div class="timer_otp mb-3">
                                <span id="timer_forgot_pass">
                                    <span id="time_forgot_pass">02:00</span>
                                </span>
                            </div>
                        </form>
                    </div>

                        <div class="form-group pull-left w-100">
                            <div class="input-group display-block">
                                <!-- <a href="#" class="pull-right">Resend</a> -->

                                <!-- <form action="{{route('forgot_pass_otp_send')}}"  name="forgot_pass_email_verify_form" method="post">
                                    @csrf
                                    
                                    <input class="form-control" name="email_mobile" value="{{ $data['data']['email_mobile'] }}" required  type="hidden">
                                        
                                    <button type="submit" name="otp_send_btn" value="otp_send_btn" class="pull-right">Resend</button>
                                </form> -->
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function validateForm() {
    
    $(".error").remove("");
    // $(".error").html("");
    // $(".error").removeClass("alert-danger");

    $("#otp_error").html("");
  
  var otp = document.forms["forgot_pass_otp_verify_form"]["otp_code"].value.trim();
  
  var is_valid = validateOTP(otp);

    return is_valid;


  
} 

window.onload = function () {
    var fiveMinutes = 60 * 2,
        // display = document.querySelector('#time_forgot_pass');
        display = $('#time_forgot_pass');
    startTimer(fiveMinutes, display, "admin_forgot_pass");
};

</script>

@endsection