@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
<div class="middle-container-wrap">
            <h1 class="breadcrums">Users / <span>Buyers</span></h1>
            <div class="clearfix"></div>
            <!--vehicle category list start -->
            <div class="users-main-block wht-tble-bg">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="sub-title-block">
                        @if(isset($data["error"]))
                            <div class="">{{ $data["error"]["message"] }}</div>
                        @endif
                            <h2 class="sub-title pull-left">Buyer Users</h2>
                            <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                            <a href="{{ route('admin_show_buyers_user') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                        </div>
                        <div class="bls-users-block">
                        @php
                        //dd($data["data"]);
                                    $first_created_date = '';
                                    $last_created_date = '';
                                    $count = 0;
                                @endphp
                            @if(isset($data["data"]) && count($data['data']) > 0)
                                
                                @foreach($data["data"] as $value)
                                    @if($count == 0)
                                        @php 
                                            $first_created_date = $value["created_at"];
                                            $count++; 
                                        @endphp
                                    @endif
                                    @php
                                        $last_created_date = $value["created_at"];
                                    @endphp

                                    <div class="bls-users-contet-block m-b20">
                                        <!-- <a href="javascript:;" class="site-button outline gray pull-right top-right-link-icon" data-tooltip="Dashboard"><i class="fa fa-dashboard"></i></a> -->
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <h3 class="bls-users-title">
                                                    @if($value["company_type"] == "Individual")
                                                        {{ isset($value["full_name"]) ? $value["full_name"] : '' }}
                                                    @else
                                                        {{ isset($value["company_name"]) ? $value["company_name"] : '' }}
                                                    @endif
                                                </h3>
                                                <ul>
                                                    <li>
                                                        <p><span class="vTitle">User Id : </span> <span class="vName">#{{ isset($value["user_id"]) ? $value["user_id"] : '' }}</span></p>
                                                    </li>
                                                    <li>
                                                        <p><span class="vTitle">Buyer Name : </span> <span class="vName">{{ isset($value["full_name"]) ? $value["full_name"] : '' }}</span></p>
                                                    </li>
                                                    <li>
                                                        <p><span class="vTitle">Mobile No. : </span> <span class="vName">{{ isset($value["mobile_number"]) ? $value["mobile_number"] : '' }}</span></p>
                                                    </li>
                                                    <li>
                                                        <p><span class="vTitle">Email : </span> <span class="vName">{{ isset($value["email"]) ? $value["email"] : '' }}</span></p>
                                                    </li>
                                                    <li>
                                                        <p><span class="vTitle">Account Type : </span> <span class="vName">{{ isset($value["account_type"]) ? $value["account_type"] : '' }}</span></p>
                                                    </li>
                                                    <li>
                                                        <p><span class="vTitle">PAN No. : </span> <span class="vName">{{ isset($value["pan_number"]) ? $value["pan_number"] : '' }}</span></p>
                                                    </li>
                                                    <!-- <li>
                                                        <p><span class="vTitle">Signup Type : </span> <span class="vName">{{ isset($value["signup_type"]) ? $value["signup_type"] : '' }}</span></p>
                                                    </li> -->
                                                    @if(isset($value["landline_number"]))
                                                        <li>
                                                            <p><span class="vTitle">Landline No : </span> <span class="vName">{{ isset($value["landline_number"]) ? $value["landline_number"] : '' }}</span></p>
                                                        </li>
                                                    @endif
                                                    <li>
                                                        <p><span class="vTitle">Register On : </span> <span class="vName">{{ isset($value["created_at"]) ? AdminController::dateTimeFormat($value["created_at"]) : '' }}</span></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                             @else
                               <p style="text-align: center;">No Record Found</p>
                            @endif
                            
                            <?php 
                    
                    $is_previous_avail = 0;
                    $is_next_avail = 0;

                    ?>
                    @if(isset($data["data"]) && count($data['data']) > 0)

                        @if(isset($data["data"]) && count($data['data']) >= ApiConfig::PAGINATION_LIMIT)

                            @if(!Request::get('before') && !Request::get('after'))

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 0; ?>

                            @else

                                <?php $is_next_avail = 1; ?>
                                <?php $is_previous_avail = 1; ?>

                            @endif
                            

                        @else

                            @if(Request::get('after'))
                                <?php $is_next_avail = 1; ?>
                                
                                
                            @elseif(Request::get('before'))
                                <?php $is_previous_avail = 1; ?>

                            @endif

                        @endif
                    @else

                        @if(Request::get('before'))

                            <?php $is_previous_avail = 1; ?>

                            @if($first_created_date == '')

                                <?php $first_created_date = Request::get('before'); ?>

                            @endif

                            

                        @endif
                        
                        @if(Request::get('after'))

                            <?php $is_next_avail = 1; ?>

                            @if($last_created_date == '')

                                <?php $last_created_date = Request::get('after'); ?>

                            @endif

                        @endif


                    @endif
                           
                            
                            <div class="pagination-block">
                                <!-- <ul class="pagination justify-content-end m-0">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                                    </li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                                    </li>
                                </ul>
                                <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                                
                                    <div class="pagination justify-content-end m-0">
                                    @if($is_previous_avail == 1)
                                        @if($first_created_date)
                                        <form action="{{ route(Route::current()->getName()) }}" method="get">

                                            @if(Request::get('company_name'))
                                                <input type="hidden" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('name'))
                                                <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('user_id'))
                                                <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('mobile_no'))
                                                <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('email'))
                                                <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('account_type'))
                                                <input type="hidden" name="account_type" value="{{ Request::get('account_type') ? Request::get('account_type') : ''  }}" class="form-control" placeholder="Name">
                                            @endif

                                            <input type="hidden" name="after" value="{{ $first_created_date }}"/>
                                            <button type="submit" class="site-button">Previous</button>                                    
                                        </form>
                                        @endif
                                        @endif
                                        @if($is_next_avail == 1)
                                        @if($last_created_date)
                                        <form action="{{ route(Route::current()->getName()) }}" method="get">

                                            @if(Request::get('company_name'))
                                                <input type="hidden" name="company_name" value="{{ Request::get('company_name') ? Request::get('company_name') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('name'))
                                                <input type="hidden" name="name" value="{{ Request::get('name') ? Request::get('name') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('user_id'))
                                                <input type="hidden" name="user_id" value="{{ Request::get('user_id') ? Request::get('user_id') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('mobile_no'))
                                                <input type="hidden" name="mobile_no" value="{{ Request::get('mobile_no') ? Request::get('mobile_no') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('email'))
                                                <input type="hidden" name="email" value="{{ Request::get('email') ? Request::get('email') : ''  }}" class="form-control" placeholder="Name">
                                            @endif
                                            @if(Request::get('account_type'))
                                                <input type="hidden" name="account_type" value="{{ Request::get('account_type') ? Request::get('account_type') : ''  }}" class="form-control" placeholder="Name">
                                            @endif

                                            <input type="hidden" name="before" value="{{ $last_created_date }}"/>
                                            <button type="submit" class="site-button">Next</button>
                                        </form>
                                        @endif
                                        @endif
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--vehicle category list end -->
        </div>

@endsection