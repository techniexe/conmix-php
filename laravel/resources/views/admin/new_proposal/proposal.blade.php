@extends('admin.layouts.admin_layout')

@section('content')

<div class="middle-container-wrap">
    <h1 class="main-title">Suggestion For New Product</h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="proposal-main-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                    <h2 class="sub-title pull-left">Suggestion For New Product</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                </div>
                <div class="proposal-block">
                @if(isset($data["data"]) && count($data["data"]) > 0)
                    @php
                        //$first_created_date = '';
                        //$last_created_date = '';
                        $count = 0;
                    @endphp
                    @foreach($data["data"] as $value)  
                        @if($count == 0)
                            @php 
                                if(isset($value["created_at"])){
                                    $first_created_date = $value["created_at"];
                                }
                                
                                $count++; 
                            @endphp
                        @endif
                        @php
                            if(isset($value["created_at"])){
                                $last_created_date = $value["created_at"];
                            }
                            
                        @endphp
                        <div class="proposal-contet-block m-b20">
                          <div class="verified-switch-btn cstm-css-checkbox" style="right: 46px;">
                                        <label class="new-switch switch-green">
                                            <input type="checkbox" class="switch-input" checked>
                                            <span class="switch-label" data-on="Verified" data-off="Unverified"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                            <a href="javascript:;" title="Compose Message" onclick="showProposalEmailDialog('{{ json_encode($value) }}')" class="site-button outline gray pull-right top-right-link-icon"><i class="fa fa-envelope"></i></a>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h3 class="proposal-title">{{ isset($value['full_name']) ? $value['full_name'] : '' }}</h3>
                                    <ul>
                                        <li>
                                            <p><span class="vTitle">Contact Person : </span> <span class="vName">John</span></p>
                                        </li>
                                        <li>
                                            <p><span class="vTitle">Category Name : </span> <span class="vName">{{ isset($value['productCategoryDetails']["category_name"]) ? $value['productCategoryDetails']["category_name"] : '' }}</span></p>
                                        </li>
                                        <li>
                                            <p><span class="vTitle">Sub Category : </span> <span class="vName">{{ isset($value['productSubCategoryDetails']["sub_category_name"]) ? $value['productSubCategoryDetails']["sub_category_name"] : '' }}</span></p>
                                        </li>
                                        <li>
                                            <p><span class="vTitle">Qty In Stock : </span> <span class="vName">{{ isset($value['quantity']) ? $value['quantity'] : '' }} </span></p>
                                        </li>
                                        <li>
                                            <p><span class="vTitle">Email: </span> <span class="vName">{{ isset($value['email']) ? $value['email'] : '' }}</span></p>
                                        </li>
                                        <li>
                                            <p><span class="vTitle">Mobile No. : </span> <span class="vName">{{ isset($value['mobile_number']) ? $value['mobile_number'] : '' }}</span></p>
                                        </li>
                                        <li>
                                            <p><span class="vTitle">Date & Time :</span> <span class="vName">{{ isset($value['created_at']) ? date('d M Y h:i:s a',strtotime($value['created_at'])) : '' }}</span></p>
                                        </li>
                                        <li>
                                            <p><span class="vTitle">Notified On : </span> <span class="vName">{{ isset($value['notified_at']) ? date('d M Y h:i:s a',strtotime($value['notified_at'])) : '' }}</span></p>
                                        </li>
                                        <li>
                                            <p><span class="vTitle">Status : </span> <span class="vName"><span class="badge bg-red">Unseen</span></span></p>
                                        </li>
                                        <li>
                                            <p class="proposal-pickup-address">
                                                <span class="vTitle">Pickup Address : </span> 
                                                <span class="vName cstm-tooltip" data-direction="bottom">
                                                    <a href="javascript:;" class="pickup-address cstm-tooltip__initiator">View</a>
                                                    <span class="cstm-tooltip__item">
                                                        <span>{{ isset($value['delivery_address']) ? $value['delivery_address'] : '' }}</span>
                                                    </span>
                                                </span>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="proposal-message-block">
                                        <h3>Message :</h3>
                                        <div class="msg_readmore">
                                        {{ isset($value['message']) ? $value['message'] : '' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                @else
                        <div style="text-align: center;">No Record Found</div>

                @endif
                    
                   
                    
                    <div class="pagination-block">
                        <!-- <ul class="pagination justify-content-end m-0">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                        <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->


                        <div class="pagination justify-content-end m-0">
                            @if(isset($first_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                @if(Request::get('categoryId'))
                                    <input type="hidden" name="categoryId" value="{{ Request::get('categoryId') ? Request::get('categoryId') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('subcategoryId'))
                                    <input type="hidden" value="{{ Request::get('subcategoryId') ? Request::get('subcategoryId') : '' }}" name="categoryId"/>
                                @endif
                                @if(Request::get('city_id'))
                                    <input type="hidden" value="{{ Request::get('city_id') ? Request::get('city_id') : '' }}" name="city_id"/>
                                @endif
                                
                                <input type="hidden" name="before" value="{{ $first_created_date }}"/>
                                <button type="submit" class="site-button">Previous</button>                                    
                            </form>
                            @endif
                            @if(isset($last_created_date))
                            <form action="{{ route(Route::current()->getName()) }}" method="get">
                                
                                
                                @if(Request::get('categoryId'))
                                    <input type="hidden" name="categoryId" value="{{ Request::get('categoryId') ? Request::get('categoryId') : ''  }}" class="form-control" >
                                @endif
                                @if(Request::get('subcategoryId'))
                                    <input type="hidden" value="{{ Request::get('subcategoryId') ? Request::get('subcategoryId') : '' }}" name="categoryId"/>
                                @endif
                               
                                @if(Request::get('city_id'))
                                    <input type="hidden" value="{{ Request::get('city_id') ? Request::get('city_id') : '' }}" name="city_id"/>
                                @endif
                                <input type="hidden" name="after" value="{{ $last_created_date }}"/>
                                <button type="submit" class="site-button">Next</button>
                            </form>
                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>

@endsection