@extends('admin.layouts.admin_layout')

@section('content')
<?php use App\Http\Controllers\Admin\AdminController;?>
@php
    $profile = session('profile_details', null);
    //dd($profile);
@endphp
<div class="middle-container-wrap">
    <h1 class="breadcrums">Transit Mixer & Pump / <span>TM Sub Category</span></h1>
    <div class="clearfix"></div>
    <!--vehicle category list start -->
    <div class="vehicle-block vehicle-category-block wht-tble-bg">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="sub-title-block">
                @if(isset($data["error"]))
                    <div class="">{{ $data["error"]["message"] }}</div>
                @endif
                    <h2 class="sub-title pull-left">TM Sub Category</h2>
                    <button id="filterCollapse" class="site-button outline pull-right"><i class="fa fa-filter"></i></button>
                    <a href="{{ route('admin_show_TM_sub_category') }}" class="site-button pull-right m-r10 clear_filter_button">Clear</a>
                    @if($profile["rights"]["TM_sub_category_add"])
                        <button class="site-button pull-right m-r10" data-toggle="modal" data-target="#TM-sub-category" onclick="resetForm('TM_add_sub_cat_form','TM-sub-category')">Add</button>
                    @endif
                </div>
                <div class="comn-table1 m-b20 pull-left w-100">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>TM Category</th>
                                <th>Sub Category</th>
                                <th>Capacity (Cu.Mtr)</th>
                                <!-- <th>Max Load Capacity (MT)</th> -->
                                <th>Date & Time</th>
                                @if($profile["rights"]["TM_sub_category_edit"])
                                    <th>Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($data["data"]) && count($data['data']) > 0)
                                @foreach($data["data"] as $value)   
                                    <tr>
                                        <td>{{ $value["TM_category"]["category_name"] }}</td>
                                        <td>{{ $value["sub_category_name"] }}</td>
                                        <td>{{ $value["load_capacity"] }} {{ $value["weight_unit_code"] }}</td>
                                        <!-- <td> {{ $value["weight_unit_code"] }}</td> -->
                                        <td>{{ AdminController::dateTimeFormat($value["created_at"]) }}</td>
                                        @if($profile["rights"]["TM_sub_category_edit"])
                                            <td>
                                                <div class="bil-action-block">
                                                    @if($profile["rights"]["TM_sub_category_edit"])
                                                        <div class="dis-inline-block">
                                                            <a href="javascript:void(0);" onclick="showEditTMSubCategoryDialog('{{ json_encode($value) }}')" class="site-button green button-sm" data-tooltip="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                    <tr>
                                        <td colspan="6">No Record Found</td>
                                    </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="pagination-block">
                    <!-- <ul class="pagination justify-content-end m-0">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fa fa-angle-double-right"></i></a>
                        </li>
                    </ul>
                    <p class="pag-showing-text">Showing <span>5</span> of <span>10</span> order</p> -->
                </div>
            </div>
        </div>
    </div>
    <!--vehicle category list end -->
</div>


@endsection