
var doAjax_params_default = {
    'url': null,
    'requestType': "GET",
    'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
    'dataType': 'json',
    'data': {},
    'beforeSendCallbackFunction': null,
    'successCallbackFunction': null,
    'completeCallbackFunction': null,
    'errorCallBackFunction': null,
};

function doAjax(doAjax_params) {

    var url = doAjax_params['url'];
    var requestType = doAjax_params['requestType'];
    var contentType = doAjax_params['contentType'];
    var dataType = doAjax_params['dataType'];
    var data = doAjax_params['data'];
    var beforeSendCallbackFunction = doAjax_params['beforeSendCallbackFunction'];
    var successCallbackFunction = doAjax_params['successCallbackFunction'];
    var completeCallbackFunction = doAjax_params['completeCallbackFunction'];
    var errorCallBackFunction = doAjax_params['errorCallBackFunction'];

    //make sure that url ends with '/'
    /*if(!url.endsWith("/")){
     url = url + "/";
    }*/
    // console.log("url......."+url);
    $.ajax({
        url: BASE_URL+'/'+url,
        crossDomain: true,
        type: requestType,
        contentType: contentType,
        dataType: dataType,
        data: data,
        beforeSend: function(jqXHR, settings) {
            if((url == 'supplier/getMonthlyChartData') || (url == 'supplier/getDailyChartData') || (url == 'supplier/getPorductBaseMonthlyChartData') || (url == 'supplier/getPorductBaseDailyChartData') || (url == 'logistics/getMonthlyChartData') || (url == 'logistics/getDailyChartData') || (url == 'buyer/check_site_exist')
             || (url == 'check_supplier_availability')  || (url.includes("product_listing_ajax")) || (url.includes("admin/getMonthlyChartData")) || (url.includes("admin/getDailyChartData"))){
                // || (url == 'buyer/product_add_to_cart')
            }else{
                $("#overlay").fadeIn(300);
            }
            
            if (typeof beforeSendCallbackFunction === "function") {
                beforeSendCallbackFunction();
            }
        },
        success: function(data, textStatus, jqXHR) {    
            if (typeof successCallbackFunction === "function") {
                successCallbackFunction(data);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            
            if (typeof errorCallBackFunction === "function") {
                errorCallBackFunction(errorThrown);
            }

        },
        complete: function(jqXHR, textStatus) {
            // console.log("requestType...."+requestType+"..."+jqXHR);
            // console.log(jqXHR['responseJSON']["status"]);
            // if(url == 'get_regional_details'){

            // }else{

                if((url == 'buyer/signup_otp_send') || (url == 'buyer/signup_otp_verify') || (url == 'buyer/forgot_pass_otp_send') || (url == 'buyer/forgot_pass_otp_verify')){
                    $("#overlay").fadeOut(300);
                }

                if(requestType == 'GET'){
                    $("#overlay").fadeOut(300);
                }
                
                if(jqXHR['responseJSON']["status"] != 200){
                    $("#overlay").fadeOut(300);
                    window.scrollTo(0, 0);
                }

                if((url == 'buyer/product_add_to_cart') || (url == 'buyer/delete_cart_item') ){
                    if(jqXHR['responseJSON']["html"].length > 0){
                        $("#overlay").fadeOut(300);
                    }
                }
            // }
            
            if (typeof completeCallbackFunction === "function") {
                completeCallbackFunction();
            }
        }
    });
}

function doAjaxWithFile(doAjax_params) {

    var url = doAjax_params['url'];
    var requestType = doAjax_params['requestType'];
    var contentType = doAjax_params['contentType'];
    var dataType = doAjax_params['dataType'];
    var data = doAjax_params['data'];
    var beforeSendCallbackFunction = doAjax_params['beforeSendCallbackFunction'];
    var successCallbackFunction = doAjax_params['successCallbackFunction'];
    var completeCallbackFunction = doAjax_params['completeCallbackFunction'];
    var errorCallBackFunction = doAjax_params['errorCallBackFunction'];

    //make sure that url ends with '/'
    /*if(!url.endsWith("/")){
     url = url + "/";
    }*/
    // console.log("url......."+url);
    $.ajax({
        url: BASE_URL+'/'+url,
        crossDomain: true,
        enctype: 'multipart/form-data',
        type: requestType,
        contentType: contentType,
        dataType: dataType,
        data: data,
        processData: false,
        contentType: false,
        beforeSend: function(jqXHR, settings) {
            $("#overlay").fadeIn(300);
            if (typeof beforeSendCallbackFunction === "function") {
                beforeSendCallbackFunction();
            }
        },
        success: function(data, textStatus, jqXHR) {    
            if (typeof successCallbackFunction === "function") {
                successCallbackFunction(data);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (typeof errorCallBackFunction === "function") {
                errorCallBackFunction(errorThrown);
            }

        },
        complete: function(jqXHR, textStatus) {
            // $("#overlay").fadeOut(300);

            if((url == 'buyer/signup_otp_send') || (url == 'supplier/signup_otp_send')){
                $("#overlay").fadeOut(300);
            }

            if(requestType == 'GET'){
                $("#overlay").fadeOut(300);
            }
            
            if(jqXHR['responseJSON']["status"] != 200){
                $("#overlay").fadeOut(300);
                window.scrollTo(0, 0);
            }
            if (typeof completeCallbackFunction === "function") {
                completeCallbackFunction();
            }
        }
    });
}
var OTP_dialog = "";
function responsehandler(url, requestType, data, modal,message,is_show_toast = 1,show_error_msg_form = 1){
    
    var params = $.extend({}, doAjax_params_default);
    params['requestType'] = ""+requestType
    params['url'] = ''+url;
    params['data'] = data;
    params['successCallbackFunction'] = function success(data){
        console.log(data);
        if((data["status"] >= 200) && (data["status"] < 300)){  //Success
            $(''+modal).modal('hide');                
            showToast(""+message,"Success");

            if((url === 'supplier/address/add_address') || (url === 'supplier/address/edit_address')){
                window.location.href = ""+BASE_URL+"/supplier/address";
            }else if(url == 'buyer/product_add_to_cart'){
                window.location.href = ""+BASE_URL+"/buyer/cart";
            }else if(url == 'buyer/login'){
                // window.location.href = ""+BASE_URL+"/buyer/cart";
                var login_open_from = $("#login_open_from").val();
                if(login_open_from == "add_to_cart"){

                    // $("#login_open_from").val("normal");

                    var add_to_cart_button_id = $("#add_to_cart_button_id").val();

                    $("#"+add_to_cart_button_id).click();

                }else{
                    // reload();
                    window.location.href = ""+BASE_URL+"/home_after_login";
                }
            }            
            else{
                reload();
            }
            
        }else{ //Error
            if(is_show_toast == 1){
                showToast(""+data["error"]["message"],"Error");
            }else{
                // $('#'+show_error_msg).html(data["error"]["message"]);
                showErrorMsgInForm(show_error_msg_form,data["error"]["message"].replace("_", " "));
            }
            if(OTP_dialog != undefined){
                $("#otpOk").unbind().one('click', OTP_dialog);
            }
            
        }

    };


    doAjax(params);
}

function responsehandlerWithFiles(url, requestType, data, modal,message,is_show_toast = 1,show_error_msg_form = 1){
    
    var params = $.extend({}, doAjax_params_default);
    params['requestType'] = ""+requestType
    params['url'] = ''+url;
    params['data'] = data;
    params['successCallbackFunction'] = function success(data){

        if((data["status"] >= 200) && (data["status"] < 300)){  //Success
            $(''+modal).modal('hide');                
            showToast(""+message+"...","Success");
            // console.log("url..."+url);
            if(url == 'supplier/register'){
                window.location.href = ""+BASE_URL+"/supplier";
                // console.log(""+BASE_URL+"/supplier");
            }else if(url == 'supplier/TM/add'){
                window.location.href = ""+BASE_URL+"/supplier/TM";
            }else if(url == 'supplier/TM/edit'){
                window.location.href = ""+BASE_URL+"/supplier/TM";
            }else if(url == 'supplier/design_mix/add'){
                window.location.href = ""+BASE_URL+"/supplier/design_mix";
            }else if(url == 'supplier/design_mix/edit'){
                window.location.href = ""+BASE_URL+"/supplier/design_mix";
            }else if(url == 'supplier/profile/edit'){
                window.location.href = ""+BASE_URL+"/supplier/profile";
            }else if(url == 'buyer/register'){
                // window.location.href = ""+BASE_URL+"/buyer/cart";
                var login_open_from = $("#login_open_from").val();
                if(login_open_from == "add_to_cart"){

                    $("#login_open_from").val("normal");

                    var add_to_cart_button_id = $("#add_to_cart_button_id").val();

                    $("#"+add_to_cart_button_id).click();

                }else{
                    reload();
                }
            }else if((url === 'supplier/billing_address/add') || (url === 'supplier/billing_address/edit_address')){
                window.location.href = ""+BASE_URL+"/supplier/billing_address";
            }else{
                reload();
            }
        }else{ //Error
            if(is_show_toast == 1){
                if(data["error"]["message"] != undefined){
                    showToast(""+data["error"]["message"],"Error");
                }else{
                    showToast("Something went wrong. plesase try again","Error");
                }

                if(OTP_dialog != undefined){
                    $("#otpOk").unbind().one('click', OTP_dialog);
                }
                
            }else{
                // $('#'+show_error_msg).html(data["error"]["message"]);
                if(url === 'product_category/add' || url === 'product_category/update'){
                    if(data["status"] == 500){  
                        showErrorMsgInForm(show_error_msg_form,"Category name already exist");
                    }else{
                        showErrorMsgInForm(show_error_msg_form,data["error"]["message"]);
                    }                    
                }else{
                    showErrorMsgInForm(show_error_msg_form,data["error"]["message"]);
                }
                
            }

            if(url == 'supplier/TM/add'){
                $("#TM_save_button").removeAttr("disabled");
            }else if(url == 'supplier/TM/edit'){
                $("#TM_save_button").removeAttr("disabled");
            }

            if(url == 'supplier/design_mix/add'){
                 $("#custom_design_mix_button").removeAttr("disabled");
            }else if(url == 'supplier/design_mix/edit'){
                 $("#custom_design_mix_button").removeAttr("disabled");
            }
        }

    };


    doAjaxWithFile(params);
}

function customResponseHandler(url, requestType, data, callback){
    
    var params = $.extend({}, doAjax_params_default);
    params['requestType'] = ""+requestType
    params['url'] = ''+url;
    params['data'] = data;
    params['successCallbackFunction'] = callback;


    doAjax(params);
}

function customResponseHandlerWithFiles(url, requestType, data, callback){
    
    var params = $.extend({}, doAjax_params_default);
    params['requestType'] = ""+requestType
    params['url'] = ''+url;
    params['data'] = data;
    params['successCallbackFunction'] = callback;


    doAjaxWithFile(params);
}


var country_name_selectID = 'add_site_country';
var state_name_selectID = 'add_site_state';
var city_name_selectID = 'add_site_city';


var country_name_states_selectID = 'country_name_states';
var state_name_city_selectID = 'state_name_city';
var state_name_city_filter_selectID = 'state_name_city_filter';
var typingTimer;                //timer identifier
var doneTypingInterval = 1500;  //time in ms (2 seconds)

function searchCountryState (url,selectedID) {
    
    var inputData = $('#' + selectedID + '_chosen .chosen-search input').val();  //get input data

    $('#' + selectedID + '_chosen .no-results').html('Getting Data = "'+inputData+'"');
    var request_data = '';
    if(url == 'region_states/getCountries'){
        request_data = {"country_name": inputData};
    }else if(url == 'region_cities/getStates'){
        request_data = {"state_name": inputData};
    }else if(url == 'product_sub_category/getCategory'){
        request_data = {"search": inputData};
    }else if(url == 'product_review/getSubCat'){
        var cat_id = $('select[name=categoryId]').val();
        if(cat_id == undefined || cat_id == ''){
            $('#' + selectedID + '_chosen .no-results').html('Please first select category');
        }
        request_data = {"sub_cat_name": inputData,"categoryId":cat_id};
    }else if(url == 'product_review/getProducts'){
        request_data = {"search": inputData};
    }else if(url == 'product_review/getUsers'){
        request_data = {"name": inputData};
    } else if(url == 'support_ticket/getSuppliers'){
        request_data = {"name": inputData};
    }else if(url == 'support_ticket/getBuyers'){
        request_data = {"name": inputData};
    }else if(url == 'support_ticket/getLogistics'){
        request_data = {"name": inputData};
    }else if(url == 'region_cities/getCities'){
        request_data = {"city_name": inputData};
    }else if(url == 'supplier/address/getStates'){
        request_data = {"search": inputData};
    }else if(url == 'supplier/address/getCities'){
        request_data = {"search": inputData};
    }else if(url == 'buyer/getCountry'){
        request_data = {"search": inputData};
    }else if(url == 'buyer/getStates'){
        request_data = {"search": inputData};
    }else if(url == 'buyer/getCities'){
        request_data = {"search": inputData};
    }else if(url == 'buyer/getSiteCities'){
        request_data = {"search": inputData};
    }

    customResponseHandler(
        url, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess

            // data = JSON.parse(data);
            var html = "<option></option>";
        
            $.each(data,function(key,value){
                
                if(url == 'region_states/getCountries'){
                    html += '<option value="'+value["country_name"]+'_'+value['country_id']+'_'+value['country_code']+'">'+value["country_name"]+'</option>';
                }else if(url == 'region_cities/getStates'){
                    html += '<option value="'+value["_id"]+'">'+value["state_name"]+'</option>';
                }else if(url == 'supplier/address/getStates'){
                    html += '<option value="'+value["_id"]+'">'+value["state_name"]+'</option>';
                }else if(url == 'product_sub_category/getCategory'){
                    html += '<option value="'+value["_id"]+'">'+value["category_name"]+'</option>';
                }else if(url == 'product_review/getSubCat'){
                    html += '<option value="'+value["_id"]+'">'+value["sub_category_name"]+'</option>';
                }else if(url == 'product_review/getProducts'){
                    html += '<option value="'+value["_id"]+'">'+value["name"]+'</option>';
                }else if(url == 'product_review/getUsers'){
                    html += '<option value="'+value["_id"]+'">'+value["full_name"]+'</option>';
                }else if(url == 'support_ticket/getSuppliers'){
                    html += '<option value="'+value["_id"]+'">'+value["full_name"]+'</option>';
                }else if(url == 'support_ticket/getBuyers'){
                    html += '<option value="'+value["_id"]+'">'+value["full_name"]+'</option>';
                }else if(url == 'support_ticket/getLogistics'){
                    html += '<option value="'+value["_id"]+'">'+value["full_name"]+'</option>';
                }else if(url == 'region_cities/getCities'){
                    html += '<option value="'+value["_id"]+'">'+value["city_name"]+'</option>';
                }else if(url == 'supplier/address/getCities'){
                    html += '<option value="'+value["_id"]+'">'+value["city_name"]+'</option>';
                }else if(url == 'buyer/getCountry'){
                    html += '<option value="'+value["country_id"]+'">'+value["country_name"]+'</option>';
                }else if(url == 'buyer/getStates'){
                    html += '<option value="'+value["_id"]+'">'+value["state_name"]+'</option>';
                }else if(url == 'buyer/getCities'){
                    html += '<option value="'+value["_id"]+'_'+value['location']['coordinates'][1]+'_'+value['location']['coordinates'][0]+'">'+value["city_name"]+'</option>';
                }else if(url == 'buyer/getSiteCities'){
                    html += '<option value="'+value["_id"]+'" data-lat="'+value["location"]["coordinates"][1]+'" data-long="'+value["location"]["coordinates"][0]+'">'+value["city_name"]+'</option>';
                }
                
            });
            
            
            var alredy_selected = $('#' + selectedID ).val();
            console.log("data.."+$('#' + selectedID ).val()+"...."+$("option:selected",'#' + selectedID ).text());

            if(url == 'buyer/getSiteCities'){
                var data_lat = $('#'+selectedID+' option:selected').attr('data-lat');
                var data_long = $('#'+selectedID+' option:selected').attr('data-long');
                html += '<option value="'+$('#' + selectedID ).val()+'" data-lat="'+data_lat+'" data-long="'+data_long+'">'+$("option:selected",'#' + selectedID ).text()+'</option>';
            }else{
                html += '<option value="'+$('#' + selectedID ).val()+'">'+$("option:selected",'#' + selectedID ).text()+'</option>';
            }

            $('#' + selectedID ).html(html);

            $('#' + selectedID ).val(alredy_selected);
            // Update chosen again after append <option>
            $('#' + selectedID ).trigger("chosen:updated");
            $('#' + selectedID + '_chosen .chosen-search input').val(inputData);

        }
    );

}



function viewMap(lattitude,longitude){
    
    var iframe = '<iframe src="https://maps.google.com/maps?q='+lattitude+', '+longitude+'&z=15&output=embed" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>';
    $('#map_iframe_div').html(iframe);
    $('#view-map-details').modal('show');

}

function showErrorMsgInForm(form_name,message){

    $('form[name="'+form_name+'"] p.error').remove();
    if(form_name == "add_mix_form"){
        $('form[name="'+form_name+'"]').append('<p class="error alert alert-danger" style="text-align: center;">'+message+'</p>');
    }else{
        $('form[name="'+form_name+'"]').prepend('<p class="error alert alert-danger" style="text-align: center;">'+message+'</p>');
        window.scrollTo(0, 0);
    }
    
    

}

function resetForm(form_name,model_id){
    // console.log("testing.."+form_name);
    // $('#us2').locationpicker("location", {latitude: '0', longitude: '0'});
    setTimeout(function() {
        
        $(':input','form[name="'+form_name+'"]')
            .not(':button, :submit, :reset, :hidden, :checkbox, select, :input[name="assigned_at"], :input[name="signup_phone"], :input[name="assigned_at_start_time"], :input[name="assigned_at_end_time"], :input[name="weight_unit_code"], :input[name="weight_unit"]') //:hidden
            .val('')
            .removeAttr('checked')
            .removeAttr('selected')
            .removeAttr('disabled')
            //.removeAttr('readonly')
            ;

        $('select','form[name="'+form_name+'"]').each(function(){
            $(this).removeAttr('disabled');
            var form_array = ['add_cement_stock_form','add_sand_stock_form',
            'add_aggregate_stock_form','add_flyash_stock_form','add_admixture_stock_form',
            'add_cement_rate_form','add_sand_rate_form','add_aggregate_rate_form',
            'add_flyash_rate_form','add_admixture_rate_form'];
            if((form_array.indexOf(form_name)) < -1 ){
                $(this).prop("selectedIndex", 0);
            }
            if(form_name == 'custom_mix_form'){
                $("#mix_cement_brnad").multiselect("deselectAll", false);
                $("#mix_sand_source").multiselect("deselectAll", false);
                $("#mix_aggregate_source").multiselect("deselectAll", false);
            }
            // console.log("$(this)...."+$(this).val());
        });
        
        $('#add_site_state').html('');    
        $('#add_site_state').attr('readonly','readonly');
        $("#add_site_state").trigger("chosen:updated");

        $('#add_site_city').html('');
        $('#add_site_city').attr('readonly','readonly');
        $("#add_site_city   ").trigger("chosen:updated");

        // $('form[name="'+form_name+'"]').find('[type="tel"]').val('+91');

        updateChosen();
        reset = 1;
        // var form_select_first_value = $('form[name="'+form_name+'"]').find('select[class="form-control-chosen"] option:first').val();
        // console.log("form_select_first_value..."+form_select_first_value);
        $('form[name="'+form_name+'"]').find('select[class="form-control-chosen"]').val($("select[class='form-control-chosen'] option:first").val()).trigger("chosen:updated");
        $('form[name="'+form_name+'"]').find('select[class="form-control-chosen valid"]').val($("select[class='form-control-chosen'] option:first").val()).trigger("chosen:updated");
        reset = 0;
        console.log("test...2..."+model_id);
        var title = $('#'+model_id+' .modal-title').html();
        console.log("test...2..."+title);
        if(title != undefined){
            title = title.replace('Edit','Add');
            $('#'+model_id+' .modal-title').html(title);
        }

        $('#'+form_name+'_type').val('add');
        // $('form[name="'+form_name+'"] .error').removeClass('error');
        $('form[name="'+form_name+'"] label.error').html('');
        $('form[name="'+form_name+'"] input.error').removeClass("error");
        $('form[name="'+form_name+'"] select.error').removeClass("error");
        $('form[name="'+form_name+'"] textarea.error').removeClass("error");
        $('form[name="'+form_name+'"] .fileuploader-input-caption.error').removeClass("error");
        $('form[name="'+form_name+'"] .chosen-single.error').removeClass("error");
        $('label.error').remove();
        $('p.error').remove();
        // $('span.error').remove();
        $('span.error').html('');

        if((form_name == 'add_site_form') || (form_name == 'add_coupon')){
            $('label.error').remove();
        }

        


     }, 200);     

}


function resetEditForm(form_name){
    // console.log("testing.."+form_name);

    setTimeout(function() {
        
        $(':input','form[name="'+form_name+'"]')
            .not(':button, :submit, :reset, :hidden') //:hidden
            .removeClass('error');

        
        $('form[name="'+form_name+'"] label.error').html('');
        $('form[name="'+form_name+'"] input.error').removeClass("error");
        $('form[name="'+form_name+'"] select.error').removeClass("error");
        $('form[name="'+form_name+'"] textarea.error').removeClass("error");
        $('form[name="'+form_name+'"] .fileuploader-input-caption.error').removeClass("error");
        $('form[name="'+form_name+'"] .chosen-single.error').removeClass("error");
        $('p.error').remove();
        $('label.error').remove();

     }, 200);     

}


function getCityByState(state_id,city_id_select){

    var request_data = {'state_id':state_id};

    customResponseHandler("buyer/getCityByState",
                    "GET", 
                    request_data, 
                    function response(data){
                        // console.log(data);
                        // var message = data["data"]["message"];                        
                        var state_data = data["data"];
                        var html = '<option disabled="disabled" selected="selected">Select City</option>';
                        $.each(state_data,function(key,value){

                            html += '<option value="'+value["_id"]+'" data-lat="'+value["location"]["coordinates"][1]+'" data-long="'+value["location"]["coordinates"][0]+'">'+value["city_name"]+'</option>'; 

                        });

                        $('#'+city_id_select).html(html);

                        $('#'+city_id_select).trigger('chosen:updated');
                    }
                );

}

function getCityByStateLocationPopUp(state_id,city_id_select){

    var request_data = {'state_id':state_id};

    customResponseHandler("buyer/getCityByState",
                    "GET", 
                    request_data, 
                    function response(data){
                        // console.log(data);
                        // var message = data["data"]["message"];                        
                        var state_data = data["data"];
                        var html = '<option disabled="disabled" selected="selected">Select City</option>';
                        $.each(state_data,function(key,value){

                            html += '<option value="'+value["_id"]+'_'+value["location"]["coordinates"][1]+'_'+value["location"]["coordinates"][0]+'_'+value["city_name"]+'" data-lat="'+value["location"]["coordinates"][1]+'" data-long="'+value["location"]["coordinates"][0]+'">'+value["city_name"]+'</option>'; 

                        });

                        $('#'+city_id_select).html(html);

                        $('#'+city_id_select).trigger('chosen:updated');
                    }
                );

}

function getCityByStateSupplierAddress(state_id,city_id_select){

    var request_data = {'state_id':state_id};

    customResponseHandler("buyer/getCityByState",
                    "GET", 
                    request_data, 
                    function response(data){
                        // console.log(data);
                        // var message = data["data"]["message"];                        
                        var state_data = data["data"];
                        var html = '<option disabled="disabled" selected="selected">Select City</option>';
                        $.each(state_data,function(key,value){

                            html += '<option value="'+value["_id"]+'" data-lat="'+value["location"]["coordinates"][1]+'" data-long="'+value["location"]["coordinates"][0]+'">'+value["city_name"]+'</option>'; 

                        });

                        $('#'+city_id_select).html(html);

                        $('#'+city_id_select).trigger('chosen:updated');
                    }
                );

}

function getVehicleSubcatByCatId(vehicle_cat_id,city_id_select){

    var request_data = {'vehicle_cat_id':''+vehicle_cat_id};

    customResponseHandler("supplier/TM/getTMSubCatByCatId",
                    "GET", 
                    request_data, 
                    function response(data){
                        // console.log(data);
                        // var message = data["data"]["message"];                        
                        var state_data = data["data"];
                        var html = '<option disabled="disabled" selected="selected">Select Vehicle Sub Category</option>';
                        $.each(state_data,function(key,value){

                            html += '<option value="'+value["_id"]+'" data-min-load="'+value['min_load_capacity']+'" data-max-load="'+value['max_load_capacity']+'">'+value["sub_category_name"]+'</option>'; 

                        });

                        $('#'+city_id_select).html(html);

                        $('#'+city_id_select).trigger('chosen:updated');
                    }
                );
                

}


function updateChosen(){

    $("#state_name_city").removeAttr("disabled");
    $("#state_name_city").trigger("chosen:updated");

    $("#product_category_subcategory_list_add_form").removeAttr("disabled");
    $("#product_category_subcategory_list_add_form").trigger('chosen:updated');

}


function validateLogin(email_mobile,pass){

    if(email_mobile.length == 0){
        $("#email_mobile_error").html("This field is required");
        $("#mobile_or_email").addClass("error");
        return false;
      }else if(isNaN(email_mobile)){
        //validate email address
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(!email_mobile.match(mailformat)){
            $("#email_mobile_error").html("Please enter valid mobile/email");
            $("#mobile_or_email").addClass("error");
            return false;
        }
      }else{
        //validate Mobile number
        var is_sign = email_mobile.charAt(0);
        if(is_sign !== '+'){
            $("#email_mobile_error").html("Please add mobile with country code");
            $("#mobile_or_email").addClass("error");
            return false;
        }
        // email_mobile = email_mobile.trim().replace("+","");
        // var phoneno = /^\d{12}$/;
        // if (!email_mobile.match(phoneno)) {
        //     // alert("Phone number should be 10 digits");
    
        //     $("#email_mobile_error").html("Please add mobile with country code");
        //     return false;
        // }
        console.log("email_mobile.."+email_mobile);
        var mobile_length = email_mobile.length;
        console.log("mobile_length.."+mobile_length);
        if(mobile_length < 13){
            $("#email_mobile_error").html("Please enter valid mobile number");
            $("#mobile_or_email").addClass("error");
            return false;
        }

        if(mobile_length > 13){
            $("#email_mobile_error").html("Please enter no more than 10 characters.");
            $("#mobile_or_email").addClass("error");
            return false;
        }

        if((email_mobile % 1) != 0){
            $("#email_mobile_error").html("No decimal no allow");
            $("#mobile_or_email").addClass("error");
            return false;
        }
        
        if((email_mobile.slice(-1)) == '.'){
            $("#email_mobile_error").html("No decimal no allow");
            $("#mobile_or_email").addClass("error");
            return false;
        }
    
    
      }
      $("#email_mobile_error").html("");
      $("#mobile_or_email").removeClass("error");
      if(pass.length == 0){
        $("#password_error").html("This field is required");
        $("#pass").addClass("error");
        return false;
      }else if(pass.length < 6){
        $("#password_error").html("Please enter at least 6 characters");
        $("#pass").addClass("error");
        return false;
      }

      
      $("#password_error").html("");
      $("#pass").removeClass("error");
      return true;
}

function validateMobileEmail(email_mobile){

    if(email_mobile.length == 0){
        $("#email_mobile_error").html("This field is required");
        $("#mobile_or_email").addClass("error");
        return false;
      }else if(isNaN(email_mobile)){
        //validate email address
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(!email_mobile.match(mailformat)){
            $("#email_mobile_error").html("Please enter valid email address");
            $("#mobile_or_email").addClass("error");
            return false;
        }
        
      }else{
        console.log("email_mobile.."+email_mobile);
        //validate Mobile number
        var is_sign = email_mobile.charAt(0);
        if(is_sign !== '+'){
            $("#email_mobile_error").html("Please add mobile with country code");
            $("#mobile_or_email").addClass("error");
            return false;
        }

        var mobile_length = email_mobile.length;
        console.log("mobile_length.."+mobile_length);
        if(mobile_length < 13){
            $("#email_mobile_error").html("Please enter valid mobile number");
            $("#mobile_or_email").addClass("error");
            return false;
        }

        if(mobile_length > 13){
            $("#email_mobile_error").html("Please enter no more than 10 characters.");
            $("#mobile_or_email").addClass("error");
            return false;
        }

        console.log("No decimal 1");
        if((email_mobile % 1) != 0){
            console.log("No decimal 2");
            $("#email_mobile_error").html("No decimal no allow");
            $("#mobile_or_email").addClass("error");
            return false;
        }
        
        if((email_mobile.slice(-1)) == '.'){
            $("#email_mobile_error").html("No decimal no allow");
            $("#mobile_or_email").addClass("error");
            return false;
        }

        // email_mobile = email_mobile.trim().replace("+","");
        // var phoneno = /^\d{12}$/;
        // if (!email_mobile.match(phoneno)) {
        //     // alert("Phone number should be 10 digits");
    
        //     $("#email_mobile_error").html("Please add mobile with country code");
        //     return false;
        // }
    
    
      }
      $("#email_mobile_error").html("");
      $("#mobile_or_email").removeClass("error");
      return true;
}


function validateOTP(otp_field){

    if(otp_field.length == 0){
        $("#otp_error").html("This field is required");
        $("#otp_code").addClass("error");
        return false;
    }
    
    if((otp_field.length > 6) || (otp_field.length < 6)){
        $("#otp_error").html('OTP length must be 6 characters long');
        $("#otp_code").addClass("error");
        return false;
    }

    if(isNaN(otp_field)){
        
        $("#otp_error").html("Please enter valid number");
        $("#otp_code").addClass("error");
        return false;
    }

    return true;

}

$(document).ready(function(){


    $('#state_name_city').on('change', function (e) {
        
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        getCityByStateLocationPopUp(valueSelected,"state_name_city_filter");

    });
    
    $('#state_name_city_address').on('change', function (e) {
        
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        getCityByStateSupplierAddress(valueSelected,"state_name_city_filter");

    });
    
    $('#vehicleCategoryId').on('change', function (e) {
        
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        getVehicleSubcatByCatId(valueSelected,"vehicleSubCategoryId");

    });


    $.validator.addMethod('minStrict', function (value, el, param) {
        return value > param;
    }, "Minimum value should be 1");

    jQuery.validator.addMethod("noSpace", function(value, element) { 
        console.log("value.trim().length..."+value.trim().length+"....."+(value.trim().length != 0)+"..."+(value.trim() != ""));
        var is_valid = true;
        if(value.length > 0){
            return (value.trim().length != 0) && (value.trim() != ""); 
        }
        // return value.trim().length != 0 && value != ""; 
        return is_valid; 
      }, "No space please and don't leave it empty");

    jQuery.validator.addMethod("mobile_validation", function(value, element) { 

        if(value.length > 0){
            var is_sign = value.charAt(0);
            if(is_sign !== '+'){
                // $("#email_mobile_error").html("Please add mobile with country code");
                return false;
            }
            email_mobile = value.trim().replace("+","");
            if(isNaN(email_mobile)){
            
                // $("#otp_error").html("Please enter valid number");
                return false;
            }
        }
        

        return true;
    // return value.trim().length != 0 && value != ""; 
    }, "Please add valid mobile with country code");

    jQuery.validator.addMethod("a_z_pattern", function(value, element) { 
        // var patt = new RegExp("/^[a-z. ]*$/i");
        var name_pattern = /^[a-z. ]*$/i;
        console.log("test...."+value.match(name_pattern));
        return value.match(name_pattern); 
      }, "Please use only letters(a-z) and periods");


      $.validator.addMethod('lessThanEqual', function(value, element, param) {
        //   console.log("tests...."+value+"...."+param+".....");
          this.val = param;
        return this.optional(element) || parseInt(value) <= parseInt(param);
    }, function() {
        // return "The value must be less than " + this.val;
        return "Delivery Qty must be less than or equal to " + this.val+ " Cu.Mtr";
      });
      
      $.validator.addMethod('greaterThanEqual', function(value, element, param) {
        //   console.log("tests...."+value+"...."+param+".....");
          this.val = param;
        return this.optional(element) || parseInt(value) >= parseInt(param);
    }, function() {
        // return "The value must be greater than " + this.val;
        return "Delivery Qty must be greater than or equal to " + this.val + " Cu.Mtr";
      });

      $.validator.addMethod("noDecimal", function(value, element) {
        //   console.log("value..."+value+"..."+(value % 1)+"++++"+value.slice(-1));
          if(value.slice(-1) == '.'){
              return false;
          }
		return !(value % 1);
	}, "No decimal numbers");

      jQuery.validator.addMethod("mobile_or_email_validate", function(value, element) { 
        // var patt = new RegExp("/^[a-z. ]*$/i");
        
        var email_mobile = value;
        $( ".invalid" ).remove();
        console.log("testse.."+email_mobile.length);
        console.log("mobile_or_email_validate.."+$("#mobile_or_email").val().length);
        if(email_mobile.length > 0){
            if(isNaN(email_mobile)){
                //validate email address
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if(!email_mobile.match(mailformat)){
                    $(element).after('<label class="error invalid">Please enter valid email address</label>');
                    // $("#email_mobile_error").html("Please enter valid email address");
                    return false;
                }
            }else{
                //validate Mobile number
                var is_sign = email_mobile.charAt(0);
                if(is_sign !== '+'){
                    $(element).after('<label class="error invalid">Please add mobile with country code</label>');
                    // $("#email_mobile_error").html("Please add mobile with country code");
                    return false;
                }
        
                var mobile_length = email_mobile.length;
                console.log("mobile_length.."+mobile_length);
                if(mobile_length < 13){
                    $(element).after('<label class="error invalid">Please enter valid mobile number</label>');
                    // $("#email_mobile_error").html("Please enter valid mobile number");
                    return false;
                }
        
                if(mobile_length > 13){
                    $(element).after('<label class="error invalid">Please enter no more than 10 characters.</label>');
                    // $("#email_mobile_error").html("Please enter no more than 15 characters.");
                    return false;
                }
            }
        }
        $( ".invalid" ).remove();
        $("#email_mobile_error").html("");
        return true; 
      }, "");
      
      jQuery.validator.addMethod("email_validate", function(value, element) { 
        // var patt = new RegExp("/^[a-z. ]*$/i");
        
        var email_mobile = value;
        $( ".invalid" ).remove();
        console.log("testse.."+email_mobile.length);
        
        if(email_mobile.length > 0){
            // if(isNaN(email_mobile)){
                //validate email address
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if(!email_mobile.match(mailformat)){
                    // $("#supplier_email-error").remove();
                    // var myEle = document.getElementById("supplier_email-error");
                    // if(myEle){
                    //     $(".error .invalid").remove();
                    // }else{
                    //     $("#supplier_email-error").remove();
                    // $("#supplier_email-error").remove();
                    //     $(element).after('<label class="error invalid">Please enter valid email address</label>');
                    // }
                    // $("#email_mobile_error").html("Please enter valid email address");
                    return false;
                }
            // }else{
            //     //validate Mobile number
            //     var is_sign = email_mobile.charAt(0);
            //     if(is_sign !== '+'){
            //         $(element).after('<label class="error invalid">Please add mobile with country code</label>');
            //         // $("#email_mobile_error").html("Please add mobile with country code");
            //         return false;
            //     }
        
            //     var mobile_length = email_mobile.length;
            //     console.log("mobile_length.."+mobile_length);
            //     if(mobile_length < 13){
            //         $(element).after('<label class="error invalid">Please enter valid mobile number</label>');
            //         // $("#email_mobile_error").html("Please enter valid mobile number");
            //         return false;
            //     }
        
            //     if(mobile_length > 13){
            //         $(element).after('<label class="error invalid">Please enter no more than 10 characters.</label>');
            //         // $("#email_mobile_error").html("Please enter no more than 15 characters.");
            //         return false;
            //     }
            // }
        }
        $( ".invalid" ).remove();
        $("#email_mobile_error").html("");
        
        return true; 
      }, "");


      $.validator.addMethod(
        "gst",
        function (value3, element3) {
            var gst_value = value3.toUpperCase();
            // var reg = /^([0-9]{2}[a-zA-Z]{4}([a-zA-Z]{1}|[0-9]{1})[0-9]{4}[a-zA-Z]{1}([a-zA-Z]|[0-9]){3}){0,15}$/;
            var reg = /^([0-9]){2}([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([0-9]){1}([a-zA-Z]){1}([0-9]){1}?$/;

            if (this.optional(element3)) {
                return true;
            }

            if (gst_value.match(reg)) {
                return true;
            } else {
                return false;
            }
        },
        "Please specify a valid GSTTIN Number"
    );

    $.validator.addMethod(
        "pan",
        function (value3, element3) {
            var pan_value = value3.toUpperCase();
            // console.log(pan_value); 
            // var reg = /([A-Z]){5}([0-9]){4}([A-Z]){1}$/;
            // var reg = "[A-Z]{3}[ABCFGHLJPTF]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}";
            var reg = /[a-zA-Z]{3}[PCHFATBLJG]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/;
            // var rx = "[A-Z]{3}([CHFATBLJGP])(?:(?<=P)" + c1 + "|(?<!P)" + c2 + ")[0-9]{4}[A-Z]";

            if (this.optional(element3)) {
                return true;
            }

            if (pan_value.match(reg)) {
                return true;
            } else {
                return false;
            }
        },
        "Please specify a valid PAN Number"
    );

    // jQuery.validator.addMethod("extension", function(value, element, param) {
    //     param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
    //     return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
    // }, jQuery.format("Please enter a value with a valid extension."));

      

    // $(".modal").on('show.bs.modal', function(e){
    //     var modal_id = $(this)[0].id;
        
    //     var title = $('#'+modal_id+' .modal-title').html();
    //     var modal_form_name = $('#'+modal_id+' form').attr("name");
    //     console.log("test");
    //     if(title.includes('Edit')){
    //         resetForm(modal_form_name,modal_id)
    //     }

    // });


    $('#mobile_or_email').keyup(function(event){

        var KeyID = event.keyCode;
        
        var email_mobile = $('#mobile_or_email').val();
        console.log("test...mobile_or_email.."+email_mobile+"..."+isNaN(email_mobile));

        if(email_mobile.length == 0){
            $( ".invalid" ).remove();
        }

        if(isNaN(email_mobile)){
            email_mobile = email_mobile.replace("+91", "");
            $('#mobile_or_email').val(email_mobile);
        }else if(email_mobile.length == 0){
            $('#mobile_or_email').val("");
        }else{
            if(!email_mobile.startsWith("+91")){
                console.log("start with +91");
                if(email_mobile.startsWith("+9")){  

                    console.log("start with +9");
                    email_mobile = email_mobile.replace("+9", "+91");
                    $('#mobile_or_email').val(""+email_mobile);

                }else if(email_mobile.startsWith("+1")){

                    console.log("start with +1");
                    email_mobile = email_mobile.replace("+1", "+91");
                    $('#mobile_or_email').val(""+email_mobile);

                }else{
                    $('#mobile_or_email').val("+91"+email_mobile);                
                }
                
                
            }
            else if(email_mobile == "+91"){
                $('#mobile_or_email').val("");
            }
            
        }

    });
    
    $('#mobile_or_email_buyer_signup').keyup(function(event){

        var KeyID = event.keyCode;

        
        var email_mobile = $('#mobile_or_email_buyer_signup').val();
        console.log("test...mobile_or_email_buyer_signup.."+email_mobile+"..."+isNaN(email_mobile));

        if(isNaN(email_mobile)){
            email_mobile = email_mobile.replace("+91", "");
            $('#mobile_or_email_buyer_signup').val(email_mobile);
        }else if(email_mobile.length == 0){
            $('#mobile_or_email_buyer_signup').val("");
        }else{
            if(!email_mobile.startsWith("+91")){
                
                $('#mobile_or_email_buyer_signup').val("+91"+email_mobile);
                
                
            }else if(email_mobile == "+91"){
                $('#mobile_or_email_buyer_signup').val("");
            }
            
        }

    });


    $('.mobile_validate').keyup(function(event){

        var KeyID = event.keyCode;
        
        var email_mobile = $(this).val();
        console.log("test...mobile_or_email.."+email_mobile+"..."+isNaN(email_mobile));

        if(isNaN(email_mobile)){
            email_mobile = email_mobile.replace("+91", "");
            $(this).val(email_mobile);
        }else if(email_mobile.length == 0){
            $(this).val("");
        }else{
            if(!email_mobile.startsWith("+91")){
                console.log("start with +91");
                if(email_mobile.startsWith("+9")){  

                    console.log("start with +9");
                    email_mobile = email_mobile.replace("+9", "+91");
                    $(this).val(""+email_mobile);

                }else if(email_mobile.startsWith("+1")){

                    console.log("start with +1");
                    email_mobile = email_mobile.replace("+1", "+91");
                    $(this).val(""+email_mobile);

                }else{
                    $(this).val("+91"+email_mobile);                
                }
                
                
            }
            else if(email_mobile == "+91"){
                $(this).val("");
            }
            
        }

    });

    $('.modal').modal({
        keyboard: false,
        backdrop: "static",
        show:false
    })

  });

  function onlyNumbers(evt) {
    var theEvent = evt || window.event;
  
    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
    // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }


  var autocomplete;
  function initialize() {
    var input = document.getElementById('searchTextField');

    // const options = {
    //     componentRestrictions: { country: "in" }
    //   };

    autocomplete = new google.maps.places.Autocomplete(input);
  
    autocomplete.setComponentRestrictions({
      country: ["in"],
    });
  
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
          var place = autocomplete.getPlace();
          console.log("place....");
          console.log(place);

          $address_components = place.address_components;

          console.log($address_components);

          $city_name = '';
          $state_name = '';
          $country_name = '';
          $lat = '';
          $long = '';

          $.each($address_components, function(key, value){
            console.log("value.types[0]::"+value.types[0]);
            if((value.types[0] == 'administrative_area_level_2')){//|| (value.types[0] == 'locality')
                $city_name = value.long_name;
            }

            if($city_name.length == 0){

                if((value.types[0] == 'administrative_area_level_3')){//|| (value.types[0] == 'locality')
                    $city_name = value.long_name;
                }

            }

            if($city_name.length == 0){

                if((value.types[0] == 'locality')){//|| (value.types[0] == 'locality')
                    $city_name = value.long_name;
                }

            }
            
            if(value.types[0] == 'administrative_area_level_1'){
                $state_name = value.long_name;
            }


          });
          console.log("city_name::"+$city_name);
          
          $place_name = place.name;
        //   $city_name = 

            $lat = place.geometry.location.lat();
            $long = place.geometry.location.lng();

            console.log($lat);
            console.log($long);
            console.log($place_name);

        //   document.getElementById('city2').value = place.name;
        //   document.getElementById('cityLat').value = place.geometry.location.lat();
        //   document.getElementById('cityLng').value = place.geometry.location.lng();

          $("#clear_location").show();
          $("#location_error").html("");
          if($city_name.length > 0){
            if($city_name == 'Ahmedabad'){
                getRegionalDetails($state_name, $city_name, $lat, $long,"yes");
            }else{
                $("#location_error").html("Currently we are serving Ahmedabad only");
            }
          }else{
              $("#location_error").html("Please select city/locality");
          }
      });
  }
  
//   google.maps.event.addDomListener(window, 'load', initialize);
window.addEventListener('load', initialize);

  function placeCurrentLocation(){

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                        // console.log("position......................");
                        // console.log(position);
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    // infoWindow.setPosition(pos);
                    // infoWindow.setContent("Location found.");
                    // infoWindow.open(_map);
                    // _map.setCenter(pos);

                    // $('#us2').locationpicker("location", {latitude: pos.lat, longitude: pos.lng},

                    // );

                    var latlng = new google.maps.LatLng(pos.lat, pos.lng);
                    // This is making the Geocode request
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'latLng': latlng },  (results, status) =>{
                        if (status !== google.maps.GeocoderStatus.OK) {
                            alert(status);
                        }
                        // This is checking to see if the Geoeode Status is OK before proceeding
                        if (status == google.maps.GeocoderStatus.OK) {
                            console.log(results);
                            var address = (results[0]);

                            autocomplete.set('place', address || []);
                            // autocomplete.set('place.geometry.location.lat', pos.lat || []);
                            // autocomplete.set('place.geometry.location.lat', pos.lng || []);
                            $("#searchTextField").val(results[0].formatted_address);
                            $("#searchTextField").text(results[0].formatted_address);
                            // google.maps.event.trigger(autocomplete, 'place_changed');
                        }
                    });

                },
                () => {
                // handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        } else {
        // Browser doesn't support Geolocation
        // handleLocationError(false, infoWindow, map.getCenter());
        }

    

  }

  function getRegionalDetails($state_name, $city_name, $lat, $long,$is_search, address_components, formatted_address = ''){

    var request_data = {

        "state_name" : $state_name,
        "city_name" : $city_name,
        "lat" : $lat,
        "long" : $long,
        "is_search" : $is_search

    }
    console.log("request_data");
    console.log("request_data");
    console.log(request_data);
    customResponseHandler("get_regional_details",
        "GET", 
        request_data, 
        function response(data){
            console.log(data);
            
            if((data["status"] >= 200) && (data["status"] < 300)){
                
                $final_data = data["data"];

                if($is_search == "not"){

                    

                    console.log(formatted_address);

                    var result = {};
                    for (var i = address_components.length - 1; i >= 0; i--) {
                        var component = address_components[i];
                        console.log("formatted_address...........1");
                        console.log(component);
                        if (component.types.indexOf("postal_code") >= 0) {
                            result.postalCode = component.short_name;
                        } else if (component.types.indexOf("street_number") >= 0) {
                            result.streetNumber = component.short_name;
                        } else if (component.types.indexOf("route") >= 0) {
                            result.streetName = component.short_name;
                        } else if (component.types.indexOf("locality") >= 0) {
                            result.city = component.short_name;
                        } else if (component.types.indexOf("sublocality") >= 0) {
                            result.district = component.short_name;
                        } else if (component.types.indexOf("administrative_area_level_1") >= 0) {
                            result.stateOrProvince = component.short_name;
                        } else if (component.types.indexOf("country") >= 0) {
                            result.country = component.short_name;
                        }
                    }
                    result.addressLine1 = [ result.streetNumber, result.streetName ].join(" ").trim();
                    console.log("formatted_address result");
                    console.log(result);
                    addressParts = formatted_address.split(', ');
                    // console.log(addressParts);

                    var address_line_1 = "";
                    var address_line_2 = "";
                    var count = 0;
                    $.each(addressParts,function(key, value){

                        if(count < (addressParts.length - 3)){

                            if(count == 0){
                                address_line_1 = value;
                            }else{
                                address_line_2 += value+", ";
                            }

                        }

                        count++;
                    });
                    address_line_2 = address_line_2.replace(/(\s*,?\s*)*$/, "");
                    console.log("addressParts");
                    console.log(address_line_1);
                    console.log(address_line_2);

                    $("#address_line1").val(address_line_1);
                    $("#address_line2").val(address_line_2);
                    $("#pincode").val(result.postalCode);
                    // $("#add_site_country").val($final_data['country_name']);
                    // $("#add_site_state").val($final_data['state_name']);
                    $("#add_site_country").html(
                        "<option value="+$final_data['country_id']+">"+$final_data['country_name']+"</option>"
                    );
                    $("#add_site_state").html(
                        "<option value="+$final_data['state_id']+">"+$final_data['state_name']+"</option>"
                    );
                    $("#add_site_city").html(
                        "<option value="+$final_data['_id']+">"+$final_data['city_name']+"</option>"
                    );

                }else{

                    $("#location_error").html("");
                    $("#product_lat").val($lat);
                    $("#product_long").val($long);
                    $("#location_state_name").val($state_name);
                    $("#location_city_name").val($city_name);
                    $("#location_state_id").val($final_data['state_id']);
                    $("#location_city_id").val($final_data['_id']);


                }

                // checkSuppliersAvailability($lat,$long,$final_data['state_id']);


            }else{
                showToast(""+data["error"]["message"],"Error");
                // $('#sign-up-form-1_error').html(data["error"]["message"]);
            }

        }
    );


  }

  function checkSuppliersAvailability(lat,long,state_id){

    $("#location_error").html("");
    $("#address_location_error").html("");

    var form_values = {
        "lat":lat,
        "long":long,
        "state_id":state_id
    };

    customResponseHandler("check_supplier_availability",
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            $("#location_error").html(data["message"]);
                            $("#address_location_error").html(data["message"]);
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            
                        }

                    }
                );

}



  var is_concrete_density_error = 0;
  var is_admixture_percent_error = 0;
  var is_water_ratio_error = 0;
  var is_fly_ash_error = 0;
  function concreteDensity(){

    console.log("concreteDensity");
    $("#concrete_density_error").html("");
    $("#concrete_density_success").html("");
    $("#is_concrete_density_error").val(0);

    var is_supplier = $("#is_supplier").val() != null ? $("#is_supplier").val() : '';

    // var percentage_plus_minus = 6.0;
    var percentage_plus_minus = 7.70;

    if(is_supplier == 'yes'){

        percentage_plus_minus = 6.25;

    }
    

    var concrete_grade_id = $("#concrete_grade_id").val() != null ? $("#concrete_grade_id").val() : '';
    var mix_cement_brnad = $("#mix_cement_brnad").val() != null ? $("#mix_cement_brnad").val() : '';
    var cement_grade_id = $("#cement_grade_id").val() != null ? $("#cement_grade_id").val() : '';
    var mix_sand_source = $("#mix_sand_source").val() != null ? $("#mix_sand_source").val() : '';
    var mix_aggregate_source = $("#mix_aggregate_source").val() != null ? $("#mix_aggregate_source").val() : '';
    var mix_sub_cat_1 = $("#mix_sub_cat_1").val() != null ? $("#mix_sub_cat_1").val() : '';
    var mix_sub_cat_2 = $("#mix_sub_cat_2").val() != null ? $("#mix_sub_cat_2").val() : '';
    var ad_mixture_brand_id = $("#ad_mixture_brand_id").val() != null ? $("#ad_mixture_brand_id").val() : '';
    var ad_mixture_category_id = $("#ad_mixture_category_id").val() != null ? $("#ad_mixture_category_id").val() : '';
    var mix_fly_ash_brand = $("#mix_fly_ash_brand").val() != null ? $("#mix_fly_ash_brand").val() : '';
    var custom_mix_with_TM = $("#custom_mix_with_TM").val() != null ? $("#custom_mix_with_TM").val() : '';
    var custom_mix_with_CP = $("#custom_mix_with_CP").val() != null ? $("#custom_mix_with_CP").val() : '';
    var address_id = $("#address_id").val() != null ? $("#address_id").val() : '';
    var design_mix_number = $("#design_mix_number").val() != null ? $("#design_mix_number").val() : '';
    var is_supplier_add_edit = $("#is_supplier_add_edit").val() != null ? $("#is_supplier_add_edit").val() : '';
    var selling_price = $("#selling_price").val() != null ? $("#selling_price").val() : '';
    var product_description = $("#product_description").val() != null ? $("#product_description").val() : '';
    
    console.log("concrete_grade_id:"+concrete_grade_id);
    console.log("address_id:"+address_id);
    console.log("is_supplier:"+is_supplier);
    console.log("is_supplier_add_edit:"+is_supplier_add_edit);
    console.log("mix_cement_brnad:"+mix_cement_brnad);
    console.log("cement_grade_id:"+cement_grade_id);
    console.log("mix_sand_source:"+mix_sand_source);
    console.log("mix_aggregate_source:"+mix_aggregate_source);
    console.log("mix_sub_cat_1:"+mix_sub_cat_1);
    console.log("mix_sub_cat_2:"+mix_sub_cat_2);
    console.log("ad_mixture_brand_id:"+ad_mixture_brand_id);
    console.log("ad_mixture_category_id:"+ad_mixture_category_id);
    console.log("mix_fly_ash_brand:"+mix_fly_ash_brand);
    console.log("custom_mix_with_TM:"+custom_mix_with_TM);
    console.log("custom_mix_with_CP:"+custom_mix_with_CP);
    console.log("selling_price:"+selling_price);
    console.log("product_description:"+product_description);

    var custom_cement_qty = $("#custom_cement_qty").val() != '' ? $("#custom_cement_qty").val() : 0;
    var custom_sand_qty = $("#custom_sand_qty").val() != '' ? $("#custom_sand_qty").val() : 0;
    var custom_aggregate1_qty = $("#custom_aggregate1_qty").val() != '' ? $("#custom_aggregate1_qty").val() : 0;
    var custom_aggregate2_qty = $("#custom_aggregate2_qty").val() != '' ? $("#custom_aggregate2_qty").val() : 0;
    var custom_admixture_qty = $("#custom_admixture_qty").val() != '' ? $("#custom_admixture_qty").val() : 0;
    var custom_flyash_qty = $("#custom_flyash_qty").val() != undefined ? $("#custom_flyash_qty").val() : 0;
    var custom_water_qty = $("#custom_water_qty").val() != '' ? $("#custom_water_qty").val() : 0;

    console.log("custom_cement_qty:"+custom_cement_qty);

    if((custom_cement_qty > 0) && (custom_water_qty > 0)){
        $("#water_quantity_error").html("");
        $("#water_quantity_success").html("");
        var W_C_ratio = (custom_water_qty / custom_cement_qty);

        

        if((W_C_ratio.toFixed(2) < 0.35) || (W_C_ratio.toFixed(2) > 0.55)){

            var min = (custom_cement_qty * 0.35).toFixed(2);
            var max = (custom_cement_qty * 0.55).toFixed(2);

            $("#water_quantity_error").html("Water qty should be between "+min+" - "+ max);
            // $("#water_quantity_success").html("Water qty should be between "+min+" - "+ max);

            // $("#water_quantity_error").html("Water / Cement Ration must be between 0.35% - 0.55%");

            $("#is_WC_ratio_error").val(1);

            is_water_ratio_error = 1;

        }else{
            $("#water_quantity_success").html("Water/Cement Ratio is "+W_C_ratio.toFixed(2)+"");

            $("#is_WC_ratio_error").val(0);

            is_water_ratio_error = 0;
        }

    }else{
        if(custom_cement_qty > 0){
            var min = (custom_cement_qty * 0.35).toFixed(2);
            var max = (custom_cement_qty * 0.55).toFixed(2);

            $("#water_quantity_error").html("");
            $("#water_quantity_success").html("Water qty should be between "+min+" - "+ max);


            
        }else{
            $("#water_quantity_error").html("");
            $("#water_quantity_success").html("");
        }
        // $("#water_quantity_error").html("Cement and Water QTY must be greater than 0");
        is_water_ratio_error = 1;
    }

    var total_density = parseInt(custom_cement_qty) +
                        parseInt(custom_sand_qty) +
                        parseInt(custom_aggregate1_qty) +
                        parseInt(custom_aggregate2_qty) +
                        // parseInt(custom_admixture_qty) +
                        parseInt(custom_water_qty);

    if(total_density > 0){
        $("#concrete_density").show();    
        $("#concrete_density_footer").show();    
    }else{
        $("#concrete_density").hide();  
        $("#concrete_density_footer").hide();  
    }
    $("#concrete_density").html(": <b>"+total_density+" KG</b>");
    $("#concrete_density_footer").html(": <b>"+total_density+" KG</b>");

    if((total_density < 2400) || (total_density > 2550) ){

        // $("#concrete_density_error").html("Concrete Density should be between 2400 to 2550");
        // $("#concrete_density_success").html("");

        $("#concrete_density").addClass("error");
        $("#concrete_density").removeClass("text-success");
        
        $("#concrete_density_footer").addClass("error");
        $("#concrete_density_footer").removeClass("text-success");

        $("#is_concrete_density_error").val(1);

        is_concrete_density_error = 1;

    }else{
        // $("#concrete_density_error").html("");
        // $("#concrete_density_success").html("You have created perfect concrete density");

        $("#concrete_density").removeClass("error");
        $("#concrete_density").addClass("text-success");
        
        $("#concrete_density_footer").removeClass("error");
        $("#concrete_density_footer").addClass("text-success");

        $("#is_concrete_density_error").val(0);

        is_concrete_density_error = 0;
    }

    if(mix_fly_ash_brand.length > 0){

        $("#custom_flyash_qty").removeAttr("readOnly");

        if(custom_flyash_qty.length == 0){
            is_fly_ash_error = 1;
        }else{
            is_fly_ash_error = 0;
        }

    }else{
        $("#custom_flyash_qty").attr("readOnly","true");
    }
    
    if(custom_flyash_qty.length > 0){

        if(mix_fly_ash_brand.length == 0){
            is_fly_ash_error = 1;
        }else{
            is_fly_ash_error = 0;
        }

    }

    var progress_percentage = 0;
    // console.log("concrete_grade_id.length...."+concrete_grade_id.length);

    if(concrete_grade_id.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(mix_cement_brnad.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(cement_grade_id.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(mix_sand_source.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    // if(mix_aggregate_source.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    if(mix_sub_cat_1.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(mix_sub_cat_2.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    // if(ad_mixture_brand_id.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(ad_mixture_category_id.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(mix_fly_ash_brand.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    if(custom_mix_with_TM.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(custom_mix_with_CP.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }

    if(custom_cement_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(custom_sand_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(custom_aggregate1_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(custom_aggregate2_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    // if(custom_admixture_qty.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(custom_flyash_qty.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    if((custom_water_qty.length > 0) && (is_water_ratio_error == 0)){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }

    if(is_supplier == 'yes'){

         if(mix_aggregate_source.length > 0){
            progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        }

        if(address_id.length > 0){
            progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        }
        
        if(is_supplier_add_edit == 'add'){
            if(design_mix_number.length > 0){
                
                    progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
            
            }
        }else{
            progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        }
        
        if(selling_price.length > 0){
            progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        }
        
        if(product_description.length > 0){
            progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        }

    }
console.log("is_concrete_density_error..."+is_concrete_density_error);
console.log("is_admixture_percent_error..."+is_admixture_percent_error);
console.log("is_water_ratio_error..."+is_water_ratio_error);
console.log("is_fly_ash_error..."+is_fly_ash_error);
    if((is_admixture_percent_error == 1) || (is_concrete_density_error == 1) || (is_water_ratio_error == 1) || (is_fly_ash_error == 1) ){
        progress_percentage = parseFloat(progress_percentage) - percentage_plus_minus; 
    }    
    console.log("progre ss_percentage...."+progress_percentage);
   if(progress_percentage > 100){
        progress_percentage = 100;
   }

    console.log("progre ss_percentage...."+progress_percentage);

    $(custom_mix_progress).css('width',progress_percentage+'%');

    if(progress_percentage >= 100){
        $("#custom_design_mix_button").removeAttr("disabled");
    }else{
        $("#custom_design_mix_button").attr("disabled","true");
    }

  }

  function calculateAdMix(){   

        var admix_percent = $("#custom_admixture_qty_per").val() != '' ? $("#custom_admixture_qty_per").val() : 0;

        var custom_cement_qty = $("#custom_cement_qty").val() != '' ? $("#custom_cement_qty").val() : 0;
        $("#admix_quantity_error").html("");
        if(admix_percent > 0){

            var admix_calc = (custom_cement_qty * admix_percent) / 100;
            $("#custom_admixture_qty").val(admix_calc.toFixed(2));
            
            if((admix_calc.toFixed(2) >= 1) && (admix_calc.toFixed(2) <= 5) ){
                

                $("#is_admixture_percent_error").val(0);

                is_admixture_percent_error = 0;
                
            }else{
                $("#admix_quantity_error").html("Admixture quantity should be between 1 - 4 Kgs");

                $("#is_admixture_percent_error").val(1);

                is_admixture_percent_error = 1;
            }
        }else{
            $("#custom_admixture_qty").val('');

            $("#admix_quantity_error").html("Admixture quantity should be between 1 - 4 Kgs");

            is_admixture_percent_error = 1;
        }

        concreteDensity();  

  }


  function concreteDensitySupplier(){

    console.log("concreteDensity");
    $("#concrete_density_error").html("");
    $("#concrete_density_success").html("");
    $("#is_concrete_density_error").val(0);

    var is_supplier = $("#is_supplier").val() != null ? $("#is_supplier").val() : '';

    // var percentage_plus_minus = 6.0;
    var percentage_plus_minus = 7.70;

    if(is_supplier == 'yes'){

        // percentage_plus_minus = 6.25;
        percentage_plus_minus = 10;

    }
    

    var concrete_grade_id = $("#concrete_grade_id").val() != null ? $("#concrete_grade_id").val() : '';
    var mix_cement_brnad = $("#mix_cement_brnad").val() != null ? $("#mix_cement_brnad").val() : '';
    var cement_grade_id = $("#cement_grade_id").val() != null ? $("#cement_grade_id").val() : '';
    var mix_sand_source = $("#mix_sand_source").val() != null ? $("#mix_sand_source").val() : '';
    var mix_aggregate_source = $("#mix_aggregate_source").val() != null ? $("#mix_aggregate_source").val() : '';
    var mix_sub_cat_1 = $("#mix_sub_cat_1").val() != null ? $("#mix_sub_cat_1").val() : '';
    var mix_sub_cat_2 = $("#mix_sub_cat_2").val() != null ? $("#mix_sub_cat_2").val() : '';
    var ad_mixture_brand_id = $("#ad_mixture_brand_id").val() != null ? $("#ad_mixture_brand_id").val() : '';
    var ad_mixture_category_id = $("#ad_mixture_category_id").val() != null ? $("#ad_mixture_category_id").val() : '';
    var mix_fly_ash_brand = $("#mix_fly_ash_brand").val() != null ? $("#mix_fly_ash_brand").val() : '';
    var custom_mix_with_TM = $("#custom_mix_with_TM").val() != null ? $("#custom_mix_with_TM").val() : '';
    var custom_mix_with_CP = $("#custom_mix_with_CP").val() != null ? $("#custom_mix_with_CP").val() : '';
    var address_id = $("#address_id").val() != null ? $("#address_id").val() : '';
    var design_mix_number = $("#design_mix_number").val() != null ? $("#design_mix_number").val() : '';
    var is_supplier_add_edit = $("#is_supplier_add_edit").val() != null ? $("#is_supplier_add_edit").val() : '';
    var selling_price = $("#selling_price").val() != null ? $("#selling_price").val() : '';
    var product_description = $("#product_description").val() != null ? $("#product_description").val() : '';
    
    console.log("concrete_grade_id:"+concrete_grade_id);
    console.log("address_id:"+address_id);
    console.log("is_supplier:"+is_supplier);
    console.log("is_supplier_add_edit:"+is_supplier_add_edit);
    console.log("mix_cement_brnad:"+mix_cement_brnad);
    console.log("cement_grade_id:"+cement_grade_id);
    console.log("mix_sand_source:"+mix_sand_source);
    console.log("mix_aggregate_source:"+mix_aggregate_source);
    console.log("mix_sub_cat_1:"+mix_sub_cat_1);
    console.log("mix_sub_cat_2:"+mix_sub_cat_2);
    console.log("ad_mixture_brand_id:"+ad_mixture_brand_id);
    console.log("ad_mixture_category_id:"+ad_mixture_category_id);
    console.log("mix_fly_ash_brand:"+mix_fly_ash_brand);
    console.log("custom_mix_with_TM:"+custom_mix_with_TM);
    console.log("custom_mix_with_CP:"+custom_mix_with_CP);
    console.log("selling_price:"+selling_price);
    console.log("product_description:"+product_description);

    var custom_cement_qty = $("#custom_cement_qty").val() != '' ? $("#custom_cement_qty").val() : 0;
    var custom_sand_qty = $("#custom_sand_qty").val() != '' ? $("#custom_sand_qty").val() : 0;
    var custom_aggregate1_qty = $("#custom_aggregate1_qty").val() != '' ? $("#custom_aggregate1_qty").val() : 0;
    var custom_aggregate2_qty = $("#custom_aggregate2_qty").val() != '' ? $("#custom_aggregate2_qty").val() : 0;
    var custom_admixture_qty = $("#custom_admixture_qty").val() != '' ? $("#custom_admixture_qty").val() : 0;
    var custom_flyash_qty = $("#custom_flyash_qty").val() != undefined ? $("#custom_flyash_qty").val() : 0;
    var custom_water_qty = $("#custom_water_qty").val() != '' ? $("#custom_water_qty").val() : 0;

    console.log("custom_cement_qty:"+custom_cement_qty);

    if((custom_cement_qty > 0) && (custom_water_qty > 0)){
        $("#water_quantity_error").html("");
        $("#water_quantity_success").html("");
        var W_C_ratio = (custom_water_qty / custom_cement_qty);

        

        if((W_C_ratio.toFixed(2) < 0.35) || (W_C_ratio.toFixed(2) > 0.55)){

            var min = (custom_cement_qty * 0.35).toFixed(2);
            var max = (custom_cement_qty * 0.55).toFixed(2);

            $("#water_quantity_error").html("Water qty should be between "+min+" - "+ max);
            // $("#water_quantity_success").html("Water qty should be between "+min+" - "+ max);

            // $("#water_quantity_error").html("Water / Cement Ration must be between 0.35% - 0.55%");

            $("#is_WC_ratio_error").val(1);

            is_water_ratio_error = 1;

        }else{
            $("#water_quantity_success").html("Water/Cement Ratio is "+W_C_ratio.toFixed(2)+"");

            $("#is_WC_ratio_error").val(0);

            is_water_ratio_error = 0;
        }

    }else{
        if(custom_cement_qty > 0){
            var min = (custom_cement_qty * 0.35).toFixed(2);
            var max = (custom_cement_qty * 0.55).toFixed(2);

            $("#water_quantity_error").html("");
            $("#water_quantity_success").html("Water qty should be between "+min+" - "+ max);


            
        }else{
            $("#water_quantity_error").html("");
            $("#water_quantity_success").html("");
        }
        // $("#water_quantity_error").html("Cement and Water QTY must be greater than 0");
        is_water_ratio_error = 1;
    }

    var total_density = parseInt(custom_cement_qty) +
                        parseInt(custom_sand_qty) +
                        parseInt(custom_aggregate1_qty) +
                        parseInt(custom_aggregate2_qty) +
                        parseInt(custom_admixture_qty) +
                        parseInt(custom_water_qty);

    if(total_density > 0){
        $("#concrete_density").show();    
    }else{
        $("#concrete_density").hide();  
    }
    $("#concrete_density").html(": <b>"+total_density+" KG</b>");

    if((total_density < 2400) || (total_density > 2550) ){

        // $("#concrete_density_error").html("Concrete Density should be between 2400 to 2550");
        // $("#concrete_density_success").html("");

        $("#concrete_density").addClass("error");
        $("#concrete_density").removeClass("text-success");

        $("#is_concrete_density_error").val(1);

        is_concrete_density_error = 1;

    }else{
        // $("#concrete_density_error").html("");
        // $("#concrete_density_success").html("You have created perfect concrete density");

        $("#concrete_density").removeClass("error");
        $("#concrete_density").addClass("text-success");

        $("#is_concrete_density_error").val(0);

        is_concrete_density_error = 0;
    }

    if(mix_fly_ash_brand.length > 0){

        $("#custom_flyash_qty").removeAttr("readOnly");

        // if(custom_flyash_qty.length == 0){
        //     is_fly_ash_error = 1;
        // }else{
            is_fly_ash_error = 0;
        // }

    }else{
        // $("#custom_flyash_qty").attr("readOnly","true");
    }
    
    if(custom_flyash_qty.length > 0){

        // if(mix_fly_ash_brand.length == 0){
        //     is_fly_ash_error = 1;
        // }else{
            is_fly_ash_error = 0;
        // }

    }

    var progress_percentage = 0;
    console.log("concrete_grade_id.length...."+concrete_grade_id.length);

    if(concrete_grade_id.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    // if(mix_cement_brnad.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(cement_grade_id.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(mix_sand_source.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(mix_aggregate_source.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(mix_sub_cat_1.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(mix_sub_cat_2.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(ad_mixture_brand_id.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(ad_mixture_category_id.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(mix_fly_ash_brand.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(custom_mix_with_TM.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    // if(custom_mix_with_CP.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }

    if(custom_cement_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(custom_sand_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(custom_aggregate1_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(custom_aggregate2_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    if(custom_admixture_qty.length > 0){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }
    // if(custom_flyash_qty.length > 0){
    //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    // }
    if((custom_water_qty.length > 0) && (is_water_ratio_error == 0)){
        progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
    }

    if(is_supplier == 'yes'){

        //  if(mix_aggregate_source.length > 0){
        //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        // }

        if(address_id.length > 0){
            progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        }
        
        if(is_supplier_add_edit == 'add'){
            if(design_mix_number.length > 0){
                
                    progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
            
            }
        }else{
            progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        }
        
        // if(selling_price.length > 0){
        //     progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        // }
        
        if(product_description.length > 0){
            progress_percentage = parseFloat(progress_percentage) + percentage_plus_minus; 
        }

    }
console.log("is_concrete_density_error..."+is_concrete_density_error);
console.log("is_admixture_percent_error..."+is_admixture_percent_error);
console.log("is_water_ratio_error..."+is_water_ratio_error);
console.log("is_fly_ash_error..."+is_fly_ash_error);
    if((is_admixture_percent_error == 1) || (is_concrete_density_error == 1) || (is_water_ratio_error == 1) || (is_fly_ash_error == 1) ){
        progress_percentage = parseFloat(progress_percentage) - percentage_plus_minus; 
    }    
    console.log("progre ss_percentage...."+progress_percentage);
   if(progress_percentage > 100){
        progress_percentage = 100;
   }

    console.log("progre ss_percentage...."+progress_percentage);

    $(custom_mix_progress).css('width',progress_percentage+'%');

    if(progress_percentage >= 100){
        $("#custom_design_mix_button").removeAttr("disabled");
    }else{
        $("#custom_design_mix_button").attr("disabled","true");
    }

  }


  function calculateAdMixSupplier(){   

    var admix_percent = $("#custom_admixture_qty_per").val() != '' ? $("#custom_admixture_qty_per").val() : 0;

    var custom_cement_qty = $("#custom_cement_qty").val() != '' ? $("#custom_cement_qty").val() : 0;
    $("#admix_quantity_error").html("");
    if(admix_percent > 0){

        var admix_calc = (custom_cement_qty * admix_percent) / 100;
        $("#custom_admixture_qty").val(admix_calc.toFixed(2));
        
        if((admix_calc.toFixed(2) >= 1) && (admix_calc.toFixed(2) <= 5) ){
            

            $("#is_admixture_percent_error").val(0);

            is_admixture_percent_error = 0;
            
        }else{
            $("#admix_quantity_error").html("Admixture quantity should be between 1 - 4 Kgs");

            $("#is_admixture_percent_error").val(1);

            is_admixture_percent_error = 1;
        }
    }else{
        $("#custom_admixture_qty").val('');

        $("#admix_quantity_error").html("Admixture quantity should be between 1 - 4 Kgs");

        is_admixture_percent_error = 1;
    }

    concreteDensitySupplier();  

}

  function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
        return true;
    }
 }

 var interval = null;
 function startTimer(duration, display, is_from) {
    clearInterval(interval);
    var timer = duration, minutes, seconds;
    interval = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        // display.textContent = minutes + ":" + seconds;
        display.html(minutes + ":" + seconds);

        if (--timer < 0) {
            if(is_from == "supplier_register"){
                clearInterval(interval);
                display.html('<button type="submit" name="register_btn" value="register_btn" class="text-primary">Resend OTP</button>');
            }else if(is_from == "supplier_forgot_pass"){
                clearInterval(interval);
                display.html('<button type="submit" name="otp_send_btn" value="otp_send_btn" class="text-primary">Resend OTP</button>');
            }else if(is_from == "supplier_otp_verify"){
                clearInterval(interval);
                display.html('<button onclick="resendOTP()" name="otp_send_btn" value="otp_send_btn" class="text-primary">Resend OTP</button>');
            }else if(is_from == "admin_otp_verify"){
                clearInterval(interval);
                display.html('<button onclick="resendOTP()" name="otp_send_btn" value="otp_send_btn" class="text-primary">Resend OTP</button>');
            }else if(is_from == "buyer_otp_forgot_pass"){
                clearInterval(interval);
                display.html('<a href="javascript:void(0)" onclick="otpResendForgotPass()">Resend OTP</a>');
            }else if(is_from == "buyer_otp_verify"){
                clearInterval(interval);
                display.html('<a href="javascript:void(0)" onclick="otpResend()">Resend OTP</a>');
            }else if(is_from == "admin_forgot_pass"){
                clearInterval(interval);
                display.html('<button type="submit" name="otp_send_btn" value="otp_send_btn" class="text-primary">Resend</button>');
            }else if(is_from == "supplier_email_verify"){
                clearInterval(interval);
                display.html('<a href="javascript:void(0)" onclick="emailVerify()">Resend OTP</a>');
            }else if(is_from == "old_mobile"){
                clearInterval(interval);
                display.html('<a href="javascript:void(0)" onclick="OTPonUpdateMobile(1, 0, 1)">Resend OTP</a>');
            }else if(is_from == "new_mobile"){
                clearInterval(interval);
                display.html('<a href="javascript:void(0)" onclick="OTPonUpdateMobile(0, 1, 1)">Resend OTP</a>');
            }else if(is_from == "old_email"){
                clearInterval(interval);
                display.html('<a href="javascript:void(0)" onclick="OTPonUpdateEmail(1, 0, 1)">Resend OTP</a>');
            }else if(is_from == "new_email"){
                clearInterval(interval);
                display.html('<a href="javascript:void(0)" onclick="OTPonUpdateEmail(0, 1, 1)">Resend OTP</a>');
            }else{
                timer = duration;
            }
            
        }
    }, 1000);
    
}

// Refresh Rate is how often you want to refresh the page
// bassed off the user inactivity.
// var refresh_rate = 10; //<-- In seconds, change to your needs
// var last_user_action = 0;
// var has_focus = false;
// var lost_focus_count = 0;
// // If the user loses focus on the browser to many times
// // we want to refresh anyway even if they are typing.
// // This is so we don't get the browser locked into
// // a state where the refresh never happens.
// var focus_margin = 10;
// // Reset the Timer on users last action
// function reset() {
//   last_user_action = 0;
//   updateVisualTimer('Reset Timer');
// }
// function updateVisualTimer(value) {
//   var element = document.getElementById('refreshTimer');
//   if (value) {
//     element.value = value
//   } else if (has_focus) {
//     element.value = 'User has focuse won\'t refresh'
//   } else if (last_user_action >= refresh_rate) {
//     element.value = 'Refreshing';
//   } else {
//     element.value = (refresh_rate - last_user_action);
//   }
// }
// function windowHasFocus() {
//   has_focus = true;
// }
// function windowLostFocus() {
//   has_focus = false;
//   lost_focus_count++;
//   console.log(lost_focus_count + " <~ Lost Focus");
// }
// // Count Down that executes ever second
// // setInterval(function() {
// //   last_user_action++;
// //   refreshCheck();
// //   updateVisualTimer();
// // }, 1000);
// // The code that checks if the window needs to reload
// function refreshCheck() {
//   var focus = window.onfocus;
//   if ((last_user_action >= refresh_rate && !has_focus && document.readyState == "complete") || lost_focus_count > focus_margin) {
//     window.location.reload(); // If this is called no reset is needed
//     reset(); // We want to reset just to make sure the location reload is not called.
//   }
// }
// window.addEventListener("focus", windowHasFocus, false);
// window.addEventListener("blur", windowLostFocus, false);
// window.addEventListener("click", reset, false);
// window.addEventListener("mousemove", reset, false);
// window.addEventListener("keypress", reset, false);
// window.addEventListener("scroll", reset, false);
// document.addEventListener("touchMove", reset, false);
// document.addEventListener("touchEnd", reset, false);