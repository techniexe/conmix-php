(function ($) {
	
    'use strict';
/*--------------------------------------------------------------------------------------------
	document.ready ALL FUNCTION START
---------------------------------------------------------------------------------------------*/	

//  > Top Search bar Show Hide function by = custom.js =================== //	
	 function site_search(){
			jQuery('a[href="#search"]').on('click', function(event) {                    
			jQuery('#search').addClass('open');
			jQuery('#search > form > input[type="search"]').focus();
		});
					
		jQuery('#search, #search button.close').on('click keyup', function(event) {
			if (event.target === this || event.target.className === 'close') {
				jQuery(this).removeClass('open');
			}
		});  
	 }	
// > Video responsive function by = custom.js ========================= //	

	function video_responsive(){	
		jQuery('iframe[src*="youtube.com"]').wrap('<div class="embed-responsive embed-responsive-16by9"></div>');
		jQuery('iframe[src*="vimeo.com"]').wrap('<div class="embed-responsive embed-responsive-16by9"></div>');	
	}  

	

// Vertically center Bootstrap modal popup function by = custom.js ==============//

	function popup_vertical_center(){	
		jQuery(function() {
			function reposition() {
				var modal = jQuery(this),
				dialog = modal.find('.modal-dialog');
				modal.css('display', 'block');
				// Dividing by two centers the modal exactly, but dividing by three 
				// or four works better for larger screens.
				dialog.css("margin-top", Math.max(0, (jQuery(window).height() - dialog.height()) / 2));
			}
			// Reposition when a modal is shown
			jQuery('.modal').on('show.bs.modal', reposition);
			// Reposition when the window is resized
			jQuery(window).on('resize', function() {
				jQuery('.modal:visible').each(reposition);
			});
		});
	}

// > Main menu sticky on top  when scroll down function by = custom.js ========== //		

	function sticky_header(){
		if(jQuery('.sticky-header').length){
			var sticky = new Waypoint.Sticky({
			  element: jQuery('.sticky-header')
			})
		}
	}

// > page scroll top on button click function by = custom.js ===================== //	

	function scroll_top(){
		jQuery("button.scroltop").on('click', function() {
			jQuery("html, body").animate({
				scrollTop: 0
			}, 1000);
			return false;
		});

		jQuery(window).on("scroll", function() {
			var scroll = jQuery(window).scrollTop();
			if (scroll > 900) {
				jQuery("button.scroltop").fadeIn(1000);
			} else {
				jQuery("button.scroltop").fadeOut(1000);
			}
		});
	}
// > box height match window height according function by = custom.js ========= //	

	function set_height() {
		if(jQuery('.demo-wraper').length){
			windowHeight = jQuery(window).innerHeight();
			jQuery('.demo-wraper').css('min-height', windowHeight);
		}
	}

// > footer fixed on bottom function by = custom.js ======================== //	

	function footer_fixed() {
	  jQuery('.site-footer').css('display', 'block');
	  jQuery('.site-footer').css('height', 'auto');
	  var footerHeight = jQuery('.site-footer').outerHeight();
	  jQuery('.footer-fixed > .page-wraper').css('padding-bottom', footerHeight);
	  jQuery('.site-footer').css('height', footerHeight);
	}

// > accordion active calss function by = custom.js ========================= //	

	function accordion_active() {
		$('.acod-head a').on('click', function() {
			$('.acod-head').removeClass('acc-actives');
			$(this).parents('.acod-head').addClass('acc-actives');
			$('.acod-title').removeClass('acc-actives'); //just to make a visual sense
			$(this).parent().addClass('acc-actives'); //just to make a visual sense
			($(this).parents('.acod-head').attr('class'));
		 });
	}	

// > Nav submenu show hide on mobile by = custom.js

	 function mobile_nav(){
		jQuery(".sub-menu").parent('li').addClass('has-child');
		jQuery(".mega-menu").parent('li').addClass('has-child');
		jQuery("<div class=' glyphicon glyphicon-plus submenu-toogle'></div>").insertAfter(".has-child > a");
		jQuery('.has-child a+.submenu-toogle').click(function(ev) {
			jQuery(this).next(jQuery('.sub-menu')).slideToggle('fast', function(){
				jQuery(this).parent().toggleClass('nav-active');
			});
			ev.stopPropagation();
		});
	 }

/* banner slider */
$(".features-slider").owlCarousel({
	items: 1,
	loop: true,
	smartSpeed: 2000,
	dots: true,
	nav: false,
	navText: ["<i class='icon icon-arrow-left'></i>", "<i class='icon icon-arrow-right'></i>"],
	// navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
	autoplay: true,
	autoplayTimeout:8000,
	mouseDrag: true,
	responsive: {
	   0: {
		  nav: false,
		  mouseDrag: false,
		  touchDrag:false,
	   },
	   600: {
		  nav: false,
		  mouseDrag: false,
		  touchDrag:false,
 
	   },
	   1000: {
		  nav: true,
		  mouseDrag: true,
		  touchDrag:true,
 
	   }
	}
 });

// Fade slider for home function by = owl.carousel.js ========================== //
	function homemain_carousel(){
	jQuery('.home-main-carousel').owlCarousel({
		loop:true,
		autoplay:true,
		autoplayTimeout:8000,
		margin:0,
		nav:false,
		navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
		items:1,
		dots: true,
		//animateOut:'fadeOut',
	});
	}
	
// > home_logo_carousel() function by = owl.carousel.js ========================== //
	function home4_logo_carousel(){
	jQuery('.home4-logo-carousel').owlCarousel({
        loop:true,
		autoplay:true,
		margin:0,
		nav:false,
		dots: false,
		navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
		responsive:{
			0:{
				items:2
			},
			480:{
				items:3
			},			
			
			767:{
				items:4
			},
			1000:{
				items:5
			}
		}
		
	});
	}	
/*--------------------------------------------------------------------------------------------
	Window on load ALL FUNCTION START
---------------------------------------------------------------------------------------------*/

// > equal each box function by  = custom.js =========================== //	 

	function equalheight(container) {
		var currentTallest = 0, 
			currentRowStart = 0, 
			rowDivs = new Array(), 
			$el, topPosition = 0,
			currentDiv = 0;

		jQuery(container).each(function() {
			$el = jQuery(this);
			jQuery($el).height('auto');
			var topPostion = $el.position().top;
			if (currentRowStart != topPostion) {
				for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0; // empty the array
				currentRowStart = topPostion;
				currentTallest = $el.height();
				rowDivs.push($el);
			} else {

				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}

			for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		});
	}

// > skills bar function function by  = custom.js ========================= //

	/* 2.1 skills bar tooltips*/
	function progress_bar_tooltips() {
		jQuery(function () { 
		  jQuery('[data-toggle="tooltips"]').tooltip({trigger: 'manual'}).tooltip('show');
		});  
	}

	/* 2.2 skills bar widths*/

	function progress_bar_width() {	
		jQuery( window ).on('scroll', function() {   
		  jQuery(".progress-bar").each(function(){
			progress_bar_width = jQuery(this).attr('aria-valuenow');
			jQuery(this).width(progress_bar_width + '%');
		  });
		}); 
	}
    
// > Top wishlist Show Hide function by = custom.js =================== //	

	 function wishlist_block(){
		jQuery('.wishlist-btn').on('click', function () { 
		jQuery( ".wishlist-dropdown-item-wraper" ).slideToggle( "slow" );
	  });  
	 }

// > masonry function function by = isotope.pkgd.min.js ========================= //	

	function masonryBox() {
        if ( jQuery().isotope ) {      
            var $container = jQuery('.portfolio-wrap');
                $container.isotope({
                    itemSelector: '.masonry-item',
                    transitionDuration: '1s',
					originLeft: true
                });

            jQuery('.masonry-filter li').on('click',function() {                           
                var selector = jQuery(this).find("a").attr('data-filter');
                jQuery('.masonry-filter li').removeClass('active');
                jQuery(this).addClass('active');
                $container.isotope({ filter: selector });
                return false;
            });
    	};
	}	

// > page loader function by = custom.js ========================= //		

	function page_loader() {
		$('.loading-area').fadeOut(1000)
	};

/*--------------------------------------------------------------------------------------------
    Window on scroll ALL FUNCTION START
---------------------------------------------------------------------------------------------*/

    function color_fill_header() {
        var scroll = $(window).scrollTop();
        if(scroll >= 100) {
            $(".is-fixed").addClass("color-fill");
        } else {
            $(".is-fixed").removeClass("color-fill");
        }
    };	

/*--------------------------------------------------------------------------------------------
	document.ready ALL FUNCTION START
---------------------------------------------------------------------------------------------*/
	jQuery(document).ready(function() {
	// > Top Search bar Show Hide function by = custom.js  		
		site_search(),
	// > Vertically center Bootstrap modal popup function by = custom.js
		popup_vertical_center();
	// > Main menu sticky on top  when scroll down function by = custom.js
		sticky_header(),
	// > page scroll top on button click function by = custom.js	
		scroll_top(),
	//	> box height match window height according function by = custom.js
		set_height(),
	// > footer fixed on bottom function by = custom.js	
		footer_fixed(),
	// > accordion active calss function by = custom.js ========================= //
		accordion_active(),
    // > Top wishlist Show Hide function by = custom.js =================== //		
		wishlist_block(),
	// > Nav submenu on off function by = custome.js ===================//
		mobile_nav(),
    // Fade slider for home function by = owl.carousel.js ========================== //   
	   homemain_carousel(),
	// home4_logo_carousel() function by = owl.carousel.js ========================== //
		home4_logo_carousel()
	}); 

/*--------------------------------------------------------------------------------------------
	Window Load START
---------------------------------------------------------------------------------------------*/
	jQuery(window).on('load', function () {
	// > equal each box function by  = custom.js			
		equalheight(".equal-wraper .equal-col"),
	// > skills bar function function by  = custom.js			
		progress_bar_tooltips(),
	// > skills bar function function by  = custom.js		
		progress_bar_width(),
	// > On scroll content animated function by = Viewportchecker.js		
	// > box height match window height according function by = custom.js		
		set_height(),
	// > masonry function function by = isotope.pkgd.min.js		
		masonryBox(),
	// > page loader function by = custom.js		
		page_loader() 
});

 /*===========================
	Window Scroll ALL FUNCTION START
===========================*/

	jQuery(window).on('scroll', function () {
	// > Window on scroll header color fill 
		color_fill_header()
	});
	
/*===========================
	Window Resize ALL FUNCTION START
===========================*/

	jQuery(window).on('resize', function () {
	// > footer fixed on bottom function by = custom.js		 
	 	footer_fixed(),
	// > box height match window height according function by = custom.js
	 	set_height()
	});

	/*rating js*/
	$(
		function () {
		  $('.rating li').on('click', function() {
			var selectedCssClass = 'selected';
			var $this = $(this);
			$this.siblings('.' + selectedCssClass).removeClass(selectedCssClass);
			$this
			  .addClass(selectedCssClass)
			  .parent().addClass('vote-cast');
		  });
		}
	);
	/*rating js end*/

})(window.jQuery);

/**/

jQuery(document).ready(function() {
    'use strict';
//  Tooltip function by = bootstrap.js ========================== //
	jQuery('[data-toggle="tooltip"]').tooltip();

//  Popovers = bootstrap.js========================= //	
	jQuery('[data-toggle="popover"]').popover();
	
//	testimonial slider ========================== //
	jQuery('.testimonial-carousel').owlCarousel({
		loop: true,
		autoplay: false,
		autoplayHoverPause: true,
		nav: true,
		dots: false,
		mouseDrag: true,
		touchDrag: true,
		slideSpeed: 500,
		navText: ["<i class='icon icon-arrow-left'></i>", "<i class='icon icon-arrow-right'></i>"],
		items: 1,
		responsive: {
			0: {
				items: 1,
				mouseDrag: false
			},
			600: {
				items: 1,
				mouseDrag: false
			},
			1000: {
				items: 1,
				mouseDrag: false
			}
		}
	})	

	//  Client logo Carousel function by = owl.carousel.js ========================== //
	jQuery('.client-logo-carousel').owlCarousel({
		loop:true,
		margin:30,
		nav:false,
		dots: false,
		responsive:{
			0:{
				items:2,
				mouseDrag: false
			},
			480:{
				items:3,
				mouseDrag: false
			},			
			767:{
				items:4,
				mouseDrag: false
			},
			1000:{
				items:6,
				mouseDrag: false
			}
		}
	})	
    
    /*cart slide menu*/
    $('.cartSlide, .cartoverlay').click(function(){
      $('.cartSlide').toggleClass('clicked');
      $('.cartoverlay').toggleClass('show');
      $('.cartslideblock').toggleClass('show');
      $('body').toggleClass('cartoverflow');
		});
		
		/*filter slide menu*/
    $('.filterSlide, .filteroverlay').click(function(){
      $('.filterSlide').toggleClass('clicked');
      $('.filteroverlay').toggleClass('show');
      $('.filterslideblock').toggleClass('show');
      $('body').toggleClass('filteroverflow');
    });

});


/*input mobile no with flag*/
var input = document.querySelector("#phone");
window.intlTelInput(input, {
  utilsScript: "js/utils.js" // just for formatting/placeholders etc
});
/*input mobile no with flag end*/

/*input browse file script*/
$(document).on('change', '.button-browse :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  
    input.trigger('fileselect', [numFiles, label, input]);
});

$('.button-browse :file').on('fileselect', function(event, numFiles, label, input) {
  var val = numFiles > 1 ? numFiles + ' files selected' : label;
  
  input.parent('.button-browse').next(':text').val(val);
});
/*input browse file script end*/

/*tabs responsive select convert start*/
	// var tabsFinish = 0;
	// $('.tab-switcher').on('click', function(){
	// 	if($(this).hasClass('active') || tabsFinish) return false;
	// 	tabsFinish = 1;
	// 	var thisIndex = $(this).parent().find('.tab-switcher').index(this);
	// 	$(this).parent().find('.active').removeClass('active');
	// 	$(this).addClass('active');

	// 	$(this).closest('.tabs-container').find('.tabs-entry:visible').animate({'opacity':'0'}, 300, function(){
	// 		$(this).hide();
	// 		var showTab = $(this).parent().find('.tabs-entry').eq(thisIndex);
	// 		showTab.show().css({'opacity':'0'});
	// 		if(showTab.find('.swiper-container').length) {
	// 			swipers['swiper-'+showTab.find('.swiper-container').attr('id')].resizeFix();
	// 			if(!showTab.find('.swiper-active-switch').length) showTab.find('.swiper-pagination-switch:first').addClass('swiper-active-switch');
	// 		}
	// 		showTab.animate({'opacity':'1'}, function(){tabsFinish = 0;});
	// 	});
		
	// });

	// $('.swiper-tabs .title, .links-drop-down .title').on('click', function(){
	// 	$(this).toggleClass('active');
	// 	$(this).next().slideToggle(300);
	// });
/*tabs responsive select convert end*/

// /*qty script start*/
// $('.add').click(function () {
//     if ($(this).prev().val()) {

// 		var qty = +$(this).prev().val() + 1;

// 		$(this).prev().val(qty);
		
// 		var token = $("#qty_csrf_token").val();
// 		var product_id = $("#qty_product_id").val();

// 		addToCart(token,product_id,qty,"cart_checkout");
//     }
// });
// $('.sub').click(function () {
//     if ($(this).next().val() > 1) {		
		
//     	if ($(this).next().val() > 1) {

// 			var qty = +$(this).next().val() - 1;

// 			$(this).next().val(qty);

// 			var token = $("#qty_csrf_token").val();
// 			var product_id = $("#qty_product_id").val();

// 			addToCart(token,product_id,qty,"cart_checkout");
// 		}
//     }
// });
// /*qty script end*/

/*order cancle count script start*/
$(document).ready(function () {
    var myDate = new Date();
    myDate.setDate(myDate.getDate() + 1);
    $("#countdown").countdown(myDate, function (event) {
        $(this).html(
            event.strftime(
                '<div class="timer-wrapper"><div class="time">%D</div><span class="text">Days</span></div><div class="timer-wrapper"><div class="time">%H</div><span class="text">Hrs</span></div><div class="timer-wrapper"><div class="time">%M</div><span class="text">Mins</span></div><div class="timer-wrapper"><div class="time">%S</div><span class="text">Sec</span></div>'
            )
        );
    });
});
/*order cancle count script end*/

/*wow animation js start*/
	new WOW().init();
/*wow animation js end*/

/*date range picker start*/
document.addEventListener("touchstart", function () {}, false);
(function ($) {
	"use strict";
	function addDays(dateObj, numDays) {
		dateObj.setDate(dateObj.getDate() + numDays);
		return dateObj;
	}
	var todayDate = moment();
	var next3days = moment().add(3, 'days');
	$('input[name="reservationdate"]').daterangepicker({
		locale: {
			format: 'DD/MM/YYYY'
		},
		startDate: todayDate,
		endDate: next3days,
        showDropdowns: false,
		autoUpdateInput: false
	});
	
	$('input[name="reservationdate"]').on('apply.daterangepicker', function (ev, picker) {
		$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
	});
	$('input[name="reservationdate"]').on('cancel.daterangepicker', function (ev, picker) {
		$(this).val('');
	});
	
})(jQuery);
/*date range picker end*/

/**/
/**/


// ConMix.................................................

