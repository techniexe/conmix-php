$( function() {
    setTimeout(removeLoader); //wait for page load PLUS two seconds.
});
function removeLoader(){
    $( "#loadingDiv" ).fadeOut(500, function() {
        // fadeOut complete. Remove the loading div
        $( "#loadingDiv" ).remove(); //makes page more lightweight
    });
}

$( function() {
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
    });

    $('.home_our_product .owl-carousel').owlCarousel({
        loop: true,
        margin:20,
        nav: true,
        dots: false,
        items:4,
        autoplay:true,
        autoplayTimeout:3000,
        navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
    $('.home_our_client .owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav: true,
        dots:false,
        items:4,
        navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
});

$( function() {
    $(".enquirytab").on("click", function() {
        $(".enquiryblock").toggleClass("open", 1000);
    });

    $('.form-control-chosen').chosen({
        width: '100% !important'
    });
});

$( function() {
    $('#us2').locationpicker({
        enableAutocomplete: true,
        enableReverseGeocode: true,
      radius: 0,
      inputBinding: {
        latitudeInput: $('#us2_lat'),
        longitudeInput: $('#us2_lon'),
        radiusInput: $('#us2-radius'),
        locationNameInput: $('#us2_address')
      },
      panControl: true,
            panControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
      onchanged: function (currentLocation, radius, isMarkerDropped) {
            var addressComponents = $(this).locationpicker('map').location.addressComponents;
            console.log("test...");
            console.log(addressComponents);
            console.log("test...");
            console.log(currentLocation);  //latlon
        // updateControls(addressComponents); //Data
        }
    });
    });

$( function() {

    var url_param = getParamsForFilter(window.location.href);
    
    // console.log("url_param..."+url_param);
    
    if(url_param == '=undefined'){
        console.log("url_param..."+url_param);
        $(".clear_filter_button").hide();
    }else{
        $(".clear_filter_button").show();
    }
    
    });