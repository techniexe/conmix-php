// Sign up Module Start..................................................


function confirmPopup(type,data,csrf,message){


    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);
        data = JSON.parse(data);
        console.log(data);
        if((type == 1)){
            $gateway_transaction_id = '';
            if(data["order"]["gateway_transaction_id"] != undefined){
                $gateway_transaction_id = data["order"]["gateway_transaction_id"];
            }
            cancel_order(''+data["order_id"],''+data["_id"], $gateway_transaction_id); //Cancel Order
        }
        
        if((type == 2)){ //Accept 7days Report
            
            accept7DaysReport(''+data["vendor_order_id"],''+data["_id"],''+data["TM_id"]); //Cancel Order
        }
        
        if((type == 3)){ //Reject 7days Report
            
            $("#report_7_track_id").val(data["_id"]);
            $("#report_7_tm_id").val(data["TM_id"]);
            $("#report_7_order_id").val(data["vendor_order_id"]);

            $('#add-7-report-details').modal('show');
        }

        if((type == 4)){ //Accept 28days Report

            accept28DaysReport(''+data["vendor_order_id"],''+data["_id"],''+data["TM_id"]); //Cancel Order
        }
        
        if((type == 5)){ //Reject 28days Report
            $("#report_28_track_id").val(data["_id"]);
            $("#report_28_tm_id").val(data["TM_id"]);
            $("#report_28_order_id").val(data["vendor_order_id"]);

            $('#add-28-report-details').modal('show');
        }

    });
}

function requestOTP(type,request_data,csrf){

    $('p.error').remove();
    $("#otp_code_1").val("");
    $("#otp_code_2").val("");
    $("#otp_code_3").val("");
    $("#otp_code_4").val("");
    $("#otp_code_5").val("");
    $("#otp_code_6").val("");

    $.ajax({
        type: "GET",
        url: BASE_URL+"/buyer/request_otp",
        data: request_data,
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            $("label.error").remove();
            $(".form-control").removeClass("error");
            if(data["status"] == 200){  //Success
                var OTP_mob_number = $("#OTP_mob_number").val();
                if(OTP_mob_number!=undefined){
                    // OTP_mob_number = OTP_mob_number.replace(OTP_mob_number.substring(5, 11),"XXXXXX");
                    $('#OTP_mob_no').html(OTP_mob_number);
                }
                showToast("OTP Sent successfully to your registered mobile number" ,"Success");

                 // Active In Active User
                    $("#otp").val("");
                    $("#otp_error").html("");

                    var OTP_dialog = function(){

                        var otp = $("#otp_code_1").val()+$("#otp_code_2").val()+$("#otp_code_3").val()+$("#otp_code_4").val()+$("#otp_code_5").val()+$("#otp_code_6").val();
                        if(otp !== ''){
                            // console.log("numeric..."+$.isNumeric(otp));
                            if($.isNumeric(otp)){
                                if((otp.length === 6) ){
                                    // $('#otpModal').modal('hide');

                                    request_data["authentication_code"] = otp;

                                    if(type ==1){ //Update Ticket status

                                        changePassword(request_data,otp);
                                    }
                                }else{
                                    $("#otp_error").html("Please enter 6 digits OTP");
                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                    otpDialog(OTP_dialog);
                                }
                            }else{
                                $("#otp_error").html("OTP should be in number");
                                // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                otpDialog(OTP_dialog);
                            }
                        }else{
                            $("#otp_error").html("Please enter OTP");
                            // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                            // $("#otpCancel").unbind().one("click", fClose);
                            otpDialog(OTP_dialog);
                        }

                    };

                    otpDialog(OTP_dialog);

            }else{ //Error
                // showToast("Somethig went wrong, Please try again","Error");
                showErrorMsgInForm("change_pass_form",data["error"]["message"]);
            }

        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });

}

function requestOTPforEditProfile(type,request_data,is_old_mobile,is_old_email,is_new_mobile,is_new_email,is_edit,is_resend){

    var param = "";
    var value = "";
    var type = "";
    var final_data = {};

    if(is_old_mobile == 1){
        param = "old_mobile_no";
        value = request_data;
        type = "mobile";
        final_data = {
            "old_mobile_no" : value
        }

        $("#mobile_type").val("old_mobile");
    }
    
    if(is_old_email == 1){
        param = "old_email";
        value = request_data;
        type = "email";

        final_data = {
            "old_email" : value
        }

        $("#email_type").val("old_email");
    }
    
    if(is_new_mobile == 1){
        param = "new_mobile_no";
        value = request_data;
        type = "mobile";

        final_data = {
            "new_mobile_no" : value
        }

        $("#mobile_type").val("new_mobile");
    }
    
    if(is_new_email == 1){
        param = "new_email";
        value = request_data;
        type = "email";

        final_data = {
            "new_email" : value
        }

        $("#email_type").val("new_email");
    }

    $.ajax({
        type: "GET", 
        url: BASE_URL+"/buyer/request_otp_for_edit_profile", 
        data: final_data,
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            $("label.error").remove();
            $(".form-control").removeClass("error");
            if(data["status"] == 200){  //Success
                
                // var OTP_mob_number = data["data"]["mobile_number"];
                $("#update_mobile_otp_confirm_message").html("Enter the 6 digits OTP sent on given "+type+" "+value);
                $("#update_email_otp_confirm_message").html("Enter the 6 digits OTP sent on given "+type+" "+value);
                // console.log("OTP_mob_number:"+OTP_mob_number);
                
                showToast("OTP Sent successfully" ,"Success");
                var fiveMinutes = 60 * 2;
                
                display = $('#time_update_mobile');

                if(param == "old_email" || param == "new_email"){
                    display = $('#time_update_email');
                }

                if(param == "old_mobile_no" || param == "new_mobile_no"){
                    startTimer(fiveMinutes, display, ""+$("#mobile_type").val());
                }

                if(param == "old_email" || param == "new_email"){
                    startTimer(fiveMinutes, display, ""+$("#email_type").val());
                }
                 // Active In Active User                    

                 if(is_resend == 0){
                    $("#new_mobile_no_popup").modal("hide");
                    $("#new_email_no_popup").modal("hide");
                    $("input[name='otp_code_1']").val('');
                    $("input[name='otp_code_2']").val('');
                    $("input[name='otp_code_3']").val('');
                    $("input[name='otp_code_4']").val('');
                    $("input[name='otp_code_5']").val('');
                    $("input[name='otp_code_6']").val('');
                    if(param == "old_mobile_no" || param == "new_mobile_no"){
                        $("#update_new_mobile_no").val('');
                        $("#update_mobile_otp").modal("show");

                    }
                    
                    if(param == "old_email" || param == "new_email"){
                        console.log("test");
                        $("#update_new_email").val('');
                        $("#update_email_otp").modal("show");
                    }
                }
                
                    
                
            }else{ //Error
                if(data["error"]["message"] != undefined){
                    showToast(""+data["error"]["message"],"Error");
                }else{
                    showToast("Something went wrong","Error");
                }
            }            
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });

}


$(document).ready(function(){

    $("#clear_location").on("click",function(){

        $("#searchTextField").val('');

        $("#location_error").html("");
        $("#product_lat").val("");
        $("#product_long").val("");
        $("#location_state_name").val("");
        $("#location_city_name").val("");
        $("#location_state_id").val("");
        $("#location_city_id").val("");

        $("#clear_location").hide();

    });

    $("#clear_address_location").on("click",function(){

        $("#us2_address").val('');


    });

    $("#deliver_qty").keyup(function(){
        let deliver_qty = $("#deliver_qty").val();
        console.log("deliver_qty....."+deliver_qty);
        $("#delivery_date_msg").html("");
        if(deliver_qty.length > 0){
            $("#deliver_qty_error").html("");
            if(!isNaN(deliver_qty)){
                $("#deliver_qty_error").html("");
                if((deliver_qty >= 3) && (deliver_qty <= 100)){
                    if(deliver_qty % 1 == 0){
                        $("#deliver_qty_error").html("");
                        $("#choose_location_popup_done_btn").removeAttr('disabled');
                        $("#location_delivery_qty").val(""+deliver_qty);

                        // var today = new Date();
                        // var today_to_30 = new Date();

                        // today.setDate(today.getDate() + 2);
                        // var dd = String(today.getDate()).padStart(2, '0');
                        // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        // var yyyy = today.getFullYear();

                        // today = dd + '/' + mm + '/' + yyyy;


                        var total_days = (parseInt(deliver_qty) / 3);
                        total_days = Math.floor(total_days);
                        $("#delivery_date_error").html("");
                        $("#delivery_date_msg").html("<i class='fa fa-info-circle'></i>You have to choose delivery date range of within "+total_days+" days");
                        console.log("total_days...."+total_days);
                        
                        var today = new Date($("#selected_delivery_date").val());

                        today.setDate(today.getDate() + total_days);
                        var dd = String(today.getDate()).padStart(2, '0');
                        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = today.getFullYear();

                        today = dd + '/' + mm + '/' + yyyy;
                        today_request = yyyy + '/' + mm + '/'+dd;
                        console.log("today...."+today);
                        console.log("today_request...."+today_request);
                        // $("#selected_delivery_date").val(today_request);
                        $("#selected_end_delivery_date").val(today_request);
                        $('input[name="daterange"]').data('daterangepicker').setEndDate(today);
                        // var date = new Date();
                        // date.setDate(date.getDate() + (total_days + 2));
                        // // var end_dd = (date.getDate() - 3);
                        // var dd = String(date.getDate()).padStart(2, '0');
                        // var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
                        // var yy = date.getFullYear();

                        // var final_date = ""+dd+'/'+mm+'/'+yy;

                        // console.log("date..."+final_date);

                        // $('input[name="daterange"]').datepicker('setEndDate', ''+dd+'/'+mm+'/'+yy).trigger('change');

                        // $('input[name="daterange"]').daterangepicker({
                        //     startDate: today,
                        //     endDate: today,
                        //     minDate: today,
                        //     // maxDate: final_date,
                        //     locale: {
                        //         format: 'DD/MM/YYYY'
                        //     }

                        // });
                    }else{
                        $("#delivery_date_error").html("");
                        $("#delivery_date_msg").html("");
                        $("#deliver_qty_error").html("Deliver quantity should not be in decimal");
                        $("#choose_location_popup_done_btn").attr('disabled','disabled');
                    }


                }else{
                    $("#delivery_date_error").html("");
                    $("#delivery_date_msg").html("");
                    // $("#deliver_qty_error").html("Deliver quantity should be between 3 to 100");
                    $("#deliver_qty_error").html("Enter quantity between 3 to 100");
                    $("#choose_location_popup_done_btn").attr('disabled','disabled');
                }
            }else{
                $("#delivery_date_error").html("");
                $("#delivery_date_msg").html("");
                $("#deliver_qty_error").html("Deliver quantity should be in number");
                $("#choose_location_popup_done_btn").attr('disabled','disabled');
            }
        }else{
            $("#delivery_date_error").html("");
            $("#delivery_date_msg").html("");
            $("#deliver_qty_error").html("This field is required");
            $("#choose_location_popup_done_btn").attr('disabled','disabled');
        }


    });


    $("form[name='login-form']").validate({
        rules: {
            email_mobile: {
                required: true,
                mobile_or_email_validate: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 20
            }
        },
        messages: {

        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            var form_values = $("form[name='login-form']").serialize();
            // console.log("form_type ..."+form_values );

            // $("form[name='logistics_register_form']").submit();
            login(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });


    $("form[name='forgot_pass_form']").validate({
        rules: {
            email_mobile: {
                required: true,
                mobile_or_email_validate: true
            }
        },
        messages: {

        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            var form_values = $("form[name='forgot_pass_form']").serialize();
            // console.log("form_type ..."+form_values );

            // $("form[name='logistics_register_form']").submit();
            forgot_pass_otp_send(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("form[name='forgot_pass_form_resend']").validate({
        rules: {
            email_mobile: {
                required: true
            }
        },
        messages: {

        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            var form_values = $("form[name='forgot_pass_form_resend']").serialize();
            // console.log("form_type ..."+form_values );

            // $("form[name='logistics_register_form']").submit();
            forgot_pass_otp_send(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("form[name='forgot_otp_form']").validate({
        rules: {
            otp_code_1: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_2: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_3: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_4: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_5: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_6: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
        },
        messages: {

        },
        errorPlacement: function(error, element) {
            if ((element.attr("name") == "otp_code_1") || (element.attr("name") == "otp_code_2") || (element.attr("name") == "otp_code_3") || (element.attr("name") == "otp_code_4") || (element.attr("name") == "otp_code_5") || (element.attr("name") == "otp_code_6") ) {
            //   error.insertAfter("#OTP_error_div");
                var str = error.prop('outerHTML');
                // console.log(str);
              if(str.includes("This field is required.")){
                $("#OTP_error_div_forgot").html('<label class="error">All fields are required</label>');
              }else{
                $("#OTP_error_div_forgot").html(error);
              }

            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            var form_values = $("form[name='forgot_otp_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            forgotOTPVerify(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("form[name='forgot_pass_form_1']").validate({
        rules: {
            new_pass: {
                required: true,
                minlength : 6,
                maxlength: 20
            }, 
            confirm_pass:{
                required: true,
                minlength : 6,
                equalTo : "#new_password"
            },           
        },
        messages: {

        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            var form_values = $("form[name='forgot_pass_form_1']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            forgotPass(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("form[name='signup-emailverify-form']").validate({
        rules: {
            email_mobile: {
                required: true,
                mobile_or_email_validate: true,
            }
        },
        messages: {

        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            var form_values = $("form[name='signup-emailverify-form']").serialize();
            // console.log("form_type ..."+form_values );

            // $("form[name='logistics_register_form']").submit();
            signupOTPSend(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });



    $("form[name='signup-emailverify-form-resend']").validate({
        rules: {
            email_mobile: {
                required: true
            }
        },
        messages: {

        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            var form_values = $("form[name='signup-emailverify-form-resend']").serialize();
            // console.log("form_type ..."+form_values );

            // $("form[name='logistics_register_form']").submit();
            signupOTPSend(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("form[name='signup-emailverify-otp-form']").validate({
        rules: {
            otp_code_1: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_2: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_3: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_4: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_5: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
            otp_code_6: {
                required: true,
                number: true,
                minlength: 1,
                maxlength: 1
            },
        },
        messages: {

        },
        errorPlacement: function(error, element) {
            if ((element.attr("name") == "otp_code_1") || (element.attr("name") == "otp_code_2") || (element.attr("name") == "otp_code_3") || (element.attr("name") == "otp_code_4") || (element.attr("name") == "otp_code_5") || (element.attr("name") == "otp_code_6") ) {
            //   error.insertAfter("#OTP_error_div");
                var str = error.prop('outerHTML');
                // console.log(str);
              if(str.includes("This field is required.")){
                $("#OTP_error_div").html('<label class="error">All fields are required</label>');
              }else{
                $("#OTP_error_div").html(error);
              }

            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            var form_values = $("form[name='signup-emailverify-otp-form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            signupOTPVerify(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("#pass_eye_on").on("click", function(){
        $("#sign_up_password").attr("type","text");
        $("#pass_eye_on").hide();
        $("#pass_eye_off").show();
    });

    $("#pass_eye_off").on("click", function(){
        $("#sign_up_password").attr("type","password");
        $("#pass_eye_on").show();
        $("#pass_eye_off").hide();
    });

    $("#old_pass_eye_on").on("click", function(){
        $("#old_password").attr("type","text");
        $("#old_pass_eye_on").hide();
        $("#old_pass_eye_off").show();
    });

    $("#old_pass_eye_off").on("click", function(){
        $("#old_password").attr("type","password");
        $("#old_pass_eye_on").show();
        $("#old_pass_eye_off").hide();
    });

    $("#new_pass_eye_on").on("click", function(){
        $("#password").attr("type","text");
        $("#new_pass_eye_on").hide();
        $("#new_pass_eye_off").show();
    });

    $("#new_pass_eye_off").on("click", function(){
        $("#password").attr("type","password");
        $("#new_pass_eye_on").show();
        $("#new_pass_eye_off").hide();
    });
    
    $("#confirm_pass_eye_on").on("click", function(){
        $("#change_pass").attr("type","text");
        $("#confirm_pass_eye_on").hide();
        $("#confirm_pass_eye_off").show();
    });

    $("#confirm_pass_eye_off").on("click", function(){
        $("#change_pass").attr("type","password");
        $("#confirm_pass_eye_on").show();
        $("#confirm_pass_eye_off").hide();
    });

    // $("#register_account_type").change(function() {
    //     $selected_value = $('option:selected', this).val();
    //     // alert( $('option:selected', this).text() );

    //     var html = '<option value="">Select Company Type</option>';

    //     if($selected_value == "Individual"){
    //         html += '<option value="One Person Company">One Person Company</option>';
    //     }else{
    //         html += '<option value="Partnership">Partnership</option>';
    //         html += '<option value="Proprietor">Proprietor</option>';
    //         html += '<option value="One Person Company">One Person Company</option>';
    //         html += '<option value="LLP.">LLP</option>';
    //         html += '<option value="LTD.">LTD.</option>';
    //         html += '<option value="PVT LTD">PVT LTD</option>';
    //     }

    //     $("#register_company_type").html(html);
    // });

    $("form[name='signup-form']").validate({
        rules: {
            full_name: {
                required: true,
                noSpace: true,
                a_z_pattern: true,
                minlength: 3,
                maxlength: 64
            },
            mobile_number: {
                required: true,
                mobile_validation: true,
                minlength: 13,
                maxlength: 13,
                noDecimal:true
            },
            email: {
                required: true,
                email_validate: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 20
            },
            account_type: {
                required: true
            },
            company_type: {
                required: function(){

                   var register_account_type = $("#register_account_type").val();

                   if(register_account_type == 'Individual'){
                       return false;
                   }

                   return true;

                }
            },
            payment_method: {
                required: false
            },
            company_name: {
                required: function(){

                    var register_account_type = $("#register_account_type").val();

                    if(register_account_type == 'Individual'){
                        return false;
                    }

                    return true;

                 }
            },
            identity_image: {
                required: true,
                extension: "jpg|png|jpeg"
            },
            pan_number: {
                required: true,
                pan:true
            },
            gst_number: {
                required: function(){

                    var register_account_type = $("#register_account_type").val();

                    if(register_account_type == 'Individual'){
                        return false;
                    }

                    return false;

                 },
                gst: true

            },
            terms_condition:{
                required: true
            }
        },
        messages: {
            identity_image: {
                extension: "Please select valid file (jpg, png, jpeg) format"
            },
            terms_condition:{
                required: "Please accept Terms and Conditions"
            },
            mobile_number:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            },
            email:{
                email_validate: "Please enter valid email address"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "identity_image" ) {
              error.insertAfter("#identity_image_file");
            }else if (element.attr("name") == "terms_condition" ) {
              error.insertAfter("#terms_condition_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            // var form_values = $("form[name='signup-form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            var form_values = new FormData(form);
            signup(form_values);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

});



function signupOTPSend(form_values){

    customResponseHandler("buyer/signup_otp_send",
                    "POST",
                    form_values,
                    function response(data){
                        console.log(data);

                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["data"]["message"];
                            showToast(""+message,"Success");
                            $("#sign-up-form-1").modal('hide');

                            $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#verify_sign_up_type").val(data["data"]["signup_type"]);
                            $("#oyp_ode_confirm_message").html(message);

                            var fiveMinutes = 60 * 2,

                            display = $('#timer');
                            startTimer(fiveMinutes, display, "buyer_otp_verify");

                            setTimeout(function(){

                                // var counter = 60;
                                // var interval = setInterval(function() {
                                //     counter--;
                                //     // Display 'counter' wherever you want to display it.
                                //     if (counter <= 0) {
                                //             clearInterval(interval);

                                //         $('#timer').html('<a href="javascript:void(0)" onclick="otpResend()">Resend OTP</a>');
                                //         return;
                                //     }else{
                                //         $('#time').text(counter);
                                //     // console.log("Timer --> " + counter);
                                //     }
                                // }, 1000);
                                resetForm('signup-emailverify-otp-form','sign-up-form-2')
                                $("#sign-up-form-2").modal('show');
                            }, 500);
                        }else{
                            // showToast(""+data["error"]["message"],"Error");
                            $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );

}

function otpResend(){


    var form_values = $("form[name='signup-emailverify-form-resend']").serialize();
    // console.log("form_type ..."+form_values );

    // $("form[name='logistics_register_form']").submit();
    $('#timer_forgot_pass').html('00:<span id="time">60</span>');
    signupOTPSend(form_values);


}

function signupOTPVerify(form_values){

    customResponseHandler("buyer/signup_otp_verify",
                    "POST",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){
                            showToast("OTP verified successfully","Success");
                            $("#sign-up-form-2").modal('hide');

                            $("#signup_form_type").val(data["data"]["signup_type"]);
                            if(data["data"]["signup_type"] == "mobile"){
                                $("#signup_phone").val(data["data"]["email_mobile"]);
                                $("#signup_phone").attr('readonly','true');
                            }else if(data["data"]["signup_type"] == "email"){
                                $("#register_email").val(data["data"]["email_mobile"]);
                                $("#register_email").attr('readonly','true');
                            }
                            // $("#signup_phone").attr('readonly','disabled');


                            setTimeout(function(){
                                // resetForm('signup-form','sign-up-form-3')
                                $("#sign-up-form-3").modal('show');
                            }, 500);
                        }else{
                            // showToast(""+data["error"]["message"],"Error");

                            showErrorMsgInForm("signup-emailverify-otp-form",data["error"]["message"]);
                        }

                    }
                );

}

function signup(form_values){

    responsehandlerWithFiles("buyer/register",
                    "POST",
                    form_values,
                    '#sign-up-form-3',
                    'You are successfully registered',
                    0,
                    "signup-form"
                );

}

function login(form_values){

    responsehandler("buyer/login",
                    "POST",
                    form_values,
                    '#Login-form',
                    'You are logged in successfully',
                    0,
                    'login-form'
                );


    // customResponseHandler("buyer/login",
    //     "POST",
    //     form_values,
    //     function response(data){
    //         console.log(data);

    //         if((data["status"] >= 200) && (data["status"] < 300)){
    //             var message = "You are logged in successfully";
    //             showToast(""+message,"Success");
    //             $("#Login-form").modal('hide');


    //         }else{
    //             // showToast(""+data["error"]["message"],"Error");
    //             // $('#forgot_pass_otp_send_error').html(data["error"]["message"]);

    //         }

    //     }
    // );

}

function forgot_pass_otp_send(form_values){

    // responsehandler("buyer/forgot_pass_otp_send",
    //                 "POST",
    //                 form_values,
    //                 '#password-form',
    //                 'OTP sent on registered mobile number',
    //                 0,
    //                 'forgot_pass_error'
    //             );


        customResponseHandler("buyer/forgot_pass_otp_send",
                "POST",
                form_values,
                function response(data){
                    console.log(data);

                    if((data["status"] >= 200) && (data["status"] < 300)){
                        var message = data["data"]["message"];
                        showToast(""+message,"Success");
                        $("#forgot_pass_pop").modal('hide');

                        $("#forgot_resend_email_mobile").val(data["data"]["email_mobile"]);
                        $("#forgot_verify_email_mobile").val(data["data"]["email_mobile"]);
                        $("#forgot_code_confirm_message").html(message);

                        var fiveMinutes = 60 * 2,

                        display = $('#timer_forgot_pass');
                        startTimer(fiveMinutes, display, "buyer_otp_forgot_pass");

                        // var counter = 60;
                        // var interval = setInterval(function() {
                        //     counter--;
                        //     // Display 'counter' wherever you want to display it.
                        //     if (counter <= 0) {
                        //             clearInterval(interval);

                        //         $('#timer_forgot_pass').html('<a href="javascript:void(0)" onclick="otpResendForgotPass()">Resend OTP</a>');
                        //         return;
                        //     }else{
                        //         $('#time_forgot_pass').text(counter);
                        //     console.log("Timer --> " + counter);
                        //     }
                        // }, 1000);
                        resetForm('forgot_otp_form','forgot_otp_popup');

                        $("#forgot_otp_popup").modal('show');
                    }else{
                        // showToast(""+data["error"]["message"],"Error");
                        // $('#forgot_pass_otp_send_error').html(data["error"]["message"]);
                        showErrorMsgInForm("forgot_pass_form",data["error"]["message"]);

                    }

                }
            );

}

function otpResendForgotPass(){


    var form_values = $("form[name='forgotpass-emailverify-form-resend").serialize();
    // console.log("form_type ..."+form_values );

    // $("form[name='logistics_register_form']").submit();

    $('#timer_forgot_pass').html('00:<span id="time_forgot_pass">60</span>');

    forgot_pass_otp_send(form_values);


}

function forgotOTPVerify(form_values){

    customResponseHandler("buyer/forgot_pass_otp_verify",
                    "POST",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){
                            showToast("OTP verified successfully","Success");
                            $("#forgot_otp_popup").modal('hide');
                            $("#forgot_pass_pop_1").modal('show');
                            // reload();
                            // $("#signup_form_type").val(data["data"]["signup_type"]);
                            // $("#signup_phone").val(data["data"]["email_mobile"]);
                            // $("#signup_phone").attr('readonly','disabled');

                            // $("#sign-up-form-3").modal('show');
                        }else{
                            // showToast(""+data["error"]["message"],"Error");

                            showErrorMsgInForm("forgot_otp_form",data["error"]["message"]);
                        }

                    }
                );

}

function forgotPass(form_values){

    customResponseHandler("buyer/forgot_pass",
                    "POST",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){
                            showToast("Password updated successfully","Success");
                           
                            $("#forgot_pass_pop_1").modal('hide');
                            reload();
                            // $("#signup_form_type").val(data["data"]["signup_type"]);
                            // $("#signup_phone").val(data["data"]["email_mobile"]);
                            // $("#signup_phone").attr('readonly','disabled');

                            // $("#sign-up-form-3").modal('show');
                        }else{
                            // showToast(""+data["error"]["message"],"Error");

                            showErrorMsgInForm("forgot_pass_form_1",data["error"]["message"]);
                        }

                    }
                );

}

// Sign up Module Over..................................................

// Profile Module Start..................................................

$(document).ready(function(){

    // $('#' + country_name_selectID + '_chosen .chosen-search input').keyup(function(e){

    //     if ((e.keyCode != 13) && (e.keyCode != 38) && (e.keyCode != 40)  ) {
    //     // Change No Result Match text to Searching.
    //   $('#' + country_name_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + country_name_selectID + '_chosen .chosen-search input').val() + '"');


    //   clearTimeout(typingTimer);  //Refresh Timer on keyup
    // //   if ($('#' + country_name_selectID + '_chosen .chosen-search input').val()) {

    //        typingTimer = setTimeout(searchCountryState, doneTypingInterval,'buyer/getCountry',country_name_selectID);  //Set timer back if got value on input

    // //   }
    //     }
    // });

    // $('#' + state_name_city_selectID + '_chosen .chosen-search input').keyup(function(e){
    //     console.log("key..."+e.keyCode);
    //     if ((e.keyCode != 13) && (e.keyCode != 38) && (e.keyCode != 40)  ) {
    //     // Change No Result Match text to Searching.
    //   $('#' + state_name_city_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + state_name_city_selectID + '_chosen .chosen-search input').val() + '"');


    //   clearTimeout(typingTimer);  //Refresh Timer on keyup
    // //   if ($('#' + state_name_city_selectID + '_chosen .chosen-search input').val()) {

    //        typingTimer = setTimeout(searchCountryState, doneTypingInterval,'buyer/getStates',state_name_city_selectID);  //Set timer back if got value on input

    // //   }
    //     }

    // });



    // $('#' + state_name_city_filter_selectID + '_chosen .chosen-search input').keyup(function(e){
    //     if ((e.keyCode != 13) && (e.keyCode != 38) && (e.keyCode != 40)  ) {
    //     // Change No Result Match text to Searching.
    //   $('#' + state_name_city_filter_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + state_name_city_filter_selectID + '_chosen .chosen-search input').val() + '"');

    //   clearTimeout(typingTimer);  //Refresh Timer on keyup

    //   typingTimer = setTimeout(searchCountryState, doneTypingInterval,'buyer/getCities',state_name_city_filter_selectID);  //Set timer back if got value on input

    //     }
    // });







    // $('#' + state_name_selectID + '_chosen .chosen-search input').keyup(function(e){

    //     if ((e.keyCode != 13) && (e.keyCode != 38) && (e.keyCode != 40)  ) {
    //     // Change No Result Match text to Searching.
    //   $('#' + state_name_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + state_name_selectID + '_chosen .chosen-search input').val() + '"');


    //   clearTimeout(typingTimer);  //Refresh Timer on keyup
    // //   if ($('#' + state_name_selectID + '_chosen .chosen-search input').val()) {

    //        typingTimer = setTimeout(searchCountryState, doneTypingInterval,'buyer/getStates',state_name_selectID);  //Set timer back if got value on input

    // //   }
    //     }
    // });


    // $('#' + city_name_selectID + '_chosen .chosen-search input').keyup(function(e){
    //     if ((e.keyCode != 13) && (e.keyCode != 38) && (e.keyCode != 40) ) {
    //     // Change No Result Match text to Searching.
    //   $('#' + city_name_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + city_name_selectID + '_chosen .chosen-search input').val() + '"');

    //   clearTimeout(typingTimer);  //Refresh Timer on keyup

    //   typingTimer = setTimeout(searchCountryState, doneTypingInterval,'buyer/getSiteCities',city_name_selectID);  //Set timer back if got value on input
    //     }

    // });


    $("form[name='account_details_update_form']").validate({
        rules: {
            full_name: {
                required: true,
                noSpace: true,
                a_z_pattern: true,
                minlength: 3,
                maxlength: 64
            },
            mobile_number: {
                required: true,
                mobile_validation: true,
                noDecimal: true,
                minlength: 13,
                maxlength: 13
            },
            email: {
                required: true,
                email_validate: true
            },
            account_type: {
                required: true
            },
            company_name: {
                required: function(){

                    var register_account_type = $("#profile_account_type").val();

                    if(register_account_type == 'Individual'){
                        return false;
                    }

                    return true;

                 }
            },
            company_type: {
                required: true
            },
            payment_method: {
                required: true
            },
            pan_number: {
                required: true,
                pan: true
            },
            gst_number: {
                required: false,
                gst: true
            },
            identity_image:{
                extension: "jpg|jpeg"
            }

        },
        messages: {
            identity_image:{
                extension: "Please select valid file (jpg,jpeg) format"
            },
            mobile_number:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            },
            email:{
                email_validate: "Please enter valid email address"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "identity_image" ) {
              error.insertAfter("#identity_image_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited.....");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            // var form_values = $("form[name='account_details_update_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            var form_values = new FormData(form);
            updateProfile(form_values);

            // var is_email_change = 0;
            // var is_mobile_change = 0;   
            
            // var edit_address_mobile = $("#edit_address_mobile").val();
            // var edit_address_email = $("#edit_address_email").val();

            // var mobile_or_email = $("#mobile_or_email").val();
            // var email = $("#edit_profile_email").val();                   

            // if(edit_address_mobile != mobile_or_email){
            //     is_mobile_change = 1;
            // }

            // if(edit_address_email != email){
            //     is_email_change = 1;
            // }

            
            // requestOTPforEditProfile(1,form_values,is_mobile_change,is_email_change,1);


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });
    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='add_site_form']").validate({
        rules: {

            company_name: {
                required: true,
                noSpace: true,
                a_z_pattern: true,
                minlength: 3,
                maxlength: 64
            },
            person_name: {
                required: true,
                noSpace: true,
                a_z_pattern: true,
                minlength: 3,
                maxlength: 64
            },
            site_name: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 64
            },
            mobile_number: {
                required: true,
                mobile_validation: true,
                minlength:13,
                maxlength: 13
            },
            email: {
                required: true,
                email: true
            },
            address_line1: {
                required: true
            },
            address_line2: {
                required: true
            },
            country_id: {
                required: true,
            },
            state_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            pincode: {
                required: true,
                number: true,
                minlength: 6,
                maxlength: 6
            },
            us2_address: "required",
            billing_address_id: "required",
        },
        messages: {
            mobile_number:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "country_id" ) {
              error.insertAfter("#country_id_div");
              error.insertAfter("#add_address_country_id_div");
            } else if (element.attr("name") == "state_id" ) {
            //   error.insertAfter("#state_id_div");
              error.insertAfter("#add_address_state_id_div");
            } else if (element.attr("name") == "city_id" ) {
            //   error.insertAfter("#city_id_div");
              error.insertAfter("#add_address_city_id_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_site_form"] p.error').remove();
            var us2_lat = $("#us2_lat").val();
            var us2_lon = $("#us2_lon").val();
            var is_valid = true;
            if(us2_lat == 0){
                is_valid = false;
                $("#us2_lat_error").html("Please select location");
                $("#us2_address").focus();
            }

            if(us2_lon == 0){
                is_valid = false;
                $("#us2_lon_error").html("Please select location");
                $("#us2_address").focus();
            }

            if(is_valid){
                var form_type = $("form[name='add_site_form'] #add_site_type").val();
                var form_values = $("form[name='add_site_form']").serialize();
                // console.log("form_type ..."+form_type );

                // $("form[name='logistics_register_form']").submit();
                // var form_values = new FormData(form);
                if(form_type === 'add'){
                    addSite(form_values);
                }else{
                    editSite(form_values);
                }

            }



        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_site_form"] p.error').remove();
        }
    });

    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='add_billing_address_form']").validate({
        rules: {

            company_name: {
                required: true,
                noSpace: true,
                a_z_pattern: true,
                minlength: 3,
                maxlength: 64
            },

            address_line1: {
                required: true
            },
            address_line2: {
                required: true
            },
            state_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            pincode: {
                required: true,
                number: true,
                minlength: 6,
                maxlength: 6
            },
            gst_number: {
                required: true,
                gst:true
            },
        },
        messages: {

        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "country_id" ) {
              error.insertAfter("#country_id_div");
              error.insertAfter("#add_address_country_id_div");
            } else if (element.attr("name") == "state_id" ) {
            //   error.insertAfter("#state_id_div");
              error.insertAfter("#billing_address_state_id_div");
            } else if (element.attr("name") == "city_id" ) {
            //   error.insertAfter("#city_id_div");
              error.insertAfter("#billing_address_city_id_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_billing_address_form"] p.error').remove();

                var form_type = $("form[name='add_billing_address_form'] #billing_address_form_type").val();
                var form_values = $("form[name='add_billing_address_form']").serialize();
                // console.log("form_type ..."+form_type );

                // $("form[name='logistics_register_form']").submit();
                // var form_values = new FormData(form);
                if(form_type === 'add'){
                    addBillingAddress(form_values);
                }else{
                    editBillingAddress(form_values);
                }





        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_site_form"] p.error').remove();
        }
    });


    $("form[name='change_pass_form']").validate({
        rules: {

            old_password: {
                required: true,
                minlength : 6,
                maxlength: 20
            },
            password: {
                required: true,
                minlength : 6,
                maxlength: 20
            },
            change_pass: {
                required: true,
                minlength : 6,
                maxlength:20,
                equalTo : "#password"
            }
        },
        messages: {
            change_pass: {
                minlength: "Confirm password should be same as new password",
                maxlength: "Confirm password should be same as new password",
                equalTo: "Confirm password should be same as new password"
            }
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='change_pass_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            

            requestOTP(1,form_values);

            // changePassword(form_values);




        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });
    
    
    $("form[name='update-mobile-otp-form']").validate({
        rules: {
            otp_code_1: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_2: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_3: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_4: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_5: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_6: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if ((element.attr("name") == "otp_code_1") || (element.attr("name") == "otp_code_2") || (element.attr("name") == "otp_code_3") || (element.attr("name") == "otp_code_4") || (element.attr("name") == "otp_code_5") || (element.attr("name") == "otp_code_6") ) {
            //   error.insertAfter("#OTP_error_div");
                var str = error.prop('outerHTML');
                // console.log(str);
              if(str.includes("This field is required.")){
                $("#OTP_error_div_mobile").html('<label class="error">All fields are required</label>');
              }else{
                $("#OTP_error_div_mobile").html(error);
              }

            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='update-mobile-otp-form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            updateMobileEmail(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });
    
    
    $("form[name='new_mobile_form']").validate({
        rules: {
            mobile: {
                required: true,
                minlength : 13,
                maxlength: 13,
                noDecimal: true
            },
            
            
        },
        messages: {
            mobile:{
                minlength: "Please enter valid mobile number",
                maxlength: "Please enter valid mobile number",
            }
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='new_mobile_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            // updateMobileEmail(form_values,"old_mobile");

            OTPonUpdateMobile(0, 1);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });
    

    $("form[name='update-email-otp-form']").validate({
        rules: {
            otp_code_1: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_2: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_3: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_4: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_5: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            otp_code_6: {
                required: true,
                minlength : 1,
                maxlength: 1,
                number:true,
                noDecimal: true
            },
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if ((element.attr("name") == "otp_code_1") || (element.attr("name") == "otp_code_2") || (element.attr("name") == "otp_code_3") || (element.attr("name") == "otp_code_4") || (element.attr("name") == "otp_code_5") || (element.attr("name") == "otp_code_6") ) {
            //   error.insertAfter("#OTP_error_div");
                var str = error.prop('outerHTML');
                // console.log(str);
              if(str.includes("This field is required.")){
                $("#OTP_error_div_email").html('<label class="error">All fields are required</label>');
              }else{
                $("#OTP_error_div_email").html(error);
              }

            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='update-email-otp-form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            updateEmail(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });


    $("form[name='new_email_form']").validate({
        rules: {
            email: {
                required: true,
                email_validate: true
            },
            
            
        },
        messages: {
            email:{
                email_validate: "Please enter valid email address"
            }
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='new_email_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            // updateMobileEmail(form_values,"old_mobile");

            OTPonUpdateEmail(0, 1);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("#password").keyup(function(){
        if($("#password").val().length >= 6){
            $("#change_pass").removeAttr("readOnly");
        }else{
            $("#change_pass").attr("readOnly","true");
        }

    });

    $('#add_site_country').on('change', function (e) {

        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        getStatesByCountry(valueSelected);

    });

    $('#add_site_state').on('change', function (e) {

        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        getCityByState(valueSelected);

    });

    $('#add_site_city').on('change', function (e) {

        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        console.log(optionSelected);
        console.log(optionSelected.attr('data-lat'));
        // getCityByState(valueSelected);

        $('#site_lat').val(optionSelected.attr('data-lat'));
        $('#site_long').val(optionSelected.attr('data-long'));

    });

    $('#add_site').on('click', function (e) {

        $("#site_lis_div").hide();
        $("#add_site_list_div").show();

    });

    $('#add_site_cancel_button').on('click', function (e) {

        $("#add_site_list_div").hide();
        $("#site_lis_div").show();

    });

    $('#confirmOk').on('click', function (e) {



    });



    $('#site_name').keyup(function(e){

        var site_name = $("#site_name").val();

        if ((e.keyCode != 13) && (e.keyCode != 38) && (e.keyCode != 40)  ) {

            if(site_name.length >= 3 ){

                // Change No Result Match text to Searching.
                // $('#' + country_name_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + country_name_selectID + '_chosen .chosen-search input').val() + '"');


                clearTimeout(typingTimer);  //Refresh Timer on keyup
                    //   if ($('#' + country_name_selectID + '_chosen .chosen-search input').val()) {

                            var form_values = {
                                "site_name" : ""+site_name
                            };

                            typingTimer = setTimeout(checkSiteExist, doneTypingInterval,form_values);  //Set timer back if got value on input

                    //   }
                }
        }
    });


});

function OTPonUpdateMobile(is_old_mobile_no, is_new_mobile_no, is_resend=0){

    if(is_old_mobile_no == 1){
        var mobile_or_email = $("#mobile_or_email").val();
    }
    
    if(is_new_mobile_no == 1){
        var mobile_or_email = $("#update_new_mobile_no").val();
    }
    
    $("#verify_update_mobile").val(mobile_or_email);
    
    requestOTPforEditProfile(1,mobile_or_email,is_old_mobile_no,0,is_new_mobile_no,0,1,is_resend);

}

function OTPonUpdateEmail(is_old_email, is_new_email,is_resend=0){

    if(is_old_email == 1){
        var mobile_or_email = $("#edit_address_email").val();
    }
    
    if(is_new_email == 1){
        var mobile_or_email = $("#update_new_email").val();
    }
    
    $("#verify_update_email").val(mobile_or_email);
    
    requestOTPforEditProfile(1,mobile_or_email,0,is_old_email,0,is_new_email,1,is_resend);

}


function updateProfile(form_values){

    // form_values=form_values+"&mobile_or_email_type="+mobile_or_email_type;

    // form_values.append('mobile_or_email_type', mobile_or_email_type);
    // form_values.append('email_code', email_otp);
    // form_values.append('authentication_code', old_mobile_otp);

    responsehandlerWithFiles("buyer/profile_edit",
                    "POST",
                    form_values,
                    '#sign-up-form-3',
                    'Profile updated successfully'
                );

}

function updateMobileEmail(form_values){

    // form_values=form_values+"&mobile_or_email_type="+mobile_or_email_type;

    // form_values.append('mobile_or_email_type', mobile_or_email_type);
    // form_values.append('email_code', email_otp);
    // form_values.append('authentication_code', old_mobile_otp);

    customResponseHandler("buyer/profile_edit",
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){                            

                            $("#update_mobile_otp").modal("hide");

                            if($("#mobile_type").val() == "old_mobile"){
                                showToast("OTP verified successfully","success");
                                $("#new_mobile_no_popup").modal("show");
                            }else{
                                showToast("Mobile No updated successfully","success");
                                reload();
                            }

                        }else{
                            showToast("Entered OTP is Wrong. Please check again","error");
                            // $("#update_mobile_otp_error").html("Enter OTP is Wrong. Please check and enter again");
                        }

                    }
                );

}

function updateEmail(form_values){

    // form_values=form_values+"&mobile_or_email_type="+mobile_or_email_type;

    // form_values.append('mobile_or_email_type', mobile_or_email_type);
    // form_values.append('email_code', email_otp);
    // form_values.append('authentication_code', old_mobile_otp);

    customResponseHandler("buyer/profile_edit",
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){
                            

                            $("#update_email_otp").modal("hide");

                            if($("#email_type").val() == "old_email"){
                                showToast("OTP verified successfully","success");
                                $("#new_email_no_popup").modal("show");
                            }else{
                                showToast("Email updated successfully","success");
                                reload();
                            }

                        }else{
                            showToast("Entered OTP is Wrong. Please check again","error");
                            // $("#update_mobile_otp_error").html("Enter OTP is Wrong. Please check and enter again");
                        }

                    }
                );

}

function getStatesByCountry(country_id){

    var request_data = {'country_id':country_id};

    customResponseHandler("buyer/getStateByCountry",
                    "GET",
                    request_data,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        var state_data = data["data"];
                        var html = '<option disabled="disabled" selected="selected">Select State</option>';
                        $.each(state_data,function(key,value){

                            html += '<option value="'+value["_id"]+'">'+value["state_name"]+'</option>';

                        });
                        // console.log(html);
                        $('#add_site_state').html(html);

                        $('#add_site_state').trigger('chosen:updated');
                    }
                );

}

function getCityByState(state_id){

    var request_data = {'state_id':state_id};

    customResponseHandler("buyer/getCityByState",
                    "GET",
                    request_data,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        var state_data = data["data"];
                        var html = '<option disabled="disabled" selected="selected">Select City</option>';
                        $.each(state_data,function(key,value){

                            html += '<option value="'+value["_id"]+'" data-lat="'+value["location"]["coordinates"][1]+'" data-long="'+value["location"]["coordinates"][0]+'">'+value["city_name"]+'</option>';

                        });

                        $('#add_site_city').html(html);

                        $('#add_site_city').trigger('chosen:updated');
                    }
                );

}


function addSite(form_values){

    responsehandler("buyer/add_site",
                    "POST",
                    form_values,
                    '#add_site_address_popup',
                    'Delivery Address added successfully',
                    0,
                    'add_site_form'
                );

}

function checkSiteExist(form_values){

    $("#real_time_check_loader").show();
    customResponseHandler("buyer/check_site_exist",
        "GET",
        form_values,
        function response(data){
            console.log(data);

            if(data["status"] == 400){

                $("#site_name_div p.error").remove();
                $("#site_name_div").append("<p class='error'>"+data["error"]["message"]+"</p>")

            }else{
                $("#site_name_div p.error").remove();
            }

            $("#real_time_check_loader").hide();
        }
    );

}

function editSiteView(data){
    console.log(data);

    // resetForm('add_site_form','add_site_address_popup');
    resetEditForm('add_site_form');

    data = JSON.parse(data);
    console.log(data);

    $('#company_name').val(data["company_name"]);
    $('#site_name').val(data["site_name"]);
    $('#person_name').val(data["person_name"]);
    $('#phone1').val(data["mobile_number"]);
    $('#email').val(data["email"]);

    
    if(data["billing_address_id"] != undefined){
        $('#billing_address_id').val(data["billing_address_id"]);
    }

    $('#add_site_country').val(data["country_id"]);
    $('#add_site_country').attr('readonly','readonly');
    $('#add_site_country').prop('disabled', true);
    $("#add_site_country").trigger("chosen:updated");

    $('#add_site_state').html('<option value="'+data["state_id"]+'">'+data["state_name"]+'</option>');
    $('#add_site_state').attr('readonly','readonly');
    $('#add_site_state').prop('disabled', true);
    $("#add_site_state").trigger("chosen:updated");

    $('#add_site_city').html('<option value="'+data["city_id"]+'">'+data["city_name"]+'</option>');
    $('#add_site_city').attr('readonly','readonly');
    $('#add_site_city').prop('disabled', true);
    $("#add_site_city").trigger("chosen:updated");

    $('#site_lat').val(data["location"]["coordinates"][1]);
    $('#site_long').val(data["location"]["coordinates"][0]);
    $('#pincode').val(data["pincode"]);

    $('#site_id').val(data["_id"]);

    $('#add_site_type').val("edit");

    // $('#us2_address').prop('disabled', true);
    $("#us2_lat").val(data["location"]["coordinates"][1]);
    $("#us2_lon").val(data["location"]["coordinates"][0]);

    $('#us2').locationpicker("location", {latitude: data["location"]["coordinates"][1], longitude: data["location"]["coordinates"][0]},


    );

    setTimeout( function(){ 
        // Do something after 1 second 

        $('#address_line1').val(data["address_line1"]);
        $('#address_line2').val(data["address_line2"]);
      }  , 500 );

    

    // $("#site_lis_div").hide();
    // $("#add_site_list_div").show();

    $("#add_site_address_popup").modal("show");

}



function editSite(form_values){

    responsehandler("buyer/edit_site",
                    "POST",
                    form_values,
                    '#add_site_address_popup',
                    'Delivery Address updated successfully'
                );

}


function siteDeleteConfirmPopup(site_id){
    console.log("site_id...."+site_id);
    var message = "Are you sure you want to delete site?";

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        deleteSite(site_id)

    });
}

function deleteSite(site_id){
    var form_values = {
        "site_id":site_id
    };

    responsehandler("buyer/delete_site",
                    "GET",
                    form_values,
                    '#Login-form',
                    'Site deleted successfully'
                );

}

function addBillingAddress(form_values){

    responsehandler("buyer/add_billing_address",
                    "POST",
                    form_values,
                    '#add_billing_address_popup',
                    'Billing Address added successfully',
                    0,
                    'add_billing_address_form'
                );

}

function editBillingAddressView(data){
    

    $("#add_billing_address_form_title").html("Edit Billing Address");

    // resetForm('add_site_form','add_site_address_popup');
    resetEditForm('add_billing_address_form');

    data = JSON.parse(data);
    console.log(data);
    if(data["company_name"] != undefined){
        $('#billing_company_name').val(data["company_name"]);
    }
    
    if(data["full_name"] != undefined){
        $('#billing_full_name').val(data["full_name"]);
    }


    $('#billing_address_line1').val(data["line1"]);
    $('#billing_address_line2').val(data["line2"]);



    $('#billing_pincode').val(data["pincode"]);

    $('#billing_address_form_id').val(data["_id"]);
    $('#state_name_city_address').val(data["state_id"]);
    $('#state_name_city_filter').val(data["city_id"]);
    $('#gst_number').val(data["gst_number"]);

    $('#billing_address_form_type').val("edit");


    // $("#site_lis_div").hide();
    // $("#add_site_list_div").show();

    $("#add_billing_address_popup").modal("show");

}

function editBillingAddress(form_values){

    responsehandler("buyer/edit_billing_address",
                    "POST",
                    form_values,
                    '#add_billing_address_popup',
                    'Billing Address updated successfully',
                    0,
                    'add_billing_address_form'
                );

}

function billingDeleteConfirmPopup(site_id){
    console.log("site_id...."+site_id);
    var message = "Are you sure you want to delete this billing address?";

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        deleteBilling(site_id)

    });
}

function deleteBilling(billing_id){
    var form_values = {
        "billing_id":billing_id
    };

    responsehandler("buyer/delete_billing_address",
                    "GET",
                    form_values,
                    '#Login-form',
                    'Billing address deleted successfully'
                );

}


function changePassword(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("buyer/change_pass",
                    "POST",
                    form_values,
                    '#Login-form',
                    'Password changed successfully',
                    1
                );

}

function showDeletePopUp(){

    $('#confirmModal').modal('show');

}
// Profile Module Over..................................................

// Support Ticket MODULE Start...........................................


$(document).ready(function() {
    /*character limit script start*/
    // $('textarea#ctsubject, textarea#ctdescription').characterlimit();
    /*character limit script end*/


    $("form[name='add_support_ticket_form']").validate({

        rules:{
            question_type: "required",
            severity: "required",
            subject: {
                required: true,
                noSpace: true
            },
            description: {
                required: true,
                noSpace: true
            },
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf"
            }


        },
        messages:{
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf) file format"
            }

        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "question_type" ) {
              error.insertAfter("#question_type_div");
            } else if (element.attr("name") == "severity" ) {
              error.insertAfter("#severity_div");
            } else if (element.attr("name") == "attachments[]" ) {
                error.insertAfter("#support_ticket_attachment_div");
              } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");

            // var form_values = $("form[name='add_support_ticket_form']").serialize();


            var form_values = new FormData(form);
            console.log(form_values);

            addSupportTicket(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });






    /*reply form show hide script */
    $('.showButton').click(function () {
        $('.rplyshowhide').fadeIn();
        $('.rplyshow').fadeOut();
    });
    $('.hideButton').click(function () {
        $('.rplyshowhide').fadeOut();
        $('.rplyshow').fadeIn();
    });

    $("form[name='support_ticket_reply_form']").validate({

        rules:{

            comment: "required",
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf|png"
            }

        },
        messages:{
            comment: "Comment is required",
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf, png) file format"
            }
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");

            // var form_values = $("form[name='add_support_ticket_form']").serialize();


            var form_values = new FormData(form);
            console.log(form_values);

            replySupportTicket(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });




});

function addSupportTicket(form_values){

    responsehandlerWithFiles("buyer/my_support_ticket/add",
                    "POST",
                    form_values,
                    "#create-ticket-popup",
                    "Support Ticket Added successfully"
                );

}

function replySupportTicket(form_values){

    responsehandlerWithFiles("buyer/my_support_ticket/reply",
                    "POST",
                    form_values,
                    "#create-ticket-popup",
                    "Reply sent successfully"
                );

}

// Support Ticket MODULE OVER...........................................

// Product MODULE Start...........................................

$(document).ready(function(){
    // $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    // $.validator.setDefaults({ ignore: ":hidden" })
    $("form[name='user_location_form']").validate({

        rules:{
            selected_delivery_date:{
                required: true
            },
            delivery_qty:{
                required: true
            },


        },
        messages:{

        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "selected_delivery_date" ) {
              error.insertAfter("#selected_delivery_date_div");
            }else if (element.attr("name") == "city_id" ) {
              error.insertAfter("#city_id_div");
            }else if (element.attr("name") == "product_cat_id" ) {
              error.insertAfter("#location_select_grade_div");
            }else if (element.attr("name") == "type_of_rmc" ) {
              error.insertAfter("#location_select_type_rmc_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            // event.preventDefault();
            console.log("submited");

            $("#delivery_date_error").html("");
            $("#location_error").html("");
            $("#deliver_qty_error").html("");

            var is_site_selected = $("#is_location_site_selected").val();
            console.log("is_site_selected.."+is_site_selected);

            var delivery_date = $("#selected_delivery_date").val();
            var product_lat = $("#product_lat").val();
            var product_long = $("#product_long").val();
            var deliver_qty = $("#deliver_qty").val();
            var grade_id = $("#location_dialog_grade_dropdown").val() != null ? $("#location_dialog_grade_dropdown").val() : '';
            var type_of_rmc = $('input[type=radio][name=rmc_type]:checked').val() != undefined ? $('input[type=radio][name=rmc_type]:checked').val() : '';

            var is_all_valid = 1;

            console.log("delivery_date: "+delivery_date);
            console.log("product_lat: "+product_lat);
            console.log("product_long: "+product_long);
            console.log("is_all_valid: "+is_all_valid);
            console.log("grade_id: "+grade_id);
            console.log("type_of_rmc: "+type_of_rmc);

            if(delivery_date == ''){

                is_all_valid = 0;
                $("#delivery_date_error").html("This field is required");
            }

            if(product_lat == ''){
                is_all_valid = 0;

                $("#location_error").html("This field is required");
            }
            
            if(grade_id == ''){
                is_all_valid = 0;

                $("#location_popup_grade_error").html("This field is required");
            }
            
            if(type_of_rmc == ''){
                is_all_valid = 0;

                $("#location_popup_type_rmc_error").html("This field is required");
            }

            if(deliver_qty == ''){
                is_all_valid = 0;

                $("#deliver_qty_error").html("This field is required");
            }else if(deliver_qty > 100){
                is_all_valid = 0;

                // $("#deliver_qty_error").html("Deliver quantity should be between 3 to 100");
                $("#deliver_qty_error").html("Enter quantity between 3 to 100");
            }else if(deliver_qty % 1 != 0){
                is_all_valid = 0;

                $("#deliver_qty_error").html("Deliver quantity should be in integer");
            }



            var days_range_error = checkDeliveryQtyDays();
            if(days_range_error == 1){
                is_all_valid = 0;

                // $("#deliver_qty_error").html("This field is required");
            }



            // if(is_site_selected == '0'){

            //     $location = $('#state_name_city_filter').val().split('_');
            //     // $state_location = $('#state_name_city').val().split('_');

            //     var element = $("#state_name_city").find('option:selected');
            //     var myTag = element.attr("data-state-name");


            //     $lat = $location[1];
            //     $long = $location[2];
            //     // $state_name = $state_location[1];
            //     $city_name = $location[3];


            //     $('#product_lat').val($lat);
            //     $('#product_long').val($long);
            //     $('#location_state_name').val(myTag);
            //     $('#location_city_name').val($city_name);

            //     var state_id = $("#state_name_city").val();

            //     var city_id = $("#state_name_city_filter").val();

            //     $("#location_state_id").val(state_id);
            //     $("#location_city_id").val(city_id);
            // }
            var form_values = $("form[name='user_location_form']").serialize();

            // var form_values = new FormData(form);
            console.log(form_values);

            // replySupportTicket(form_values);
            if(is_all_valid == 1){
                $("form[name='user_location_form']")[0].submit();
            }
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

    $(document).on("click",".add1",function () {
        if ($(this).prev().val() < 100) {
            // console.log("product_id..."+$(this).attr("data-product-id"));
            var qty = +$(this).prev().val() + 1;

            $(this).prev().val(qty);

            // var token = $("#qty_csrf_token").val();
            // var token = $(this).attr("data-token-id");
            // var product_id = $(this).attr("data-product-id");
            // var product_id = $("#qty_product_id").val();

            // addToCart(token,product_id,qty,"cart_checkout");
        }
    });

    $(document).on("click",".sub1",function () {
        $min_order_qty = $('#product_page_minimum_order').val();
        console.log("min_order_qty..."+$min_order_qty);
        console.log("next..."+$(this).next().val());
        console.log("test..."+($(this).next().val() > $min_order_qty));
        if (parseInt($(this).next().val()) > parseInt($min_order_qty)) {
            // console.log("product_id..."+$(this).attr("data-product-id"));
            if ($(this).next().val() > 1) {

                var qty = +$(this).next().val() - 1;

                $(this).next().val(qty);

                // var token = $(this).attr("data-token-id");
                // var product_id = $(this).attr("data-product-id");
                // var product_id = $("#qty_product_id").val();

                // addToCart(token,product_id,qty,"cart_checkout");
            }
        }
    });

    $(document).on("keypress",".qty_input1",function (e) {
        //if the letter is not digit then display error and don't type anything
        is_pressed_alpha = 0;
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
           //display error message
           is_pressed_alpha = 1;
           var product_id = $(this).attr("data-product-id");
           $("#qty_digit_error_"+product_id).html("Digits Only").show().fadeOut("slow");
                  return false;
       }
      });



    //   $(document).on("keyup","#qty_input1",function (e) {
      $(document).on("keyup",".qty_input1",function (e) {
        // }

            // clearTimeout(add_tocart_timer);  //Refresh Timer on keyup


            // var token = $(this).attr("data-token-id");
            var product_id = $(this).attr("data-product-id");

            var qty = $(this).val();

            console.log("qty.."+qty);
            $min_order_qty = $('#product_page_minimum_order').val();
            if(qty != ''){
                if(parseInt(qty) >= $min_order_qty){
                    $("#product_detail_qty_error_"+product_id).html("");
                    // if(is_pressed_alpha == 0){
                    //     add_tocart_timer = setTimeout(addToCart, 1000,token,product_id,qty,"cart_checkout");
                    // }

                }else{
                    $("#product_detail_qty_error_"+product_id).html("Quantity should be greater than 3 Cu. Mtr");
                }

            }


        });





    /*qty script start*/
    $(document).on("click",".add",function () {
        if ($(this).prev().val() < 100) {
            console.log("product_id..."+$(this).attr("data-product-id"));
            console.log("q$(this).prev().val()ty..."+$(this).prev().val());
            var qty = +$(this).prev().val() + 1;
            console.log("qty..."+qty);
            $(this).prev().val(qty);

            // var token = $("#qty_csrf_token").val();
            var token = $(this).attr("data-token-id");
            var product_id = $(this).attr("data-product-id");
            var product_details = $(this).attr("data-product");
            var is_custom_mix = $(this).attr("data-is-custom-mix");
            // var product_id = $("#qty_product_id").val();

            if(is_custom_mix == 0){
                var design_mix_detail = $(this).attr("data-design-mix-detail");
                $("#design_mix_product_details").val(design_mix_detail);
            }

            addToCart(token,product_details,qty,"cart_checkout",is_custom_mix,product_id);
        }
    });

    $(document).on("click",".sub",function () {
        if ($(this).next().val() > 3) {
            console.log("product_id..."+$(this).attr("data-product-id"));
            if (parseInt($(this).next().val()) > 3) {

                var qty = +$(this).next().val() - 1;

                $(this).next().val(qty);

                var token = $(this).attr("data-token-id");
                var product_id = $(this).attr("data-product-id");
                var product_details = $(this).attr("data-product");
                var is_custom_mix = $(this).attr("data-is-custom-mix");
                // var product_id = $("#qty_product_id").val();

                if(is_custom_mix == 0){
                    var design_mix_detail = $(this).attr("data-design-mix-detail");
                    $("#design_mix_product_details").val(design_mix_detail);
                }

                addToCart(token,product_details,qty,"cart_checkout",is_custom_mix,product_id);
            }
        }
    });
    /*qty script end*/
    var add_tocart_timer = "";
    var is_pressed_alpha = 0;
    // $("#quantity").keypress(function (e) {
    $(document).on("keypress","#qty_input",function (e) {
        //if the letter is not digit then display error and don't type anything
        is_pressed_alpha = 0;
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
           //display error message
           is_pressed_alpha = 1;
           var product_id = $(this).attr("data-product-id");
           $("#qty_digit_error_"+product_id).html("Digits Only").show().fadeOut("slow");
                  return false;
       }
      });

    // $("#qty_input").keyup(function(){
    $(document).on("keyup","#qty_input",function (e) {

        // console.log("digits only...."+e.which);
        // if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //     console.log("digits only");
        //     is_pressed_alpha = 1;
        // }

            clearTimeout(add_tocart_timer);  //Refresh Timer on keyup

            // var token = $("#qty_csrf_token").val();
            // var product_id = $("#qty_product_id").val();

            var token = $(this).attr("data-token-id");
            var product_id = $(this).attr("data-product-id");
            var product_details = $(this).attr("data-product");
            var is_custom_mix = $(this).attr("data-is-custom-mix");

            var qty = $(this).val();

            if(qty != ''){
                if(parseInt(qty) >= 3){
                    if(is_pressed_alpha == 0){
                        add_tocart_timer = setTimeout(addToCart, 1000,token,product_details,qty,"cart_checkout",is_custom_mix,product_id);
                    }

                }
            }


    });


    $("#delivery_site_location_drop").change(function() {
        // alert( $('option:selected', this).val() );

        selectSiteLocation($('option:selected', this).attr("data"));
    });
    
    $("#location_dialog_grade_dropdown").change(function() {
        // alert( $('option:selected', this).val() );
        $("#location_popup_grade_error").html("");
        $type_of_rmc = $('input[type=radio][name=rmc_type]:checked').val();
        if($type_of_rmc != undefined){
            
            if($type_of_rmc == 'product_rmc'){

                $("#product_cat_id").val($('option:selected', this).val());
                

            }else if($type_of_rmc == 'product_custom_rmc'){

                $("#product_cat_id").val("custom_mix_dialog_on");

            }

        }
        $("#rmc_grade_id").val($('option:selected', this).val());
        
    });

    $('input[type=radio][name=rmc_type]').change(function() {
        $("#location_popup_type_rmc_error").html("");
        if (this.value == 'product_rmc') {
            $rmc_grade = $("#location_dialog_grade_dropdown").val();
            if($rmc_grade != undefined){
                $("#product_cat_id").val($rmc_grade);
                $("#type_of_rmc").val(this.value);
            }
            $("#open_page_type").val("product_listing");
        }
        else if (this.value == 'product_custom_rmc') {
            
            $("#product_cat_id").val("custom_mix_dialog_on");
            $("#type_of_rmc").val(this.value);

            $("#open_page_type").val("change_location");
            
        }

        $rmc_grade = $("#location_dialog_grade_dropdown").val();
        if($rmc_grade != undefined){
            $("#rmc_grade_id").val($rmc_grade);
        }
    });

});

function openLocationDialog(product_id,open_page_type,custom_rmc_save_id = ''){
    $("#custom_mix_save_id").val('');
    if(product_id != ''){
        $("#product_cat_id").val(product_id);
        if(product_id == 'custom_mix_dialog_on'){
            $("#product_custom_rmc").prop("checked", true);
            $("#open_page_type").val(open_page_type);
            $("#custom_mix_save_id").val(custom_rmc_save_id);
        }else{
            $("#product_rmc").prop("checked", true);
            $("#location_dialog_grade_dropdown").val(product_id);
            $("#open_page_type").val('product_listing');
            
        }
    }else{
        $rmc_type = $("#popup_selected_type_of_rmc").val();

        if($rmc_type == 'product_rmc'){
            $("#open_page_type").val('product_listing');
        }else {
            $("#open_page_type").val(open_page_type);
        }
        
    }    


    $("#nearest-vendors").modal('hide');
    $("#Select_location_guest").modal('show');
    
}

function submitLocation(){

    // var state_id = $("#state_name_city").val();

    // var city_id = $("#state_name_city_filter").val();

    // $("#location_state_id").val(state_id);
    // $("#location_city_id").val(city_id);

    // $("form[name='user_location_form']").submit();

    // $("form[name='user_location_form']").valid();

    $("#location_dialog_submit_button").click();
}

function selectSiteLocation(site_data){

    site_data = JSON.parse(site_data);

    console.log(site_data);
    site_id = site_data["_id"];
    // var is_selected = $("#is_site_selected_"+site_id).val();

    // $(".is_site_selected_class").val(0);
    // $(".site_select_class").html("Select")

    // if(is_selected == '0'){

        site_location = site_data["location"]["coordinates"]

        $("#product_lat").val(site_location[1]);
        $("#product_long").val(site_location[0]);
        $("#location_state_name").val(site_data["state_name"]);
        $("#location_city_name").val(site_data["city_name"]);
        console.log(site_data["state_id"]);
        console.log(site_data["city_id"]);
        $("#location_state_id").val(site_data["state_id"]);
        $("#location_city_id").val(site_data["city_id"]);

        $("#is_location_site_selected").val("1");
        $("#selected_site_address").val(site_id);


        checkSuppliersAvailability(site_location[1],site_location[0],site_data["state_id"]);

        // $("#is_site_selected_"+site_id).val("1");

        // $("#site_"+site_id).html("Unselect");

    // }else{

    //     $("#product_lat").val("");
    //     $("#product_long").val("");
    //     $("#location_state_name").val("");
    //     $("#location_city_name").val("");
    //     $("#location_state_id").val("");
    //     $("#location_city_id").val("");

    //     $("#is_location_site_selected").val("0");

    //     $("#is_site_selected_"+site_id).val("0");

    //     $("#selected_site_address").val("");

    //     $("#site_"+site_id).html("Select");

    // }



}

function productClick(id,data){

    $('#product_detail_'+id).val(data);

    $('#product_detail_form_'+id).submit();

}

function buynow(token,product_details,quantity,cart_place,is_custom_mix,product_id){

    $("#buy_now_button_clicked").val(1);

    if(product_id != ''){
        $("#clicked_product_id").val(product_id);
    }

    addToCart(token,product_details,quantity,cart_place,is_custom_mix);

}

function loginAddTocart(id){

    $("#login_open_from").val("add_to_cart");
    $("#add_to_cart_button_id").val(""+id);

}

function addToCart(token,product_details,quantity,cart_place,is_custom_mix,item_id){

    if($("#TM_checkbox").prop('checked') != undefined){
        if($("#TM_checkbox").prop('checked') == false){
            //do something

            var is_TM_detail_avail = $("#is_TM_detail_avail").val();

            if(is_TM_detail_avail == 0){

                showToast("Please add your TM details","Error");

                $("#Buyer_tm_detail").modal("show");

                return;

            }
        }
    }

    $min_order_qty = $('#product_page_minimum_order').val();

    var buy_now_button_clicked = $("#buy_now_button_clicked").val();

    if(buy_now_button_clicked == 1){

        if(is_custom_mix == 1){
            var clicked_product_id = $("#clicked_product_id").val();

            quantity = $(".class_"+clicked_product_id).val();
        }else if(is_custom_mix == 0){

            $qty_field = $('#qty_input1').val();
            if($qty_field != undefined){
                quantity = $qty_field;
            }else{

            }

        }

    }else{

        $qty_field = $('#qty_input1').val();
        if($qty_field != undefined){
            quantity = $qty_field;
        }else{

        }

    }


    if(cart_place == 'slide_cart'){
        with_TM = $("#TM_checkbox").prop('checked') == true ? 'yes' : 'no';
        with_CP = $("#CP_checkbox").prop('checked') == true ? 'yes' : 'no';
    }else{

        if($("#with_TM_"+item_id).val() != undefined){
            // with_TM = $("#with_TM_"+item_id).val() == '1' ? 'true' : 'true';
            with_TM = $("#with_TM_"+item_id).val() == '1' ? 'yes' : 'no';
        }

        if($("#with_CP_"+item_id).val() != undefined){
            with_CP = $("#with_CP_"+item_id).val() == '1' ? 'yes' : 'no';
        }

    }


    console.log("quantity..."+quantity);
    console.log("min_order_qty..."+$min_order_qty);
    console.log(""+quantity+" >= "+$min_order_qty+"..."+(quantity >= $min_order_qty));
    console.log(""+quantity+" <= "+$min_order_qty+"..."+($min_order_qty <= quantity));
    console.log("with_TM..."+with_TM);
    console.log("with_CP..."+with_CP);
    // if($min_order_qty >= quantity){
    if(parseInt(quantity) >= parseInt($min_order_qty)){

        var request_data = {
            "_token":token,
            "quantity":quantity,
            'cart_place':cart_place,
            'is_custom_mix':is_custom_mix,
            'with_TM':with_TM,
            'with_CP':with_CP,
        }

        if(is_custom_mix == 1){
            request_data["product_details"] = product_details;
        }else if(is_custom_mix == 0){
            request_data["design_mix_details"] = $("#design_mix_product_details").val();
            request_data["design_mix_id"] = product_details;
        }

        if($("#time_cart_start_delivery_date").val() != undefined){

            request_data["time_cart_delivery_date"] = $("#time_cart_start_delivery_date").val();
            request_data["time_cart_end_delivery_date"] = $("#time_cart_end_delivery_date").val();
            request_data["time_cart_design_mix_detail_id"] = $("#time_cart_design_mix_detail_id").val();


        }else{

            if(item_id != undefined){

                if(item_id != 0){
                    request_data["time_cart_delivery_date"] = $("#delivery_date_"+item_id).val();
                }

            }

        }

        if(item_id != undefined){

            if(item_id != 0){
                request_data["item_id"] = item_id;
            }

        }



        if(($("#is_time_popup_submit").val() != undefined) && ($("#is_time_popup_submit").val() != "")){

            if($("#is_time_popup_submit").val() == 0){

                var cart_TM_no = $("#cart_TM_no").val();
                var cart_driver_name = $("#cart_driver_name").val();
                var cart_driver_mobile = $("#cart_driver_mobile").val();
                var cart_TM_operator_name = $("#cart_TM_operator_name").val();
                var cart_TM_operator_mobile_no = $("#cart_TM_operator_mobile_no").val();


                request_data["TM_no"] = cart_TM_no;
                request_data["driver_name"] = cart_driver_name;
                request_data["driver_mobile"] = cart_driver_mobile;
                request_data["TM_operator_name"] = cart_TM_operator_name;
                request_data["TM_operator_mobile_no"] = cart_TM_operator_mobile_no;

                request_data["with_TM"] = 'false';
                request_data["with_CP"] = 'false';


            }


        }

        // responsehandler("buyer/product_add_to_cart",
        //                 "POST",
        //                 request_data,
        //                 '#Login-form',
        //                 'Product added successfully',
        //                 0,
        //                 'add_to_cart_error_form'
        //             );


        customResponseHandler(
            "buyer/product_add_to_cart", // Ajax URl
            'POST', // Method call
            request_data, // Request data
            function success(data){ // onSuccess
                console.log(data);


                // data = JSON.parse(data);
                if((data["status"] >= 200) && (data["status"] < 300)){
                    // var message = data["data"]["message"];
                    if(data["status_type"] == 'error'){

                        showToast("Please check suggestion","Error");
                        var html = '';
                        var suggestion = data["suggest_array"];

                        $.each(suggestion, function(key, value){

                            html += '<p>'+value+'</p>';

                        });
                        $("#qty_error_"+id).html(""+html);

                        $("#buy_now_button_clicked").val(0);

                    }else{


                        // $("#cart_item_count").html((parseInt(cart_item_count) - 1));

                        // $("#"+item_div_id).remove();

                        if(cart_place === "slide_cart" ){
                            showToast("Product added successfully","Success");

                            $("#cart_content").html(data["html"]);

                            var buy_now_button_clicked = $("#buy_now_button_clicked").val();

                            if(buy_now_button_clicked == 1){
                                window.location.href = BASE_URL+"/buyer/cart";
                            }

                            $("#buy_now_button_clicked").val(0);

                            cart_item_count = parseInt($("#cart_item_count").text());

                            if(cart_item_count > 0){
                                cart_item_count = cart_item_count + 1;

                                if(cart_item_count <= 0 ){
                                    cart_item_count = 0;
                                }

                                $("#cart_item_count").html(cart_item_count);
                            }

                            if(cart_item_count > 0){
                                $("#cart_item_count").show();
                            }else{
                                $("#cart_item_count").hide();
                            }


                            window.location.href = BASE_URL+"/buyer/cart";

                        }else{

                            showToast("Cart updated successfully","Success");

                            $("#choose_time_popup").modal("hide");
                            $("#Buyer_tm_detail").modal("hide");

                            $("#cart_checkout_row_div").html(data["html"]);
                            $("#cart_content").html(data["slide_html"]);


                        }



                    }





                }else{
                    // showToast(""+data["error"]["message"],"Error");
                    // $("#qty_error_"+id).html(""+data["error"]["message"]);
                    var login_open_from = $("#login_open_from").val();
                    if(login_open_from == "add_to_cart"){
                        showToast(""+data["error"]["message"],"error");
                        reload();
                    }else{
                        // alert("terewr");
                        if(is_custom_mix == 1){
                            // alert(item_id);
                            $("#product_detail_qty_error_"+item_id+"_custom_product_id").html(""+data["error"]["message"]);
                        }else{
                            $("#product_detail_qty_error").html(""+data["error"]["message"]);
                        }
                        
                    }
                    

                    $("#buy_now_button_clicked").val(0);

                }


            }

        );

    }

}


function cartChangeDate(token,product_details,quantity,cart_place,is_custom_mix,item_id,cart_design_mix_detail_id){

    var product_json = product_details;
    var product_details = JSON.parse(product_details);
    console.log(product_details);
    var TM_details = '<p class="mb-1"><b>Rate of Transit Mixer :</b> ₹ '+product_details["TM_price"].toFixed(2)+' Per Cu.Mtr / Km</p>';



    if(product_details["with_CP"] == "true"){

        TM_details += '<div id="CP_in_choose_date_popup">';
        TM_details += '<p class="mb-1"><b>Rate of Concrete Pump :</b> ₹ '+product_details["CP_price"].toFixed(2)+' Per Cu.Mtr</p>';
        TM_details += '</div>';
    }

    $("#time_popup_TM_details").html(TM_details);
    // $("#time_popup_TM_and_CP").html('TM '+(product_details["with_CP"] == true ? '& CP' : '')+' Available Date & Time');
    $("#time_popup_TM_and_CP").html('Choose your delivery date below');

    var todayDate = product_details["delivery_date"].slice(0, 10);
    var endDate = product_details["end_date"].slice(0, 10);

    // $('.TM_CP_change_date').datepicker().val(''+todayDate).trigger('change');

    // $('.TM_CP_change_date').attr("id","time_cart_delivery_date_"+item_id);
    // console.log("endDate::"+endDate);

    var today_current = new Date();

    today_current.setDate(today_current.getDate() + 2);
    var dd = String(today_current.getDate()).padStart(2, '0');
    var mm = String(today_current.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today_current.getFullYear();

    today_current = dd + '/' + mm + '/' + yyyy;

    var today = new Date(todayDate);
    var today_to_30 = new Date();
    var endDate = new Date(endDate);

    // today.setDate(today.getDate() + 2);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    final_send_start_date = yyyy + '/' + mm + '/' + dd;
    today_request = yyyy + '/' + mm + '/'+dd;
    // console.log("today..."+today);

    today_to_30.setDate(today_to_30.getDate() + 32);
    var dd_30 = String(today_to_30.getDate()).padStart(2, '0');
    var mm_30 = String(today_to_30.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy_30 = today_to_30.getFullYear();

    today_to_30 = dd_30 + '/' + mm_30 + '/' + yyyy_30;
    today_to_30_request =  yyyy_30 + '/' + mm_30 + '/' + dd_30;

    var dd_end_date = String(endDate.getDate()).padStart(2, '0');
    var mm_end_date = String(endDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy_end_date = endDate.getFullYear();

    // var end_date = today;
    // var end_date = endDate;
    var final_end_date = dd_end_date + '/' + mm_end_date + '/' + yyyy_end_date;
    var final_send_end_date = yyyy_end_date + '/' + mm_end_date + '/' + dd_end_date;
    console.log("final_end_date::"+final_end_date);
    $('input[name="modify_daterange"]').daterangepicker({
        startDate: today,
        endDate: final_end_date,
        minDate: today_current,
        maxDate: today_to_30,
        locale: {
            format: 'DD/MM/YYYY'
        },
        drops: 'up'

    });

    var total_days = (parseInt(quantity) / 3);
    total_days = Math.floor(total_days);
    $("#modify_delivery_date_error").html("");
    $("#modify_delivery_date_msg").html("<i class='fa fa-info-circle'></i>You have to choose delivery date range of within "+total_days+" days");



    $('input[name="modify_daterange"]').on('apply.daterangepicker', function(ev, picker) {

        // var start_date = picker.startDate.format('DD/MM/YYYY');
        var start_date = picker.startDate.format('YYYY/MM/DD');
        var end_date = picker.endDate.format('YYYY/MM/DD');

        final_send_start_date = start_date;
        final_send_end_date = end_date;

        // $("#selected_delivery_date").val(start_date);
        // $("#selected_end_delivery_date").val(end_date);
        $("#modify_delivery_date_error").html("");

        console.log(start_date);
        console.log(end_date);

        var diff = picker.endDate.diff(picker.startDate, 'days');

        // $("#selected_range_diff").val(diff);

        console.log(diff);

        var has_error = checkModifyDeliveryQtyDays(quantity, diff);


        if(has_error == 1){
            $("#modify_date_range_button").attr("disabled","disabled");
        }else{

            $('#time_cart_start_delivery_date').val(''+final_send_start_date);
            $('#time_cart_end_delivery_date').val(''+final_send_end_date);

            $("#modify_date_range_button").removeAttr("disabled");
        }


    });

    $('#time_cart_start_delivery_date').val(''+final_send_start_date);
    $('#time_cart_end_delivery_date').val(''+final_send_end_date);
    $('#time_product_details').val(''+product_json);
    $('#time_quantity').val(''+quantity);
    $('#time_cart_place').val(''+cart_place);
    $('#time_is_custom_mix').val(''+is_custom_mix);
    $('#time_item_id').val(''+item_id);
    $('#time_csrf_token').val(''+token);

    if(is_custom_mix == 1){
        $('#time_design_mix_id').val('');
    }else{
        $('#time_design_mix_id').val(''+product_details['design_mix_id']);
        $('#time_cart_design_mix_detail_id').val(''+cart_design_mix_detail_id);
    }

    $("#choose_time_popup").modal("show");

}

function checkModifyDeliveryQtyDays(deliver_qty, selected_range_diff){

    // let deliver_qty = $("#deliver_qty").val();
    // let selected_range_diff = $("#selected_range_diff").val();

    var is_error = 0;

    if(deliver_qty.length > 0){
        if(!isNaN(deliver_qty)){
            if((deliver_qty >= 3) && (deliver_qty <= 100)){


                var total_days = (parseInt(deliver_qty) / 3);
                total_days = Math.floor(total_days);

                if(selected_range_diff > total_days ){
                    $("#modify_delivery_date_msg").html("");
                    $("#modify_delivery_date_error").html("<i class='fa fa-info-circle'></i>You have to choose delivery date range of within "+total_days+" days");

                    is_error = 1;
                }else{
                    $("#modify_delivery_date_msg").html("<i class='fa fa-info-circle'></i>You have to choose delivery date range of within "+total_days+" days");
                    $("#modify_delivery_date_error").html("");
                }


            }
        }
    }

    return is_error;

}

function updateCartItemDate(){

    var product_json = $('#time_product_details').val();
    var quantity = $('#time_quantity').val();
    var cart_place = $('#time_cart_place').val();
    var is_custom_mix = $('#time_is_custom_mix').val();
    var time_csrf_token = $('#time_csrf_token').val();
    var product_id = $('#time_item_id').val();
    var design_mix_id = $('#time_design_mix_id').val();

    if(is_custom_mix == 0){
        product_json = design_mix_id;
    }

    $("#is_time_popup_submit").val("1");

    addToCart(time_csrf_token,product_json,quantity,"cart_checkout",is_custom_mix,product_id);

}

function addTMDetails(token,product_details,quantity,cart_place,is_custom_mix,item_id){

    var product_json = product_details;
    var product_details = JSON.parse(product_details);


    var todayDate = product_details["delivery_date"].slice(0, 10);

    $('.buyer_detail_change_date').datepicker().val(''+todayDate).trigger('change');

    $('.buyer_detail_change_date').attr("id","time_cart_delivery_date_"+item_id);

    $('#TM_detail_product_details').val(''+product_json);
    $('#TM_detail_quantity').val(''+quantity);
    $('#TM_detail_cart_place').val(''+cart_place);
    $('#TM_detail_is_custom_mix').val(''+is_custom_mix);
    $('#TM_detail_item_id').val(''+item_id);
    $('#TM_detail_csrf_token').val(''+token);

    if(is_custom_mix == 1){
        $('#TM_detail_design_mix_id').val('');
    }else{
        $('#TM_detail_design_mix_id').val(''+product_details['design_mix_id']);
    }

    $("#Buyer_tm_detail").modal("show");

}

function updateCartItemTMDetails(){

    $("#cart_buyer_tm_detail_error").html("");

    var product_json = $('#TM_detail_product_details').val();
    var quantity = $('#TM_detail_quantity').val();
    var cart_place = $('#TM_detail_cart_place').val();
    var is_custom_mix = $('#TM_detail_is_custom_mix').val();
    var time_csrf_token = $('#TM_detail_csrf_token').val();
    var product_id = $('#TM_detail_item_id').val();
    var design_mix_id = $('#TM_detail_design_mix_id').val();

    if(is_custom_mix == 0){
        product_json = design_mix_id;
    }

    $("#is_time_popup_submit").val("0");

    var cart_TM_no = $("#cart_TM_no").val();
    var cart_driver_name = $("#cart_driver_name").val();
    var cart_driver_mobile = $("#cart_driver_mobile").val();
    var cart_TM_operator_name = $("#cart_TM_operator_name").val();
    var cart_TM_operator_mobile_no = $("#cart_TM_operator_mobile_no").val();

    if((cart_TM_no != '') && (cart_driver_name != '') && (cart_driver_mobile != '') && (cart_TM_operator_name != '') && (cart_TM_operator_mobile_no != '') ){

        addToCart(time_csrf_token,product_json,quantity,"cart_checkout",is_custom_mix,product_id);

    }else{

        $("#cart_buyer_tm_detail_error").html("All fields are required");

    }



}

function addToWishList(token,id,quantity,sub_cat_id, added_from){

    var request_data = {
        "_token":token,
        "product_id":id,
        "quantity":quantity,
        "sub_category_id":sub_cat_id,
    }

    // responsehandler("buyer/product_add_to_wish_list",
    //                 "POST",
    //                 request_data,
    //                 '#Login-form',
    //                 'Product added to wish list',
    //                 0,
    //                 'add_to_cart_error_form'
    //             );


    customResponseHandler(
        "buyer/product_add_to_wish_list", // Ajax URl
        'POST', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data)
            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                showToast("Product added to wish list","Success");

                if(added_from == 'detail_page'){

                    $('#add_wishlidt_item_detail').hide();
                    $('#delete_wishlidt_item_detail').show();

                }else if(added_from == 'listing_page'){

                    $('#add_wishlidt_item_listing_'+id).hide();
                    $('#delete_wishlidt_item_listing_'+id).show();

                }

                wishlist_item_count = parseInt($("#wishlist_count").text());

                if(wishlist_item_count >= 0){
                    wishlist_item_count = wishlist_item_count + 1;

                    if(wishlist_item_count <= 0 ){
                        wishlist_item_count = 0;
                    }

                    $("#wishlist_count").html(wishlist_item_count);
                }

                console.log("wishlist_item_count..."+wishlist_item_count);

                if(wishlist_item_count > 0){
                    $("#wishlist_count").show();
                }else{
                    $("#wishlist_count").hide();
                }

            }
            else{
                showToast(""+data["error"]["message"],"Error");

            }


        }
    );

}

// Product MODULE OVER...........................................

// Cart MODULE Start...........................................

$(document).ready(function(){

    $("#site_addresses_select").change(function(){
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        console.log(valueSelected);

        if(valueSelected.length > 0){

            $('#site_select_error').html("");

            var data = JSON.parse(valueSelected);

            addSiteAddressToCart(data);
        }





    });

    $("#billing_addresses_select").change(function(){
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        console.log(valueSelected);

        if(valueSelected.length > 0){

            $('#billing_select_error').html("");

            var data = JSON.parse(valueSelected);

            addBillingAddressToCart(data);
        }
    });

    $("#change_site_yes").on("click", function(){

        var optionSelected = $("#site_addresses_select").val();;
        var valueSelected = optionSelected;

        console.log(valueSelected);

        if(valueSelected.length > 0){

            $('#site_select_error').html("");

            var data = JSON.parse(valueSelected);

            changeSiteAddressToCart(data);
        }

    });

    $("#change_site_no").on("click", function(){

        var selected_site = $("#cart_site_id").val();
        var selected_site_detail = $("#selected_site_detail_"+selected_site).val();
        console.log("selected_site::"+selected_site);
        $("#site_addresses_select").val(selected_site_detail);

    });

});

function selectCartSiteLocation(){

    var selected_site = $("#is_cart_site_selected").val();

    $("#site_addresses_select").val(selected_site);

    $("#cart-site-address-popup").modal("hide");

    selected_site = JSON.parse(selected_site);

    addSiteAddressToCart(selected_site);
}

function onclickCartSiteLocation(data){

    $(".cart_site_selecte_button").html("Select");

    $("#is_cart_site_selected").val(data);

    data = JSON.parse(data);

    $("#cart_site_button_"+data["_id"]).html("Unselect");

}

function addSiteAddressToCart(data){
    console.log(data);
    var site_id = data["_id"];
    var site_name = data["site_name"];
    var address_1 = data["address_line1"];
    var address_2 = data["address_line2"];
    var city_name = data["city_name"];
    var state_name = data["state_name"];
    var country_name = data["country_name"];
    var contact_person = data["person_name"];
    var contact_person_no = data["mobile_number"];
    var pincode = data["pincode"];
    var selected_state_id = data["state_id"];
    var site_lat = data["location"]["coordinates"][1];
    var site_long = data["location"]["coordinates"][0];

    var request_data = {
        "site_id":site_id,
        "state_name":state_name,
        "city_name":city_name,
        "site_lat":site_lat,
        "site_long":site_long,
        "state_id":selected_state_id
    }

    // console.log("first_cat_id.............."+first_cat_id);

    customResponseHandler(
        "buyer/add_site_to_cart", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data)
            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                showToast("Site selected successfully","Success");

                location.reload();

                $("#add-address-form").modal("hide");

                // $("#site_details h4").remove();
                // $("#site_details p").remove();
                $("#site_details").html("");

                $("#site_details").append("<b class='d-block'>"+site_name+"</b>");
                $("#site_details").append(""+address_1+", "+address_2+" <br> "+city_name+" "+pincode+", ("+country_name+")");
                $("#site_details").append('<span class="d-block">Site Contact Person: '+contact_person+'</span>');
                $("#site_details").append('<span class="d-block">Site Mobile No.: '+contact_person_no+'</span>');



            }else if((data["status"] == 301) && (data["redirect_to_listing"] == 'yes'))
            {
                // window.location.href = BASE_URL+"/product_listing/"+first_cat_id;
                $("#site_change_Modal").modal("show");
            }
            else{
                // showToast(""+data["error"]["message"],"Error");
                $("#site_addresses_select").val($("#site_addresses_select option:first").val());

                $('#site_select_error').html(data["error"]["message"]);


            }


        }
    );

}

function addBillingAddressToCart(data){
    console.log(data);
    var billing_address_id = data["_id"];    
    var company_name = data["company_name"];
    var address_1 = data["line1"];
    var address_2 = data["line2"] != undefined ? data["line2"] : '';
    var city_name = data["city_name"];
    var state_name = data["state_name"];
    var country_name = data["country_name"];
    var pincode = data["pincode"];
    var gst_number = data["gst_number"];
    var selected_state_id = data["state_id"];

    var request_data = {
        "billing_address_id":billing_address_id
    }

    // console.log("first_cat_id.............."+first_cat_id);

    customResponseHandler(
        "buyer/add_billing_to_cart", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data)
            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                showToast("Site selected successfully","Success");

                location.reload();

                // $("#add-address-form").modal("hide");

                $("#billing_details").html("");

                $("#billing_details").append("<b class='d-block'>"+company_name+"</b>");
                $("#billing_details").append(""+address_1+", "+address_2+" <br> "+city_name+" "+pincode+", ("+country_name+")");
                $("#billing_details").append('<span class="d-block">GST No.: '+gst_number+'</span>');



            }else if((data["status"] == 301) && (data["redirect_to_listing"] == 'yes'))
            {
                // window.location.href = BASE_URL+"/product_listing/"+first_cat_id;
                $("#site_change_Modal").modal("show");
            }
            else{
                // showToast(""+data["error"]["message"],"Error");
                $("#site_addresses_select").val($("#site_addresses_select option:first").val());

                $('#site_select_error').html(data["error"]["message"]);


            }


        }
    );

}

function changeSiteAddressToCart(data){

    var site_id = data["_id"];
    var site_name = data["site_name"];
    var address_1 = data["address_line1"];
    var address_2 = data["address_line2"];
    var city_name = data["city_name"];
    var state_name = data["state_name"];
    var country_name = data["country_name"];
    var pincode = data["pincode"];
    var selected_state_id = data["state_id"];
    var site_lat = data["location"]["coordinates"][1];
    var site_long = data["location"]["coordinates"][0];

    var request_data = {
        "site_id":site_id,
        "state_name":state_name,
        "city_name":city_name,
        "site_lat":site_lat,
        "site_long":site_long,
        "state_id":selected_state_id
    }

    // console.log("first_cat_id.............."+first_cat_id);

    customResponseHandler(
        "buyer/change_site_to_cart", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data)
            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                showToast("Site selected successfully","Success");

                // location.reload();

                $("#add-address-form").modal("hide");

                // $("#site_details h4").remove();
                // $("#site_details p").remove();
                $("#site_details").html("");

                $("#site_details").append("<b class='d-block'>"+site_name+"</b>");
                $("#site_details").append(""+address_1+", "+address_2+" <br> "+city_name+" "+pincode+", ("+country_name+")");

                window.location.href = BASE_URL+"/home";


            }else if((data["status"] == 301) && (data["redirect_to_listing"] == 'yes'))
            {
                // window.location.href = BASE_URL+"/product_listing/"+first_cat_id;
                $("#site_change_Modal").modal("show");
            }
            else{
                // showToast(""+data["error"]["message"],"Error");
                $("#site_addresses_select").val($("#site_addresses_select option:first").val());

                $('#site_select_error').html(data["error"]["message"]);


            }


        }
    );

}





function placeOrder(transaction_id){

    // $is_site_selected = $("#site_addresses_select"). children("option:selected"). val();

    // var is_TM_details_not_available = $("#is_TM_details_not_available").val() == undefined ? 0 : $("#is_TM_details_not_available").val();

    // if(is_TM_details_not_available == 0){

    //     if($is_site_selected.length > 0){

            var request_data = {
                "transaction_id": transaction_id
            };

            customResponseHandler(
                "buyer/place_order", // Ajax URl
                'GET', // Method call
                request_data, // Request data
                function success(data){ // onSuccess
                    console.log(data)
                    // data = JSON.parse(data);
                    if((data["status"] >= 200) && (data["status"] < 300)){
                        // var message = data["data"]["message"];
                        showToast("Order placed successfully","Success");

                        $("#success_order_id").html(''+data["data"]["display_id"]);

                        $("#payment_success_popup").hide();
                        $("#payment_fail_popup").hide();
                        $("#order_success_popup").show();



                    }else{
                        // showToast(""+data["error"]["message"],"Error");
                        $("#site_addresses_select").val($("#site_addresses_select option:first").val());

                        $('#site_select_error').html(data["error"]["message"]);


                    }


                }
            );
    //     }else{
    //         $('#site_select_error').html("Please select delivery address");
    //     }
    // }else{
    //     $('#site_select_error').html("One of your item do not have the TM details. Please add to proceed");
    // }

}


function deleteCartItem(csrf,product_id, item_div_id, cart_item_count,cart_place,design_mix_detail_id = ''){

    var request_data = {
        "_token": csrf,
        "product_id":product_id,
        'cart_place':cart_place
    }

    if(design_mix_detail_id != ''){
        request_data['cart_design_mix_detail_id'] = design_mix_detail_id;
    }


    customResponseHandler(
        "buyer/delete_cart_item", // Ajax URl
        'POST', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);



            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                showToast("Cart item deleted successfully","Success");

                // $("#cart_item_count").html((parseInt(cart_item_count) - 1));

                // $("#"+item_div_id).remove();

                if(cart_place === "slide_cart" ){
                    $("#cart_content").html(data["html"]);

                }else{
                    $("#cart_checkout_row_div").html(data["html"]);
                    $("#cart_content").html(data["slide_html"]);
                }


                cart_item_count = parseInt($("#cart_item_count").text());

                if(cart_item_count > 0){
                    cart_item_count = cart_item_count - 1;

                    if(cart_item_count <= 0 ){
                        cart_item_count = 0;
                    }

                    $("#cart_item_count").html(cart_item_count);
                }

                if(cart_item_count > 0){
                    $("#cart_item_count").show();
                }else{
                    $("#cart_item_count").hide();
                    reload();
                }


            }else{
                showToast(""+data["error"]["message"],"Error");



            }


        }
    );


}

// Cart MODULE OVER...........................................

// Wish list MODULE start...........................................



function deleteWishListItem(csrf,wish_list_id, item_div_id, wishlist_item_count,added_from){

    var request_data = {
        "_token": csrf,
        "wish_list_id":wish_list_id
    }


    customResponseHandler(
        "buyer/delete_wishlist_item", // Ajax URl
        'POST', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data)
            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                // showToast("Item successfully removed from wishlist","Success");

                wishlist_item_count = parseInt($("#wishlist_count").text());

                if(wishlist_item_count > 0){
                    wishlist_item_count = wishlist_item_count - 1;

                    if(wishlist_item_count <= 0 ){
                        wishlist_item_count = 0;
                    }

                    $("#wishlist_count").html(wishlist_item_count);
                }

                if(wishlist_item_count > 0){
                    $("#wishlist_count").show();
                }else{
                    $("#wishlist_count").hide();
                }


                // $("#"+item_div_id).remove();

                // reload();

                if(added_from == 'detail_page'){

                    $("#delete_wishlidt_item_detail").hide();
                    $("#add_wishlidt_item_detail").show();

                }else if(added_from == 'listing_page'){

                    $('#add_wishlidt_item_listing_'+wish_list_id).show();
                    $('#delete_wishlidt_item_listing_'+wish_list_id).hide();

                }else{
                    reload();
                }



            }else{
                showToast(""+data["error"]["message"],"Error");


            }


        }
    );


}

// Wish list MODULE OVER...........................................

// Order MODULE Start...........................................

$(document).ready(function(){

    $(document).ready(async function(){
        $('.order_assign_qty_change_date').datepicker({
            autoclose: true,
            startDate: '1d',
            format: 'd M yyyy'
            // format: 'yyyy-mm-dd'
        });

        // $('.order_assign_qty_change_date').datepicker("setDate",  );

        // $('.order_assign_qty_change_date').datepicker().val('{{ $selected_delivery_date }}').trigger('change')

        $(".order_assign_qty_change_date").on('change', function(event) {
            event.preventDefault(); 
            // console.log("order_assign_qty_change_date call....");
            $("#assigned_quantity").removeAttr('disabled');
            // alert(this.value);
            // $("#selected_delivery_date").val(this.value);
            // $("#delivery_date_error").html("");
            /* Act on the event */
        });
    });

    $("#invoice_options").change(function() {
        var selected_tag = $('option:selected', this);
        var selected_option = $('option:selected', this).val();

        if(selected_option === 'cancel_order'){
            // printDiv("order_details_div");
            // console.log("attr.."+selected_tag.attr('data-csrf'));
            var csrf = selected_tag.attr('data-csrf');
            var order_id = selected_tag.attr('data-order-id');

            // cancelOrder(csrf,order_id);
            cancelOrderConfirmPopup(csrf,order_id);

        }else if(selected_option === 'pdf'){
            // printPDF("order_details_div");
            printDiv("order_details_div");
        }
    });



    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='order_complain_form']").validate({

        rules:{

            complaint_text: {
                required: true,
                noSpace:true
            },
            complaint_type: {
                required: true
            }

        },
        messages:{

        },errorPlacement: function(error, element) {
            if (element.attr("name") == "complaint_type" ) {
              error.insertAfter("#complaint_type_div");
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function(form, event) {
            // event.preventDefault();
            console.log("submited");

            var form_values = $("form[name='order_complain_form']").serialize();

            // var form_values = new FormData(form);
            console.log(form_values);

            // replySupportTicket(form_values);
            ordercomplaint(form_values);
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });
    
    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='feedback-form']").validate({

        rules:{

            feedback_description: {
                required: true,
                noSpace:true
            },
            feedback_type: {
                required: true
            }

        },
        messages:{

        },errorPlacement: function(error, element) {
            if (element.attr("name") == "complaint_type" ) {
              error.insertAfter("#complaint_type_div");
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function(form, event) {
            // event.preventDefault();
            console.log("submited");

            var form_values = $("form[name='feedback-form']").serialize();

            // var form_values = new FormData(form);
            console.log(form_values);

            // replySupportTicket(form_values);
            orderFeedback(form_values);
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });


    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='order_assign_qty']").validate({

        rules:{

            delivery_date: {
                required: true
            },
            assigned_quantity: {
                required: true,
                number:true,
                noDecimal:true,
                greaterThanEqual:3,
                lessThanEqual:function(){
                    // console.log("remain qty........."+$("#order_remain_part_qty").val());
                    return $("#order_remain_part_qty").val();
                }
            },
            delivery_start_time: {
                required: true
            }

        },
        messages:{
            assigned_quantity:{
                // lessThanEqual: "The value must be less than pending to be assign Qty "+$("#order_remain_part_qty").val()
                lessThanEqual: function(){
                    return "The value must be less than or equal to the pending quantity to be assigned Qty i.e. "+$("#order_remain_part_qty").val()+" Cu.Mtr";
                }
            }

        },errorPlacement: function(error, element) {
            if (element.attr("name") == "complaint_type" ) {
              error.insertAfter("#complaint_type_div");
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function(form, event) {
            // event.preventDefault();
            console.log("submited");

            var order_assign_part_qty = $("#order_assign_part_qty").val()
            var assigned_quantity = $("#assigned_quantity").val()

            var is_error = 0;

            if(parseInt(assigned_quantity) > parseInt(order_assign_part_qty)){

                $("#assigned_quantity-error").html('The value must be less than assigned Qty '+order_assign_part_qty);

                is_error = 1;
            }

            if(is_error == 0){

                var form_values = $("form[name='order_assign_qty']").serialize();

                // var form_values = new FormData(form);
                console.log(form_values);

                // replySupportTicket(form_values);
                assignOrderQty(form_values);

            }


        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

    $("#assigned_quantity").keyup(function(){
        var assigned_quantity = $("#assigned_quantity").val();
        var order_item_part_qty = $("#order_assign_part_qty").val();
        var order_remain_part_qty = $("#order_remain_part_qty").val();

        $('#delivery_start_time').get(0).selectedIndex = 0;
        $("#delivery_end_time").html('<option disabled="disabled" selected="selected">End Time</option>');

        $("#order_assign_qty_success").html("");
        $('#order_assign_qty_error').html("");
        $('#order_short_close_error').html("");

        if(assigned_quantity.length > 0){

            if(!isNaN(assigned_quantity)){

                if((assigned_quantity >= 3) && (assigned_quantity <= 100)){
                    $("#delivery_start_time").removeAttr("disabled");

                    console.log("order_item_part_qty.."+order_item_part_qty);
                    console.log("assigned_quantity.."+assigned_quantity);
                    console.log("order_remain_part_qty.."+order_remain_part_qty);

                    var remain_qty = parseInt(order_item_part_qty) - parseInt(assigned_quantity);
                    
                    console.log("remain_qty.."+remain_qty);
                    if((remain_qty > 0) && (remain_qty <= 2)){
                        $("#order_short_close_error").html("Since the balance order Qty now is less than 3 Cu.Mtr, your order will be closed. You may assign "+order_item_part_qty+" Cu.Mtr to avoid short close of your order.");
                    }

                    
                }else{
                    $("#delivery_start_time").attr("disabled","true");
                }

            }else{
                $("#delivery_start_time").attr("disabled","true");
            }

        }else{
            $("#delivery_start_time").attr("disabled","true");
        }
    });

    $("#delivery_start_time").change(function() {

        $("#order_assign_qty_success").html("");
        $('#order_assign_qty_error').html("");

        var start_time = $('option:selected', this).val();

        if(start_time.length > 0){
            // $("#delivery_end_time").removeAttr("disabled");

            var assigned_quantity = $("#assigned_quantity").val();
            assigned_quantity_hours = Math.ceil(parseInt(assigned_quantity) / 5);

            assigned_quantity_hours = assigned_quantity_hours * 3;

            var delivery_date = $("#order_assign_qty_change_date").val();

            var end_time = $('option:selected', this).val();

            var delivery_date = new Date(delivery_date);

            var dd = String(delivery_date.getDate()).padStart(2, '0');
            var mm = String(delivery_date.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = delivery_date.getFullYear();

            delivery_date = yyyy+"-"+mm+"-"+dd;

            var start_date_time = dd + '-' + mm + '-' + yyyy+" "+start_time;

            var end_date_time = new Date(delivery_date+"T"+start_time+":00+05:30");
            // var end_date_time = end_date_time.toLocaleString();
            // console.log("end_date_time......."+end_date_time.getDate());

            // end_date_time.setDate(end_date_time.getDate() + 2);
            end_date_time.setHours(end_date_time.getHours() + parseInt(assigned_quantity_hours));

            var dd = String(end_date_time.getDate()).padStart(2, '0');
            var mm = String(end_date_time.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = end_date_time.getFullYear();
            var hours = end_date_time.getHours();
            var min = end_date_time.getMinutes();

            end_date_time = dd + '-' + mm + '-' + yyyy+" "+hours+":00";

            console.log("end_date_time......."+end_date_time);
            console.log("start_date_time......."+start_date_time);
            console.log("delivery_date......."+delivery_date);


            if((delivery_date.length > 0) && (start_date_time.length > 0) && (end_date_time.length > 0)){

                $("#order_start_date_time").val(start_date_time);
                $("#order_end_date_time").val(end_date_time);

                var vendor_id = $("#order_detail_vendor_id").val();

                // checkTMAvailability(delivery_date,start_time,end_time,vendor_id);
                checkTMAvailability(delivery_date,start_date_time,end_date_time,vendor_id);

            }

            // var selected_time_key = (time_key_array[start_time] + 1);
            // var assigned_quantity_count = assigned_quantity + selected_time_key;
            // var html = '<option disabled="disabled" selected="selected">End Time</option>';
            // for(var key=selected_time_key;key<assigned_quantity_count;key++){

            //     if(time_array[key] != undefined){
            //         html += '<option value="'+time_array[key]+'">'+time_array[key]+'</option>';
            //     }

            // };

            // $("#delivery_end_time").html(html);
        }


    });

    $("#delivery_end_time").change(function() {

        var delivery_date = $("#order_assign_qty_change_date").val();
        var start_time = $("#delivery_start_time").val();
        var end_time = $('option:selected', this).val();

        if((delivery_date.length > 0) && (start_time.length > 0) && (end_time.length > 0)){

            var vendor_id = $("#order_detail_vendor_id").val();

            checkTMAvailability(delivery_date,start_time,end_time,vendor_id);

        }


    });



});

var is_open_1_month = 0;
var assign_delivery_date = "";
var assign_end_date = "";


function openAssignQtyPopup(order_id, order_item_id, vendor_id, order_itme_qty,order_item_part_qty,delivery_date,end_date){
    console.log(delivery_date +".............."+ end_date);
    $("#order_assign_qty_order_id").val(order_id);
    $("#order_assign_qty_order_item_id").val(order_item_id);
    $("#order_assign_part_qty").val(order_item_part_qty);
    $("#order_remain_part_qty").val(order_item_part_qty);

    $("#order_detail_vendor_id").val(vendor_id);
    $("#order_detail_item_qty").val(order_itme_qty);

    $("#order_assign_qty_success").html("");
    $('#order_assign_qty_error').html("");
    $('#order_short_close_error').html("");

    var today_date = new Date();
    var dd = String(today_date.getDate()).padStart(2, '0');
    var mm = String(today_date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today_date.getFullYear();

    // delivery_date = "2021-08-20";
    var parts = delivery_date.split('-');
    var delivery_date = new Date(parts[2], parts[1] - 1, parts[0]);
    today_date = yyyy + '-' + mm + '-' + dd;
    console.log(today_date);
    today_date = new Date(today_date);
    var d1_delivery_date = new Date(delivery_date);

    // var dd_delivery_date = String(d1_delivery_date.getDate()).padStart(2, '0');
    // var mm_delivery_date = String(d1_delivery_date.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var yyyy_delivery_date = d1_delivery_date.getFullYear();

    // d1_delivery_date = yyyy_delivery_date + '-' + mm_delivery_date + '-' + dd_delivery_date;
    // console.log("d1_delivery_date...."+d1_delivery_date);
    // d1_delivery_date = new Date(d1_delivery_date);
    var d2_end_date = new Date(''+end_date);
    console.log(d1_delivery_date.getTime() +"..."+ today_date.getTime());
    if(today_date.getTime() ==  d1_delivery_date.getTime()){

        d1_delivery_date.setDate(today_date.getDate() + 1);
        d1_delivery_date.setMonth(today_date.getMonth());
        var dd = String(d1_delivery_date.getDate()).padStart(2, '0');
        var mm = String(d1_delivery_date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = d1_delivery_date.getFullYear();
        
        delivery_date = dd + '-' + mm + '-' + yyyy;

    }
    // console.log("Inside logic...."+today_date.getTime()+"...."+d1_delivery_date.getTime()+"....."+d2_end_date.getTime());
    if((today_date.getTime() >  d1_delivery_date.getTime()) && (today_date.getTime() <  d2_end_date.getTime())){
        // console.log("Inside logic...."+today_date.getMonth());
        d1_delivery_date.setDate(today_date.getDate() + 1);
        d1_delivery_date.setMonth(today_date.getMonth());
        var dd = String(d1_delivery_date.getDate()).padStart(2, '0');
        var mm = String(d1_delivery_date.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = d1_delivery_date.getFullYear();

        delivery_date = dd + '-' + mm + '-' + yyyy;
        // console.log("Inside logic...."+delivery_date);
    }

    assign_delivery_date = delivery_date;
    assign_end_date = end_date;

    // var same = d1.getTime() === d2.getTime();
    // console.log("same::"+same);
    // console.log(d1.getTime() === d2.getTime());
    // console.log(d1.getTime() +".............."+ d2.getTime());
    // console.log(delivery_date +".............."+ end_date);

    // $('#order_assign_qty_change_date').datepicker('setStartDate', delivery_date).trigger('change');
    // $('#order_assign_qty_change_date').datepicker('setEndDate', end_date).trigger('change');
    
    $('#order_assign_qty_change_date').datepicker('setStartDate', delivery_date);
    $('#order_assign_qty_change_date').datepicker('setEndDate', end_date);

    $("#order_assign_qty").modal("show");

}

function assignOrderQty(request_data){


    responsehandler("buyer/order_assign_qty",
                    "POST",
                    request_data,
                    '#order_assign_qty',
                    'Order Qty assigned successfully',
                    0,
                    'order_assign_qty'
                );


}

function checkTMAvailability(delivery_date,start_time,end_time,vendor_id){

    var request_data = {
        "delivery_date" : delivery_date,
        "start_time" : start_time,
        "end_time" : end_time,
        "vendor_id" : vendor_id,

    }

    // responsehandler("buyer/order_assign_qty",
    //                 "POST",
    //                 request_data,
    //                 '#order_assign_qty',
    //                 'Order QTY assigned successfully',
    //                 0,
    //                 'order_assign_qty'
    //             );

    $("#order_assign_qty_success").html("");
    $('#order_assign_qty_error').html("");

    customResponseHandler("buyer/order_check_tm_assign",
        "GET",
        request_data,
        function response(data){
            console.log(data);

            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["html"];
                // showToast(""+message,"Success");
                // $("#sign-up-form-1").modal('hide');

                // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                $("#order_assign_qty_success").html("Yes, Supplier is able to deliver in chosen date & time. Go ahead!");

                if(is_open_1_month == 1){

                    $('#order_assign_qty_change_date').datepicker('setStartDate', assign_delivery_date).trigger('change');
                    $('#order_assign_qty_change_date').datepicker('setEndDate', assign_end_date).trigger('change');

                }

                $("#order_assign_qty_button").removeAttr("disabled");

            }else{
                // showToast(""+data["error"]["message"],"Error");
                $('#order_assign_qty_error').html("Supplier not able to deliver in selected time slot.Please choose another time");

                var today = new Date();
                    // var today_to_30 = new Date();

                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();

                today = yyyy + '-' + mm + '-' + dd;

                today = new Date(today);

                var del_date = new Date(delivery_date);

                console.log("delivery_date::"+(today.getTime() == del_date.getTime()));
                console.log("delivery_date::"+delivery_date);
                console.log("today::"+today);

                if((today.getTime() == del_date.getTime())){

                    // del_date.setDate(del_date.getDate() + 30);
                    var dd = String(del_date.getDate()).padStart(2, '0');
                    var mm = String(del_date.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = del_date.getFullYear();

                    del_date = dd + '-' + mm + '-' + yyyy;

                    $('#order_assign_qty_change_date').datepicker('setStartDate', del_date).trigger('change');

                    today.setDate(today.getDate() + 30);
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = today.getFullYear();

                    today = dd + '-' + mm + '-' + yyyy;
                    // today = yyyy + '-' + mm + '-' + dd;
                    console.log("today::"+today);
                    $('#order_assign_qty_change_date').datepicker('setEndDate', today).trigger('change');

                    is_open_1_month = 1;

                }

                $("#order_assign_qty_button").attr("disabled","true");
            }

        }
    );



}

var time_array = Array();

time_array[0] = "12:00";
time_array[1] = "00:00";
time_array[2] = "01:00";
time_array[3] = "02:00";
time_array[4] = "03:00";
time_array[5] = "04:00";
time_array[6] = "05:00";
time_array[7] = "06:00";
time_array[8] = "07:00";
time_array[9] = "08:00";
time_array[10] = "09:00";
time_array[11] = "10:00";
time_array[12] = "11:00";
time_array[13] = "12:00";
time_array[14] = "13:00";
time_array[15] = "14:00";
time_array[16] = "15:00";
time_array[17] = "16:00";
time_array[18] = "17:00";
time_array[19] = "18:00";
time_array[20] = "19:00";
time_array[21] = "20:00";
time_array[22] = "21:00";
time_array[23] = "22:00";
time_array[24] = "23:00";

console.log("time_array::"+time_array[10]);

var time_key_array = Array();

time_key_array["12:00"] = 0;
time_key_array["00:00"] = 1;
time_key_array["01:00"] = 2;
time_key_array["02:00"] = 3;
time_key_array["03:00"] = 4;
time_key_array["04:00"] = 5;
time_key_array["05:00"] = 6;
time_key_array["06:00"] = 7;
time_key_array["07:00"] = 8;
time_key_array["08:00"] = 9;
time_key_array["09:00"] = 10;
time_key_array["10:00"] = 11;
time_key_array["11:00"] = 12;
time_key_array["12:00"] = 13;
time_key_array["13:00"] = 14;
time_key_array["14:00"] = 15;
time_key_array["15:00"] = 16;
time_key_array["16:00"] = 17;
time_key_array["17:00"] = 18;
time_key_array["18:00"] = 19;
time_key_array["19:00"] = 20;
time_key_array["20:00"] = 21;
time_key_array["21:00"] = 22;
time_key_array["22:00"] = 23;
time_key_array["23:00"] = 24;

console.log("time_key_array::"+time_key_array["09:00"]);

function cancelOrderConfirmPopup(token,order_id){

    var message = "Are you sure you want to cancel this #"+order_id+" order?";

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        cancelOrder(token, order_id);

    });
}

function cancelOrder(token, order_id){

    var request_data = {
        "_token":token,
        "order_id":order_id
    }

    responsehandler("buyer/order_cancel",
                    "POST",
                    request_data,
                    '#Login-form',
                    'Order cancelled successfully',
                    0,
                    'add_to_cart_error_form'
                );


}

function ordercomplaint(request_data){


    responsehandler("buyer/order_complain",
                    "POST",
                    request_data,
                    '#complain-popup',
                    'Complain submitted successfully',
                    0,
                    'order_complain_form'
                );


}
function orderFeedback(request_data){


    responsehandler("buyer/feedback",
                    "POST",
                    request_data,
                    '#feedback-popup',
                    'Feedback submitted successfully',
                    0,
                    'feedback-form'
                );


}

function itemTrack(item_id){

    var form_values = {};

    customResponseHandler("buyer/order_track/"+item_id,
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);

                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#multi_truck_details_div").html(message);

                            $("#multi-truck-popup").modal('show');
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );


}


function CPTrack(item_part_id){

    var form_values = {};

    customResponseHandler("buyer/order_CP_track_detail/"+item_part_id,
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);

                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#order_CP_trackking_div").html(message);

                            $("#order-CP-track").modal('show');
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );


}


function productReview(request_data){


    responsehandler("buyer/order_product_review",
                    "POST",
                    request_data,
                    '#feedback-popup',
                    'Thank You for your review \n Your review sent for approval',
                    0,
                    'order_product_review_form'
                );


}


function showReviewDialog(data, vendor_id, order_item_id, form_type){

    console.log("form_type:"+form_type);

    if(form_type == "edit"){

        resetEditForm('order_product_review_form');

        data = JSON.parse(data);

        $("#review_form_type").val("edit");
        $("#review_vendor_id").val(vendor_id);
        $("#review_comment").val(data["review_text"]);
        $("#review_rating_number").val(data["rating"]);
        $("#review_id").val(data["_id"]);
        $("#order_item_id").val(order_item_id);

        $('.my-rating').starRating('setRating', data["rating"]);

    }else if(form_type == "add"){

        resetForm('order_product_review_form','review_popup');

        $("#review_form_type").val("add");

        $("#review_form_type").val("add");
        $("#review_vendor_id").val(vendor_id);
        $("#review_comment").val("");
        $("#review_rating_number").val(0);
        $("#review_id").val("");
        $("#order_item_id").val(order_item_id);

        $('.my-rating').starRating('setRating', 0);

    }

    $("#review_popup").modal('show');

}

function closeReviewDialog(){

    $("#review_popup").modal('hide');

}



$(document).ready(function(){
    $.validator.setDefaults({ ignore: ":hidden:not(input)" })
    $("form[name='order_product_review_form']").validate({

        rules:{

            rating: {
                required: true
            },
            product_id: {
                required: true
            },
            review_text: {
                required: true,
                noSpace:true
            }

        },
        messages:{
            rating: {
                required: "Rating is required"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "rating" ) {
              error.insertAfter(".star-rating");
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function(form, event) {
            // event.preventDefault();
            console.log("submited");

            var form_values = $("form[name='order_product_review_form']").serialize();

            // var form_values = new FormData(form);
            console.log(form_values);

            // replySupportTicket(form_values);
            productReview(form_values);
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

});








function printDiv(div_id)
{

  var divToPrint=document.getElementById(''+div_id);

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<link rel="stylesheet" type="text/css" href="http://www.conmate.com/assets/buyer/css/style.css"><html><body onload="window.print()">@page {size: landscape;}'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);



}

function printPDF(div_id){

    var doc = new jsPDF();
    var elementHandler = {
    '#ignorePDF': function (element, renderer) {
        return true;
    }
    };
    // var source = window.document.getElementsByTagName("body")[0];
    var source = document.getElementById(''+div_id);
    // doc.fromHTML(
    //     source,
    //     15,
    //     15,
    //     {
    //         'width': 180,
    //         'elementHandlers': elementHandler
    //     }
    // );

    doc.addHTML($('#'+div_id).html(), function() {
        pdf.save('a4.pdf');
    });

    // doc.output("dataurlnewwindow");

    // var doc = new jsPDF();

    // doc.text('Hello world!', 10, 10);
    // doc.save('a4.pdf');

}

// Order MODULE OVER...........................................

// Get curretnt location MODULE Start...........................................

var x = document.getElementById("demo");

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function showPosition(position) {
  x.innerHTML = "Latitude: " + position.coords.latitude +
  "<br>Longitude: " + position.coords.longitude;
}


// Get curretnt location MODULE OVER...........................................


var current_seelected_sub_cat_id = 'all_ajax';
var current_selected_source_id = 'no_source';
var current_selected_sub_Cat_id_div = 'subcat_id_all';
var current_short_by_rating_low_to_high = "no_short_rating_low_high";
var current_short_by_rating_high_to_low = "no_short_rating_high_low";
var current_short_by_stock_low_to_high = "no_short_stock_low_high";
var current_short_by_stock_high_to_low = "no_short_stock_high_low";

function getProductListing(product_id, product_sub_cat_id, sub_Cat_id_div){


    if(product_sub_cat_id != 'all_ajax_source'){
        current_selected_product_id = product_id;
        current_seelected_sub_cat_id = product_sub_cat_id;
        current_selected_sub_Cat_id_div = sub_Cat_id_div;
    }

    // if(short_by_rating_low_high != 'no_short_rating_low_high'){
    //     current_short_by_rating_low_to_high = short_by_rating_low_high;
    // }

    // if(short_by_rating_high_low != 'no_short_rating_high_low'){
    //     current_short_by_rating_high_to_low = short_by_rating_high_low;
    // }

    // if(short_by_stock_low_high != 'no_short_stock_low_high'){
    //     current_short_by_stock_low_to_high = short_by_stock_low_high;
    // }

    // if(short_by_stock_high_low != 'no_short_stock_high_low'){
    //     current_short_by_stock_high_to_low = short_by_stock_high_low;
    // }

    source_id = $('#source_drop_down_list').val();
    source_name = $('#source_drop_down_list').find(":selected").text();

    console.log("source_id..."+source_id);

    if(current_selected_source_id == ''){
        $("#selected_source_name").html('');
    }
    $("#selected_source_name").html('');

    if(source_id != null && source_id != ''){
        current_selected_source_id = source_id;
        $("#selected_source_name").append('<span class="m-r10">'+source_name+' <i onclick="proSourceFilterRemove()" class="fa fa-close"></i></span>');
    }

    if(current_short_by_rating_low_to_high != 'no_short_rating_low_high'){
        $("#selected_source_name").append('<span class="m-r10">Rating low to high <i onclick="removeSorting()" class="fa fa-close"></i></span>');
    }

    if(current_short_by_rating_high_to_low != 'no_short_rating_high_low'){
        $("#selected_source_name").append('<span class="m-r10">Rating high to low <i onclick="removeSorting()" class="fa fa-close"></i></span>');
    }

    if(current_short_by_stock_low_to_high != 'no_short_stock_low_high'){
        $("#selected_source_name").append('<span class="m-r10">Stock low to high <i onclick="removeSorting()" class="fa fa-close"></i></span>');
    }

    if(current_short_by_stock_high_to_low != 'no_short_stock_high_low'){
        $("#selected_source_name").append('<span class="m-r10">Stock high to low <i onclick="removeSorting()" class="fa fa-close"></i></span>');
    }

    if($('.filterslideblock').hasClass('show')){
        $('.filterslide-close').click();
    }



    $("#product_listing_div").html('');
    $("#product_loading_div").show();

    $(".tab-switcher").removeClass("active");
    $("#"+current_selected_sub_Cat_id_div).addClass("active");

    var form_values = {
        "short_by_rating_low_high": current_short_by_rating_low_to_high,
        "short_by_rating_high_low": current_short_by_rating_high_to_low,
        "short_by_stock_low_high": current_short_by_stock_low_to_high,
        "short_by_stock_high_low": current_short_by_stock_high_to_low,
    };

    customResponseHandler("product_listing_ajax/"+current_selected_product_id+"/"+current_seelected_sub_cat_id+"/yes/"+current_selected_source_id,
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);


                        if((data["status"] >= 200) && (data["status"] < 300)){
                            // var message = data["data"]["message"];
                            if(data["status_type"] == 'error'){

                                $("#product_listing_div").html('<p class="alert alert-secondary">No data found!</p>');

                                // showToast("No Data Found","Error");
                                // var html = '';
                                // var suggestion = data["suggest_array"];

                                // $.each(suggestion, function(key, value){

                                //     html += '<p>'+value+'</p>';

                                // });
                                // $("#qty_error_"+id).html(""+html);

                            }else{
                                // showToast("Product added successfully","Success");

                                // $("#cart_item_count").html((parseInt(cart_item_count) - 1));

                                // $("#"+item_div_id).remove();

                                $("#product_loading_div").hide();

                                $("#product_listing_div").html(data["html"]);

                            }



                        }else{
                            // showToast(""+data["error"]["message"],"Error");
                            // $("#qty_error_"+id).html(""+data["error"]["message"]);
                            $("#product_listing_div").html('<p class="alert alert-secondary">No data found!</p>');
                        }
                    }
                );
}

function proSourceFilterRemove(){

    console.log("testting...");

    current_selected_source_id = 'no_source';

    $("#source_drop_down_list").prop("selectedIndex", 0);

    $("#selected_source_name").html('');

    getProductListing('','all_ajax_source','subcat_id_all');

}


function shorting(short_by_rating_low_high,short_by_rating_high_low, short_by_stock_low_high, short_by_stock_high_low){

    current_short_by_rating_low_to_high = short_by_rating_low_high;
    current_short_by_rating_high_to_low = short_by_rating_high_low;
    current_short_by_stock_low_to_high = short_by_stock_low_high;
    current_short_by_stock_high_to_low = short_by_stock_high_low;


    getProductListing('','all_ajax_source','subcat_id_all');

}


function removeSorting(){

    current_short_by_rating_low_to_high = "no_short_rating_low_high";
    current_short_by_rating_high_to_low = "no_short_rating_high_low";
    current_short_by_stock_low_to_high = "no_short_stock_low_high";
    current_short_by_stock_high_to_low = "no_short_stock_high_low";

    getProductListing('','all_ajax_source','subcat_id_all');

}


function cancel_order(order_id, item_id, gateway_transaction_id){

    var request_data = {
        "order_id":order_id,
        "item_id":item_id,
        "gateway_transaction_id":gateway_transaction_id
    }

    responsehandler("buyer/order_item_cancel",
                    "GET",
                    request_data,
                    '',
                    'Order item is cancelled successfully',
                    1,
                    ''
                );


}

// Conmix......................................
$(document).ready(function(){

	$( "#show_singup_m" ).click(function() {
		setTimeout(function(){
            resetForm('signup-emailverify-form','sign-up-form-1')
			$('#sign-up-form-1').modal('show');

		}, 500);
	});
	$( "#show_login_m" ).click(function() {
		setTimeout(function(){
            resetForm('signup-emailverify-form','sign-up-form-1');
			$('#Login-form').modal('show');
		}, 500);
	});
	$( "#show_popup_otp" ).click(function() {
		setTimeout(function(){
            resetForm('signup-emailverify-otp-form','sign-up-form-2');
			$('#Signup_popup_otp').modal('show');
		}, 500);
	});
	$( "#show_reg_form" ).click(function() {
		setTimeout(function(){
			$('#Registration_form').modal('show');
		}, 500);
	});




});


// Custom Design Mix..............................start


$(document).ready(function(){
    $.validator.setDefaults({ ignore: ":hidden:not(input)" })
    $("form[name='custom_mix_form']").validate({

        rules:{

            concrete_grade_id: {
                required: true
            },
            "cement_brand_id[]": {
                required: true
            },
            "cement_grade_id[]": {
                required: true
            },
            cement_quantity: {
                required:true,
                number:true,
                lessThanEqual:999,
                greaterThanEqual:1
            },
            "sand_source_id[]": {
                required: true
            },
            "sand_zone_id[]": {
                required: true
            },
            sand_quantity: {
                required:true,
                number:true,
                lessThanEqual:999,
                greaterThanEqual:1
            },
            "aggregate_source_id[]": {
                required: true
            },
            "aggregate1_sub_cat_id[]": {
                required: true
            },
            "aggregate2_sub_cat_id[]": {
                required: true
            },
            aggregate1_quantity: {
                required:true,
                number:true,
                lessThanEqual:999,
                greaterThanEqual:1
            },
            aggregate2_quantity: {
                required:true,
                number:true,
                lessThanEqual:999,
                greaterThanEqual:1
            },
            "admix_brand_id[]": {
                required: true
            },
            "admix_code_id[]": {
                required: function(){

                    if($("#ad_mixture_brand_id").val().length > 0){
                        return true;
                    }

                    return false;

                }
            },
            "admix_brand_id_2[]": {
                required: false
            },
            "admix_code_id_2[]": {
                required: function(){

                    if($("#ad_mixture_brand_2_id").val().length > 0){
                        return true;
                    }

                    return false;

                }
            },
            "water_type[]": {
                required: function(){

                
                    return true;
                

                }
            },
            admix_quantity_per: {
                required:function(){

                    if($("#ad_mixture_brand_id").val().length > 0){
                        return true;
                    }

                    return false;

                },
                number:true
            },
            site_id: {
                required:function(){

                    if($("#is_custom_mix_save").val() == 0){
                        return false;
                    }

                    return true;

                }
            },
            water_quantity: {
                required:true,
                number:true
            },
            with_TM: {
                required:true
            },
            with_CP: {
                required:true
            }

        },
        messages:{
            rating: {
                required: "Rating is required"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "rating" ) {
              error.insertAfter(".star-rating");
            } else if (element.attr("name") == "concrete_grade_id" ) {
            //   error.insertAfter("#concrete_grade_id_div");
              $("#concrete_grade_id_div").append(error);
            }else if (element.attr("name") == "cement_brand_id[]" ) {
            //   error.insertAfter("#mix_cement_brnad_div");
                $("#mix_cement_brnad_div").append(error);
            }else if (element.attr("name") == "cement_grade_id[]" ) {
            //   error.insertAfter("#cement_grade_id_div");
                $("#cement_grade_id_div").append(error);
            }else if (element.attr("name") == "sand_source_id[]" ) {
            //   error.insertAfter("#mix_sand_source_div");
                $("#mix_sand_source_div").append(error);
            }else if (element.attr("name") == "aggregate_source_id[]" ) {
            //   error.insertAfter("#aggregate_source_id_div");
                $("#aggregate_source_id_div").append(error);
            }else if (element.attr("name") == "aggregate1_sub_cat_id[]" ) {
            //   error.insertAfter("#aggregate1_sub_cat_id_div");
                $("#aggregate1_sub_cat_id_div").append(error);
            }else if (element.attr("name") == "aggregate2_sub_cat_id[]" ) {
            //   error.insertAfter("#aggregate2_sub_cat_id_div");
                $("#aggregate2_sub_cat_id_div").append(error);
            }else if (element.attr("name") == "admix_brand_id[]" ) {
            //   error.insertAfter("#ad_mixture_brand_id_div");
                $("#ad_mixture_brand_id_div").append(error);
            }else if (element.attr("name") == "admix_code_id" ) {
            //   error.insertAfter("#admix_code_id_div");
                $("#admix_code_id_div").append(error);
            }else if (element.attr("name") == "admix_brand_id_2[]" ) {
            //   error.insertAfter("#ad_mixture_brand_id_div");
                $("#ad_mixture_brand_2_id_div").append(error);
            }else if (element.attr("name") == "admix_code_id_2" ) {
            //   error.insertAfter("#admix_code_id_div");
                $("#admix_code_id_2_div").append(error);
            }else if (element.attr("name") == "fly_ash_source_id[]" ) {
            //   error.insertAfter("#fly_ash_source_id_div");
                $("#fly_ash_source_id_div").append(error);
            }else if (element.attr("name") == "water_type[]" ) {
            //   error.insertAfter("#fly_ash_source_id_div");
                $("#water_type_div").append(error);
            } else {
              error.insertAfter(element);
            }

            // element.scrollIntoView({ behavior: 'smooth' });
            element[0].scrollIntoView({ behavior: 'smooth' });
        },
        submitHandler: function(form, event) {
            // event.preventDefault();
            console.log("submited");

            var form_values = $("form[name='custom_mix_form']").serialize();

            var is_concrete_density_error = $("#is_concrete_density_error").val();
            var is_admixture_percent_error = $("#is_admixture_percent_error").val();
            var is_WC_ratio_error = $("#is_WC_ratio_error").val();

            var is_error = 0;

            // if(is_concrete_density_error == 1){
            //     $("#concrete_density_error_span").html("Concrete Density must be between 2400 - 2550 KG");

            //     is_error = 1;

            //     return;
            // }

            // if(is_admixture_percent_error == 1){
            //     $("#concrete_density_error_span").html("Admixture quantity should be between 1 - 4 KG");

            //     is_error = 1;

            //     return;
            // }

            // if(is_WC_ratio_error == 1){
            //     $("#concrete_density_error_span").html("Water / Cement ratio must be between 0.35% - 0.55%");

            //     is_error = 1;

            //     return;
            // }

            // // var form_values = new FormData(form);
            // console.log(form_values);

            // // replySupportTicket(form_values);
            if(is_error == 0){
                customDesignMix(form_values);
            }

            // $("form[name='custom_mix_form']")[0].submit();
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

    $("#custom_mix_with_TM").change(function() {
        // alert( $('option:selected', this).text() );
        if($('option:selected', this).val() == 'no'){
            $("#custome_mix_TM_no_warning").show();
        }else{
            $("#custome_mix_TM_no_warning").hide();
        }
    });

    $.validator.setDefaults({ ignore: ":hidden:not(input)" })
    $("form[name='TM_CP_choose_date_form']").validate({

        rules:{

            delivery_date: {
                required: true
            }

        },
        messages:{

        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "rating" ) {
              error.insertAfter(".star-rating");
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function(form, event) {
            // event.preventDefault();
            console.log("submited");

            var form_values = $("form[name='TM_CP_choose_date_form']").serialize();

            // var form_values = new FormData(form);
            console.log(form_values);

            // replySupportTicket(form_values);
            var time_is_design_mix = $("#time_is_design_mix").val();

            if(time_is_design_mix == '1'){ //For Design mix
                TM_CP_designmix_DateUpdate(form_values);
            }else{ // For Custom Mix
                TM_CP_DateUpdate(form_values);
            }

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });


    $.validator.setDefaults({ ignore: ":hidden:not(input)" })
    $("form[name='buyer_TM_detail']").validate({

        rules:{

            TM_no: {
                required: true
            },
            driver_name: {
                required: true
            },
            driver_mobile: {
                required: true,
                number:true,
                minlength:10,
                maxlength:10
            },
            // TM_operator_name: {
            //     required: true
            // },
            // TM_operator_mobile_no: {
            //     required: true,
            //     number:true,
            //     minlength:10,
            //     maxlength:10
            // },
            delivery_date: {
                required: true
            },

        },
        messages:{

        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "rating" ) {
              error.insertAfter(".star-rating");
            } else {
              error.insertAfter(element);
            }
        },
        submitHandler: function(form, event) {
            // event.preventDefault();
            console.log("submited");

            var form_values = $("form[name='buyer_TM_detail']").serialize();

            // var form_values = new FormData(form);
            console.log(form_values);

            var buyer_tm_is_design_mix = $("#buyer_tm_is_design_mix").val();

            if(buyer_tm_is_design_mix == 1){
                TM_buyer_designmix_detail(form_values);
            }else{
                TM_buyer_detail(form_values);
            }

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });


    $('#ad_mixture_brand_id').on('change', function() {

        if($('option:selected', this).val().length > 0){
            // alert( $('option:selected', this).val() );
                    
            getAdmixtureTypes($('option:selected', this).val(),'ad_mixture_category_id');
        }else{
            var html = '<option disabled="disabled" selected="selected">Select Admixture Code</option>';
            
    
            $("#ad_mixture_category_id").html(html);
        }
        

      });
      
      $('#ad_mixture_brand_2_id').on('change', function() {

        if($('option:selected', this).val().length > 0){
            // alert( $('option:selected', this).val() );
                    
            getAdmixtureTypes($('option:selected', this).val(), 'ad_mixture_category_2_id');
        }else{
            var html = '<option disabled="disabled" selected="selected">Select Admixture Code 2</option>';
            
    
            $("#ad_mixture_category_2_id").html(html);
        }
        

      });

      var choose_time_popup = 0;
      var Buyer_tm_detail = 0;

      $('#TM_checkbox').change(function() {
        if(this.checked) {
            console.log("test");
            $("#TM_checkbox").prop("checked", false);

            // $("#CP_checkbox").prop("checked", true);

            // is_CP_check = "yes";

            var CP_checkbox_price = $("#CP_checkbox_price").val();

            $("CP_in_choose_date_popup").html('<p class="mb-1"><b>With Concrete Pump :</b> ₹ '+CP_checkbox_price+' Per day</p>');

            $("#choose_time_popup").modal("show");
            // choose_time_popup = 1;
            // Buyer_tm_detail = 0;
        }else{

            console.log("test2");

            $("#TM_checkbox").prop("checked", true);

            // $("#CP_checkbox").prop("checked", true);

            $("CP_in_choose_date_popup").html("");

            // $("#Buyer_tm_detail").modal("show");
            $("#TM_confirmModal").modal("show");

            // choose_time_popup = 0;
            Buyer_tm_detail = 1;
        }

    });
    var is_CP_check = "";
    var is_TM_check = "";
    $('#CP_checkbox').change(function() {
        var url = window.location.origin + window.location.pathname;
        console.log("testing..."+this.checked+"....."+url);
        if(this.checked) {

            $("#with_TM_or_CP_check").html("WIth TM & CP");

            // is_CP_check = "no";
            is_CP_check = "yes";

            if($("#TM_checkbox").is(":checked") == false){
                is_TM_check = "yes";
            }

            $("#TM_checkbox").prop("checked", true);

            

            var CP_checkbox_price = $("#CP_checkbox_price").val();

            $("CP_in_choose_date_popup").html('<p class="mb-1"><b>With Concrete Pump :</b> ₹ '+CP_checkbox_price+' Per day</p>');

            window.location.href = url + '?with_CP='+is_CP_check;
            // $("#choose_time_popup").modal("show");

            // choose_time_popup = 1;
            // Buyer_tm_detail = 0;
        }else{

            $("#with_TM_or_CP_check").html("WIth TM");

            // is_CP_check = "yes";
            is_CP_check = "no";

            // $("#CP_checkbox").prop("checked", true);
            
            $("CP_in_choose_date_popup").html("");
            window.location.href = url + '?with_CP='+is_CP_check;
        }

    });

    $("#Buyer_tm_detail_close").on("click", function(){

        $("#TM_checkbox").prop("checked", $("#TM_checkbox").is(":checked"));
        $("#CP_checkbox").prop("checked", $("#CP_checkbox").is(":checked"));

    });

    $("#choose_time_popup_close").on("click", function(){

        $("#TM_checkbox").prop("checked", $("#TM_checkbox").is(":checked"));

        if(is_CP_check == 'yes'){
            $("#CP_checkbox").prop("checked", true);



        }else{
            $("#CP_checkbox").prop("checked", false);

            if(is_TM_check == 'yes'){
                $("#TM_checkbox").prop("checked", false);
            }
        }




    });

    $("#confirmTMOk").on("click",function(){

        if(Buyer_tm_detail == 1){
            $("#Buyer_tm_detail").modal("show");
        }

    });


    $("#mix_sub_cat_1").change(function() {

        var selected_cat = $('option:selected', this).val();
        var selected_cat_text = $('option:selected', this).text();

        $("#aggreagte_cat_1").html(selected_cat_text+" (kg)");
        getSecondAggregateSubCat(selected_cat);



    });

    $("#mix_sub_cat_2").change(function() {

        var selected_cat = $('option:selected', this).val();
        var selected_cat_text = $('option:selected', this).text();

        $("#aggreagte_cat_2").html(selected_cat_text+" (kg)");



    });


    var myDropdown = document.getElementById('dropdownMenuLink1');
    if(myDropdown != undefined){
        myDropdown.addEventListener('show.bs.dropdown', function () {
        // do something...
        notificationClick();
        });
    }

    $("#custom_mix_delivery_site_location_drop").change(function() {
        // alert( $('option:selected', this).val() );

        var site_data = $('option:selected', this).attr("data");

        site_data = JSON.parse(site_data);

        console.log(site_data);
        site_id = site_data["_id"];
        // var is_selected = $("#is_site_selected_"+site_id).val();

        // $(".is_site_selected_class").val(0);
        // $(".site_select_class").html("Select")

        // if(is_selected == '0'){

            site_location = site_data["location"]["coordinates"]

            $("#custom_mix_selected_lat").val(site_location[1]);
            $("#custom_mix_selected_long").val(site_location[0]);
            $("#custom_mix_selected_state_id").val(site_data["state_id"]);
            // $("#location_city_id").val(site_data["city_id"]);

            // $("#is_location_site_selected").val("1");
            // $("#selected_site_address").val(site_id);
    });


});

function getSecondAggregateSubCat(sub_cat_id){


    var request_data = {
        "sub_cat_id":sub_cat_id
    }

    customResponseHandler(
        "/buyer/get_agregate_sub_cat_2/"+sub_cat_id, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);


            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];

                // showToast("Quantity updated","Success");
                var html = '<option disabled="disabled" selected="selected">Select Aggregate category 2</option>';
                $.each(data["data"],function(key,value){

                    // console.log(value);

                    html += '<option value="'+value["_id"]+'">'+value["sub_category_name"]+'</option>'

                });

                $("#mix_sub_cat_2").html(html);

             }else{
                // showToast(""+data["error"]["message"],"Error");
                showToast("Something went wrong. Please try again","Error");

            }


        }

    );




}

function getAdmixtureTypes(admix_brand_id,ad_mixture_category_dropdown_id){


    var request_data = {
        "admix_brand_id":admix_brand_id
    }

    customResponseHandler(
        "buyer/get_admixture_types", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            // console.log(data);


            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];

                // showToast("Quantity updated","Success");
                var html = '<option value="" selected="selected">Select Admixture Code</option>';
                $.each(data["data"],function(key,value){

                    // console.log(value);

                    html += '<option value="'+value["_id"]+'">'+value["category_name"]+' - '+value["admixture_type"]+'</option>'

                });

                // $("#ad_mixture_category_id").html(html);
                $("#"+ad_mixture_category_dropdown_id).html(html);

             }else{
                // showToast(""+data["error"]["message"],"Error");
                showToast("Something went wrong. Please try again","Error");

            }


        }

    );




}

function openCustomMixDialog(open_from){


    $("#is_custom_mix_open_from_vendor_detail").val(open_from);
    $("#is_custom_mix_save").val(0);
    $("#is_custom_mix_add_edit").val(0);
    $("#is_custom_mix_open_from_save").val(0);
    $("#custom_design_mix_button").html('Submit');
    $("#custom_mix_delivery_site_location_drop_div").css('display','none');
    $('#custom_mix_edit_id').val(0);

    resetForm("custom_mix_form","cust_design_pop");

    $("#cust_design_pop").modal("show");
    $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());

    setTimeout(function() {
        // alert("okk..."+$('#popup_selected_rmc_grade_id').val());
        // $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());
        if($('#popup_selected_rmc_grade_id').val() != undefined && $('#popup_selected_rmc_grade_id').val() != ''){
            $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());
        }
    }, 200);
    
}

function openCustomMixDialogToSave(open_from,is_add_edit){   


    $("#is_custom_mix_open_from_vendor_detail").val(open_from);
    $("#is_custom_mix_save").val(1);
    $("#is_custom_mix_add_edit").val(is_add_edit);
    $("#is_custom_mix_open_from_save").val(0);
    $("#custom_design_mix_button").html('Save');
    $("#custom_mix_delivery_site_location_drop_div").css('display','block');
    $('#custom_mix_edit_id').val(0);

    resetForm("custom_mix_form","cust_design_pop");

    $("#cust_design_pop").modal("show");
    $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());

    setTimeout(function() {
        // alert("okk..."+$('#popup_selected_rmc_grade_id').val());
        // $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());
        if($('#popup_selected_rmc_grade_id').val() != undefined && $('#popup_selected_rmc_grade_id').val() != ''){
            $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());
        }
    }, 200);
    
}
function openCustomMixDialogToSaveEdit(open_from,is_add_edit,rmc_data){


    $("#is_custom_mix_open_from_vendor_detail").val(open_from);
    if(is_add_edit == 3){
        $("#is_custom_mix_save").val(0);
        $("#is_custom_mix_add_edit").val(0);
        $("#is_custom_mix_open_from_save").val(1);
        
        $("#custom_design_mix_button").html('Submit');
    }else{
        $("#is_custom_mix_save").val(1);
        $("#is_custom_mix_add_edit").val(is_add_edit);
        $("#is_custom_mix_open_from_save").val(0);
        $("#custom_design_mix_button").html('Save');
    }
    
    
    $("#custom_mix_delivery_site_location_drop_div").css('display','block');

    resetForm("custom_mix_form","cust_design_pop");
    

    $("#cust_design_pop").modal("show");
    // $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());
    data = JSON.parse(rmc_data);
    setTimeout(function() {
        // alert("okk..."+$('#popup_selected_rmc_grade_id').val());
        // $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());
        // if($('#popup_selected_rmc_grade_id').val() != undefined && $('#popup_selected_rmc_grade_id').val() != ''){
        //     $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());
        // }
        
        console.log(data);
        fillCustomRMCForm(data);

    }, 200);
    
}

function fillCustomRMCForm(data){

    $('#custom_mix_delivery_site_location_drop').val(data['site_id']);
    $("#custom_mix_delivery_site_location_drop").trigger('change');
        $('#concrete_grade_id').val(data['concrete_grade']['_id']);
        
        var cement_brand_array = [];
        cement_brand_array.push(data['cement_brand1']['_id']);
        if(data['cement_brand2'] != null){
            if(data['cement_brand2']['_id'] != null){
                cement_brand_array.push(data['cement_brand2']['_id']);
            }
        }
        $('#mix_cement_brnad').val(cement_brand_array);
        $('#mix_cement_brnad').multiselect('refresh');

        $('#cement_grade_id').val(data['cement_grade']['_id']);
        $('#custom_cement_qty').val(data['cement_quantity']);

        var mix_sand_source_array = [];
        mix_sand_source_array.push(data['sand_source1']['_id']);
        if(data['sand_source2'] != null){
            if(data['sand_source2']['_id'] != null){
                mix_sand_source_array.push(data['sand_source2']['_id']);
            }
        }
        $('#mix_sand_source').val(mix_sand_source_array);
        $('#mix_sand_source').multiselect('refresh');

        $('#mix_sand_zone').val(data['sand_zone']['_id']);
        $('#custom_sand_qty').val(data['sand_quantity']);
        
        var mix_aggregate_source_array = [];
        mix_aggregate_source_array.push(data['aggregate_source1']['_id']);
        if(data['aggregate_source2'] != null){
            if(data['aggregate_source2']['_id'] != null){
                mix_aggregate_source_array.push(data['aggregate_source2']['_id']);
            }
        }
        $('#mix_aggregate_source').val(mix_aggregate_source_array);
        $('#mix_aggregate_source').multiselect('refresh');

        $('#mix_sub_cat_1').val(data['aggregate1_sub_category']['_id']);

        var subcat_2_html = '<option value="'+data['aggregate2_sub_category']['_id']+'" selected>'+data['aggregate2_sub_category']['sub_category_name']+'</option>';

        // $('#mix_sub_cat_2').val(data['aggregate2_sub_category']['_id']);
        $('#mix_sub_cat_2').append(subcat_2_html);
        
        $('#custom_aggregate1_qty').val(data['aggregate1_quantity']);
        $('#custom_aggregate2_qty').val(data['aggregate2_quantity']);

        $('#ad_mixture_brand_id').val(data['admix_brand1']['_id']);

        var ad_mixture_category_html = '<option value="'+data['admix_category1']['_id']+'" selected>'+data['admix_category1']['category_name']+' - '+data['admix_category1']['admixture_type']+'</option>';

        // $('#ad_mixture_category_id').val(data['admix_category1']['_id']);
        $('#ad_mixture_category_id').append(ad_mixture_category_html);
        
        if(data['admix_brand2'] != null){
            if(data['admix_brand2']['_id'] != null){
                $('#ad_mixture_brand_2_id').val(data['admix_brand2']['_id']);
                
                var ad_mixture_category_2_html = '<option value="'+data['admix_category2']['_id']+'" selected>'+data['admix_category2']['category_name']+' - '+data['admix_category2']['admixture_type']+'</option>';

                $('#ad_mixture_category_2_id').append(ad_mixture_category_2_html);
            }
        }

        $('#custom_admixture_qty_per').val(data['admix_quantity_per']);
        $('#custom_admixture_qty').val(data['admix_quantity']);
        
        $('#mix_fly_ash_brand').val(data['fly_ash_source']['_id']);

        $('#custom_flyash_qty').val(data['fly_ash_quantity']);
        $('#custom_water_qty').val(data['water_quantity']);
        
        $('#custom_mix_with_TM').val((data['with_TM'] == '1' ? 'yes' : ''));
        $('#custom_mix_with_CP').val((data['with_CP'] == '1' ? 'yes' : 'no'));

        $('#custom_mix_edit_id').val(data['_id']);

        concreteDensity();

}

function customDesignMix(request_data){


    // responsehandler("custom_mix",
    //                 "POST",
    //                 request_data,
    //                 '#cust_design_pop',
    //                 '',
    //                 0,
    //                 'custom_mix_form'
    //             );

    console.log("request_data");

    request_data = request_data + '&is_ajax=1'

    console.log(request_data);
    customResponseHandler("buyer/custom_mix",
        "POST",
        request_data,
        function response(data){
            console.log(data);


            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];

                if($("#is_custom_mix_save").val() == 0){

                    var is_custom_mix_open_from_vendor_detail = $("#is_custom_mix_open_from_vendor_detail").val();

                    if(is_custom_mix_open_from_vendor_detail == 0){
                        $("form[name='custom_mix_form']")[0].submit();
                    }else{
                        var detail_vendor_id = $("#detail_vendor_id").val();
                        var detail_address_id = $("#detail_address_id").val();

                        window.location.href= BASE_URL+"/mix_detail/"+detail_vendor_id+"/"+detail_address_id+"";
                    }
                }else {
                    if($("#is_custom_mix_add_edit").val() == 1){
                        showToast("Custom RMC Added successfully","Success");
                        reload();
                    }else if($("#is_custom_mix_add_edit").val() == 2){
                        showToast("Custom RMC Added successfully","Success");
                        reload();
                    }   
                }




            }else{
                // showToast(""+data["error"]["message"],"Error");
                // $("#qty_error_"+id).html(""+data["error"]["message"]);
                // $("#product_listing_div").html('<p class="alert alert-secondary">No data found!</p>');

                showErrorMsgInForm("custom_mix_form",data["error"]["message"]);
                
            }
        }
    );


}

function customRMCDeleteConfirmPopup(custom_rmc_id){
    console.log("custom_rmc_id...."+custom_rmc_id);
    var message = "Are you sure you want to delete this Custom RMC?";

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        deleteCustomRMC(custom_rmc_id)

    });
}

function deleteCustomRMC(custom_rmc_id){
    var form_values = {
        "custom_rmc_id":custom_rmc_id
    };

    responsehandler("buyer/delete_custom_rmc",
                    "GET",
                    form_values,
                    '#Login-form',
                    'Custom RMC deleted successfully'
                );

}


function TM_CP_DateUpdate(request_data){

    var detail_vendor_id = $("#detail_vendor_id").val();
    var detail_address_id = $("#detail_address_id").val();

    // var TM_checkbox = $("#TM_checkbox").val();
    // var CP_checkbox = $("#CP_checkbox").val();

    // var TM_checkbox = $("#TM_checkbox").is(":checked") ? 'yes' : 'no';
    var TM_checkbox = 'yes';
    var CP_checkbox = $("#CP_checkbox").is(":checked") ? 'yes' : 'no';

    request_data = request_data + "&with_TM="+TM_checkbox+"&with_CP="+CP_checkbox;

    customResponseHandler(
        "mix_detail/"+detail_vendor_id+"/"+detail_address_id+"", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);



            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                $("#choose_time_popup").modal("hide");
                // showToast("Quantity updated","Success");
                // console.log(BASE_URL+"mix_detail/"+detail_vendor_id+"/"+detail_address_id+"");
                window.location.href= BASE_URL+"/mix_detail/"+detail_vendor_id+"/"+detail_address_id+"";

             }else{
                // showToast(""+data["error"]["message"],"Error");
                showToast("Something went wrong. Please try again","Error");

            }


        }

    );


}


function TM_CP_designmix_DateUpdate(request_data){

    var detail_vendor_id = $("#detail_vendor_id").val();
    var detail_product_id = $("#detail_product_id").val();
    var detail_address_id = $("#detail_address_id").val();

    // var TM_checkbox = $("#TM_checkbox").val();
    // var CP_checkbox = $("#CP_checkbox").val();

    // var TM_checkbox = $("#TM_checkbox").is(":checked") ? 'yes' : 'no';
    var TM_checkbox = 'yes';
    var CP_checkbox = $("#CP_checkbox").is(":checked") ? 'yes' : 'no';

    request_data = request_data + "&with_TM="+TM_checkbox+"&with_CP="+CP_checkbox;

    customResponseHandler(
        "product_detail/"+detail_product_id, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);



            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                $("#choose_time_popup").modal("hide");
                // showToast("Quantity updated","Success");
                // console.log(BASE_URL+"mix_detail/"+detail_product_id);
                window.location.href= BASE_URL+"/product_detail/"+detail_product_id;

             }else{
                // showToast(""+data["error"]["message"],"Error");
                showToast("Something went wrong. Please try again","Error");

            }


        }

    );


}

function TM_buyer_detail(request_data){

    var detail_vendor_id = $("#detail_vendor_id").val();
    var detail_address_id = $("#detail_address_id").val();

    var TM_checkbox = 'no';
    var CP_checkbox = 'no';

    request_data = request_data + "&with_TM="+TM_checkbox+"&with_CP="+CP_checkbox;
console.log(request_data);
    customResponseHandler(
        "mix_detail/"+detail_vendor_id+"/"+detail_address_id+"", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);


            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                $("#Buyer_tm_detail").modal("hide");
                // showToast("Quantity updated","Success");
                // console.log(BASE_URL+"mix_detail/"+detail_vendor_id+"/"+detail_address_id+"");
                window.location.href= BASE_URL+"/mix_detail/"+detail_vendor_id+"/"+detail_address_id+"";

             }else{
                // showToast(""+data["error"]["message"],"Error");
                showToast("Something went wrong. Please try again","Error");

            }


        }

    );


}


function TM_buyer_designmix_detail(request_data){

    var detail_vendor_id = $("#detail_vendor_id").val();
    var detail_product_id = $("#detail_product_id").val();
    var detail_address_id = $("#detail_address_id").val();

    var TM_checkbox = 'no';
    var CP_checkbox = 'no';

    request_data = request_data + "&with_TM="+TM_checkbox+"&with_CP="+CP_checkbox;
console.log(request_data);
    customResponseHandler(
        "product_detail/"+detail_product_id, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);


            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                $("#Buyer_tm_detail").modal("hide");
                // showToast("Quantity updated","Success");
                // console.log(BASE_URL+"mix_detail/"+detail_vendor_id+"/"+detail_address_id+"");
                window.location.href= BASE_URL+"/product_detail/"+detail_product_id;

             }else{
                // showToast(""+data["error"]["message"],"Error");
                showToast("Something went wrong. Please try again","Error");

            }


        }

    );


}


function applyOfferCode(code, applied_from){

    var applied_code = "";
    if(applied_from == 'added_coupon'){

        applied_code = $("#promocode").val();
        console.log("applied_code.length::"+applied_code.length);
        if(applied_code.length === '0'){
            $("#coupon_errors").html("Please enter coupon code");
        }

    }else{
        applied_code = code;
    }

    if(applied_code.length > 0){

    }else{

        if(applied_from == 'remove_coupon'){

        }else{

            $("#coupon_errors").html("Please enter coupon code");

            return;

        }

    }

        $("#coupon_errors").html("");
        var request_data = {"coupon_code":applied_code};

        customResponseHandler(
            "coupon_apply", // Ajax URl
            'GET', // Method call
            request_data, // Request data
            function success(data){ // onSuccess
                console.log(data);
                // data = JSON.parse(data);
                if((data["status"] >= 200) && (data["status"] < 300)){
                    // var message = data["data"]["message"];

                    if(applied_code == ''){
                        showToast("Code Removed successfully","Success");
                    }else{
                        showToast("Code Applied successfully","Success");
                    }




                    $("#choose_time_popup").modal("hide");
                    $("#Buyer_tm_detail").modal("hide");

                    $("#cart_checkout_row_div").html(data["html"]);
                    $("#cart_content").html(data["slide_html"]);

                    $(".ofrSldm-hamburger").click();
                }
                else{
                    // showToast(""+data["error"]["message"],"Error");

                    $("#coupon_errors").html(data["error"]["message"]);

                }



            }
        );


}

// Custom Design Mix..............................Over

// Notification MODULE OVER...........................................

function notificationClick(){

    var request_data = {};

    $("#noti_list_div").html('<div class="overlay">\
    <span class="spinner"></span>\
</div>');

    customResponseHandler(
        "buyer/notification", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);


            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];


                        // showToast("Product added successfully","Success");

                        $("#noti_list_div").html(data["html"]);

              }else{
                showToast(""+data["error"]["message"],"Error");

            }


        }

    );

}
// Notification MODULE OVER...........................................

// Support Ticket MODULE Start...........................................


$(document).ready(function() {
    /*character limit script start*/
    // $('textarea#ctsubject, textarea#ctdescription').characterlimit();
    /*character limit script end*/
    // $("#nearest-vendors").modal('show');

    $("form[name='add_support_ticket_form']").validate({

        rules:{
            question_type: "required",
            severity: "required",
            subject: {
                required: true,
                noSpace: true
            },
            description: {
                required: true,
                noSpace: true
            },
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf|png"
            }


        },
        messages:{
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf, png) file format"
            }

        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "question_type" ) {
              error.insertAfter("#question_type_div");
            } else if (element.attr("name") == "severity" ) {
              error.insertAfter("#severity_div");
            } else if (element.attr("name") == "attachments[]" ) {
                error.insertAfter("#support_ticket_attachment_div");
              } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");

            // var form_values = $("form[name='add_support_ticket_form']").serialize();


            var form_values = new FormData(form);
            console.log(form_values);

            addSupportTicket(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });



    /*reply form show hide script */
    $('.showButton').click(function () {
        $('.rplyshowhide').fadeIn();
        $('.rplyshow').fadeOut();
    });
    $('.hideButton').click(function () {
        $('.rplyshowhide').fadeOut();
        $('.rplyshow').fadeIn();
    });

    $("form[name='support_ticket_reply_form']").validate({

        rules:{

            comment: "required",
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf|png"
            }

        },
        messages:{
            comment: "Comment is required",
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf, png) file format"
            }
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");

            // var form_values = $("form[name='add_support_ticket_form']").serialize();


            var form_values = new FormData(form);
            console.log(form_values);

            replySupportTicket(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

    $('#change_pass').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $( "#edit_profile" ).on( "click", function() {

        $('.profile_ele').removeAttr("disabled");
        $("#email_update_label").show();
        $("#mobile_update_label").show();
        $("#profile_cancel").show();
      });

      $( "#profile_cancel" ).on( "click", function() {

        resetEditForm('account_details_update_form');

        $('.profile_ele').attr("disabled","false");

        $("#email_update_label").hide();
        $("#mobile_update_label").hide();
        $("#profile_cancel").hide();
      });
      
      $("#mobile_update_label").on( "click", function() {

        resetEditForm('update-mobile-otp-form');
        
        OTPonUpdateMobile(1,0);
      });
      
      $("#email_update_label").on( "click", function() {

        resetEditForm('update-email-otp-form');
        
        OTPonUpdateEmail(1,0);
      });


});

function addSupportTicket(form_values){

    responsehandlerWithFiles("buyer/my_support_ticket/add",
                    "POST",
                    form_values,
                    "#create-ticket-popup",
                    "Support Ticket Added successfully"
                );

}

function replySupportTicket(form_values){

    responsehandlerWithFiles("buyer/my_support_ticket/reply",
                    "POST",
                    form_values,
                    "#create-ticket-popup",
                    "Reply sent successfully"
                );

}

// Support Ticket MODULE OVER...........................................

var getParams = function (url) {
	var params = {};
    var param_string = "";
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);

        param_string += pair[0]+'='+decodeURIComponent(pair[1])+'&';
	}
	// return params;
    param_string = param_string.substring(0, param_string.length - 1);
	return param_string;
};

var getParamsForFilter = function (url) {
	var params = {};
    var param_string = "";
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
    var is_after = 0;
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);

       
        param_string += pair[0]+'='+decodeURIComponent(pair[1])+'&';
        

        
	}

	// return params;
    param_string = param_string.substring(0, param_string.length - 1);
	return param_string;
};



$(function () {
    var smsCodes = $('.smsCode');
    function goToNextInput(e) {
        var key = e.which,
            t = $(e.target),
            // Get the next input
            sib = t.closest('.smsCode').next();
        // Not allow any keys to work except for tab and number
        if (key != 9 && (key < 48 || key > 57) && (key < 95 || key > 106)) {
            console.log("!=9");
            e.preventDefault();
            return false;
        }
        // Tab
        if (key === 9) {
            console.log("===9");
            return true;
        }
        // Go back to the first one
        if (!sib || !sib.length) {
            console.log("!sib || !sib.length");
            sib = $('.smsCode').eq(0);
            console.log(sib);
        }
        sib.select().focus();
    }
    function onKeyDown(e) {
        var key = e.which;

        // only allow tab and number
        if (key === 9 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key === 8) {
            return true;
        }
        e.preventDefault();
        return false;
    }
    function onFocus(e) {
        $(e.target).select();
    }
    smsCodes.on('keyup', goToNextInput);
    smsCodes.on('keydown', onKeyDown);
    smsCodes.on('click', onFocus);
});


$(document).ready(function(){
    $("#deliver_qty").val($('#popup_selected_delivery_qty').val());
    
    $("#location_delivery_qty").val($('#popup_selected_delivery_qty').val());
    $("#location_dialog_grade_dropdown").val($('#popup_selected_rmc_grade_id').val());
    
    
  

    $selected_rmc = $('#popup_selected_type_of_rmc').val();

    if($selected_rmc == 'product_rmc'){
        $("#product_rmc").prop("checked", true);
        $("#type_of_rmc").val($selected_rmc);
        $("#product_cat_id").val($('#popup_selected_rmc_grade_id').val());
        $("#rmc_grade_id").val($('#popup_selected_rmc_grade_id').val());
    }else if($selected_rmc == 'product_custom_rmc'){
        $("#product_custom_rmc").prop("checked", true);
        $("#concrete_grade_id").val($('#popup_selected_rmc_grade_id').val());
        $("#type_of_rmc").val($selected_rmc);
        $("#product_cat_id").val("custom_mix_dialog_on");
        $("#rmc_grade_id").val($('#popup_selected_rmc_grade_id').val());
    }

	$('.CP_unavail_date').datepicker({
        autoclose: true,
        startDate: '1d',
        format: 'dd M yyyy'
        // format: 'yyyy-mm-dd'
	});

    $(".CP_unavail_date").on('change', function(event) {
        event.preventDefault();
        // alert(this.value);
        $("#selected_delivery_date").val(this.value);
        $("#delivery_date_error").html("");
        /* Act on the event */
    });

    $('.CP_unavail_date').datepicker().val($('#popup_selected_delivery_date').val()).trigger('change')

    var selected_delivery_date = $('#popup_selected_delivery_date').val();
    var selected_delivery_end_date = $('#popup_selected_delivery_end_date').val();

    var today = new Date();
    var today_to_30 = new Date();

    today.setDate(today.getDate() + 2);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    today_request = yyyy + '/' + mm + '/'+dd;
    // console.log("today..."+today);

    today_to_30.setDate(today_to_30.getDate() + 32);
    var dd_30 = String(today_to_30.getDate()).padStart(2, '0');
    var mm_30 = String(today_to_30.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy_30 = today_to_30.getFullYear();

    today_to_30 = dd_30 + '/' + mm_30 + '/' + yyyy_30;
    today_to_30_request =  yyyy_30 + '/' + mm_30 + '/' + dd_30;

    var end_date = today;
    var end_date_request = today_request;
    $("#selected_delivery_date").val(today_request);

        if(selected_delivery_date != ''){
            var selected_start_date = new Date(selected_delivery_date);
            var dd = String(selected_start_date.getDate()).padStart(2, '0');
            var mm = String(selected_start_date.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = selected_start_date.getFullYear();

            $("#selected_delivery_date").val(''+selected_delivery_date);

            selected_delivery_date = dd + '/' + mm + '/' + yyyy;

            today = selected_delivery_date;
        }


    // $("#selected_end_delivery_date").val(today_to_30_request);
    $("#selected_end_delivery_date").val(end_date_request);
    // $("#selected_end_delivery_date").val(today_request);


        if(selected_delivery_end_date != ''){
            var selected_end_date = new Date(selected_delivery_end_date);
            var dd = String(selected_end_date.getDate()).padStart(2, '0');
            var mm = String(selected_end_date.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = selected_end_date.getFullYear();
            console.log("selected_delivery_end_date...."+selected_delivery_end_date);
            $("#selected_end_delivery_date").val(''+selected_delivery_end_date);

            selected_delivery_end_date = dd + '/' + mm + '/' + yyyy;

            end_date = selected_delivery_end_date;

        }

        console.log("today...."+today);
        console.log("end_date...."+end_date);

    $('input[name="daterange"]').daterangepicker({
        startDate: today,
        endDate: end_date,
        minDate: today,
        maxDate: today_to_30,
        locale: {
            format: 'DD/MM/YYYY'
        },
        drops: 'up'

    });

    // $('input[name="daterange"]').on('change.datepicker', function(ev){
    //     var picker = $(ev.target).data('daterangepicker');
    //     console.log(picker.startDate); // contains the selected start date
    //     console.log(picker.endDate); // contains the selected end date

    //     // ... here you can compare the dates and call your callback.
    // });

    $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {

        // var start_date = picker.startDate.format('DD/MM/YYYY');
        var start_date = picker.startDate.format('YYYY/MM/DD');
        var end_date = picker.endDate.format('YYYY/MM/DD');
        console.log("end_date::"+end_date);
        $("#selected_delivery_date").val(start_date);
        $("#selected_end_delivery_date").val(end_date);
        $("#delivery_date_error").html("");

        console.log(start_date);
        console.log(end_date);

        var diff = picker.endDate.diff(picker.startDate, 'days');

        $("#selected_range_diff").val(diff);

        console.log(diff);

        checkDeliveryQtyDays();




    });

    // $("#deliver_qty").keyup();

});

$(document).ready(function(){
    // Initialize
    var bLazy = new Blazy({ 
        selector: 'img', // all images
        // offset: 100
    });
})


$(document).ready(function(){
    $("#attachments_support").on('change', function (e){
        
        // var fileName = $(this).val();
        var fileName = e.target.files[0].name;
        console.log("tset..."+fileName);
        $("#selected_ticket_doc").val(fileName);
    });
    
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        var id_name = $(this).attr("id");
        // console.log("id_name......"+id_name);
            // console.log("fileName......"+fileName);
            // $("#"+id_name+"_text").val(fileName);
            $(this).parent().next( ".browse-input" ).val(fileName);
            $(this).trigger( "focusout" );
    
            // alert('The file "' + fileName +  '" has been selected.');
        });
});

function downloadOrders(type) {
    var after = '';
    var name = '';
    
    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
    window.location.href =
    "buyer/reports/order_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
        //     window.location.href =
        //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
        // } else if (type == "csv") {
            //     window.location.href =
            //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
            // }
    }

$(document).ready(function(){
    
    $(document).on('change','.mix_cement_brnad',function() {
        
        var cement_brand_id = $('option:selected', this).attr('data-cement-brand-id');
        var cement_grade_id = $('option:selected', this).attr('data-cement-grade-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_cement_brnad::::...."+selected);
        
        $("#mix_rmc_cement_brand_id_"+rmc_id).val(cement_brand_id);
        $("#mix_rmc_cement_grade_id_"+rmc_id).val(cement_grade_id);

        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_sand_source',function() {
        
        var sand_source_id = $('option:selected', this).attr('data-sand-source-id');
        var sand_zone_id = $('option:selected', this).attr('data-sand-zone-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_rmc_sand_source_id_"+rmc_id).val(sand_source_id);
        $("#mix_rmc_sand_zone_id_"+rmc_id).val(sand_zone_id);
          
        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_aggregate_source_cat_1_2',function() {
        
        var final_agg_source_id = $('option:selected', this).attr('data-aggr-source-id');
        var final_aggregate1_sub_cat_id = $('option:selected', this).attr('data-sub-cat-1-id');
        var final_aggregate2_sub_cat_id = $('option:selected', this).attr('data-sub-cat-2-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_rmc_aggr_source_id_"+rmc_id).val(final_agg_source_id);
        $("#mix_rmc_aggr_cat_1_id_"+rmc_id).val(final_aggregate1_sub_cat_id);
        $("#mix_rmc_aggr_cat_2_id_"+rmc_id).val(final_aggregate2_sub_cat_id);
          
        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_admix_source',function() {
        
        var final_admix_brand_id = $('option:selected', this).attr('data-admix-brand-id');
        var final_admix_cat_id = $('option:selected', this).attr('data-admix-cat-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_rmc_admix_brand_id_"+rmc_id).val(final_admix_brand_id);
        $("#mix_rmc_admix_cat_id_"+rmc_id).val(final_admix_cat_id);
          
        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_flyash_source',function() {
        
        var final_fly_ash_source_id = $('option:selected', this).attr('data-flyash-source-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_rmc_fly_ash_id_"+rmc_id).val(final_fly_ash_source_id);
          
        getRMCCombination(rmc_id);
    });

    $(document).on('change','.mix_water_type',function() {
        
        var water_type = $('option:selected', this).attr('data-water-type');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_water_type_"+rmc_id).val(water_type);
          
        getRMCCombination(rmc_id);
    });

    $("form[name='add_7_report_form']").validate({

        rules:{
            buyer_qube_test_report_7days:{
                required:true,
                extension: "jpg|jpeg|doc|docx|pdf" 
            },
            qube_test_report_7days_reject_reason:{
                required:true,
            }
            
        },
        messages:{
            buyer_qube_test_report_7days : {
                extension: "Please enter valid file (jpg, jpeg, doc, docx, pdf) formats"
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "buyer_qube_test_report_7days" ) {
              error.insertAfter("#pic_7_report_div");
            }  else {
              error.insertAfter(element);
            }

          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_7_report_form"] p.error').remove();
            // var form_type = $("form[name='upload_order_bill_form'] #upload_order_bill_form_type").val();
            // var form_values = $("form[name='upload_order_bill_form']").serialize();
            var form_values = new FormData(form);
            // form_values.append('qube_test_report_7days_accept_reject','2');
            console.log(form_values);
            // console.log("form_type ..."+form_type );

            reject7DaysReport(form_values);
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_7_report_form"] p.error').remove();
        }

    });

    $("form[name='add_28_report_form']").validate({

        rules:{
            buyer_qube_test_report_28days:{
                required:true,
                extension: "jpg|jpeg|doc|docx|pdf" 
            },
            qube_test_report_28days_reject_reason:{
                required:true,
            }
            
        },
        messages:{
            buyer_qube_test_report_28days : {
                extension: "Please enter valid file (jpg, jpeg, doc, docx, pdf) formats"
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "buyer_qube_test_report_28days" ) {
              error.insertAfter("#pic_28_report_div");
            }  else {
              error.insertAfter(element);
            }

          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_28_report_form"] p.error').remove();
            // var form_type = $("form[name='upload_order_bill_form'] #upload_order_bill_form_type").val();
            // var form_values = $("form[name='upload_order_bill_form']").serialize();
            var form_values = new FormData(form);
            console.log(form_values);
            // console.log("form_type ..."+form_type );

            reject28DaysReport(form_values);
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_28_report_form"] p.error').remove();
        }

    });
    

});

function getRMCCombination(rmc_id){

    var request_data = $("#rmc_combination_form_"+rmc_id).serialize();

    customResponseHandler(
        "buyer/product_detail/get_single_mix_combination", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // showToast("Quantity updated","Success");

                // $("#design_mix_body").html(data["html"]);
                $("#product_detail_rmc").html(data["html"]);

                // $("#show-design-mix-details").modal("show");

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );


}

function orderReassignAcceptReject(status,order_item_part_id){

    var request_data = {
        'reassign_status' : status,
        'order_item_part_id' : order_item_part_id,
    };

    customResponseHandler(
        "buyer/order_reassign_status_change", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // showToast("Quantity updated","Success");

                // $("#design_mix_body").html(data["html"]);
                if(status == 1){
                    showToast("Order reassign request accepted successfully","Success");      
                }
                
                if(status == 2){
                    showToast("Order reassign request rejected successfully","Success");      
                }
                // $("#product_detail_rmc").html(data["html"]);

                // $("#show-design-mix-details").modal("show");

                reload();

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );


}

function accept7DaysReport(vendor_order_id, track_id, TM_Id){

    var request_data = {
        "qube_test_report_7days_accept_reject":1,
        "vendor_order_id":vendor_order_id,
        "track_id":track_id,
        "TM_Id":TM_Id
    }

    responsehandler("buyer/order_7_28_days_report_accept",
                    "GET",
                    request_data,
                    '',
                    'Report accepted successfully',
                    1,
                    ''
                );


}

function reject7DaysReport(form_values){

    // var request_data = form_values;

    responsehandlerWithFiles("buyer/order_7_28_days_report_reject", 
                    "POST", 
                    form_values, 
                    "#add-7-report-details",
                    "Rejected and your 7th day report added successfully",                    1,
                    "add_7_report_form"
                );


}



function accept28DaysReport(vendor_order_id, track_id, TM_Id){

    var request_data = {
        "qube_test_report_28days_accept_reject":1,
        "vendor_order_id":vendor_order_id,
        "track_id":track_id,
        "TM_Id":TM_Id
    }

    responsehandler("buyer/order_7_28_days_report_accept",
                    "GET",
                    request_data,
                    '',
                    'Report accepted successfully',
                    1,
                    ''
                );


}

function reject28DaysReport(form_values){

    responsehandlerWithFiles("buyer/order_7_28_days_report_reject", 
                    "POST", 
                    form_values, 
                    "#add-7-report-details",
                    "Rejected and your 7th day report added successfully",                    1,
                    "add_7_report_form"
                );



}