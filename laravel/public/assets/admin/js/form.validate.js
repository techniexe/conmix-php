$(function() {
    $("form[name='vehicle_add_cat_form']").validate({
        rules: {
            category_name: "required"
        },

        messages: {
            category_name: "Please enter category name"
        },
        submitHandler: function(form) {
            // form.submit();
        },
        invalidHandler: function(event, validator) {
            event.preventDefault();
        }
    });
});