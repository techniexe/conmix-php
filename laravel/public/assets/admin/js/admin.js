// var doAjax_params_default = {
//     'url': null,
//     'requestType': "GET",
//     'contentType': 'application/x-www-form-urlencoded; charset=UTF-8',
//     'dataType': 'json',
//     'data': {},
//     'beforeSendCallbackFunction': null,
//     'successCallbackFunction': null,
//     'completeCallbackFunction': null,
//     'errorCallBackFunction': null,
// };

// function doAjax(doAjax_params) {

//     var url = doAjax_params['url'];
//     var requestType = doAjax_params['requestType'];
//     var contentType = doAjax_params['contentType'];
//     var dataType = doAjax_params['dataType'];
//     var data = doAjax_params['data'];
//     var beforeSendCallbackFunction = doAjax_params['beforeSendCallbackFunction'];
//     var successCallbackFunction = doAjax_params['successCallbackFunction'];
//     var completeCallbackFunction = doAjax_params['completeCallbackFunction'];
//     var errorCallBackFunction = doAjax_params['errorCallBackFunction'];

//     //make sure that url ends with '/'
//     /*if(!url.endsWith("/")){
//      url = url + "/";
//     }*/
//     // console.log("url......."+url);
//     $.ajax({
//         url: BASE_URL+'/'+url,
//         crossDomain: true,
//         type: requestType,
//         contentType: contentType,
//         dataType: dataType,
//         data: data,
//         beforeSend: function(jqXHR, settings) {
//             $("#overlay").fadeIn(300);
//             if (typeof beforeSendCallbackFunction === "function") {
//                 beforeSendCallbackFunction();
//             }
//         },
//         success: function(data, textStatus, jqXHR) {    
//             if (typeof successCallbackFunction === "function") {
//                 successCallbackFunction(data);
//             }
//         },
//         error: function(jqXHR, textStatus, errorThrown) {
//             if (typeof errorCallBackFunction === "function") {
//                 errorCallBackFunction(errorThrown);
//             }

//         },
//         complete: function(jqXHR, textStatus) {
//             $("#overlay").fadeOut(300);
//             if (typeof completeCallbackFunction === "function") {
//                 completeCallbackFunction();
//             }
//         }
//     });
// }

// function doAjaxWithFile(doAjax_params) {

//     var url = doAjax_params['url'];
//     var requestType = doAjax_params['requestType'];
//     var contentType = doAjax_params['contentType'];
//     var dataType = doAjax_params['dataType'];
//     var data = doAjax_params['data'];
//     var beforeSendCallbackFunction = doAjax_params['beforeSendCallbackFunction'];
//     var successCallbackFunction = doAjax_params['successCallbackFunction'];
//     var completeCallbackFunction = doAjax_params['completeCallbackFunction'];
//     var errorCallBackFunction = doAjax_params['errorCallBackFunction'];

//     //make sure that url ends with '/'
//     /*if(!url.endsWith("/")){
//      url = url + "/";
//     }*/
//     // console.log("url......."+url);
//     $.ajax({
//         url: BASE_URL+'/'+url,
//         crossDomain: true,
//         enctype: 'multipart/form-data',
//         type: requestType,
//         contentType: contentType,
//         dataType: dataType,
//         data: data,
//         processData: false,
//         contentType: false,
//         beforeSend: function(jqXHR, settings) {
//             $("#overlay").fadeIn(300);
//             if (typeof beforeSendCallbackFunction === "function") {
//                 beforeSendCallbackFunction();
//             }
//         },
//         success: function(data, textStatus, jqXHR) {    
//             if (typeof successCallbackFunction === "function") {
//                 successCallbackFunction(data);
//             }
//         },
//         error: function(jqXHR, textStatus, errorThrown) {
//             if (typeof errorCallBackFunction === "function") {
//                 errorCallBackFunction(errorThrown);
//             }

//         },
//         complete: function(jqXHR, textStatus) {
//             $("#overlay").fadeOut(300);
//             if (typeof completeCallbackFunction === "function") {
//                 completeCallbackFunction();
//             }
//         }
//     });
// }

// function responsehandler(url, requestType, data, modal,message){
    
//     var params = $.extend({}, doAjax_params_default);
//     params['requestType'] = ""+requestType
//     params['url'] = ''+url;
//     params['data'] = data;
//     params['successCallbackFunction'] = function success(data){
//         console.log(data);
//         if((data["status"] >= 200) && (data["status"] < 300)){  //Success
//             $(''+modal).modal('hide');                
//             showToast(""+message,"Success");
//             reload();
//         }else{ //Error
//             showToast(""+data["error"]["message"],"Error");
//         }

//     };


//     doAjax(params);
// }

// function responsehandlerWithFiles(url, requestType, data, modal,message){
    
//     var params = $.extend({}, doAjax_params_default);
//     params['requestType'] = ""+requestType
//     params['url'] = ''+url;
//     params['data'] = data;
//     params['successCallbackFunction'] = function success(data){

//         if((data["status"] >= 200) && (data["status"] < 300)){  //Success
//             $(''+modal).modal('hide');                
//             showToast(""+message,"Success");
//             reload();
//         }else{ //Error
//             showToast(""+data["error"]["message"],"Error");
//         }

//     };


//     doAjaxWithFile(params);
// }

// function customResponseHandler(url, requestType, data, callback){
    
//     var params = $.extend({}, doAjax_params_default);
//     params['requestType'] = ""+requestType
//     params['url'] = ''+url;
//     params['data'] = data;
//     params['successCallbackFunction'] = callback;


//     doAjax(params);
// }
var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var phoneno = /^\d{12}$/;
$(document).ready(function(){
   
    $("form[name='forgot_pass_form']").validate({

        rules:{
            
            password: {
                required: true,
                minlength : 6,
                maxlength: 20
            }, 
            confirm_password:{
                required: true,
                minlength : 6,
                equalTo : "#new_password"
            },           
            
        },
        messages:{

        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            
            // var form_values = $("form[name='forgot_pass_form']").serialize();            
            
            // var form_values = new FormData(form);
            // console.log(form_values);
        
            // changePassword(form_values);
            // $("form[name='forgot_pass_form']").submit();
            $("form[name='forgot_pass_form']")[0].submit();
            // requestOTP(19,"","");
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

    $("form[name='add_user_form']").validate({
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
                a_z_pattern: true
            },
            email: {
                required:true,
                email:true
            },
            mobile:{
                required:true,
                mobile_validation: true,
                minlength: 13,
                maxlength: 13
            },
            admin_type: "required",
            password: {
                required: function(){
                    return $("#add_user_form_type").val() === 'add';
              },
                minlength: 6,
                maxlength: 20
            }
        },
        messages: {
            mobile:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            }
        },
        errorPlacement: function(error, element) {
            
            if (element.attr("name") == "admin_type" ) {
              error.insertAfter("#admin_type_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {

            $('form[name="add_user_form"] p.error').remove();

            var form_type = $("#add_user_form_type").val();
            var form_values = $("form[name='add_user_form']").serialize();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addUser(form_values);
            }else if(form_type == "edit"){
                $('#add-user-popup').modal('hide'); 

                requestOTP(2,"","","edit_admin");
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_user_form"] p.error').remove();
        }
    });

    // $('#add_admin_user').submit(function(e){
    //     e.preventDefault();
    //     var form_type = $("#add_user_form_type").val();
    //     console.log(form_type);
    //     if(form_type === 'add'){
            
    //         var email = $("#admin_email").val();
    //         if(!email.match(mailformat)){
    //             $("#admin_email_error").html("Please enter valid email address");
    //             return false;
    //         }
    //         var mobile = $("#mobileno").val().trim().replace("+","");
    //         if(!mobile.match(phoneno)){
    //             $("#admin_mobile_error").html("Mobile number should be 12 digit");
    //             return false;
    //         }
            
    //         var form_values = $(this).serialize();
            
    //         $.ajax({
    //             type: "POST",
    //             url: "admin_user/add", 
    //             data: form_values,
    //             beforeSend: function(){
    //                 $("#overlay").fadeIn(300);
    //             },
    //             success: function(data){
    //                 if(data["status"] == 200){  //Success
    //                     $('#add-user-popup').modal('hide');                
    //                     showToast("Admin user added successfully","Success");
    //                     reload();
    //                 }else{ //Error
    //                     showToast(""+data["error"]["message"],"Error");
    //                 }                
                    
    //             },
    //             complete: function(){
    //                 $("#overlay").fadeOut(300);
    //             }
    //         });
    //     }else if(form_type === 'edit'){
            
    //         $('#add-user-popup').modal('hide'); 

    //         requestOTP(2,"","");
    //     }
    // });
   
    $("form[name='TM_add_cat_form']").validate({
        rules: {
            category_name: {
                required: true,
                minlength: 2,
                maxlength: 64,
                noSpace: true
            }
        },
        messages: {
            category_name: {
            }
        },
        submitHandler: function(form) {

            $('form[name="TM_add_cat_form"] p.error').remove();

            var form_type = $("form[name='TM_add_cat_form'] #TM_add_cat_form_type").val();
            var form_values = $("form[name='TM_add_cat_form']").serialize();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addTMCategory(form_values);
            }else if(form_type == "edit"){
                // $("#add-TM-category").modal("hide");
                requestOTP(4,form_values,"","edit_TM_category");
            }

            
        },
        invalidHandler: function(event, validator) {
            $('form[name="TM_add_cat_form"] p.error').remove();
        }
    });   

    $("form[name='TM_add_sub_cat_form']").validate({
        rules: {
            TM_cat_id: "required",
            sub_category_name: {
                required: true,
                minlength: 2,
                maxlength: 64,
                noSpace: true
            },
            TM_capacity: {
                required: true,
                number: true
            },
            weight_unit_code: {
                required: true,
                noSpace: true
            },
            weight_unit: {
                required: true,
                noSpace: true
            }
        },
        messages: {
            sub_category_name: {
            },
            TM_capacity:{
                number: "TM capacity should be number"
            },
            weight_unit_code: {
            },
            weight_unit: {
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "TM_cat_id" ) {
              error.insertAfter("#vehicle_cat_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            // console.log("submited");

            $('form[name="TM_add_sub_cat_form"] p.error').remove();

            var form_type = $("form[name='TM_add_sub_cat_form'] #TM_add_sub_cat_form_type").val();
            var form_values = $("form[name='TM_add_sub_cat_form']").serialize();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addTMSubCategory(form_values);
            }else if(form_type == "edit"){
                // $("#TM-sub-category").modal("hide");
                requestOTP(5,form_values,"","edit_TM_subcategory");
            }

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="TM_add_sub_cat_form"] p.error').remove();
        }
    });


    $("form[name='pump_add_cat_form']").validate({
        rules: {
            category_name: {
                required: true,
                minlength: 2,
                maxlength: 64,
                noSpace: true
            }
        },
        messages: {
            category_name: {
            }
        },
        submitHandler: function(form) {

            $('form[name="pump_add_cat_form"] p.error').remove();

            var form_type = $("form[name='pump_add_cat_form'] #pump_add_cat_form_type").val();
            var form_values = $("form[name='pump_add_cat_form']").serialize();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addPumpCategory(form_values);
            }else if(form_type == "edit"){
                // $("#add-pump-category").modal("hide");
                requestOTP(24,form_values,"","edit_pump_cat");
            }

            
        },
        invalidHandler: function(event, validator) {
            $('form[name="pump_add_cat_form"] p.error').remove();
        }
    });   


    $("form[name='vehical_pm_km_form']").validate({
        rules: {
            state_id: {
                required: true
            },
            region_type: {
                required: true
            },
            per_metric_ton_per_km_rate: {
                required: true,
                number:true
            }
        },
        messages: {

        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "state_id" || element.attr("name") == "region_type" ) {
              error.insertAfter(element.parent());
            }  else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {

            $('form[name="vehical_pm_km_form"] p.error').remove();

            var form_type = $("form[name='vehical_pm_km_form'] #vehical_pm_km_form_type").val();
            var form_values = $("form[name='vehical_pm_km_form']").serialize();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addVehiclePMKM(form_values);
            }else if(form_type == "edit"){
                console.log("form_type ..."+form_type );
                // $("#add-vehical-category").modal("hide");
                editVehiclePMKM(form_values);
            }

            
        },
        invalidHandler: function(event, validator) {
            $('form[name="vehical_pm_km_form"] p.error').remove();
        }
    }); 




    $.validator.setDefaults({ ignore: ":hidden:not(select)" });
    $("form[name='add_country_form']").validate({

        rules:{
            country_id: {
                required: true,
                noSpace: true
            },
            country_name: "required",
            country_code: {
                required: true,
                noSpace: true
            },

        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "country_name" ) {
              error.insertAfter("#country_name_div");
            } else {
              error.insertAfter(element);
            }
          },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
           
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_country_form"] p.error').remove();

            var form_type = $("form[name='add_country_form'] #add_country_form_type").val();
            var form_values = $("form[name='add_country_form']").serialize();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addRegionCountry(form_values);
            }else if(form_type == 'edit'){
                updateCountryDetails(form_values);
            }
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_country_form"] p.error').remove();
        }
    });   
    
    $('#' + country_name_states_selectID + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + country_name_states_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + country_name_states_selectID + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + country_name_states_selectID + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'region_states/getCountries',country_name_states_selectID);  //Set timer back if got value on input

    //   }
  
   


        // Change No Result Match text to Searching.
      $('#' + country_name_states_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + country_name_states_selectID + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + country_name_states_selectID + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'region_states/getCountries',country_name_states_selectID);  //Set timer back if got value on input

    //   }
  
    });
    
    $('#' + state_name_city_selectID + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + state_name_city_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + state_name_city_selectID + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + state_name_city_selectID + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'region_cities/getStates',state_name_city_selectID);  //Set timer back if got value on input

    //   }
  
    });
    
    $('#' + state_name_city_filter_selectID + '_chosen .chosen-search input').keyup(function(){

        // Change No Result Match text to Searching.
      $('#' + state_name_city_filter_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + state_name_city_filter_selectID + '_chosen .chosen-search input').val() + '"');

      clearTimeout(typingTimer);  //Refresh Timer on keyup
      
      typingTimer = setTimeout(searchCountryState, doneTypingInterval,'region_cities/getStates',state_name_city_filter_selectID);  //Set timer back if got value on input

  
    });

    $("#country_name_states").on('change',function(){

        var value = $(this).val().split("_");
        var country_name = value[0];
        var country_id = value[1];
        var country_code = value[2];

        $("#state_country_name").val(country_name);
        $("#state_country_id").val(country_id);
        $("#state_country_code").val(country_code);

    });

    // $('#' + country_name_states_selectID).chosen().change(function(){
    //     var value = $(this).val().split("_");
    //     var country_name = value[0];
    //     var country_id = value[1];
    //     var country_code = value[2];

    //     $("#state_country_name").val(country_name);
    //     $("#state_country_id").val(country_id);
    //     $("#state_country_code").val(country_code);
    // });

    $.validator.setDefaults({ ignore: ":hidden:not(select)" });
    $("form[name='add_states_form']").validate({

        rules:{
            country_name_select: "required",
            country_id: "required",
            country_name: "required",
            country_code: "required",
            state_name: {
                required: true
            },
        },
        messages:{
            // country_id: "Please add country id",
            // country_name: "Please select at least one country",
            // country_name_select: "Please select at least one country",
            // country_code: "Please add country code",
            // state_name: {
            //     required: "Please add state name"
            // },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "country_name_select" ) {
              error.insertAfter("#country_name_div");
            } else {
              error.insertAfter(element);
            }
          },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
           
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_states_form"] p.error').remove();

            var form_type = $("form[name='add_states_form'] #add_states_form_type").val();
            var form_values = $("form[name='add_states_form']").serialize();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addRegionState(form_values);
            }
            else if(form_type == 'edit'){
                requestOTP(43,form_values,"","edit_state");
                
            }
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_states_form"] p.error').remove();
        }

    });
    
    $.validator.setDefaults({ ignore: ":hidden:not(select)" });

    

    $("form[name='add_city_form']").validate({

        rules:{
            state_id: "required",
            city_name: {
                required: true
            },
            us2_address: "required"
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "state_id" ) {
              error.insertAfter("#state_name_div");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
           
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_city_form"] p.error').remove();

            var us2_lat = $("#us2_lat").val();
            var us2_lon = $("#us2_lon").val();
            var is_valid = true;
            if(us2_lat == 0){
                is_valid = false;
                $("#us2_lat_error").html("Please select location");
                $("#us2_address").focus();
            }

            if(us2_lon == 0){
                is_valid = false;
                $("#us2_lon_error").html("Please select location");
                $("#us2_address").focus();
            }

            if(is_valid){
                var form_type = $("form[name='add_city_form'] #add_city_form_type").val();
                var form_values = $("form[name='add_city_form']").serialize();
                console.log("form_type ..."+form_type );

                if(form_type == 'add'){
                    addRegionCity(form_values);
                }
                else if(form_type == 'edit'){
                    requestOTP(44,form_values,"","edit_city");
                    
                }
            }
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_city_form"] p.error').remove();
        }

    });


    // $('select[class="form-control-chosen"]').change(function(){
    //     console.log("test...1");
    //     $(this).valid();
    // });

    $('select[class="form-control-chosen"]').on('change', function() {
        // console.log("test...1..."+reset);
        // if(reset == 0){
            $(this).trigger('blur');
        // }
        
      });

});
var reset = 0;
function addUser(form_values){

    responsehandler("admin/admin_user/add", 
                    "POST", 
                    form_values, 
                    "#add-user-popup",
                    "Admin user added successfully",
                    0,
                    "add_user_form"
                );


}
var active_inactive_data = {};
var delete_data = {};
function confirmPopup(type,data,csrf,id,is_request_otp = 1){
    
    var message = "";
    if((type == 1) || (type == 28)){
        //Active InActive

        data = JSON.parse(data);
        console.log(data);
        var active_inactive = "Active";
        if(data["is_active"])
        {
            active_inactive = "InActive";
        }
        var number = "";
        if(data["mobile_number"] == undefined){
            number = "";
        }else{
            number = " "+data["mobile_number"];
        }

        message = "Are you sure you want to "+ active_inactive +number+"?";
    }else if((type == 3) || (type == 6) || (type == 7) || (type == 8) || (type == 15) || (type == 21) || (type == 22) || (type == 23)
     || (type == 24)|| (type == 29) || (type == 31)|| (type == 33)|| (type == 35) || (type == 45) || (type == 46) || (type == 47)){
        //Delete User
        delete_data = data;
        data = JSON.parse(data);
        console.log(data);
        var number = "";
        console.log(data["mobile_number"]);
        if(data["mobile_number"] === undefined){
            number = "";
        }else{
            number = " "+data["mobile_number"];
        }

        message = "Are you sure you want to delete "+number+"?";
    }

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        if(is_request_otp == 1){
            if(type == 3){
                requestOTP(type,delete_data,csrf,"delete_admin");
            }else if(type == 1){
                active_inactive_data = data;
                if(active_inactive == 'Active'){
                    requestOTP(type,data,csrf,"active_admin");
                }else if(active_inactive == 'InActive'){
                    requestOTP(type,data,csrf,"Inactive_admin");
                }
                
            }else if(type == 7){
                requestOTP(type,delete_data,csrf,"delete_state");
                
                
            }else if(type == 45){
                requestOTP(type,delete_data,csrf,"delete_city");
                
                
            }else{
                requestOTP(type,data,csrf);
            }
        }else{
            deleteRecord(type,data,csrf);
        }

    });
}


function resendOTP(){
    
    $.ajax({
        type: "GET", 
        url: BASE_URL+"/admin_user/request_otp", 
        data: {},
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            $("#overlay").fadeOut(300);
            var fiveMinutes = 60 * 2,
                
            display = $('#time_forgot_pass');
            startTimer(fiveMinutes, display, "admin_otp_verify");
            if(data["status"] == 200){
                showToast("OTP Sent successfully to your registered mobile number" ,"Success");

            }else{
                showToast("Somethig went wrong, Please try again","Error");
            }

        }
    });

}

function requestOTP(type,request_data,csrf,event_type){

    var final_data = request_data;
    if(event_type != undefined){

        request_data = request_data+"&event_type="+event_type;

        
    }

    $.ajax({
        type: "GET",
        url: BASE_URL+"/admin_user/request_otp", 
        data: request_data,
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            $("label.error").remove();
            $(".form-control").removeClass("error");
            if(data["status"] == 200){  //Success
                if(event_type == "update_password"){
                    $('#change-password-popup').modal('hide');
                }else if(event_type == "edit_TM_category"){
                    $('#add-TM-category').modal('hide');
                }else if(event_type == "edit_TM_subcategory"){
                    $('#TM-sub-category').modal('hide');
                }else if(event_type == "edit_agg_category"){
                    $('#product-category-popup').modal('hide');
                }else if(event_type == "edit_agg_sub_category"){
                    $('#product-sub-category-popup').modal('hide');
                }else if(event_type == "edit_cg"){
                    $('#concrete-grade-popup').modal('hide');
                }else if(event_type == "edit_cement_brand"){
                    $('#cement-brand-popup').modal('hide');
                }else if(event_type == "edit_admix_brand"){
                    $('#admixture-brand-popup').modal('hide');
                }else if(event_type == "edit_pump_cat"){
                    $('#add-pump-category').modal('hide');
                }else if(event_type == "edit_admix_cat"){
                    $('#admixture-type-popup').modal('hide');
                }else if(event_type == "edit_cement_grade"){
                    $('#cement-grade-popup').modal('hide');
                }else if(event_type == "edit_agg_source"){
                    $('#aggregate-source-popup').modal('hide');
                }else if(event_type == "edit_flyAsh_source"){
                    $('#flyash-source-popup').modal('hide');
                }else if(event_type == "edit_sand_source"){
                    $('#sand-source-popup').modal('hide');
                }else if(event_type == "edit_state"){
                    $('#state-popup').modal('hide');
                }else if(event_type == "edit_city"){
                    $('#city-popup').modal('hide');
                }else if(event_type == "edit_sand_zone"){
                    $('#sand-zone-popup').modal('hide');
                }
                var OTP_mob_number = $("#OTP_mob_number").val();
                if(OTP_mob_number!=undefined){
                    // OTP_mob_number = OTP_mob_number.replace(OTP_mob_number.substring(5, 11),"XXXXXX");
                    $('#OTP_mob_no').html(OTP_mob_number);     
                }
                showToast("OTP Sent successfully to your registered mobile number" ,"Success");
                $("#time_forgot_pass").html("02:00");
                var fiveMinutes = 60 * 2,
                
                display = $('#time_forgot_pass');
                startTimer(fiveMinutes, display, "admin_otp_verify");
                 // Active In Active User
                    $("#otp").val("");
                    $("#otp_error").html("");

                    OTP_dialog = function(){

                        var otp = $("#otp").val();
                        if(otp !== ''){
                            // console.log("numeric..."+$.isNumeric(otp));
                            if($.isNumeric(otp)){
                                if((otp.length === 6) ){    
                                    // $('#otpModal').modal('hide');  
                                    
                                    request_data["authentication_code"] = otp;

                                    if(type == 1){ //Active InActive User
                                        activeInactive(csrf,final_data,otp);
                                    }else if(type == 2){ // Edit User Details
                                        
                                        updateAdminUserDetails(otp);
                                    }else if(type == 3){ //Delete User
                                        deleteAdminUser(csrf,otp,final_data);
                                    }else if(type == 4){ //update category details
                                        
                                        updateTMCategoryDetails(request_data,otp);
                                    }else if(type ==5){ //update subcategory details
                                        updateTMSubCatDetails(request_data,otp)
                                    }else if(type == 7){
                                        deleteState(final_data,csrf,otp);
                                    }else if(type ==8){ //update product category details
                                        // request_data.append("authentication_code",otp);
                                        updateProductCategory(request_data,otp)
                                    }else if(type ==9){ //update product subcategory details
                                        // request_data.append("authentication_code",otp);
                                        updateProductSubCategory(request_data,otp);
                                    }else if(type ==10){ //update Product details
                                        
                                        updateProductDetails(request_data,otp);
                                    }else if(type ==11){ //update Product verify by admin details
                                        
                                        updateVerifyByAdmin(request_data,otp);
                                    }else if(type ==12){ //add Margin Slab
                                        addMarginSlab(request_data,otp);
                                    }else if(type ==13){ //edit Margin Slab
                                        editMarginSlab(request_data,otp);
                                    }else if(type ==14){ //edit GST Slab
                                        editGSTSlab(request_data,otp);
                                    }else if(type ==16){ //edit GST Slab
                                        updateProductReviewDetails(request_data,otp);
                                    }else if(type ==17){ //Update Order and Payment Status
                                        updateOrderStatus(request_data,otp);
                                    }else if(type ==18){ //Update Admin Profile
                                        updateAdminProfile(request_data,otp);
                                    }else if(type ==19){ //Update Ticket status
                                        
                                        updateTicketStatus(request_data,otp);
                                    }else if(type ==20){ //Update Ticket status
                                        
                                        updateAdminPassword(request_data,otp);
                                    }else if(type ==21){ //Update Concrete Grade
                                        
                                        updateConcreteGrade(request_data,otp);
                                    }else if(type ==22){ //Update Cement Brand
                                        
                                        updateCementBrand(request_data,otp);
                                    }else if(type ==23){ //Update AdMixture Brand
                                        
                                        updateAdmixtureBrand(request_data,otp);
                                    }else if(type ==24){ //Update Pump category
                                        
                                        updatePumpCategoryDetails(request_data,otp);
                                    }else if(type == 25){ //Update Admixture Type category
                                        
                                        updateAdmixtureTypes(request_data,otp);
                                    }else if(type == 26){ //Update Cement grade category
                                        
                                        updateCementGrade(request_data,otp);
                                    }else if(type == 27){ //Update Coupon
                                        
                                        editCoupon(request_data,otp);
                                    }else if(type == 28){ //Active-inActive Coupon
                                        
                                        activeInactiveCoupon(csrf,request_data);
                                    }else if(type == 29){ //Delete Coupon
                                        
                                        deleteCoupon(csrf,request_data,otp);
                                    }else if(type == 30){ //Delete Coupon
                                        
                                        editAggregatesource(request_data,otp);
                                    }else if(type == 31){ //Delete Coupon
                                        
                                        deleteAggregatesource(request_data,csrf,otp);
                                    }else if(type == 32){ //Delete Coupon
                                        
                                        editFlyashsource(request_data,otp);
                                    }else if(type == 33){ //Delete Coupon
                                        
                                        deleteFlyashsource(request_data,csrf,otp);
                                    }else if(type == 34){ //Delete Coupon
                                        
                                        editSandsource(request_data,otp);
                                    }else if(type == 35){ //Delete Coupon
                                        
                                        deleteSandsource(request_data,csrf,otp);
                                    }else if(type == 36){ //Delete Coupon
                                        
                                        updatePaymentMethod(request_data,otp);
                                    }else if(type == 37){ //Delete Coupon
                                        
                                        supplierVerifyByAdmin(request_data,otp);
                                    }else if(type == 38){ //Delete Coupon
                                        
                                        supplierBlockUnblockedByAdmin(request_data,otp);
                                    }else if(type == 39){ //Delete Coupon
                                        
                                        supplierRejectedByAdmin(request_data,otp);
                                    }else if(type == 40){ //Delete Coupon
                                        
                                        supplierPlantRejectedByAdmin(request_data,otp);
                                    }else if(type == 41){ //Delete Coupon
                                        
                                        supplierPlantVerifyByAdmin(request_data,otp);
                                    }else if(type == 42){ //Delete Coupon
                                        
                                        supplierPlantBlockUnblockedByAdmin(request_data,otp);
                                    }else if(type == 43){ //Delete Coupon
                                        updateStateDetails(request_data,otp);
                                        
                                    }else if(type == 44){ //Delete Coupon
                                        updateCityDetails(request_data,otp);
                                        
                                    }else if(type == 45){ //Delete Coupon
                                        deleteCity(final_data,csrf,otp);
                                        
                                    }else if(type == 46){ //Delete Coupon
                                        
                                        editSandZone(request_data,otp);
                                    }else if(type == 47){ //Delete Coupon
                                        
                                        deleteSandZone(request_data,csrf,otp);
                                    }
                                }else{
                                    $("#otp_error").html("Please enter 6 digits OTP");    
                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                    otpDialog(OTP_dialog);
                                }
                            }else{
                                $("#otp_error").html("OTP should be in number");
                                // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                otpDialog(OTP_dialog);
                            }
                        }else{
                            $("#otp_error").html("Please enter OTP");
                            // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                            // $("#otpCancel").unbind().one("click", fClose);
                            otpDialog(OTP_dialog);
                        }

                    };

                    otpDialog(OTP_dialog);
                
            }else{ //Error
                showToast(""+data["error"]["message"],"Error");
                if(event_type == "update_password"){
                    showErrorMsgInForm("update_password_form",data["error"]["message"]);
                }else if(event_type == "edit_TM_category"){
                    showErrorMsgInForm("TM_add_cat_form",data["error"]["message"]);
                }else if(event_type == "edit_TM_subcategory"){
                    showErrorMsgInForm("TM_add_sub_cat_form",data["error"]["message"]);
                }else if(event_type == "edit_agg_category"){
                    showErrorMsgInForm("add_product_cat_form",data["error"]["message"]);
                }else if(event_type == "edit_agg_sub_category"){
                    showErrorMsgInForm("add_product_sub_cat_form",data["error"]["message"]);
                }else if(event_type == "edit_cg"){
                    showErrorMsgInForm("add_grade_form",data["error"]["message"]);
                }else if(event_type == "edit_cement_brand"){
                    showErrorMsgInForm("cement_brand_form",data["error"]["message"]);
                }else if(event_type == "edit_admix_brand"){
                    showErrorMsgInForm("admixture_brand_form",data["error"]["message"]);
                }else if(event_type == "edit_pump_cat"){
                    showErrorMsgInForm("pump_add_cat_form",data["error"]["message"]);
                }else if(event_type == "edit_admix_cat"){
                    showErrorMsgInForm("admixture_type_form",data["error"]["message"]);
                }else if(event_type == "edit_cement_grade"){
                    showErrorMsgInForm("cement_grade_form",data["error"]["message"]);
                }else if(event_type == "edit_agg_source"){
                    showErrorMsgInForm("add_aggregate_source_form",data["error"]["message"]);
                }else if(event_type == "edit_flyAsh_source"){
                    showErrorMsgInForm("add_flyash_source_form",data["error"]["message"]);
                }else if(event_type == "edit_sand_source"){
                    showErrorMsgInForm("add_sand_source_form",data["error"]["message"]);
                }else if(event_type == "edit_state"){
                    showErrorMsgInForm("add_states_form",data["error"]["message"]);
                }else if(event_type == "edit_city"){
                    showErrorMsgInForm("add_city_form",data["error"]["message"]);
                }else{
                    showToast(""+data["error"]["message"],"Error");
                }
                
            }            
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });

}

function requestOTPforEditProfile(type,request_data,is_old_mobile,is_old_email,is_new_mobile,is_new_email,is_edit,is_resend){

    var param = "";
    var value = "";
    var type = "";
    var final_data = {};

    if(is_old_mobile == 1){
        param = "old_mobile_no";
        value = request_data;
        type = "mobile";
        final_data = {
            "old_mobile_no" : value
        }

        $("#mobile_type").val("old_mobile");
    }
    
    if(is_old_email == 1){
        param = "old_email";
        value = request_data;
        type = "email";

        final_data = {
            "old_email" : value
        }

        $("#email_type").val("old_email");
    }
    
    if(is_new_mobile == 1){
        param = "new_mobile_no";
        value = request_data;
        type = "mobile";

        final_data = {
            "new_mobile_no" : value
        }

        $("#mobile_type").val("new_mobile");
    }
    
    if(is_new_email == 1){
        param = "new_email";
        value = request_data;
        type = "email";

        final_data = {
            "new_email" : value
        }

        $("#email_type").val("new_email");
    }

    $.ajax({
        type: "GET", 
        url: BASE_URL+"/admin_user/request_otp_for_edit_profile", 
        data: final_data,
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            
            $("label.error").remove();
            $(".form-control").removeClass("error");
            if(data["status"] == 200){  //Success
                $("input[name='otp_code_1']").val("");
                // var OTP_mob_number = data["data"]["mobile_number"];
                $("#update_mobile_otp_confirm_message").html("Enter the 6 digits OTP sent on given "+type+" "+value);
                $("#update_email_otp_confirm_message").html("Enter the 6 digits OTP sent on given "+type+" "+value);
                // console.log("OTP_mob_number:"+OTP_mob_number);
                
                showToast("OTP Sent successfully" ,"Success");
                $("#time_update_mobile").html("02:00");
                $("#time_update_email").html("02:00");
                var fiveMinutes = 60 * 2;
                
                display = $('#time_update_mobile');

                if(param == "old_email" || param == "new_email"){
                    display = $('#time_update_email');
                }

                if(param == "old_mobile_no" || param == "new_mobile_no"){
                    startTimer(fiveMinutes, display, ""+$("#mobile_type").val());
                }

                if(param == "old_email" || param == "new_email"){
                    startTimer(fiveMinutes, display, ""+$("#email_type").val());
                }

                 // Active In Active User                    

                 if(is_resend == 0){
                    $("#new_mobile_no_popup").modal("hide");
                    $("#new_email_no_popup").modal("hide");
                    $("#plant_mobile_otp").val('');
                    if(param == "old_mobile_no" || param == "new_mobile_no"){
                        $("#update_new_mobile_no").val('');
                        $("#update_mobile_profile").modal("show");
                    }
                    
                    if(param == "old_email" || param == "new_email"){
                        $("#update_new_email").val('');
                        $("#update_email_profile").modal("show");
                    }
                }
                
                    
                
            }else{ //Error
                if(data["error"]["message"] != undefined){
                    if(param == "old_mobile_no" || param == "new_mobile_no"){
                        showErrorMsgInForm("new_mobile_form",data["error"]["message"]);
                    }
                    
                    if(param == "old_email" || param == "new_email"){
                        showErrorMsgInForm("new_email_form",data["error"]["message"]);
                    }
                    // showToast(""+data["error"]["message"],"Error");
                }else{
                    showToast("Something went wrong","Error");
                }
            }            
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });

}


function activeInactive(csrf,data,otp){

    var active_inactive = 1;
    if(active_inactive_data["is_active"] == 1)
    {
        active_inactive = 0;
       
    }
    
    if(active_inactive_data["is_active"] == 0)
    {
        active_inactive = 1;
       
    }
    active_inactive_data["authentication_code"] = otp;
    active_inactive_data["is_active"] = active_inactive;

    // data = JSON.parse(data);
    // console.log(data["is_active"]);
    // if(data["is_active"] == "1"){
    //     data["is_active"] = 0;
    // }
    
    // if(data["is_active"] == "0"){
    //     data["is_active"] = 1;
    // }

    // console.log(data);
    // console.log(active_inactive_data);
    $.ajax({
        type: "POST",
        url: BASE_URL+"/admin_user/active_inactive", 
        data: {"_token": ""+csrf,"data":active_inactive_data},
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            if(data["status"] == 200){  //Success
                $('#add-user-popup').modal('hide');                
                showToast("User status successsfully updated to "+ active_inactive ,"Success");
                reload();
            }else{ //Error
                showToast(""+data["error"]["message"],"Error");
                $("#otpOk").unbind().one('click', OTP_dialog);
            }             
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });
}
// var add_edit_user_form_html = "";
function showAdminDialog(data){
    data = JSON.parse(data);
    console.log(data);
    // add_edit_user_form_html = $("#add_admin_user").html();
    resetEditForm('add_user_form');

    $("#admin_dialog_popup_title").html("Edit User");
    $("#user_id").val(data["_id"]);
    $("#admin_user_name").val(data["full_name"]);

    $("#admin_email").val(data["email"]);
    $("#admin_email").attr("disabled",true);

    $("#mobileno").val(data["mobile_number"]);
    $("#mobileno").attr("disabled",true);

    $("#admin_user_role").val(data["admin_type"]);
    $("#admin_user_role").attr("disabled",true);

    $("#admin_user_password").val("");
    $("#admin_user_password").removeAttr("required");

    $("#add_user_form_type").val("edit");



    $("#add-user-popup").modal("show");      

}

function showEditPmKmDialog(data){
    data = JSON.parse(data);

    // add_edit_user_form_html = $("#add_admin_user").html();
    resetEditForm('vehical_pm_km_form');

    $("#vehicle_pmkm_state_id").html('<option value="">'+data["state_name"]+'</option>');
    $("#vehicle_pmkm_state_id").attr('disabled','disabled');
    
    $("#pmkm_region_type").val(data["region_type"]);

    $("#per_metric_ton_per_km_rate").val(data["per_metric_ton_per_km_rate"]);   

    $("#vehical_pm_km_form_type").val("edit");

    var title = $('#vehical_pm_km_title').html();
    title = title.replace('Add','Edit');
    $("#vehical_pm_km_title").html(title);
    $("#vehical_pm_km_id").val(data["_id"]);


    $("#add-vehical-pm-km").modal("show");      

}

function updateAdminUserDetails(auth_code){
    // console.log(add_edit_user_form_html);
    $("#admin_edit_form_authentication_code").val(auth_code);

    var form_values = $("#add_admin_user").serialize();

    responsehandler("admin/admin_user/update", 
                    "POST", 
                    form_values, 
                    "#add-user-popup",
                    "Admin user updated successfully",
                    1,
                    "add_user_form"
                );

    // $.ajax({
    //     type: "POST",
    //     url: BASE_URL+"/admin_user/update", 
    //     data: form_values,
    //     beforeSend: function(){
    //         $("#overlay").fadeIn(300);
    //     },
    //     success: function(data){
    //         if(data["status"] == 200){  //Success                             
    //             showToast("Admin user updated successfully","Success");
    //             reload();
    //         }else{ //Error
    //             showToast(""+data["error"]["message"],"Error");
    //         }                
            
    //     },
    //     complete: function(){
    //         $("#overlay").fadeOut(300);
    //     }
    // });

}

function deleteAdminUser(csrf,otp,request_data){
    
    var data = JSON.parse(request_data);
    
    $.ajax({
        type: "POST",
        url: BASE_URL+"/admin_user/delete", 
        data: {"_token":csrf,"authentication_code":otp,"_id":data["_id"]},
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            if(data["status"] == 200){  //Success                             
                showToast("Admin user deleted successfully","Success");
                reload();
            }else{ //Error
                showToast(""+data["error"]["message"],"Error");
                $("#otpOk").unbind().one('click', OTP_dialog);
            }                
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });


}



// VEHICLE MODULE START...........................................


function addVehiclePMKM(form_values){
    // console.log(form_values);
    
    responsehandler("admin/vehicle_pm_km_rate_add", 
                    "POST", 
                    form_values, 
                    "#add-vehical-pm-km",
                    "Vehicle PM par KM price added successfully",
                    0,
                    "vehical_pm_km_form"
                );

}

function editVehiclePMKM(form_values){
    // console.log(form_values);
    
    responsehandler("admin/vehicle_pm_km_rate_edit", 
                    "POST", 
                    form_values, 
                    "#add-vehical-pm-km",
                    "Vehicle PM par KM price updated successfully",
                    0,
                    "vehical_pm_km_form"
                );

}


function addTMCategory(form_values){
    // console.log(form_values);
    
    responsehandler("TM_category/add", 
                    "POST", 
                    form_values, 
                    "#add-TM-category",
                    "TM category added successfully",
                    0,
                    "TM_add_cat_form"
                );

}


function showEditTMCategoryDialog(data){
    data = JSON.parse(data);

    $("#add_TM_category_title").html("Edit Vehical Category");
    
    $("#TM_add_cat_form_type").val("edit");
    $("#TM_category_name").val(data["category_name"]);    
    $("#cat_id").val(data["_id"]);

    resetEditForm('TM_add_cat_form');
    
    $("#add-TM-category").modal("show");

}

function updateTMCategoryDetails(request_data, auth_code){
    request_data = request_data + "&authentication_code="+auth_code;
    console.log(request_data);
    responsehandler("TM_category/update", 
                    "POST", 
                    request_data, 
                    "#add-TM-category",
                    "TM category updated successfully",
                    1
                );

}


function addTMSubCategory(form_values){
    // console.log(form_values);
    
    responsehandler("TM_sub_category/add", 
                    "POST", 
                    form_values, 
                    "#TM-sub-category",
                    "TM sub category added successfully",
                    0,
                    "TM_add_sub_cat_form"
                );

}

function showEditTMSubCategoryDialog(data){
    data = JSON.parse(data);
    // console.log(data);
    resetEditForm('TM_add_sub_cat_form');

    $("#TM_sub_category_title").html("Edit TM Sub Category");

    $("#TM_add_sub_cat_form_type").val("edit");
    $("#sub_category_name").val(data["sub_category_name"]);    
    $("#TM_capacity").val(data["load_capacity"]);      
    $("#weight_unit_code").val(data["weight_unit_code"]);    
    $("#weight_unit").val(data["weight_unit"]);    
    $("#TM_sub_cat_id").val(data["_id"]);    
    $("#TM_cat_id").val(data["TM_category_id"]);
    // $("#vehicle_cat_id").html("<option>"+data["vehicle_category"]["category_name"]+"</option>");
    $("#TM_cat_id").attr('disabled','true');
   


    $("#TM-sub-category").modal("show");

}

function updateTMSubCatDetails(request_data,auth_code){
    request_data = request_data + "&authentication_code="+auth_code;
    responsehandler("TM_sub_category/update", 
                    "POST", 
                    request_data, 
                    "#TM-sub-category",
                    "TM sub category updated successfully",
                    1
                );

}


function addPumpCategory(form_values){
    // console.log(form_values);
    
    responsehandler("pump_category/add", 
                    "POST", 
                    form_values, 
                    "#add-pump-category",
                    "Concrete Pump category added successfully",
                    0,
                    "pump_add_cat_form"
                );

}


function showEditPumpCategoryDialog(data){
    data = JSON.parse(data);

    $("#add_pump_category_title").html("Edit Concrete Pump Category");
    
    $("#pump_add_cat_form_type").val("edit");
    $("#pump_category_name").val(data["category_name"]);    
    $("#cat_id").val(data["_id"]);

    resetEditForm('pump_add_cat_form');
    
    $("#add-pump-category").modal("show");

}

function updatePumpCategoryDetails(request_data, auth_code){
    request_data = request_data + "&authentication_code="+auth_code;
    console.log(request_data);
    responsehandler("pump_category/update", 
                    "POST", 
                    request_data, 
                    "#add-pump-category",
                    "Concrete Pump category updated successfully",
                    1
                );

}


function viewMap(lattitude,longitude){
    console.log("test");
    var iframe = '<iframe src="https://maps.google.com/maps?q='+lattitude+', '+longitude+'&z=15&output=embed" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>';
    $('#map_iframe_div').html(iframe);
    $('#view-map-details').modal('show');

}
// VEHICLE MODULE OVER...........................................

// REGION MODULE START...........................................

function addRegionCountry(form_values){

    responsehandler("region_country/add", 
                    "POST", 
                    form_values, 
                    "#country-popup",
                    "Country added successfully",
                    0,
                    "add_country_form"
                );

}

function showEditCountryDialog(data){
    data = JSON.parse(data);

    resetEditForm('add_country_form');

    $("#country_popup_title").html("Edit Country");

    $("#country_id").val(data["country_id"]);
    $("#country_id").attr("disabled","disabled");

    $("#country_name").val(data["country_name"]);
    $("#country_name").trigger("chosen:updated");

    $("#country_code").val(data["country_code"]);
    $("#country_original_id").val(data["country_id"]);

    $("#add_country_form_type").val("edit");    

    $("#country-popup").modal("show");
}

function updateCountryDetails(form_values){

    responsehandler("region_country/update", 
                    "POST", 
                    form_values, 
                    "#country-popup",
                    "Country updated successfully",
                    0,
                    "add_country_form"
                );

}

function deleteRecord(type,data,csrf){

    if(type == 6){
        deleteCountry(data,csrf);
    }else if(type == 7){
        deleteState(data,csrf);
    }else if(type == 8){
        deleteCity(data,csrf);
    }else if(type == 15){
        deleteAttachment(data,csrf);
    }else if(type == 21){
        deleteProductsource(data,csrf);
    }

}

function deleteCountry(data,csrf){

    responsehandler("region_country/delete", 
                    "POST", 
                    {"_token":csrf,"_id":data["country_id"]}, 
                    "#country-popup",
                    "Country deleted successfully"
                );

}


var country_name_states_selectID = 'country_name_states';
var state_name_city_selectID = 'state_name_city';
var state_name_city_filter_selectID = 'state_name_city_filter';
var typingTimer;                //timer identifier
var doneTypingInterval = 2000;  //time in ms (2 seconds)

function searchCountryState (url,selectedID) {
    
    var inputData = $('#' + selectedID + '_chosen .chosen-search input').val();  //get input data

    $('#' + selectedID + '_chosen .no-results').html('Getting Data = "'+inputData+'"');
    var request_data = '';
    if(url == 'region_states/getCountries'){
        request_data = {"country_name": inputData};
    }else if(url == 'region_cities/getStates'){
        request_data = {"state_name": inputData};
    }else if(url == 'product_sub_category/getCategory'){
        request_data = {"search": inputData};
    }else if(url == 'product_review/getSubCat'){
        var cat_id = $('select[name=categoryId]').val();
        if(cat_id == undefined || cat_id == ''){
            $('#' + selectedID + '_chosen .no-results').html('Please first select category');
        }
        request_data = {"sub_cat_name": inputData,"categoryId":cat_id};
    }else if(url == 'product_review/getProducts'){
        request_data = {"search": inputData};
    }else if(url == 'product_review/getUsers'){
        request_data = {"name": inputData};
    } else if(url == 'support_ticket/getSuppliers'){
        request_data = {"name": inputData};
    }else if(url == 'support_ticket/getBuyers'){
        request_data = {"name": inputData};
    }else if(url == 'support_ticket/getLogistics'){
        request_data = {"name": inputData};
    }else if(url == 'region_cities/getCities'){
        request_data = {"city_name": inputData};
    }         

    customResponseHandler(
        url, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess

            // data = JSON.parse(data);
            var html = "<option></option>";
        
            $.each(data,function(key,value){
                
                if(url == 'region_states/getCountries'){
                    html += '<option value="'+value["country_name"]+'_'+value['country_id']+'_'+value['country_code']+'">'+value["country_name"]+'</option>';
                }else if(url == 'region_cities/getStates'){
                    html += '<option value="'+value["_id"]+'">'+value["state_name"]+'</option>';
                }else if(url == 'product_sub_category/getCategory'){
                    html += '<option value="'+value["_id"]+'">'+value["category_name"]+'</option>';
                }else if(url == 'product_review/getSubCat'){
                    html += '<option value="'+value["_id"]+'">'+value["sub_category_name"]+'</option>';
                }else if(url == 'product_review/getProducts'){
                    html += '<option value="'+value["_id"]+'">'+value["name"]+'</option>';
                }else if(url == 'product_review/getUsers'){
                    html += '<option value="'+value["_id"]+'">'+value["full_name"]+'</option>';
                }else if(url == 'support_ticket/getSuppliers'){
                    html += '<option value="'+value["_id"]+'">'+value["full_name"]+'</option>';
                }else if(url == 'support_ticket/getBuyers'){
                    html += '<option value="'+value["_id"]+'">'+value["full_name"]+'</option>';
                }else if(url == 'support_ticket/getLogistics'){
                    html += '<option value="'+value["_id"]+'">'+value["full_name"]+'</option>';
                }else if(url == 'region_cities/getCities'){
                    html += '<option value="'+value["_id"]+'">'+value["city_name"]+'</option>';
                }                
                
            });
            
            $('#' + selectedID ).html(html);

            // Update chosen again after append <option>
            $('#' + selectedID ).trigger("chosen:updated");
            $('#' + selectedID + '_chosen .chosen-search input').val(inputData);

        }
    );

}

function addRegionState(form_values){

    responsehandler("region_states/add", 
                    "POST", 
                    form_values, 
                    "#state-popup",
                    "State added successfully",
                    0,
                    "add_states_form"
                );

}


function showEditStateDialog(data){

    data = JSON.parse(data);
    
    resetEditForm('add_states_form');

    $("#state_popup_title").html("Edit State");

    $("#state_country_id").val(data["country_id"]);
    $("#state_name").val(data["state_name"]);
    $("#state_country_name").val(data["country_name"]);
    

    $("#country_name_states_1").val(data["country_name"]+'_'+data['country_id']+'_'+data['country_code']);
    $("#country_name_states").val(data["country_name"]+'_'+data['country_id']+'_'+data['country_code']);
    $("#country_name_states").trigger("chosen:updated");
    var selectedCountry = $("#country_name_states").children("option:selected").val();
    

    if(selectedCountry == undefined){ // If country not listed in dropdown
        console.log("selectedCountry.."+selectedCountry);
        $("#country_name_states").append('<option value="'+data["country_name"]+'_'+data['country_id']+'_'+data['country_code']+'">'+data["country_name"]+'</option>');
        $("#country_name_states").trigger("chosen:updated");

        $("#country_name_states").val(data["country_name"]+'_'+data['country_id']+'_'+data['country_code']);
        $("#country_name_states").trigger("chosen:updated");
    }

    $("#state_country_code").val(data["country_code"]);
    $("#state_original_id").val(data["_id"]);

    $("#add_states_form_type").val("edit");    

    $("#state-popup").modal("show");
}

function updateStateDetails(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("region_states/update", 
                    "POST", 
                    request_data, 
                    "#state-popup",
                    "State updated successfully",
                    1,
                    "add_states_form"
                );

}

function deleteState(data,csrf,otp){
    data = JSON.parse(data);
    var request_data = {
        '_id' : data["_id"],
        '_token': csrf,
        'authentication_code': otp,
    }

    responsehandler("region_states/delete", 
                    "POST", 
                    request_data, 
                    "#state-popup",
                    "State deleted successfully",
                    1
                );

}

function addRegionCity(form_values){

    responsehandler("region_cities/add", 
                    "POST", 
                    form_values, 
                    "#city-popup",
                    "City added successfully",
                    0,
                    "add_city_form"
                );

}


function showEditCityDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_city_form');

    $("#city_popup_title").html("Edit City");

    $("#city_name").val(data["city_name"]);    
    // console.log(data["state_id"]);
    $("#state_name_city_1").val(data["state_id"]);
    $("#state_name_city").val(data["state_id"]);
    $("#state_name_city").attr("disabled","disabled");
    $("#state_name_city").trigger("chosen:updated");
    $("#state_name_city_1").attr("disabled","disabled");
    $("#state_name_city_1").trigger("chosen:updated");
    var selectedState = $("#state_name_city").children("option:selected").val();
    var selectedState = $("#state_name_city_1").children("option:selected").val();
    

    if(selectedState == undefined){ // If country not listed in dropdown
        console.log("selectedState.."+selectedState);
        $("#state_name_city").append('<option value="'+data["state_id"]+'">'+data["state_name"]+'</option>');
        $("#state_name_city").trigger("chosen:updated");

        $("#state_name_city").val(data["state_id"]);
        $("#state_name_city").trigger("chosen:updated");
        
        $("#state_name_city_1").append('<option value="'+data["state_id"]+'">'+data["state_name"]+'</option>');
        $("#state_name_city_1").trigger("chosen:updated");

        $("#state_name_city_1").val(data["state_id"]);
        $("#state_name_city_1").trigger("chosen:updated");
    }

    $("#city_original_id").val(data["_id"]);

    $("#add_city_form_type").val("edit"); 
    
    
    $("#us2_lat").val(data["location"]["coordinates"][1]); 
    $("#us2_lon").val(data["location"]["coordinates"][0]); 

    $('#us2').locationpicker("location", {latitude: data["location"]["coordinates"][1], longitude: data["location"]["coordinates"][0]});

    $("#city-popup").modal("show");
}

// $("#city_name").keyup(function(){

//     $("#us2_address").val($("#city_name").val());    

// });

function updateCityDetails(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("region_cities/update", 
                    "POST", 
                    request_data, 
                    "#city-popup",
                    "City updated successfully",
                    1,
                    "add_city_form"
                );

}

function deleteCity(data,csrf,otp){
    data = JSON.parse(data);
    var request_data = {
        '_id' : data["_id"],
        '_token': csrf,
        'authentication_code': otp,
    }


    responsehandler("region_cities/delete", 
                    "POST", 
                    request_data, 
                    "#city-popup",
                    "City deleted successfully",
                    1
                );

}

// REGION MODULE OVER...........................................

function rejectPopUp(supplier_id){

    resetForm('supplier_block_form','supplier-block-popup');

    $("#reject_supplier_id").val(supplier_id);

    $("#supplier-block-popup").modal("show");
}

function rejectPlantPopUp(address_id,supplier_id){

    resetForm('supplier_plant_block_form','supplier-plant-block-popup');

    $("#reject_supplier_plant_id").val(address_id);
    $("#reject_vendor_id").val(supplier_id);

    $("#sub_vender_popup").modal("hide");
    $("#supplier-plant-block-popup").modal("show");
}

// Product MODULE START...........................................
var select_product_category_in_subcat_list = 'product_category_subcategory_list';
var select_product_category_in_subcat_list_add_form = 'product_category_subcategory_list_add_form';
var select_product_subcat_list = 'product__sub_category_list';
var select_review_product_list = 'review_product_list';
var select_review_user_list = 'review_user_list';
$(document).ready(function(){

    $('input[name=product_img_text]').change(function(e){
        $in=$(this);
        $('#product_img_text').val($in.val());
      });

    
      $("form[name='supplier_block_form']").validate({

        rules:{
            reject_reason: {
                required: true
            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "product_img" ) {
              error.insertAfter(".file-browse");
            } else {
              error.insertAfter(element);
            }
          },
          
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="supplier_block_form"] p.error').remove();

            // var form_type = $("form[name='add_grade_form'] #add_grade_form_type").val();
            var form_values = $("form[name='supplier_block_form']").serialize();
            // var form_values = new FormData(form);
            // console.log(form_values);
            // console.log("form_type ..."+form_type );

            // if(form_type == 'add'){
            //     addConcreteGrade(form_values);
            // }
            // else if(form_type == 'edit'){
                $('#supplier-block-popup').modal('hide');
                requestOTP(39,form_values,"","vendor_rejection");                
            // }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="supplier_block_form"] p.error').remove();
        }

    });
    
    $("form[name='supplier_plant_block_form']").validate({

        rules:{
            reject_reason: {
                required: true
            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "product_img" ) {
              error.insertAfter(".file-browse");
            } else {
              error.insertAfter(element);
            }
          },
          
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="supplier_plant_block_form"] p.error').remove();

            // var form_type = $("form[name='add_grade_form'] #add_grade_form_type").val();
            var form_values = $("form[name='supplier_plant_block_form']").serialize();
            // var form_values = new FormData(form);
            // console.log(form_values);
            // console.log("form_type ..."+form_type );

            // if(form_type == 'add'){
            //     addConcreteGrade(form_values);
            // }
            // else if(form_type == 'edit'){
                $('#supplier-plant-block-popup').modal('hide');
                requestOTP(40,form_values,"","plant_rejection");                
            // }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="supplier_block_form"] p.error').remove();
        }

    });
      
      $("form[name='add_grade_form']").validate({

        rules:{
            grade_name: {
                required: true,
                minlength: 3,
                maxlength: 150,
                noSpace: true

            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "product_img" ) {
              error.insertAfter(".file-browse");
            } else {
              error.insertAfter(element);
            }
          },
          
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_grade_form"] p.error').remove();

            var form_type = $("form[name='add_grade_form'] #add_grade_form_type").val();
            var form_values = $("form[name='add_grade_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addConcreteGrade(form_values);
            }
            else if(form_type == 'edit'){
                // $('#concrete-grade-popup').modal('hide');
                requestOTP(21,form_values,"","edit_cg");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_grade_form"] p.error').remove();
        }

    });

    
    $("form[name='cement_brand_form']").validate({

        rules:{
            brand_name: {
                required: true,
                minlength: 3,
                maxlength: 150,
                noSpace: true

            },
            brand_img: {
                
                required: function() {
                    return ($("form[name='cement_brand_form'] #cement_brand_form_type").val() == 'add');
                  },
                  extension: "jpg|jpeg"
                // required: $("form[name='cement_brand_form'] #cement_brand_form_type").val() == 'add' ? true : false

            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "brand_img" ) {
              error.insertAfter(".file-browse");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="cement_brand_form"] p.error').remove();

            var form_type = $("form[name='cement_brand_form'] #cement_brand_form_type").val();
            
            var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addCementBrand(form_values);
            }
            else if(form_type == 'edit'){
                // $('#cement-brand-popup').modal('hide');
                var form_values = $("form[name='cement_brand_form']").serialize();
                requestOTP(22,form_values,"","edit_cement_brand");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="cement_brand_form"] p.error').remove();
        }

    });
    
    
    $("form[name='admixture_brand_form']").validate({

        rules:{
            brand_name: {
                required: true,
                minlength: 3,
                maxlength: 150,
                noSpace: true

            },
            brand_img: {
                
                required: function() {
                    return ($("form[name='admixture_brand_form'] #admixture_brand_form_type").val() == 'add');
                  },
                  extension: "jpg|jpeg"
                // required: $("form[name='admixture_brand_form'] #admixture_brand_form_type").val() == 'add' ? true : false

            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "brand_img" ) {
              error.insertAfter(".file-browse");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="admixture_brand_form"] p.error').remove();

            var form_type = $("form[name='admixture_brand_form'] #admixture_brand_form_type").val();
            
            var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addAdmixtureBrand(form_values);
            }
            else if(form_type == 'edit'){
                // $('#admixture-brand-popup').modal('hide');
                var form_values = $("form[name='admixture_brand_form']").serialize();
                requestOTP(23,form_values,"","edit_admix_brand");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="admixture_brand_form"] p.error').remove();
        }

    });

   
    

    $("form[name='admixture_type_form']").validate({

        rules:{
            category_name: {
                required: true,
                minlength: 3,
                maxlength: 150,
                noSpace: true

            },
            brand_id: {
                required: true

            },
            admixture_type: {
                required: true

            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "brand_img" ) {
              error.insertAfter(".file-browse");
            }else if (element.attr("name") == "brand_id" ) {
              error.insertAfter("#brand_id_div");
            }else if (element.attr("name") == "admixture_type" ) {
              error.insertAfter("#admixture_type_div");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="admixture_type_form"] p.error').remove();

            var form_type = $("form[name='admixture_type_form'] #admixture_type_form_type").val();
            
            var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addAdmixtureTypes(form_values);
            }
            else if(form_type == 'edit'){
                // $('#admixture-type-popup').modal('hide');
                var form_values = $("form[name='admixture_type_form']").serialize();
                requestOTP(25,form_values,"","edit_admix_cat");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="admixture_type_form"] p.error').remove();
        }

    });
    
    
    $("form[name='cement_grade_form']").validate({

        rules:{
            cement_grade_name: {
                required: true,
                minlength: 3,
                maxlength: 150,
                noSpace: true

            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "brand_img" ) {
              error.insertAfter(".file-browse");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="cement_grade_form"] p.error').remove();

            var form_type = $("form[name='cement_grade_form'] #cement_grade_form_type").val();
            
            var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addCementGrade(form_values);
            }
            else if(form_type == 'edit'){
                // $('#cement-grade-popup').modal('hide');
                var form_values = $("form[name='cement_grade_form']").serialize();
                requestOTP(26,form_values,"","edit_cement_grade");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="cement_grade_form"] p.error').remove();
        }

    });
    
    $("form[name='payment_method_form']").validate({

        rules:{
            payment_method_name: {
                required: true,
                minlength: 3,
                maxlength: 150,
                noSpace: true

            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            
              error.insertAfter(element);
           
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="payment_method_form"] p.error').remove();

            var form_type = $("form[name='payment_method_form'] #payment_method_form_type").val();
            // var form_values = $("form[name='payment_method_form']").serialize();
            var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addPaymentMethod(form_values);
            }
            else if(form_type == 'edit'){
                $('#payment-method-popup').modal('hide');
                requestOTP(36,form_values,"","");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="payment_method_form"] p.error').remove();
        }

    });

    
      $("form[name='add_product_cat_form']").validate({

        rules:{
            category_name: {
                required: true,
                minlength: 3,
                maxlength: 150,
                noSpace: true

            },
            product_img: {
                
                required: function() {
                    return ($("form[name='add_product_cat_form'] #add_product_cat_form_type").val() == 'add');
                  },
                  extension: "jpg|jpeg"
                // required: $("form[name='add_product_cat_form'] #add_product_cat_form_type").val() == 'add' ? true : false

            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "product_img" ) {
              error.insertAfter(".file-browse");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_product_cat_form"] p.error').remove();

            var form_type = $("form[name='add_product_cat_form'] #add_product_cat_form_type").val();
            
            var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addProductCategory(form_values);
            }
            else if(form_type == 'edit'){
                // $('#product-category-popup').modal('hide');
                var form_values = $("form[name='add_product_cat_form']").serialize();
                requestOTP(8,form_values,"","edit_agg_category");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_product_cat_form"] p.error').remove();
        }

    });
    
    
    
    $("form[name='add_product_sub_cat_form']").validate({

        rules:{
            categoryId: "required",
            sub_category_name: {
                required: true,
                minlength: 3,
                maxlength: 150,
                noSpace: true
            },
            product_sub_cat_image: {
                
                required: function() {
                    return ($("form[name='add_product_sub_cat_form'] #add_product_sub_cat_form_type").val() == 'add');
                  },
                  extension: "jpg|jpeg"
                // required: $("form[name='add_product_sub_cat_form'] #add_product_sub_cat_form_type").val() == 'add' ? true : false
                
            },
            min_quantity: {
                number: true
            },
            gst_slab_id: {
                required: true,
            },
            margin_rate_id: {
                required: true,
            }
        },
        messages:{
            categoryId: "Please select category name",
            sub_category_name: {
                required: "Please add sub category name"
            },
            product_sub_cat_image: {
                extension: "Please select valid file (jpg, jpeg) format"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "product_sub_cat_image" ) {
              error.insertAfter(".file-browse");
            }else if (element.attr("name") == "categoryId" ) {
              error.insertAfter("#category_id_div");
            }else if (element.attr("name") == "gst_slab_id" ) {
              error.insertAfter("#gst_slab_id_div");
            }else if (element.attr("name") == "margin_rate_id" ) {
              error.insertAfter("#margin_rate_id_div");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_product_sub_cat_form"] p.error').remove();

            var form_type = $("form[name='add_product_sub_cat_form'] #add_product_sub_cat_form_type").val();
            
            var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addProductSubCategory(form_values);
            }
            else if(form_type == 'edit'){
                // $('#product-sub-category-popup').modal('hide');
                var form_values = $("form[name='add_product_sub_cat_form']").serialize();
                requestOTP(9,form_values,"","edit_agg_sub_category");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });
    
    
    $("form[name='add_product_form']").validate({

        rules:{
            
            minimum_order:{
                number: true
            },
            unit_price:{
                number: true
            }
        },
        messages:{
            
            minimum_order: 'Minimum Order should be in number only',
            unit_price: 'Unit Price should be in number only'
        },
        submitHandler: function(form) {
            console.log("submited");

            var form_type = $("form[name='add_product_form'] #add_product_form_type").val();
            var form_values = $("form[name='add_product_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                // addProductSubCategory(form_values);
            }
            else if(form_type == 'edit'){
                $('#add-product-list').modal('hide');
                requestOTP(10,form_values,"","");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });
    

    $("form[name='add_aggregate_source_form']").validate({

        rules:{
            
            region_id:{
                required: true
            },
            aggregate_source_name:{
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true
            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "region_id" ) {
              error.insertAfter("#region_id_div");
            }else if (element.attr("name") == "categoryId" ) {
              error.insertAfter("#category_id_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");

            var form_type = $("form[name='add_aggregate_source_form'] #add_aggregate_source_form_type").val();
            var form_values = $("form[name='add_aggregate_source_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addAggregatesource(form_values);
            }
            else if(form_type == 'edit'){
                // $('#aggregate-source-popup').modal('hide');
                
                requestOTP(30,form_values,"","edit_agg_source");
               
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });
    
    
    $("form[name='add_sand_source_form']").validate({

        rules:{
            
            region_id:{
                required: true
            },
            sand_source_name:{
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true
            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "region_id" ) {
              error.insertAfter("#region_id_div");
            }else if (element.attr("name") == "categoryId" ) {
              error.insertAfter("#category_id_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");

            var form_type = $("form[name='add_sand_source_form'] #add_sand_source_form_type").val();
            var form_values = $("form[name='add_sand_source_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addSandsource(form_values);
            }
            else if(form_type == 'edit'){
                // $('#sand-source-popup').modal('hide');
                var form_values = $("form[name='add_sand_source_form']").serialize();
                requestOTP(34,form_values,"","edit_sand_source");
                
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });
    
    $("form[name='add_sand_zone_form']").validate({

        rules:{
            
            zone_name:{
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true
            },
            finess_module_range:{
                required: true,
                minlength: 3,
                maxlength: 64,
            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "region_id" ) {
              error.insertAfter("#region_id_div");
            }else if (element.attr("name") == "categoryId" ) {
              error.insertAfter("#category_id_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");

            var form_type = $("form[name='add_sand_zone_form'] #add_sand_zone_form_type").val();
            var form_values = $("form[name='add_sand_zone_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addSandZone(form_values);
            }
            else if(form_type == 'edit'){
                // $('#sand-source-popup').modal('hide');
                var form_values = $("form[name='add_sand_zone_form']").serialize();
                requestOTP(46,form_values,"","edit_sand_zone");
                
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });
    
    $("form[name='add_flyash_source_form']").validate({

        rules:{
            
            region_id:{
                required: true
            },
            flyash_source_name:{
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true
            }
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "region_id" ) {
              error.insertAfter("#region_id_div");
            }else if (element.attr("name") == "categoryId" ) {
              error.insertAfter("#category_id_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");

            var form_type = $("form[name='add_flyash_source_form'] #add_flyash_source_form_type").val();
            var form_values = $("form[name='add_flyash_source_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addFlyashsource(form_values);
            }
            else if(form_type == 'edit'){
                // $('#flyash-source-popup').modal('hide');
                requestOTP(32,form_values,"","edit_flyAsh_source");
               
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

    
    $("form[name='add_margin_slab_form']").validate({

        rules:{
            slab_title: {
                required: true,
                noSpace: true
            },
            "margin_upto[]":{
                required: true,
                number: true
            },
            "margin_rate[]":{
                required: true,
                number: true
            }
        },
        messages:{
            slab_title: {
            },
            "margin_upto[]": {
                number: 'Margin price should be in number only'
            },
            "margin_rate[]": {
                number: 'Margin rate should be in number only'
            }
        },
        submitHandler: function(form) {
            console.log("submited");

            var form_type = $("form[name='add_margin_slab_form'] #add_margin_slab_form_type").val();
            var form_values = $("form[name='add_margin_slab_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type);

            if(form_type == 'add'){
                // $('#add-margin-slab-popup').modal('hide');
                // requestOTP(12,form_values,"","");
                addMarginSlab(form_values);
            }
            else if(form_type == 'edit'){
                $('#add-margin-slab-popup').modal('hide');
                requestOTP(13,form_values,"","");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });
    
    $("form[name='add_gst_slab_form']").validate({

        rules:{
            title: {
                required: true,
                noSpace: true
            },
            igst_rate:{
                required: true,
                number: true
            },
            sgst_rate:{
                required: true,
                number: true
            },
            cgst_rate:{
                required: true,
                number: true
            },
            hsn_code: {
                noSpace: true
            }
        },
        messages:{
            
            igst_rate: {
                number: 'IGST should be in number only',
            },
            sgst_rate: {
                number: 'SGST rate should be in number only',
            },
            cgst_rate: {
                number: 'CGST rate should be in number only'
            }
        },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_gst_slab_form"] p.error').remove();

            var form_type = $("form[name='add_gst_slab_form'] #add_gst_slab_form_type").val();
            var form_values = $("form[name='add_gst_slab_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addGSTSlab(form_values);
            }
            else if(form_type == 'edit'){
                $('#add-gst-info-popup').modal('hide');
                requestOTP(14,form_values,"","");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_gst_slab_form"] p.error').remove();
        }

    });


    $('#' + select_product_category_in_subcat_list + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + select_product_category_in_subcat_list + '_chosen .no-results').html('Searching = "'+ $('#' + select_product_category_in_subcat_list + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_product_category_in_subcat_list + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'product_sub_category/getCategory',select_product_category_in_subcat_list);  //Set timer back if got value on input

    //   }
  
    });
    
    $('#' + select_product_category_in_subcat_list_add_form + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + select_product_category_in_subcat_list_add_form + '_chosen .no-results').html('Searching = "'+ $('#' + select_product_category_in_subcat_list_add_form + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_product_category_in_subcat_list_add_form + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'product_sub_category/getCategory',select_product_category_in_subcat_list_add_form);  //Set timer back if got value on input

    //   }
  
    });

    $('#' + select_product_subcat_list + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + select_product_subcat_list + '_chosen .no-results').html('Searching = "'+ $('#' + select_product_subcat_list + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_product_subcat_list + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'product_review/getSubCat',select_product_subcat_list);  //Set timer back if got value on input

    //   }
  
    });
    
    $('#' + select_review_product_list + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + select_review_product_list + '_chosen .no-results').html('Searching = "'+ $('#' + select_review_product_list + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_review_product_list + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'product_review/getProducts',select_review_product_list);  //Set timer back if got value on input

    //   }
  
    });
    
    $('#' + select_review_user_list + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + select_review_user_list + '_chosen .no-results').html('Searching = "'+ $('#' + select_review_user_list + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_review_user_list + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'product_review/getUsers',select_review_user_list);  //Set timer back if got value on input

    //   }
  
    });

    

});


function viewProductDesignMixOfGrade(grade_id){

        
    var request_data = {
        "grade_id":grade_id
    }

    customResponseHandler(
        "admin/product/designmix_detail", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // showToast("Quantity updated","Success");

                $("#design_mix_body").html(data["html"]);

                $("#show-design-mix-details").modal("show");

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );


}

function addAggregatesource(form_values){

    responsehandler("admin/add_aggregate_source", 
                    "POST", 
                    form_values, 
                    "#aggregate-source-popup",
                    "Aggregate source added successfully",
                    0,
                    "add_aggregate_source_form"
                );

}

function showEditAggregateSourceDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_aggregate_source_form');    

    $("#aggregate_region_id").val(data["region_id"]);    
    $("#aggregate_region_id").attr('disabled','disabled');

    $("#aggregate_source_name").val(data["aggregate_source_name"]); 
    
    $("#aggregate_source_id").val(data["_id"]);
    
    $("#add_aggregate_source_form_type").val("edit"); 
    
    $("#aggregate-source-popup").modal("show");
}

function editAggregatesource(form_values,otp){

    form_values += form_values+"&authentication_code="+otp;

    responsehandler("admin/edit_aggregate_source", 
                    "POST", 
                    form_values, 
                    "#aggregate-source-popup",
                    "Aggregate source updated successfully",
                    1,
                    "add_aggregate_source_form"
                );

}

function deleteAggregatesource(data,csrf,otp){
    // console.log(data);
    // data = JSON.parse(data);
    var request_data = {
        'source_id' : data["_id"],
        '_token': csrf,
        'authentication_code': otp
    }

    responsehandler("admin/delete_aggregate_source", 
                    "POST", 
                    request_data, 
                    "#aggregate-source-popup",
                    "Aggregate source deleted successfully",
                    1,
                    "add_aggregate_source_form"
                );

}


function addSandsource(form_values){

    responsehandler("admin/add_sand_source", 
                    "POST", 
                    form_values, 
                    "#sand-source-popup",
                    "Sand source added successfully",
                    0,
                    "add_sand_source_form"
                );

}

function showEditSandSourceDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_sand_source_form');    

    $("#sand_region_id").val(data["region_id"]);    
    $("#sand_region_id").attr('disabled','disabled');

    $("#sand_source_name").val(data["sand_source_name"]); 
    
    $("#sand_source_id").val(data["_id"]);
    
    $("#add_sand_source_form_type").val("edit"); 
    
    $("#sand-source-popup").modal("show");
}

function editSandsource(form_values,otp){

    form_values += form_values+"&authentication_code="+otp;

    responsehandler("admin/edit_sand_source", 
                    "POST", 
                    form_values, 
                    "#sand-source-popup",
                    "Sand source updated successfully",
                    1,
                    "add_sand_source_form"
                );

}

function deleteSandsource(data,csrf,otp){
    // console.log(data);
    // data = JSON.parse(data);
    var request_data = {
        'source_id' : data["_id"],
        '_token': csrf,
        'authentication_code': otp
    }

    responsehandler("admin/delete_sand_source", 
                    "POST", 
                    request_data, 
                    "#sand-source-popup",
                    "Sand source deleted successfully",
                    1,
                    "add_sand_source_form"
                );

}

function addSandZone(form_values){

    responsehandler("admin/add_sand_zone", 
                    "POST", 
                    form_values, 
                    "#sand-zone-popup",
                    "Sand zone added successfully",
                    0,
                    "add_sand_zone_form"
                );

}

function showEditSandZoneDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_sand_zone_form');    


    $("#zone_name").val(data["zone_name"]); 
    
    $("#finess_module_range").val(data["finess_module_range"]);
    $("#sand_zone_id").val(data["_id"]);
    
    $("#add_sand_zone_form_type").val("edit"); 
    
    $("#sand-zone-popup").modal("show");
}

function editSandZone(form_values,otp){

    form_values += form_values+"&authentication_code="+otp;

    responsehandler("admin/edit_sand_zone", 
                    "POST", 
                    form_values, 
                    "#sand-zone-popup",
                    "Sand zone updated successfully",
                    1,
                    "add_sand_zone_form"
                );

}

function deleteSandZone(data,csrf,otp){
    // console.log(data);
    // data = JSON.parse(data);
    var request_data = {
        'zone_id' : data["_id"],
        '_token': csrf,
        'authentication_code': otp
    }

    responsehandler("admin/delete_sand_zone", 
                    "POST", 
                    request_data, 
                    "#sand-zone-popup",
                    "Sand zone deleted successfully",
                    1,
                    "add_sand_zone_form"
                );

}

function addFlyashsource(form_values){

    responsehandler("admin/add_flyash_source", 
                    "POST", 
                    form_values, 
                    "#flyash-source-popup",
                    "Flyash source added successfully",
                    0,
                    "add_flyash_source_form"
                );

}

function showEditFlyashSourceDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_flyash_source_form');    

    $("#flyash_region_id").val(data["region_id"]);    
    $("#flyash_region_id").attr('disabled','disabled');

    $("#flyash_source_name").val(data["fly_ash_source_name"]); 
    
    $("#flyash_source_id").val(data["_id"]);
    
    $("#add_flyash_source_form_type").val("edit"); 
    
    $("#flyash-source-popup").modal("show");
}

function editFlyashsource(form_values,otp){

    form_values += form_values+"&authentication_code="+otp;

    responsehandler("admin/edit_flyash_source", 
                    "POST", 
                    form_values, 
                    "#flyash-source-popup",
                    "Flyash source updated successfully",
                    1,
                    "add_flyash_source_form"
                );

}

function deleteFlyashsource(data,csrf,otp){
    // console.log(data);
    // data = JSON.parse(data);
    var request_data = {
        'source_id' : data["_id"],
        '_token': csrf,
        'authentication_code': otp
    }

    responsehandler("admin/delete_flyash_source", 
                    "POST", 
                    request_data, 
                    "#flyash-source-popup",
                    "Flyash source deleted successfully",
                    1,
                    "add_flyash_source_form"
                );

}

function addConcreteGrade(form_values){
    // console.log(data);
    // data = JSON.parse(data);
   

    responsehandler("concrete_grade/add", 
                    "POST", 
                    form_values, 
                    "#concrete-grade-popup",
                    "Concrete Grade Added Successfully",
                    0,
                    "add_grade_form"
                );

}



function showEditConcreteGradeDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_grade_form');

    $("#concrete_grade_popup_title").html("Edit Concrete Grade");

    $("#grade_name").val(data["name"]);    
    console.log(data["state_id"]);
    
    $("#grade_id").val(data["_id"]);
    

    $("#add_grade_form_type").val("edit"); 
    
    $("#concrete-grade-popup").modal("show");
}

function updateConcreteGrade(form_values,otp){
    // console.log(form_values);
    // $('#product_cat_authentication_code').val(otp);

    form_values = form_values + '&authentication_code='+otp;
    console.log(form_values);

    responsehandler("concrete_grade/update", 
                    "POST", 
                    form_values, 
                    "#concrete-grade-popup",
                    "Concrete Grade updated successfully",
                    1,
                    "add_grade_form"
                );

}


function addCementBrand(form_values){

    responsehandlerWithFiles("cement_brand/add", 
                    "POST", 
                    form_values, 
                    "#cement-brand-popup",
                    "Cement Brand added successfully",
                    0,
                    "cement_brand_form"
                );

}


function showEditCementBrandDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('cement_brand_form');

    $("#cement_brand_popup_title").html("Edit Cement Brand");

    $("#brand_name").val(data["name"]);    
    // console.log(data["state_id"]);
    
    $("#brand_id").val(data["_id"]);
    $("#brand_img_text").val(data["image_url"].substring(data["image_url"].lastIndexOf('/')+1,data["image_url"].length));

    $("#cement_brand_form_type").val("edit"); 
    
    $("#cement-brand-popup").modal("show");
}

function updateCementBrand(form_values,otp){
    
    // $('#product_cat_authentication_code').val(otp);
    form_values = new FormData(document.getElementById("cement_brand_form"));
    form_values.append("authentication_code", otp);
    console.log(form_values);

    responsehandlerWithFiles("cement_brand/update", 
                    "POST", 
                    form_values, 
                    "#cement-brand-popup",
                    "Cement Brand updated successfully",
                    1,
                    "cement_brand_form"
                );

}



function addAdmixtureBrand(form_values){

    responsehandlerWithFiles("admixture_brand/add", 
                    "POST", 
                    form_values, 
                    "#admixture-brand-popup",
                    "AdMixture Brand added successfully",
                    0,
                    "admixture_brand_form"
                );

}


function showEditAdmixtureBrandDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('admixture_brand_form');

    $("#cement_brand_popup_title").html("Edit Cement Brand");

    $("#brand_name").val(data["name"]);    
    // console.log(data["state_id"]);
    
    $("#brand_id").val(data["_id"]);
    $("#brand_img_text").val(data["image_url"].substring(data["image_url"].lastIndexOf('/')+1,data["image_url"].length));

    $("#admixture_brand_form_type").val("edit"); 
    
    $("#admixture-brand-popup").modal("show");
}

function updateAdmixtureBrand(form_values,otp){
    
    // $('#product_cat_authentication_code').val(otp);

    form_values = new FormData(document.getElementById("admixture_brand_form"));
    // form_values = new FormData($("form[name='admixture_brand_form']"));
    form_values.append("authentication_code", otp);
    console.log(form_values);

    responsehandlerWithFiles("admixture_brand/update", 
                    "POST", 
                    form_values, 
                    "#admixture-brand-popup",
                    "AdMixture Brand updated successfully",
                    1,
                    "admixture_brand_form"
                );

}



function addAdmixtureTypes(form_values){

    responsehandlerWithFiles("admixture_types/add", 
                    "POST", 
                    form_values, 
                    "#admixture-type-popup",
                    "AdMixture Type added successfully",
                    0,
                    "admixture_type_form"
                );

}


function showEditAdmixtureTypeDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('admixture_type_form');

    $("#admixture_type_popup_title").html("Edit Admixture Type");

    $("#category_name").val(data["category_name"]);    
    // console.log(data["state_id"]);
    
    $("#brand_id").val(data["brand_id"]);
    $("#admixture_type_id").val(data["_id"]);
    $("#admixture_type").val(data["admixture_type"]);

    $("#admixture_type_form_type").val("edit"); 
    
    $("#admixture-type-popup").modal("show");
}

function updateAdmixtureTypes(form_values,otp){
    
    // $('#product_cat_authentication_code').val(otp);
    form_values = new FormData(document.getElementById("admixture_type_form"));
    form_values.append("authentication_code", otp);
    console.log(form_values);

    responsehandlerWithFiles("admixture_types/update", 
                    "POST", 
                    form_values, 
                    "#admixture-type-popup",
                    "AdMixture Type updated successfully",
                    1,
                    "admixture_type_form"
                );

}


function addCementGrade(form_values){

    responsehandlerWithFiles("cement_grade/add", 
                    "POST", 
                    form_values, 
                    "#cement-grade-popup",
                    "Cement Grade added successfully",
                    0,
                    "cement_grade_form"
                );

}


function showEditCementGradeDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('cement_grade_form');

    $("#cement_grade_popup_title").html("Edit Cement Grade");

    $("#cement_grade_name").val(data["name"]);    
    // console.log(data["state_id"]);

    $("#grade_id").val(data["_id"]);

    $("#cement_grade_form_type").val("edit"); 
    
    $("#cement-grade-popup").modal("show");
}

function updateCementGrade(form_values,otp){
    
    // $('#product_cat_authentication_code').val(otp);
    form_values = new FormData(document.getElementById("cement_grade_form"));
    form_values.append("authentication_code", otp);
    console.log(form_values);

    responsehandlerWithFiles("cement_grade/update", 
                    "POST", 
                    form_values, 
                    "#payment-method-popup",
                    "Cement Grade updated successfully",
                    1,
                    "cement_grade_form"
                );

}


function addPaymentMethod(form_values){

    responsehandlerWithFiles("payement_method/add", 
                    "POST", 
                    form_values, 
                    "#payment-method-popup",
                    "Payment Method added successfully",
                    0,
                    "payment_method_form"
                );

}


function showEditPaymentMethodDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('payment_method_form');

    $("#payment_method_popup_title").html("Edit Payment Method");

    $("#payment_method_name").val(data["name"]);    
    // console.log(data["state_id"]);

    $("#pay_id").val(data["_id"]);

    $("#payment_method_form_type").val("edit"); 
    
    $("#payment-method-popup").modal("show");
}

function updatePaymentMethod(form_values,otp){
    
    // $('#product_cat_authentication_code').val(otp);

    form_values.append("authentication_code", otp);
    console.log(form_values);

    responsehandlerWithFiles("payement_method/update", 
                    "POST", 
                    form_values, 
                    "#payment-method-popup",
                    "Payment Method updated successfully",
                    1,
                    "payment_method_form"
                );

}


function addProductCategory(form_values){

    responsehandlerWithFiles("product_category/add", 
                    "POST", 
                    form_values, 
                    "#product-category-popup",
                    "Product category added successfully",
                    0,
                    "add_product_cat_form"
                );

}

function updateProductCategory(form_values,otp){
    console.log(form_values);
    // $('#product_cat_authentication_code').val(otp);

    form_values = new FormData(document.getElementById("add_product_cat_form"));
    // form_values = new FormData($("form[name='admixture_brand_form']"));
    form_values.append("authentication_code", otp);

    responsehandlerWithFiles("product_category/update", 
                    "POST", 
                    form_values, 
                    "#product-category-popup",
                    "Product category updated successfully",
                    1,
                    "add_product_cat_form"
                );

}


function showEditProductCatDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_product_cat_form');

    $("#product_category_popup_title").html("Edit Product Category List");

    $("#product_cat_name").val(data["category_name"]);    
    console.log(data["state_id"]);
    
    $("#product_cat_id").val(data["_id"]);
    $("#product_img_text").val(data["image_url"].substring(data["image_url"].lastIndexOf('/')+1,data["image_url"].length));

    $("#add_product_cat_form_type").val("edit"); 
    
    $("#product-category-popup").modal("show");
}


function addProductSubCategory(form_values){

    responsehandlerWithFiles("product_sub_category/add", 
                    "POST", 
                    form_values, 
                    "#product-sub-category-popup",
                    "Product sub category added successfully",
                    0,
                    "add_product_sub_cat_form"
                );

}

function showEditProductSubCatDialogOld(data){
    console.log(data);
    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_product_sub_cat_form');

    $("#product_sub_category_popup_title").html("Edit Product Sub Category List");

    $("#sub_category_name").val(data["sub_category_name"]);    
    
    $("#product_category_subcategory_list_add_form").val(data["category_id"]);
    $("#product_category_subcategory_list_add_form").attr("disabled","disabled");
    $("#product_category_subcategory_list_add_form").trigger('chosen:updated');

    if(data["gst_slab"]["_id"] != undefined){
        $("#gst_slab_id").val(data["gst_slab"]["_id"]);
    }
    
    if(data["margin_rate"]["_id"]){
        $("#margin_rate_id").val(data["margin_rate"]["_id"]);
    }

    if(data["selling_unit"].length > 0){

        $("#selling_unit").val(data["selling_unit"]);
        $("#selling_unit").trigger('chosen:updated');
    }

    if(data["min_quantity"] != undefined){
        $.each(data["min_quantity"], function(key, value){
            console.log("min_quantity...."+value);
            $("#min_quantity").val(value);

        })
        
    }
    

    $("#product_sub_cat_id").val(data["_id"]);
    if(data["image_url"] != undefined){
        $("#product_sub_cat_image_text").val(data["image_url"].substring(data["image_url"].lastIndexOf('/')+1,data["image_url"].length));
    }

    $("#add_product_sub_cat_form_type").val("edit"); 
    
    $("#product-sub-category-popup").modal("show");
}

function showEditProductSubCatDialog(sub_category_name,category_id,gst_slab,margin_rate_id,selling_unit,min_quantity,_id,image_url){
    // console.log(data);
    // data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_product_sub_cat_form');

    $("#product_sub_category_popup_title").html("Edit Product Sub Category List");

    $("#sub_category_name").val(sub_category_name);    
    
    $("#product_category_subcategory_list_add_form").val(category_id);
    $("#product_category_subcategory_list_add_form").attr("disabled","disabled");
    $("#product_category_subcategory_list_add_form").trigger('chosen:updated');

    if(gst_slab != undefined){
        $("#gst_slab_id").val(gst_slab);
    }
    
    if(margin_rate_id){
        $("#margin_rate_id").val(margin_rate_id);
    }

    if(selling_unit.length > 0){

        $("#selling_unit").val(selling_unit);
        $("#selling_unit").trigger('chosen:updated');
    }
    
    if(min_quantity.length > 0){

        $("#min_quantity").val(min_quantity);
    }

    // if(data["min_quantity"] != undefined){
    //     $.each(data["min_quantity"], function(key, value){
    //         console.log("min_quantity...."+value);
    //         $("#min_quantity").val(value);

    //     })
        
    // }
    

    $("#product_sub_cat_id").val(_id);
    if(image_url != undefined){
        $("#product_sub_cat_image_text").val(image_url.substring(image_url.lastIndexOf('/')+1,image_url.length));
    }

    $("#add_product_sub_cat_form_type").val("edit"); 
    
    $("#product-sub-category-popup").modal("show");
}

function updateProductSubCategory(form_values,otp){
    console.log(form_values);
    // $('#product_cat_authentication_code').val(otp);

    form_values = new FormData(document.getElementById("add_product_sub_cat_form"));
    // form_values = new FormData($("form[name='admixture_brand_form']"));
    form_values.append("authentication_code", otp);

    responsehandlerWithFiles("product_sub_category/update", 
                    "POST", 
                    form_values, 
                    "#product-sub-category-popup",
                    "Product Sub category updated successfully",
                    1,
                    "add_product_sub_cat_form"
                );

}


function showEditProductDialog(data){

    data = JSON.parse(data);
    console.log(data);

    // $("#product_name").val(data['name']);
    
    $("#add_prodcut_category").append('<option selected>'+data['productCategory']['category_name']+'</option>');
    $("#add_prodcut_category").attr('disabled','disabled');

    $("#add_product_sub_cat").append('<option selected>'+data['productSubCategory']['sub_category_name']+'</option>');
    $("#add_product_sub_cat").attr('disabled','disabled');

    $("#contact_person").append('<option selected>'+data['supplier']['full_name']+'</option>');
    $("#contact_person").attr('disabled','disabled');

    $("#contact_address").append('<option selected>'+data['address']['line1'] + ','+ data['address']['line2'] +', '+data['address']['city_name'] +', '+ data['address']['pincode']+ ', '+ data['address']['state_name'] + '</option>');
    $("#contact_address").attr('disabled','disabled');

    if(data['unit_price'] != undefined){
        $("#unit_price").val(data['unit_price']);
        
    }
    if(data['quantity'] != undefined){
        $("#product_qty").val(data['quantity']);
        $("#product_qty").attr('disabled','disabled');
    }
    if(data['minimum_order'] != undefined){
        $("#minimum_order").val(data['minimum_order']);
    }    

    if(data['is_available']){
        $('#is_product_avail').prop('checked',true);
    }else{
        $('#is_product_avail').prop('checked',false);
    }
    
    if(data['self_logistics']){
        $('#is_self_logistics').prop('checked',true);
    }else{
        $('#is_self_logistics').prop('checked',false);
    }
    
    if(data['verified_by_admin']){
        $('#verified_by_admin_input').val(true);
    }else{
        $('#verified_by_admin_input').val(false);
    }

    $("#add_product_id").val(data['_id']);
    $("#add_product_form_type").val('edit');

    $("#add-product-list").modal("show");

}


function updateProductDetails(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("product_list/update", 
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Product updated successfully",
                    1,
                    "add_product_form"
                );

}

function updateVerifyByAdmin(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("product_list/verify_by_admin", 
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Product verification updated successfully",
                    1
                );

}

function addMarginSlab(request_data,otp){
    // request_data = request_data + "&authentication_code="+otp;
    request_data = request_data;
    responsehandler("margin_slab/add", 
                    "POST", 
                    request_data, 
                    "#add-margin-slab-popup",
                    "Margin slab added successfully",
                    1,
                    "add_margin_slab_form"
                );

}


function showEditMarginDialog(data){

    data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_margin_slab_form');

    $("#add_margin_slab_popup_title").html("Edit Margin Slab");

    $("#slab_title").val(data['title']);
    $("#et-dynamic-textboxcontainer").html('');
    var count = 0;
    $.each(data['slab'],function(key,value){
        console.log(value);
       
        var html = '<div class = "col-md-12 et-dynamic-input-block"> <div class="row"> <div class="col-md-6"> <input class = "form-control" placeholder = "e.g. 5000" name = "margin_upto[]" value="'+value['upto']+'" type="text" /></div><div class="col-md-6"><input class = "form-control" placeholder = "e.g. 5" name = "margin_rate[]" value="'+value['rate']+'" type="text" /></div></div>';
        if(count > 0){
            html += '<span class="inputremove"><i class="fa fa-close"></i></span>';
        }
        html += '</div>';
        var div = $(html);
        $("#et-dynamic-textboxcontainer").append(div);
        count++;

        
    });
    

    $("#margin_slab_id").val(data['_id']);
    $("#add_margin_slab_form_type").val('edit');

    $("#add-margin-slab-popup").modal("show");

}

function addMarginSlab(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("margin_slab/add", 
                    "POST", 
                    request_data, 
                    "#add-margin-slab-popup",
                    "Margin slab added successfully",
                    1,
                    "add_margin_slab_form"
                );

}

function editMarginSlab(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("margin_slab/update", 
                    "POST", 
                    request_data, 
                    "#add-margin-slab-popup",
                    "Margin slab updated successfully",
                    1,
                    "add_margin_slab_form"
                );

}

function addGSTSlab(request_data){

    responsehandler("gst_info/add", 
                    "POST", 
                    request_data, 
                    "#add-gst-info-popup",
                    "GST slab added successfully",
                    0,
                    "add_gst_slab_form"
                );

}

function showEditGSTDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_gst_slab_form');

    $("#add_gst_info_popup_title").html("Edit GST Info");

    $("#gst_title").val(data['title']);
    $("#igst_rate").val(data['igst_rate']);
    $("#sgst_rate").val(data['sgst_rate']);
    $("#cgst_rate").val(data['cgst_rate']);

    if(data['hsn_code'] != undefined){
        $("#hsn_code").val(data['hsn_code']);
    }

    $("#gst_slab_id").val(data['_id']);
    $("#add_gst_slab_form_type").val('edit');

    $("#add-gst-info-popup").modal("show");

}

function editGSTSlab(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("gst_info/update", 
                    "POST", 
                    request_data, 
                    "#add-gst-info-popup",
                    "GST slab updated successfully",
                    1,
                    "add_gst_slab_form"
                );

}
// Product MODULE OVER...........................................


// EMAIL TEMPLATE MODULE START...........................................

$(document).ready(function(){

    
    $("form[name='add_email_template_form']").validate({

        rules:{
            templateType: 'required',
            from_email:{
                required: true,
                email: true
            },
            'header_name[]':{
                required: true
            },
            'header_value[]':{
                required: true
            },
            subject:{
                required:true,
                noSpace:true
            },
            from_name: {
                a_z_pattern: true
            }
            
        },
        messages:{
            templateType: 'Please select atleast one template type',
            from_email: 'Please enter valid email address',
            'header_name[]': 'Header Name should not be empty',
            'header_value[]': 'Header Value should not be empty'
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "templateType" ) {
              error.insertAfter("#template_type_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");

            $('form[name="add_email_template_form"] p.error').remove();

            var emails_cc = $('#email_cc').val().split(',');
            var emails_bcc = $('#email_bcc').val().split(',');
            console.log(emails_cc.length);
            var is_cc_emails_valid = true;

            $.each(emails_cc,function(key,value){
                if(value != ''){
                    if(!value.match(mailformat)){
                        console.log("Not matched email");
                        is_cc_emails_valid = false;
                    }
                }
        
            });

            var is_bcc_emails_valid = true;

            $.each(emails_bcc,function(key,value){

                if(value != ''){
                    if(!value.match(mailformat)){
                        console.log("Not matched email");
                        is_bcc_emails_valid = false;
                    }
                }
        
            });

            console.log("is_cc_emails_valid..."+is_cc_emails_valid);
            if(!is_cc_emails_valid){
                
                $('#email_cc_error').html("Please enter all valid email address");
                $('#email_cc').focus();
            }else{
                $('#email_cc_error').html("");
            }
            // console.log("is_cc_emails_valid..."+is_cc_emails_valid);
            if(!is_bcc_emails_valid){
                
                $('#email_bcc_error').html("Please enter all valid email address");
                $('#email_bcc').focus();
                
            }else{
                $('#email_bcc_error').html("");
            }

            // console.log("is_bcc_emails_valid..."+is_bcc_emails_valid);

            if(is_cc_emails_valid && is_bcc_emails_valid){
                var form_type = $("form[name='add_email_template_form'] #add_email_template_form_type").val();
                // var form_values = $("form[name='add_email_template_form']").serialize();
                $("#txtEditor").val($("#txtEditor").Editor("getText"));
                var form_values = new FormData(form);
                console.log(form_values);
                console.log("form_type ..."+form_type );

                if(form_type == 'add'){
                    addEmailTemplate(form_values);
                }
                else if(form_type == 'edit'){

                    updateEmailTemplate(form_values);
                    
                }
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_email_template_form"] p.error').remove();
        }

    });

});

function emailValidate(emails){

    var emails = emails.split(',');

    var is_emails_valid = true;
    $.each(emails_cc,function(key,value){

        if(!value.match(mailformat)){
            console.log("Not matched email")
            is_emails_valid = false;
        }

    });

    return is_emails_valid;

}

function addEmailTemplate(form_values){

    responsehandlerWithFiles("email_template/add", 
                    "POST", 
                    form_values, 
                    "#create-email-template",
                    "Email Template added successfully",
                    0,
                    "add_email_template_form"
                );

}

function updateEmailTemplate(form_values){

    responsehandlerWithFiles("email_template/update", 
                    "POST", 
                    form_values, 
                    "#create-email-template",
                    "Email Template updated successfully",
                    0,
                    "add_email_template_form"
                );

}


function getEmailTemplateDetails(templateType,csrf){

    var url = "email_template/getTemplate";
    var request_data = {"templateType":templateType,'_token':csrf};

    customResponseHandler(
        url, // Ajax URl
        'POST', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data)
            // data = JSON.parse(data);
            
            showEditEmailTemplateDialog(data["data"]);

        }
    );

}

function getSubVendorPlants(supplier_id){
        
    var request_data = {
        "supplier_id":supplier_id
    }

    customResponseHandler(
        "admin/supplier_plants_user", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // showToast("Quantity updated","Success");

                $("#supplier_plant_details").html(data["html"]);

                

                $("#sub_vender_popup").modal("show");

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );

}


function showEditEmailTemplateDialog(data){

    // data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_email_template_form');

    $("#create_email_template_title").html("Edit Email Template");

    $("#templateType").val(data['template_type']);
    $("#templateType").attr('disabled','disabled');


    if(data['subject'] != undefined){
        $("#subject").val(data['subject']);
    }
    
    $("#from_email").val(data['from_email']);

    if(data['from_name'] != undefined){
        $("#from_name").val(data['from_name']);
    }
    

    $("#et-dynamic-textboxcontainer").html('');
    $.each(data['headers'],function(key,value){
        console.log(value);       
        
        var div = $('<div class = "col-md-12 et-dynamic-input-block"> <div class="row"> <div class="col-md-6"> <input class = "form-control" placeholder = "Name" name = "header_name[]" type="text" value = "'+value['header_name']+'" /></div><div class="col-md-6"><input class = "form-control" placeholder = "Value" name = "header_value[]" type="text" value = "'+value['header_value']+'" /></div></div><span class="inputremove"><i class="fa fa-close"></i></span></div>');
        $("#et-dynamic-textboxcontainer").append(div);   

        
    });

    if(data['html'] != undefined){
        // $("#html").html(data['html']);
        // $("#txtEditor").val(data['html']);
        $('.Editor-editor').html(data['html']);
    }
    
    $("#email_cc").val(data['cc']);
    $('#tags').tagInput({labelClass:"badge badge-success"});
    
    $("#email_bcc").val(data['bcc']);
    $('#tags1').tagInput({labelClass:"badge badge-success"});

    $("#email_template_type").val(data['template_type']);
    $("#add_email_template_form_type").val('edit');

    $("#create-email-template").modal("show");


}

function showEmailPreview(template_url,template_name){

    $('#preview_template_type').html(template_name);
    $('#template_iframe').html('<iframe id="viewDevicesiframe" src="'+template_url+'" frameborder="0" width="100%" height="100%"></iframe>');

}

function deleteAttachment(data,csrf){

    console.log(data);

    request_data = {"_token":csrf,"template_type":data["template_type"],"template_id":data["_id"]}

    responsehandler("email_template/delete_attach", 
                    "POST", 
                    request_data, 
                    "#add-gst-info-popup",
                    "Attachment deleted successfully"
                );

}

// EMAIL TEMPLATE MODULE OVER...........................................


// PRODUCT REVIEW MODULE OVER...........................................


$(document).ready(function(){

    $("form[name='update_product_review_form']").validate({

        rules:{            
            review_text:{
                required: true,
                noSpace: true
            }
        },
        messages:{
            
        },
        submitHandler: function(form) {            
            console.log("submited");
            
            var form_type = $("form[name='update_product_review_form'] #update_product_review_form_type").val();
            var form_values = $("form[name='update_product_review_form']").serialize();
           
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                // addEmailTemplate(form_values);
            }
            else if(form_type == 'edit'){
                $('#write-RC-popup').modal('hide');
                requestOTP(16,form_values,"","");
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

});

function showEditProductReviewDialog(data){

    resetEditForm('update_product_review_form');

    data = JSON.parse(data);

    $("#review_text").val(data['review_text']);      

    $("#status").val(data['status']);
    $("#peoduct_review_id").val(data['_id']);

    $("#update_product_review_form_type").val('edit');

    $("#write-RC-popup").modal("show");

}

function updateProductReviewDetails(request_data,otp){

    request_data = request_data + "&authentication_code="+otp;
    responsehandler("product_review/update", 
                    "POST", 
                    request_data, 
                    "#write-RC-popup",
                    "Product review updated successfully",
                    1
                );

}

function getSubcatByCategoryId () {
    
    var cat_id = $('select[name=categoryId]').val();
   
    var url = "product_review/getSubCat"

    var request_data = '';
    
    request_data = {"categoryId": cat_id};

    console.log(request_data);

    customResponseHandler(
        url, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            // data = JSON.parse(data);
            var html = "<option></option>";
        
            $.each(data,function(key,value){
                
                              
                html += '<option value="'+value["_id"]+'">'+value["sub_category_name"]+'</option>';
                                
                
            });
            console.log(html);
            $('#product__sub_category_list').html(html);
            // Update chosen again after append <option>
            $('#product__sub_category_list').trigger("chosen:updated");
            // $('#product__sub_category_list_chosen .chosen-search input').val();
           

        }
    );

}

// PRODUCT REVIEW MODULE OVER...........................................


// Access Logs MODULE OVER...........................................

$(document).ready(function(){

    
    $("form[name='resend_email_form']").validate({

        rules:{
            from_email:{
                required: true,
                email: true
            },from_name:{
                required: true
            },
            'header_name[]':{
                required: true
            },
            'header_value[]':{
                required: true
            }
            
        },
        messages:{
            from_email: 'Please enter valid email address',
            from_name: 'From name should bot be empty',
            'header_name[]': 'Header Name should not be empty',
            'header_value[]': 'Header Value should not be empty'
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");
            $('form[name="resend_email_form"] p.error').remove();
            var emails_cc = $('#email_cc').val().split(',');
            var emails_bcc = $('#email_bcc').val().split(',');

            var is_cc_emails_valid = true;

            $.each(emails_cc,function(key,value){

                if(!value.match(mailformat)){
                    console.log("Not matched email")
                    is_cc_emails_valid = false;
                }
        
            });

            var is_bcc_emails_valid = true;
            console.log("emails_bcc.length..."+emails_bcc.length);
            if(emails_bcc.length > 0){
                
                if(emails_bcc[0] != ''){

                    $.each(emails_bcc,function(key,value){

                        if(!value.match(mailformat)){
                            console.log("Not matched email")
                            is_bcc_emails_valid = false;
                        }
                
                    });

                }
                
            }
            

            console.log("is_cc_emails_valid..."+is_cc_emails_valid);
            if(!is_cc_emails_valid){
                
                $('#email_cc_error').html("Please enter all valid email address");
                $('#email_cc').focus();
            }else{
                $('#email_cc_error').html("");
            }
            // console.log("is_cc_emails_valid..."+is_cc_emails_valid);
            if(!is_bcc_emails_valid){
                
                $('#email_bcc_error').html("Please enter all valid email address");
                $('#email_bcc').focus();
                
            }else{
                $('#email_bcc_error').html("");
            }

            // console.log("is_bcc_emails_valid..."+is_bcc_emails_valid);

            if(is_cc_emails_valid && is_bcc_emails_valid){
                var form_type = $("form[name='resend_email_form'] #add_email_template_form_type").val();
                // var form_values = $("form[name='add_email_template_form']").serialize();
                $("#txtEditor").val($("#txtEditor").Editor("getText"));
                var form_values = new FormData(form);
                console.log(form_values);
                console.log("form_type ..."+form_type );

                if(form_type == 'edit'){
                    resendEmail(form_values);
                }
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="resend_email_form"] p.error').remove();
        }

    });

    $(".select2").select2();
});

function logEmailPreview(html){

    $('#log_email_preview').html('<iframe id="viewDevicesiframe" srcdoc="'+html+'" frameborder="0" width="100%" height="100%"></iframe>');

    $("#tempPrev-mode").modal('show');

}

function showResendEmailTemplateDialog(data){
    // console.log(data);
    data = JSON.parse(data);
    // console.log(data);

    // $("#templateType").val(data['template_type']);
    // $("#templateType").attr('disabled','disabled');


    if(data['subject'] != undefined){
        $("#subject").val(data['subject']);
    }
    
    $("#from_email").val(data['from_email']);

    if(data['from_name'] != undefined){
        $("#from_name").val(data['from_name']);
    }
    

    $("#et-dynamic-textboxcontainer").html('');
    $.each(data['headers'],function(key,value){
        console.log(value);       
        
        var div = $('<div class = "col-md-12 et-dynamic-input-block"> <div class="row"> <div class="col-md-6"> <input class = "form-control" placeholder = "Name" name = "header_name[]" type="text" value = "'+value['header_name']+'" /></div><div class="col-md-6"><input class = "form-control" placeholder = "Value" name = "header_value[]" type="text" value = "'+value['header_value']+'" /></div></div><span class="inputremove"><i class="fa fa-close"></i></span></div>');
        $("#et-dynamic-textboxcontainer").append(div);   

        
    });

    if(data['html'] != undefined){
        // $("#html").html(data['html']);
        // $("#txtEditor").val(data['html']);
        $('.Editor-editor').html(data['html']);
    }
    
    $("#email_cc").val(data['cc']);

    if(data['cc'] == undefined){
        $("#email_cc").val(data['to_email']);
    }

    // $("#email_cc").val(data['cc']);
    $('#tags').tagInput({labelClass:"badge badge-success"});
    
    $("#email_bcc").val(data['bcc']);
    $('#tags1').tagInput({labelClass:"badge badge-success"});

    // $("#email_template_type").val(data['template_type']);
    $("#add_email_template_form_type").val('edit');

    $("#create-email-template").modal("show");


}


function resendEmail(form_values){

    responsehandlerWithFiles("email_logs/email_resend", 
                    "POST", 
                    form_values, 
                    "#create-email-template",
                    "Email Sent successfully",
                    0,
                    "resend_email_form"
                );

}

// Access Logs MODULE OVER...........................................

// Ledger MODULE OVER...........................................

$(document).ready(function(){

    
    $("form[name='update_order_status_form']").validate({

        rules:{
            payment_status:{
                required: true
            }
            
        },
        messages:{
            
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            
            var form_values = $("form[name='update_order_status_form']").serialize();
            
            // var form_values = new FormData(form);
            console.log(form_values);

           
            $("#changes-ds-popup").modal('hide');
            requestOTP(17,form_values,"","");
            
            
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

});

function showOrderStatusDialog(dialog_type,data){

    data = JSON.parse(data);

    if(dialog_type == 'order_status'){

        $('#order_status_div').show();
        $('#order_status').removeAttr('disabled');

        $('#order_vendor_item_status_div').hide();
        $('#order_vendor_item_status').attr('disabled','disabled');

        $('#order_status').val(data["order_status"]);

        $('#payment_status_div').hide();
        $('#payment_status').attr('disabled','disabled');

        $('#cancel_status_div').hide();
        $('#cancel_status').attr('disabled','disabled');

        $('#order_item_status_div').hide();
        $('#order_item_status').attr('disabled','disabled');

        $('#status_dialog_title').html("Changes Delivery Status");

        $('#order_id').val(data["_id"]);

        $('#is_item_status').val('no');

    }else if(dialog_type == 'payment_status'){

        $('#order_status_div').hide();
        $('#order_status').attr('disabled','disabled');
        
        $('#order_vendor_item_status_div').hide();
        $('#order_vendor_item_status').attr('disabled','disabled');

        $('#cancel_status_div').hide();
        $('#cancel_status').attr('disabled','disabled');
        
        $('#order_item_status_div').hide();
        $('#order_item_status').attr('disabled','disabled');

        $('#payment_status_div').show();
        $('#payment_status').removeAttr('disabled');

        $('#payment_status').val(data["payment_status"]);
        
        $('#status_dialog_title').html("Changes Payment Status");

        $('#order_id').val(data["_id"]);
        
        $('#is_item_status').val('no');

    }else if(dialog_type == 'cancel_status'){

        $('#order_status_div').hide();
        $('#order_status').attr('disabled','disabled');
        
        $('#order_vendor_item_status_div').hide();
        $('#order_vendor_item_status').attr('disabled','disabled');
        
        $('#payment_status_div').hide();
        $('#payment_status').attr('disabled','disabled');
        
        $('#order_item_status_div').hide();
        $('#order_item_status').attr('disabled','disabled');

        $('#cancel_status_div').show();
        $('#cancel_status').removeAttr('disabled');

        $('#status_dialog_title').html("Cancel");

        $('#order_id').val(data["_id"]);
        
        $('#is_item_status').val('no');

    }else if(dialog_type == 'item_status'){

        $('#order_item_status_div').show();
        $('#order_item_status').removeAttr('disabled');
        
        $('#order_status_div').hide();
        $('#order_status').attr('disabled','disabled');
        
        $('#order_vendor_item_status_div').hide();
        $('#order_vendor_item_status').attr('disabled','disabled');

        $('#order_item_status').val(data["item_status"]);

        $('#payment_status_div').hide();
        $('#payment_status').attr('disabled','disabled');

        $('#cancel_status_div').hide();
        $('#cancel_status').attr('disabled','disabled');

        $('#status_dialog_title').html("Change Item Status");

        $('#item_id').val(data["_id"]);
        $('#is_item_status').val('yes');

    }else if(dialog_type == 'vendor_item_status'){

        resetForm('update_order_status_form','changes-ds-popup');

        $('#order_vendor_item_status_div').hide();
        // $('#order_vendor_item_status').removeAttr('disabled');
        $('#order_vendor_item_status').attr('disabled','disabled');
        
        $('#order_item_status_div').hide();
        $('#order_item_status').attr('disabled','disabled');
        
        $('#order_status_div').hide();
        $('#order_status').attr('disabled','disabled');

        $('#order_vendor_item_status').val(data["order_status"]);

        $('#admin_remarks').hide();
        $('#payment_status_div').show();
        $('#payment_status').removeAttr('disabled');
        // $('#payment_status').attr('disabled','disabled');

        $('#cancel_status_div').hide();
        $('#cancel_status').attr('disabled','disabled');

        $('#status_dialog_title').html("Change Payment Status");

        $('#order_id').val(data["_id"]);
        $('#is_item_status').val('no');

    }
    
    
    $("#changes-ds-popup").modal('show');

}

function updateOrderStatus(request_data,otp){
    
    request_data = request_data + "&authentication_code="+otp;
    var url = '';
    var is_item_status = $('#is_item_status').val();
    
    if(is_item_status == 'no'){
        url = 'orders/status';
    }else{
        url = 'orders/item_status';
    }


    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#changes-ds-popup",
                    "Status updated successfully",
                    1
                );

}

function updateBillStatus(bill_status, order_id, bill_id, csrf,bill_reject_reason,bill_type){
    console.log("test....");
    if(bill_status == "2"){

        bill_reject_reason = $("#"+bill_reject_reason).val();

    }

    var request_data = {
        "_token": csrf,
        "order_id": order_id,
        "bill_id": bill_id,
        "bill_status": bill_status,
        "bill_reject_reason": bill_reject_reason,
        "bill_type": bill_type
    };

    responsehandler("admin/orders/update_supplier_bill_status", 
        "POST", 
        request_data, 
        "#changes-ds-popup",
        "Bill status updated successfully"
    );

}

function showSupplierDialog(supplier_data){
    // console.log(supplier_data);
    supplier_data = JSON.parse(supplier_data);
    console.log(supplier_data);
    $('#supplier_order_name').html(supplier_data['full_name']);
    $('#supplier_mobile_no').html(supplier_data['mobile_number']);
    // $('#supplier_landline_no').html(supplier_data['mobile_number']);
    $('#supplier_email').html(supplier_data['email']);

    $('#changes-splr-popup').modal('show');

}


function updateLogisticsBillStatus(bill_status, order_id, bill_id, csrf,bill_reject_reason,bill_type){

    var bill_reject_reasons = null;

    if(bill_status == "2"){

        bill_reject_reasons = $("#"+bill_reject_reason).val();

        // console.log("bill_reject_reason..."+bill_reject_reasons);

    }

    var request_data = {
        "_token": csrf,
        "order_id": order_id,
        "bill_id": bill_id,
        "bill_status": bill_status,
        "bill_reject_reason": bill_reject_reasons,
        "bill_type": bill_type
    };

    responsehandler("admin/orders/update_logistics_bill_status", 
        "POST", 
        request_data, 
        "#changes-ds-popup",
        "Bill status updated successfully"
    );

}


function buyerItemTrack(item_id){

    var form_values = {};

    customResponseHandler("admin/buyers_orders_truck_tracking/"+item_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#multi_truck_details_div").html(message);

                            $("#multi-truck-popup").modal('show');
                        }else{
                            showToast("Something went wrong. Please try again","Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}

function buyerItemTracking(item_id,tracking_id){

    // $("#multi-truck-popup").modal('hide');

    var form_values = {};

    customResponseHandler("admin/buyers_orders_item_tracking/"+item_id+'/'+tracking_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#buyers_item_tracking_div").html(message);

                            $("#order-track").modal('show');
                        }else{
                            showToast("Something went wrong. Please try again","Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}

function buyerCPTrack(item_part_id){

    var form_values = {};

    customResponseHandler("admin/buyers_orders_CP_tracking/"+item_part_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#order_CP_trackking_div").html(message);

                            $("#order-CP-track").modal('show');
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}

function supplierCPTrack(item_part_id){

    var form_values = {};

    customResponseHandler("admin/suppliers_orders_CP_tracking/"+item_part_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#order_CP_trackking_div").html(message);

                            $("#order-CP-track").modal('show');
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}


function supplierItemTrack(item_id,tracking_id){

    var form_values = {};

    customResponseHandler("admin/suppliers_orders_item_tracking/"+item_id+'/'+tracking_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#buyers_item_tracking_div").html(message);

                            $("#order-track").modal('show');
                        }else{
                            showToast("Something went wrong. Please try again","Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}

function logisticsItemTrack(item_id,tracking_id){

    var form_values = {};

    customResponseHandler("admin/logistics_orders_item_tracking/"+item_id+'/'+tracking_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#buyers_item_tracking_div").html(message);

                            $("#order-track").modal('show');
                        }else{
                            showToast("Something went wrong. Please try again","Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}


// Ledger MODULE OVER...........................................

// Support Ticket MODULE Start...........................................

var select_supplier_user_list = 'supplier_user_list';
var select_buyer_user_list = 'buyer_user_list';
var select_logistics_user_list = 'logistics_user_list';

$(document).ready(function() {
    /*character limit script start*/
    $('textarea#ctsubject, textarea#ctdescription').characterlimit();
    /*character limit script end*/

    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='add_support_ticket_form']").validate({

        rules:{
            question_type: "required",
            severity: "required",
            subject: {
                required: true,
                noSpace: true
            },
            description: {
                required: true,
                noSpace: true
            },
            user_type: "required",
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf"
            },
            supplier_user_id: {
                required: function(){
                    return $('#user_type option:selected').val() === 'Supplier';
              }
            },
            buyer_user_id: {
                required: function(){
                    return $('#user_type option:selected').val() === 'Buyer';
              }
            },
            logistics_user_id: {
                required: function(){
                    return $('#user_type option:selected').val() === 'Logistics';
              }
            }
            
        },
        messages:{
            
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf) file format"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "user_type" ) {
                error.insertAfter("#user_type_div");
            } else if (element.attr("name") == "question_type" ) {
                error.insertAfter("#question_type_div");
            } else if (element.attr("name") == "severity" ) {
                error.insertAfter("#severity_div");
            } else if (element.attr("name") == "attachments[]" ) {
                error.insertAfter("#support_ticket_attach_div");
            }else if (element.attr("name") == "supplier_user_id" ) {
                error.insertAfter("#user_type_list_div");
            }else if (element.attr("name") == "buyer_user_id" ) {
                error.insertAfter("#user_type_list_div");
            }else if (element.attr("name") == "logistics_user_id" ) {
                error.insertAfter("#user_type_list_div");
            } else {
                error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // $("#" + elem.attr("name") + "_text").addClass(errorClass);
            $(".fileuploader-input-caption").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // $("#" + elem.attr("name") + "_text").removeClass(errorClass);
            $(".fileuploader-input-caption").addClass(errorClass);
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="add_support_ticket_form"] p.error').remove();
            // var form_values = $("form[name='add_support_ticket_form']").serialize();
            
            var user_type_selected = $('#user_type option:selected').val();
            console.log("user_type_selected.."+user_type_selected);

            var is_user_selected = false;

            if(user_type_selected === 'Vendor'){
                console.log("value _selected.."+$('#supplier_user_list option:selected').val());
                if($('#supplier_user_list option:selected').val().length > 0){
                    is_user_selected = true;
                }else{
                    $('#user_type_listing_error').html("Please select supplier");
                }

            }else if(user_type_selected === 'Buyer'){
                if($('#buyer_user_list option:selected').val().length > 0){
                    is_user_selected = true;
                }else{
                    $('#user_type_listing_error').html("Please select buyers");
                }
            }else if(user_type_selected === 'Logistics'){
                if($('#logistics_user_list option:selected').val().length > 0){
                    is_user_selected = true;
                }else{
                    $('#user_type_listing_error').html("Please select logistic user");
                }
            }

            if(is_user_selected){

                var form_values = new FormData(form);
                console.log(form_values);
            
                addSupportTicket(form_values);

            }
            
            
            
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_support_ticket_form"] p.error').remove();
        }

    });
    
    


    /*reply form show hide script */
    $('.showButton').click(function () {
        $('.rplyshowhide').fadeIn();
        $('.rplyshow').fadeOut();
    });
    $('.hideButton').click(function () {
        $('.rplyshowhide').fadeOut();
        $('.rplyshow').fadeIn();
    });

    $('#user_type').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        console.log("valueSelected.."+valueSelected);
        if(valueSelected === 'Vendor'){
            $("#supplier_user_list_chosen").show();
            $("#buyer_user_list_chosen").hide();
            $("#logistics_user_list_chosen").hide();
            $("#question_type_dropdown").html($("#question_type_dropdown_supplier").html());
        }else if(valueSelected === 'Buyer'){
            $("#supplier_user_list_chosen").hide();
            $("#buyer_user_list_chosen").show();
            $("#logistics_user_list_chosen").hide();
            $("#question_type_dropdown").html($("#question_type_dropdown_buyer").html());
        }else if(valueSelected === 'Logistics'){
            $("#supplier_user_list_chosen").hide();
            $("#buyer_user_list_chosen").hide();
            $("#logistics_user_list_chosen").show();
        }

    });

    $("#supplier_user_list_chosen").hide();
    $("#buyer_user_list_chosen").hide();
    $("#logistics_user_list_chosen").hide();


    $('#' + select_supplier_user_list + '_chosen .chosen-search input').keyup(function(e){
        console.log("e.key"+e.key);
        console.log("e.code"+e.code);
        console.log("event.which"+e.which);

        if(e.which != 13){
        // Change No Result Match text to Searching.
      $('#' + select_supplier_user_list + '_chosen .no-results').html('Searching = "'+ $('#' + select_supplier_user_list + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_supplier_user_list + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'support_ticket/getSuppliers',select_supplier_user_list);  //Set timer back if got value on input

    //   }
        }else{
            clearTimeout(typingTimer);
        }
    });

    
    $('#' + select_buyer_user_list + '_chosen .chosen-search input').keyup(function(e){

        if(e.which != 13){
        // Change No Result Match text to Searching.
      $('#' + select_buyer_user_list + '_chosen .no-results').html('Searching = "'+ $('#' + select_buyer_user_list + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_buyer_user_list + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'support_ticket/getBuyers',select_buyer_user_list);  //Set timer back if got value on input

    //   }
        }else{
            clearTimeout(typingTimer);
        }
    });
    
    $('#' + select_logistics_user_list + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + select_logistics_user_list + '_chosen .no-results').html('Searching = "'+ $('#' + select_logistics_user_list + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_logistics_user_list + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'support_ticket/getLogistics',select_logistics_user_list);  //Set timer back if got value on input

    //   }
  
    });


    $("form[name='support_ticket_reply_form']").validate({

        rules:{
            
            comment: {
                required: true,
                noSpace: true
            },
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf|png"
            }
            
        },
        messages:{
            comment: {
                required: "Comment is required"
            },
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf, png) file format"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "attachments[]" ) {
                error.insertAfter("#reply_attachment_div");
            } else {
                error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="support_ticket_reply_form"] p.error').remove();
            // var form_values = $("form[name='add_support_ticket_form']").serialize();
            
            
            var form_values = new FormData(form);
            console.log(form_values);
        
            replySupportTicket(form_values);   
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="support_ticket_reply_form"] p.error').remove();
        }

    });


    $("form[name='ticket_resolve_form']").validate({

        rules:{
            
            
        },
        messages:{
            
          
        },
        
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            
            var form_values = $("form[name='ticket_resolve_form']").serialize();
            
            
            // var form_values = new FormData(form);
            // console.log(form_values);
        
            requestOTP(19,form_values,"","resolve_ticket");
            

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }

    });

});

function updateTicketStatus(form_values,otp){
    
    form_values = form_values + "&authentication_code="+otp;
    
    responsehandler('admin/support_ticket/ticket_detail/resolve', 
        "POST", 
        form_values,
        "#make-default-alert-popup",
        "Ticket status updated successfully",
        1
    );

}


function addSupportTicket(form_values){

    responsehandlerWithFiles("support_ticket/add", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Support Ticket Added successfully",
                    0,
                    "add_support_ticket_form"
                );

}

function replySupportTicket(form_values){

    responsehandlerWithFiles("support_ticket/reply", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Reply sent successfully",
                    0,
                    "support_ticket_reply_form"
                );

}

// Support Ticket MODULE OVER...........................................

// Proposal MODULE Start...........................................
var select_city_list = 'city_list';
$(document).ready(function(){

    $('#' + select_city_list + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + select_city_list + '_chosen .no-results').html('Searching = "'+ $('#' + select_city_list + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + select_city_list + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'region_cities/getCities',select_city_list);  //Set timer back if got value on input

    //   }
  
    });


    $("form[name='compose_new_message_form']").validate({

        rules:{
            
            to_email:{
                required: true
            },
            subject: "required",
            html: "required",
            'header_name[]': "required",
            'header_value[]': "required",
            
        },
        messages:{
            to_email: "To email is required",
            subject: "Subject is required",
            html: "Message is required",
            'header_name[]': "Header key is required",
            'header_value[]': "Header value is required",
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="compose_new_message_form"] p.error').remove();
            // var form_values = $("form[name='add_support_ticket_form']").serialize();
            var emails_cc = $('#email_cc').val().split(',');
            var to_email = $('#to_email').val().split(',');

            var is_cc_emails_valid = true;

            if(emails_cc.length > 0){
                
                if(emails_cc[0] != ''){
                    $.each(emails_cc,function(key,value){

                        if(!value.match(mailformat)){
                            console.log("Not matched email")
                            is_cc_emails_valid = false;
                        }
                
                    });
                }
            }

            var is_bcc_emails_valid = true;
            console.log("to_email.length..."+to_email.length);
            if(to_email.length > 0){
                
                if(to_email[0] != ''){

                    $.each(to_email,function(key,value){

                        if(!value.match(mailformat)){
                            console.log("Not matched email")
                            is_bcc_emails_valid = false;
                        }
                
                    });

                }
                
            }
            

            console.log("is_cc_emails_valid..."+is_cc_emails_valid);
            if(!is_cc_emails_valid){
                
                $('#email_cc_error').html("Please enter all valid email address");
                $('#email_cc').focus();
                $('#email_cc_error').show();
            }else{
                $('#email_cc_error').html("");
                $('#email_cc_error').hide();
            }
            // console.log("is_cc_emails_valid..."+is_cc_emails_valid);
            if(!is_bcc_emails_valid){
                
                $('#email_to_error').html("Please enter all valid email address");
                $('#to_email').focus();
                
            }else{
                $('#email_to_error').html("");
            }
            
            if(is_cc_emails_valid && is_bcc_emails_valid){
                $("#txtEditor").val($("#txtEditor").Editor("getText"));

                var form_values = new FormData(form);
                console.log(form_values);
            
                sendProposalEmail(form_values);

            }
            

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="compose_new_message_form"] p.error').remove();
        }

    });

});

function showProposalEmailDialog($data){

    $data = JSON.parse($data);

    $('#to_email').val($data["email"]);

    $('#tags').tagInput({labelClass:"badge badge-success"});

    $('#compose-new-message').modal('show');

}

function sendProposalEmail(form_values){

    responsehandlerWithFiles("proposal/send_email", 
                    "POST", 
                    form_values, 
                    "#compose-new-message",
                    "Email sent successfully",
                    0,
                    "compose_new_message_form"
                );

}

// Proposal MODULE OVER...........................................

// Profile MODULE Start...........................................

$(document).ready(function(){

    $("form[name='update_profile_form']").validate({

        rules:{
            
            full_name:{
                required: true,
                minlength:3,
                maxlength:64,
                
            },
            email:{
                required: true
            },
            mobile_number:{
                required: true
            },
            
            
        },
        messages:{
            
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="update_profile_form"] p.error').remove();
            var form_values = $("form[name='update_profile_form']").serialize();
            
                    
            // var form_values = new FormData(form);
            console.log(form_values);
            $('#edit-profile-popup').modal('hide');
            // requestOTP(18,form_values,"","");

            updateAdminProfile(form_values);

            // var is_email_change = 0;
            // var is_mobile_change = 0;   
            
            // var edit_address_mobile = $("#edit_address_mobile").val();
            // var edit_address_email = $("#edit_address_email").val();

            // var mobile_or_email = $("#mobile_or_email").val();
            // var email = $("#email").val();                   
            
            // if(edit_address_mobile != mobile_or_email){
            //     console.log("edit_address_mobile..."+edit_address_mobile);
            //     console.log("mobile_or_email..."+mobile_or_email);
            //     is_mobile_change = 1;
            // }

            // if(edit_address_email != email){
            //     is_email_change = 1;
            // }
            // // console.log("is_mobile_change..."+is_mobile_change);
            
            // requestOTPforEditProfile(1,form_values,is_mobile_change,is_email_change,1);   

            

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="update_profile_form"] p.error').remove();
        }

    });

    $("form[name='update-mobile-otp-form']").validate({
        rules: {
            otp_code_1: {
                required: true,
                minlength : 6,
                maxlength: 6,
                number:true,
                noDecimal: true
            }
            
        },
        messages: {
            
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='update-mobile-otp-form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            updateMobileEmail(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });
    
    
    $("form[name='new_mobile_form']").validate({
        rules: {
            mobile: {
                required: true,
                minlength : 13,
                maxlength: 13,
                noDecimal: true
            },
            
            
        },
        messages: {
            mobile:{
                minlength: "Please enter valid mobile number",
                maxlength: "Please enter valid mobile number",
            }
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='new_mobile_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            // updateMobileEmail(form_values,"old_mobile");

            OTPonUpdateMobile(0, 1);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("form[name='update-email-otp-form']").validate({
        rules: {
            otp_code_1: {
                required: true,
                minlength : 6,
                maxlength: 6,
                number:true,
                noDecimal: true
            },
            
            
        },
        messages: {
            
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='update-email-otp-form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            updateEmail(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });


    $("form[name='new_email_form']").validate({
        rules: {
            email: {
                required: true,
                email_validate: true
            },
            
            
        },
        messages: {
            email:{
                email_validate: "Please enter valid email address"
            }
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='new_email_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            // updateMobileEmail(form_values,"old_mobile");

            OTPonUpdateEmail(0, 1);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    
    $("form[name='update_password_form']").validate({

        rules:{            
            old_password:{
                required: true,
                minlength : 6
            },
            password:{
                required: true,
                minlength : 6
            },
            confirm_password:{
                required: true,
                minlength : 6,
                equalTo : "#new_password"
            },            
            
        },
        messages:{
           
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="update_password_form"] p.error').remove();
            var form_values = $("form[name='update_password_form']").serialize();
            
                    
            // var form_values = new FormData(form);
            console.log(form_values);
            
            requestOTP(20,form_values,"","update_password");

            

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="update_password_form"] p.error').remove();
        }

    });

});

function OTPonUpdateMobile(is_old_mobile_no, is_new_mobile_no, is_resend=0){

    if(is_old_mobile_no == 1){
        var mobile_or_email = $("#mobile_or_email").val();
    }
    
    if(is_new_mobile_no == 1){
        var mobile_or_email = $("#update_new_mobile_no").val();
    }
    
    $("#verify_update_mobile").val(mobile_or_email);
    
    $("#edit-profile-popup").modal("hide");

    requestOTPforEditProfile(1,mobile_or_email,is_old_mobile_no,0,is_new_mobile_no,0,1,is_resend);

}

function OTPonUpdateEmail(is_old_email, is_new_email,is_resend=0){

    if(is_old_email == 1){
        var mobile_or_email = $("#edit_address_email").val();
    }
    
    if(is_new_email == 1){
        var mobile_or_email = $("#update_new_email").val();
    }
    
    $("#verify_update_email").val(mobile_or_email);
    
    $("#edit-profile-popup").modal("hide");

    requestOTPforEditProfile(1,mobile_or_email,0,is_old_email,0,is_new_email,1,is_resend);

}

function updateMobileEmail(form_values){

    // form_values=form_values+"&mobile_or_email_type="+mobile_or_email_type;

    // form_values.append('mobile_or_email_type', mobile_or_email_type);
    // form_values.append('email_code', email_otp);
    // form_values.append('authentication_code', old_mobile_otp);

    customResponseHandler("my_profile/update",
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){                            

                            $("#update_mobile_profile").modal("hide");

                            if($("#mobile_type").val() == "old_mobile"){
                                showToast("OTP verified successfully","success");
                                $("#new_mobile_no_popup").modal("show");
                            }else{
                                showToast("Mobile No updated successfully","success");
                                reload();
                            }

                        }else{
                            showToast("Entered OTP is Wrong. Please check again","error");
                            // $("#update_mobile_otp_error").html("Enter OTP is Wrong. Please check and enter again");
                        }

                    }
                );

}

function updateEmail(form_values){
    console.log(form_values);
    // form_values=form_values+"&mobile_or_email_type="+mobile_or_email_type;

    // form_values.append('mobile_or_email_type', mobile_or_email_type);
    // form_values.append('email_code', email_otp);
    // form_values.append('authentication_code', old_mobile_otp);

    customResponseHandler("my_profile/update",
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){                            

                            $("#update_email_profile").modal("hide");

                            if($("#email_type").val() == "old_email"){
                                showToast("OTP verified successfully","success");
                                $("#new_email_no_popup").modal("show");
                            }else{
                                showToast("Email updated successfully","success");
                                reload();
                            }

                        }else{
                            showToast("Entered OTP is Wrong. Please check again","error");
                            // $("#update_mobile_otp_error").html("Enter OTP is Wrong. Please check and enter again");
                        }

                    }
                );

}

function showEditProfileDialog($data){

    resetEditForm('update_profile_form');

    $data = JSON.parse($data);

    $('#full_name').val($data["full_name"]);
    $('#email').val($data["email"]);
    $('#mobile_or_email').val($data["mobile_number"]);


    $('#edit-profile-popup').modal('show');


}

function updateAdminProfile(request_data){
    
    // request_data = request_data + "&authentication_code="+otp+ "&mobile_code="+mobile_otp+ "&email_code="+email_otp;
    var url = 'my_profile/update';

    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#changes-ds-popup",
                    "Updated successfully",
                    1,
                    "update_profile_form"
                );

}


function updateAdminPassword(request_data,otp){
    
    request_data = request_data + "&authentication_code="+otp;
    var url = 'my_profile/change_pass';

    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#changes-ds-popup",
                    "Password change successfully",
                    1,
                    "update_password_form"
                );

}



function supplierVerifyByAdmin(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("admin/supplier_verify",
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Supplier verification updated successfully",
                    1
                );

}

function supplierPlantVerifyByAdmin(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("admin/supplier_plant_verify",
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Supplier plant verification updated successfully",
                    1
                );

}

function supplierBlockUnblockedByAdmin(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("admin/supplier_block_unblock",
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Supplier block/Unblock updated successfully",
                    1
                );

}

function supplierPlantBlockUnblockedByAdmin(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("admin/supplier_plant_block_unblock",
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Supplier Plant block/Unblock updated successfully",
                    1
                );

}

function supplierRejectedByAdmin(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("admin/supplier_rejected",
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Supplier Rejected successfully",
                    1
                );

}

function supplierPlantRejectedByAdmin(request_data,otp){
    request_data = request_data + "&authentication_code="+otp;
    responsehandler("admin/supplier_plant_rejected",
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Supplier Rejected successfully",
                    1
                );

}

function logisticsVerifyByAdmin(request_data){
    // request_data = request_data + "&authentication_code="+otp;
    responsehandler("admin/logistics_verify",
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Logistics verification updated successfully"
                );

}



// Profile MODULE OVER...........................................

// Report MODULE Start...........................................

$(document).ready(function(){

    $('.from').datepicker({
	    autoclose: true,
	    minViewMode: 1,
	    format: 'M yyyy'
		}).on('changeDate', function(selected){
	        startDate = new Date(selected.date.valueOf());
	        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    });
    
    var daily_month;
    var daily_year;
    $("#daily_chart_year_filter").datepicker().on('changeMonth', function(e){ 
    // daily_month = new Date(e.date).getMonth() + 1;
    // daily_month = all_month_array[daily_month-1];
    // daily_year = String(e.date).split(" ")[3];
    // console.log("daily_year..."+daily_year+"..."+daily_month);

    //    var monthly_chart_status = $("#daily_chart_status_filter").val();

    //    getDailyChartDetails(daily_year,daily_month, monthly_chart_status);
    });

});

function downloadAggrSand(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "agg_sand_cat_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadAggrSandSubCat(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "agg_sand_subcategory_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadAggrSource(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "agg_source_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadCementBrand(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "cement_brand_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadCementGrade(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "cement_grade_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadConcreteGrade(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "concrete_grade_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadConcretePumpCat(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "concrete_pump_category_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadCoupon(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "coupon_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadSuportTicket(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "support_ticket_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadFlyAsh(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "flyAsh_source_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}


function downloadGST(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "gst_slab_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadMarginRate(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "margin_rate_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadSandSource(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "sand_source_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadDesignMix(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "designmix_list_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadTMList(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "vehicle_list_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadTMCatList(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "TM_category_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadTMSubCatList(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "TM_sub_category_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadAdmixTypes(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "admix_types_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadAdmixBrand(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "admix_brand_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadBuyerList(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "buyer_listing_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadSupplierList(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "supplier_listing_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadBuyerOrders(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "buyer_orders_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadSupplierOrders(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "supplier_order_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}


var getParams = function (url) {
	var params = {};
    var param_string = "";
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);

        param_string += pair[0]+'='+decodeURIComponent(pair[1])+'&';
	}
	// return params;
    param_string = param_string.substring(0, param_string.length - 1);
	return param_string;
};

var getParamsForFilter = function (url) {
	var params = {};
    var param_string = "";
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
    var is_after = 0;
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);

       
        param_string += pair[0]+'='+decodeURIComponent(pair[1])+'&';
        

        
	}

	// return params;
    param_string = param_string.substring(0, param_string.length - 1);
	return param_string;
};


function viewDesignMixOfGrade(grade_id){

        
    var request_data = {
        "grade_id":grade_id
    }

    customResponseHandler(
        "admin/reports/designmix_detail", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // showToast("Quantity updated","Success");

                $("#design_mix_body").html(data["html"]);

                $("#show-design-mix-details").modal("show");

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );


}

// Report MODULE OVER...........................................

// Coupon MODULE Start...........................................

$(document).ready(function(){
    
    $("form[name='add_coupon']").validate({

        rules:{            
            code:{
                required: true
            },
            discount_type:{
                required: true
            }, 
            discount_value:{
                required: true,
                number:true
            }, 
            min_order:{
                required: true,
                number:true
            }, 
            max_discount:{
                required: true,
                number:true
            },  
            max_usage:{
                required: true,
                number:true
            }, 
            start_date:{
                required: true
            },
            end_date:{
                required: true
            },
            unique_use:{
                required: false
            },
            info:{
                required: true
            },
            tnc:{
                required: true
            },            
            
        },
        messages:{
           
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "discount_type" || element.attr("name") == "start_date" || element.attr("name") == "end_date" ) {
              error.insertAfter(element.parent());
            }else if(element.attr("name") == "unique_use"){
                error.insertAfter("#single_use_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="add_coupon"] p.error').remove();
            var form_values = $("form[name='add_coupon']").serialize();
            
                    
            // var form_values = new FormData(form);
            console.log(form_values);

            var form_type = $("#add_coupon_type").val();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addCoupon(form_values);
            }else if(form_type == "edit"){
                
                $('#add-coupon-popup').modal('hide');
                requestOTP(27,form_values,"","");

            }
            

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_coupon"] p.error').remove();
        }

    });

    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        var id_name = $(this).attr("id");
        // console.log("id_name......"+id_name);
        // console.log("fileName......"+fileName);
        // $("#"+id_name+"_text").val(fileName);
        $(this).parent().next( ".browse-input" ).val(fileName);
        $(this).trigger( "focusout" );

        // alert('The file "' + fileName +  '" has been selected.');
    });

});

function addCoupon(form_values){

    responsehandler("coupon/add", 
                    "POST", 
                    form_values, 
                    "#add-coupon-popup",
                    "Coupon Added successfully",
                    0,
                    "add_coupon"
                );

}

function showEditCoupon(data){
    data = JSON.parse(data);
    console.log(data);
    // add_edit_user_form_html = $("#add_admin_user").html();
    resetEditForm('add_coupon');
    
    $("#code").val(data["code"]);

    $("#discount_type").val(data["discount_type"]);   
    $("#discount_value").val(data["discount_value"]);   
    $("#min_order").val(data["min_order"]);   
    $("#max_discount").val(data["max_discount"]);   
    $("#max_usage").val(data["max_usage"]);   
    $("#start_date").val(data["start_date"].replace("T00:00:00.000Z", ""));   
    $("#end_date").val(data["end_date"].replace("T00:00:00.000Z", ""));   
    $("#ctsubject").val(data["info"]);   
    $("#summernotecoupon").val(data["tnc"]);   

    // if(data["unique_use"] == true){
        // $("#unique_use").prop("checked",true);
    // }else{
    //     $("#unique_use").prop(false);
    // }

    // $.each(data['buyer_ids'],function(kye,value){
    //     $('#buyer_id').val(value).change();
    // })
   

    $("#add_coupon_type").val("edit");

    var title = $('#add_coupon_popup_title').html();
    title = title.replace('Add','Edit');
    $("#add_coupon_popup_title").html(title);
    $("#coupon_id").val(data["_id"]);


    $("#add-coupon-popup").modal("show");      

}

function editCoupon(form_values,otp){

    // form_values.append("authentication_code", otp);

    form_values = form_values+"&authentication_code="+otp;

    responsehandler("coupon/edit", 
                    "POST", 
                    form_values, 
                    "#add-coupon-popup",
                    "Coupon updated successfully",
                    1,
                    "add_coupon"
                );

}

function activeInactiveCoupon(csrf,data){

    var active_inactive = "Active";
    if(data["is_active"])
    {
        active_inactive = "InActive";
       
    }

    $.ajax({
        type: "POST",
        url: BASE_URL+"/coupon/active_inactive", 
        data: {"_token": ""+csrf,data},
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            if(data["status"] == 200){  //Success
                $('#add-coupon-popup').modal('hide');                
                showToast("Coupon status successsfully updated to "+ active_inactive ,"Success");
                reload();
            }else{ //Error
                showToast(""+data["error"]["message"],"Error");
                $("#otpOk").unbind().one('click', OTP_dialog);
            }             
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });
}


function deleteCoupon(csrf,data, otp){
    // console.log(data);
    // data = JSON.parse(data);
    var request_data = {
        '_id' : data["_id"],
        '_token': csrf,
        'authentication_code': otp,
    }

    responsehandler("coupon/delete", 
                    "POST", 
                    request_data, 
                    "#flyash-source-popup",
                    "Coupon deleted successfully",
                    1,
                    "add_flyash_source_form"
                );

}


// Coupon MODULE OVER...........................................

// Notification MODULE OVER...........................................

function notificationClick(){

    var request_data = {};

    $("#noti_list_div").html('<div class="overlay">\
    <span class="spinner"></span>\
</div>');

    customResponseHandler(
        "admin/notification", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                    
                
                    // $("#cart_item_count").html((parseInt(cart_item_count) - 1));

                    // $("#"+item_div_id).remove();                
                    
                    
                        // showToast("Product added successfully","Success");
                        
                        $("#noti_count").remove();
                        $("#noti_list_div").remove();
                        $("#noti_main_div").html(data["html"]);
                        
              }else{
                showToast(""+data["error"]["message"],"Error");              
                
            }
            

        }
        
    );

}

function updateNotiStatus($id){

    var request_data = {};

    customResponseHandler(
        "admin/notification/"+$id, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            // if((data["status"] >= 200) && (data["status"] < 300)){
            //     // var message = data["data"]["message"];
            
                
            
            //     // $("#cart_item_count").html((parseInt(cart_item_count) - 1));

            //     // $("#"+item_div_id).remove();                
                
                
            //         // showToast("Product added successfully","Success");

            //         // $("#noti_list_div").html(data["html"]);
            //         $("#noti_details").html(data["html"]);
                    
            // }else{
            // showToast(""+data["error"]["message"],"Error");              
            
            // }
            

        }
        
    );

   

}


// Notification MODULE OVER...........................................

function couponInfo(details){

    $("#coupon_info_details").html(details);

    $("#coupon-info-popup").modal("show");

}

function couponTnc(details){

    $("#tnc_info_details").html(details);

    $("#tnc-info-popup").modal("show");
}