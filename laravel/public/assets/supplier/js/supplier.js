// Login Module Start..................................................

$(document).ready(function(){

   

});

var time_array = Array();

time_array[0] = "12:00";
time_array[1] = "00:00";
time_array[2] = "01:00";
time_array[3] = "02:00";
time_array[4] = "03:00";
time_array[5] = "04:00";
time_array[6] = "05:00";
time_array[7] = "06:00";
time_array[8] = "07:00";
time_array[9] = "08:00";
time_array[10] = "09:00";
time_array[11] = "10:00";
time_array[12] = "11:00";
time_array[13] = "12:00";
time_array[14] = "13:00";
time_array[15] = "14:00";
time_array[16] = "15:00";
time_array[17] = "16:00";
time_array[18] = "17:00";
time_array[19] = "18:00";
time_array[20] = "19:00";
time_array[21] = "20:00";
time_array[22] = "21:00";
time_array[23] = "22:00";
time_array[24] = "23:00";

console.log("time_array::"+time_array[10]);

var time_key_array = Array();

time_key_array["12:00"] = 0;
time_key_array["00:00"] = 1;
time_key_array["01:00"] = 2;
time_key_array["02:00"] = 3;
time_key_array["03:00"] = 4;
time_key_array["04:00"] = 5;
time_key_array["05:00"] = 6;
time_key_array["06:00"] = 7;
time_key_array["07:00"] = 8;
time_key_array["08:00"] = 9;
time_key_array["09:00"] = 10;
time_key_array["10:00"] = 11;
time_key_array["11:00"] = 12;
time_key_array["12:00"] = 13;
time_key_array["13:00"] = 14;
time_key_array["14:00"] = 15;
time_key_array["15:00"] = 16;
time_key_array["16:00"] = 17;
time_key_array["17:00"] = 18;
time_key_array["18:00"] = 19;
time_key_array["19:00"] = 20;
time_key_array["20:00"] = 21;
time_key_array["21:00"] = 22;
time_key_array["22:00"] = 23;
time_key_array["23:00"] = 24;

console.log("time_key_array::"+time_key_array["09:00"]);

function confirmPopup(type,data,csrf,message){
    
    $("#show-TM-Unavail-details").modal("hide");
    $("#show-CP-Unavail-details").modal("hide");
    

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);
        var request_data = data;
        data = JSON.parse(data);
        if((type == 1)){
            orderAccept(data['_id']+'','ACCEPTED'); //Order Accept
        }else if(type == 2){
            data = 'order_id='+data['_id'];
            requestOTP(2,data,'order_rejected');
        }else if(type == 3){
            surpriseOrderAccept(data['_id']+'','ACCEPTED'); // Suprprice Order Accept
        }else if(type == 4){
            
            requestOTP(3,data,'');
        }else if(type == 5){
            
            deleteTMUnavail(data['_id']);
        }else if(type == 6){
            
            deleteCPUnavail(data['_id']);
        }else if(type == 7){
            console.log("test");
            deleteCementRate(data['_id']);
        }else if(type == 8){
            console.log("test");
            showCementStockInactiveDialog(request_data);
        }else if(type == 9){
            console.log("test");
            showSandStockInactiveDialog(request_data);
        }else if(type == 10){
            console.log("test");
            showAggregateStockInactiveDialog(request_data);
        }else if(type == 11){
            console.log("test");
            showFlyashStockInactiveDialog(request_data);
        }else if(type == 12){
            console.log("test");
            showAdmixtureStockInactiveDialog(request_data);
        }else if(type == 13){
            console.log("test");
            showCementRateInactiveDialog(request_data);
        }else if(type == 14){
            console.log("test");
            showSandRateInactiveDialog(request_data);
        }else if(type == 15){
            console.log("test");
            showAggregateRateInactiveDialog(request_data);
        }else if(type == 16){
            console.log("test");
            showFlyashRateInactiveDialog(request_data);
        }else if(type == 17){
            console.log("test");
            showAdmixtureRateInactiveDialog(request_data);
        }else if(type == 18){
            console.log("test");
            showWaterRateInactiveDialog(request_data);
        }

    });
}

var getParamsForFilter = function (url) {
	var params = {};
    var param_string = "";
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
    var is_after = 0;
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);

       
        param_string += pair[0]+'='+decodeURIComponent(pair[1])+'&';
        

        
	}

	// return params;
    param_string = param_string.substring(0, param_string.length - 1);
	return param_string;
};

function resendOTP(){
    
    $.ajax({
        type: "GET", 
        url: BASE_URL+"/supplier/request_otp", 
        data: {},
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            $("#overlay").fadeOut(300);
            var fiveMinutes = 60 * 2,
                
            display = $('#time_forgot_pass');
            startTimer(fiveMinutes, display, "supplier_otp_verify");
            if(data["status"] == 200){
                showToast("OTP Sent successfully to your registered mobile number" ,"Success");

            }else{
                showToast("Somethig went wrong, Please try again","Error");
            }

        }
    });

}

function requestOTP(type,request_data,csrf){
    
    var final_data = {};

    // if(csrf == "update_password"){
    if(csrf != undefined){
        final_data = request_data+"&event_type="+csrf;
    }
    // console.log(final_data);
    $.ajax({
        type: "GET", 
        url: BASE_URL+"/supplier/request_otp", 
        data: final_data,
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            
            $("label.error").remove();
            $(".form-control").removeClass("error");
            if(data["status"] == 200){  //Success
                if(csrf == "update_password"){
                    $('#change-password-popup').modal('hide');
                }else if(csrf == "edit_admix_stock"){
                    $('#add-admixture-stock-details').modal('hide');
                }else if(csrf == "edit_sand_stock"){
                    $('#add-sand-stock-details').modal('hide');
                }else if(csrf == "edit_agg_stock"){
                    $('#add-aggregate-stock-details').modal('hide');
                }else if(csrf == "edit_cement_stock"){
                    $('#add-cement-stock-details').modal('hide');
                }else if(csrf == "edit_flyAsh_stock"){
                    $('#add-flyash-stock-details').modal('hide');
                }else if(csrf == "reassign_order"){
                    $('#reassign-order-popup').modal('hide');
                    $('#reassign-reject-order-popup').modal('hide');
                }else if(csrf == "edit_cement_rate"){
                    $('#add-cement-rate-details').modal('hide');
                }else if(csrf == "edit_sand_rate"){
                    $('#add-sand-rate-details').modal('hide');
                }else if(csrf == "edit_agg_rate"){
                    $('#add-aggregate-rate-details').modal('hide');
                }else if(csrf == "edit_flyAsh_rate"){
                    $('#add-flyash-rate-details').modal('hide');
                }else if(csrf == "edit_admix_rate"){
                    $('#add-admixture-rate-details').modal('hide');
                }else if(csrf == "edit_water_rate"){
                    $('#add-water-rate-details').modal('hide');
                }
                var OTP_mob_number = $("#OTP_mob_number").val();
                console.log("OTP_mob_number:"+OTP_mob_number);
                if(OTP_mob_number!=undefined){
                    // OTP_mob_number = OTP_mob_number.replace(OTP_mob_number.substring(5, 11),"XXXXXX");
                    $('#OTP_mob_no').html(OTP_mob_number);     
                }
                showToast("OTP Sent successfully to your registered mobile number" ,"Success");
                var fiveMinutes = 60 * 2;
                
                display = $('#time_forgot_pass');
                startTimer(fiveMinutes, display, "supplier_otp_verify");
                 // Active In Active User
                    $("#otp").val("");
                    $("#otp_error").html("");

                    var OTP_dialog = function(){

                        var otp = $("#otp").val();
                        if(otp !== ''){
                            // console.log("numeric..."+$.isNumeric(otp));
                            if($.isNumeric(otp)){
                                if((otp.length === 6) ){    
                                    // $('#otpModal').modal('hide');  
                                    
                                    request_data["authentication_code"] = otp;

                                    if(type ==1){ //Update Ticket status
                                        
                                        changePassword(request_data,otp);
                                    }
                                    if(type ==2){ //Update Ticket status
                                        
                                        orderAccept(request_data['_id']+'','REJECTED',otp); //Order Reject 
                                    }
                                    if(type == 3){ //Update Ticket status
                                        
                                        surpriseOrderAccept(request_data['_id']+'','REJECTED',otp); // Suprprice Order Reject 
                                    }
                                    if(type == 4){ //Update Ticket status
                                        
                                        orderAceeptOrNot("off",otp);
                                    }
                                    if(type == 5){ //Update Ticket status
                                        
                                        updateCementStock(request_data,otp);
                                    }
                                    if(type == 6){ //Update Ticket status                                        
                                        deleteCementStock(request_data,otp);
                                    }
                                    if(type == 7){ //Update Ticket status
                                        updateSandStock(request_data,otp);
                                    }
                                    if(type == 8){ //Update Ticket status
                                        deleteSandStock(request_data,otp);
                                    }
                                    if(type == 9){ //Update Ticket status
                                        updateAggregateStock(request_data,otp);
                                    }
                                    if(type == 10){ //Update Ticket status
                                        deleteAggregateStock(request_data,otp);
                                    }
                                    if(type == 11){ //Update Ticket status
                                        updateFlyashStock(request_data,otp);
                                    }
                                    if(type == 12){ //Update Ticket status
                                        deleteFlyashStock(request_data,otp);
                                    }
                                    if(type == 13){ //Update Ticket status
                                        updateAdmixtureStock(request_data,otp);
                                    }
                                    if(type == 14){ //Update Ticket status
                                        deleteAdmixtureStock(request_data,otp);
                                    }
                                    if(type == 15){ //Update Ticket status
                                        reassigningOrders(request_data,otp);
                                    }
                                    if(type == 16){ //Update Ticket status
                                        updateCementRate(request_data,otp);
                                    }
                                    if(type == 17){ //Update Ticket status
                                        updateSandRate(request_data,otp);
                                    }
                                    if(type == 18){ //Update Ticket status
                                        updateAggregateRate(request_data,otp);
                                    }
                                    if(type == 19){ //Update Ticket status
                                        updateFlyashRate(request_data,otp);
                                    }
                                    if(type == 20){ //Update Ticket status
                                        updateAdmixtureRate(request_data,otp);
                                    }
                                    if(type == 21){ //Update Ticket status
                                        updateWaterRate(request_data,otp);
                                    }
                                }else{
                                    $("#otp_error").html("Please enter 6 digits OTP");    
                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                    otpDialog(OTP_dialog);
                                }
                            }else{
                                $("#otp_error").html("OTP should be in number");
                                // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                otpDialog(OTP_dialog);
                            }
                        }else{
                            $("#otp_error").html("Please enter OTP");
                            // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                            // $("#otpCancel").unbind().one("click", fClose);
                            otpDialog(OTP_dialog);
                        }

                    };

                    otpDialog(OTP_dialog);
                
            }else{ //Error
                if(csrf == "update_password"){
                    showErrorMsgInForm("update_password_form",data["error"]["message"]);
                }else if(csrf == "edit_admix_stock"){
                    showErrorMsgInForm("add_admixture_stock_form",data["error"]["message"]);
                }else if(csrf == "edit_sand_stock"){
                    showErrorMsgInForm("add_sand_stock_form",data["error"]["message"]);
                }else if(csrf == "edit_agg_stock"){
                    showErrorMsgInForm("add_aggregate_stock_form",data["error"]["message"]);
                }else if(csrf == "edit_cement_stock"){
                    showErrorMsgInForm("add_cement_stock_form",data["error"]["message"]);
                }else if(csrf == "edit_flyAsh_stock"){
                    showErrorMsgInForm("add_flyash_stock_form",data["error"]["message"]);
                }else{
                    showToast(""+data["error"]["message"],"Error");
                }
            }            
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });

}

function requestOTPToMaster(type,request_data,csrf){
    
    $.ajax({
        type: "GET", 
        url: BASE_URL+"/supplier/request_otp_to_master", 
        data: {},
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            $("label.error").remove();
            $(".form-control").removeClass("error");
            if(data["status"] == 200){  //Success
                // var OTP_mob_number = $("#OTP_mob_number").val();
                
                if(type ==3){
                    $('#add-profit-margin-details').modal('hide');
                }
                if(type ==5){
                    $('#add-overhead-margin-details').modal('hide');
                }

                var OTP_mob_number = data["data"]["mobile_number"];
                $("#OTP_label").html("Enter the 6 digits OTP sent on master vendor's mobile no."+OTP_mob_number);
                console.log("OTP_mob_number:"+OTP_mob_number);
                if(OTP_mob_number!=undefined){
                    // OTP_mob_number = OTP_mob_number.replace(OTP_mob_number.substring(5, 11),"XXXXXX");
                    $('#OTP_mob_no').html(OTP_mob_number);       
                }
                showToast("OTP Sent successfully to your master vendor mobile number" ,"Success");
                var fiveMinutes = 60 * 2;
                
                display = $('#time_forgot_pass');
                startTimer(fiveMinutes, display, "supplier_otp_verify");
                 // Active In Active User
                    $("#otp").val("");
                    $("#otp_error").html("");

                    var OTP_dialog = function(){

                        var otp = $("#otp").val();
                        if(otp !== ''){
                            // console.log("numeric..."+$.isNumeric(otp));
                            if($.isNumeric(otp)){
                                if((otp.length === 6) ){    
                                    $('#otpModal').modal('hide');  
                                    
                                    request_data["authentication_code"] = otp;

                                    if(type ==1){ //Update Ticket status
                                        
                                        orderAceeptOrNot("off",otp);
                                    }
                                    if(type ==2){ //Update Ticket status
                                        
                                        orderAceeptOrNot("on",otp);
                                    }
                                    if(type ==3){ //Update Ticket status
                                        
                                        updateProfitMargin(request_data,otp);
                                    }
                                    if(type ==4){ //Update Ticket status
                                        
                                        deleteProfitMargin(request_data,otp);
                                    }
                                    
                                    if(type ==5){ //Update Ticket status
                                        
                                        updateOverheadMargin(request_data,otp);
                                    }
                                    if(type ==6){ //Update Ticket status
                                        
                                        deleteOverheadMargin(request_data,otp);
                                    }
                                    
                                }else{
                                    $("#otp_error").html("Please enter 6 digits OTP");    
                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                    otpDialog(OTP_dialog);
                                }
                            }else{
                                $("#otp_error").html("OTP should be in number");
                                // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                otpDialog(OTP_dialog);
                            }
                        }else{
                            $("#otp_error").html("Please enter OTP");
                            // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                            // $("#otpCancel").unbind().one("click", fClose);
                            otpDialog(OTP_dialog);
                        }

                    };

                    otpDialog(OTP_dialog);
                
            }else{ //Error
                showToast("Somethig went wrong, Please try again","Error");
            }            
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });

}


function requestOTPToPlant(type,request_data,is_mobile_change,is_email_change,is_edit){
    
    var plant_mobile = $("#mobile_or_email").val();
    var plant_email = $("#email").val();
    var final_data = {};
    var msg = "";
    if(is_edit == 0){

        final_data = {
            'plant_mobile': plant_mobile,
            'plant_email' : plant_email
        }

        msg = "Enter the 6 digits OTP sent on given mobile no "+plant_mobile+" and email "+plant_email;

    }else{

        if(is_mobile_change == 1 && is_email_change == 1){

            final_data = {
                'plant_mobile': plant_mobile,
                'plant_email' : plant_email
            }

            msg = "Enter the 6 digits OTP sent on given mobile no "+plant_mobile+" and email "+plant_email;

        }else if(is_mobile_change == 1){
        
            final_data = {
                'plant_mobile': plant_mobile,
             
            }

            msg = "Enter the 6 digits OTP sent on given mobile no "+plant_mobile;

        }else if(is_email_change == 1){
        
            final_data = {
             
                'plant_email' : plant_email
            }

            msg = "Enter the 6 digits OTP sent on given email "+plant_email;

        }

    }
    // console.log(final_data);

    $.ajax({
        type: "GET", 
        url: BASE_URL+"/supplier/request_otp_to_plant", 
        data: final_data,
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            $("label.error").remove();
            $(".form-control").removeClass("error");
            if(data["status"] == 200){  //Success

                $("#plant_mobile_otp").val('');
                $("#plant_email_otp").val('');

                var OTP_mob_number = $("#OTP_mob_number").val();
                
                // var OTP_mob_number = data["data"]["mobile_number"];
                $("#plant_OTP_label").html(msg);
                // console.log("OTP_mob_number:"+OTP_mob_number);
                if(OTP_mob_number!=undefined){
                    // OTP_mob_number = OTP_mob_number.replace(OTP_mob_number.substring(5, 11),"XXXXXX");
                    $('#OTP_mob_no').html(OTP_mob_number);       
                }
                showToast(""+msg ,"Success");
                var fiveMinutes = 60 * 2;
                
                display = $('#time_forgot_pass');
                startTimer(fiveMinutes, display, "supplier_otp_verify");
                 // Active In Active User
                    $("#plant_mobile_otp").val("");
                    $("#plant_mobile_otp_error").html("");
                    $("#plant_email_otp_error").html('');

                    if(is_edit == 1){
                        if(is_mobile_change == 1){
                            $("#plant_mobile_otp").show();
                        }else{
                            $("#plant_mobile_otp").hide();
                        }
                        
                        if(is_email_change == 1){
                            $("#plant_email_otp").show();
                        }else{
                            $("#plant_email_otp").hide();
                        }
                    }

                    var OTP_dialog = function(){

                        var otp = $("#plant_mobile_otp").val();
                        var email_otp = $("#plant_email_otp").val();
                        
                        if(is_edit == 0){
                            if(otp !== ''){
                                // console.log("numeric..."+$.isNumeric(otp));
                                if($.isNumeric(otp)){
                                    if((otp.length === 6) ){    
                                        if(email_otp != ''){
                                            if($.isNumeric(email_otp)){

                                                if((email_otp.length === 6) ){
                                                
                                                    $('#Plant_otpModal').modal('hide');  
                                        
                                                    request_data["authentication_code"] = otp;

                                                    if(type ==1){ //Update Ticket status
                                                        
                                                        
                                                        addAddress(request_data,email_otp,otp);
                                                    }
                                                    if(type ==2){ //Update Ticket status
                                                        
                                                        
                                                        editAddress(request_data,email_otp,otp);
                                                    }

                                                }else{
                                                    $("#plant_mobile_otp_error").html("");
                                                    $("#plant_email_otp_error").html("Please enter 6 digits OTP");    
                                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                                    plantOTPDialog(OTP_dialog);
                                                }


                                            }else{
                                                $("#plant_mobile_otp_error").html("");
                                                $("#plant_email_otp_error").html("OTP should be in number");
                                                // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                                plantOTPDialog(OTP_dialog);
                                            }

                                        }else{
                                            $("#plant_mobile_otp_error").html("");
                                            $("#plant_email_otp_error").html("Please enter OTP");
                                            // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                            // $("#otpCancel").unbind().one("click", fClose);
                                            plantOTPDialog(OTP_dialog);
                                        }
                                        
                                        
                                    }else{
                                        $("#plant_mobile_otp_error").html("Please enter 6 digits OTP");    
                                        // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                        plantOTPDialog(OTP_dialog);
                                    }
                                }else{
                                    $("#plant_mobile_otp_error").html("OTP should be in number");
                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                    plantOTPDialog(OTP_dialog);
                                }
                            }else{
                                $("#plant_mobile_otp_error").html("Please enter OTP");
                                // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                // $("#otpCancel").unbind().one("click", fClose);
                                plantOTPDialog(OTP_dialog);
                            }
                        }else{

                            if((is_mobile_change == 1) && (is_email_change == 1)){

                                if(otp !== ''){
                                    // console.log("numeric..."+$.isNumeric(otp));
                                    if($.isNumeric(otp)){
                                        if((otp.length === 6) ){    
                                            if(email_otp != ''){
                                                if($.isNumeric(email_otp)){
    
                                                    if((email_otp.length === 6) ){
                                                    
                                                        $('#Plant_otpModal').modal('hide');  
                                            
                                                        request_data["authentication_code"] = otp;
    
                                                        if(type ==2){ //Update Ticket status                                                        
                                                            
                                                            editAddress(request_data,email_otp,otp);
                                                        }
    
                                                    }else{
                                                        $("#plant_mobile_otp_error").html("");
                                                        $("#plant_email_otp_error").html("Please enter 6 digits OTP");    
                                                        // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                                        plantOTPDialog(OTP_dialog);
                                                    }
    
    
                                                }else{
                                                    $("#plant_mobile_otp_error").html("");
                                                    $("#plant_email_otp_error").html("OTP should be in number");
                                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                                    plantOTPDialog(OTP_dialog);
                                                }
    
                                            }else{
                                                $("#plant_mobile_otp_error").html("");
                                                $("#plant_email_otp_error").html("Please enter OTP");
                                                // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                                // $("#otpCancel").unbind().one("click", fClose);
                                                plantOTPDialog(OTP_dialog);
                                            }
                                            
                                            
                                        }else{
                                            $("#plant_mobile_otp_error").html("Please enter 6 digits OTP");    
                                            // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                            plantOTPDialog(OTP_dialog);
                                        }
                                    }else{
                                        $("#plant_mobile_otp_error").html("OTP should be in number");
                                        // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                        plantOTPDialog(OTP_dialog);
                                    }
                                }else{
                                    $("#plant_mobile_otp_error").html("Please enter OTP");
                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                    // $("#otpCancel").unbind().one("click", fClose);
                                    plantOTPDialog(OTP_dialog);
                                }
    
                            }else
                            if(is_mobile_change == 1){

                                if(otp !== ''){
                                    // console.log("numeric..."+$.isNumeric(otp));
                                    if($.isNumeric(otp)){
                                        if((otp.length === 6) ){    
                                            
                                            $('#Plant_otpModal').modal('hide');  
                                        
                                            request_data["authentication_code"] = otp;

                                            if(type ==2){ //Update Ticket status
                                                
                                                
                                                editAddress(request_data,email_otp,otp);
                                            }
                                            
                                        }else{
                                            $("#plant_mobile_otp_error").html("Please enter 6 digits OTP");    
                                            // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                            plantOTPDialog(OTP_dialog);
                                        }
                                    }else{
                                        $("#plant_mobile_otp_error").html("OTP should be in number");
                                        // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                        plantOTPDialog(OTP_dialog);
                                    }
                                }else{
                                    $("#plant_mobile_otp_error").html("Please enter OTP");
                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                    // $("#otpCancel").unbind().one("click", fClose);
                                    plantOTPDialog(OTP_dialog);
                                }

                            }else if(is_email_change == 1){

                                if(email_otp != ''){
                                    if($.isNumeric(email_otp)){

                                        if((email_otp.length === 6) ){
                                        
                                            $('#Plant_otpModal').modal('hide');  
                                
                                            request_data["authentication_code"] = otp;

                                            if(type ==2){ //Update Ticket status
                                                
                                                
                                                editAddress(request_data,email_otp,otp);
                                            }

                                        }else{
                                            $("#plant_mobile_otp_error").html("");
                                            $("#plant_email_otp_error").html("Please enter 6 digits OTP");    
                                            // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                            plantOTPDialog(OTP_dialog);
                                        }


                                    }else{
                                        $("#plant_mobile_otp_error").html("");
                                        $("#plant_email_otp_error").html("OTP should be in number");
                                        // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                        plantOTPDialog(OTP_dialog);
                                    }

                                }else{
                                    $("#plant_mobile_otp_error").html("");
                                    $("#plant_email_otp_error").html("Please enter OTP");
                                    // $("#otpOk").unbind().one('click', requestOTP(type,request_data,csrf));
                                    // $("#otpCancel").unbind().one("click", fClose);
                                    plantOTPDialog(OTP_dialog);
                                }

                            }

                        }

                    };

                    plantOTPDialog(OTP_dialog);
                
            }else{ //Error
                // showToast("Somethig went wrong, Please try again","Error");
                if(data["error"]["message"] != undefined){
                    // showToast(""+data["error"]["message"],"Error");
                    showErrorMsgInForm("add_address_form",data["error"]["message"].replace("_", " "));
                }else{
                    showToast("Somethig went wrong, Please try again","Error");
                }
                
            }            
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });

}



function requestOTPforEditProfile(type,request_data,is_old_mobile,is_old_email,is_new_mobile,is_new_email,is_edit,is_resend){

    var param = "";
    var value = "";
    var type = "";
    var final_data = {};

    if(is_old_mobile == 1){
        param = "old_mobile_no";
        value = request_data;
        type = "mobile";
        final_data = {
            "old_mobile_no" : value
        }

        $("#mobile_type").val("old_mobile");
    }
    
    if(is_old_email == 1){
        param = "old_email";
        value = request_data;
        type = "email";

        final_data = {
            "old_email" : value
        }

        $("#email_type").val("old_email");
    }
    
    if(is_new_mobile == 1){
        param = "new_mobile_no";
        value = request_data;
        type = "mobile";

        final_data = {
            "new_mobile_no" : value
        }

        $("#mobile_type").val("new_mobile");
    }
    
    if(is_new_email == 1){
        param = "new_email";
        value = request_data;
        type = "email";

        final_data = {
            "new_email" : value
        }

        $("#email_type").val("new_email");
    }

    $.ajax({
        type: "GET", 
        url: BASE_URL+"/supplier/request_otp_for_edit_profile", 
        data: final_data,
        beforeSend: function(){
            $("#overlay").fadeIn(300);
        },
        success: function(data){
            console.log(data);
            $("label.error").remove();
            $(".form-control").removeClass("error");
            if(data["status"] == 200){  //Success
                
                // var OTP_mob_number = data["data"]["mobile_number"];
                $("#update_mobile_otp_confirm_message").html("Enter the 6 digits OTP sent on given "+type+" "+value);
                $("#update_email_otp_confirm_message").html("Enter the 6 digits OTP sent on given "+type+" "+value);
                // console.log("OTP_mob_number:"+OTP_mob_number);
                
                showToast("OTP Sent successfully" ,"Success");
                $("#time_update_mobile").html("02:00");
                $("#time_update_email").html("02:00");
                var fiveMinutes = 60 * 2;
                
                display = $('#time_update_mobile');
                
                if(param == "old_email" || param == "new_email"){
                    display = $('#time_update_email');
                    
                }

                if(param == "old_mobile_no" || param == "new_mobile_no"){
                    startTimer(fiveMinutes, display, ""+$("#mobile_type").val());
                }

                if(param == "old_email" || param == "new_email"){
                    startTimer(fiveMinutes, display, ""+$("#email_type").val());
                }

                 // Active In Active User                    

                 if(is_resend == 0){
                    $("#new_mobile_no_popup").modal("hide");
                    $("#new_email_no_popup").modal("hide");
                    $("#otp_code_1_mobile").val('');
                    $("#otp_code_1_email").val('');
                    if(param == "old_mobile_no" || param == "new_mobile_no"){
                        $("#update_mobile_profile").modal("show");
                    }
                    
                    if(param == "old_email" || param == "new_email"){
                        $("#update_email_profile").modal("show");
                    }
                }
                
                    
                
            }else{ //Error
                if(data["error"]["message"] != undefined){
                    showToast(""+data["error"]["message"],"Error");
                }else{
                    showToast("Something went wrong","Error");
                }
            }            
            
        },
        complete: function(){
            $("#overlay").fadeOut(300);
        }
    });

}



// Login Module Over..................................................
// Sign up Module Start..................................................

$(document).ready(function(){
    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='supplier_register_form']").validate({
        rules: {
            full_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                a_z_pattern:true,
                noSpace: true,
            },
            company_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
            },
            mobile_number: {
                required: true,
                minlength: 13,
                maxlength: 13,
            },
            email: {
                required: true,
                email_validate: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 20,
                noSpace: true,
            },
            pan_number: {
                required: true,
                pan: true
            },
            gst_number:{
                required: false,
                maxlength: 100,
                gst:true
            },
            company_certification_number: {
                required: function(){
                    $company_type = $("#signup_company_type").val();

                    if($company_type == 'Proprietor'){
                        return false;
                    }
                    return true;
                }
            },
            pancard_image: {
                required: true,
                extension: "jpg|jpeg|pdf"
            },
            company_certification_image : {
                required: function(){
                    $company_type = $("#signup_company_type").val();

                    if($company_type == 'Proprietor'){
                        return false;
                    }
                    return true;
                },
                extension: "jpg|jpeg|pdf"
            },
            account_type : {
                
            },
            company_type: {
                required: true
            },
            landline_number: {
                number: true
            },
            gst_certification_image: {
                required: false,
                extension: "jpg|jpeg|pdf"
            },
            terms_condition: {
                required: true
            }
            
        },
        messages: {
            company_certification_image: {
                extension: "Please select valid file (jpg, jpeg, pdf) format"
            },
            pancard_image: {
                extension: "Please select valid file (jpg, jpeg, pdf) format"
            },
            gst_certification_image: {
                extension: "Please select valid file (jpg, jpeg, pdf) format"
            },
            email:{
                email_validate: "Please enter valid email address"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "pancard_image" ) {
              error.insertAfter("#pancard_image_div");
            }else if (element.attr("name") == "pancard_image" ) {
              error.insertAfter("#pancard_image_div");
            } else if (element.attr("name") == "company_certification_image" ) {
                error.insertAfter("#company_certification_image_div");
              }else if (element.attr("name") == "gst_certification_image" ) {
                error.insertAfter("#gst_certification_image_div");
              }else if (element.attr("name") == "terms_condition" ) {
                error.insertAfter("#terms_condition_div");
              } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            // $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // $("#" + elem.attr("id") + "_text").addClass(errorClass);
            // $("#region_served_text_chosen ul li input").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            // $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // $("#" + elem.attr("id") + "_text").removeClass(errorClass);
            // $("#region_served_text_chosen ul li input").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $("label.error").html("");
            $(".error").removeClass("alert-danger");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            // var form_values = $("form[name='vehicle_add_sub_cat_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='supplier_register_form']")[0].submit();
            // addVehicleSubCategory(form_values);

            // var form_values = new FormData(form);
            // signup(form_values);

            

            emailVerify();

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $("label.error").html("");
            $(".error").removeClass("alert-danger");
        }
    });

    $("form[name='email_verify_popup']").validate({
        rules: {
            email_otp: {
                required: true,
                minlength: 6,
                maxlength: 6,
                number:true,
            },
            
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "pancard_image" ) {
              error.insertAfter("#pancard_image_div");
            } else if (element.attr("name") == "company_certification_image" ) {
                error.insertAfter("#company_certification_image_div");
              }else if (element.attr("name") == "gst_certification_image" ) {
                error.insertAfter("#gst_certification_image_div");
              } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("id") + "_text").addClass(errorClass);
            $("#region_served_text_chosen ul li input").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("id") + "_text").removeClass(errorClass);
            $("#region_served_text_chosen ul li input").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $(".error").html("");
            $(".error").removeClass("alert-danger");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            // var form_values = $("form[name='vehicle_add_sub_cat_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='supplier_register_form']")[0].submit();
            // addVehicleSubCategory(form_values);

            // var form = $("form[name='supplier_register_form']");

            var form_values = new FormData(document.getElementById("supplier_register_form"));
            console.log(form_values);
            var email_otp = $("#email_otp").val();
            form_values.append('authentication_code', ''+email_otp);
            form_values.append('register_btn', 'register_btn');
            console.log(form_values);
            $("#Email_very_otp").modal("hide");
            signup(form_values);           

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $(".error").html("");
            $(".error").removeClass("alert-danger");
        }
    });

    $("#plant_capacity_per_hour").change(function() {
        
        var plant_capacity_per_hour = $("#plant_capacity_per_hour").val() != null ? $("#plant_capacity_per_hour").val() : 0;
        var no_of_plants = $("#no_of_plants").val() != null ? $("#no_of_plants").val() : 0;
        var no_of_operation_hour = $("#no_of_operation_hour").val() != null ? $("#no_of_operation_hour").val() : 0;

        console.log("plant_capacity_per_hour.."+plant_capacity_per_hour);

        plantCapacity(plant_capacity_per_hour,no_of_plants,no_of_operation_hour);

    });
    
    $("#no_of_plants").change(function() {

        var plant_capacity_per_hour = $("#plant_capacity_per_hour").val() != null ? $("#plant_capacity_per_hour").val() : 0;
        var no_of_plants = $("#no_of_plants").val() != null ? $("#no_of_plants").val() : 0;
        var no_of_operation_hour = $("#no_of_operation_hour").val() != null ? $("#no_of_operation_hour").val() : 0;

        console.log("plant_capacity_per_hour.."+plant_capacity_per_hour);

        plantCapacity(plant_capacity_per_hour,no_of_plants,no_of_operation_hour);
    });
    
    $("#no_of_operation_hour").change(function() {

        var plant_capacity_per_hour = $("#plant_capacity_per_hour").val() != null ? $("#plant_capacity_per_hour").val() : 0;
        var no_of_plants = $("#no_of_plants").val() != null ? $("#no_of_plants").val() : 0;
        var no_of_operation_hour = $("#no_of_operation_hour").val() != null ? $("#no_of_operation_hour").val() : 0;

        console.log("plant_capacity_per_hour.."+plant_capacity_per_hour);

        plantCapacity(plant_capacity_per_hour,no_of_plants,no_of_operation_hour);
    });

    

});

function signup(form_values){

    responsehandlerWithFiles("supplier/register",
                    "POST", 
                    form_values, 
                    '#sign-up-form-3',
                    'You are successfully registered. Please Login',
                    0,
                    "supplier_register_form"
                );

}

function emailVerify(){
    var email = $("#supplier_email").val();
    var form_values = {
        "email": email
    }

    // responsehandler("supplier/email_verify",
    //                 "POST", 
    //                 form_values, 
    //                 '#sign-up-form-3',
    //                 'Verification code sent sccessfully',
    //                 0,
    //                 "supplier_register_form"
    //             );


    customResponseHandler("supplier/email_verify",
                "GET", 
                form_values, 
                function response(data){
                    console.log(data);
                    
                    if((data["status"] >= 200) && (data["status"] < 300)){
                        
                        showToast("Verification code sent sccessfully","Success");
                        // $("#sign-up-form-1").modal('hide');

                        $("#supplier_register_email_id").html(email);

                        // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                        // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                        var fiveMinutes = 60 * 2;
                        display = $('#time_forgot_pass');
                        startTimer(fiveMinutes, display, "supplier_email_verify");
                        resetForm('email_verify_popup','Email_very_otp');
                        $("#Email_very_otp").modal('show');
                    }else{
                        showToast(""+data["error"]["message"],"Error");
                        // $('#sign-up-form-1_error').html(data["error"]["message"]);
                    }

                }
            );           

}

function plantCapacity(plant_capacity_per_hour,no_of_plants,no_of_operation_hour){

    if((plant_capacity_per_hour > 0) && (no_of_operation_hour > 0)){

        capacity = (plant_capacity_per_hour * no_of_operation_hour);

        $("#plant_capacity").html("Your Total plant capacity is "+capacity+" Cu. Mtr./day");

    }else{
        $("#plant_capacity").html("");
    }


}


// full_name: "Full name is required",
//             company_name: "Company name is required",
//             password: "Password is required",
//             mobile_number: "Mobile number is required",
//             email: "Email is required",
//             pan_number:{
//                 required: "Please add min load capacity"
//             }, 
//             company_certification_number: {
//                 required: "Please add max load capacity"
//             },
//             pancard_image: {
//                 required: "Pan card image is required"
//             },
//             company_certification_image : {
//                 required: "Company Certification image is required"
//             },
//             account_type : {
//                 required: "Account type is required"
//             }

// Sign up Module Over..................................................

// Contact Details Module Start..................................................

$(document).ready(function(){

    /*input mobile no with flag*/
    // var input = document.querySelector("#mobileno");
    // window.intlTelInput(input, {
    //   utilsScript: "js/utils.js" // just for formatting/placeholders etc
    // });

    // var input = document.querySelector("#altnateno");
    // window.intlTelInput(input, {
    //   utilsScript: "js/utils.js" // just for formatting/placeholders etc
    // });

    // var input = document.querySelector("#whtsappno");
    // window.intlTelInput(input, {
    //   utilsScript: "js/utils.js" // just for formatting/placeholders etc
    // });
    /*input mobile no with flag end*/

    $("form[name='supplier_otp_verify_form']").validate({
        rules: {
            otp_code: {
                required: true,
                number:true,
                minlength:6,
                maxlength:6
            }
        },
        messages: {
           
        },
        errorPlacement: function(error, element) {
            
            error.insertAfter(element);
          },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="supplier_otp_verify_form"] p.error').remove();

            $("form[name='supplier_otp_verify_form']")[0].submit();
            
            

        
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="supplier_otp_verify_form"] p.error').remove();
            $('button[name="otp_btn"]').button('reset');
        }
    });
    
    $("form[name='supplier_email_verify_form']").validate({
        rules: {
            email_mobile: {
                required: true,
                mobile_validation: true,
                minlength: 13,
                maxlength: 13,
                noDecimal: true
                // mobile_or_email_validate: true,
            }
        },
        messages: {
            email_mobile: {
                minlength: "Please enter valid mobile number",
                maxlength: "Please enter valid mobile number"
            }
        },
        errorPlacement: function(error, element) {
            $( ".invalid" ).remove();
            error.insertAfter(element);
          },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="supplier_email_verify_form"] p.error').remove();

            $("form[name='supplier_email_verify_form']")[0].submit();         
        
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="supplier_email_verify_form"] p.error').remove();
            $('button[name="register_btn"]').button('reset');
        }
    });
    
    
    $("form[name='supplier_login_form']").validate({
        rules: {
            email_mobile: {
                required: true,
                mobile_or_email_validate: true,
                noDecimal:true
            },
            password: {
                required: true,
                noSpace: true,
                minlength: 6,
                maxlength: 20
            }
        },
        messages: {
           
        },
        errorPlacement: function(error, element) {
            $( ".invalid" ).remove();
            error.insertAfter(element);
          },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="supplier_login_form"] p.error').remove();

            $("form[name='supplier_login_form']")[0].submit();
            
            

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="supplier_login_form"] p.error').remove();
        }
    });
    
    $("form[name='add_contact_detail_form']").validate({
        rules: {
            person_name: {
                required: true,
                noSpace:true,
                a_z_pattern: true
            },
            title: {
                required: true,
                noSpace: true
            },
            mobile_number: {
                required: true,
                mobile_validation: true,
                noDecimal:true,
                minlength: 13,
                maxlength: 13
            },
            email: {
                required: true,
                email: true
            },
            alt_mobile_number: {
                mobile_validation: true,
                noDecimal:true,
            },
            whatsapp_number: {
                mobile_validation: true,
                noDecimal:true,
            },
            landline_number: {
                number: true,
                noDecimal:true,
            }
        },
        messages: {
            mobile_number:{
                minlength: "Please enter atleast 12 character"
            },
            mobile_number:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "mobile_number" ) {
              error.insertAfter("#mobile_number_div");
            }else if (element.attr("name") == "alt_mobile_number" ) {
              error.insertAfter("#alt_mobile_number_div");
            } else if (element.attr("name") == "whatsapp_number" ) {
              error.insertAfter("#whatsapp_number_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_contact_detail_form"] p.error').remove();

            var form_type = $("form[name='add_contact_detail_form'] #add_contact_detail_form_type").val();
            var form_values = $("form[name='add_contact_detail_form']").serialize();
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addContactDetails(form_values);    
            }else{
                updateContactDetails(form_values);
            }
            
            

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_contact_detail_form"] p.error').remove();
        }
    });

});

function addContactDetails(request_data){
    var url = 'supplier/contact_details/add';

    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#add-contact-details",
                    "Contact Details Added successfully",
                    0,
                    "add_contact_detail_form"
                );

}

function updateContactDetails(request_data){
    var url = 'supplier/contact_details/update';

    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#add-contact-details",
                    "Contact Details updated successfully",
                    0,
                    "add_contact_detail_form"
                );

}

function showContactDetailEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_contact_detail_form');

    $("#add_contact_details_title").html("Edit Contact Details");

    $('#person_name').val(data['person_name']);
    $('#title').val(data['title']);
    $('#person_mobile_no').val(data['mobile_number']);
    $('#email').val(data['email']);
    // console.log(data['alt_mobile_number']);
    if(data['alt_mobile_number'] == undefined){
        // $('#person_alt_no').val("");
    }else{
        $('#person_alt_no').val(data['alt_mobile_number']);
    }
    
    if(data['whatsapp_number'] == undefined){
        // $('#person_whats_no').val("");
    }else{
        $('#person_whats_no').val(data['whatsapp_number']);
    }
    
    $('#landline_number').val(data['landline_number']);

    $('#add_contact_detail_form_type').val("edit");
    $('#contact_detail_id').val(data['_id']);

    $('#add-contact-details').modal('show');

}

// Contact Details Module Over..................................................


// Bank Details Module Start..................................................

$(document).ready(function(){

    $("form[name='add_bank_detail_form']").validate({
        rules: {
            account_holder_name: {
                required: true,
                noSpace:true
            },
            account_number: {
                required: true,
                number:true,
                noDecimal:true,
            },
            ifsc: {
                required: true
            },
            branch_name: {
                required: true
            },
            account_type: {
                required: true
            },
            bank_name: {
                required: true,
                noSpace:true
            },
            cancelled_cheque_image : {
                required: function(){
                    return $("form[name='add_bank_detail_form'] #add_bank_detail_form_type").val() == 'add' ? true :false; 
                },
                extension: "jpg|jpeg"
            },
            
        },
        messages: {
            cancelled_cheque_image : {
                extension: "Please enter valid file (jpg, jpeg) formats"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_bank_detail_form"] p.error').remove();

            var form_type = $("form[name='add_bank_detail_form'] #add_bank_detail_form_type").val();
            // var form_values = $("form[name='add_bank_detail_form']").serialize();
            console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            if(form_type === 'add'){
                addBankDetails(form_values);
            }else{
                updateBankDetails(form_values);
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_bank_detail_form"] p.error').remove();
        }
    });

});

function addBankDetails(form_values){

    responsehandlerWithFiles("supplier/bank_details/add", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Bank Details Added successfully",
                    0,
                    "add_bank_detail_form"
                );

}

function showBankDetailEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_bank_detail_form');

    $("#add_bank_details_title").html("Edit Bank Details");

    $('#account_holder_name').val(data['account_holder_name']);
    $('#bank_name').val(data['bank_name']);
    $('#account_number').val(data['account_number']);
    $('#ifsc').val(data['ifsc']);
    if(data['branch_name'] != undefined){
        $('#branch_name').val(data['branch_name']);
    }
    $('#account_type').val(data['account_type']);
    

    $('#add_bank_detail_form_type').val("edit");
    $('#bank_detail_id').val(data['_id']);

    $('#add-bank-details').modal('show');

}

function updateBankDetails(form_values){

    responsehandlerWithFiles("supplier/bank_details/update", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Bank Details updated successfully",
                    0,
                    "add_bank_detail_form"
                );

}

function showSetDefaultDialog(data,csrf){

    data = JSON.parse(data);

    $('#make_default_bank_detail_id').val(data["_id"]);
    $('#make_default_bank_detail_csrf').val(csrf);

    $('#make-default-alert-popup').modal('show');

}

function setBankAccountDefault(){

    var bank_detail_id = $('#make_default_bank_detail_id').val();
    var csrf = $('#make_default_bank_detail_csrf').val();
    var request_data = {'bank_detail_id':bank_detail_id,'_token':csrf}; 
    responsehandler('supplier/bank_details/make_default', 
        "POST", 
        request_data, 
        "#make-default-alert-popup",
        "Account set default successfully"
    );

}

// Bank Details Module Over..................................................

// Product Module Start..................................................

$(document).ready(function(){

    

    $('#daily_category_id').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
       
        console.log("valueSelected..."+valueSelected);

        getSubCategory(valueSelected,"daily_sub_category_id");

    });


    $('#category_id').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
       
        console.log("valueSelected..."+valueSelected);

        getSubCategory(valueSelected,"sub_category_id");

    });


    $("form[name='add_product_form']").validate({

        rules:{
            category_id: 'required',
            sub_category_id: 'required',
            contact_person_id: 'required',
            pickup_address_id: 'required',
            minimum_order:{
                required:true,
                number: true
            },
            unit_price:{
                required:true,
                number: true
            },
            quantity:{
                required:true,
                number: true,
                minStrict: 0
            },
            minimum_order:{
                required:true,
                number: true,
                minStrict: 0
            },
        },
        messages:{
            
        },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_product_form"] p.error').remove();
            var form_type = $("form[name='add_product_form'] #add_product_form_type").val();
            var form_values = $("form[name='add_product_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            console.log("form_type ..."+form_type );

            if(form_type == 'add'){
                addProduct(form_values);
                
            }
            else if(form_type == 'edit'){
                updateProductDetails(form_values);
                
            }
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_product_form"] p.error').remove();
        }

    });


    /*qty script start*/
    $('.add').click(function () {
        // console.log($(this).prev().val());
        // if ($(this).prev().val()) {

        $(this).css('background-color','#687ae8');
        $(this).css('color','#fff');
        $(this).css('border','1px solid #687ae8');
        
        $('.sub').css('background-color','#fff');
        $('.sub').css('color','#687ae8');
        $('.sub').css('border','1px solid #687ae8');

            $("#qty_digit_error").html("").hide();

            $('#stock_plus_minus_sign_div').html('+');

            $('#plus_or_minus').val('1');

            var stock_update_input = $("#stock_update_input").val();

            // var qty_updated_value = parseInt($(this).prev().val()) + 1;
            var qty_updated_value = parseInt(stock_update_input);

            // $(this).prev().val(qty_updated_value);

            var current_stock = $('#current_stock').val();

            var total_stock = parseInt(current_stock) + parseInt(qty_updated_value);

            $('#total_stock').val(total_stock);
        // }
    });
    $('.sub').click(function () {
        // console.log($(this).next().val());
        // if ($(this).next().val() > 0) {

            $(this).css('background-color','#687ae8');
            $(this).css('color','#fff');
            $(this).css('border','1px solid #687ae8');
            
            $('.add').css('background-color','#fff');
            $('.add').css('color','#687ae8');
            $('.add').css('border','1px solid #687ae8');

            $("#qty_digit_error").html("").hide();

            $('#stock_plus_minus_sign_div').html('-');

            $('#plus_or_minus').val('0');

            var stock_update_input = $("#stock_update_input").val();

            // var qty_updated_value = parseInt($(this).next().val()) - 1;
            var qty_updated_value = parseInt(stock_update_input);

            var current_stock = $('#current_stock').val();

            var total_stock = parseInt(current_stock) - parseInt(qty_updated_value);

            if(total_stock > 0){
                // $(this).next().val(qty_updated_value); 

                $('#total_stock').val(total_stock);
            }else{
                $("#qty_digit_error").html("Total stock should be greater than 0").show();
            }
            
        // }
    });
    /*qty script end*/

    $(document).on("keypress","#stock_update_input",function (e) { 
        //if the letter is not digit then display error and don't type anything
        is_pressed_alpha = 0;
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
           //display error message
           is_pressed_alpha = 1;
           $("#qty_digit_error").html("Digits Only").show().fadeOut("slow");
                  return false;
       }
      });

    $('#stock_update_input').on('input',function(e){
        // console.log("test..."+$('#stock_update_input').val());

        if($('#stock_update_input').val().length > 0){
            var plus_or_minus = $('#plus_or_minus').val();
            $("#qty_digit_error").html("").hide();
            if(plus_or_minus == '1'){

                var qty_updated_value = parseInt($('#stock_update_input').val()) + 0;

                $('#stock_update_input').val(qty_updated_value); 

                var current_stock = $('#current_stock').val();

                var total_stock = parseInt(current_stock) + parseInt(qty_updated_value);

                $('#total_stock').val(total_stock);

            }else if(plus_or_minus == '0'){

                var qty_updated_value = parseInt($('#stock_update_input').val()) + 0;

                $('#stock_update_input').val(qty_updated_value); 

                var current_stock = $('#current_stock').val();  
                var total_stock = parseInt(current_stock) - parseInt(qty_updated_value);

                if(total_stock > 0){                                 

                    $('#total_stock').val(total_stock);
                }else{
                    $("#qty_digit_error").html("Total stock should be greater than 0").show();
                }
                

            }
        }else{
            $('#stock_update_input').val('0');

            var qty_updated_value = parseInt($('#stock_update_input').val()) + 0;

            $('#stock_update_input').val(qty_updated_value); 

            var current_stock = $('#current_stock').val();

            var total_stock = parseInt(current_stock) - parseInt(qty_updated_value);

            $('#total_stock').val(total_stock);

        }

    });


    $("form[name='stock_update_form']").validate({

        rules:{
            
            quantity:{
                required:true,
                number: true,
                minStrict: 0
            }
            
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "quantity" ) {
              error.insertAfter("#stock_quanity_div");
            }  else {
              error.insertAfter(element);
            }

          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="stock_update_form"] p.error').remove();
            // var form_type = $("form[name='stock_update_form'] #stock_update_form_type").val();
            var form_values = $("form[name='stock_update_form']").serialize();
            // var form_values = new FormData(form);
            console.log(form_values);
            // console.log("form_type ..."+form_type );

            updateProductStock(form_values);
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="stock_update_form"] p.error').remove();
        }

    });

    

});

function updateProductStock(form_values){

    responsehandler('supplier/product/udpateStock', 
        "POST", 
        form_values,
        "#change-stock",
        "Product stock updated successfully",
        0,
        "stock_update_form"
    );

}

function editProductStock(current_stock, product_id){
    $('form[name="stock_update_form"] p.error').remove();

    $("#qty_digit_error").html("").hide();

    $('#plus_or_minus').val('1');
    $('#stock_plus_minus_sign_div').html('+');

    $('#current_stock').val(current_stock);
    $('#stock_product_id').val(product_id);

    var stock_update_input = 1;

    $('#stock_update_input').val(stock_update_input);

    var total_stock = parseInt(stock_update_input) + parseInt(current_stock);

    $('#total_stock').val(total_stock);

}

function getSubCategory(category_id, drop_down_id){

    var request_data = ''; 
    var url = 'supplier/product/getSubcat/'+category_id;
    
    customResponseHandler(
        url, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess

            // data = JSON.parse(data);
            var html = '<option disabled="disabled" selected="selected">Select sub category</option>';
        
            $.each(data["data"],function(key,value){
                
                
                    html += '<option value="'+value["_id"]+'">'+value["sub_category_name"]+'</option>';
                            
                
            });
            
            $('#'+drop_down_id ).html(html);

            $('#'+drop_down_id +"_div").show();
            

        }
    );

}

function addProduct(form_values){

    responsehandler('supplier/product/add', 
        "POST", 
        form_values,
        "#add-product-list",
        "Product added successfully",
        0,
        "add_product_form"
    );


    

}

function showEditProductDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_product_form');

    $("#add_product_list_title").html("Edit Product");

    // $("#product_name").val(data['name']);
    
    $("#category_id").append('<option selected>'+data['productCategory']['category_name']+'</option>');
    $("#category_id").attr('disabled','disabled');

    $("#sub_category_id").append('<option selected>'+data['productSubCategory']['sub_category_name']+'</option>');
    $("#sub_category_id").attr('disabled','disabled');

    $("#contact_person").append('<option selected>'+data['supplier']['full_name']+'</option>');
    $("#contact_person").attr('disabled','disabled');

    $("#contact_address").append('<option selected>'+data['address']['line1'] + ','+ data['address']['line2'] +', '+data['address']['city_name'] +', '+ data['address']['pincode']+ ', '+ data['address']['state_name'] + '</option>');
    $("#contact_address").attr('disabled','disabled');

    if(data['unit_price'] != undefined){
        $("#unit_price").val(data['unit_price']);
        
    }
    if(data['quantity'] != undefined){
        $("#product_qty").val(data['quantity']);
        $("#product_qty").attr('disabled','disabled');
    }
    if(data['minimum_order'] != undefined){
        $("#minimum_order").val(data['minimum_order']);
    }    

    if(data['is_available']){
        $('#is_product_avail').prop('checked',true);
    }else{
        $('#is_product_avail').prop('checked',false);
    }
    
    if(data['self_logistics']){
        $('#is_self_logistics').prop('checked',true);
    }else{
        $('#is_self_logistics').prop('checked',false);
    }
    
    if(data['verified_by_admin']){
        $('#verified_by_admin_input').val(true);
    }else{
        $('#verified_by_admin_input').val(false);
    }

    $("#add_product_id").val(data['_id']);
    $("#add_product_form_type").val('edit');

    $("#add-product-list").modal("show");

}

function updateProductDetails(request_data){
    
    
    responsehandler("supplier/product/update", 
                    "POST", 
                    request_data, 
                    "#add-product-list",
                    "Product updated successfully",
                    0,
                    "add_product_form"
                );

}

// Product Module Over..................................................

// Address Module Start..................................................

$(document).ready(function(){

    $('#' + state_name_city_selectID + '_chosen .chosen-search input').keyup(function(){


        // Change No Result Match text to Searching.
      $('#' + state_name_city_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + state_name_city_selectID + '_chosen .chosen-search input').val() + '"');


      clearTimeout(typingTimer);  //Refresh Timer on keyup 
    //   if ($('#' + state_name_city_selectID + '_chosen .chosen-search input').val()) {

           typingTimer = setTimeout(searchCountryState, doneTypingInterval,'supplier/address/getStates',state_name_city_selectID);  //Set timer back if got value on input

    //   }
  
    });
    
    $('#' + state_name_city_filter_selectID + '_chosen .chosen-search input').keyup(function(){
        console.log("test");
        // Change No Result Match text to Searching.
      $('#' + state_name_city_filter_selectID + '_chosen .no-results').html('Searching = "'+ $('#' + state_name_city_filter_selectID + '_chosen .chosen-search input').val() + '"');

      clearTimeout(typingTimer);  //Refresh Timer on keyup
      
      typingTimer = setTimeout(searchCountryState, doneTypingInterval,'supplier/address/getCities',state_name_city_filter_selectID);  //Set timer back if got value on input

  
    });
    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    var vadd_address_form_obj = $("form[name='add_address_form']").validate({
        rules:{
            business_name: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 64
            },
            full_name: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 64
            },
            mobile:{
                required: true,
                minlength: 13,
                maxlength: 13,
                noDecimal: true
            },
            email:{
                required: true,
                email_validate: true
            },
            password:{
                required: function(){

                    var add_address_form_type = $("#add_address_form_type").val();

                    if(add_address_form_type == "edit"){
                        return false;
                    }

                    return true;

                },
                minlength: 6,
                maxlength: 20,
            },
            line1: {
                required: true,
                noSpace: true
            },
            line2: {
                required: true,
                noSpace: true
            },
            state_id: "required",
            city_id: "required",
            pincode: {
                required: true,
                noSpace: true,
                number: true,
                minlength: 6,
                maxlength: 6
            },
            address_type: "required",
            us2_address: "required",
            billing_address_id: "required",
            plant_capacity_per_hour: "required",
            no_of_operation_hour: "required",
            
        },
        messages:{
            mobile:{
                minlength: "Please enter valid mobile number",
                maxlength: "Please enter valid mobile number",
            },
            email:{
                email_validate: "Please enter valid email address"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "state_id" ) {
              error.insertAfter("#state_name_city_chosen");
            } else if (element.attr("name") == "city_id" ) {
                error.insertAfter("#state_name_city_filter_chosen");
              }else if (element.attr("name") == "region_id" ) {
                error.insertAfter("#region_id_div");
              }else if (element.attr("name") == "source_id" ) {
                error.insertAfter("#source_id_div");
              }else if (element.attr("name") == "billing_address_id" ) {
                error.insertAfter("#billing_address_id_div");
              } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // $(label).closest('.control-group').removeClass('error success').addClass('error');
            // console.log("tets....highlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // }else if (elem.attr("name") == "city_id") {
            //     $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // } else {
            //     elem.addClass(errorClass);
            // }
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // console.log("tets....unhighlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // } else {
            //     elem.removeClass(errorClass);
            // }
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_address_form"] p.error').remove();
            var us2_lat = $("#us2_lat").val();
            var us2_lon = $("#us2_lon").val();
            var is_valid = true;
            if(us2_lat == 0){
                is_valid = false;
                $("#us2_lat_error").html("Please select location");
                $("#us2_address").focus();
            }

            if(us2_lon == 0){
                is_valid = false;
                $("#us2_lon_error").html("Please select location");
                $("#us2_address").focus();
            }

            if(is_valid){
                var form_type = $("form[name='add_address_form'] #add_address_form_type").val();
                var form_values = $("form[name='add_address_form']").serialize();
                console.log("form_type ..."+form_type );

                var is_email_change = 0;
                var is_mobile_change = 0;
                

                if(form_type == 'add'){
                    // addAddress(form_values);
                    requestOTPToPlant(1,form_values,is_mobile_change,is_email_change,0);
                }
                else if(form_type == 'edit'){

                    var edit_address_mobile = $("#edit_address_mobile").val();
                    var edit_address_email = $("#edit_address_email").val();

                    var mobile_or_email = $("#mobile_or_email").val();
                    var email = $("#email").val();                   

                    if(edit_address_mobile != mobile_or_email){
                        is_mobile_change = 1;
                    }

                    if(edit_address_email != email){
                        is_email_change = 1;
                    }

                    if(is_email_change == 0 && is_mobile_change == 0){
                        editAddress(form_values);
                    }else{
                        
                        requestOTPToPlant(2,form_values,is_mobile_change,is_email_change,1);
                    }
                    
                }
            }
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_address_form"] p.error').remove();
        }

    });

    $(document).on('change', '.form-control-chosen', function () {
        if (!$.isEmptyObject(vadd_address_form_obj.submitted)) {
            vadd_address_form_obj.form();
        }
    });

    $('#select_region_id').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
       
        console.log("valueSelected..."+valueSelected);

        getSourceByRegion(valueSelected);

    });
    
    $('#billing_address_id_dropdown').on('change', function (e) {
        var optionSelected = $(this).find('option:selected');
        var valueSelected = optionSelected.val();
        var value = optionSelected.attr("data-value");
       
        if(value != undefined){
            value = JSON.parse(value);
            var billing_state = value['stateDetails']['state_name'];
            console.log("valueSelected..."+valueSelected);
            console.log("state..."+billing_state);
            console.log(value);

            var plant_state = $("#add_site_state").find('option:selected');
            
            if(plant_state.val() != 'State'){
                var plant_state_value = plant_state.val();
                console.log("plant_state_value..."+plant_state_value);

                if(billing_state != plant_state_value){

                    $('#billing-addr-state-alert-popup').modal('show');

                }
            }

        }

    });
    
    $('#billing-addr-state-check-cancel').on('click', function (e) {        
       
        $("#billing_address_id_dropdown").prop('selectedIndex', 0);
    });


// Billing Address............

$.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='add_billing_address_form']").validate({
        rules:{
            company_name: {
                required: true,
                noSpace: true,
                minlength: 3,
                maxlength: 64
            },
            line1: {
                required: true,
                noSpace: true
            },
            line2: {
                required: true,
                noSpace: true
            },
            gst_number: {
                required: true,
                gst:true,
                noSpace: true
            },
            state_id: "required",
            city_id: "required",
            pincode: {
                required: true,
                noSpace: true,
                number: true,
                minlength: 6,
                maxlength: 6
            },
            gst_image: {
                required: function(){

                    var form_type = $("form[name='add_billing_address_form'] #add_billing_address_form_type").val(); 

                    if(form_type == "edit"){
                        return false;
                    }

                    return true;
                },
                extension: "jpg|jpeg|pdf"
            },
            logo_image: {
                required: function(){

                    var form_type = $("form[name='add_billing_address_form'] #add_billing_address_form_type").val(); 

                    if(form_type == "edit"){
                        return false;
                    }

                    return true;
                },
                extension: "jpg|jpeg"
            }
            
        },
        messages:{
           
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "state_id" ) {
              error.insertAfter("#state_name_city_div");
            } else if (element.attr("name") == "city_id" ) {
                error.insertAfter("#state_name_city_filter_div");
              }else if (element.attr("name") == "region_id" ) {
                error.insertAfter("#region_id_div");
              }else if (element.attr("name") == "source_id" ) {
                error.insertAfter("#source_id_div");
              } else if (element.attr("name") == "gst_image" ) {
                error.insertAfter("#gst_image_div");
              }else if (element.attr("name") == "logo_image" ) {
                error.insertAfter("#logo_image_div");
              } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // console.log("tets....unhighlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // } else {
            //     elem.removeClass(errorClass);
            // }
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_billing_address_form"] p.error').remove();
            

            var form_type = $("form[name='add_billing_address_form'] #add_billing_address_form_type").val();
            // var form_values = $("form[name='add_billing_address_form']").serialize();
            console.log("form_type ..."+form_type);            
            var form_values = new FormData(form);
            
            if(form_type == 'add'){
                addBillingAddress(form_values);
            }
            else if(form_type == 'edit'){
                editBillingAddress(form_values);
            }
          
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_billing_address_form"] p.error').remove();
        }

    });


});

function addAddress(form_values,email_otp,mobile_otp){

    form_values = form_values + "&mobile_code="+mobile_otp+"&email_code="+email_otp;

    responsehandler('supplier/address/add_address', 
        "POST", 
        form_values,
        "#make-default-alert-popup",
        "Address added successfully",
        0,
        "add_address_form"
    );

}

function editAddress(form_values,email_otp,mobile_otp){

    form_values = form_values + "&mobile_code="+mobile_otp+"&email_code="+email_otp;


    responsehandler('supplier/address/edit_address', 
        "POST", 
        form_values,
        "#make-default-alert-popup",
        "Address updated successfully",
        0,
        "add_address_form"
    );

}

function addBillingAddress(form_values){

    responsehandlerWithFiles('supplier/billing_address/add', 
        "POST", 
        form_values,
        "#make-default-alert-popup",
        "Billing Address added successfully",
        0,
        "add_billing_address_form"
    );

}

function editBillingAddress(form_values){
    
responsehandlerWithFiles('supplier/billing_address/edit_address', 
        "POST", 
        form_values,
        "#make-default-alert-popup",
        "Billing Address updated successfully",
        0,
        "add_billing_address_form"
    );

}

function getSourceByRegion(region_id){

    var request_data = {'region_id':region_id};
    
    customResponseHandler(
        'supplier/address/getSourceByRegion', // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            // data = JSON.parse(data);
            var html = '<option disabled="disabled" selected="selected">Select Source</option>';
        
            $.each(data,function(key,value){
                
                
                    html += '<option value="'+value["_id"]+'">'+value["source_name"]+'</option>';
                            
                
            });
            
            $('#region_source_id').html(html);
            
            var selected_source_id = $('#span_source_id').html();

            if(selected_source_id != undefined){
                if(selected_source_id.length > 0){
                    $('#region_source_id').val(selected_source_id);    
                }
            }
            
            

        }
    );

}

// Address Module Over..................................................


// Support Ticket MODULE Start...........................................


$(document).ready(function() {
    /*character limit script start*/
    $('textarea#ctsubject, textarea#ctdescription, textarea#product_description').characterlimit();
    /*character limit script end*/


    $("form[name='add_support_ticket_form']").validate({

        rules:{
            question_type: "required",
            severity: "required",
            subject: {
                required: true,
                noSpace: true
            },
            description: {
                required:true,
                noSpace: true
            },
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf"
            }
            
        },
        messages:{
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf) file format"
            }
          
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "attachments[]" ) {
              error.insertAfter("#support_ticket_attachment_div");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
           
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $(".fileuploader-input-caption").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
           
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $(".fileuploader-input-caption").removeClass(errorClass);
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="add_support_ticket_form"] p.error').remove();
            // var form_values = $("form[name='add_support_ticket_form']").serialize();
            
            
            var form_values = new FormData(form);
            console.log(form_values);
        
            addSupportTicket(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_support_ticket_form"] p.error').remove();
        }

    });

    $("form[name='ticket_resolve_form']").validate({

        rules:{
            
            comment: {
                required: true,
                noSpace: true
            },
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf|png"
            }
        },
        messages:{
            
            comment: {
                required: "Comment is required"
            },
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf, png) file format"
            }
          
        },
        
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="ticket_resolve_form"] p.error').remove();
            var form_values = $("form[name='ticket_resolve_form']").serialize();
            
            
            // var form_values = new FormData(form);
            console.log(form_values);
        
            updateTicketStatus(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="ticket_resolve_form"] p.error').remove();
        }

    });
    
    


    /*reply form show hide script */
    $('.showButton').click(function () {
        $('.rplyshowhide').fadeIn();
        $('.rplyshow').fadeOut();
    });
    $('.hideButton').click(function () {
        $('.rplyshowhide').fadeOut();
        $('.rplyshow').fadeIn();
    });

    $("form[name='support_ticket_reply_form']").validate({

        rules:{
            
            comment: "required",
            'attachments[]':{
                extension: "jpg|jpeg|mp4|doc|docx|pdf|png"
            }
            
        },
        messages:{
            comment: "Comment is required",
            'attachments[]':{
                extension: "Please use valid file (jpg, jpeg, mp4, doc, docx, pdf, png) file format"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "attachments[]" ) {
              error.insertAfter("#reply_attachment_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="support_ticket_reply_form"] p.error').remove();
            // var form_values = $("form[name='add_support_ticket_form']").serialize();
            
            
            var form_values = new FormData(form);
            console.log(form_values);
        
            replySupportTicket(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="support_ticket_reply_form"] p.error').remove();
        }

    });

});

function updateTicketStatus(form_values){

    responsehandler('supplier/support_ticket/ticket_detail/resolve', 
        "POST", 
        form_values,
        "#make-default-alert-popup",
        "Ticket status updated successfully",
    );

}

function addSupportTicket(form_values){

    responsehandlerWithFiles("supplier/support_ticket/add", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Support Ticket Added successfully",
                    0,
                    "add_support_ticket_form"
                );

}

function replySupportTicket(form_values){

    responsehandlerWithFiles("supplier/support_ticket/reply", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Reply sent successfully",
                    0,
                    "support_ticket_reply_form"
                );

}

// Support Ticket MODULE OVER...........................................

// Profile MODULE Start...........................................

$(document).ready(function(){

    $("form[name='forgot_pass_form']").validate({

        rules:{
            
            password: {
                required: true,
                minlength : 6,
                maxlength: 20
            }, 
            confirm_password:{
                required: true,
                minlength : 6,
                equalTo : "#new_password"
            },           
            
        },
        messages:{

        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="forgot_pass_form"] p.error').remove();
            // var form_values = $("form[name='forgot_pass_form']").serialize();            
            
            // var form_values = new FormData(form);
            // console.log(form_values);
        
            // changePassword(form_values);
            // $("form[name='forgot_pass_form']").submit();
            $("form[name='forgot_pass_form']")[0].submit();
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="forgot_pass_form"] p.error').remove();
        }

    });

    $("form[name='edit_myprofile_details_form']").validate({

        rules:{
            full_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
                a_z_pattern: true
            },
            company_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
                
            },
            mobile_number: {
                required:true,
                mobile_validation: true,
                noDecimal: true,
                minlength: 13,
                maxlength: 13
            },
            email: {
                required:true,
                email_validate: true
            },
            landline_number: {
                number: true
            },
            password: {
                minlength : 6,
                maxlength: 20
            }, 
            confirm_password:{
                minlength : 6,
                equalTo : "#new_password"
            },
            company_type: {
                required: true
            },
            company_certification_image: {
                extension: "jpg|jpeg|pdf"
            },
            pancard_image: {
                extension: "jpg|jpeg|pdf"
            },
            gst_certification_image: {
                extension: "jpg|jpeg|pdf"
            },
            gst_number: {
                required: false,
                gst: true
            },
            pan_number: {
                required: true,
                pan: true
            }
            
        },
        messages:{
            
            company_certification_image: {
                extension: "Please select valid file (jpg, jpeg, pdf) format"
            },
            pancard_image: {
                extension: "Please select valid file (jpg, jpeg, pdf) format"
            },
            gst_certification_image: {
                extension: "Please select valid file (jpg, jpeg, pdf) format"
            },
            mobile_number:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            },
            email:{
                email_validate: "Please enter valid email address"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "mobile_number" ) {
              error.insertAfter("#edit_profile_mobile_number_div");
            }else if (element.attr("name") == "region_served[]" ) {
              error.insertAfter("#selectregion");
            } else if (element.attr("name") == "company_certification_image" ) {
              error.insertAfter("#company_certification_image_div");
            }else if (element.attr("name") == "pancard_image" ) {
              error.insertAfter("#pancard_image_div");
            }else if (element.attr("name") == "gst_certification_image" ) {
              error.insertAfter("#gst_certification_image");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="edit_myprofile_details_form"] p.error').remove();
            // var form_values = $("form[name='edit_myprofile_details_form']").serialize();            
            
            var form_values = new FormData(form);
            console.log(form_values);
        
            editMyProfile(form_values);

            // var is_email_change = 0;
            // var is_mobile_change = 0;   
            
            // var edit_address_mobile = $("#edit_address_mobile").val();
            // var edit_address_email = $("#edit_address_email").val();

            // var mobile_or_email = $("#mobile_or_email").val();
            // var email = $("#edit_profile_email").val();                   

            // if(edit_address_mobile != mobile_or_email){
            //     is_mobile_change = 1;
            // }

            // if(edit_address_email != email){
            //     is_email_change = 1;
            // }

            
            // requestOTPforEditProfile(1,form_values,is_mobile_change,is_email_change,1);                           
                
            

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="edit_myprofile_details_form"] p.error').remove();
        }

    });
    
    $("form[name='edit_business_details_form']").validate({

        rules:{
            annual_turnover:{
                number: true
            },
            number_of_employee:{
                number: true
            },
            
            
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "company_certification_image" ) {
              error.insertAfter("#company_certification_image_div");
            } else if (element.attr("name") == "pancard_image" ) {
              error.insertAfter("#pancard_image_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="edit_business_details_form"] p.error').remove();
            // var form_values = $("form[name='edit_myprofile_details_form']").serialize();            
            
            var form_values = new FormData(form);
            console.log(form_values);
        
            editBusinessProfile(form_values);

            // var is_email_change = 0;
            // var is_mobile_change = 0;  

            
            // requestOTPforEditProfile(2,form_values,is_mobile_change,is_email_change,1); 

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="edit_business_details_form"] p.error').remove();
        }

    });

    $("form[name='update-mobile-otp-form']").validate({
        rules: {
            otp_code_1: {
                required: true,
                minlength : 6,
                maxlength: 6,
                number:true,
                noDecimal: true
            }
            
        },
        messages: {
            
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='update-mobile-otp-form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            updateMobileEmail(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });
    
    
    $("form[name='new_mobile_form']").validate({
        rules: {
            mobile: {
                required: true,
                minlength : 13,
                maxlength: 13,
                noDecimal: true
            },
            
            
        },
        messages: {
            mobile:{
                minlength: "Please enter valid mobile number",
                maxlength: "Please enter valid mobile number",
            }
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='new_mobile_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            // updateMobileEmail(form_values,"old_mobile");

            OTPonUpdateMobile(0, 1);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });

    $("form[name='update-email-otp-form']").validate({
        rules: {
            otp_code_1: {
                required: true,
                minlength : 6,
                maxlength: 6,
                number:true,
                noDecimal: true
            },
            
            
        },
        messages: {
            
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='update-email-otp-form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            updateEmail(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });


    $("form[name='new_email_form']").validate({
        rules: {
            email: {
                required: true,
                email_validate: true
            },            
            
        },
        messages: {
            email:{
                email_validate: "Please enter valid email address"
            }
        },
        submitHandler: function(form) {
            console.log("submited");
            // var form_type = $("form[name='add_site_form'] #add_site_type").val();
            var form_values = $("form[name='new_email_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='logistics_register_form']").submit();
            // var form_values = new FormData(form);

            // requestOTP(1,form_values);

            // changePassword(form_values);

            // updateMobileEmail(form_values,"old_mobile");

            OTPonUpdateEmail(0, 1);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
        }
    });


    $("form[name='update_password_form']").validate({

        rules:{            
            old_password:{
                required: true,
                minlength : 6
            },
            password:{
                required: true,
                minlength : 6
            },
            confirm_password:{
                required: true,
                minlength : 6,
                equalTo : "#new_password"
            },            
            
        },
        messages:{
           
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="update_password_form"] p.error').remove();
            var form_values = $("form[name='update_password_form']").serialize();
            
                    
            // var form_values = new FormData(form);
            console.log(form_values);
            
            requestOTP(1,form_values,"update_password");

            

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="update_password_form"] p.error').remove();
        }

    });

});

function changePassword(form_values,otp){   

    form_values = form_values + "&authentication_code="+otp;

    responsehandler('supplier/change_pass', 
        "POST", 
        form_values,
        "#make-default-alert-popup",
        "Password changed successfully",
        1,
        "forgot_pass_form"
    );

}

function OTPonUpdateMobile(is_old_mobile_no, is_new_mobile_no, is_resend=0){

    if(is_old_mobile_no == 1){
        var mobile_or_email = $("#mobile_or_email").val();
    }
    
    if(is_new_mobile_no == 1){
        var mobile_or_email = $("#update_new_mobile_no").val();
    }
    
    $("#verify_update_mobile").val(mobile_or_email);
    
    $("#edit-profile-popup").modal("hide");

    requestOTPforEditProfile(1,mobile_or_email,is_old_mobile_no,0,is_new_mobile_no,0,1,is_resend);

}

function OTPonUpdateEmail(is_old_email, is_new_email,is_resend=0){

    if(is_old_email == 1){
        var mobile_or_email = $("#edit_address_email").val();
    }
    
    if(is_new_email == 1){
        var mobile_or_email = $("#update_new_email").val();
    }
    
    $("#verify_update_email").val(mobile_or_email);
    
    $("#edit-profile-popup").modal("hide");

    requestOTPforEditProfile(1,mobile_or_email,0,is_old_email,0,is_new_email,1,is_resend);

}

function updateMobileEmail(form_values){

    // form_values=form_values+"&mobile_or_email_type="+mobile_or_email_type;

    // form_values.append('mobile_or_email_type', mobile_or_email_type);
    // form_values.append('email_code', email_otp);
    // form_values.append('authentication_code', old_mobile_otp);

    customResponseHandler("supplier/profile/edit",
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){                            

                            $("#update_mobile_profile").modal("hide");

                            if($("#mobile_type").val() == "old_mobile"){
                                showToast("OTP verified successfully","success");
                                $("#new_mobile_no_popup").modal("show");
                            }else{
                                showToast("Mobile No updated successfully","success");
                                reload();
                            }

                        }else{
                            // showToast("Entered OTP is Wrong. Please check again","error");
                            // $("#update_mobile_otp_error").html("Enter OTP is Wrong. Please check and enter again");
                            
                            showToast(""+data["error"]["message"],"error");

                        }

                    }
                );

}

function updateEmail(form_values){
    console.log(form_values);
    // form_values=form_values+"&mobile_or_email_type="+mobile_or_email_type;

    // form_values.append('mobile_or_email_type', mobile_or_email_type);
    // form_values.append('email_code', email_otp);
    // form_values.append('authentication_code', old_mobile_otp);

    customResponseHandler("supplier/profile/edit",
                    "GET",
                    form_values,
                    function response(data){
                        console.log(data);
                        // var message = data["data"]["message"];
                        if(data["status"] == 200){
                            

                            $("#update_email_profile").modal("hide");

                            if($("#email_type").val() == "old_email"){
                                showToast("OTP verified successfully","success");
                                $("#new_email_no_popup").modal("show");
                            }else{
                                showToast("Email updated successfully","success");
                                reload();
                            }

                        }else{
                            // showToast("Entered OTP is Wrong. Please check again","error");
                            // $("#update_mobile_otp_error").html("Enter OTP is Wrong. Please check and enter again");
                            showToast(""+data["error"]["message"],"error");
                        }

                    }
                );

}


function editMyProfile(form_values){
    // console.log(form_values);
    // form_values = form_values + "&mobile_code="+mobile_otp+"&email_code="+email_otp+"&authentication_code="+old_mobile_otp;
    // form_values.append('mobile_code', mobile_otp);
    // form_values.append('email_code', email_otp);
    // form_values.append('authentication_code', old_mobile_otp);
    // responsehandler('supplier/profile/edit', 
    //     "POST", 
    //     form_values,
    //     "#make-default-alert-popup",
    //     "Profile updated successfully",
    //     0,
    //     "edit_myprofile_details_form"
    // );

    responsehandlerWithFiles("supplier/profile/edit", 
                    "POST", 
                    form_values, 
                    "#make-default-alert-popup",
                    "Profile updated successfully",
                    0,
                    "edit_myprofile_details_form"
                );

}

function editBusinessProfile(form_values){

    // form_values.append('authentication_code', old_mobile_otp);

    responsehandlerWithFiles("supplier/profile/edit", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Profile updated successfully",
                    0,
                    "edit_business_details_form"
                );

}

// Profile MODULE OVER...........................................

// Proposal MODULE Start...........................................

$(document).ready(function(){

    $("form[name='add_proposal_form']").validate({

        rules:{
            contact_person_id:{
                required: true
            },
            category_name:{
                required: true,
                minlength: 3,
                maxlength: 64
            },        
            quantity:{
                required: true,
                number: true
            },
            pickup_address_id:{
                required: true
            },
            description:{
                required: true
            },
            
            
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "contact_person_id" ) {
              error.insertAfter("#contact_person_id_div");
            } else if (element.attr("name") == "pickup_address_id" ) {
              error.insertAfter("#pickup_address_id_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="add_proposal_form"] p.error').remove();
            var form_values = $("form[name='add_proposal_form']").serialize();            
            
            // var form_values = new FormData(form);
            console.log(form_values);
        
            // editBusinessProfile(form_values);
            addProposal(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_proposal_form"] p.error').remove();
        }

    });

});

function addProposal(form_values){

    responsehandler('supplier/add_proposal', 
        "POST", 
        form_values,
        "#add-proposal",
        "Proposal added successfully",
        0,
        "add_proposal_form"
    );

}


// Proposal MODULE OVER...........................................
// Order MODULE Start...........................................

$(document).ready(function(){

    $("form[name='truck_assigned_form']").validate({

        rules:{
            vehicle_id: {
                required: true
            },
            delivered_quantity: {
                required:true,
                number: true,
                greaterThanEqual:0,
                noDecimal:true
            }
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "vehicle_id" ) {
              error.insertAfter(element.parent());
            } else {
              error.insertAfter(element);
            }
          },
        messages:{
            
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           

            var order_assign_item_assign_qty = $("#order_assign_item_assign_qty").val()
            var TM_assign_delivered_quantity = $("#TM_assign_delivered_quantity").val()

            var is_error = 0;

            console.log("TM_assign_delivered_quantity..."+TM_assign_delivered_quantity);
            console.log("order_assign_item_assign_qty..."+order_assign_item_assign_qty);

            if(parseInt(TM_assign_delivered_quantity) > parseInt(order_assign_item_assign_qty)){

                $("#TM_assign_delivered_quantity_error").html("This value must be less than to be assigned Qty "+order_assign_item_assign_qty);

                is_error = 1;

            }

            if(is_error == 0){

                $('form[name="truck_assigned_form"] p.error').remove();
                var form_values = $("form[name='truck_assigned_form']").serialize();            
                
                // var form_values = new FormData(form);
                // console.log(form_values);
            
                // editMyProfile(form_values);

                truckAssign(form_values);

            }

            

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="truck_assigned_form"] p.error').remove();
        }

    });  
    
    $("form[name='cp_assigned_form']").validate({

        rules:{
            cp_id: {
                required: true
            },
            assigned_at: {
                required:true
            }
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cp_id" ) {
              error.insertAfter(element.parent());
            } else {
              error.insertAfter(element);
            }
          },
        messages:{
            
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="cp_assigned_form"] p.error').remove();
            var form_values = $("form[name='cp_assigned_form']").serialize();            
            
            // var form_values = new FormData(form);
            // console.log(form_values);
        
            // editMyProfile(form_values);

            CPAssign(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="cp_assigned_form"] p.error').remove();
        }

    });  

    $("form[name='upload_order_bill_form']").validate({

        rules:{
            bill_type: {
                required: true
            },
            bill_image:{
                required:true,
                extension: "jpg|jpeg|doc|docx|pdf" 
            }
            
        },
        messages:{
            bill_image : {
                extension: "Please enter valid file (jpg, jpeg, doc, docx, pdf) formats"
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "bill_image" ) {
              error.insertAfter("#bill_image_div");
            }  else {
              error.insertAfter(element);
            }

          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="upload_order_bill_form"] p.error').remove();
            // var form_type = $("form[name='upload_order_bill_form'] #upload_order_bill_form_type").val();
            // var form_values = $("form[name='upload_order_bill_form']").serialize();
            var form_values = new FormData(form);
            console.log(form_values);
            // console.log("form_type ..."+form_type );

            uploadOrderBill(form_values);
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="stock_update_form"] p.error').remove();
        }

    });
    
    $("form[name='add_7_report_form']").validate({

        rules:{
            pic_7_report:{
                required:true,
                extension: "jpg|jpeg|doc|docx|pdf" 
            }
            
        },
        messages:{
            pic_7_report : {
                extension: "Please enter valid file (jpg, jpeg, doc, docx, pdf) formats"
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "pic_7_report" ) {
              error.insertAfter("#pic_7_report_div");
            }  else {
              error.insertAfter(element);
            }

          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_7_report_form"] p.error').remove();
            // var form_type = $("form[name='upload_order_bill_form'] #upload_order_bill_form_type").val();
            // var form_values = $("form[name='upload_order_bill_form']").serialize();
            var form_values = new FormData(form);
            console.log(form_values);
            // console.log("form_type ..."+form_type );

            upload7CubeReport(form_values);
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_7_report_form"] p.error').remove();
        }

    });
    
    $("form[name='add_28_report_form']").validate({

        rules:{
            pic_28_report:{
                required:true,
                extension: "jpg|jpeg|doc|docx|pdf" 
            }
            
        },
        messages:{
            pic_28_report : {
                extension: "Please enter valid file (jpg, jpeg, doc, docx, pdf) formats"
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "pic_28_report" ) {
              error.insertAfter("#pic_28_report_div");
            }  else {
              error.insertAfter(element);
            }

          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_28_report_form"] p.error').remove();
            // var form_type = $("form[name='upload_order_bill_form'] #upload_order_bill_form_type").val();
            // var form_values = $("form[name='upload_order_bill_form']").serialize();
            var form_values = new FormData(form);
            console.log(form_values);
            // console.log("form_type ..."+form_type );

            upload28CubeReport(form_values);
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_28_report_form"] p.error').remove();
        }

    });

    
    
    

});

function truckAssign(request_data){
    var url = 'supplier/supplier_truck_assign';

    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#assign-truck-popup",
                    "Truck assigned successfully",
                    0,
                    "truck_assigned_form"
                );

}

function CPAssign(request_data){
    var url = 'supplier/supplier_cp_assign';

    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#assign-CP-popup",
                    "CP assigned successfully",
                    0,
                    "cp_assigned_form"
                );

}

function orderAccept(order_id,order_status,otp){
    var url = 'supplier/supplier_order_accept';
    var msg = "Order successfully accepted";
    var request_data = {
        "order_id" : order_id,
        "order_status" : order_status,
    }

    if(order_status == 'REJECTED'){
        request_data["authentication_code"] = otp;

        msg = "Order successfully rejected";
    }

    responsehandler(url, 
                    "GET", 
                    request_data, 
                    "#assign-truck-popup",
                    ""+msg,
                    1,
                    "truck_assigned_form"
                );

}

function surpriseOrderAccept(order_id,order_status,otp){
    var url = 'supplier/supplier_surprise_order_accept';

    var request_data = {
        "order_id" : order_id,
        "order_status" : order_status
    }

    if(order_status == 'REJECTED'){
        request_data["authentication_code"] = otp;
    }

    responsehandler(url, 
                    "GET", 
                    request_data, 
                    "#assign-truck-popup",
                    "Order successfully accepted",
                    1,
                    "truck_assigned_form"
                );

}

function uploadOrderBill(form_values){

    responsehandlerWithFiles("supplier/orders/bill_upload", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Bill uploaded successfully",
                    1,
                    "edit_business_details_form"
                );

}


function itemTrack(item_id,tracking_id){

    var form_values = {};

    customResponseHandler("supplier/order_track_detail/"+item_id+'/'+tracking_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#order_trackking_div").html(message);

                            $("#order-track").modal('show');
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}

function show7CubeReportDialog(track_id, tm_id, order_id){

    // data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_7_report_form');

    // $("#add_driver_form_title").html("Edit Driver Details");

    $('#report_7_track_id').val(track_id);
    $('#report_7_tm_id').val(tm_id);
    $('#report_7_order_id').val(order_id);
    

    $('#add-7-report-details').modal('show');

}

function show28CubeReportDialog(track_id, tm_id, order_id){

    // data = JSON.parse(data);
    // console.log(data);

    resetEditForm('add_28_report_form');

    // $("#add_driver_form_title").html("Edit Driver Details");

    $('#report_28_track_id').val(track_id);
    $('#report_28_tm_id').val(tm_id);
    $('#report_28_order_id').val(order_id);
    

    $('#add-28-report-details').modal('show');

}

function upload7CubeReport(form_values){

    responsehandlerWithFiles("supplier/supplier_report_7_day", 
                    "POST", 
                    form_values, 
                    "#add-7-report-details",
                    "7th day report added successfully",
                    1,
                    "add_7_report_form"
                );

}

function upload28CubeReport(form_values){

    responsehandlerWithFiles("supplier/supplier_report_28_day", 
                    "POST", 
                    form_values, 
                    "#add-28-report-details",
                    "28th day report added successfully",
                    1,
                    "add_28_report_form"
                );

}

function CPTrack(item_id){

    var form_values = {};

    customResponseHandler("supplier/order_CP_track_detail/"+item_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#order_CP_trackking_div").html(message);

                            $("#order-CP-track").modal('show');
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}

function ReasignOrder($reassign_reason,$vendor_id,order_track_id = ''){

    if($reassign_reason == 'order_rejected'){

        $("#reject_order_reassign_reason").val($reassign_reason);
        $("#reject_order_reassign_reason_field").val($reassign_reason);
        $("#reject_order_reassign_reason").attr("disabled",true);
        $('#reject_order_reassign_reason option:not(:selected)').attr('disabled', true);
        // $("#CP_dropdown_assign").attr("readOnly","readOnly");
        
        $("#reject_order_reassign_vendor_id_field").val($vendor_id);
        $("#reject_order_reassign_order_item_track_id_field").val(order_track_id);

        $("#reassign-reject-order-popup").modal('show');

    }else{

        $("#order_reassign_reason").val($reassign_reason);
        $("#order_reassign_reason_field").val($reassign_reason);
        $("#order_reassign_reason").attr("disabled",true);
        $('#order_reassign_reason option:not(:selected)').attr('disabled', true);
        // $("#CP_dropdown_assign").attr("readOnly","readOnly");
        
        $("#order_reassign_vendor_id_field").val($vendor_id);

        $("#reassign-order-popup").modal('show');

    }

    
    

}

function getAssignedOrdersBySelectedDateRange(start_date, end_date){

    var form_values = {
        'start_date' : start_date,
        'end_date' : end_date,
        'vendor_id' : $("#order_reassign_vendor_id_field").val(),
        'reassign_reason' : $("#order_reassign_reason_field").val(),
    };

    customResponseHandler("supplier/get_assigned_order_list",
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#reassigned_order_popup_tbody").html(message);

                            // $("#order-track").modal('show');
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );

}

// Order MODULE OVER...........................................

// Report MODULE Start...........................................

$(document).ready(function(){

    $('.from').datepicker({
	    autoclose: true,
	    minViewMode: 1,
	    format: 'M yyyy'
		}).on('changeDate', function(selected){
	        startDate = new Date(selected.date.valueOf());
	        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    });
    
    var daily_month;
    var daily_year;
    $("#daily_chart_year_filter").datepicker().on('changeMonth', function(e){ 
    // daily_month = new Date(e.date).getMonth() + 1;
    // daily_month = all_month_array[daily_month-1];
    // daily_year = String(e.date).split(" ")[3];
    // console.log("daily_year..."+daily_year+"..."+daily_month);

    //    var monthly_chart_status = $("#daily_chart_status_filter").val();

    //    getDailyChartDetails(daily_year,daily_month, monthly_chart_status);
    });

});


function downloadTMList(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "TM_listing_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadCPList(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "CP_listing_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadAdmixStock(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "admix_stock_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadAggStock(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "agg_stock_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadCementStock(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "cement_stock_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadFlyashStock(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "flyAsh_stock_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadSandStock(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "sand_stock_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadBankDetails(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "bank_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadDesignMixDetails(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "design_mix_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadTMDriver(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "TM_driver_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadOperator(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "operator_listing_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadPlantAddress(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "plant_address_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadBillingAddress(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "billing_address_report_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}

function downloadSupplierOrders(type) {
    var after = '';
    var name = '';

    console.log(window.location.href);
    console.log(getParams(window.location.href)); 
     var url_param = getParams(window.location.href);
    // if (type == "excel") {
        window.location.href =
            "supplier_order_export?"+url_param+"&file_type="+type;
    // } else if (type == "pdf") {
    //     window.location.href =
    //         "/vendors/branch/exportpdf?after=" + after + "&name=" + name;
    // } else if (type == "csv") {
    //     window.location.href =
    //         "/vendors/branch/exportcsv?after=" + after + "&name=" + name;
    // }
}


var getParams = function (url) {
	var params = {};
    var param_string = "";
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);

        param_string += pair[0]+'='+decodeURIComponent(pair[1])+'&';
	}
	// return params;
    param_string = param_string.substring(0, param_string.length - 1);
	return param_string;
};


// Report MODULE OVER...........................................


// Driver Module Over..................................................


$(document).ready(function(){

    $("form[name='add_driver_form']").validate({
        rules: {
            driver_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
                a_z_pattern: true
            },
            driver_mobile_number: {
                required: true,
                mobile_validation: true,
                noDecimal:true,
                minlength: 13,
                maxlength: 13
            },
            sub_vendor_id: {
                required: true
            },
            driver_alt_mobile_number: {
            },
            driver_whatsapp_number: {
            },
            driver_pic : {
                required: function(){
                   return  ($("form[name='add_driver_form'] #add_driver_form_type").val() == 'add')
                },
                extension: "jpg|png|jpeg"
            },
            
        },
        messages: {
            cancelled_cheque_image: {
                extension: "Please select valid file (jpg, png, jpeg) format"
            },
            driver_mobile_number:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "account_type" ) {
              error.insertAfter("#account_type_div");
            }else if (element.attr("name") == "driver_pic" ) {
              error.insertAfter("#driver_pic_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_driver_form"] p.error').remove();
            var form_type = $("form[name='add_driver_form'] #add_driver_form_type").val();
            // var form_values = $("form[name='add_driver_form']").serialize();
            console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            if(form_type === 'add'){
                addDriverDetails(form_values);
            }else{
                updateDriverDetails(form_values);
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_driver_form"] p.error').remove();
        }
    });

});


function addDriverDetails(form_values){

    responsehandlerWithFiles("supplier/driver_details/add", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Driver Added successfully",
                    0,
                    "add_driver_form"
                );

}

function showDriverDetailEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_driver_form');

    $("#add_driver_form_title").html("Edit Driver Details");

    $('#driver_name').val(data['driver_name']);
    $('#driver_mobile_number').val(data['driver_mobile_number']);
    $('#driver_alt_mobile_number').val(data['driver_alt_mobile_number']);
    $('#driver_whatsapp_number').val(data['driver_whatsapp_number']);
    

    $('#add_driver_form_type').val("edit");
    $('#driver_id').val(data['_id']);
    $('#diriver_sub_vendor_id').val(data['sub_vendor_id']);

    $('#add-bank-details').modal('show');

}

function updateDriverDetails(form_values){

    responsehandlerWithFiles("supplier/driver_details/update", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Driver Details updated successfully",
                    0,
                    "add_driver_form"
                );

}

// Driver Module Over..................................................



// Operator Module Over..................................................


$(document).ready(function(){

    $("form[name='add_operator_form']").validate({
        rules: {
            driver_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
                a_z_pattern: true
            },
            driver_mobile_number: {
                required: true,
                mobile_validation: true,
                noDecimal:true,
                minlength: 13,
                maxlength: 13
            },
            sub_vendor_id: {
                required: true
            },
            driver_alt_mobile_number: {
            },
            driver_whatsapp_number: {
            },
            driver_pic : {
                required: function(){
                   return  ($("form[name='add_operator_form'] #add_operator_form_type").val() == 'add')
                },
                extension: "jpg|png|jpeg"
            },
            
        },
        messages: {
            driver_pic: {
                extension: "Please select valid file (jpg, png, jpeg) format"
            },
            driver_mobile_number:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "account_type" ) {
              error.insertAfter("#account_type_div");
            }else if (element.attr("name") == "driver_pic" ) {
              error.insertAfter("#driver_pic_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_operator_form"] p.error').remove();
            var form_type = $("form[name='add_operator_form'] #add_operator_form_type").val();
            // var form_values = $("form[name='add_operator_form']").serialize();
            console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            if(form_type === 'add'){
                addOperatorDetails(form_values);
            }else{
                updateOperatorDetails(form_values);
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_operator_form"] p.error').remove();
        }
    });

});


function addOperatorDetails(form_values){

    responsehandlerWithFiles("supplier/operator_details/add", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Operator Added successfully",
                    0,
                    "add_operator_form"
                );

}

function showOperatorDetailEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_operator_form');

    $("#add_operator_form_title").html("Edit Operator Details");

    $('#driver_name').val(data['operator_name']);
    $('#driver_mobile_number').val(data['operator_mobile_number']);
    $('#driver_alt_mobile_number').val(data['operator_alt_mobile_number']);
    $('#driver_whatsapp_number').val(data['operator_whatsapp_number']);
    

    $('#add_operator_form_type').val("edit");
    $('#driver_id').val(data['_id']);
    $('#operator_sub_vendor_id').val(data['sub_vendor_id']);

    $('#add-operator-details').modal('show');

}

function updateOperatorDetails(form_values){

    responsehandlerWithFiles("supplier/operator_details/update", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Operator Details updated successfully",
                    0,
                    "add_operator_form"
                );

}

// Operator Module Over..................................................

// Pump Gang Module Over..................................................


$(document).ready(function(){

    $("form[name='add_pumpgang_form']").validate({
        rules: {
            driver_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
                a_z_pattern: true
            },
            driver_mobile_number: {
                required: true,
                mobile_validation: true,
                noDecimal:true,
                minlength: 13,
                maxlength: 13
            },
            sub_vendor_id: {
                required: true
            },
            driver_alt_mobile_number: {
            },
            driver_whatsapp_number: {
            },
            driver_pic : {
                required: function(){
                   return  ($("form[name='add_pumpgang_form'] #add_pumpgang_form_type").val() == 'add')
                },
                extension: "jpg|png|jpeg"
            },
            
        },
        messages: {
            driver_pic: {
                extension: "Please select valid file (jpg, png, jpeg) format"
            },
            driver_mobile_number:{
                minlength: "Please add valid mobile number",
                maxlength: "Please add valid mobile number"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "account_type" ) {
              error.insertAfter("#account_type_div");
            }else if (element.attr("name") == "driver_pic" ) {
              error.insertAfter("#driver_pic_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_pumpgang_form"] p.error').remove();
            var form_type = $("form[name='add_pumpgang_form'] #add_pumpgang_form_type").val();
            // var form_values = $("form[name='add_operator_form']").serialize();
            console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            if(form_type === 'add'){
                addPumpgangDetails(form_values);
            }else{
                updatePumpgangDetails(form_values);
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_pumpgang_form"] p.error').remove();
        }
    });

});


function addPumpgangDetails(form_values){

    responsehandlerWithFiles("supplier/pumpgang_details/add", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Pump Gang Added successfully",
                    0,
                    "add_pumpgang_form"
                );

}

function showPumpgangDetailEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_pumpgang_form');

    $("#add_pumpgang_form_title").html("Edit CP Gang Details");

    $('#driver_name').val(data['gang_name']);
    $('#driver_mobile_number').val(data['gang_mobile_number']);
    $('#driver_alt_mobile_number').val(data['gang_alt_number']);
    $('#driver_whatsapp_number').val(data['gang_whatsapp_number']);
    

    $('#add_pumpgang_form_type').val("edit");
    $('#driver_id').val(data['_id']);
    $('#operator_sub_vendor_id').val(data['sub_vendor_id']);

    $('#add-pumpgang-details').modal('show');

}

function updatePumpgangDetails(form_values){

    responsehandlerWithFiles("supplier/pumpgang_details/update", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Pump Gang Details updated successfully",
                    0,
                    "add_pumpgang_form"
                );

}

// Pump Gang Module Over..................................................


// Vehicle Module Start..................................................

$(document).ready(function(){
    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    var validobj = $("form[name='add_vehicles_form']").validate({
        rules: {
            vehicleCategoryId: {
                required: true
            },
            vehicleSubCategoryId: {
                required: true
            },
            vehicle_rc_number_1: {
                required: true,
                minlength: 2,
                maxlength: 2,
                lettersonly: true
            },
            vehicle_rc_number_2: {
                required: true,
                number:true,
                noDecimal:true,
                minlength: 2,
                maxlength: 2
            },
            vehicle_rc_number_3: {
                required: true,
                minlength: 2,
                maxlength: 2,
                lettersonly: true
            },
            vehicle_rc_number_4: {
                required: true,
                number:true,
                noDecimal:true,
                minlength: 4,
                maxlength: 4
            },
            min_trip_price: {
                required: true,
                number: true,
                greaterThanEqual: 0,
                maxlength:8
                
            },
            manufacturer_name: {
                required: true
            },
            vehicle_model: {
                required: true
            }, 
            manufacture_year: {
                required: true,
                number: true,
                noDecimal:true,
                lessThanEqual: 9999,
                minlength: 4,
                maxlength: 4
            },
            
            delivery_range: {
                required: true,
                number: true,
                noDecimal:true,
                lessThanEqual: 700,
                greaterThanEqual: 50
            },
            // per_metric_ton_per_km_rate: {
            //     number: true,
            //     lessThanEqual: 9.99,
            //     greaterThanEqual: 1.00
            // },
            driver1_id: {
                required: true
            },
            driver2_id: {
                required: true
            },
            address_id: {
                required: true
            },
            per_Cu_mtr_km: {
                required: true,
                number: true,
                greaterThanEqual:1,
                maxlength:8
            },
            
            rc_book_image: {
                required: function(){
                    return $("form[name='add_vehicles_form'] #add_vehicle_form_type").val() === 'add';
              },
              extension: "jpg|jpeg"
                
            },
            insurance_image : {
                required: function(){
                    return $("form[name='add_vehicles_form'] #add_vehicle_form_type").val() === 'add';
              },
              extension: "jpg|jpeg"
                
            },
            vehicle_image : {
                required: function(){
                    return $("form[name='add_vehicles_form'] #add_vehicle_form_type").val() === 'add';
              },
              extension: "jpg|jpeg"
            }
        },
        messages: {
            rc_book_image: {
                extension: "Please enter valid file (jpg, jpeg) format"
            },
            insurance_image: {
                extension: "Please enter valid file (jpg, jpeg) format"
            },
            vehicle_image: {
                extension: "Please enter valid file (jpg, jpeg) format"
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "vehicleCategoryId" ) {
              error.insertAfter("#vehicleCategoryId_div");
            } else if (element.attr("name") == "vehicleSubCategoryId" ) {
              error.insertAfter("#vehicleSubCategoryId_div");
            }else if (element.attr("name") == "address_id" ) {
              error.insertAfter("#address_id_div");
            } else if (element.attr("name") == "delivery_range" ) {
              error.insertAfter("#delivery_range_div");
            }else if (element.attr("name") == "rc_book_image" ) {
              error.insertAfter("#rc_book_image_div");
            }else if (element.attr("name") == "insurance_image" ) {
              error.insertAfter("#insurance_image_div");
            }else if (element.attr("name") == "vehicle_image" ) {
              error.insertAfter("#vehicle_image_div");
            }else if (element.attr("name") == "driver1_id" ) {
              error.insertAfter("#driver1_id_div");
            } else if (element.attr("name") == "driver2_id" ) {
              error.insertAfter("#driver2_id_div");
            }else if (element.attr("name") == "state_id" ) {
              error.insertAfter("#state_name_city_div");
            }else if (element.attr("name") == "city_id" ) {
              error.insertAfter("#state_name_city_filter_div");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // $(label).closest('.control-group').removeClass('error success').addClass('error');
            // console.log("tets....highlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // }else if (elem.attr("name") == "city_id") {
            //     $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // } else {
            //     elem.addClass(errorClass);
            // }
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // console.log("tets....unhighlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // } else {
            //     elem.removeClass(errorClass);
            // }
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $(".error").html();
            $('form[name="add_vehicles_form"] p.error').remove();
            var form_type = $("form[name='add_vehicles_form'] #add_vehicle_form_type").val();
            // var form_values = $("form[name='vehicle_add_sub_cat_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='add_vehicles_form']").submit();
            var form_values = new FormData(form);

            $("#TM_save_button").attr("disabled","true");

            if(form_type === 'add'){
                
                addTM(form_values);
            }else{
                editTM(form_values);
            }      
            

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $(".error").html();
            $('form[name="add_vehicles_form"] p.error').remove();
        }
    });



    $(document).on('change', '.form-control-chosen', function () {
        if (!$.isEmptyObject(validobj.submitted)) {
            validobj.form();
        }
    });
    
   

    // $(document).on('change', '#state_name_city_filter', function () {
    //     if (!$.isEmptyObject(validobj.submitted)) {
    //         validobj.form();
    //     }
    // });


    $('#is_use_registered_address').change(function() {
        console.log("checked")
        if(this.checked) {

            var profile_data = $("#user_profile_data").val();

            addRegisteredAddressInVehicle(profile_data);

        }else{
            addRegisteredAddressInVehicle('{}');
        }
             
    });
    
    $('#vehicleSubCategoryId').on('change', function() {
        var optionSelected = $("option:selected", this);
        // console.log("checked"+optionSelected.attr('data-min-load'));
        

        var min_load = optionSelected.attr('data-min-load');
        var max_load = optionSelected.attr('data-max-load');
             
        $('#vehicle_min_load').val(min_load);
        $('#vehicle_max_load').val(max_load);

    });

});

function addRegisteredAddressInVehicle(data){

    
    data = JSON.parse(data);
    console.log(data);

    console.log("undefined..."+(data["line1"] != undefined ? data["line1"] : ''));
    
    $('#vehicle_address_line_1').val((data["line1"] != undefined ? data["line1"] : ''));
    $('#vehicle_address_line_2').val((data["line2"] != undefined ? data["line2"] : ''));
    $('#state_name_city').val((data["state_id"] != undefined ? data["state_id"] : ''));
    $('#state_name_city_filter').val((data["city_id"] != undefined ? data["city_id"] : ''));
    $('#vehicle_pincode').val((data["pincode"] != undefined ? data["pincode"] : ''));

    $("#state_name_city").trigger("chosen:updated");
    $("#state_name_city_filter").trigger("chosen:updated");

    var lat = (data["pickup_location"] != undefined ? data["pickup_location"]["coordinates"][1] : 0);
    var lon = (data["pickup_location"] != undefined ? data["pickup_location"]["coordinates"][0] : 0);

    $('#us2_address').val('');
    $('#us2_lat').val('');
    $('#us2_lon').val('');

    $('#us2').locationpicker("location", {latitude: ''+lat, longitude: ''+lon});
    $('#us2_address').val('');

}

function addTM(form_values){

    responsehandlerWithFiles("supplier/TM/add", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "TM Added successfully",
                    0,
                    "add_vehicles_form"
                );

}

function editTM(form_values){

    responsehandlerWithFiles("supplier/TM/edit", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "TM updated successfully",
                    0,
                    "add_vehicles_form"
                );

}

// Vehicle Module Over..................................................



// Concrete Pump Module Start..................................................

$(document).ready(function(){


    $('#is_Operator').change(function() {
        if(this.checked) {            
            $("#operator_id_div").show();
        }else{
            $("#operator_id_div").hide();
        }
        // $('#textbox1').val(this.checked);        
    });

    $("form[name='add_concrete_pump_form']").validate({
        rules: {
            ConcretePumpCategoryId: {
                required: true
            },
            company_name: {
                required: true
            },
            concrete_pump_model: {
                required: true
            },
            manufacture_year: {
                required: true,
                number:true
            },
            serial_number: {
                required: true
            },
            operator_id: {
                required: function(){
                    return $("#is_Operator").is(":checked") ? true :false; 
                }
            },
            gang_id: {
                required: function(){
                    return true; 
                }
            },
            pipe_connection: {
                required: true,
                number:true
            },
            concrete_pump_capacity: {
                required: true,
                number:true,
                minlength:1,
                maxlength:8
            },
            transportation_charge: {
                required: true,
                number:true,
                greaterThanEqual:1,
                maxlength:8
            },
            concrete_pump_price: {
                required: true,
                number:true,
                greaterThanEqual:1,
                maxlength:8
            },
            address_id: {
                required: true
            },
            concrete_pump_image : {
                required: function(){
                    return $("form[name='add_concrete_pump_form'] #add_concrete_pump_form_type").val() == 'add' ? true :false; 
                },
                extension: "jpg|png|jpeg"
            },
            
        },
        messages: {
            concrete_pump_image : {
                extension: "Please enter valid file (jpg, png, jpeg) formats"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "concrete_pump_image" ) {
              error.insertAfter("#concrete_pump_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_concrete_pump_form"] p.error').remove();

            var form_type = $("form[name='add_concrete_pump_form'] #add_concrete_pump_form_type").val();
            // var form_values = $("form[name='add_concrete_pump_form']").serialize();
            console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            if(form_type === 'add'){
                addConcretePump(form_values);
            }else{
                updateConcretePump(form_values);
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_concrete_pump_form"] p.error').remove();
        }
    });

});

function addConcretePump(form_values){

    responsehandlerWithFiles("supplier/concrete_pump_details/add", 
                    "POST", 
                    form_values, 
                    "#add-concrete-pump-details",
                    "Concrete Pump Added successfully",
                    0,
                    "add_concrete_pump_form"
                );

}

function showConcretePumpEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_concrete_pump_form');

    $("#add_concrete_pump_title").html("Edit Concrete Pump");

    $('#ConcretePumpCategoryId').val(data['concrete_pump_category_id']);
    $('#pump_make').val(data['company_name']);
    $('#concrete_pump_model').val(data['concrete_pump_model']);
    $('#manufacture_year').val(data['manufacture_year']);
    $('#pipe_connection').val(data['pipe_connection']);
    $('#concrete_pump_capacity').val(data['concrete_pump_capacity']);
    $('#CP_serial_number').val(data['serial_number']);
    $('#CP_address_id').val(data['address_id']);

    if(data['is_Operator'] != undefined){
        if(data['is_Operator']){
            $( "#is_Operator" ).prop( "checked", true );
        }else{
            $( "#is_Operator" ).prop( "checked", false );
        }
    }
    
    if(data['operator_id'] != undefined){
        $('#operator_id').val(data['operator_id']);
    } 
    // else{
    //     $("#operator_id_div").hide();
    // }
    
    if(data['gang_id'] != undefined){
        $('#gang_id').val(data['gang_id']);
    }

    if(data['is_helper'] != undefined){
        $( "#is_helper" ).prop( "checked", data['is_helper'] );
    }
    
    $('#transportation_charge').val(data['transportation_charge']);
    $('#concrete_pump_price').val(data['concrete_pump_price']);
    

    $('#add_concrete_pump_form_type').val("edit");
    $('#concrete_pump_id').val(data['_id']);

    $('#add-concrete-pump-details').modal('show');

}

function updateConcretePump(form_values){

    responsehandlerWithFiles("supplier/concrete_pump_details/update", 
                    "POST", 
                    form_values, 
                    "#add-concrete-pump-details",
                    "Concrete Pump updated successfully",
                    0,
                    "add_concrete_pump_form"
                );

}


// Concrete Pump Module Over..................................................



// Design Mix Module Start..................................................

$(document).ready(function(){

    $("#cement_brand_id").change(function() {
        // alert( $('option:selected', this).text() );

        $("#cement_brand_name").val($('option:selected', this).text());
    });
    $("#sand_source_id").change(function() {
        // alert( $('option:selected', this).text() );

        $("#sand_source_name").val($('option:selected', this).text());
    });
    $("#aggregate_source_id").change(function() {
        // alert( $('option:selected', this).text() );

        $("#aggregate_source_name").val($('option:selected', this).text());
    });
    // $("#aggregate1_sub_category_id").change(function() {
    $("#mix_sub_cat_1").change(function() {
        // alert( $('option:selected', this).text() );

        $("#aggregate1_sub_category_name").val($('option:selected', this).text());
        $("#aggr_cat1_qty_lable").html($('option:selected', this).text()+" (KG)");
    });
    $("#mix_sub_cat_2").change(function() {
        // alert( $('option:selected', this).text() );

        $("#aggregate2_sub_category_name").val($('option:selected', this).text());
        $("#aggr_cat2_qty_lable").html($('option:selected', this).text()+" (KG)");
    });
    $("#fly_ash_source_id").change(function() {
        // alert( $('option:selected', this).text() );

        $("#fly_ash_source_name").val($('option:selected', this).text());
    });
    $("#ad_mixture_brand_id").change(function() {
        // alert( $('option:selected', this).text() );

        $("#admix_brand_name").val($('option:selected', this).text());
    });

    // $("#aggregate1_sub_category_id").change(function() {
    $("#mix_sub_cat_1").change(function() {
        
        var selected_cat = $('option:selected', this).val();

        getSecondAggregateSubCat(selected_cat);

    });

    $("form[name='add_mix_form']").validate({
        rules: {
            grade_id: {
                required: true
            },
            design_mix_number: {
                required: true
            },
            cement_brand_id: {
                required: false
            },
            cement_grade_id: {
                required: false
            },
            cement_quantity: {
                required: true,
                number: true,
                lessThanEqual:999,
                greaterThanEqual:1
            },
            sand_source_id: {
                required: false
            },
            sand_zone_id: {
                required: false
            },
            sand_quantity: {
                required: true,
                number:true,
                lessThanEqual:999,
                greaterThanEqual:1
            },
            aggregate_source_id: {
                required: false
            },
            aggregate1_sub_category_id: {
                required: false
            },
            aggregate1_quantity: {
                required: true,
                number: true,
                lessThanEqual:999,
                greaterThanEqual:1
            },
            aggregate2_sub_category_id: {
                required: false
            },
            aggregate2_quantity: {
                required: true,
                number: true,
                lessThanEqual:999,
                greaterThanEqual:1
            },
            fly_ash_quantity: {
                required: function(){
                    if($("#mix_fly_ash_brand").val() != null){
                        return true;
                    }else{
                        return false;
                    }
                },
                number: true
            },
            ad_mixture_brand_id: {
                required: false
            },
            ad_mixture_category_id: {
                required: false
            },
            ad_mixture_quantity: {
                required: true,
                number: true
            },
            water_quantity: {
                required: true,
                number: true
            },
            selling_price: {
                required: false,
                number: true,
                greaterThanEqual:1,
                maxlength: 8
            },
            description: {
                required: true,
            },
            is_availble: {
                required: false
            },
            is_custom: {
                required: false
            },
            address_id: {
                required: true
            },
            // concrete_pump_image : {
            //     required: function(){
            //         return $("form[name='add_concrete_pump_form'] #add_concrete_pump_form_type").val() == 'add' ? true :false; 
            //     },
            //     extension: "jpg|png|jpeg"
            // },
            
        },
        messages: {
            concrete_pump_image : {
                extension: "Please enter valid file (jpg, png, jpeg) formats"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "concrete_pump_image" ) {
              error.insertAfter("#concrete_pump_image_div");
            } else if (element.attr("name") == "is_availble" ) {
              error.insertAfter("#is_availble_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_mix_form"] p.error').remove();

            var form_type = $("form[name='add_mix_form'] #add_mix_form_type").val();
            // var form_values = $("form[name='add_mix_form']").serialize();
            console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            var is_concrete_density_error = $("#is_concrete_density_error").val();
            var is_admixture_percent_error = $("#is_admixture_percent_error").val();
            var is_WC_ratio_error = $("#is_WC_ratio_error").val();

            var is_error = 0;

            if(is_concrete_density_error == 1){
                $("#concrete_density_error_span").html("Concrete Density must be between 2400 - 2550 KG");

                is_error = 1;

                return;
            }
            
            // if(is_admixture_percent_error == 1){
            //     $("#concrete_density_error_span").html("Admixture quantity should be between 1 - 4 KG");

            //     is_error = 1;

            //     return;
            // }
            
            // if(is_WC_ratio_error == 1){
            //     $("#concrete_density_error_span").html("Water / Cement ratio must be between 0.35% - 0.55%");

            //     is_error = 1;

            //     return;
            // }

            if(is_error == 0){
                $("#custom_design_mix_button").attr("disabled","true");
                if(form_type === 'add'){
                    addMix(form_values);
                }else{
                    updateMix(form_values);
                }
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_mix_form"] p.error').remove();
        }
    });
    
    
    $("form[name='add_primary_media_detail_form']").validate({
        rules: {
            
            primary_image : {
                required: function(){
                    return true; 
                },
                extension: "jpg|jpeg"
            }
            
        },
        messages: {
            primary_image : {
                extension: "Please enter valid file (jpg, jpeg) formats"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "primary_image" ) {
              error.insertAfter("#primary_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_primary_media_detail_form"] p.error').remove();

            // var form_type = $("form[name='add_primary_media_detail_form'] #add_primary_media_detail_form_type").val();
            // var form_values = $("form[name='add_primary_media_detail_form']").serialize();
            // console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            // if(form_type === 'add'){
                addPrimaryMedia(form_values);
            // }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_primary_media_detail_form"] p.error').remove();
        }
    });
    
    $("form[name='add_secondory_media_detail_form']").validate({
        rules: {
            
            'secondory_image[]' : {
                required: function(){
                    return true; 
                },
                extension: "jpg|jpeg"
            }
            
        },
        messages: {
            'secondory_image[]' : {
                extension: "Please enter valid file (jpg, jpeg) formats"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "secondory_image[]" ) {
              error.insertAfter("#secondory_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#secondory_image_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#secondory_image_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_secondory_media_detail_form"] p.error').remove();

            // var form_type = $("form[name='add_secondory_media_detail_form'] #add_secondory_media_detail_form_type").val();
            // var form_values = $("form[name='add_secondory_media_detail_form']").serialize();
            // console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            // if(form_type === 'add'){
                addSecondoryMedia(form_values);
            // }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_secondory_media_detail_form"] p.error').remove();
        }
    });


    $('#ad_mixture_brand_id').on('change', function() {
        
        
        // alert( $('option:selected', this).val() );

        getAdmixtureTypes($('option:selected', this).val());
        
      });

});

function getSecondAggregateSubCat(sub_cat_id){

        
    var request_data = {
        "sub_cat_id":sub_cat_id
    }

    customResponseHandler(
        "/supplier/get_agregate_sub_cat_2/"+sub_cat_id, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // showToast("Quantity updated","Success");
                var html = '<option disabled="disabled" selected="selected">Select Aggregate category 2</option>';
                $.each(data["data"],function(key,value){

                    // console.log(value);

                    html += '<option value="'+value["_id"]+'">'+value["sub_category_name"]+'</option>' 

                });

                $("#mix_sub_cat_2").html(html);

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );

    


}

function addPrimaryMedia(form_values){

    responsehandlerWithFiles("supplier/media_design_mix/add", 
                    "POST", 
                    form_values, 
                    "#add-primary-media-details",
                    "Primary media Added successfully",
                    0,
                    "add_primary_media_detail_form"
                );

}

function addSecondoryMedia(form_values){

    responsehandlerWithFiles("supplier/second_media_design_mix/add", 
                    "POST", 
                    form_values, 
                    "#add-secondory-media-details",
                    "Secondory media Added successfully",
                    0,
                    "add_secondory_media_detail_form"
                );

}

function addMix(form_values){

    responsehandlerWithFiles("supplier/design_mix/add", 
                    "POST", 
                    form_values, 
                    "#add-concrete-pump-details",
                    "Design Mix Added successfully",
                    0,
                    "add_mix_form"
                );

}



function updateMix(form_values){

    responsehandlerWithFiles("supplier/design_mix/edit", 
                    "POST", 
                    form_values, 
                    "#add-concrete-pump-details",
                    "Design Mix updated successfully",
                    0,
                    "add_mix_form"
                );

}


function viewDesignMixOfGrade(grade_id){

        
        var request_data = {
            "grade_id":grade_id
        }

        customResponseHandler(
            "supplier/design_mix/get_grade_mix", // Ajax URl
            'GET', // Method call
            request_data, // Request data
            function success(data){ // onSuccess
                console.log(data);
                

                // data = JSON.parse(data);
                if((data["status"] >= 200) && (data["status"] < 300)){
                    // var message = data["data"]["message"];
                    
                    // showToast("Quantity updated","Success");

                    $("#design_mix_body").html(data["html"]);

                    $("#show-design-mix-details").modal("show");

                 }else{
                    // showToast(""+data["error"]["message"],"Error");              
                    showToast("Something went wrong. Please try again","Error");  
                    
                }
                

            }
            
        );


}

function viewDesignMixCombination(grade_id,grade_name){

        
        var request_data = {
            "grade_id":grade_id
        }

        customResponseHandler(
            "supplier/design_mix/get_mix_combination", // Ajax URl
            'GET', // Method call
            request_data, // Request data
            function success(data){ // onSuccess
                console.log(data);
                

                // data = JSON.parse(data);
                if((data["status"] >= 200) && (data["status"] < 300)){
                    // var message = data["data"]["message"];
                    
                    // showToast("Quantity updated","Success");

                    $("#design_mix_body").html(data["html"]);

                    note_html='<div class="bank-detail-main-block design_mix_popup_bx wht-tble-bg" id="design_mix_poup_footer">'+
                    '<div class="row">' +
                    '<b>Note:</b> <i>' +
                        '(1) To view other price combinations, select desired brand and source of materials.<br />' +
                        '(2) Only Acive brands & material source will be available to choose for price combination.' +
                    '</i>'+
                    '</div>'+
                '</div>';

                $("#design_mix_body").append(note_html);

                    $("#design_mix_popup_title").html("Standard Design Mix (Rate for Material Combinations) - "+grade_name);

                    $("#show-design-mix-details").modal("show");

                 }else{
                    // showToast(""+data["error"]["message"],"Error");              
                    showToast("Something went wrong. Please try again","Error");  
                    
                }
                

            }
            
        );


}


function getAdmixtureTypes(admix_brand_id){

        
        var request_data = {
            "admix_brand_id":admix_brand_id
        }

        customResponseHandler(
            "supplier/get_admixture_types", // Ajax URl
            'GET', // Method call
            request_data, // Request data
            function success(data){ // onSuccess
                // console.log(data);
                

                // data = JSON.parse(data);
                if((data["status"] >= 200) && (data["status"] < 300)){
                    // var message = data["data"]["message"];
                    
                    // showToast("Quantity updated","Success");
                    var html = '<option disabled="disabled" selected="selected">Select Admixture Code</option>';
                    $.each(data["data"],function(key,value){

                        // console.log(value);

                        html += '<option value="'+value["_id"]+'">'+value["category_name"]+' - '+value["admixture_type"]+'</option>' 

                    });

                    $("#ad_mixture_category_id").html(html);

                 }else{
                    // showToast(""+data["error"]["message"],"Error");              
                    showToast("Something went wrong. Please try again","Error");  
                    
                }
                

            }
            
        );


}

function getAdmixtureTypesForAddForm(admix_brand_id,admix_brand_name){

        
        var request_data = {
            "admix_brand_id":admix_brand_id
        }

        customResponseHandler(
            "supplier/get_admixture_types", // Ajax URl
            'GET', // Method call
            request_data, // Request data
            function success(data){ // onSuccess
                // console.log(data);
                

                // data = JSON.parse(data);
                if((data["status"] >= 200) && (data["status"] < 300)){
                    // var message = data["data"]["message"];
                    
                    // showToast("Quantity updated","Success");
                    var html = '<div id="admix_brand_div_'+admix_brand_id+'">';
                    html += '<label><b>'+admix_brand_name+' </b></label>';
                    $.each(data["data"],function(key,value){

                        // console.log(value);

                        // html += '<option value="'+value["_id"]+'">'+value["category_name"]+' - '+value["admixture_type"]+'</option>' 

                        html += '<div class="col-md-4" id="admix_type_div_'+value["_id"]+'" style="float: left;margin-bottom: 15px;">' +
                                '<div class="form-check form-check-inline">' +
                                    '<div class="">'+
                                        '<input class="form-check-input" type="checkbox" value="'+value["_id"]+'" id="" >' +
                                        '<input type="hidden" value="" id="" />'+
                                        '<span class="form-check-label">' +
                                            value["category_name"]+
                                        '</span>' +
                                        '<div style="margin-top: 15px;">' +
                                            '<input type="text" name="product_name_1" value="" class="form-control" placeholder="Add Stock for '+value["category_name"]+' In KG">' +
                                        '</div>'+
                                    '</div>'+                                 
                                '</div>' +
                            '</div>'
                    });
                    html += '</div>';
                    $("#admix_type_selection_box_div").append(html);

                 }else{
                    // showToast(""+data["error"]["message"],"Error");              
                    showToast("Something went wrong. Please try again","Error");  
                    
                }
                

            }
            
        );


}


function deleteMedia(mediaId){

    var message = "Are you sure you want to delete this image?"

    var request_data = {"mediaId" : mediaId}

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        responsehandler('supplier/media_design_mix/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Image deleted successfully",
            1,
            "add_proposal_form"
        );

    });    

}

// Design Mix Module Over..................................................

// Cement Rate Module Start..................................................

$(document).ready(function(){

    $("form[name='add_cement_rate_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            brand_id: {
                required: true
            },
            grade_id: {
                required: true
            },
            per_kg_rate: {
                required: true,
                number: true,
                maxlength:8,
                greaterThanEqual:1
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_cement_rate_form"] p.error').remove();

            var form_type = $("form[name='add_cement_rate_form'] #add_cement_rate_form_type").val();
            var form_values = $("form[name='add_cement_rate_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addCementRate(form_values);
            }else{
                // updateCementRate(form_values);
                requestOTP(16,form_values,"edit_cement_rate");
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_cement_rate_form"] p.error').remove();
        }
    });

});

function addCementRate(form_values){

    responsehandler("supplier/cement_rate/add", 
                    "POST", 
                    form_values, 
                    "#add-cement-rate-details",
                    "Cement Rate Added successfully",
                    0,
                    "add_cement_rate_form"
                );

}

function showCementRateEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_cement_rate_form');

    $("#add_cement_rate_title").html("Edit Cement Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#brand_id').val(data['cement_brand']['_id']);
    $('#grade_id').val(data['cement_grade']['_id']);
    $('#per_kg_rate').val(data['per_kg_rate']);
    

    $('#add_cement_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-cement-rate-details').modal('show');

}

function showCementRateInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_cement_rate_form');

    $("#add_cement_rate_title").html("Edit Cement Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#brand_id').val(data['cement_brand']['_id']);
    $('#grade_id').val(data['cement_grade']['_id']);
    $('#per_kg_rate').val(0);
    

    $('#add_cement_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-cement-rate-details').modal('show');
    var form_values = $("form[name='add_cement_rate_form']").serialize();
    form_values = form_values+'&is_active=0';
    requestOTP(16,form_values,"edit_cement_rate");
}

function updateCementRate(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/cement_rate/update", 
                    "POST", 
                    form_values, 
                    "#add-cement-rate-details",
                    "Cement Rate updated successfully",
                    0,
                    "add_cement_rate_form"
                );

}

function deleteCementRate(rate_id){

    var message = "Are you sure you want to delete this?"

    var request_data = {"rate_id" : rate_id}

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        responsehandler('supplier/cement_rate/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Cement Rate deleted successfully",
            1,
            "add_cement_rate_form"
        );

    });    

}



// Cement Rate Module Over..................................................


// Sand Rate Module Start..................................................

$(document).ready(function(){

    $("form[name='add_sand_rate_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            source_id: {
                required: true
            },
            sand_zone_id: {
                required: true
            },
            per_kg_rate: {
                required: true,
                number: true,
                greaterThanEqual:1,
                maxlength:8
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_sand_rate_form"] p.error').remove();

            var form_type = $("form[name='add_sand_rate_form'] #add_sand_rate_form_type").val();
            var form_values = $("form[name='add_sand_rate_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addSandRate(form_values);
            }else{
                // updateSandRate(form_values);
                requestOTP(17,form_values,"edit_sand_rate");
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_sand_rate_form"] p.error').remove();
        }
    });

});

function addSandRate(form_values){

    responsehandler("supplier/sand_rate/add", 
                    "POST", 
                    form_values, 
                    "#add-sand-rate-details",
                    "Sand Rate Added successfully",
                    0,
                    "add_sand_rate_form"
                );

}

function showSandRateEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_sand_rate_form');

    $("#add_cement_rate_title").html("Edit Sand Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['sand_source']['_id']);
    if(data['sand_zone'] != undefined){
        $('#sand_zone_id').val(data['sand_zone']['_id']);
    }
    $('#per_kg_rate').val(data['per_kg_rate']);
    

    $('#add_sand_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-sand-rate-details').modal('show');

}

function showSandRateInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_sand_rate_form');

    $("#add_cement_rate_title").html("Edit Sand Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['sand_source']['_id']);
    if(data['sand_zone'] != undefined){
        $('#sand_zone_id').val(data['sand_zone']['_id']);
    }
    $('#per_kg_rate').val(0);
    

    $('#add_sand_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-sand-rate-details').modal('show');
    var form_values = $("form[name='add_sand_rate_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(17,form_values,"edit_sand_rate");
}

function updateSandRate(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/sand_rate/update", 
                    "POST", 
                    form_values, 
                    "#add-sand-rate-details",
                    "Sand Rate updated successfully",
                    0,
                    "add_sand_rate_form"
                );

}

function deleteSandRate(rate_id){

    var message = "Are you sure you want to delete this?"

    var request_data = {"rate_id" : rate_id}

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        responsehandler('supplier/sand_rate/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Sand Rate deleted successfully",
            1,
            "add_sand_rate_form"
        );

    });    

}



// Sand Rate Module Over..................................................


// Aggregate Rate Module Start..................................................

$(document).ready(function(){

    $("form[name='add_aggregate_rate_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            source_id: {
                required: true
            },
            sub_cat_id: {
                required: true
            },
            per_kg_rate: {
                required: true,
                number: true,
                greaterThanEqual:1,
                maxlength:8
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_aggregate_rate_form"] p.error').remove();

            var form_type = $("form[name='add_aggregate_rate_form'] #add_aggregate_rate_form_type").val();
            var form_values = $("form[name='add_aggregate_rate_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addAggregateRate(form_values);
            }else{
                // updateAggregateRate(form_values);
                requestOTP(18,form_values,"edit_agg_rate");
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_aggregate_rate_form"] p.error').remove();
        }
    });

});

function addAggregateRate(form_values){

    responsehandler("supplier/aggregate_rate/add", 
                    "POST", 
                    form_values, 
                    "#add-aggregate-rate-details",
                    "Aggregate Rate Added successfully",
                    0,
                    "add_aggregate_rate_form"
                );

}

function showAggregateRateEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_aggregate_rate_form');

    $("#add_aggregate_rate_title").html("Edit Aggregate Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['aggregate_source']['_id']);
    $('#sub_cat_id').val(data['aggregate_sand_sub_category']['_id']);
    $('#per_kg_rate').val(data['per_kg_rate']);
    

    $('#add_aggregate_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-aggregate-rate-details').modal('show');

}

function showAggregateRateInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_aggregate_rate_form');

    $("#add_aggregate_rate_title").html("Edit Aggregate Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['aggregate_source']['_id']);
    $('#sub_cat_id').val(data['aggregate_sand_sub_category']['_id']);
    $('#per_kg_rate').val(0);
    

    $('#add_aggregate_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-aggregate-rate-details').modal('show');
    var form_values = $("form[name='add_aggregate_rate_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(18,form_values,"edit_agg_rate");
}

function updateAggregateRate(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/aggregate_rate/update", 
                    "POST", 
                    form_values, 
                    "#add-aggregate-rate-details",
                    "Aggregate Rate updated successfully",
                    0,
                    "add_aggregate_rate_form"
                );

}

function deleteAggregateRate(rate_id){

    var message = "Are you sure you want to delete this?"

    var request_data = {"rate_id" : rate_id}

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        responsehandler('supplier/aggregate_rate/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Aggregate Rate deleted successfully",
            1,
            "add_aggregate_rate_form"
        );

    });    

}



// Aggregate Rate Module Over..................................................


// Flyash Rate Module Start..................................................

$(document).ready(function(){

    $("form[name='add_flyash_rate_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            source_id: {
                required: true
            },
            per_kg_rate: {
                required: true,
                number: true,
                greaterThanEqual: 1,
                maxlength:8
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_flyash_rate_form"] p.error').remove();

            var form_type = $("form[name='add_flyash_rate_form'] #add_flyash_rate_form_type").val();
            var form_values = $("form[name='add_flyash_rate_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addFlyashRate(form_values);
            }else{
                // updateFlyashRate(form_values);
                requestOTP(19,form_values,"edit_flyAsh_rate");
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_flyash_rate_form"] p.error').remove();
        }
    });

});

function addFlyashRate(form_values){

    responsehandler("supplier/flyash_rate/add", 
                    "POST", 
                    form_values, 
                    "#add-flyash-rate-details",
                    "Flyash Rate Added successfully",
                    0,
                    "add_flyash_rate_form"
                );

}

function showFlyashRateEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_flyash_rate_form');

    $("#add_flyash_rate_title").html("Edit Fly Ash Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['fly_ash_source']['_id']);
    $('#per_kg_rate').val(data['per_kg_rate']);
    

    $('#add_flyash_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-flyash-rate-details').modal('show');

}

function showFlyashRateInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_flyash_rate_form');

    $("#add_flyash_rate_title").html("Edit Fly Ash Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['fly_ash_source']['_id']);
    $('#per_kg_rate').val(0);
    

    $('#add_flyash_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-flyash-rate-details').modal('show');
    var form_values = $("form[name='add_flyash_rate_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(19,form_values,"edit_flyAsh_rate");
}

function updateFlyashRate(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/flyash_rate/update", 
                    "POST", 
                    form_values, 
                    "#add-flyash-rate-details",
                    "Flyash Rate updated successfully",
                    0,
                    "add_flyash_rate_form"
                );

}

function deleteFlyashRate(rate_id){

    var message = "Are you sure you want to delete this?"

    var request_data = {"rate_id" : rate_id}

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        responsehandler('supplier/flyash_rate/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Flyash Rate deleted successfully",
            1,
            "add_flyash_rate_form"
        );

    });    

}



// Flyash Rate Module Over..................................................


// Admixture Rate Module Start..................................................

$(document).ready(function(){

    $("form[name='add_admixture_rate_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            brand_id: {
                required: true
            },
            ad_mixture_category_id: {
                required: true
            },
            per_kg_rate: {
                required: true,
                number: true,
                greaterThanEqual:1,
                maxlength:8
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_admixture_rate_form"] p.error').remove();

            var form_type = $("form[name='add_admixture_rate_form'] #add_admixture_rate_form_type").val();
            var form_values = $("form[name='add_admixture_rate_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addAdmixtureRate(form_values);
            }else{
                // updateAdmixtureRate(form_values);
                requestOTP(20,form_values,"edit_admix_rate");
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_admixture_rate_form"] p.error').remove();
        }
    });

});

function addAdmixtureRate(form_values){

    responsehandler("supplier/admixture_rate/add", 
                    "POST", 
                    form_values, 
                    "#add-admixture-rate-details",
                    "Admixture Rate Added successfully",
                    0,
                    "add_admixture_rate_form"
                );

}

function showAdmixtureRateEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_admixture_rate_form');

    $("#add_admixture_rate_title").html("Edit Admixture Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#ad_mixture_brand_id').val(data['admixture_brand']['_id']);
    $('#ad_mixture_category_id').val(data['admixture_category']['_id']);
    $('#per_kg_rate').val(data['per_kg_rate']);
    

    $('#add_admixture_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-admixture-rate-details').modal('show');

}

function showAdmixtureRateInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_admixture_rate_form');

    $("#add_admixture_rate_title").html("Edit Admixture Per KG Rate");

    $('#address_id').val(data['address']['_id']);
    $('#ad_mixture_brand_id').val(data['admixture_brand']['_id']);
    $('#ad_mixture_category_id').val(data['admixture_category']['_id']);
    $('#per_kg_rate').val(0);
    

    $('#add_admixture_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-admixture-rate-details').modal('show');
    var form_values = $("form[name='add_admixture_rate_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(20,form_values,"edit_admix_rate");


}

function updateAdmixtureRate(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/admixture_rate/update", 
                    "POST", 
                    form_values, 
                    "#add-admixture-rate-details",
                    "Admixture Rate updated successfully",
                    0,
                    "add_admixture_rate_form"
                );

}

function deleteAdmixtureRate(rate_id){

    var message = "Are you sure you want to delete this?"

    var request_data = {"rate_id" : rate_id}

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        responsehandler('supplier/admixture_rate/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Admixture Rate deleted successfully",
            1,
            "add_admixture_rate_form"
        );

    });    

}



// Admixture Rate Module Over..................................................

// Water Rate Module Start..................................................

$(document).ready(function(){

    $("form[name='add_water_rate_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            water_type: {
                required: true
            },
            per_kg_rate: {
                required: true,
                number: true,
                greaterThanEqual: 1,
                maxlength: 8
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_water_rate_form"] p.error').remove();

            var form_type = $("form[name='add_water_rate_form'] #add_water_rate_form_type").val();
            var form_values = $("form[name='add_water_rate_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addWaterRate(form_values);
            }else{
                // updateWaterRate(form_values);
                requestOTP(21,form_values,"edit_water_rate");
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_water_rate_form"] p.error').remove();
        }
    });

});

function addWaterRate(form_values){

    responsehandler("supplier/water_rate/add", 
                    "POST", 
                    form_values, 
                    "#add-water-rate-details",
                    "Water Rate Added successfully",
                    0,
                    "add_water_rate_form"
                );

}

function showWaterRateEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_water_rate_form');

    $("#add_water_rate_title").html("Edit Water Per LTR Rate");

    $('#address_id').val(data['address']['_id']);
    $('#per_kg_rate').val(data['per_ltr_rate']);

    if(data['water_type'] != null){
        $('#water_type_drop').val(data['water_type']);
    }
    

    $('#add_water_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-water-rate-details').modal('show');

}

function showWaterRateInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_water_rate_form');

    $("#add_water_rate_title").html("Edit Water Per LTR Rate");

    $('#address_id').val(data['address']['_id']);
    $('#per_kg_rate').val(0);
    

    $('#add_water_rate_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-water-rate-details').modal('show');
    var form_values = $("form[name='add_water_rate_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(21,form_values,"edit_water_rate");
}

function updateWaterRate(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/water_rate/update", 
                    "POST", 
                    form_values, 
                    "#add-water-rate-details",
                    "Water Rate updated successfully",
                    0,
                    "add_water_rate_form"
                );

}

function deleteWaterRate(rate_id){

    var message = "Are you sure you want to delete this?"

    var request_data = {"rate_id" : rate_id}

    confirmDialog(message, function(){
        // console.log(data);
        // console.log("deleted!.."+type);

        responsehandler('supplier/water_rate/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Water Rate deleted successfully",
            1,
            "add_water_rate_form"
        );

    });    

}



// Water Rate Module Over..................................................

//Rate Module Over...................................................................................................


// Cement Stock Module Start..................................................

$(document).ready(function(){

    $("form[name='add_cement_stock_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            brand_id: {
                required: true
            },
            grade_id: {
                required: true
            },
            quantity: {
                required: true,
                number: true,
                greaterThanEqual: 1,
                maxlength:8,
                noDecimal: true
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_cement_stock_form"] p.error').remove();

            var form_type = $("form[name='add_cement_stock_form'] #add_cement_stock_form_type").val();
            var form_values = $("form[name='add_cement_stock_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addCementStock(form_values);
            }else{
                // $("#add-cement-stock-details").modal("hide");
                requestOTP(5,form_values,"edit_cement_stock");
                
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_cement_stock_form"] p.error').remove();
        }
    });

});

function addCementStock(form_values){

    responsehandler("supplier/cement_stock/add", 
                    "POST", 
                    form_values, 
                    "#add-cement-stock-details",
                    "Cement Stock Added successfully",
                    0,
                    "add_cement_stock_form"
                );

}

function showCementStockEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_cement_stock_form');

    $("#add_cement_stock_title").html("Edit Cement Stock");

    $('#address_id').val(data['address']['_id']);
    $('#brand_id').val(data['cement_brand']['_id']);
    $('#grade_id').val(data['cement_grade']['_id']);
    $('#quantity').val(data['quantity']);
    

    $('#add_cement_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-cement-stock-details').modal('show');

}

function showCementStockInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_cement_stock_form');

    $("#add_cement_stock_title").html("Edit Cement Stock");

    $('#address_id').val(data['address']['_id']);
    $('#brand_id').val(data['cement_brand']['_id']);
    $('#grade_id').val(data['cement_grade']['_id']);
    $('#quantity').val(0);
    

    $('#add_cement_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-cement-stock-details').modal('show');

    var form_values = $("form[name='add_cement_stock_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(5,form_values,"edit_cement_stock");

}

function updateCementStock(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/cement_stock/update", 
                    "POST", 
                    form_values, 
                    "#add-cement-stock-details",
                    "Cement Stock updated successfully",
                    1,
                    "add_cement_stock_form"
                );

}

function deleteCementStock(rate_id,otp){

    var message = "Are you sure you want to delete this?"

    var request_data = {
        "rate_id" : rate_id,
        "authentication_code" : otp
    }

    responsehandler('supplier/cement_stock/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Cement Stock deleted successfully",
            1,
            "add_cement_stock_form"
        );

    // confirmDialog(message, function(){
    //     // console.log(data);
    //     // console.log("deleted!.."+type);

        

    // });    

}



// Cement Stock Module Over..................................................


// Sand Stock Module Start..................................................

$(document).ready(function(){

    $("form[name='add_sand_stock_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            source_id: {
                required: true
            },
            sand_zone_id: {
                required: true
            },
            quantity: {
                required: true,
                number: true,
                greaterThanEqual: 1,
                maxlength: 8,
                noDecimal: true
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_sand_stock_form"] p.error').remove();

            var form_type = $("form[name='add_sand_stock_form'] #add_sand_stock_form_type").val();
            var form_values = $("form[name='add_sand_stock_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addSandStock(form_values);
            }else{
                // $("#add-sand-stock-details").modal("hide");
                requestOTP(7,form_values,"edit_sand_stock");
                
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_sand_stock_form"] p.error').remove();
        }
    });

});

function addSandStock(form_values){

    responsehandler("supplier/sand_stock/add", 
                    "POST", 
                    form_values, 
                    "#add-sand-stock-details",
                    "Sand Stock Added successfully",
                    0,
                    "add_sand_stock_form"
                );

}

function showSandStockEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_sand_stock_form');

    $("#add_sand_stock_title").html("Edit Sand Stock");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['sand_source']['_id']);
    if(data['sand_zone'] != undefined){
        $('#sand_zone_id').val(data['sand_zone']['_id']);
    }
    $('#quantity').val(data['quantity']);
    

    $('#add_sand_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-sand-stock-details').modal('show');

    

}

function showSandStockInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_sand_stock_form');

    $("#add_sand_stock_title").html("Edit Sand Stock");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['sand_source']['_id']);
    if(data['sand_zone'] != undefined){
        $('#sand_zone_id').val(data['sand_zone']['_id']);
    }
    $('#quantity').val(1);
    

    $('#add_sand_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-sand-stock-details').modal('show');

    var form_values = $("form[name='add_sand_stock_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(7,form_values,"edit_sand_stock");

}

function updateSandStock(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/sand_stock/update", 
                    "POST", 
                    form_values, 
                    "#add-sand-stock-details",
                    "Sand Stock updated successfully",
                    1,
                    "add_sand_stock_form"
                );

}

function deleteSandStock(rate_id,otp){

    var message = "Are you sure you want to delete this?"

    var request_data = {
        "rate_id" : rate_id,
        "authentication_code" : otp
    }

    responsehandler('supplier/sand_stock/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Sand Stock deleted successfully",
            1,
            "add_sand_stock_form"
        );

    // confirmDialog(message, function(){
    //     // console.log(data);
    //     // console.log("deleted!.."+type);

        

    // });    

}



// Sand Stock Module Over..................................................


// Aggregate Stock Module Start..................................................

$(document).ready(function(){

    $("form[name='add_aggregate_stock_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            source_id: {
                required: true
            },
            sub_cat_id: {
                required: true
            },
            quantity: {
                required: true,
                number: true,
                greaterThanEqual: 1,
                maxlength: 8,
                noDecimal: true
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_aggregate_stock_form"] p.error').remove();

            var form_type = $("form[name='add_aggregate_stock_form'] #add_aggregate_stock_form_type").val();
            var form_values = $("form[name='add_aggregate_stock_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addAggregateStock(form_values);
            }else{
                // $("#add-aggregate-stock-details").modal("hide");
                requestOTP(9,form_values,"edit_agg_stock");
                
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_aggregate_stock_form"] p.error').remove();
        }
    });

});

function addAggregateStock(form_values){

    responsehandler("supplier/aggregate_stock/add", 
                    "POST", 
                    form_values, 
                    "#add-aggregate-stock-details",
                    "Aggregate Stock Added successfully",
                    0,
                    "add_aggregate_stock_form"
                );

}

function showAggregateStockEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_aggregate_stock_form');

    $("#add_aggregate_stock_title").html("Edit Aggregate Stock");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['aggregate_source']['_id']);
    $('#sub_cat_id').val(data['aggregate_sand_sub_category']['_id']);
    $('#quantity').val(data['quantity']);
    

    $('#add_aggregate_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-aggregate-stock-details').modal('show');

}

function showAggregateStockInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_aggregate_stock_form');

    $("#add_aggregate_stock_title").html("Edit Aggregate Stock");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['aggregate_source']['_id']);
    $('#sub_cat_id').val(data['aggregate_sand_sub_category']['_id']);
    $('#quantity').val(0);
    

    $('#add_aggregate_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-aggregate-stock-details').modal('show');
    var form_values = $("form[name='add_aggregate_stock_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(9,form_values,"edit_agg_stock");

}

function updateAggregateStock(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/aggregate_stock/update", 
                    "POST", 
                    form_values, 
                    "#add-aggregate-stock-details",
                    "Aggregate Stock updated successfully",
                    1,
                    "add_aggregate_stock_form"
                );

}

function deleteAggregateStock(rate_id,otp){

    var message = "Are you sure you want to delete this?"

    var request_data = {
        "rate_id" : rate_id,
        "authentication_code" : otp
    }

    responsehandler('supplier/aggregate_stock/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Aggregate Stock deleted successfully",
            1,
            "add_aggregate_stock_form"
        );

    // confirmDialog(message, function(){
    //     // console.log(data);
    //     // console.log("deleted!.."+type);

        

    // });    

}



// Aggregate Stock Module Over..................................................


// Flyash Stock Module Start..................................................

$(document).ready(function(){

    $("form[name='add_flyash_stock_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            source_id: {
                required: true
            },
            quantity: {
                required: true,
                number: true,
                greaterThanEqual: 1,
                maxlength: 8,
                noDecimal: true
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_flyash_stock_form"] p.error').remove();

            var form_type = $("form[name='add_flyash_stock_form'] #add_flyash_stock_form_type").val();
            var form_values = $("form[name='add_flyash_stock_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addFlyashStock(form_values);
            }else{
                // $("#add-flyash-stock-details").modal("hide");
                requestOTP(11,form_values,"edit_flyAsh_stock");
                
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_flyash_stock_form"] p.error').remove();
        }
    });

});

function addFlyashStock(form_values){

    responsehandler("supplier/flyash_stock/add", 
                    "POST", 
                    form_values, 
                    "#add-flyash-stock-details",
                    "Flyash Stock Added successfully",
                    0,
                    "add_flyash_stock_form"
                );

}

function showFlyashStockEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_flyash_stock_form');

    $("#add_flyash_stock_title").html("Edit Flyash Stock");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['fly_ash_source']['_id']);
    $('#quantity').val(data['quantity']);
    

    $('#add_flyash_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-flyash-stock-details').modal('show');

}

function showFlyashStockInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_flyash_stock_form');

    $("#add_flyash_stock_title").html("Edit Flyash Stock");

    $('#address_id').val(data['address']['_id']);
    $('#source_id').val(data['fly_ash_source']['_id']);
    $('#quantity').val(0);
    

    $('#add_flyash_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-flyash-stock-details').modal('show');
    var form_values = $("form[name='add_flyash_stock_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(11,form_values,"edit_flyAsh_stock");

}

function updateFlyashStock(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/flyash_stock/update", 
                    "POST", 
                    form_values, 
                    "#add-flyash-stock-details",
                    "Flyash Stock updated successfully",
                    1,
                    "add_flyash_stock_form"
                );

}

function deleteFlyashStock(rate_id,otp){

    var message = "Are you sure you want to delete this?"

    var request_data = {
        "rate_id" : rate_id,
        "authentication_code" : otp
    }

    responsehandler('supplier/flyash_stock/delete', 
        "GET", 
        request_data,
        "#add-proposal",
        "Flyash Stock deleted successfully",
        1,
        "add_flyash_stock_form"
    );

    // confirmDialog(message, function(){
    //     // console.log(data);
    //     // console.log("deleted!.."+type);

        

    // });    

}



// Flyash Stock Module Over..................................................


// Admixture Stock Module Start..................................................

$(document).ready(function(){

    $("form[name='add_admixture_stock_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            brand_id: {
                required: true
            },
            ad_mixture_category_id: {
                required: true
            },
            quantity: {
                required: true,
                number: true,
                greaterThanEqual: 1,
                maxlength:8,
                noDecimal: true
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_admixture_stock_form"] p.error').remove();

            var form_type = $("form[name='add_admixture_stock_form'] #add_admixture_stock_form_type").val();
            var form_values = $("form[name='add_admixture_stock_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addAdmixtureStock(form_values);
            }else{
                // $("#add-admixture-stock-details").modal("hide");
                requestOTP(13,form_values,"edit_admix_stock");
                
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_admixture_stock_form"] p.error').remove();
        }
    });

});

function addAdmixtureStock(form_values){

    responsehandler("supplier/admixture_stock/add", 
                    "POST", 
                    form_values, 
                    "#add-admixture-stock-details",
                    "Admixture Stock Added successfully",
                    0,
                    "add_admixture_stock_form"
                );

}

function showAdmixtureStockEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_admixture_stock_form');

    $("#add_admixture_stock_title").html("Edit Admixture Stock");

    $('#address_id').val(data['address']['_id']);
    $('#ad_mixture_brand_id').val(data['admixture_brand']['_id']);
    $('#ad_mixture_category_id').val(data['admixture_category']['_id']);
    $('#quantity').val(data['quantity']);
    

    $('#add_admixture_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    $('#add-admixture-stock-details').modal('show');

}

function showAdmixtureStockInactiveDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_admixture_stock_form');

    $("#add_admixture_stock_title").html("Edit Admixture Stock");

    $('#address_id').val(data['address']['_id']);
    $('#ad_mixture_brand_id').val(data['admixture_brand']['_id']);
    $('#ad_mixture_category_id').val(data['admixture_category']['_id']);
    $('#quantity').val(0);
    

    $('#add_admixture_stock_form_type').val("edit");
    $('#rate_id').val(data['_id']);

    // $('#add-admixture-stock-details').modal('show');
    var form_values = $("form[name='add_admixture_stock_form']").serialize();
    form_values = form_values + "&is_active=0";
    requestOTP(13,form_values,"edit_admix_stock");

}

function updateAdmixtureStock(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/admixture_stock/update", 
                    "POST", 
                    form_values, 
                    "#add-admixture-stock-details",
                    "Admixture Stock updated successfully",
                    1,
                    "add_admixture_stock_form"
                );

}

function deleteAdmixtureStock(rate_id,otp){

    var message = "Are you sure you want to delete this?"

    var request_data = {
        "rate_id" : rate_id,
        "authentication_code" : otp
    }

    responsehandler('supplier/admixture_stock/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Admixture Stock deleted successfully",
            1,
            "add_admixture_stock_form"
        );

    // confirmDialog(message, function(){
    //     // console.log(data);
    //     // console.log("deleted!.."+type);

        

    // });    

}



// Admixture Stock Module Over..................................................


// Setting Module Start..................................................

$(document).ready(function(){

    $("form[name='setting_form']").validate({
        rules: {
            TM_price: {
                required: true,
                number: true
            },
            CP_price: {
                required: true,
                number: true
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="setting_form"] p.error').remove();

            // var form_type = $("form[name='setting_form'] #setting_form_type").val();
            var form_values = $("form[name='setting_form']").serialize();
            // console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            
                updateSettings(form_values);
            
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="setting_form"] p.error').remove();
        }
    });
    
    
    $("form[name='admixture_rate_setting_form']").validate({
        rules: {
            less_than_5_ad_mixture_brand_id_1:{
                required: true
            },
            less_than_5_ad_mixture_category_id_1:{
                required: true
            },
            less_than_5_ad_mixture_brand_id_2:{
                required: true
            },
            less_than_5_ad_mixture_category_id_2:{
                required: true
            },
            less_than_5_rate:{
                required: true
            },
            less_than_5_rate:{
                required: true,
                number:true,
                lessThanEqual:250,
                greaterThanEqual:1

            },
            ad_5_10_ad_mixture_brand_id_1:{
                required: true,
            },
            ad_5_10_ad_mixture_category_id_1:{
                required: true
            },
            ad_5_10_ad_mixture_brand_id_2:{
                required: true
            },
            ad_5_10_ad_mixture_category_id_2:{
                required: true
            },
            ad_5_10_rate:{
                required: true,
                number:true,
                lessThanEqual:250,
                greaterThanEqual:1
            },
            ad_11_15_ad_mixture_brand_id_1:{
                required: true
            },
            ad_11_15_ad_mixture_category_id_1:{
                required: true
            },
            ad_11_15_ad_mixture_brand_id_2:{
                required: true
            },
            ad_11_15_ad_mixture_category_id_2:{
                required: true
            },
            ad_11_15_rate:{
                required: true,
                number:true,
                lessThanEqual:250,
                greaterThanEqual:1
            },
            ad_16_20_ad_mixture_brand_id_1:{
                required: true
            },
            ad_16_20_ad_mixture_category_id_1:{
                required: true
            },
            ad_16_20_ad_mixture_brand_id_2:{
                required: true
            },
            ad_16_20_ad_mixture_category_id_2:{
                required: true
            },
            ad_16_20_rate:{
                required: true,
                number:true,
                lessThanEqual:250,
                greaterThanEqual:1
            },
            ad_21_25_ad_mixture_brand_id_1:{
                required: true
            },
            ad_21_25_ad_mixture_category_id_1:{
                required: true
            },
            ad_21_25_ad_mixture_brand_id_2:{
                required: true
            },
            ad_21_25_ad_mixture_category_id_2:{
                required: true
            },
            ad_21_25_ad_mixture_category_id_2:{
                required: true
            },
            ad_21_25_rate:{
                required: true,
                number:true,
                lessThanEqual:250,
                greaterThanEqual:1
            },
            ad_26_30_ad_mixture_brand_id_1:{
                required: true
            },
            ad_26_30_ad_mixture_category_id_1:{
                required: true
            },
            ad_26_30_ad_mixture_brand_id_2:{
                required: true
            },
            ad_26_30_ad_mixture_category_id_2:{
                required: true
            },
            ad_26_30_rate:{
                required: true,
                number:true,
                lessThanEqual:250,
                greaterThanEqual:1
            },
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="admixture_rate_setting_form"] p.error').remove();

            // var form_type = $("form[name='admixture_rate_setting_form'] #admixture_rate_setting_form_type").val();
            var form_values = $("form[name='admixture_rate_setting_form']").serialize();
            // console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            
            AdmixupdateSettings(form_values);
            
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="admixture_rate_setting_form"] p.error').remove();
        }
    });


    $('#less_than_5_ad_mixture_brand_id_1').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"less_than_5_ad_mixture_category_id_1");
           

    });
    
    $('#less_than_5_ad_mixture_brand_id_2').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"less_than_5_ad_mixture_category_id_2");
           

    });
    
    $('#5_10_ad_mixture_brand_id_1').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"5_10_ad_mixture_category_id_1");
           

    });
    
    $('#5_10_ad_mixture_brand_id_2').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"5_10_ad_mixture_category_id_2");
           

    });
    
    $('#11_15_ad_mixture_brand_id_1').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"11_15_ad_mixture_category_id_1");
           

    });
    
    $('#11_15_ad_mixture_brand_id_2').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"11_15_ad_mixture_category_id_2");
           

    });
    
    $('#11_15_ad_mixture_brand_id_2').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"11_15_ad_mixture_category_id_2");
           

    });
    
    $('#16_20_ad_mixture_brand_id_1').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"16_20_ad_mixture_category_id_1");
           

    });
    
    $('#16_20_ad_mixture_brand_id_2').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"16_20_ad_mixture_category_id_2");
           

    });
    
    $('#21_25_ad_mixture_brand_id_1').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"21_25_ad_mixture_category_id_1");
           

    });
    
    $('#21_25_ad_mixture_brand_id_2').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"21_25_ad_mixture_category_id_2");
           

    });
    
    $('#26_30_ad_mixture_brand_id_1').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"26_30_ad_mixture_category_id_1");
           

    });
    
    $('#26_30_ad_mixture_brand_id_2').change(function() {       
        
        var data = getSettingAdmixtureTypes($('option:selected', this).val(),"26_30_ad_mixture_category_id_2");
           

    });
    
    $('#is_order_accept').change(function() {

        console.log("is_order_accept..."+this.checked)
        if(this.checked) {
            $("#is_order_accept").prop("checked",false);
            // orderAceeptOrNot(this.checked,'');
            requestOTPToMaster(2,this.checked,'');
        }else{
            $("#is_order_accept").prop("checked",true);
            requestOTPToMaster(1,this.checked,'');
        }

        
                
    });

    $("#TM_unavailable_start_time").change(function() {

        // $("#order_assign_qty_success").html("");
        // $('#order_assign_qty_error').html("");

        var start_time = $('option:selected', this).val();

        if(start_time.length > 0){
            $("#TM_unavailable_end_time").removeAttr("disabled");

            var selected_time_key = (time_key_array[start_time] + 1);
            // var assigned_quantity_count = assigned_quantity + selected_time_key;
            var html = '<option disabled="disabled" selected="selected">End Time</option>';
            for(var key=selected_time_key;key<time_array.length;key++){

                if(time_array[key] != undefined){
                    html += '<option value="'+time_array[key]+'">'+time_array[key]+'</option>';
                }

            };

            $("#TM_unavailable_end_time").html(html);
        }

        
    });


    $("form[name='add_TM_Unavail_form']").validate({
        rules: {
            unavailable_at: {
                required: true
            },
            start_time: {
                required: true
            },
            end_time: {
                required: true
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_TM_Unavail_form"] p.error').remove();

            var form_type = $("#TM_unavail_type").val();
            var form_values = $("form[name='add_TM_Unavail_form']").serialize();
            console.log("form_type ..."+form_type );

            // var form_values = new FormData(form);

            if(form_type == 'add'){
                addTMUnavail(form_values);
            }else{
                editTMUnavail(form_values);
            }
            
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_TM_Unavail_form"] p.error').remove();
        }
    });
    
    $("#CP_unavailable_start_time").change(function() {

        // $("#order_assign_qty_success").html("");
        // $('#order_assign_qty_error').html("");

        var start_time = $('option:selected', this).val();

        if(start_time.length > 0){
            $("#CP_unavailable_end_time").removeAttr("disabled");

            var selected_time_key = (time_key_array[start_time] + 1);
            // var assigned_quantity_count = assigned_quantity + selected_time_key;
            var html = '<option disabled="disabled" selected="selected">End Time</option>';
            for(var key=selected_time_key;key<time_array.length;key++){

                if(time_array[key] != undefined){
                    html += '<option value="'+time_array[key]+'">'+time_array[key]+'</option>';
                }

            };

            $("#CP_unavailable_end_time").html(html);
        }

        
    });
    
    $("form[name='add_CP_Unavail_form']").validate({
        rules: {
            unavailable_at: {
                required: true
            },
            start_time:{
                required: true
            },
            end_time:{
                required: true
            }
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjusCPents as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_CP_Unavail_form"] p.error').remove();

            var form_type = $("#CP_unavail_type").val();
            var form_values = $("form[name='add_CP_Unavail_form']").serialize();
            // console.log("form_type ..."+form_type );

            // var form_values = new FormData(form);

            if(form_type == "add"){
                addCPUnavail(form_values);
            }else{
                editCPUnavail(form_values);
            }

            
            
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_CP_Unavail_form"] p.error').remove();
        }
    });


    $("#TM_assign_delivered_quantity").keyup(function(){
        
        assignQTY();

        // assignDeliveryDate();

    });


    $("#add_TM_driver_1").on("change", function(){

        // alert( $('option:selected', this).text() );
        var selected_val = $('option:selected', this).val();
        var add_TM_dirver_1_data = $("#add_TM_dirver_1_data").val();
        var plant_id = $('option:selected', this).attr('data-plant-id');
        add_TM_dirver_1_data = JSON.parse(add_TM_dirver_1_data);
        var html = '<option disabled="disabled" selected="selected">Select Driver</option>';
        $.each(add_TM_dirver_1_data, function(key,val){
            // console.log(val["_id"] +" "+ selected_val);
            if(val["_id"] != selected_val){
                if(val["addressDetails"]["_id"] == plant_id){
                    console.log(val["_id"]);
                    html += '<option value="'+val["_id"]+'">'+val["driver_name"]+' - '+val["addressDetails"]["business_name"]+'</option>';
                }
            }
        });

        $("#add_TM_driver_2").html(html);

    });
    
    $("#TM_address_id").on("change", function(){

        // alert( $('option:selected', this).text() );
        var selected_val = $('option:selected', this).val();
        var add_TM_dirver_1_data = $("#add_TM_dirver_1_data").val();
        // var plant_id = $('option:selected', this).attr('data-plant-id');
        add_TM_dirver_1_data = JSON.parse(add_TM_dirver_1_data);
        var html = '<option disabled="disabled" selected="selected">Select Driver</option>';
        $.each(add_TM_dirver_1_data, function(key,val){
            // console.log(val["_id"] +" "+ selected_val);
            if(val["addressDetails"]["_id"] == selected_val){
                console.log(val["_id"]);
                html += '<option value="'+val["_id"]+'" data-plant-id="'+val["addressDetails"]["_id"]+'">'+val["driver_name"]+' - '+val["addressDetails"]["business_name"]+'</option>';
                
            }
        });

        $("#add_TM_driver_1").html(html);
        var html = '<option disabled="disabled" selected="selected">Select Driver</option>';
        $("#add_TM_driver_2").html(html);

    });
    
    // $("#add_TM_driver_2").on("change", function(){
    //     var selected_val = $('option:selected', this).val();
    //     var add_TM_dirver_2_data = $("#add_TM_dirver_2_data").val();
    //     add_TM_dirver_2_data = JSON.parse(add_TM_dirver_2_data);
    //     var html = '<option disabled="disabled" selected="selected">Select Driver</option>';
    //     $.each(add_TM_dirver_2_data, function(key,val){
    //         if(val["_id"] != selected_val){
    //             html += '<option value="'+val["_id"]+'">'+val["driver_name"]+' - '+val["addressDetails"]["business_name"]+'</option>';
    //         }
    //     });

    //     $("#add_TM_driver_1").html(html);

    // });

    $('#CP_is_already_delivered').change(function() {
        $('form[name="cp_assigned_form"] p.error').remove();
        if(this.checked) {
            vendor_order_id = $(this).attr("data-order-id");
            order_item_id = $(this).attr("data-order-item-id");
            data_plant_address = $(this).attr("data-plant-address");
            console.log("CP delivered Check..."+vendor_order_id+"...."+order_item_id+"..."+data_plant_address);
            getDeliveredCP(vendor_order_id,order_item_id,data_plant_address);
        }else{
            console.log("CP delivered Uncheck");
            // $("#CP_dropdown_assign").html($("#CP_dropdown_assign_hidden").html());
            $("#CP_dropdown_assign").html($("#CP_dropdown_assign_hidden").html());
            var ddl = document.getElementById('CP_dropdown_assign');
            for (i = 0; i < ddl.length; i++) {
                if(i != 0){
                    if(data_plant_address != ddl.options[i].getAttribute("data-address")){
                        // console.log("Detail::"+ddl.options[i].getAttribute("data-address"));
                        ddl.options[i] = null;
                    }
                }
                // ddl .options[i].value;
            }
        }
    });

});

function getDeliveredCP(vendor_order_id,order_item_id,plant_address_id){

        
    var request_data = {
    }

    customResponseHandler(
        "supplier/order_track/get_CP_delivered/"+vendor_order_id+"/"+order_item_id, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // return $data;
                $("#CP_dropdown_assign").html($("#CP_dropdown_assign_hidden").html());
                var ddl = document.getElementById('CP_dropdown_assign');
                for (i = 0; i < ddl.length; i++) {
                    if(i != 0){
                        if(plant_address_id != ddl.options[i].getAttribute("data-address")){
                            console.log("Detail::"+ddl.options[i].getAttribute("data-address"));
                            ddl.options[i] = null;
                        }
                        
                    }
                    // ddl .options[i].value;
                }
                
                for (i = 0; i < ddl.length; i++) {
                    if(i != 0){
                        $.each(data['data'],function(key,value){

                            if(value['CP_id'] != ddl.options[i].getAttribute("value")){

                                ddl.options[i] = null;

                            }

                            if(data['data'].length == 1){
                                // ddl.options[0] = null;
                                if(ddl.options[i] != undefined){
                                    ddl.options[i].setAttribute("selected","selected");
                                    // ddl.options[i].setAttribute("readOnly","readOnly");
                                    $("#CP_dropdown_assign").attr("readOnly","readOnly");
                                }
                                
                            }

                        });
                    }
                    // ddl .options[i].value;
                }

                // showToast("Quantity updated","Success");
                // var html = '<option disabled="disabled" selected="selected">Select Admixture Code</option>';
                // $.each(data["data"],function(key,value){

                //     // console.log(value);

                //     html += '<option value="'+value["_id"]+'">'+value["category_name"]+' - '+value["admixture_type"]+'</option>' 

                // });

                // $("#"+add_id).html(html);

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                // showToast("Something went wrong. Please try again","Error");  
                // $('form[name="cp_assigned_form"]').prepend('<p class="error">'+data["error"]["message"]+'</p>');
                showErrorMsgInForm("cp_assigned_form",data["error"]["message"].replace("_", " "));
            }
            

        }
        
    );


}

function getSettingAdmixtureTypes(admix_brand_id,add_id){

        
    var request_data = {
        "admix_brand_id":admix_brand_id
    }

    customResponseHandler(
        "supplier/get_admixture_types", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            // console.log(data);
            
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // return $data;

                // showToast("Quantity updated","Success");
                var html = '<option disabled="disabled" selected="selected">Select Admixture Code</option>';
                $.each(data["data"],function(key,value){

                    // console.log(value);

                    html += '<option value="'+value["_id"]+'">'+value["category_name"]+' - '+value["admixture_type"]+'</option>' 

                });

                $("#"+add_id).html(html);

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );


}


var is_assign_qty_error = 0;
var is_delivery_date_error = 0;

function assignQTY(){

    // var qty = $("#TM_assign_delivered_quantity").val();
    var qty = document.getElementById("TM_assign_delivered_quantity").value;
    var balance_qty = $("#order_assign_item_assign_qty").val();

    console.log("qty::"+qty);
    console.log("balance_qty::"+balance_qty);
    console.log("balance_qty::"+qty.length);

    if(qty.length > 0){
        // if(qty % 1 == 0){
            if(parseInt(balance_qty) < parseInt(qty)){
                console.log("qty::"+qty);
                $("#TM_assign_delivered_quantity_error").html("Deliver quntity should be greater than or equal to Pending Qty To Assign "+balance_qty);

                // $("#TM_assign_delivered_quantity_save").attr("disabled",true);

                is_assign_qty_error = 1;

            }else{
                $("#TM_assign_delivered_quantity_error").html("");
                // $("#TM_assign_delivered_quantity_save").removeAttr("disabled");

                is_assign_qty_error = 0;
            }
        // }else{
        //     $("#TM_assign_delivered_quantity_error").html("Deliver quntity should not be decimal number");
        // }
    }else{
        // $("#TM_assign_delivered_quantity_save").attr("disabled",true);
        $("#TM_assign_delivered_quantity_error").html("");
        is_assign_qty_error = 1;
    }

    if((is_assign_qty_error == 0)){
        $("#TM_assign_delivered_quantity_save").removeAttr("disabled");
    }else{
        $("#TM_assign_delivered_quantity_save").attr("disabled",true);
    }
    

}

function assignDeliveryDate(){

    var TM_assign_delivered_assign_date = $('#TM_assign_delivered_assign_date').val();
    var order_delivery_date = $('#order_delivery_date').val();

    var firstValue = TM_assign_delivered_assign_date.split('-');
    var secondValue = order_delivery_date.split('-');
console.log(firstValue);
    var date1 = firstValue[2]+'-'+firstValue[1]+'-'+firstValue[0];
    var date2 = secondValue[2]+'-'+secondValue[1]+'-'+secondValue[0];
    console.log(date1 +"....."+ date2);
    var d1 = new Date(''+date1);
    var d2 = new Date(''+date2);
    var same = d1.getTime() === d2.getTime();
    console.log("same::"+same);
    console.log(d1.getTime() === d2.getTime());
    console.log(d1.getTime() +".............."+ d2.getTime());
    if(!same){
        $("#TM_assign_delivered_assign_date_error").html("You can not choose date other than order delivery date "+order_delivery_date);

        // $("#TM_assign_delivered_quantity_save").attr("disabled",true);

        is_delivery_date_error = 1;
    }else{
        console.log("same::"+same);
        $("#TM_assign_delivered_assign_date_error").html(""); 
        // $("#TM_assign_delivered_quantity_save").removeAttr("disabled");

        is_delivery_date_error = 0;
        
    }

    console.log("Selected date: "+TM_assign_delivered_assign_date+"....."+order_delivery_date+"..."+same);

    if((is_delivery_date_error == 0) && (is_assign_qty_error == 0)){
        $("#TM_assign_delivered_quantity_save").removeAttr("disabled");
    }else{
        $("#TM_assign_delivered_quantity_save").attr("disabled",true);
    }

}



function updateSettings(form_values){

    responsehandler("supplier/settings/update", 
                    "POST", 
                    form_values, 
                    "#add-admixture-stock-details",
                    "Setting updated successfully",
                    0,
                    "setting_form"
                );

}

function AdmixupdateSettings(form_values){

    responsehandler("supplier/admix_settings/update", 
                    "POST", 
                    form_values, 
                    "#add-admixture-stock-details",
                    "Admixture Rate updated successfully",
                    0,
                    "admixture_rate_setting_form"
                );

}

function orderAceeptOrNot(order_status,otp){

    var is_accept = 'true';

    if(order_status == 'off'){
        is_accept = 'false';
    }

    var form_values = {
        "new_order_taken": is_accept
    };

    if(otp != ''){
        form_values["authentication_code"] = otp;
    }

    responsehandler("supplier/settings/order_accept_or_not", 
                    "GET", 
                    form_values, 
                    "#add-admixture-stock-details",
                    "Order status updated successfully",
                    1,
                    "setting_form"
                );

}





// Setting Module Over..................................................

// TM Unavailable Module Start..................................................

function viewTMunAvailability(TM_id,TM_sub_vendor_id){

        
    var request_data = {
        "TM_id":TM_id
    }

    customResponseHandler(
        "supplier/TM_unavailable", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // showToast("Quantity updated","Success");

                $("#TM_Unavail_body").html(data["html"]);
                
                $("#TM_unavail_id").val(TM_id);
                $("#TM_unavail_type").val("add");
                $("#TM_sub_vendor_id").val(TM_sub_vendor_id);

                $("#show-TM-Unavail-details").modal("show");

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );


}


function addTMUnavail(form_values){

    responsehandler("supplier/TM_unavailable/add", 
                    "POST", 
                    form_values, 
                    "#add-TM-Unavail-details",
                    "TM Un Availabiity Added successfully",
                    0,
                    "add_TM_Unavail_form"
                );

}

function showEditTMPopup(id,date, start_time, end_time){

    // data = JSON.parse(data);
    console.log(date);
    console.log(start_time);
    console.log(end_time);

    resetEditForm('add_TM_Unavail_form');

    $("#add_TM_Unavail_title").html("Edit TM UnAvailability");

    $(".TM_unavail_date").datepicker("setDate",''+date).trigger('change');

    $("#TM_unavailable_start_time").val(start_time).change();
    $("#TM_unavailable_end_time").val(end_time);
    $("#TM_unavail_id").val(id);
    $("#TM_unavail_type").val("edit");

    // $("#add_product_form_type").val('edit');

    $("#show-TM-Unavail-details").modal("hide");
    $("#add-TM-Unavail-details").modal("show");

}

function editTMUnavail(form_values){

    responsehandler("supplier/TM_unavailable/edit", 
                    "POST", 
                    form_values, 
                    "#add-TM-Unavail-details",
                    "TM Un Availabiity updated successfully",
                    0,
                    "add_TM_Unavail_form"
                );

}

function deleteTMUnavail(id){

    var request_data = {
        "_id":id
    }

    responsehandler("supplier/TM_unavailable/delete", 
                    "GET", 
                    request_data, 
                    "#add-TM-Unavail-details",
                    "TM Un Availabiity deleted successfully",
                    1,
                    "add_TM_Unavail_form"
                );

}


// TM Unavailable Module Over..................................................

// Concrete Pump Unavailable Module Start..................................................

function viewCPunAvailability(CP_id,CP_sub_vendor_id){

        
    var request_data = {
        "CP_id":CP_id
    }

    customResponseHandler(
        "supplier/CP_unavailable", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                // showToast("Quantity updated","Success");

                $("#CP_Unavail_body").html(data["html"]);

                $("#CP_unavail_id").val(CP_id);
                $("#CP_unavail_type").val("add");
                $("#CP_sub_vendor_id").val(CP_sub_vendor_id);

                $("#show-CP-Unavail-details").modal("show");

             }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );


}

function addCPUnavail(form_values){

    responsehandler("supplier/CP_unavailable/add", 
                    "POST", 
                    form_values, 
                    "#add-CP-Unavail-details",
                    "CP Un Availabiity Added successfully",
                    0,
                    "add_CP_Unavail_form"
                );

}

function showEditCPPopup(id,date, start_time, end_time){

    // data = JSON.parse(data);
    console.log(date);
    console.log(start_time);
    console.log(end_time);

    resetEditForm('add_CP_Unavail_form');

    $("#add_CP_Unavail_title").html("Edit CP UnAvailability");

    $(".CP_unavail_date").datepicker("setDate",''+date).trigger('change');

    $("#CP_unavailable_start_time").val(start_time).change();
    $("#CP_unavailable_end_time").val(end_time);
    $("#CP_unavail_id").val(id);
    $("#CP_unavail_type").val("edit");

    // $("#add_product_form_type").val('edit');

    $("#show-CP-Unavail-details").modal("hide");
    $("#add-CP-Unavail-details").modal("show");

}

function editCPUnavail(form_values){

    responsehandler("supplier/CP_unavailable/edit", 
                    "POST", 
                    form_values, 
                    "#add-CP-Unavail-details",
                    "CP Un Availabiity updated successfully",
                    0,
                    "add_CP_Unavail_form"
                );

}

function deleteCPUnavail(id){

    var request_data = {
        "_id":id
    }

    responsehandler("supplier/CP_unavailable/delete", 
                    "GET", 
                    request_data, 
                    "#add-CP-Unavail-details",
                    "CP Un Availabiity deleted successfully",
                    1,
                    "add_CP_Unavail_form"
                );

}


// Concrete Pump Unavailable Module Over..................................................

// Notification MODULE OVER...........................................

function notificationClick(){

    var request_data = {};

    $("#noti_list_div").html('<div class="overlay">\
    <span class="spinner"></span>\
</div>');

    customResponseHandler(
        "supplier/notification", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                // var message = data["data"]["message"];
                
                    
                
                    // $("#cart_item_count").html((parseInt(cart_item_count) - 1));

                    // $("#"+item_div_id).remove();                
                    
                    
                        // showToast("Product added successfully","Success");

                        // $("#noti_list_div").html(data["html"]);
                        $("#noti_count").remove();
                        $("#noti_details").html(data["html"]);
                        
              }else{
                showToast(""+data["error"]["message"],"Error");              
                
            }
            

        }
        
    );

   

}

function updateNotiStatus($id){

    var request_data = {};

    customResponseHandler(
        "supplier/notification/"+$id, // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            // if((data["status"] >= 200) && (data["status"] < 300)){
            //     // var message = data["data"]["message"];
            
                
            
            //     // $("#cart_item_count").html((parseInt(cart_item_count) - 1));

            //     // $("#"+item_div_id).remove();                
                
                
            //         // showToast("Product added successfully","Success");

            //         // $("#noti_list_div").html(data["html"]);
            //         $("#noti_details").html(data["html"]);
                    
            // }else{
            // showToast(""+data["error"]["message"],"Error");              
            
            // }
            

        }
        
    );

   

}

function openTMassignPopup(delivery_date,start_time,end_time,item_part_id,item_assign_qty,to_be_assign_qty,plant_address_id){

    resetForm('truck_assigned_form','assign-truck-popup');

    // $('.truck_assign_date').datepicker().val(delivery_date).trigger('change');
    // console.log(delivery_date);
    var date = new Date();
    var dd = date.getDate();
    var mm = date.getMonth()+1;
    var yy = date.getFullYear();

    $("#order_assign_item_assign_qty").val(to_be_assign_qty);
    $("#TM_assign_delivered_assign_date").val(delivery_date);
    $("#TM_assign_delivered_assign_start_time").val(start_time);
    $("#TM_assign_delivered_assign_end_time").val(end_time);
    $("#order_assign_item_part_id").val(item_part_id);
    // $("#order_assign_item_assign_qty").val(item_assign_qty);

    // $('.truck_assign_date').datepicker('setDate', ''+dd+'-'+mm+'-'+yy).trigger('change');

    // var ddl = document.getElementById('TM_dropdown_assign');
    // for (i = 0; i < ddl.length; i++) {
    //     if(i != 0){
    //         console.log("Detail::"+ddl.options[i].getAttribute("data-address")+"...."+ddl.length+"...."+i);
    //         if(plant_address_id != ddl.options[i].getAttribute("data-address")){
    //             // console.log("Detail::"+ddl.options[i].getAttribute("data-address"));
    //             ddl.options[i] = null;
    //         }
    //     }
    //     // ddl .options[i].value;
    // }

    $("#assign-truck-popup").modal('show');


}

function openCPassignPopup(delivery_date,start_time,end_time,item_part_id,plant_address_id){

    resetForm('cp_assigned_form','assign-CP-popup');

    // $('.truck_assign_date').datepicker().val(delivery_date).trigger('change');
    // console.log(delivery_date);
    var date = new Date(delivery_date);
    var start_dd = (date.getDate() - 3);
    // var end_dd = (date.getDate() - 3);
    var dd = date.getDate();
    var mm = date.getMonth()+1;
    var yy = date.getFullYear();

    // $('.cp_assign_date').datepicker('setStartDate', ''+start_dd+'-'+mm+'-'+yy).trigger('change');
    // $('.cp_assign_date').datepicker('setEndDate', ''+dd+'-'+mm+'-'+yy).trigger('change');
    // $('.cp_assign_date').datepicker('setDate', ''+dd+'-'+mm+'-'+yy).trigger('change');
    $("#CP_assign_delivered_assign_date").val(delivery_date);
    $("#CP_assign_delivered_assign_start_time").val(start_time);
    $("#CP_assign_delivered_assign_end_time").val(end_time);
    $("#cp_order_assign_item_part_id").val(item_part_id);
   
    var ddl = document.getElementById('CP_dropdown_assign');
    for (i = 0; i < ddl.length; i++) {
        if(i != 0){
            if(plant_address_id != ddl.options[i].getAttribute("data-address")){
                // console.log("Detail::"+ddl.options[i].getAttribute("data-address"));
                ddl.options[i] = null;
            }
        }
        // ddl .options[i].value;
    }

    $("#assign-CP-popup").modal('show');


}

// Notification MODULE OVER...........................................

function getDayCapacity(date){

        
    var request_data = {
        "date":date
    }

    customResponseHandler(
        "supplier/get_day_capacity", // Ajax URl
        'GET', // Method call
        request_data, // Request data
        function success(data){ // onSuccess
            console.log(data);
            

            // data = JSON.parse(data);
            if((data["status"] >= 200) && (data["status"] < 300)){
                var capacity = "% Capacity Avaialble To Take Order on selected date";
                if(data["data"] != undefined){
                    // var message = data["data"]["message"];
                    var day_capacity = data["data"]["day_capacity"];
                    var vendor_capacity = data["data"]["vendor_capacity"];
                    // showToast("Quantity updated","Success");

                    $("#day_capacity_value").html(day_capacity+' Cu.Mtr');
                    $("#total_day_capacity").html(vendor_capacity+' Cu.Mtr');

                    var total_qty = vendor_capacity;
                    var remain_qty = day_capacity;

                    var percentage = (remain_qty * 100) / total_qty;
                    percentage = 100 - percentage;
                    
                    // var percentage = 90;
                    /*progress bar script start*/
                    if(percentage <= 25){
                        
                        $('#day_capacity_progress').LineProgressbar({
                            percentage: percentage,
                            fillBackgroundColor: '#dd4b39',
                            unit: capacity
                        });


                    }else if((percentage > 25) && (percentage <= 50)){

                        $('#day_capacity_progress').LineProgressbar({
                            percentage: percentage,
                            fillBackgroundColor: '#00c0ef',
                            unit: capacity
                        });

                    }else if((percentage > 50) && (percentage <= 75)){

                        $('#day_capacity_progress').LineProgressbar({
                            percentage: percentage,
                            fillBackgroundColor: '#ff9600',
                            unit: capacity
                        });

                    }else if((percentage > 75) && (percentage <= 100)){

                        $('#day_capacity_progress').LineProgressbar({
                            percentage: percentage,
                            fillBackgroundColor: '#00a65a',
                            unit: capacity
                        });

                    }
                }else{
                    $('#day_capacity_progress').LineProgressbar({
                        percentage: 0,
                        fillBackgroundColor: '#00a65a',
                        unit: capacity
                    });
                }

            }else{
                // showToast(""+data["error"]["message"],"Error");              
                showToast("Something went wrong. Please try again","Error");  
                
            }
            

        }
        
    );
}

$(document).ready(function(){

    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];


    $('.day_from').datepicker({
	    autoclose: true,
	    format: 'd M yyyy'
		}).on('changeDate', function(selected){
	        startDate = new Date(selected.date.valueOf());
            console.log(startDate);
	        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));

            var dd = String(startDate.getDate()).padStart(2, '0');
            var mm = String(startDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = startDate.getFullYear();

            // startDate = dd + '/' + mm + '/' + yyyy;
            startDate = yyyy + '-' + mm + '-' + dd;

            getDayCapacity(startDate);
    });

    var current_date = new Date();
    
    var dd = String(current_date.getDate()).padStart(2, '0');
    var mm = String(current_date.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = current_date.getFullYear();

    // current_date = dd + '/' + mm + '/' + yyyy;
    current_date = yyyy + '-' + mm + '-' + dd;
    display_date = dd + ' ' + monthNames[mm-1] + ' ' + yyyy ;

    $('.day_from').datepicker().val(display_date).trigger('change');

    // getDayCapacity(current_date);

    // var daily_month;
    // var daily_year;
    // $("#day_capacity_filter").datepicker().on('changeDate', function(e){ 
    // // daily_month = new Date(e.date).getMonth() + 1;
    // // daily_month = all_month_array[daily_month-1];
    // // daily_year = String(e.date).split(" ")[3];
    // // console.log("daily_year..."+daily_year+"..."+daily_month);

    // //    var monthly_chart_status = $("#daily_chart_status_filter").val();

    // //    getDailyChartDetails(daily_year,daily_month, monthly_chart_status);
    // });

    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        var id_name = $(this).attr("id");
        // console.log("id_name......"+id_name);
        // console.log("fileName......"+fileName);
        // $("#"+id_name+"_text").val(fileName);
        $(this).parent().next( ".browse-input" ).val(fileName);
        $(this).trigger( "focusout" );

        // alert('The file "' + fileName +  '" has been selected.');
    });

    $('.admix_brand_checkbox').change(function() {
        var admix_id = $(this).val();
        if(this.checked) {
            console.log("ggg"+$(this).val());
            console.log("ggg"+$(this).attr("data-name"));
            
            var admix_name = $(this).attr("data-name");

            getAdmixtureTypesForAddForm(admix_id,admix_name)
        }else{
            $("#admix_brand_div_"+admix_id).remove();
        }
        // $('#textbox1').val(this.checked);        
    });
    
    $('.cement_brand_checkbox').change(function() {
        var id = $(this).val();
        if(this.checked) {
            console.log("ggg"+$(this).val());
            console.log("ggg"+$(this).attr("data-name"));
            
            $("#right_side_cement_brand_selection").val(id);

            $('form[name="cement_filter_form"]')[0].submit();
            // var admix_name = $(this).attr("data-name");

            // getAdmixtureTypesForAddForm(admix_id,admix_name)
        }else{
            // $("#admix_brand_div_"+admix_id).remove();
            $(".clear_filter_button").get(0).click();
        }
        // $('#textbox1').val(this.checked);        
    });
    
    $('.sand_source_checkbox').change(function() {
        var id = $(this).val();
        if(this.checked) {
            console.log("ggg"+$(this).val());
            console.log("ggg"+$(this).attr("data-name"));
            
            $("#right_side_sand_source_selection").val(id);

            $('form[name="sand_filter_form"]')[0].submit();
            // var admix_name = $(this).attr("data-name");

            // getAdmixtureTypesForAddForm(admix_id,admix_name)
        }else{
            // $("#admix_brand_div_"+admix_id).remove();
            $(".clear_filter_button").get(0).click();
        }
        // $('#textbox1').val(this.checked);        
    });
    
    $('.aggregate_source_checkbox').change(function() {
        var id = $(this).val();
        if(this.checked) {
            console.log("ggg"+$(this).val());
            console.log("ggg"+$(this).attr("data-name"));
            
            $("#right_side_aggregate_source_selection").val(id);

            $('form[name="aggregate_filter_form"]')[0].submit();
            // var admix_name = $(this).attr("data-name");

            // getAdmixtureTypesForAddForm(admix_id,admix_name)
        }else{
            // $("#admix_brand_div_"+admix_id).remove();
            $(".clear_filter_button").get(0).click();
        }
        // $('#textbox1').val(this.checked);        
    });
    
    $('.fly_ash_source_checkbox').change(function() {
        var id = $(this).val();
        if(this.checked) {
            console.log("ggg"+$(this).val());
            console.log("ggg"+$(this).attr("data-name"));
            
            $("#right_side_fly_ash_source_selection").val(id);

            $('form[name="fly_ash_filter_form"]')[0].submit();
            // var admix_name = $(this).attr("data-name");

            // getAdmixtureTypesForAddForm(admix_id,admix_name)
        }else{
            // $("#admix_brand_div_"+admix_id).remove();
            $(".clear_filter_button").get(0).click();
        }
        // $('#textbox1').val(this.checked);        
    });
    
    $('.admix_brand_checkbox').change(function() {
        var id = $(this).val();
        if(this.checked) {
            console.log("ggg"+$(this).val());
            console.log("ggg"+$(this).attr("data-name"));
            
            $("#right_side_admix_brand_selection").val(id);

            $('form[name="admix_brand_filter_form"]')[0].submit();
            // var admix_name = $(this).attr("data-name");

            // getAdmixtureTypesForAddForm(admix_id,admix_name)
        }else{
            // $("#admix_brand_div_"+admix_id).remove();
            $(".clear_filter_button").get(0).click();
        }
        // $('#textbox1').val(this.checked);        
    });
    
    $(document).on('change','.mix_cement_brnad',function() {
        
        var cement_brand_id = $('option:selected', this).attr('data-cement-brand-id');
        var cement_grade_id = $('option:selected', this).attr('data-cement-grade-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_cement_brnad::::...."+selected);
        
        $("#mix_rmc_cement_brand_id_"+rmc_id).val(cement_brand_id);
        $("#mix_rmc_cement_grade_id_"+rmc_id).val(cement_grade_id);

        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_sand_source',function() {
        
        var sand_source_id = $('option:selected', this).attr('data-sand-source-id');
        var sand_zone_id = $('option:selected', this).attr('data-sand-zone-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_rmc_sand_source_id_"+rmc_id).val(sand_source_id);
        $("#mix_rmc_sand_zone_id_"+rmc_id).val(sand_zone_id);
          
        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_aggregate_source_cat_1_2',function() {
        
        var final_agg_source_id = $('option:selected', this).attr('data-aggr-source-id');
        var final_aggregate1_sub_cat_id = $('option:selected', this).attr('data-sub-cat-1-id');
        var final_aggregate2_sub_cat_id = $('option:selected', this).attr('data-sub-cat-2-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_rmc_aggr_source_id_"+rmc_id).val(final_agg_source_id);
        $("#mix_rmc_aggr_cat_1_id_"+rmc_id).val(final_aggregate1_sub_cat_id);
        $("#mix_rmc_aggr_cat_2_id_"+rmc_id).val(final_aggregate2_sub_cat_id);
          
        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_admix_source',function() {
        
        var final_admix_brand_id = $('option:selected', this).attr('data-admix-brand-id');
        var final_admix_cat_id = $('option:selected', this).attr('data-admix-cat-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_rmc_admix_brand_id_"+rmc_id).val(final_admix_brand_id);
        $("#mix_rmc_admix_cat_id_"+rmc_id).val(final_admix_cat_id);
          
        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_flyash_source',function() {
        
        var final_fly_ash_source_id = $('option:selected', this).attr('data-flyash-source-id');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_rmc_fly_ash_id_"+rmc_id).val(final_fly_ash_source_id);
          
        getRMCCombination(rmc_id);
    });
    
    $(document).on('change','.mix_water_type',function() {
        
        var water_type = $('option:selected', this).attr('data-water-type');
        var rmc_id = $('option:selected', this).attr('data-rmc-id');

        // console.log("mix_sand_source::::...."+selected);

        $("#mix_water_type_"+rmc_id).val(water_type);
          
        getRMCCombination(rmc_id);
    });

    function getRMCCombination(rmc_id){

        var request_data = $("#rmc_combination_form_"+rmc_id).serialize();

        customResponseHandler(
            "supplier/design_mix/get_single_mix_combination", // Ajax URl
            'GET', // Method call
            request_data, // Request data
            function success(data){ // onSuccess
                console.log(data);
                

                // data = JSON.parse(data);
                if((data["status"] >= 200) && (data["status"] < 300)){
                    // var message = data["data"]["message"];
                    
                    // showToast("Quantity updated","Success");

                    // $("#design_mix_body").html(data["html"]);
                    $("#rmc_combination_div_"+rmc_id).html(data["html"]);

                    // $("#show-design-mix-details").modal("show");

                 }else{
                    // showToast(""+data["error"]["message"],"Error");              
                    showToast("Something went wrong. Please try again","Error");  
                    
                }
                

            }
            
        );


    }

    $("form[name='order_reassigned_form']").validate({

        rules:{
            order_reassign_desc:{
                required:true,
            },
            daterange:{
                required:true,
            }
            
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "pic_28_report" ) {
            error.insertAfter("#pic_28_report_div");
            }  else {
            error.insertAfter(element);
            }

        },
        submitHandler: function(form) {
            console.log("submited");
            // $('form[name="add_28_report_form"] p.error').remove();
            // var form_type = $("form[name='upload_order_bill_form'] #upload_order_bill_form_type").val();
            // var form_values = $("form[name='upload_order_bill_form']").serialize();
            // var form_values = new FormData(form);
            // console.log(form_values);
            // console.log("form_type ..."+form_type );

            // upload28CubeReport(form_values);
            
            if(reassignMaterialValdation()){
                console.log("validation OKKKKKKKKKK");

                var form_values = $("form[name='order_reassigned_form']").serialize();

                requestOTP(15,form_values,"reassign_order");
                // reassigningOrders(form_values,"123456");
            }else{
                console.log("validation NOTTTTTTTT");
            }

        },
        invalidHandler: function(event, validator) {
            console.log("error");

            // $('form[name="add_28_report_form"] p.error').remove();
        }

    });
    
    $("form[name='reject_order_reassigned_form']").validate({

        rules:{
            order_reassign_desc:{
                required:true,
            },
            new_reject_order_delivery_start_date:{
                required:true,
            },
            new_reject_order_delivery_time:{
                required:true,
            }
            
        },
        messages:{
            
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "pic_28_report" ) {
            error.insertAfter("#pic_28_report_div");
            }  else {
            error.insertAfter(element);
            }

        },
        submitHandler: function(form) {
            console.log("submited");
            // $('form[name="add_28_report_form"] p.error').remove();
            // var form_type = $("form[name='upload_order_bill_form'] #upload_order_bill_form_type").val();
            // var form_values = $("form[name='upload_order_bill_form']").serialize();
            // var form_values = new FormData(form);
            // console.log(form_values);
            // console.log("form_type ..."+form_type );

            // upload28CubeReport(form_values);
            
            // if(reassignMaterialValdation()){
                console.log("validation OKKKKKKKKKK");

                var form_values = $("form[name='reject_order_reassigned_form']").serialize();

                requestOTP(15,form_values,"reassign_order");
                // reassigningOrders(form_values,"123456");
            // }else{
            //     console.log("validation NOTTTTTTTT");
            // }

        },
        invalidHandler: function(event, validator) {
            console.log("error");

            // $('form[name="add_28_report_form"] p.error').remove();
        }

    });

    
    
});

function reassigningOrders(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;
    // console.log("form_values::..."+form_values);
    responsehandler("supplier/orders/reassign", 
                    "POST", 
                    form_values, 
                    "#reassign-order-popup",
                    "Reassign Order request sent to buyer",
                    1,
                    "order_reassigned_form"
                );

}

// Cement Stock Module Start..................................................

$(document).ready(function(){

    $("form[name='add_profit_margin_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            margin_percentage: {
                required: true
            },
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_profit_margin_form"] p.error').remove();

            var form_type = $("form[name='add_profit_margin_form'] #add_profit_margin_form_type").val();
            var form_values = $("form[name='add_profit_margin_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addProfitMargin(form_values);
            }else{
                // $("#add-cement-stock-details").modal("hide");
                requestOTPToMaster(3,form_values,"edit_profit_margin");
                
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_profit_margin_form"] p.error').remove();
        }
    });

});

function addProfitMargin(form_values){

    responsehandler("supplier/profit_margin/add", 
                    "POST", 
                    form_values, 
                    "#add-profit-margin-details",
                    "Profit Margin Added successfully",
                    0,
                    "add_profit_margin_form"
                );

}

function showProfitMarginEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_profit_margin_form');

    $("#add_profit_margin_title").html("Edit Profit Margin");

    $('#address_id').val(data['address']['_id']);
    $('#margin_percentage').val(data['margin_percentage']);
    

    $('#add_profit_margin_form_type').val("edit");
    $('#profit_margin_id').val(data['_id']);

    $('#add-profit-margin-details').modal('show');

}

function updateProfitMargin(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/profit_margin/update", 
                    "POST", 
                    form_values, 
                    "#add-profit-margin-details",
                    "Profit Margin updated successfully",
                    1,
                    "add_profit_margin_form"
                );

}

function deleteProfitMargin(rate_id,otp){

    var message = "Are you sure you want to delete this?"

    var request_data = {
        "profit_margin_id" : rate_id,
        "authentication_code" : otp
    }

    responsehandler('supplier/profit_margin/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Profit Margin deleted successfully",
            1,
            "add_profit_margin_form"
        );

    // confirmDialog(message, function(){
    //     // console.log(data);
    //     // console.log("deleted!.."+type);

        

    // });    

}



$(document).ready(function(){

    $("form[name='add_overhead_margin_form']").validate({
        rules: {
            address_id: {
                required: true
            },
            margin_percentage: {
                required: true
            },
            
        },
        messages: {
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            }  else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");

            $('form[name="add_overhead_margin_form"] p.error').remove();

            var form_type = $("form[name='add_overhead_margin_form'] #add_overhead_margin_form_type").val();
            var form_values = $("form[name='add_overhead_margin_form']").serialize();
            console.log("form_type ..."+form_type );


            // var form_values = new FormData(form);

            if(form_type === 'add'){
                addOverheadMargin(form_values);
            }else{
                // $("#add-cement-stock-details").modal("hide");
                requestOTPToMaster(5,form_values,"edit_overhead_margin");
                
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="add_overhead_margin_form"] p.error').remove();
        }
    });

});

function addOverheadMargin(form_values){

    responsehandler("supplier/overhead_margin/add", 
                    "POST", 
                    form_values, 
                    "#add-overhead-margin-details",
                    "Overhead Margin Added successfully",
                    0,
                    "add_overhead_margin_form"
                );

}

function showOverheadMarginEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_overhead_margin_form');

    $("#add_overhead_margin_title").html("Edit Overhead Margin");

    $('#address_id').val(data['address']['_id']);
    $('#margin_percentage').val(data['margin_percentage']);
    

    $('#add_overhead_margin_form_type').val("edit");
    $('#overhead_margin_id').val(data['_id']);

    $('#add-overhead-margin-details').modal('show');

}

function updateOverheadMargin(form_values,otp){

    form_values = form_values + "&authentication_code="+otp;

    responsehandler("supplier/overhead_margin/update", 
                    "POST", 
                    form_values, 
                    "#add-overhead-margin-details",
                    "Overhead Margin updated successfully",
                    1,
                    "add_overhead_margin_form"
                );

}

function deleteOverheadMargin(rate_id,otp){

    var message = "Are you sure you want to delete this?"

    var request_data = {
        "overhead_margin_id" : rate_id,
        "authentication_code" : otp
    }

    responsehandler('supplier/overhead_margin/delete', 
            "GET", 
            request_data,
            "#add-proposal",
            "Overhead Margin deleted successfully",
            1,
            "add_overhead_margin_form"
        );

    // confirmDialog(message, function(){
    //     // console.log(data);
    //     // console.log("deleted!.."+type);

        

    // });    

}
