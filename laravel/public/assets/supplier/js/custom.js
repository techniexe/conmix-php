(function($){
    'use strict';
    /*document.ready ALL FUNCTION START*/
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
    
    /*New sidebar dropdown menu script start*/
    $(".NW-sidebar-dropdown > a").click(function() {
      $(".NW-sidebar-submenu").slideUp(200);
      if (
        $(this)
          .parent()
          .hasClass("active")
      ) {
        $(".NW-sidebar-dropdown").removeClass("active");
        $(this)
          .parent()
          .removeClass("active");
      } else {
        $(".NW-sidebar-dropdown").removeClass("active");
        $(this)
          .next(".NW-sidebar-submenu")
          .slideDown(200);
        $(this)
          .parent()
          .addClass("active");
      }
    });
    /*New sidebar dropdown menu script end*/
    
    // user icon toggle jquery
    //    $(".user_icon").click(function () {
    //        $(".user_menu").slideToggle();
    //    });

        // Notification toggle jquery
    //    $(".notification").click(function () {
    //        $(".more_notification").slideToggle();
    //    });

        // Notification toggle jquery
    //    $(".cartdropdown").click(function () {
    //        $(".cartdropdown_detail").slideToggle();
    //    });
    
    /*show/hide popup function start*/
    /*0*/
    $(document).on('click','.userdropdownClick',function(e){
        e.preventDefault(); // stops link from making page jump to the top
        e.stopPropagation(); // when you click the button, it stops the page from seeing it as clicking the body too
        $('.user_menu').toggle();
        $('.more_notification').hide();
    });
    // $('.userdropdownClick').click( function(e) {
    //     e.preventDefault(); // stops link from making page jump to the top
    //     e.stopPropagation(); // when you click the button, it stops the page from seeing it as clicking the body too
    //     $('.user_menu').toggle();
    //     $('.more_notification').hide();
    // });
    $('.user_menu').click( function(e) {
        e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
        $('.more_notification').hide();
    });
    $('body').click( function() {
        $('.user_menu').hide();
    });
    /*1st*/
    $('.notificationClick').click( function(e) {
        e.preventDefault(); // stops link from making page jump to the top
        e.stopPropagation(); // when you click the button, it stops the page from seeing it as clicking the body too
        $('.more_notification').toggle();
        $('.user_menu').hide();
    });
    $('.more_notification').click( function(e) {
        e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
        $('.user_menu').hide();
    });
    $('body').click( function() {
        $('.more_notification').hide();
    });
    /*2nd*/
    $('.cartdropdownClick').click( function(e) {
        e.preventDefault(); // stops link from making page jump to the top
        e.stopPropagation(); // when you click the button, it stops the page from seeing it as clicking the body too
        $('.cartdropdown_detail').toggle();
    });
    $('.cartdropdown_detail').click( function(e) {
        e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
    });
    $('body').click( function() {
        $('.cartdropdown_detail').hide();
    });
    /*3rd*/
    $('.rejectbillClick').click( function(e) {
        e.preventDefault(); // stops link from making page jump to the top
        e.stopPropagation(); // when you click the button, it stops the page from seeing it as clicking the body too
        $('.rejectbillContent').toggle();
    });
    $('.rejectbillContent').click( function(e) {
        e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
    });
    $('body').click( function() {
        $('.rejectbillContent').hide();
    });
    /*show/hide popup function end*/
    
    /*custom scroll bar script start*/
    $(window).on("load",function(){
//        $(".cstm-scroll").mCustomScrollbar({
//            autoHideScrollbar:true,
//        });
        $("a[rel='load-content']").click(function(e){
            e.preventDefault();
            var url=$(this).attr("href");
            $.get(url,function(data){
                $(".content .mCSB_container").append(data); //load new content inside .mCSB_container
                //scroll-to appended content
                $(".content").mCustomScrollbar("scrollTo","h2:last");
            });
        });
        $(".content").delegate("a[href='top']","click",function(e){
            e.preventDefault();
            $(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
        });
    });
    /*custom scroll bar script end*/
    
    /*input browse file script*/
    $(document).on('change', '.button-browse :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

        input.trigger('fileselect', [numFiles, label, input]);
    });

    $('.button-browse :file').on('fileselect', function(event, numFiles, label, input) {
      var val = numFiles > 1 ? numFiles + ' files selected' : label;

      input.parent('.button-browse').next(':text').val(val);
    });
    /*input browse file script end*/
    $(function () {
    /*filter sidebar script start*/
    $("#filterSidebar").mCustomScrollbar({
        theme: "minimal"
    });
    
//  $("#sidebar1").on('click', function (){
//
//  });
    
    $('#filterdismiss, .filteroverlay').on('click', function () {
        $('#filterSidebar').removeClass('active');
        $('.filteroverlay').removeClass('active');
    });

    $('#filterCollapse').on('click', function () {
        $('#filterSidebar').addClass('active');
        $('.filteroverlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    /*filter sidebar script end*/
});
    /*Add input text script start*/
    $(function () {
        $("#AddMorebtn").bind("click", function () {
            var div = $('<div class = "col-md-12 et-dynamic-input-block"> <div class="row"> <div class="col-md-6"> <input class = "form-control" placeholder = "Name" name = "DynamicTextBox" type="text" value = "" /></div><div class="col-md-6"><input class = "form-control" placeholder = "Value" name = "DynamicTextBox" type="text" value = "" /></div></div><span class="inputremove"><i class="fa fa-close"></i></span></div>');
            $("#et-dynamic-textboxcontainer").append(div);
        });
        $("#btnGet").bind("click", function () {
            var values = "";
            $("input[name=DynamicTextBox]").each(function () {
                values += $(this).val() + "\n";
            });
            alert(values);
        });
        $("body").on("click", ".inputremove", function () {
            $(this).closest("div").remove();
        });
    });
    /*2nd*/
    $(function () {
        $("#AddMorebtn1").bind("click", function () {
            var div = $('<div class = "col-md-12 et-dynamic-input-block"> <div class="row"> <div class="col-md-6"> <input class = "form-control" placeholder = "Name" name = "DynamicTextBox" type="text" value = "" /></div><div class="col-md-6"><input class = "form-control" placeholder = "Value" name = "DynamicTextBox" type="text" value = "" /></div></div><span class="inputremove"><i class="fa fa-close"></i></span></div>');
            $("#et-dynamic-textboxcontainer1").append(div);
        });
        $("#btnGet").bind("click", function () {
            var values = "";
            $("input[name=DynamicTextBox]").each(function () {
                values += $(this).val() + "\n";
            });
            alert(values);
        });
        $("body").on("click", ".inputremove", function () {
            $(this).closest("div").remove();
        });
    });
    /*Add input text script end*/

    
    
})(window.jQuery);

/*****************************************************************************************************************************/

/*message more less function script file start*/
!function(s){s.fn.moreLines=function(e){"use strict";return this.each(function(){var t=s(this),i=(t.find("p"),parseFloat(t.css("line-height"))),a=t.innerHeight(),n=s.extend({linecount:1,baseclass:"b-morelines_",basejsclass:"js-morelines_",classspecific:"section",buttontxtmore:"more lines",buttontxtless:"less lines",animationspeed:1},e),c=n.baseclass+n.classspecific+"_ellipsis",l=n.baseclass+n.classspecific+"_button",o=n.baseclass+n.classspecific+"_wrapper",h=n.basejsclass+n.classspecific+"_wrapper",r=s("<div>").addClass(o+" "+h).css({"max-width":t.css("width")}),m=i*n.linecount;if(t.wrap(r),t.parent().not(h)&&a>m){t.addClass(c).css({"min-height":m,"max-height":m,overflow:"hidden"});var p=s("<div>",{class:l,click:function(){t.toggleClass(c),s(this).toggleClass(l+"_active"),"none"!==t.css("max-height")?t.css({height:m,"max-height":""}).animate({height:a},n.animationspeed,function(){p.html(n.buttontxtless)}):t.animate({height:m},n.animationspeed,function(){p.html(n.buttontxtmore),t.css("max-height",m)})},html:n.buttontxtmore});t.after(p)}}),this}}(jQuery);
/*message more less function script file end*/

/*message more less function script start*/
$(function() {
  $('.msg_readmore').moreLines({
    linecount: 6,
    baseclass: 'msg-class-description',
    basejsclass: 'msg-js-description',
    classspecific: '_readmore',
    buttontxtmore: "Read More",
    buttontxtless: "Read Less",
    animationspeed: 250
  });
});
/*message more less function script end*/

/*===========================================================================================================================*/

/*Input multi select tags/label script file start*/
$.fn.tagInput=function(e){return this.each(function(){var a=$.extend({},{labelClass:"label label-success"},e),l=$(this),t=$(this).children("input[type=hidden]"),n=$(this).children("input[type=text]");f();var r=t.val().split(",");if(""!=t.val())for(i=0;i<r.length;i++)d(r[i]);function c(){if(str=n.val(),/\S/.test(str)){str=str.replace(",",""),str=str.trim(),n.val(""),d(str);var e=n.next();e.val(e.val()+","+str),f()}}function b(e){label=e>0?l.children("span.tagLabel[data-badge="+e+"]"):l.children("span.tagLabel").last(),t.val(t.val().replace(label.text().slice(0,-2),"")),f(),label.remove()}function d(e){if(l.children("span.tagLabel").length>0){badge=n.prev();var t=badge.data("badge")+1;label=$('<span class="'+a.labelClass+' tagLabel" data-badge="'+t+'">'+e+' <a href="#" data-badge="'+t+'" aria-label="close" class="closelabel">&times;</a></span> ').insertAfter(badge)}else label=$('<span class="'+a.labelClass+' tagLabel" data-badge="1">'+e+' <a href="#" data-badge="1" aria-label="close" class="closelabel">&times;</a></span> ').insertBefore(n);label.children(".closelabel").click(function(){b($(this).data("badge"))})}function f(){s=t.val(),s=s.replace(/^( *, *)+|(, *(?=,|$))+/g,""),t.val(s)}n.keydown(function(e){var a=$(this).val();if(8==e.keyCode)0==a.length&&b(-1);else if(13==e.keyCode)return c(),e.preventDefault(),!1}),n.keyup(function(e){var a=$(this).val();if(27==e.keyCode)n.val(""),n.blur();else if(13==e.keyCode)return c(),e.preventDefault(),!1;a.indexOf(",")>=0&&c()}),n.change(function(){c()})})};
/*Input multi select tags/label script file end*/

/*Input multi select tags/label script end*/
$(document).ready(function(){
    $('#tags').tagInput({labelClass:"badge badge-success"});
    $('#tags1').tagInput({labelClass:"badge badge-success"});
});
/*Input multi select tags/label script end*/

/*Template Preview iframe script start*/
var theme_list_open=!1;$(document).ready(function(){IS_IPAD=null!=navigator.userAgent.match(/iPad/i),$(window).resize(function(){var e;e=$("#devicesSwitcher").height(),$("#viewDevicesiframe").attr("height",$(window).height()-e+"px")}).resize(),clicked="desktop";var e={desktop:"100%",tabletlandscape:1040,tabletportrait:788,mobilelandscape:500,mobileportrait:300,placebo:0};jQuery(".devicesMenu a").on("click",function(){var a=jQuery(this);jQuery(".iframe-content-block").removeClass("desktop-frame mobileportrait-frame tabletlandscape-frame tabletportrait-frame mobilelandscape-frame"),jQuery(this).hasClass("mobileportrait")?jQuery(".iframe-content-block").addClass("mobileportrait-frame"):jQuery(this).hasClass("tabletlandscape")?jQuery(".iframe-content-block").addClass("tabletlandscape-frame"):jQuery(this).hasClass("tabletportrait")?jQuery(".iframe-content-block").addClass("tabletportrait-frame"):jQuery(this).hasClass("mobilelandscape")?jQuery(".iframe-content-block").addClass("mobilelandscape-frame"):jQuery(this).hasClass("desktop")&&jQuery(".iframe-content-block").addClass("desktop-frame");for(device in e)console.log(device),console.log(e[device]),a.hasClass(device)&&(clicked=device,jQuery("#viewDevicesiframe").width(e[device]),clicked==device&&(jQuery(".devicesMenu a").removeClass("active"),a.addClass("active")));return!1}),IS_IPAD&&$("#viewDevicesiframe").css("padding-bottom","0")});
/*Template Preview iframe script end*/

/*Image light box script start*/
(function($){
	$(document).ready(function(){
		var galLink = $("a.gal_link");
		galLink.lightbox();
	});
})(jQuery);
/*Image light box script end*/