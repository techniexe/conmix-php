<?php 
/**
* File: cache.php
* Author: design1online.com, LLC
* Purpose: updates and loads the current css cache file
**/
// $path = "path_to_your_public_directory/";
$path = "";
// $dir = "name_of_your_css_folder_directory/";
$dir = ".";
$cachefilename = "cache.css";
$files_to_cache = array(
    "../../common/css/shimmer.css",
    "bootstrap.min.css",
    "font-awesome.min.css",
    "jquery.mCustomScrollbar.css",
    "style.css",
    "custom.css",
    "new-theme-style.css",
    "../../common/css/jquery.toast.css",
    "../../admin/css/select2-chosen.css",
    "../../admin/css/daterangepicker.css",
    "datepicker.min.css",
);
// echo $path . $dir;
// echo "test";exit;
//check for file changes
if ($handle = opendir($path . $dir)) {
    
 	while (false !== ($entry = readdir($handle))) 
        {
            
            // echo $entry;
            
 		//if any of the $files_to_cache have changed recently we need to update the cache file
                //to do this we compare the last modified times on the files to see if one of our
                //$files_to_cache was updated more recently than our cache file
 		if (is_file($entry) && is_readable($entry) && ($entry != "css_cache.php") && substr($entry, -3, 3) == "css" 
        // )
        && date(date(filemtime($entry)) > filemtime($cachefilename)))
        {
                    // echo "test";exit;
                        //we've found a $file_to_cache that was updated more recently than the cache file
                        //so open the cache file and prepare to overwrite the contents with the changes
			$cachefile = fopen($cachefilename, "w");

			//write all of the $files_to_cache to the cache file
			foreach ($files_to_cache as $filename)
			{
				$copyfile = fopen($filename, "r");
				fwrite($cachefile, fread($copyfile, filesize($filename)));
				fclose($copyfile);		
			}

                        //close the connection to the cache file - this also updates the last modified time
                        //on the cache file so that it won't try to re-generate the cache file again until 
                        //a $files_to_cache has been changed again
			fclose($cachefile);

                        //since we found one file that was changed and we've updated the cache file 
                        //we no longer need to look for other files that may have potentially been updated, so 
                        //this will tell the loop to stop running
			break;
		}
    }
}

//output the contents of the cache file
//the header content type will make sure the
//browser interprets the file contents as text/css
header("Content-type: text/css");

//clear the buffers to prevent overflows
ob_clean();
flush();

//open and return the contents of the cachefile
//you'll notice your cache file contains all of your
//$files_to_cache merged into one
readfile($cachefilename);

?>