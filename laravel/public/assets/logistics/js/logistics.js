// Sign up Module Start..................................................

$(document).ready(function(){

    $("form[name='logistics_register_form']").validate({
        rules: {
            full_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
                a_z_pattern: true
            },
            company_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
            },
            mobile_number: {
                required: true,
                mobile_validation: true,
                minlength: 13,
                maxlength: 15
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 20
            },
            pan_number: {
                required: true,
                maxlength: 10,
            },
            pancard_image: {
                required: true,
                extension: "jpg|png|jpeg|pdf"
            },
            company_certification_image : {
                required: true,
                extension: "jpg|png|jpeg|pdf"
            },
            account_type : {

            },
            company_certification_number:{
                required: true
            },
            gst_number: {
                required: true,
                maxlength: 100
            },company_type: {
                required: true
            },
            gst_certification_image: {
                required: true,
                extension: "jpg|png|jpeg|pdf"
            }
        },
        messages: {
            
            gst_certification_image:{
                extension: "Please enter valid (jpg, png, jpeg, pdf) format"
            },
            company_certification_image:{
                extension: "Please enter valid (jpg, png, jpeg, pdf) format"
            },
            pancard_image:{
                extension: "Please enter valid (jpg, png, jpeg, pdf) format"
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "pancard_image" ) {
              error.insertAfter("#pancard_image_div");
            } else if (element.attr("name") == "company_certification_image" ) {
                error.insertAfter("#company_certification_image_div");
              }else if (element.attr("name") == "gst_certification_image" ) {
                error.insertAfter("#gst_certification_image_div");
              } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $(".error").html("");
            $(".error").removeClass("alert-danger");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            // var form_values = $("form[name='vehicle_add_sub_cat_form']").serialize();
            // console.log("form_type ..."+form_type );

            $("form[name='logistics_register_form']")[0].submit();
            // addVehicleSubCategory(form_values);
            

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $(".error").html("");
            $(".error").removeClass("alert-danger");
        }
    });


    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    $("form[name='logistics_address_register_form']").validate({
        rules: {
            state_id: {
                required: true
            },
            city_id: {
                required: true
            },
            address_line_1: {
                required: true,
                noSpace: true
            },
            address_line_2: {
                required: true,
                noSpace: true
            },
            pincode: {
                required: true,
                number: true,
                noSpace: true,
                minlength: 6,
                maxlength: 6,               
                
            },
            us2_lat: {
                required: true
            },
            us2_lon: {
                required: true
            },
            
        },
        messages: {
            
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "state_id" ) {
              error.insertAfter("#state_name_city_div");
            } else if (element.attr("name") == "city_id" ) {
                error.insertAfter("#state_name_city_filter_div");
              } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $(".error").html("");
            $(".error").removeClass("alert-danger");
            // var form_type = $("form[name='vehicle_add_sub_cat_form'] #add_sub_category_form_type").val();
            // var form_values = $("form[name='vehicle_add_sub_cat_form']").serialize();
            // console.log("form_type ..."+form_type );

            $("form[name='logistics_address_register_form']")[0].submit();
            // addVehicleSubCategory(form_values);
            

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $(".error").html("");
            $(".error").removeClass("alert-danger");
        }
    });

    $('select[class="form-control-chosen"]').on('change', function() {
        // console.log("test...1..."+reset);
        // if(reset == 0){
            $(this).trigger('blur');
        // }
        
      });


});

// Sign up Module Over..................................................

// Quote Module Start..................................................

function updateQuote(request_data){
    var url = 'logistics/send_quote/update';

    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#add-contact-details",
                    "Quote updated successfully"
                );

}




// Quote Module Over..................................................

// Vehicle Module Start..................................................

$(document).ready(function(){
    $.validator.setDefaults({ ignore: ":hidden:not(select)" })
    var validobj = $("form[name='add_vehicles_form']").validate({
        rules: {
            vehicleCategoryId: {
                required: true
            },
            vehicleSubCategoryId: {
                required: true
            },
            vehicle_rc_number_1: {
                required: true,
                minlength: 2,
                maxlength: 2
            },
            vehicle_rc_number_2: {
                required: true,
                number:true
            },
            vehicle_rc_number_3: {
                required: true,
                minlength: 2,
                maxlength: 2
            },
            vehicle_rc_number_4: {
                required: true,
                number:true,
                minlength: 4,
                maxlength: 4
            },
            min_trip_price: {
                required: true,
                number: true,
                greaterThanEqual: 0,
                
            },
            manufacturer_name: {
                required: true
            },
            vehicle_model: {
                required: true
            }, 
            manufacture_year: {
                required: true,
                number: true,
                lessThanEqual: 9999,
                minlength: 4,
                maxlength: 4
            },
            
            delivery_range: {
                required: true,
                number: true,
                lessThanEqual: 700,
                greaterThanEqual: 50
            },
            // per_metric_ton_per_km_rate: {
            //     number: true,
            //     lessThanEqual: 9.99,
            //     greaterThanEqual: 1.00
            // },
            driver1_id: {
                required: true
            },
            driver2_id: {
                required: true
            },
            line1: {
                required: true
            },
            line2: {
                required: true
            },
            state_id: {
                required: true
            },
            city_id: {
                required: true
            },
            pincode: {
                required: true
            },
            rc_book_image: {
                required: function(){
                    return $("form[name='add_vehicles_form'] #add_vehicle_form_type").val() === 'add';
              },
              extension: "jpg|png|jpeg"
                
            },
            insurance_image : {
                required: function(){
                    return $("form[name='add_vehicles_form'] #add_vehicle_form_type").val() === 'add';
              },
              extension: "jpg|png|jpeg"
                
            },
            vehicle_image : {
                required: function(){
                    return $("form[name='add_vehicles_form'] #add_vehicle_form_type").val() === 'add';
              },
              extension: "jpg|png|jpeg"
            }
        },
        messages: {
            driver_mobile_number:{
                minlength: "Please enter at least 12 character"
            },
            rc_book_image: {
                extension: "Please enter valid file (jpg, png, jpeg) format"
            },
            insurance_image: {
                extension: "Please enter valid file (jpg, png, jpeg) format"
            },
            vehicle_image: {
                extension: "Please enter valid file (jpg, png, jpeg) format"
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "vehicleCategoryId" ) {
              error.insertAfter("#vehicleCategoryId_div");
            } else if (element.attr("name") == "vehicleSubCategoryId" ) {
              error.insertAfter("#vehicleSubCategoryId_div");
            } else if (element.attr("name") == "delivery_range" ) {
              error.insertAfter("#delivery_range_div");
            }else if (element.attr("name") == "rc_book_image" ) {
              error.insertAfter("#rc_book_image_div");
            }else if (element.attr("name") == "insurance_image" ) {
              error.insertAfter("#insurance_image_div");
            }else if (element.attr("name") == "vehicle_image" ) {
              error.insertAfter("#vehicle_image_div");
            }else if (element.attr("name") == "driver1_id" ) {
              error.insertAfter("#driver1_id_div");
            } else if (element.attr("name") == "driver2_id" ) {
              error.insertAfter("#driver2_id_div");
            }else if (element.attr("name") == "state_id" ) {
              error.insertAfter("#state_name_city_div");
            }else if (element.attr("name") == "city_id" ) {
              error.insertAfter("#state_name_city_filter_div");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // $(label).closest('.control-group').removeClass('error success').addClass('error');
            // console.log("tets....highlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // }else if (elem.attr("name") == "city_id") {
            //     $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // } else {
            //     elem.addClass(errorClass);
            // }
            elem.addClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // console.log("tets....unhighlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // } else {
            //     elem.removeClass(errorClass);
            // }
            elem.removeClass(errorClass);
            $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $(".error").html();
            $('form[name="add_vehicles_form"] p.error').remove();
            var form_type = $("form[name='add_vehicles_form'] #add_vehicle_form_type").val();
            // var form_values = $("form[name='vehicle_add_sub_cat_form']").serialize();
            // console.log("form_type ..."+form_type );

            // $("form[name='add_vehicles_form']").submit();
            var form_values = new FormData(form);

            if(form_type === 'add'){
                addVehicles(form_values);
            }else{
                editVehicles(form_values);
            }      
            

            
        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $(".error").html();
            $('form[name="add_vehicles_form"] p.error').remove();
        }
    });



    $(document).on('change', '.form-control-chosen', function () {
        if (!$.isEmptyObject(validobj.submitted)) {
            validobj.form();
        }
    });
    
   

    // $(document).on('change', '#state_name_city_filter', function () {
    //     if (!$.isEmptyObject(validobj.submitted)) {
    //         validobj.form();
    //     }
    // });


    $('#is_use_registered_address').change(function() {
        console.log("checked")
        if(this.checked) {

            var profile_data = $("#user_profile_data").val();

            addRegisteredAddressInVehicle(profile_data);

        }else{
            addRegisteredAddressInVehicle('{}');
        }
             
    });
    
    $('#vehicleSubCategoryId').on('change', function() {
        var optionSelected = $("option:selected", this);
        // console.log("checked"+optionSelected.attr('data-min-load'));
        

        var min_load = optionSelected.attr('data-min-load');
        var max_load = optionSelected.attr('data-max-load');
             
        $('#vehicle_min_load').val(min_load);
        $('#vehicle_max_load').val(max_load);

    });

});

function addRegisteredAddressInVehicle(data){

    
    data = JSON.parse(data);
    console.log(data);

    console.log("undefined..."+(data["line1"] != undefined ? data["line1"] : ''));
    
    $('#vehicle_address_line_1').val((data["line1"] != undefined ? data["line1"] : ''));
    $('#vehicle_address_line_2').val((data["line2"] != undefined ? data["line2"] : ''));
    $('#state_name_city').val((data["state_id"] != undefined ? data["state_id"] : ''));
    $('#state_name_city_filter').val((data["city_id"] != undefined ? data["city_id"] : ''));
    $('#vehicle_pincode').val((data["pincode"] != undefined ? data["pincode"] : ''));

    $("#state_name_city").trigger("chosen:updated");
    $("#state_name_city_filter").trigger("chosen:updated");

    var lat = (data["pickup_location"] != undefined ? data["pickup_location"]["coordinates"][1] : 0);
    var lon = (data["pickup_location"] != undefined ? data["pickup_location"]["coordinates"][0] : 0);

    $('#us2_address').val('');
    $('#us2_lat').val('');
    $('#us2_lon').val('');

    $('#us2').locationpicker("location", {latitude: ''+lat, longitude: ''+lon});
    $('#us2_address').val('');

}

function addVehicles(form_values){

    responsehandlerWithFiles("logistics/vehicles/add", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Vehicle Added successfully",
                    0,
                    "add_vehicles_form"
                );

}

function editVehicles(form_values){

    responsehandlerWithFiles("logistics/vehicles/edit", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Vehicle updated successfully",
                    0,
                    "add_vehicles_form"
                );

}

// Vehicle Module Over..................................................

// Bank Details Module Start..................................................

$(document).ready(function(){

    $("form[name='add_bank_detail_form']").validate({
        rules: {
            account_holder_name: {
                required: true
            },
            account_number: {
                required: true,
                number:true
            },
            ifsc: {
                required: true
            },
            account_type: {
                required: true
            },
            cancelled_cheque_image : {
                required: function(){
                   return  ($("form[name='add_bank_detail_form'] #add_bank_detail_form_type").val() == 'add')
                },
                extension: "jpg|png|jpeg"
            },
            
        },
        messages: {
            cancelled_cheque_image: {
                extension: "Please select valid file (jpg, png, jpeg) format"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "account_type" ) {
              error.insertAfter("#account_type_div");
            }else if (element.attr("name") == "cancelled_cheque_image" ) {
              error.insertAfter("#cancelled_cheque_image_div");
            } else {
              error.insertAfter(element);
            }
          },
          highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // $(label).closest('.control-group').removeClass('error success').addClass('error');
            // console.log("tets....highlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // }else if (elem.attr("name") == "city_id") {
            //     $("#" + elem.attr("id") + "_chosen a").addClass(errorClass);
            // } else {
            //     elem.addClass(errorClass);
            // }
            elem.addClass(errorClass);
            $("#" + elem.attr("name") + "_text").addClass(errorClass);
        },

        //When removing make the same adjustments as when adding
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            // console.log("tets....unhighlight");
            // if (elem.attr("name") == "state_id") {
            //     $("#" + elem.attr("id") + "_chosen a").removeClass(errorClass);
            // } else {
            //     elem.removeClass(errorClass);
            // }
            elem.removeClass(errorClass);
            $("#" + elem.attr("name") + "_text").removeClass(errorClass);
        },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_bank_detail_form"] p.error').remove();
            var form_type = $("form[name='add_bank_detail_form'] #add_bank_detail_form_type").val();
            // var form_values = $("form[name='add_bank_detail_form']").serialize();
            console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            if(form_type === 'add'){
                addBankDetails(form_values);
            }else{
                updateBankDetails(form_values);
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_bank_detail_form"] p.error').remove();
        }
    });

});

function addBankDetails(form_values){

    responsehandlerWithFiles("logistics/bank_details/add", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Bank Details Added successfully",
                    0,
                    "add_bank_detail_form"
                );

}

function showBankDetailEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_bank_detail_form');

    $("#add_bank_details_title").html("Edit Bank Details");

    $('#account_holder_name').val(data['account_holder_name']);
    $('#account_number').val(data['account_number']);
    $('#ifsc').val(data['ifsc']);
    $('#account_type').val(data['account_type']);
    

    $('#add_bank_detail_form_type').val("edit");
    $('#bank_detail_id').val(data['_id']);

    $('#add-bank-details').modal('show');

}

function updateBankDetails(form_values){

    responsehandlerWithFiles("logistics/bank_details/update", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Bank Details updated successfully",
                    0,
                    "add_bank_detail_form"
                );

}

function showSetDefaultDialog(data,csrf){

    data = JSON.parse(data);

    $('#make_default_bank_detail_id').val(data["_id"]);
    $('#make_default_bank_detail_csrf').val(csrf);

    $('#make-default-alert-popup').modal('show');

}

function setBankAccountDefault(){

    var bank_detail_id = $('#make_default_bank_detail_id').val();
    var csrf = $('#make_default_bank_detail_csrf').val();
    var request_data = {'bank_detail_id':bank_detail_id,'_token':csrf}; 
    responsehandler('logistics/bank_details/make_default', 
        "POST", 
        request_data, 
        "#make-default-alert-popup",
        "Account set default successfully"
    );

}

// Bank Details Module Over..................................................

// Driver Module Over..................................................


$(document).ready(function(){

    $("form[name='add_driver_form']").validate({
        rules: {
            driver_name: {
                required: true,
                minlength: 3,
                maxlength: 64,
                noSpace: true,
                a_z_pattern: true
            },
            driver_mobile_number: {
                required: true,
                mobile_validation: true,
                minlength: 13,
                maxlength: 15
            },
            driver_alt_mobile_number: {
            },
            driver_whatsapp_number: {
            },
            driver_pic : {
                // required: function(){
                //    return  ($("form[name='add_driver_form'] #add_driver_form_type").val() == 'add')
                // },
                extension: "jpg|png|jpeg"
            },
            
        },
        messages: {
            cancelled_cheque_image: {
                extension: "Please select valid file (jpg, png, jpeg) format"
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "account_type" ) {
              error.insertAfter("#account_type_div");
            }else if (element.attr("name") == "driver_pic" ) {
              error.insertAfter("#driver_pic_div");
            } else {
              error.insertAfter(element);
            }
          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="add_driver_form"] p.error').remove();
            var form_type = $("form[name='add_driver_form'] #add_driver_form_type").val();
            // var form_values = $("form[name='add_driver_form']").serialize();
            console.log("form_type ..."+form_type );


            var form_values = new FormData(form);

            if(form_type === 'add'){
                addDriverDetails(form_values);
            }else{
                updateDriverDetails(form_values);
            }
         },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="add_driver_form"] p.error').remove();
        }
    });

});


function addDriverDetails(form_values){

    responsehandlerWithFiles("logistics/driver_details/add", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Driver Added successfully",
                    0,
                    "add_driver_form"
                );

}

function showDriverDetailEditDialog(data){

    data = JSON.parse(data);
    console.log(data);

    resetEditForm('add_driver_form');

    $("#add_driver_form_title").html("Edit Driver Details");

    $('#driver_name').val(data['driver_name']);
    $('#driver_mobile_number').val(data['driver_mobile_number']);
    $('#driver_alt_mobile_number').val(data['driver_alt_mobile_number']);
    $('#driver_whatsapp_number').val(data['driver_whatsapp_number']);
    

    $('#add_driver_form_type').val("edit");
    $('#driver_id').val(data['_id']);

    $('#add-bank-details').modal('show');

}

function updateDriverDetails(form_values){

    responsehandlerWithFiles("logistics/driver_details/update", 
                    "POST", 
                    form_values, 
                    "#add-bank-details",
                    "Driver Details updated successfully",
                    0,
                    "add_driver_form"
                );

}

// Driver Module Over..................................................





// Profile MODULE Start...........................................

$(document).ready(function(){

    $("form[name='edit_myprofile_details_form']").validate({

        rules:{
            full_name: {
                required: true,
                noSpace: true,
                a_z_pattern: true,
                minlength: 3,
                maxlength: 64
            },
            mobile_number: {
                required:true,
                mobile_validation: true,
                minlength: 13,
                maxlength: 15
            },email: {
                required:true,
                email: true
            },
            landline_number: {
                number: true
            },
            password: {
                minlength : 6,
                maxlength: 20
            }, 
            pan_number: {
                required:true,
                minlength : 10
            }, 
            gst_number: {
                required: true,
                maxlength: 100
            },
            confirm_password:{
                minlength : 6,
                maxlength: 20,
                equalTo : "#new_password"
            },
            company_type: {
                required: true
            },
            gst_certification_image: {
                extension: "jpg|png|jpeg|pdf"
            },
            company_certification_image : {
                extension: "jpg|png|jpeg|pdf"
            },
            pancard_image: {
                extension: "jpg|png|jpeg|pdf"
            },
            company_certification_number:{
                required: true
            }
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "pancard_image" ) {
              error.insertAfter("#pancard_image_div");
            } else if (element.attr("name") == "company_certification_image" ) {
                error.insertAfter("#company_certification_image_div");
              }else if (element.attr("name") == "gst_certification_image" ) {
                error.insertAfter("#gst_certification_image_div");
              } else {
              error.insertAfter(element);
            }
          },
        messages:{
            mobile_number: {
                minlength: "Please add atleast 12 character"
            },
            gst_certification_image:{
                extension: "Please enter valid (jpg, png, jpeg, pdf) format"
            },
            company_certification_image:{
                extension: "Please enter valid (jpg, png, jpeg, pdf) format"
            },
            pancard_image:{
                extension: "Please enter valid (jpg, png, jpeg, pdf) format"
            },
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="edit_myprofile_details_form"] p.error').remove();
            // var form_values = $("form[name='edit_myprofile_details_form']").serialize();            
            
            var form_values = new FormData(form);
            console.log(form_values);
        
            editMyProfile(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="edit_myprofile_details_form"] p.error').remove();
        }

    });  
    

});

function editMyProfile(form_values){


    responsehandlerWithFiles("logistics/profile/edit", 
                    "POST", 
                    form_values, 
                    "#make-default-alert-popup",
                    "Profile updated successfully",
                    0,
                    "edit_myprofile_details_form"
                );

    // responsehandler('logistics/profile/edit', 
    //     "POST", 
    //     form_values,
    //     "#make-default-alert-popup",
    //     "Profile updated successfully",
    //     0,
    //     "edit_myprofile_details_form"
    // );

}



// Profile MODULE OVER...........................................

// latest Order MODULE start...........................................


$(document).ready(function(){

    $("form[name='truck_assigned_form']").validate({

        rules:{
            vehicle_id: {
                required: true
            },
            delivered_quantity: {
                required:true,
                number: true,
                lessThanEqual: $('#logistic_order_item_qty').val()
            },assigned_at: {
                required:true
            }
            
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "vehicle_id" ) {
              error.insertAfter(element.parent());
            } else {
              error.insertAfter(element);
            }
          },
        messages:{
            
        },
        submitHandler: function(form, event) {
            event.preventDefault();
            console.log("submited");           
            $('form[name="truck_assigned_form"] p.error').remove();
            var form_values = $("form[name='truck_assigned_form']").serialize();            
            
            // var form_values = new FormData(form);
            // console.log(form_values);
        
            // editMyProfile(form_values);

            truckAssign(form_values);

        },
        invalidHandler: function(event, validator) {
            console.log("error");
            $('form[name="truck_assigned_form"] p.error').remove();
        }

    });  
    

    $("form[name='upload_order_bill_form']").validate({

        rules:{
            bill_type:{
                required:true
            },
            bill_image:{
                required:true,
                extension: "jpg|jpeg|doc|docx|pdf" 
            }
            
        },
        messages:{
            bill_image : {
                extension: "Please enter valid file (jpg, jpeg, doc, docx, pdf) formats"
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "bill_image" ) {
              error.insertAfter("#bill_image_div");
            }  else {
              error.insertAfter(element);
            }

          },
        submitHandler: function(form) {
            console.log("submited");
            $('form[name="upload_order_bill_form"] p.error').remove();
            // var form_type = $("form[name='upload_order_bill_form'] #upload_order_bill_form_type").val();
            // var form_values = $("form[name='upload_order_bill_form']").serialize();
            var form_values = new FormData(form);
            console.log(form_values);
            // console.log("form_type ..."+form_type );

            uploadOrderBill(form_values);
            
        },
        invalidHandler: function(event, validator) {
            console.log("error");

            $('form[name="upload_order_bill_form"] p.error').remove();
        }

    });

    $('#order_status_filter').on('change', function() {
         var selected_status = this.value;

         window.location = BASE_URL+"/logistics/latest_trips?delivery_status="+selected_status;

    }); 
    
    $('#order_status_report_filter').on('change', function() {
         var selected_status = this.value;

         window.location = BASE_URL+"/logistics/report/orders?delivery_status="+selected_status;

    });    

    

    $('#per_metric_ton_per_km_rate').mask('0.00', { reverse: true });

});


function truckAssign(request_data){
    var url = 'logistics/latest_trips_truck_assign';

    responsehandler(url, 
                    "POST", 
                    request_data, 
                    "#assign-truck-popup",
                    "Truck assigned successfully",
                    0,
                    "truck_assigned_form"
                );

}

function uploadOrderBill(form_values){

    responsehandlerWithFiles("logistics/orders/bill_upload", 
                    "POST", 
                    form_values, 
                    "#create-ticket-popup",
                    "Bill uploaded successfully",
                    1,
                    "edit_business_details_form"
                );

}

function itemTrack(item_id,tracking_id){

    var form_values = {};

    customResponseHandler("logistics/order_track_detail/"+item_id+'/'+tracking_id,
                    "GET", 
                    form_values, 
                    function response(data){
                        console.log(data);
                        
                        if((data["status"] >= 200) && (data["status"] < 300)){
                            var message = data["html"];
                            // showToast(""+message,"Success");
                            // $("#sign-up-form-1").modal('hide');

                            // $("#resend_email_mobile").val(data["data"]["email_mobile"]);
                            // $("#verify_email_mobile").val(data["data"]["email_mobile"]);
                            $("#order_trackking_div").html(message);

                            $("#order-track").modal('show');
                        }else{
                            showToast(""+data["error"]["message"],"Error");
                            // $('#sign-up-form-1_error').html(data["error"]["message"]);
                        }

                    }
                );
    

}


// latest Order MODULE OVER...........................................