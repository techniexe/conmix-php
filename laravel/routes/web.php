<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('users', function () {
//     return 'Users!';
// });

Route::get('users', 'UserController@getIndex');

Route::get('admin','Admin\AdminController@index');
// Route::get('admin/login','Admin\AdminController@index');
Route::post('admin/login','Admin\AdminController@login');
Route::get('admin/login','Admin\AdminController@index')
        ->name("admin_login")
        ->middleware("prevent-back-history");

Route::get('admin/notification','Admin\ProfileController@getAdminNotifications')
        ->name('admin_noti');

Route::get('admin/notification/{id}','Admin\ProfileController@updateNotiStatus')
        ->name('admin_noti_status_update');
        
Route::get('admin/notifications','Admin\ProfileController@adminNotifications')
        ->name('admin_notifications')
        ->middleware("admin_auth");

Route::get('admin/forgot_pass_email_verifier','Admin\ForgotPassController@index')
        ->name('admin_forgot_pass_email_verifier');

Route::post('admin/forgot_pass_otp_send','Admin\ForgotPassController@forgotPassOTPSend')
        ->name('admin_forgot_pass_otp_send');

Route::get('admin/forgot_pass_otp_send','Admin\ForgotPassController@forgotPassOTPSend')
        ->name('admin_forgot_pass_otp_send');

Route::post('admin/forgot_pass_otp_verify','Admin\ForgotPassController@forgotPassOTPVerify')
        ->name('admin_forgot_pass_otp_verify')
        ->middleware("prevent-back-history");

Route::get('admin/forgot_pass_otp_verify','Admin\ForgotPassController@forgotPassOTPVerify')
        ->name('admin_forgot_pass_otp_verify')
        ->middleware("prevent-back-history");

Route::post('admin/forgot_pass','Admin\ForgotPassController@forgotPass')
        ->name('admin_forgot_pass')
        ->middleware("prevent-back-history");



Route::get('admin/dashboard','Admin\DashboardController@index')
        ->name('admin_dashboard')
        ->middleware("admin_auth");

Route::get('admin/dashboard_after_login','Admin\AdminController@afterLogin')
        ->name('admin_dashboard_after_login')
        ->middleware("admin_auth");

Route::get('logout','Admin\AdminController@logout');




Route::get('admin/getMonthlyChartData','Admin\DashboardController@getMonthlyChartDetails')
        ->name('admin_monthly_chart_data')
        ->middleware("admin_auth");

Route::get('admin/getDailyChartData','Admin\DashboardController@getDayChartDetails')
        ->name('admin_daily_chart_data')
        ->middleware("admin_auth");

Route::get('admin/admin_user','Admin\UsersController@index')
        ->name('admin_show_user')
        ->middleware("admin_auth");

Route::post('admin/admin_user/add','Admin\UsersController@addUser')
        ->middleware("admin_auth");

Route::post('admin_user/active_inactive','Admin\UsersController@activeInActiveUser')
        ->middleware("admin_auth");

Route::get('admin_user/request_otp','Admin\UsersController@requestOTP')
        ->middleware("admin_auth");

Route::get('admin_user/request_otp_for_edit_profile','Admin\UsersController@requestOTPForEditProfile')
        ->middleware("admin_auth");

Route::post('admin/admin_user/update','Admin\UsersController@editUserDetails')
        ->middleware("admin_auth");

Route::post('admin_user/delete','Admin\UsersController@deleteUser')
        ->middleware("admin_auth");

Route::get('admin/admin_user/filters','Admin\UsersController@adminUserApplyFilter')
        ->name('admin_show_user_filters')
        ->middleware("admin_auth");

Route::get('admin/logistics_user','Admin\UsersController@showLogistics')
        ->name('admin_show_logistics_user')
        ->middleware("admin_auth");

Route::get('admin/logistics_user/filters','Admin\UsersController@showLogistics')
        ->name('admin_show_logistics_user_filters')
        ->middleware("admin_auth");

Route::get('admin/suppliers_user','Admin\UsersController@showSuppliers')
        ->name('admin_show_suppliers_user')
        ->middleware("admin_auth");

Route::get('suppliers_user/filters','Admin\UsersController@showSuppliers')
        ->name('admin_show_suppliers_user_filters')
        ->middleware("admin_auth");

Route::post('admin/supplier_verify','Admin\UsersController@supplierVerifyByAdmin')
        ->name('admin_show_supplier_verify')
        ->middleware("admin_auth");

Route::post('admin/supplier_plant_verify','Admin\UsersController@supplierPlantVerifyByAdmin')
        ->name('admin_show_supplier_plant_verify')
        ->middleware("admin_auth");

Route::post('admin/supplier_block_unblock','Admin\UsersController@supplierBlockedByAdmin')
        ->name('admin_show_supplier_block_unblock')
        ->middleware("admin_auth");

Route::post('admin/supplier_plant_block_unblock','Admin\UsersController@supplierPlantBlockedByAdmin')
        ->name('admin_show_supplier_plant_block_unblock')
        ->middleware("admin_auth");

Route::post('admin/supplier_rejected','Admin\UsersController@supplierRejectedByAdmin')
        ->name('admin_show_supplier_rejected')
        ->middleware("admin_auth");

Route::post('admin/supplier_plant_rejected','Admin\UsersController@supplierplantRejectedByAdmin')
        ->name('admin_show_supplier_plant_rejected')
        ->middleware("admin_auth");

Route::post('admin/logistics_verify','Admin\UsersController@logisticsVerifyByAdmin')
        ->name('admin_show_logistics_verify')
        ->middleware("admin_auth");

Route::get('admin/buyers_user','Admin\UsersController@showBuyers')
        ->name('admin_show_buyers_user')
        ->middleware("admin_auth");

Route::get('admin/supplier_plants_user','Admin\UsersController@showSuppliersPlants')
        ->name('admin_show_plants_user')
        ->middleware("admin_auth");

Route::get('buyers_user/filters','Admin\UsersController@showBuyers')
        ->name('admin_show_buyers_user_filters')
        ->middleware("admin_auth");

// Vehicle Module
Route::get('admin/TM_category','Admin\VehicleController@index')
        ->name('admin_show_TM_category')
        ->middleware("admin_auth");

Route::post('TM_category/add','Admin\VehicleController@addTMCategory')
        ->middleware("admin_auth");

Route::post('TM_category/update','Admin\VehicleController@editTMCategory')
        ->middleware("admin_auth");

Route::get('admin/TM_sub_category','Admin\VehicleController@showTMSubCategoies')
        ->name('admin_show_TM_sub_category')
        ->middleware("admin_auth");

Route::post('TM_sub_category/add','Admin\VehicleController@addTMSubCategory')
        ->middleware("admin_auth");

Route::post('TM_sub_category/update','Admin\VehicleController@editTMSubCategory')
        ->middleware("admin_auth");


Route::get('admin/pump_category','Admin\VehicleController@showPumpCategory')
        ->name('admin_show_pump_category')
        ->middleware("admin_auth");

Route::post('pump_category/add','Admin\VehicleController@addPumpCategory')
        ->middleware("admin_auth");

Route::post('pump_category/update','Admin\VehicleController@editPumpCategory')
        ->middleware("admin_auth");



Route::get('admin/vehicle_list','Admin\VehicleController@showVehicles')
        ->name('admin_show_vehicle_list')
        ->middleware("admin_auth");

Route::get('admin/vehicle_pm_km_rate','Admin\VehicleController@showVehiclesPmKM')
        ->name('admin_show_vehicle_pm_km_rate')
        ->middleware("admin_auth");

Route::get('admin/vehicle_list/filters','Admin\VehicleController@showVehicles')
        ->name('admin_show_vehicle_list_filters')
        ->middleware("admin_auth");

Route::post('admin/vehicle_pm_km_rate_add','Admin\VehicleController@addVehiclePMKM')
        ->name('admin_show_vehicle_pm_km_rate_add')
        ->middleware("admin_auth");

Route::post('admin/vehicle_pm_km_rate_edit','Admin\VehicleController@editVehiclePMKM')
        ->name('admin_show_vehicle_pm_km_rate_edit')
        ->middleware("admin_auth");

Route::get('admin/get_vehicle_pm_km_rate/{state_id}','Admin\VehicleController@getPMKMRateByStateId')
        ->name('get_admin_show_vehicle_pm_km_rate')
        ->middleware("admin_auth");

//Region Module

Route::get('admin/region_country','Admin\RegionController@index')
        ->name('admin_show_region_country')
        ->middleware("admin_auth");

Route::post('region_country/add','Admin\RegionController@addCountry')
        ->middleware("admin_auth");

Route::get('admin/region_country/filters','Admin\RegionController@index')
        ->name('admin_show_region_country_filters')
        ->middleware("admin_auth");

Route::post('region_country/update','Admin\RegionController@editCountry')
        ->middleware("admin_auth");

Route::post('region_country/delete','Admin\RegionController@deleteCountry')
        ->middleware("admin_auth");

Route::get('admin/region_states','Admin\RegionController@showStates')
        ->name('admin_show_region_states')
        ->middleware("admin_auth");

Route::get('region_states/getCountries','Admin\RegionController@getCountries')
        ->middleware("admin_auth");

Route::get('admin/region_states/filters','Admin\RegionController@showStates')
        ->name('admin_show_region_states_filters')
        ->middleware("admin_auth");

Route::post('region_states/add','Admin\RegionController@addState')
        ->middleware("admin_auth");

Route::post('region_states/update','Admin\RegionController@editState')
        ->middleware("admin_auth");

Route::post('region_states/delete','Admin\RegionController@deleteState')
        ->middleware("admin_auth");

Route::get('admin/region_cities','Admin\RegionController@showCities')
        ->name('admin_show_region_cities')
        ->middleware("admin_auth");

Route::get('region_cities/getStates','Admin\RegionController@getStates')
        ->middleware("admin_auth");

Route::post('region_cities/add','Admin\RegionController@addCity')
        ->middleware("admin_auth");

Route::get('region_cities/filters','Admin\RegionController@showCities')
        ->middleware("admin_auth");

Route::post('region_cities/update','Admin\RegionController@editCity')
        ->middleware("admin_auth");

Route::post('region_cities/delete','Admin\RegionController@deleteCity')
        ->middleware("admin_auth");

Route::get('region_cities/getCities','Admin\RegionController@getCities')
        ->middleware("admin_auth");

//Product Module Start

// Grade Category Brand
// Grade Category Brand

Route::get('admin/concrete_grade','Admin\GradeCatBrandController@index')
        ->name('admin_show_concrete_grade')
        ->middleware("admin_auth");

Route::post('concrete_grade/add','Admin\GradeCatBrandController@addConcreteGrade')
        ->middleware("admin_auth");

Route::post('concrete_grade/update','Admin\GradeCatBrandController@editConcreteGrade')
        ->middleware("admin_auth");

Route::get('admin/cement_brand','Admin\GradeCatBrandController@getCementBrand')
        ->name('admin_show_cement_brand')
        ->middleware("admin_auth");

Route::post('cement_brand/add','Admin\GradeCatBrandController@addCementBrand')
        ->middleware("admin_auth");

Route::post('cement_brand/update','Admin\GradeCatBrandController@editCementBrand')
        ->middleware("admin_auth");

Route::get('admin/admixture_brand','Admin\GradeCatBrandController@getAddMixtureBrand')
        ->name('admin_show_admixture_brand')
        ->middleware("admin_auth");

Route::post('admixture_brand/add','Admin\GradeCatBrandController@addAddMixtureBrand')
        ->middleware("admin_auth");

Route::post('admixture_brand/update','Admin\GradeCatBrandController@editAddMixtureBrand')
        ->middleware("admin_auth");


Route::get('admin/admixture_types','Admin\GradeCatBrandController@getAddMixtureTypes')
        ->name('admin_show_admixture_types')
        ->middleware("admin_auth");

Route::post('admixture_types/add','Admin\GradeCatBrandController@addAddMixtureTypes')
        ->middleware("admin_auth");

Route::post('admixture_types/update','Admin\GradeCatBrandController@editAddMixtureTypes')
        ->middleware("admin_auth");


Route::get('admin/cement_grade','Admin\GradeCatBrandController@getCementGrade')
        ->name('admin_show_cement_grade')
        ->middleware("admin_auth");

Route::post('cement_grade/add','Admin\GradeCatBrandController@addCementGrade')
        ->middleware("admin_auth");

Route::post('cement_grade/update','Admin\GradeCatBrandController@editCementGrade')
        ->middleware("admin_auth");


Route::get('admin/payement_method','Admin\GradeCatBrandController@getPaymentMethod')
        ->name('admin_show_payement_method')
        ->middleware("admin_auth");

Route::post('payement_method/add','Admin\GradeCatBrandController@addPaymentMethod')
        ->middleware("admin_auth");

Route::post('payement_method/update','Admin\GradeCatBrandController@editPaymentMethod')
        ->middleware("admin_auth");



// Porduct Category

Route::get('admin/product_category','Admin\ProductController@index')
        ->name('admin_show_product_category')
        ->middleware("admin_auth");

Route::get('admin/product_category/filters','Admin\ProductController@index')
        ->name('admin_show_product_category_filters')
        ->middleware("admin_auth");


Route::post('product_category/add','Admin\ProductController@addCategory')
        ->middleware("admin_auth");

Route::post('product_category/update','Admin\ProductController@editCategory')
        ->middleware("admin_auth");

Route::get('admin/product_sub_category','Admin\ProductController@showSubCategories')
        ->name('admin_show_product_sub_category')
        ->middleware("admin_auth");

Route::get('admin/product_sub_category/filters','Admin\ProductController@showSubCategories')
        ->name('admin_show_product_sub_category_filters')
        ->middleware("admin_auth");

Route::get('product_sub_category/getCategory','Admin\ProductController@index')
        ->middleware("admin_auth");

Route::get('product_sub_category/getSubCategory','Admin\ProductController@index')
        ->middleware("admin_auth");

Route::post('product_sub_category/add','Admin\ProductController@addSubCategory')
        ->middleware("admin_auth");

Route::post('product_sub_category/update','Admin\ProductController@editSubCategory')
        ->middleware("admin_auth");


Route::get('admin/product_list','Admin\ProductController@showProducts')
        ->name('admin_show_product_list')
        ->middleware("admin_auth");

Route::post('product_list/update','Admin\ProductController@editProducts')
        ->middleware("admin_auth");

Route::post('product_list/verify_by_admin','Admin\ProductController@verifyProducts')
        ->middleware("admin_auth");

Route::get('admin/product_list/filters','Admin\ProductController@showProducts')
        ->name('admin_show_product_list_filters')
        ->middleware("admin_auth");

Route::get('admin/product_detail/{id}','Admin\ProductDetailController@index')
        ->name('admin_show_detail')
        ->middleware("admin_auth");

Route::get('admin/product/designmix_detail','Admin\ProductController@designmix_detail')
        ->name('admin_show_product_designmix_detail')
        ->middleware("admin_auth");





// GST Slab Module

Route::get('gst_slab/get','Admin\ProductController@getGstSalb')
        ->middleware("admin_auth");

Route::get('admin/gst_info','Admin\ProductController@showGstSalb')
        ->name('admin_show_gst_info')
        ->middleware("admin_auth");

Route::post('gst_info/add','Admin\ProductController@addGstSalb')
        ->middleware("admin_auth");

Route::post('gst_info/update','Admin\ProductController@editGstSalb')
        ->middleware("admin_auth");


// Margin Rate Module
Route::get('margin_rate/get','Admin\ProductController@getMarginRate')
        ->middleware("admin_auth");

Route::get('admin/margin_slab','Admin\ProductController@showMarginRate')
        ->name('admin_show_margin_slab')
        ->middleware("admin_auth");

Route::post('margin_slab/add','Admin\ProductController@addMarginSlab')
        ->middleware("admin_auth");

Route::post('margin_slab/update','Admin\ProductController@editMarginSlab')
        ->middleware("admin_auth");

Route::get('admin/aggregate_source','Admin\ProductController@showAggregateSource')
        ->name('admin_show_aggregate_source')
        ->middleware("admin_auth");

Route::post('admin/add_aggregate_source','Admin\ProductController@addAggregateSource')
        ->name('admin_add_aggregate_source')
        ->middleware("admin_auth");

Route::post('admin/edit_aggregate_source','Admin\ProductController@editAggregateSource')
        ->name('admin_edit_aggregate_source')
        ->middleware("admin_auth");

Route::post('admin/delete_aggregate_source','Admin\ProductController@deleteAggregateSource')
        ->name('admin_delete_aggregate_source')
        ->middleware("admin_auth");


Route::get('admin/sand_source','Admin\ProductController@showSandSource')
        ->name('admin_show_sand_source')
        ->middleware("admin_auth");

Route::post('admin/add_sand_source','Admin\ProductController@addSandSource')
        ->name('admin_add_sand_source')
        ->middleware("admin_auth");

Route::post('admin/edit_sand_source','Admin\ProductController@editSandSource')
        ->name('admin_edit_sand_source')
        ->middleware("admin_auth");

Route::post('admin/delete_sand_source','Admin\ProductController@deleteSandSource')
        ->name('admin_delete_sand_source')
        ->middleware("admin_auth");
        
        
Route::get('admin/sand_zone','Admin\ProductController@showSandZone')
        ->name('admin_show_sand_zone')
        ->middleware("admin_auth");

Route::post('admin/add_sand_zone','Admin\ProductController@addSandZone')
        ->name('admin_add_sand_zone')
        ->middleware("admin_auth");

Route::post('admin/edit_sand_zone','Admin\ProductController@editSandZone')
        ->name('admin_edit_sand_zone')
        ->middleware("admin_auth");

Route::post('admin/delete_sand_zone','Admin\ProductController@deleteSandZone')
        ->name('admin_delete_sand_zone')
        ->middleware("admin_auth");


Route::get('admin/flyash_source','Admin\ProductController@showFlyashSource')
        ->name('admin_show_flyash_source')
        ->middleware("admin_auth");

Route::post('admin/add_flyash_source','Admin\ProductController@addFlyashSource')
        ->name('admin_add_flyash_source')
        ->middleware("admin_auth");

Route::post('admin/edit_flyash_source','Admin\ProductController@editFlyashSource')
        ->name('admin_edit_flyash_source')
        ->middleware("admin_auth");

Route::post('admin/delete_flyash_source','Admin\ProductController@deleteFlyashSource')
        ->name('admin_delete_flyash_source')
        ->middleware("admin_auth");

//Product Module Over

//Email Template Module Start

Route::get('admin/email_template','Admin\EmailTemplateController@index')
        ->name('admin_show_email_template')
        ->middleware("admin_auth");

Route::post('email_template/add','Admin\EmailTemplateController@addEmailTemplate')
        ->middleware("admin_auth");

Route::post('email_template/getTemplate','Admin\EmailTemplateController@getEmailTemplate')
        ->middleware("admin_auth"); //Get single template details by template type

Route::post('email_template/update','Admin\EmailTemplateController@editEmailTemplate')
        ->middleware("admin_auth");

Route::get('email_template/iframe','Admin\EmailTemplateController@showEmailTemplateInIframe')
        ->middleware("admin_auth");

Route::post('email_template/delete_attach','Admin\EmailTemplateController@deleteEmailAttachment')
        ->middleware("admin_auth");

//Email Template Module Over

//Product Review Module Start

Route::get('admin/product_review','Admin\ReviewController@index')
        ->name('admin_show_product_review')
        ->middleware("admin_auth");
        
Route::get('admin/feedback','Admin\ReviewController@getFeedback')
        ->name('admin_show_feedback')
        ->middleware("admin_auth");

Route::post('product_review/update','Admin\ReviewController@editReview')
        ->middleware("admin_auth");

Route::get('admin/product_review/filters','Admin\ReviewController@index')
        ->middleware("admin_auth");

Route::get('product_review/getSubCat','Admin\ReviewController@getSubCategories')
        ->middleware("admin_auth");

Route::get('product_review/getProducts','Admin\ReviewController@getProducts')
        ->middleware("admin_auth");

Route::get('product_review/getUsers','Admin\ReviewController@getBuyersList')
        ->middleware("admin_auth");


//Product Review Module Over

//Access Log Module Start

Route::get('admin/email_logs','Admin\AccessLogController@index')
        ->name('admin_show_email_logs')
        ->middleware("admin_auth");

Route::get('admin/email_logs/filters','Admin\AccessLogController@index')
        ->name('admin_show_email_logs_filters')
        ->middleware("admin_auth");

Route::post('email_logs/email_resend','Admin\AccessLogController@emailResend')
        ->middleware("admin_auth");

Route::get('admin/action_logs','Admin\AccessLogController@showActionLogs')
        ->name('admin_show_action_logs')
        ->middleware("admin_auth");

Route::get('action_logs/filters','Admin\AccessLogController@showActionLogs')
        ->name('admin_show_action_logs_filters')
        ->middleware("admin_auth");

//Access Log Module Over

//Ledger Module Start

Route::get('admin/orders','Admin\OrderController@index')
        ->name('admin_show_orders')
        ->middleware("admin_auth");

Route::post('orders/status','Admin\OrderController@updateStatus')
        ->middleware("admin_auth");

Route::post('orders/item_status','Admin\OrderController@updateItemStatus')
        ->middleware("admin_auth");

Route::get('orders/filters','Admin\OrderController@index')
        ->name('admin_show_orders_filters')
        ->middleware("admin_auth");

Route::get('admin/orders/details/{id}','Admin\OrderController@orderDetails')
        ->name('admin_show_order_details')
        ->middleware("admin_auth");

Route::post('admin/orders/update_supplier_bill_status','Admin\OrderController@updateSupplierBillStatus')
        ->name('admin_show_update_supplier_bill_status')
        ->middleware("admin_auth");

Route::post('admin/orders/update_logistics_bill_status','Admin\OrderController@updateLogisticsBillStatus')
        ->name('admin_show_update_logistics_bill_status')
        ->middleware("admin_auth");


Route::get('admin/logisctic_order_detail/{id}','Admin\OrderController@showLogisticsOrderDetail')
        ->name('admin_show_logisctic_order_details')
        ->middleware("admin_auth");

Route::get('admin/supplier_order_detail/{id}','Admin\OrderController@showSupplierOrderDetail')
        ->name('admin_show_supplier_order_details')
        ->middleware("admin_auth");

Route::get('admin/supplier_orders','Admin\OrderController@showSuppliersOrder')
        ->name('admin_show_supplier_orders')
        ->middleware("admin_auth");

Route::get('admin/logistics_orders','Admin\OrderController@showLogisticsOrder')
        ->name('admin_show_logistics_orders')
        ->middleware("admin_auth");

Route::get('admin/buyers_cancel_orders','Admin\OrderController@buyersCancelOrders')
        ->name('admin_show_buyers_cancel_orders')
        ->middleware("admin_auth");

Route::get('admin/buyers_processing_orders','Admin\OrderController@buyersProcessingOrders')
        ->name('admin_show_buyers_processing_orders')
        ->middleware("admin_auth");

Route::get('admin/buyers_orders_truck_tracking/{id}','Admin\OrderController@trackOrderItem')
        ->name('admin_show_buyers_orders_truck_tracking')
        ->middleware("admin_auth");

Route::get('admin/buyers_orders_item_tracking/{item_id}/{tracking_id}','Admin\OrderController@trackingDetails')
        ->name('admin_show_buyers_orders_item_tracking')
        ->middleware("admin_auth");

Route::get('admin/buyers_orders_CP_tracking/{item_part_id}','Admin\OrderController@CPtrackingDetails')
        ->name('admin_show_buyers_orders_CP_tracking')
        ->middleware("admin_auth");

Route::get('admin/suppliers_orders_CP_tracking/{item_part_id}','Admin\OrderController@SupplierCPtrackingDetails')
        ->name('admin_show_suppliers_orders_CP_tracking')
        ->middleware("admin_auth");

Route::get('admin/suppliers_orders_item_tracking/{item_id}/{tracking_id}','Admin\OrderController@supplierTrackingDetails')
        ->name('admin_show_suppliers_orders_item_tracking')
        ->middleware("admin_auth");


Route::get('admin/logistics_orders_item_tracking/{item_id}/{tracking_id}','Admin\OrderController@logisticsTrackingDetails')
        ->name('logistics_orders_item_tracking')
        ->middleware("admin_auth");


Route::get('admin/orders/invoice/{id}','Admin\OrderController@viewInvoice')
        ->name('admin_show_order_invoice')
        ->middleware("admin_auth");
Route::get('admin/orders/track/{id}','Admin\OrderController@itemTrack')
        ->name('admin_show_order_track')
        ->middleware("admin_auth");

Route::get('admin/transaction','Admin\OrderController@transactions')
        ->name('admin_show_transaction')
        ->middleware("admin_auth");
Route::get('transaction/filters','Admin\OrderController@transactions')
        ->name('admin_show_transaction_filters')
        ->middleware("admin_auth");



//Ledger Module Over

//Report Module Start

Route::get('admin/reports/admix_brand_report','Admin\ReportContrller@admix_brand_report')
        ->name('admin_admix_brand_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/admix_brand_report_export','Admin\ReportContrller@AdmixBrandExport')
        ->name('admin_admix_brand_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/admix_types_report','Admin\ReportContrller@admix_types_report')
        ->name('admin_admix_types_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/admix_types_report_export','Admin\ReportContrller@AdmixTypesExport')
        ->name('admin_admix_types_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/agg_sand_cat_report','Admin\ReportContrller@agg_sand_cat_report')
        ->name('admin_agg_sand_cat_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/agg_sand_cat_report_export','Admin\ReportContrller@AggrSandExport')
        ->name('admin_agg_sand_cat_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/agg_sand_subcategory_report','Admin\ReportContrller@agg_sand_subcategory_report')
        ->name('admin_agg_sand_subcategory_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/agg_sand_subcategory_report_export','Admin\ReportContrller@AggrSandSubCatExport')
        ->name('admin_agg_sand_subcategory_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/agg_source_report','Admin\ReportContrller@agg_source_report')
        ->name('admin_agg_source_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/agg_source_report_export','Admin\ReportContrller@AggrSourceExport')
        ->name('admin_agg_source_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/cement_brand_report','Admin\ReportContrller@cement_brand_report')
        ->name('admin_cement_brand_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/cement_brand_report_export','Admin\ReportContrller@CementbrandExport')
        ->name('admin_cement_brand_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/cement_grade_report','Admin\ReportContrller@cement_grade_report')
        ->name('admin_cement_grade_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/cement_grade_report_export','Admin\ReportContrller@CementGradeExport')
        ->name('admin_cement_grade_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/concrete_grade_report','Admin\ReportContrller@concrete_grade_report')
        ->name('admin_concrete_grade_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/concrete_grade_report_export','Admin\ReportContrller@ConcreteGradeExport')
        ->name('admin_concrete_grade_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/concrete_pump_category_report','Admin\ReportContrller@concrete_pump_category_report')
        ->name('admin_concrete_pump_category_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/concrete_pump_category_report_export','Admin\ReportContrller@ConcretePumpCatExport')
        ->name('admin_concrete_pump_category_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/coupon_report','Admin\ReportContrller@coupon_report')
        ->name('admin_coupon_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/coupon_report_export','Admin\ReportContrller@CouponExport')
        ->name('admin_coupon_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/support_ticket_report','Admin\ReportContrller@support_ticket_report')
        ->name('admin_support_ticket_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/support_ticket_report_export','Admin\ReportContrller@SupportTicketExport')
        ->name('admin_support_ticket_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/flyAsh_source_report','Admin\ReportContrller@flyAsh_source_report')
        ->name('admin_flyAsh_source_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/flyAsh_source_report_export','Admin\ReportContrller@FlyAshExport')
        ->name('admin_flyAsh_source_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/gst_slab_report','Admin\ReportContrller@gst_slab_report')
        ->name('admin_gst_slab_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/gst_slab_report_export','Admin\ReportContrller@GstExport')
        ->name('admin_gst_slab_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/margin_rate_report','Admin\ReportContrller@margin_rate_report')
        ->name('admin_margin_rate_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/margin_rate_report_export','Admin\ReportContrller@MarginRateExport')
        ->name('admin_margin_rate_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/sand_source_report','Admin\ReportContrller@sand_source_report')
        ->name('admin_sand_source_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/sand_source_report_export','Admin\ReportContrller@SandSourceExport')
        ->name('admin_sand_source_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/buyer_listing','Admin\ReportContrller@buyer_listing_report')
        ->name('admin_report_buyer_listing')
        ->middleware("admin_auth");

Route::get('admin/reports/supplier_listing','Admin\ReportContrller@supplier_listing_report')
        ->name('admin_report_supplier_listing')
        ->middleware("admin_auth");

Route::get('admin/reports/logistics_listing','Admin\ReportContrller@logistics_listing_report')
        ->name('admin_report_logistics_listing')
        ->middleware("admin_auth");

Route::get('admin/reports/buyer_orders','Admin\ReportContrller@buyer_orders_report')
        ->name('admin_report_buyer_orders')
        ->middleware("admin_auth");

Route::get('admin/reports/buyer_order_detail/{id}','Admin\ReportContrller@buyer_orders_detail_report')
        ->name('admin_report_buyer_order_detail')
        ->middleware("admin_auth");


Route::get('admin/reports/supplier_orders','Admin\ReportContrller@supplier_orders_report')
        ->name('admin_report_supplier_orders')
        ->middleware("admin_auth");

Route::get('admin/reports/supplier_order_detail/{id}/{user_id}','Admin\ReportContrller@supplier_orders_detail_report')
        ->name('admin_report_supplier_order_detail')
        ->middleware("admin_auth");


Route::get('admin/reports/logistics_orders','Admin\ReportContrller@logistics_orders_report')
        ->name('admin_report_logistics_orders')
        ->middleware("admin_auth");

Route::get('admin/reports/logistic_order_detail/{id}/{user_id}','Admin\ReportContrller@logistics_orders_detail_report')
        ->name('admin_report_logistic_order_detail')
        ->middleware("admin_auth");

Route::get('admin/reports/designmix_list','Admin\ReportContrller@designmix_listing_report')
        ->name('admin_report_designmix_list')
        ->middleware("admin_auth");
        
Route::get('admin/reports/designmix_list_export','Admin\ReportContrller@DesignMixListExport')
        ->name('admin_report_designmix_list_export')
        ->middleware("admin_auth");

Route::get('admin/reports/designmix_detail','Admin\ReportContrller@designmix_detail_report')
        ->name('admin_report_designmix_detail')
        ->middleware("admin_auth");

Route::get('admin/reports/vehicle_list','Admin\ReportContrller@vehicle_listing_report')
        ->name('admin_report_vehicle_list')
        ->middleware("admin_auth");
        
Route::get('admin/reports/vehicle_list_export','Admin\ReportContrller@VehicleListExport')
        ->name('admin_report_vehicle_list_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/TM_category_report','Admin\ReportContrller@TM_category_report')
        ->name('admin_report_TM_category_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/TM_category_report_export','Admin\ReportContrller@TMCatExport')
        ->name('admin_report_TM_category_report_export')
        ->middleware("admin_auth");
        
Route::get('admin/reports/TM_sub_category_report','Admin\ReportContrller@TM_sub_category_report')
        ->name('admin_report_TM_sub_category_report')
        ->middleware("admin_auth");
        
Route::get('admin/reports/TM_sub_category_report_export','Admin\ReportContrller@TMsubCatExport')
        ->name('admin_report_TM_sub_category_report_export')
        ->middleware("admin_auth");

Route::get('admin/reports/buyer_listing_export','Admin\ReportContrller@buyerListingExport')
        ->name('admin_report_export')
        ->middleware("admin_auth");

Route::get('admin/reports/buyer_orders_export','Admin\ReportContrller@buyerOrderExport')
        ->name('admin_report_buyer_orders_export')
        ->middleware("admin_auth");

Route::get('admin/reports/supplier_listing_export','Admin\ReportContrller@supplierListingExport')
        ->name('admin_report_supplier_listing_export')
        ->middleware("admin_auth");

Route::get('admin/reports/supplier_order_export','Admin\ReportContrller@supplierOrderExport')
        ->name('admin_report_supplier_order_export')
        ->middleware("admin_auth");



//Report Module Over




//Support Ticket Module Start

Route::get('admin/support_ticket','Admin\SupportTicketContoller@index')
        ->name('admin_show_support_ticket')
        ->middleware("admin_auth");
Route::get('admin/support_ticket/filters','Admin\SupportTicketContoller@index')
        ->name('admin_show_support_ticket_filters')
        ->middleware("admin_auth");
Route::post('support_ticket/add','Admin\SupportTicketContoller@addTicket');
Route::post('support_ticket/reply','Admin\SupportTicketContoller@replyTicket');
Route::get('admin/support_ticket/ticket_detail/{id}','Admin\SupportTicketContoller@showTicketDetails')
        ->name('admin_show_support_ticket_details')
        ->middleware("admin_auth");

Route::get('support_ticket/getBuyers','Admin\SupportTicketContoller@getBuyersList')
        ->middleware("admin_auth");

Route::get('support_ticket/getSuppliers','Admin\SupportTicketContoller@getSuppliersList')
        ->middleware("admin_auth");

Route::get('support_ticket/getLogistics','Admin\SupportTicketContoller@getLogisticsList')
        ->middleware("admin_auth");

Route::post('admin/support_ticket/ticket_detail/resolve','Admin\SupportTicketContoller@updateTicketStatus')
        ->name('admin_support_ticket_detail/resolve')
        ->middleware("admin_auth");


//Support Ticket Module Over
//Coupon Module Over

Route::get('admin/coupon','Admin\CouponController@index')
        ->name('admin_show_coupon')
        ->middleware("admin_auth");

Route::post('coupon/add','Admin\CouponController@addCoupon');
Route::post('coupon/edit','Admin\CouponController@editCoupon');
Route::post('coupon/active_inactive','Admin\CouponController@couponActiveInActive');
Route::post('coupon/delete','Admin\CouponController@couponDelete');

//Coupon Module Over

//New Proposal Module Start

Route::get('admin/proposal','Admin\ProposalController@index')
        ->name('admin_show_proposal')
        ->middleware("admin_auth");
Route::get('admin/proposal/filters','Admin\ProposalController@index')
        ->name('admin_show_proposal_filters')
        ->middleware("admin_auth");
Route::post('proposal/send_email','Admin\ProposalController@sendProposalEmail');

//New Proposal Module Over

//FAQ Module Start

Route::get('admin/faq','Admin\FAQContrller@index')
        ->name('admin_show_faq')
        ->middleware("admin_auth");

//FAQ Module Over

//Report Module Start

Route::get('admin/report','Admin\ReportContrller@index')
        ->name('admin_show_report')
        ->middleware("admin_auth");

Route::get('admin/report_detail','Admin\ReportContrller@report_detail')
        ->name('admin_show_report_detail')
        ->middleware("admin_auth");

//Report Module Over

//Profile Module Start

Route::get('admin/my_profile','Admin\ProfileController@index')
        ->name('admin_show_my_profile')
        ->middleware("admin_auth");
Route::post('my_profile/update','Admin\ProfileController@editProfile');
Route::get('my_profile/update','Admin\ProfileController@editProfile');
Route::post('my_profile/change_pass','Admin\ProfileController@changePassword');

//Profile Module Over

//Admin Panel Over..................................................

//Supplier Panel Start.................................................

Route::get('supplier','Supplier\LoginController@index')
        ->name('supplier');

Route::get('supplier/request_otp','Supplier\LoginController@requestOTP')
        ->middleware("supplier_auth");

Route::get('supplier/request_otp_to_master','Supplier\LoginController@requestOTPToMaster')
        ->middleware("supplier_auth");

Route::get('supplier/request_otp_to_plant','Supplier\LoginController@requestOTPToPlant')
        ->middleware("supplier_auth");

Route::get('supplier/request_otp_for_edit_profile','Supplier\LoginController@requestOTPforEditProfile')
        ->middleware("supplier_auth");

Route::post('supplier/login','Supplier\LoginController@login')
        ->name('supplier_login');

Route::get('supplier/notification','Supplier\ProfileController@getSupplierNotifications')
        ->name('supplier_noti');
        
Route::get('supplier/notification/{id}','Supplier\ProfileController@updateNotiStatus')
        ->name('supplier_noti_status_update');
        
Route::get('supplier/notifications','Supplier\ProfileController@supplierNotifications')
        ->name('supplier_notifications')
        ->middleware("supplier_auth");

Route::get('supplier/login','Supplier\LoginController@index')
        ->name('supplier_login');

Route::get('supplier/logout','Supplier\LoginController@logout')
        ->name('logout');

Route::get('supplier/signup','Supplier\SignupController@index')
        ->name('register');

Route::get('supplier/email_verify','Supplier\SignupController@signupEmailVerify')
        ->name('supplier_email_verify');

Route::post('supplier/register','Supplier\SignupController@signupRegistration')
        ->name('supplier_register');

Route::get('supplier/register','Supplier\SignupController@emailVerifier')
        ->name('supplier_register');

Route::get('supplier/signup_email_verifier','Supplier\SignupController@emailVerifier')
        ->name('signup_email_verifier');

Route::post('supplier/signup_otp_send','Supplier\SignupController@signupOTPSend')
        ->name('signup_otp_send');

Route::get('supplier/signup_otp_send','Supplier\SignupController@emailVerifier')
        ->name('signup_otp_send');

Route::get('supplier/signup_email_verifier_otp','Supplier\SignupController@emailVerifierOtp')
        ->name('signup_email_verifier_otp');


Route::post('supplier/signup_otp_verify','Supplier\SignupController@signupOTPVerify')
        ->name('signup_otp_verify');

Route::get('supplier/signup_otp_verify','Supplier\SignupController@emailVerifier')
        ->name('signup_otp_verify');

Route::get('supplier/dashboard','Supplier\DashboardController@index')
        ->name('supplier_dashboard')
        ->middleware("supplier_auth");

Route::get('supplier/dashboard_after_login','Supplier\DashboardController@afterLogin')
        ->name('supplier_dashboard_after_login')
        ->middleware("supplier_auth");

Route::get('supplier/get_day_capacity','Supplier\DashboardController@getDayCapacity')
        ->name('admin_get_day_capacity')
        ->middleware("supplier_auth");

Route::get('supplier/getMonthlyChartData','Supplier\DashboardController@getMonthlyChartDetail')
        ->name('supplier_monthly_chart_data')
        ->middleware("supplier_auth");

Route::get('supplier/getDailyChartData','Supplier\DashboardController@getDailyChartDetail')
        ->name('supplier_daily_chart_data')
        ->middleware("supplier_auth");

Route::get('supplier/getPorductBaseMonthlyChartData','Supplier\DashboardController@getProductBaseMonthlyChartDetails')
        ->name('supplier_product_base_monthly_chart_data')
        ->middleware("supplier_auth");

Route::get('supplier/getPorductBaseDailyChartData','Supplier\DashboardController@getProductBaseDayChartDetails')
        ->name('supplier_product_base_daily_chart_data')
        ->middleware("supplier_auth");


Route::get('supplier/forgot_pass_email_verifier','Supplier\ForgotPassController@index')
        ->name('forgot_pass_email_verifier');

Route::post('supplier/forgot_pass_otp_send','Supplier\ForgotPassController@forgotPassOTPSend')
        ->name('forgot_pass_otp_send');

Route::get('supplier/forgot_pass_otp_send','Supplier\ForgotPassController@forgotPassOTPSend')
        ->name('forgot_pass_otp_send');

Route::post('supplier/forgot_pass_otp_verify','Supplier\ForgotPassController@forgotPassOTPVerify')
        ->name('forgot_pass_otp_verify');

Route::get('supplier/forgot_pass_otp_verify','Supplier\ForgotPassController@forgotPassOTPVerify')
        ->name('forgot_pass_otp_verify');

Route::get('supplier/forgot_pass','Supplier\ForgotPassController@forgotPassOTPSend')
        ->name('forgot_pass');
        
Route::post('supplier/forgot_pass','Supplier\ForgotPassController@forgotPass')
        ->name('forgot_pass');

Route::post('supplier/change_pass','Supplier\ProfileController@changePassword')
        ->name('supplier_change_pass');


//Product Module Start................................................

Route::get('supplier/product','Supplier\ProductController@showProducts')
        ->name('supplier_product')
        ->middleware("supplier_auth");

Route::get('supplier/product/filters','Supplier\ProductController@showProducts')
        ->name('supplier_product_filters')
        ->middleware("supplier_auth");

Route::post('supplier/product/add','Supplier\ProductController@addProduct')
        ->name('supplier_product_add')
        ->middleware("supplier_auth");

Route::post('supplier/product/update','Supplier\ProductController@editProducts')
        ->name('supplier_product_update')
        ->middleware("supplier_auth");

Route::get('supplier/product/getSubcat/{id}','Supplier\ProductController@getSubCategory')
        ->name('supplier_product_sub_cat')
        ->middleware("supplier_auth");

Route::post('supplier/product/udpateStock','Supplier\ProductController@updateProductStock')
        ->name('supplier_product_udpateStock')
        ->middleware("supplier_auth");

//Product Module Over................................................


//Contact Details Module Over................................................

Route::get('supplier/contact_details','Supplier\ContactDetailController@index')
        ->name('contact_details')
        ->middleware("supplier_auth");

Route::post('supplier/contact_details/add','Supplier\ContactDetailController@addContactDetails')
        ->name('contact_details_add')
        ->middleware("supplier_auth");

Route::post('supplier/contact_details/update','Supplier\ContactDetailController@editContactDetails')
        ->name('contact_details_update')
        ->middleware("supplier_auth");


Route::get('supplier/driver_details','Supplier\DriverController@index')
        ->name('supplier_driver_details')
        ->middleware("supplier_auth");

Route::post('supplier/driver_details/add','Supplier\DriverController@addDriverDetails')
        ->name('supplier_driver_details_add')
        ->middleware("supplier_auth");


Route::post('supplier/driver_details/update','Supplier\DriverController@editDriverDetails')
        ->name('supplier_driver_details_update')
        ->middleware("supplier_auth");


Route::get('supplier/operator_details','Supplier\DriverController@showOperatorDetails')
        ->name('supplier_operator_details')
        ->middleware("supplier_auth");

Route::post('supplier/operator_details/add','Supplier\DriverController@addOperatorDetails')
        ->name('supplier_operator_details_add')
        ->middleware("supplier_auth");


Route::post('supplier/operator_details/update','Supplier\DriverController@editOperatorDetails')
        ->name('supplier_operator_details_update')
        ->middleware("supplier_auth");
        
Route::get('supplier/pumpgang_details','Supplier\DriverController@showPumpGangDetails')
        ->name('supplier_pumpgang_details')
        ->middleware("supplier_auth");

Route::post('supplier/pumpgang_details/add','Supplier\DriverController@addPumpGangDetails')
        ->name('supplier_pumpgang_details_add')
        ->middleware("supplier_auth");


Route::post('supplier/pumpgang_details/update','Supplier\DriverController@editPumpGangDetails')
        ->name('supplier_pumpgang_details_update')
        ->middleware("supplier_auth");

//Contact Details Module Over................................................
//Concrete Pump Module Start................................................


Route::get('supplier/concrete_pump_details','Supplier\ConcretePumpController@index')
        ->name('supplier_concrete_pump_details')
        ->middleware("supplier_auth");

Route::post('supplier/concrete_pump_details/add','Supplier\ConcretePumpController@addConcretePump')
        ->name('supplier_concrete_pump_details_add')
        ->middleware("supplier_auth");


Route::post('supplier/concrete_pump_details/update','Supplier\ConcretePumpController@editConcretePump')
        ->name('supplier_concrete_pump_details_update')
        ->middleware("supplier_auth");


//Concrete Pump Module Over................................................


//Vehicle Module Start................................................

Route::get('supplier/TM','Supplier\VehicleController@index')
        ->name('TM_detail')
        ->middleware("supplier_auth");

Route::get('supplier/TM/add_view','Supplier\VehicleController@addTMView')
        ->name('TM_add_view')
        ->middleware("supplier_auth");

Route::post('supplier/TM/add','Supplier\VehicleController@addTM')
        ->name('TM_add')
        ->middleware("supplier_auth");

Route::get('supplier/TM/edit_view/{id}','Supplier\VehicleController@editTMView')
        ->name('TM_edit_view')
        ->middleware("supplier_auth");

// Route::get('supplier/TM/edit_view','Supplier\VehicleController@index')
//         ->name('TM_edit_view')

Route::post('supplier/TM/edit','Supplier\VehicleController@editTM')
        ->name('TM_edit')
        ->middleware("supplier_auth");

Route::get('supplier/TM/getTMSubCatByCatId','Supplier\VehicleController@getSubCatByCatId')
        ->name('TM_getTMSubCatByCatId')
        ->middleware("supplier_auth");


//Vehicle Module Over................................................
//Settings Module Over................................................

Route::get('supplier/settings','Supplier\SettingController@index')
        ->name('supplier_settings')
        ->middleware("supplier_auth");

Route::post('supplier/settings/update','Supplier\SettingController@editSettings')
        ->name('supplier_settings_update')
        ->middleware("supplier_auth");

Route::post('supplier/admix_settings/update','Supplier\SettingController@editAdmixSettings')
        ->name('supplier_admix_settings_update')
        ->middleware("supplier_auth");

Route::get('supplier/settings/order_accept_or_not','Supplier\SettingController@orderAceeptOrNot')
        ->name('supplier_order_accept_or_not')
        ->middleware("supplier_auth");

//Settings Module Over................................................
//TM Un Availability Module Start................................................

Route::get('supplier/TM_unavailable','Supplier\VehicleController@getTMunAvailabiity')
        ->name('supplier_TM_unavailable')
        ->middleware("supplier_auth");

Route::post('supplier/TM_unavailable/add','Supplier\VehicleController@addTMunAvailabiity')
        ->name('supplier_TM_unavailable_add')
        ->middleware("supplier_auth");

Route::post('supplier/TM_unavailable/edit','Supplier\VehicleController@editTMunAvailabiity')
        ->name('supplier_TM_unavailable_edit')
        ->middleware("supplier_auth");

Route::get('supplier/TM_unavailable/delete','Supplier\VehicleController@deleteTMunAvailabiity')
        ->name('supplier_TM_unavailable_delete')
        ->middleware("supplier_auth");

//TM Un Availability Module Over................................................

//CP Un Availability Module Start................................................

Route::get('supplier/CP_unavailable','Supplier\VehicleController@getCPunAvailabiity')
        ->name('supplier_CP_unavailable')
        ->middleware("supplier_auth");

Route::post('supplier/CP_unavailable/add','Supplier\VehicleController@addCPunAvailabiity')
        ->name('supplier_CP_unavailable_add')
        ->middleware("supplier_auth");

Route::post('supplier/CP_unavailable/edit','Supplier\VehicleController@editCPunAvailabiity')
        ->name('supplier_CP_unavailable_edit')
        ->middleware("supplier_auth");

Route::get('supplier/CP_unavailable/delete','Supplier\VehicleController@deleteCPunAvailabiity')
        ->name('supplier_CP_unavailable_delete')
        ->middleware("supplier_auth");

//CP Un Availability Module Over................................................

//Design Mix Module Start................................................

Route::get('supplier/design_mix','Supplier\DesignMixController@index')
        ->name('design_mix_detail')
        ->middleware("supplier_auth");

Route::get('supplier/design_mix/add_view','Supplier\DesignMixController@addMixView')
        ->name('design_mix_add_view')
        ->middleware("supplier_auth");

Route::post('supplier/design_mix/add','Supplier\DesignMixController@addMix')
        ->name('design_mix_add')
        ->middleware("supplier_auth");

Route::get('supplier/design_mix/edit_view/{id}','Supplier\DesignMixController@editMixView')
        ->name('design_mix_edit_view')
        ->middleware("supplier_auth");

// Route::get('supplier/TM/edit_view','Supplier\DesignMixController@index')
//         ->name('TM_edit_view')

Route::post('supplier/design_mix/edit','Supplier\DesignMixController@editMix')
        ->name('design_mix_edit')
        ->middleware("supplier_auth");

Route::get('supplier/design_mix/get_grade_mix','Supplier\DesignMixController@getDesignMixGradeWise')
        ->name('design_mix_get_grade_wise')
        ->middleware("supplier_auth");
        
Route::get('supplier/design_mix/get_mix_combination','Supplier\DesignMixController@getDesignMixCombinationGradeWise')
        ->name('design_mix_get_mix_combination')
        ->middleware("supplier_auth");
        
Route::get('supplier/design_mix/get_single_mix_combination','Supplier\DesignMixController@getSingleDesignMixCombinationByBrandIds')
        ->name('design_mix_get_single_mix_combination')
        ->middleware("supplier_auth");

Route::get('supplier/get_admixture_types','Supplier\DesignMixController@getAdmixtureTypes')
        ->name('supplier_get_admixture_types')
        ->middleware("supplier_auth");

Route::get('supplier/get_agregate_sub_cat_2/{id}','Supplier\DesignMixController@getAggregateSubCat2')
        ->name('supplier_get_agregate_sub_cat_2')
        ->middleware("supplier_auth");

Route::get('supplier/design_mix/getTMSubCatByCatId','Supplier\DesignMixController@getSubCatByCatId')
        ->name('design_mix_getTMSubCatByCatId')
        ->middleware("supplier_auth");


Route::get('supplier/media_design_mix','Supplier\DesignMixController@showMedia')
        ->name('media_design_mix_detail')
        ->middleware("supplier_auth");

Route::post('supplier/media_design_mix/add','Supplier\DesignMixController@addPrimaryMedia')
        ->name('media_design_mix_add_detail')
        ->middleware("supplier_auth");

Route::post('supplier/second_media_design_mix/add','Supplier\DesignMixController@addSecondoryMedia')
        ->name('second_media_design_mix_add_detail')
        ->middleware("supplier_auth");

Route::get('supplier/media_design_mix/delete','Supplier\DesignMixController@deleteMedia')
        ->name('media_design_mix_delete_detail')
        ->middleware("supplier_auth");

//Design Mix Module Over................................................
//Rate Module Over................................................

Route::get('supplier/cement_rate','Supplier\RateContoller@showCementRate')
        ->name('cement_rate')
        ->middleware("supplier_auth");

Route::post('supplier/cement_rate/add','Supplier\RateContoller@addCementRate')
        ->name('cement_rate_add')
        ->middleware("supplier_auth");

Route::post('supplier/cement_rate/update','Supplier\RateContoller@editCementRate')
        ->name('cement_rate_update')
        ->middleware("supplier_auth");

Route::get('supplier/cement_rate/delete','Supplier\RateContoller@deleteCementRate')
        ->name('cement_rate_delete')
        ->middleware("supplier_auth");


Route::get('supplier/sand_rate','Supplier\RateContoller@showSandRate')
        ->name('sand_rate')
        ->middleware("supplier_auth");

Route::post('supplier/sand_rate/add','Supplier\RateContoller@addSandRate')
        ->name('sand_rate_add')
        ->middleware("supplier_auth");

Route::post('supplier/sand_rate/update','Supplier\RateContoller@editSandRate')
        ->name('sand_rate_update')
        ->middleware("supplier_auth");

Route::get('supplier/sand_rate/delete','Supplier\RateContoller@deleteSandRate')
        ->name('sand_rate_delete')
        ->middleware("supplier_auth");


Route::get('supplier/aggregate_rate','Supplier\RateContoller@showAggregateRate')
        ->name('aggregate_rate')
        ->middleware("supplier_auth");

Route::post('supplier/aggregate_rate/add','Supplier\RateContoller@addAggregateRate')
        ->name('aggregate_rate_add')
        ->middleware("supplier_auth");

Route::post('supplier/aggregate_rate/update','Supplier\RateContoller@editAggregateRate')
        ->name('aggregate_rate_update')
        ->middleware("supplier_auth");

Route::get('supplier/aggregate_rate/delete','Supplier\RateContoller@deleteAggregateRate')
        ->name('aggregate_rate_delete')
        ->middleware("supplier_auth");

Route::get('supplier/flyash_rate','Supplier\RateContoller@showFlyashRate')
        ->name('flyash_rate')
        ->middleware("supplier_auth");

Route::post('supplier/flyash_rate/add','Supplier\RateContoller@addFlyashRate')
        ->name('flyash_rate_add')
        ->middleware("supplier_auth");

Route::post('supplier/flyash_rate/update','Supplier\RateContoller@editFlyashRate')
        ->name('flyash_rate_update')
        ->middleware("supplier_auth");

Route::get('supplier/flyash_rate/delete','Supplier\RateContoller@deleteFlyashRate')
        ->name('flyash_rate_delete')
        ->middleware("supplier_auth");

Route::get('supplier/admixture_rate','Supplier\RateContoller@showAdmixtureRate')
        ->name('admixture_rate')
        ->middleware("supplier_auth");

Route::post('supplier/admixture_rate/add','Supplier\RateContoller@addAdmixtureRate')
        ->name('admixture_rate_add')
        ->middleware("supplier_auth");

Route::post('supplier/admixture_rate/update','Supplier\RateContoller@editAdmixtureRate')
        ->name('admixture_rate_update')
        ->middleware("supplier_auth");

Route::get('supplier/admixture_rate/delete','Supplier\RateContoller@deleteAdmixtureRate')
        ->name('admixture_rate_delete')
        ->middleware("supplier_auth");

Route::get('supplier/water_rate','Supplier\RateContoller@showWaterRate')
        ->name('water_rate')
        ->middleware("supplier_auth");

Route::post('supplier/water_rate/add','Supplier\RateContoller@addWaterRate')
        ->name('water_rate_add')
        ->middleware("supplier_auth");

Route::post('supplier/water_rate/update','Supplier\RateContoller@editWaterRate')
        ->name('water_rate_update')
        ->middleware("supplier_auth");

Route::get('supplier/water_rate/delete','Supplier\RateContoller@deleteWaterRate')
        ->name('water_rate_delete')
        ->middleware("supplier_auth");

//Rate Module Over................................................

//Profit Overhead Margin Module Over................................................

Route::get('supplier/profit_margin','Supplier\ProfitOverheadController@showProfitMargin')
        ->name('profit_margin')
        ->middleware("supplier_auth");

Route::post('supplier/profit_margin/add','Supplier\ProfitOverheadController@addProfitMargin')
        ->name('profit_margin_add')
        ->middleware("supplier_auth");

Route::post('supplier/profit_margin/update','Supplier\ProfitOverheadController@editProfitMargin')
        ->name('profit_margin_update')
        ->middleware("supplier_auth");

Route::get('supplier/profit_margin/delete','Supplier\ProfitOverheadController@deleteProfitMargin')
        ->name('profit_margin_delete')
        ->middleware("supplier_auth");
        
Route::get('supplier/overhead_margin','Supplier\ProfitOverheadController@showOverheadMargin')
        ->name('overhead_margin')
        ->middleware("supplier_auth");

Route::post('supplier/overhead_margin/add','Supplier\ProfitOverheadController@addOverheadMargin')
        ->name('overhead_margin_add')
        ->middleware("supplier_auth");

Route::post('supplier/overhead_margin/update','Supplier\ProfitOverheadController@editOverheadMargin')
        ->name('overhead_margin_update')
        ->middleware("supplier_auth");

Route::get('supplier/overhead_margin/delete','Supplier\ProfitOverheadController@deleteOverheadMargin')
        ->name('overhead_margin_delete')
        ->middleware("supplier_auth");
        
        
//stock Module Over................................................

Route::get('supplier/cement_stock','Supplier\StockController@showCementStock')
        ->name('cement_stock')
        ->middleware("supplier_auth");

Route::post('supplier/cement_stock/add','Supplier\StockController@addCementStock')
        ->name('cement_stock_add')
        ->middleware("supplier_auth");

Route::post('supplier/cement_stock/update','Supplier\StockController@editCementStock')
        ->name('cement_stock_update')
        ->middleware("supplier_auth");

Route::get('supplier/cement_stock/delete','Supplier\StockController@deleteCementStock')
        ->name('cement_stock_delete')
        ->middleware("supplier_auth");


Route::get('supplier/sand_stock','Supplier\StockController@showSandStock')
        ->name('sand_stock')
        ->middleware("supplier_auth");

Route::post('supplier/sand_stock/add','Supplier\StockController@addSandStock')
        ->name('sand_stock_add')
        ->middleware("supplier_auth");

Route::post('supplier/sand_stock/update','Supplier\StockController@editSandStock')
        ->name('sand_stock_update')
        ->middleware("supplier_auth");

Route::get('supplier/sand_stock/delete','Supplier\StockController@deleteSandStock')
        ->name('sand_stock_delete')
        ->middleware("supplier_auth");


Route::get('supplier/aggregate_stock','Supplier\StockController@showAggregateStock')
        ->name('aggregate_stock')
        ->middleware("supplier_auth");

Route::post('supplier/aggregate_stock/add','Supplier\StockController@addAggregateStock')
        ->name('aggregate_stock_add')
        ->middleware("supplier_auth");

Route::post('supplier/aggregate_stock/update','Supplier\StockController@editAggregateStock')
        ->name('aggregate_stock_update')
        ->middleware("supplier_auth");

Route::get('supplier/aggregate_stock/delete','Supplier\StockController@deleteAggregateStock')
        ->name('aggregate_stock_delete')
        ->middleware("supplier_auth");

Route::get('supplier/flyash_stock','Supplier\StockController@showFlyashStock')
        ->name('flyash_stock')
        ->middleware("supplier_auth");

Route::post('supplier/flyash_stock/add','Supplier\StockController@addFlyashStock')
        ->name('flyash_stock_add')
        ->middleware("supplier_auth");

Route::post('supplier/flyash_stock/update','Supplier\StockController@editFlyashStock')
        ->name('flyash_stock_update')
        ->middleware("supplier_auth");

Route::get('supplier/flyash_stock/delete','Supplier\StockController@deleteFlyashStock')
        ->name('flyash_stock_delete')
        ->middleware("supplier_auth");

Route::get('supplier/admixture_stock','Supplier\StockController@showAdmixtureStock')
        ->name('admixture_stock')
        ->middleware("supplier_auth");

Route::post('supplier/admixture_stock/add','Supplier\StockController@addAdmixtureStock')
        ->name('admixture_stock_add')
        ->middleware("supplier_auth");

Route::post('supplier/admixture_stock/update','Supplier\StockController@editAdmixtureStock')
        ->name('admixture_stock_update')
        ->middleware("supplier_auth");

Route::get('supplier/admixture_stock/delete','Supplier\StockController@deleteAdmixtureStock')
        ->name('admixture_stock_delete')
        ->middleware("supplier_auth");

//stock Module Over................................................


//Bank Details Module Start................................................

Route::get('supplier/bank_details','Supplier\BankDetailsController@index')
        ->name('bank_details')
        ->middleware("supplier_auth");

Route::post('supplier/bank_details/add','Supplier\BankDetailsController@addBankDetails')
        ->name('bank_details_add')
        ->middleware("supplier_auth");

Route::post('supplier/bank_details/update','Supplier\BankDetailsController@editBankDetails')
        ->name('bank_details_update')
        ->middleware("supplier_auth");

Route::post('supplier/bank_details/make_default','Supplier\BankDetailsController@setDefaultBaknkDefault')
        ->name('bank_details_default')
        ->middleware("supplier_auth");

//Bank Details Module Over................................................

//Address Module Start................................................

Route::get('supplier/address','Supplier\AddressController@index')
        ->name('address_view')
        ->middleware("supplier_auth");

Route::get('supplier/address/add','Supplier\AddressController@addAddressView')
        ->name('address_add')
        ->middleware("supplier_auth");

Route::post('supplier/address/edit','Supplier\AddressController@editAddressView')
        ->name('address_edit')
        ->middleware("supplier_auth");

Route::get('supplier/address/edit','Supplier\AddressController@index')
        ->name('address_edit')
        ->middleware("supplier_auth");

Route::post('supplier/address/add_address','Supplier\AddressController@addAddress')
        ->name('address_add_address')
        ->middleware("supplier_auth");

Route::post('supplier/address/edit_address','Supplier\AddressController@editAddress')
        ->name('address_edit_address')
        ->middleware("supplier_auth");

Route::get('supplier/address/getStates','Supplier\AddressController@getStates')
        ->name('get_states')
        ->middleware("supplier_auth");

Route::get('supplier/address/getCities','Supplier\AddressController@getCities')
        ->name('get_cities')
        ->middleware("supplier_auth");

Route::get('supplier/address/getSourceByRegion','Supplier\AddressController@getSourceByRegion')
        ->name('get_source_by_region')
        ->middleware("supplier_auth");

Route::get('supplier/billing_address','Supplier\AddressController@showbillingAddress')
        ->name('suppllier_billing_address_view')
        ->middleware("supplier_auth");

Route::get('supplier/billing_address/add','Supplier\AddressController@addBillingAddressView')
        ->name('suppllier_billing_address_add')
        ->middleware("supplier_auth");

Route::post('supplier/billing_address/add','Supplier\AddressController@addBillingAddress')
        ->name('suppllier_billing_add_address_add')
        ->middleware("supplier_auth");

Route::post('supplier/billing_address/edit','Supplier\AddressController@editBillingAddressView')
        ->name('suppllier_billing_address_edit')
        ->middleware("supplier_auth");

Route::post('supplier/billing_address/edit_address','Supplier\AddressController@editBillingAddress')
        ->name('suppllier_billing_address_edit_address')
        ->middleware("supplier_auth");

Route::get('supplier/billing_address/edit','Supplier\AddressController@showbillingAddress')
        ->name('suppllier_billing_address_edit_get')
        ->middleware("supplier_auth");

//Address Module Over................................................

//Support Module Start................................................

Route::get('supplier/support_ticket','Supplier\SupportTicketController@index')
        ->name('support_ticket_view')
        ->middleware("supplier_auth");

Route::get('supplier/support_ticket/filters','Supplier\SupportTicketController@index')
        ->name('support_ticket_filters')
        ->middleware("supplier_auth");

Route::post('supplier/support_ticket/add','Supplier\SupportTicketController@addTicket')
        ->name('support_ticket_add')
        ->middleware("supplier_auth");

Route::post('supplier/support_ticket/reply','Supplier\SupportTicketController@replyTicket')
        ->name('support_ticket_reply')
        ->middleware("supplier_auth");

Route::get('supplier/support_ticket/ticket_detail/{id}','Supplier\SupportTicketController@showTicketDetails')
        ->name('support_ticket_detail/{id}')
        ->middleware("supplier_auth");

Route::post('supplier/support_ticket/ticket_detail/resolve','Supplier\SupportTicketController@updateTicketStatus')
        ->name('support_ticket_detail/resolve')
        ->middleware("supplier_auth");




//Support Module Over................................................

//Orders Module Start................................................

Route::get('supplier/orders','Supplier\OrderController@index')
        ->name('orders_view')
        ->middleware("supplier_auth");

Route::get('supplier/surprise_orders','Supplier\OrderController@surpriseOrders')
        ->name('surprise_orders_view')
        ->middleware("supplier_auth");

Route::get('supplier/orders/filters','Supplier\OrderController@index')
        ->name('orders_filter')
        ->middleware("supplier_auth");

Route::get('supplier/orders/details/{id}','Supplier\OrderController@orderDetails')
        ->name('supplier_orders_details')
        ->middleware("supplier_auth");

Route::get('supplier/surprise_orders/details/{id}','Supplier\OrderController@surpriseOrderDetails')
        ->name('supplier_surprise_orders_details')
        ->middleware("supplier_auth");

Route::post('supplier/orders/bill_upload','Supplier\OrderController@uploadOrderBill')
        ->name('supplier_orders_bill_upload')
        ->middleware("supplier_auth");
        
Route::post('supplier/orders/reassign','Supplier\OrderController@reassigningOrders')
        ->name('supplier_orders_reassign')
        ->middleware("supplier_auth");

Route::get('supplier/order_track_detail/{item_id}/{tracking_id}','Supplier\OrderController@trackingDetails')
        ->name('supplier_order_track_detail');
        
Route::get('supplier/get_assigned_order_list','Supplier\OrderController@getAssignedOrderListByDate')
        ->name('supplier_get_assigned_order_list');

Route::get('supplier/order_CP_track_detail/{item_part_id}','Supplier\OrderController@CPtrackingDetails')
        ->name('supplier_order_CP_track_detail');

Route::get('supplier/order_track/get_CP_delivered/{vendor_order_id}/{order_item_id}','Supplier\OrderController@getDeliveredCP')
        ->name('supplier_order_get_CP_delivered_detail');

Route::post('supplier/supplier_truck_assign','Supplier\OrderController@truckAssign')
        ->name('supplier_truck_assign');

Route::post('supplier/supplier_cp_assign','Supplier\OrderController@cpAssign')
        ->name('supplier_cp_assign');

Route::get('supplier/supplier_order_accept','Supplier\OrderController@orderAccept')
        ->name('supplier_order_accept');

Route::get('supplier/supplier_surprise_order_accept','Supplier\OrderController@surpriseOrderAccept')
        ->name('supplier_surprise_order_accept');

Route::post('supplier/supplier_report_7_day','Supplier\OrderController@report7DayCubeTest')
        ->name('supplier_report_7_day');

Route::post('supplier/supplier_report_28_day','Supplier\OrderController@report28DayCubeTest')
        ->name('supplier_report_28_day');

//Orders Module Over................................................

//Report Module Over................................................

Route::get('supplier/reports','Supplier\ReportController@index')
        ->name('supplier_reports')
        ->middleware("supplier_auth");

Route::get('supplier/report/supplier_order_export','Supplier\ReportController@supplierOrderExport')
        ->name('supplier_report_supplier_order_export')
        ->middleware("supplier_auth");

//Report Module Over................................................

//FAQ Module Over................................................

Route::get('supplier/faq','Supplier\FAQController@index')
        ->name('supplier_faq')
        ->middleware("supplier_auth");

Route::get('supplier/how_it_works','Supplier\FAQController@howItWorks')
        ->name('supplier_how_it_works')
        ->middleware("supplier_auth");

//FAQ Module Over................................................

//Payment History Module Over................................................

Route::get('supplier/payment_history','Supplier\PaymentHistoryController@index')
        ->name('supplier_payment_history')
        ->middleware("supplier_auth");

//Payment History Module Over................................................

//Prodile Module Over................................................

Route::get('supplier/profile','Supplier\ProfileController@index')
        ->name('supplier_profile')
        ->middleware("supplier_auth");

Route::get('supplier/profile/edit_view','Supplier\ProfileController@editProfileView')
        ->name('supplier_profile_edit_view')
        ->middleware("supplier_auth");

Route::post('supplier/profile/edit','Supplier\ProfileController@editProfile')
        ->name('supplier_profile_edit')
        ->middleware("supplier_auth");

Route::get('supplier/profile/edit','Supplier\ProfileController@editProfile')
        ->name('supplier_profile_edit_get')
        ->middleware("supplier_auth");

//Prodile Module Over................................................

//New Proposal Module Start................................................

Route::get('supplier/proposal','Supplier\NewProposalConroller@index')
        ->name('supplier_proposal')
        ->middleware("supplier_auth");

Route::post('supplier/add_proposal','Supplier\NewProposalConroller@addProposal')
        ->name('supplier_add_proposal')
        ->middleware("supplier_auth");

//New Proposal Module Over................................................

//Report Module Start................................................

Route::get('supplier/reports/plant_address_report','Supplier\ReportController@plant_address_listing_report')
        ->name('supplier_plant_address_report')
        ->middleware("supplier_auth");      


Route::get('supplier/reports/plant_address_report_export','Supplier\ReportController@plantAddressOrderExport')
        ->name('supplier_report_plant_address_report_export')
        ->middleware("supplier_auth");

Route::get('supplier/reports/billing_address_report','Supplier\ReportController@billing_address_listing_report')
        ->name('supplier_billing_address_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/billing_address_report_export','Supplier\ReportController@billingAddressOrderExport')
        ->name('supplier_billing_address_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/TM_driver_report','Supplier\ReportController@TM_driver_listing_report')
        ->name('supplier_TM_driver_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/TM_driver_report_export','Supplier\ReportController@TMDriverExport')
        ->name('supplier_TM_driver_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/operator_listing_report','Supplier\ReportController@operator_listing_report')
        ->name('supplier_operator_listing_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/operator_listing_report_export','Supplier\ReportController@operatorExport')
        ->name('supplier_operator_listing_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/TM_listing_report','Supplier\ReportController@TM_listing_report')
        ->name('supplier_TM_listing_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/TM_listing_report_export','Supplier\ReportController@TMListExport')
        ->name('supplier_TM_listing_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/CP_listing_report','Supplier\ReportController@CP_listing_report')
        ->name('supplier_CP_listing_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/CP_listing_report_export','Supplier\ReportController@CPListExport')
        ->name('supplier_CP_listing_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/admix_stock_report','Supplier\ReportController@admix_stock_report')
        ->name('supplier_admix_stock_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/admix_stock_report_export','Supplier\ReportController@admixStockExport')
        ->name('supplier_admix_stock_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/agg_stock_report','Supplier\ReportController@agg_stock_report')
        ->name('supplier_agg_stock_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/agg_stock_report_export','Supplier\ReportController@aggStockExport')
        ->name('supplier_agg_stock_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/cement_stock_report','Supplier\ReportController@cement_stock_report')
        ->name('supplier_cement_stock_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/cement_stock_report_export','Supplier\ReportController@cementStockExport')
        ->name('supplier_cement_stock_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/flyAsh_stock_report','Supplier\ReportController@flyAsh_stock_report')
        ->name('supplier_flyAsh_stock_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/flyAsh_stock_report_export','Supplier\ReportController@flyAshStockExport')
        ->name('supplier_flyAsh_stock_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/sand_stock_report','Supplier\ReportController@sand_stock_report')
        ->name('supplier_sand_stock_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/sand_stock_report_export','Supplier\ReportController@sandStockExport')
        ->name('supplier_sand_stock_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/bank_report','Supplier\ReportController@bank_report')
        ->name('supplier_bank_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/bank_report_export','Supplier\ReportController@BankdetailsExport')
        ->name('supplier_bank_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/design_mix_report','Supplier\ReportController@design_mix_report')
        ->name('supplier_design_mix_report')
        ->middleware("supplier_auth");
        
Route::get('supplier/reports/design_mix_report_export','Supplier\ReportController@DesignmixDetailExport')
        ->name('supplier_design_mix_report_export')
        ->middleware("supplier_auth");
        
Route::get('supplier/report/orders','Supplier\ReportController@order_listing_report')
        ->name('supplier_order_list_report')
        ->middleware("supplier_auth");

Route::get('supplier/report/products','Supplier\ReportController@product_listing_report')
        ->name('supplier_product_list_report')
        ->middleware("supplier_auth");



//Report Module Over................................................

//Supplier Panel Over.................................................

//Logistics Panel Start.................................................

Route::get('logistics','Logistics\LoginController@index')
        ->name('logistics');

Route::post('logistics/login','Logistics\LoginController@login')
        ->name('logistics_login');

Route::get('logistics/login','Logistics\LoginController@index')
        ->name('logistics_login');

Route::get('logistics/logout','Logistics\LoginController@logout')
        ->name('logistic_logout');

Route::get('logistics/signup','Logistics\SignupController@index')
        ->name('logistic_register');

Route::post('logistics/register','Logistics\SignupController@signupRegistration')
        ->name('logistics_register');

Route::get('logistics/register','Logistics\SignupController@emailVerifier')
        ->name('logistics_register');

Route::get('logistics/register_2','Logistics\SignupController@signupRegistration2')
        ->name('logistics_register_2');

Route::post('logistics/register_2_submit','Logistics\SignupController@signupRegistration2Submit')
        ->name('logistics_register_2_submit');

Route::get('logistics/signup_email_verifier','Logistics\SignupController@emailVerifier')
        ->name('logistic_signup_email_verifier');

Route::post('logistics/signup_otp_send','Logistics\SignupController@signupOTPSend')
        ->name('logistic_signup_otp_send');

Route::get('logistics/signup_otp_send','Logistics\SignupController@emailVerifier')
        ->name('logistic_signup_otp_send');

Route::get('logistics/signup_email_verifier_otp','Logistics\SignupController@emailVerifierOtp')
        ->name('logistic_signup_email_verifier_otp');

Route::post('logistics/signup_otp_verify','Logistics\SignupController@signupOTPVerify')
        ->name('logistic_signup_otp_verify');


Route::get('logistics/forgot_pass_email_verifier','Logistics\ForgotPassController@index')
        ->name('logistics_forgot_pass_email_verifier');

Route::post('logistics/forgot_pass_otp_send','Logistics\ForgotPassController@forgotPassOTPSend')
        ->name('logistics_forgot_pass_otp_send');

Route::get('logistics/forgot_pass_otp_send','Logistics\ForgotPassController@forgotPassOTPSend')
        ->name('logistics_forgot_pass_otp_send');

Route::post('logistics/forgot_pass_otp_verify','Logistics\ForgotPassController@forgotPassOTPVerify')
        ->name('logistics_forgot_pass_otp_verify');

Route::post('logistics/forgot_pass','Logistics\ForgotPassController@forgotPass')
        ->name('logistics_forgot_pass');



Route::get('logistics/dashboard','Logistics\DashboardController@index')
        ->name('logistics_dashboard')
        ->middleware("logistics_auth");


//Dashboard Module Start................................................


Route::get('logistics/getMonthlyChartData','Logistics\DashboardController@getMonthlyChartDetail')
        ->name('logistics_monthly_chart_data')
        ->middleware("logistics_auth");

Route::get('logistics/getDailyChartData','Logistics\DashboardController@getDailyChartDetail')
        ->name('logistics_daily_chart_data')
        ->middleware("logistics_auth");


//Dashboard Module End................................................
//Quote Module Start................................................

Route::get('logistics/send_quote','Logistics\QuoteController@index')
        ->name('logistic_send_quote');

Route::post('logistics/send_quote/update','Logistics\QuoteController@updateQuote')
        ->name('logistic_send_quote_update');


//Quote Module Over................................................

//Latest Trips Module Start................................................

Route::get('logistics/latest_trips','Logistics\LatestTripsController@index')
        ->name('logistic_latest_trips');

Route::get('logistics/latest_trips_details/{id}','Logistics\LatestTripsController@tripDetail')
        ->name('logistic_latest_trips_details');

Route::post('logistics/latest_trips_truck_assign','Logistics\LatestTripsController@truckAssign')
        ->name('logistic_latest_trips_truck_assign');

Route::post('logistics/orders/bill_upload','Logistics\LatestTripsController@uploadOrderBill')
        ->name('logistics_orders_bill_upload')
        ->middleware("logistics_auth");

Route::get('logistics/order_track_detail/{item_id}/{tracking_id}','Logistics\LatestTripsController@trackingDetails')
        ->name('logistics_order_track_detail')
        ->middleware("logistics_auth");

//Latest Trips Module Over................................................

//Vehicle Module Start................................................

Route::get('logistics/vehicles','Logistics\VehicleController@index')
        ->name('logistic_vehicles');

Route::get('logistics/vehicles/add_view','Logistics\VehicleController@addVehicleView')
        ->name('logistic_vehicles_add_view');

Route::post('logistics/vehicles/add','Logistics\VehicleController@addVehicle')
        ->name('logistic_vehicles_add');

Route::get('logistics/vehicles/edit_view/{id}','Logistics\VehicleController@editVehicleView')
        ->name('logistic_vehicles_edit_view');

// Route::get('logistics/vehicles/edit_view','Logistics\VehicleController@index')
//         ->name('logistic_vehicles_edit_view');

Route::post('logistics/vehicles/edit','Logistics\VehicleController@editVehicle')
        ->name('logistic_vehicles_edit');

Route::get('logistics/vehicles/getVehicleSubCatByCatId','Logistics\VehicleController@getSubCatByCatId')
        ->name('logistic_vehicles_getVehicleSubCatByCatId');


//Vehicle Module Over................................................

//Bank Module Over................................................

Route::get('logistics/bank_details','Logistics\BankController@index')
        ->name('logistic_bank_details');

Route::post('logistics/bank_details/add','Logistics\BankController@addBankDetails')
        ->name('logistics_bank_details_add')
        ->middleware("logistics_auth");

Route::post('logistics/bank_details/update','Logistics\BankController@editBankDetails')
        ->name('logistics_bank_details_update')
        ->middleware("logistics_auth");

Route::post('logistics/bank_details/make_default','Logistics\BankController@setDefaultBaknkDefault')
        ->name('logistics_bank_details_default')
        ->middleware("logistics_auth");

//Bank Module Over................................................

//Logistics Module Start................................................

Route::get('logistics/driver_details','Logistics\DriverController@index')
        ->name('logistic_driver_details');

Route::post('logistics/driver_details/add','Logistics\DriverController@addDriverDetails')
        ->name('logistic_driver_details_add');


Route::post('logistics/driver_details/update','Logistics\DriverController@editDriverDetails')
        ->name('logistic_driver_details_update');

//Logistics Module Over................................................

//Profile Module Start................................................

Route::get('logistics/profile','Logistics\ProfileController@index')
        ->name('logistics_profile')
        ->middleware("logistics_auth");

Route::get('logistics/profile/edit_view','Logistics\ProfileController@editProfileView')
        ->name('logistics_profile_edit_view')
        ->middleware("logistics_auth");

Route::post('logistics/profile/edit','Logistics\ProfileController@editProfile')
        ->name('logistics_profile_edit')
        ->middleware("logistics_auth");

//Profile Module Over................................................
//Payment History Module Start................................................

Route::get('logistics/payment_history','Logistics\PaymentHistoryController@index')
        ->name('logistics_payment_history')
        ->middleware("logistics_auth");

//Payment History Module Over................................................

//Report Module Start................................................

Route::get('logistics/report','Logistics\ReportController@index')
        ->name('logistics_report')
        ->middleware("logistics_auth");

//Report Module Over................................................

//FAQ Module Start................................................

Route::get('logistics/faq','Logistics\FAQController@index')
        ->name('logistics_faq')
        ->middleware("logistics_auth");

//FAQ Module Over................................................
//Report Module Start................................................




Route::get('logistics/report/orders','Logistics\ReportController@order_listing_report')
        ->name('logistics_order_listing_report')
        ->middleware("logistics_auth");

//Report Module Over................................................

//Logistics Panel Over.................................................

//Buyer Panel Start.................................................

Route::get('','Buyer\HomeController@index')
        ->name('buyer_home_1');

Route::get('home','Buyer\HomeController@index')
        ->name('buyer_home');
        
Route::get('landing_home','Buyer\HomeController@index')
        ->name('buyer_landing_home');

Route::get('home_after_login','Buyer\LoginController@afterLogin')
        ->name('buyer_home_after_login');

Route::get('buyer/notification','Buyer\ProfileController@gettNotifications')
        ->name('buyer_notification')
        ->middleware("buyer_auth");

Route::get('buyer/getStates','Buyer\HomeController@getStates')
        ->name('buyer_getStates');

Route::get('buyer/getCountry','Buyer\HomeController@getCountries')
        ->name('buyer_getCountries');

Route::get('buyer/getCities','Buyer\HomeController@getCities')
        ->name('buyer_getCities');

Route::get('buyer/getSiteCities','Buyer\HomeController@getCities')
        ->name('buyer_getSiteCities');


//Login signup module.................................................

Route::post('buyer/login','Buyer\LoginController@login')
        ->name('buyer_login');

Route::get('buyer/login_mobile','Buyer\LoginController@login_mobile')
        ->name('buyer_login_mobile');

Route::get('buyer/logout','Buyer\LoginController@logout')
        ->name('buyer_logout');

Route::get('buyer/signup','Buyer\SignUpController@index')
        ->name('buyer_register');

Route::post('buyer/register','Buyer\SignUpController@signupRegistration')
        ->name('buyer_register');

Route::get('buyer/signup_email_verifier','Buyer\SignUpController@emailVerifier')
        ->name('buyer_signup_email_verifier');

Route::post('buyer/signup_otp_send','Buyer\SignUpController@signupOTPSend')
        ->name('buyer_signup_otp_send');

Route::get('buyer/signup_email_verifier_otp','Buyer\SignUpController@emailVerifierOtp')
        ->name('buyer_signup_email_verifier_otp');

Route::post('buyer/signup_otp_verify','Buyer\SignUpController@signupOTPVerify')
        ->name('buyer_signup_otp_verify');


Route::get('buyer/forgot_pass_email_verifier','Buyer\ForgotPassController@index')
        ->name('buyer_forgot_pass_email_verifier');

Route::post('buyer/forgot_pass_otp_send','Buyer\ForgotPassController@forgotPassOTPSend')
        ->name('buyer_forgot_pass_otp_send');

Route::post('buyer/forgot_pass_otp_verify','Buyer\ForgotPassController@forgotPassOTPVerify')
        ->name('buyer_forgot_pass_otp_verify');

Route::post('buyer/forgot_pass','Buyer\ForgotPassController@forgotPass')
        ->name('buyer_forgot_pass');

//Login signup module.................................................

//Product module.................................................

Route::get('get_regional_details','Buyer\ProductController@getRegionalDetails')
        ->name('buyer_get_regional_details');

Route::get('check_supplier_availability','Buyer\ProductController@checkSuppliersAvailable')
        ->name('buyer_check_supplier_availability');

Route::get('buyer/product_listing/{id}','Buyer\ProductController@index')
        ->name('buyer_product_listing');

Route::get('buyer/product_listing/{id}/{sub_cat_id}','Buyer\ProductController@index')
        ->name('buyer_product_listing_with_subcat');

Route::get('buyer/product_listing_ajax/{id}/{sub_cat_id}/{is_ajax}/{source_name}','Buyer\ProductController@index')
        ->name('buyer_product_listing_with_subcat_ajax');

Route::post('buyer/product_listing_form','Buyer\ProductController@productListing')
        ->name('product_listing_form');

Route::get('buyer/cart','Buyer\CartController@index')
        ->name('buyer_cart_checkout')
        ->middleware("buyer_auth");

Route::get('buyer/product_detail/get_single_mix_combination','Buyer\ProductController@getSingleDesignMixCombinationByBrandIds')
        ->name('buyer_design_mix_get_single_mix_combination');

Route::get('buyer/product_detail/{id}','Buyer\ProductController@product_detail')
        ->name('buyer_product_detail');



Route::post('buyer/product_add_to_cart','Buyer\ProductController@addTocart')
        ->name('buyer_product_add_to_cart')
        ;



Route::get('buyer/add_site_to_cart','Buyer\CartController@addSiteIdToCart')
        ->name('buyer_add_site_to_cart');

Route::post('buyer/delete_cart_item','Buyer\CartController@deleteCartItem')
        ->name('buyer_delete_cart_item');

Route::get('buyer/change_site_to_cart','Buyer\CartController@changeSiteDetails')
        ->name('buyer_change_site_to_cart');

Route::get('buyer/add_billing_to_cart','Buyer\CartController@addBiilingIdToCart')
        ->name('buyer_add_billing_to_cart');

Route::get('buyer/place_order','Buyer\CartController@placeOrder')
        ->name('buyer_place_order')
        ->middleware("buyer_auth");


Route::get('wishlist','Buyer\WishlistController@index')
        ->name('buyer_wishlist');

Route::post('buyer/product_add_to_wish_list','Buyer\ProductController@addToWishList')
        ->name('buyer_product_add_to_wish_list');

Route::post('buyer/delete_wishlist_item','Buyer\ProductController@deleteWishListItem')
        ->name('buyer_delete_wishlist_item');

Route::get('coupon_apply','Buyer\CartController@apply_coupon')
        ->name('buyer_coupon_apply')
        ;

//Product module.................................................

//ORDER module start.................................................

Route::get('orders','Buyer\OrderController@index')
        ->name('buyer_order')
        ->middleware("buyer_auth");

Route::get('order_detail/{id}','Buyer\OrderController@orderDetails')
        ->name('buyer_order_detail')
        ->middleware("buyer_auth");

Route::post('buyer/order_cancel','Buyer\OrderController@cancelOrder')
        ->name('buyer_order_cancel');

Route::get('buyer/order_item_cancel','Buyer\OrderController@cancelOrderItem')
        ->name('buyer_order_cancel');

Route::post('buyer/order_product_review','Buyer\OrderController@addProductReview')
        ->name('buyer_order_product_review');

Route::post('buyer/order_complain','Buyer\OrderController@orderComplain')
        ->name('buyer_order_complain');
        
Route::post('buyer/feedback','Buyer\OrderController@orderFeedback')
        ->name('buyer_feedback');

Route::post('buyer/order_assign_qty','Buyer\OrderController@assignQtyOrder')
        ->name('buyer_order_assign_qty');
        
Route::get('buyer/order_reassign_status_change','Buyer\OrderController@reassignStatusChange')
        ->name('buyer_order_reassign_status_change');
        
Route::get('buyer/order_7_28_days_report_accept','Buyer\OrderController@accept7And28DaysReport')
        ->name('buyer_order_7_28_days_report_accept');
        
Route::post('buyer/order_7_28_days_report_reject','Buyer\OrderController@reject7And28DaysReport')
        ->name('buyer_order_7_28_days_report_reject');

Route::get('buyer/order_check_tm_assign','Buyer\OrderController@checkTMAvailability')
        ->name('buyer_order_check_tm_assign');

Route::get('buyer/order_track/{id}','Buyer\OrderController@trackOrderItem')
        ->name('buyer_order_track');

Route::get('buyer/order_track_detail/{item_id}/{tracking_id}','Buyer\OrderController@trackingDetails')
        ->name('buyer_order_track_detail');

Route::get('buyer/order_CP_track_detail/{item_part_id}','Buyer\OrderController@CPtrackingDetails')
        ->name('buyer_order_CP_track_detail');

Route::get('buyer/reports/order_report_export','Buyer\OrderController@buyerOrderExport')
        ->name('buyer_order_report_export')
        ->middleware("buyer_auth");

//ORDER module.................................................

//Profile module.................................................

Route::get('buyer/profile','Buyer\ProfileController@index')
        ->name('buyer_profile')
        ->middleware("buyer_auth");

Route::post('buyer/profile_edit','Buyer\ProfileController@editProfile')
        ->name('buyer_profile_edit');

Route::get('buyer/profile_edit','Buyer\ProfileController@editProfile')
        ->name('buyer_profile_edit_get');

Route::get('buyer/get_site','Buyer\ProfileController@getSites')
        ->name('buyer_get_site')
        ->middleware("buyer_auth");

Route::get('buyer/update_pass','Buyer\ProfileController@getPassword')
        ->name('buyer_get_password')
        ->middleware("buyer_auth");

Route::post('buyer/add_site','Buyer\ProfileController@addSites')
        ->name('buyer_add_site')
        ->middleware("buyer_auth");

Route::get('buyer/check_site_exist','Buyer\ProfileController@checkSiteExist')
        ->name('buyer_check_site_exist');

Route::post('buyer/edit_site','Buyer\ProfileController@editSites')
        ->name('buyer_edit_site')
        ->middleware("buyer_auth");

Route::post('buyer/change_pass','Buyer\ProfileController@changePass')
        ->name('buyer_change_pass');

Route::get('buyer/delete_site','Buyer\ProfileController@deleteSites')
        ->name('buyer_delete_site');

Route::get('buyer/request_otp','Buyer\ProfileController@requestOTP')
        ->name('buyer_request_otp');

Route::get('buyer/request_otp_for_edit_profile','Buyer\ProfileController@requestOTPForEditProfile')
        ->name('buyer_request_otp');

Route::get('buyer/getStateByCountry','Buyer\ProfileController@getStateByCountry')
        ->name('buyer_getStateByCountry');

Route::get('buyer/getCityByState','Buyer\ProfileController@getCityByState')
        ->name('buyer_getCityByState');

Route::get('buyer/show_billing_address','Buyer\ProfileController@showBillngAddress')
        ->name('buyer_show_billing_address')
        ->middleware("buyer_auth");

Route::post('buyer/add_billing_address','Buyer\ProfileController@addBillngAddress')
        ->name('buyer_add_billing_address');

Route::post('buyer/edit_billing_address','Buyer\ProfileController@editBillngAddress')
        ->name('buyer_edit_billing_address');

Route::get('buyer/delete_billing_address','Buyer\ProfileController@deleteBilling')
        ->name('buyer_delete_billing_address');
        
Route::get('buyer/show_custom_rmc','Buyer\ProfileController@showCustomRMC')
        ->name('buyer_show_custom_rmc')
        ->middleware("buyer_auth");

Route::post('buyer/add_custom_rmc','Buyer\ProfileController@addCustomRMC')
        ->name('buyer_add_custom_rmc');

Route::post('buyer/edit_custom_rmc','Buyer\ProfileController@editCustomRMC')
        ->name('buyer_edit_custom_rmc');

Route::get('buyer/delete_custom_rmc','Buyer\ProfileController@deleteCustomRMC')
        ->name('buyer_delete_custom_rmc');
        
Route::get('buyer/delete_account','Buyer\ProfileController@deleteAccount')
        ->name('buyer_delete_account');
//Profile module.................................................

//Support Ticket Module Start

Route::get('buyer/my_support_ticket','Buyer\SupportTicketController@index')
        ->name('buyer_my_support_ticket')
        ->middleware("buyer_auth");

Route::get('buyer/my_support_ticket/mobile/{custom_token}','Buyer\SupportTicketController@mobileSupportTicket')
        ->name('buyer_my_mobile_support_ticket');

Route::get('buyer/my_support_ticket/filters','Buyer\SupportTicketController@index')
        ->name('buyer_ticket_filters');

Route::post('buyer/my_support_ticket/add','Buyer\SupportTicketController@addTicket')
        ->name('buyer_ticket_add');

Route::post('buyer/my_support_ticket/reply','Buyer\SupportTicketController@replyTicket')
        ->name('buyer_ticket_reply');

Route::get('buyer/my_support_ticket/ticket_detail/{id}','Buyer\SupportTicketController@showTicketDetails')
        ->name('buyer_ticket_ticket_detail')
        ->middleware("buyer_auth");

Route::get('buyer/my_support_ticket/mobile/ticket_detail/{id}','Buyer\SupportTicketController@showTicketDetails')
        ->name('buyer_ticket_mobile_ticket_detail');


Route::get('contact_us','Buyer\ContactController@index')
        ->name('buyer_contact_us');

Route::get('about_us','Buyer\ContactController@aboutUs')
        ->name('buyer_about_us');

Route::get('shipping_policy','Buyer\ContactController@shippingPolicy')
        ->name('buyer_shipping_policy');

Route::get('privacy_policy','Buyer\ContactController@privacyPolicy')
        ->name('buyer_privacy_policy');
        
Route::get('supplier_privacy_policy','Buyer\ContactController@supplierPrivacyPolicy')
        ->name('supplier_privacy_policy');
        
Route::get('terms_of_use','Buyer\ContactController@termsOfUse')
        ->name('buyer_terms_of_use');
        
Route::get('supplier_terms_of_use','Buyer\ContactController@supplierTermsOfUse')
        ->name('supplier_terms_of_use');
        
Route::get('return_policy','Buyer\ContactController@returnPolicy')
        ->name('buyer_return_policy');
        
Route::get('supplier_return_policy','Buyer\ContactController@supplierReturnPolicy')
        ->name('supplier_return_policy');

Route::get('faq','Buyer\ContactController@faq')
        ->name('buyer_faq');

Route::get('buyer/notifications','Buyer\HomeController@notification')
        ->name('buyer_notifications');

//Support Ticket Module End

//Product Module start...............................................

Route::post('buyer/custom_mix','Buyer\CustomMixController@index')
        ->name('buyer_custom_mix');

Route::get('buyer/custom_mix','Buyer\CustomMixController@index')
        ->name('buyer_custom_mix');

Route::get('buyer/mix_detail/{vendor_id}/{address_id}','Buyer\CustomMixController@showCustomDetail')
        ->name('buyer_mix_detail');

//Product Module over...............................................

// Post Route For Makw Payment Request
Route::post('payment', 'Buyer\RazorpayController@payment')->name('payment');

Route::get('buyer/get_agregate_sub_cat_2/{id}','Supplier\DesignMixController@getAggregateSubCat2')
        ->name('buyer_get_agregate_sub_cat_2');

Route::get('buyer/get_admixture_types','Supplier\DesignMixController@getAdmixtureTypes')
        ->name('buyer_get_admixture_types');


//Buyer Panel Over.................................................
