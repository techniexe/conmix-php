 $(document).ready(function() {
 	// for multiple select
	$('.multiple_select').multiselect();

	//qty script start*/
	$('.qty_add').click(function () {
	    if ($(this).prev().val() < 999 ) {
	        $(this).prev().val(+$(this).prev().val() + 1);
	    }
	});
	$('.qty_sub').click(function () {
	    if ($(this).next().val() > 1) {
	    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
	    }
	});
	//qty script end

	// login signup popup
	$( "#show_singup_m" ).click(function() {
		setTimeout(function(){
			$('#Signup_popup').modal('show');
		}, 500);
	});
	$( "#show_login_m" ).click(function() {
		setTimeout(function(){
			$('#Login_popup').modal('show');
		}, 500);
	});
	$( "#show_popup_otp" ).click(function() {
		setTimeout(function(){
			$('#Signup_popup_otp').modal('show');
		}, 500);
	});
	$( "#show_reg_form" ).click(function() {
		setTimeout(function(){
			$('#Registration_form').modal('show');
		}, 500);
	});
});